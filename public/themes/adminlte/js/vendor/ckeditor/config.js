/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {

	config.skin = 'bootstrapck';

	config.toolbar = [
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'document', items: [ 'Source', /*'-', 'Save', 'NewPage', 'Preview', 'Print', */ '-', 'Templates', '-', 'Scayt' ] },
		
		/*{ name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-' ] },
		/*{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },*/
		'/',
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', /* 'BidiLtr', 'BidiRtl', 'Language' */] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', /*'Flash',*/ 'Table', 'HorizontalRule', /*'Smiley',*/ 'SpecialChar'/*, 'PageBreak', 'Iframe'*/ ] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize'/*, 'ShowBlocks'*/ ] },
		{ name: 'about', items: [ 'About' ] }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Make dialogs simpler.	
	config.removeDialogTabs = 'image:advanced';
	config.allowedContent = true;
	config.filebrowserImageBrowseLinkUrl = '';
	config.filebrowserBrowseUrl =  Asgard.mediaGridCkEditor;

	config.extraAllowedContent = 'div(*); a div;a span;a i;a h1;a h2;a h3;a h4;a p;a img; a img h2 p';
	//url de templates
	//config.templates = 'default';
	config.templates_replaceContent = false;
	config.templates_files = [ '/themes/adminlte/js/vendor/ckeditor/plugins/templates/templates/default.js' ];
};

CKEDITOR.dtd.$removeEmpty['i'] = false;
CKEDITOR.dtd.$removeEmpty.span = false; 
CKEDITOR.dtd.$removeEmpty.i = false;
CKEDITOR.dtd.$removeEmpty['a'] = 1;
CKEDITOR.dtd.$inline['a'] = 0;
CKEDITOR.dtd.a.h2 = 1;
CKEDITOR.dtd.a.h1 = 1;
CKEDITOR.dtd.a.h3 = 1;
CKEDITOR.dtd.a.h4 = 1;
CKEDITOR.dtd.a.h5 = 1;
CKEDITOR.dtd.a.h6 = 1;
CKEDITOR.dtd.a.p = 1;
CKEDITOR.dtd.span.a = 1;
