$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        statusCode: {
            404: function(res) {
                AppGeneral.notificaciones("warning", $.parseJSON(res.responseText).message);
            },
            500: function(res) {
                AppGeneral.notificaciones("error", $.parseJSON(res.responseText).message);
            }
        } 
    });

    $('[data-slug="source"]').each(function(){
       $(this).slug();
   });

    $(document).ajaxStart(function() { Pace.restart(); });

    Mousetrap.bind('f1', function() {
        window.open('https://asgardcms.com/docs', '_blank');
    });

    $('input[type="checkbox"], input[type="radio"]').iCheck({
    	checkboxClass: 'icheckbox_flat-blue', 
    	radioClass: 'iradio_flat-blue'
    });


    //solo numeros
    $('.soloNumeros').on('keypress', function( e ){
        if( !/\d+/.test( e.key ) ){
            return false;
        }

        return ;
    });
    
    //solo letras
    $('.soloLetras').on('keypress', function( e ){
      return soloLetras(e);
    });
    //alfanumerico
    $('.alfanumerico').on('keypress', function( e ){
        if( !/[A-Za-z0-9]+/.test( e.key ) ){
            return false;
        }

        return ;
    });

    //valida los formularios que tengan 
    if( $("form.formValida").length ) {
        $("form.formValida").submit(function(){

            var error = true;
            $(this).find("span.text-danger").hide();

            //buscando todos los campos que 
            //contengan la clase requerido 
            
            var focus = true;
            $(this).find(".requerido").each(function(){

                var objReq = $(this);
                var padreObj = objReq.parent();                

                var valor = $.trim( objReq.val() );

                if( objReq.hasClass("ckeditor")){

                    valor = $.trim( CKEDITOR.instances[ objReq.attr("id") ].getData() );
                
                }else if ( objReq.hasClass("selectized")){
                
                     var valorSlectize = $(this).selectize().val();
                     valor = '';

                    if( valorSlectize ){

                        valor = $.trim( valorSlectize.join(','));
                    }                    
                }

                if( valor != '' ){

                    if( objReq.hasClass("validaRut") && !$.validateRut( valor ) ){
                        error = false;
                        padreObj.find("span.text-danger.errorValidaRut").show();
                    }

                    if( objReq.hasClass("validaEmail") && !validarEmail( valor ) ){
                        error = false; 
                        padreObj.find("span.text-danger.erroValidaEmail").show(); 
                    }

                    if( objReq.hasClass("validaPassword") && 
                            ( 
                                valor.length < 8  || 
                                valor.length > 15  || 
                                !/\d+/.test(valor) ||
                                !/[a-z]+/.test(valor) ||
                                !/[A-Z]/.test(valor) 
                                ) 
                            ){
                            error = false; 
                        padreObj.find("span.text-danger.errorValidaPassword").show();
                    }


                }else{
                                    
                    error = false;
                    padreObj
                    .find("span.text-danger")
                    .not(".erroValidaEmail")
                    .not(".errorValidaPassword")
                    .not(".errorValidaRut")
                    .show();

                    if( objReq.hasClass("archivo")){
                        padreObj.parent().find("span.text-danger").show();
                    }
                }

                if( !error && focus ){
                    if( objReq.hasClass("archivo")){

                        $(window).scrollTop( padreObj.parent().find("label").position().top );
                        focus = false;
                    }else if (objReq.hasClass("fileInput")) {

                        $(window).scrollTop(padreObj.parent().parent().position().top);
                        focus = false;
                    } else{
                        $(window).scrollTop( padreObj.find("label").position().top );
                        focus = false;
                    }

                }

            });

            $("#contenedor_formulario").find("span.text-danger").hide();

            if( !error ){
                return false;
            }

            return true;
        });
    }

});

function toUnicode(theString) {
    var letra = theString; 
    var theUnicode = theString.charCodeAt(0) 
    .toString(16) 
    .toUpperCase(); 

    while (theUnicode.length < 4) {
        theUnicode = '0' + theUnicode; 
    } 

    theUnicode = '\\u' + theUnicode; 
    if( !/(u00E1|u00E9|u00ED|u00F3|u00FA|u00FC|u00F1)+/i.test( theUnicode ) ){
        return false;
    } 

    return true;
}

var soloLetras = function(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " �����abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(!tecla_especial && ( !/(\s|[a-z])/i.test( tecla ) && !toUnicode( tecla ) )){
        return false;
    }
};

var validarEmail = function( strEmail ){
    var email = strEmail.replace(/\s+/g, "");

    if( /(\w|\-|\_|\.)+\@(\w|\-|\_|\.)+\.(\w|\.)+/.test( email ) ){
        return true;
    }

    return false;
};