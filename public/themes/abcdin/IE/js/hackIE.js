$(document).ready(function(){

   //funcion para arreglar intervenir el tamaño de algunos elementos
   var fnWindowRes = function(){
      //tag header
      var objTarget = $("body > #header > div.header-top.container-fluid");
      var objPadre = objTarget.parent().width();
      var selector = 'section.Banners a.bannerAnchoCompleto > img.img-fluid';

      objTarget.width( objPadre + 'px' );
     $("body > #header > div.header-top.container-fluid > div.row")
      .width((objPadre + 30) + 'px');

      return $(selector)
        .width( objPadre + 'px' );
   };
   
    fnWindowRes();

   //obteniendo el tamaño de la ventana, al momento de 
   // resize
   return $(window).on("resize", function(){
      return fnWindowRes();
   });
   
});