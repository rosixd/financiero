var AppValidacion = function(){

	var baseurl, consultatarjeta, tarjetaseleccionada = '0', rut, clave, ajaxlogin = null;

	var init = function(){
		AppValidacion.validarRut();
		AppValidacion.bloqueoDesbloqueo();
		$('#myPopover .list-group').on('click', 'input', function(){
			AppValidacion.tarjetaseleccionada = $(this).val();
			AppValidacion.checkedpopover();
			AppValidacion.validarBoton();

			datalayerpush({
				'event': 'loginelecciontarjeta',
				'category': 'click',
				'label': 'loginelecciontarjeta'
			});
		});
		$( "#validarRut, #validarClave").keyup(function( event ) {
			AppValidacion.validarBoton();
		});
	};

	var validarBoton = function() {

		var rut = $( "#validarRut").val();
		var clave = $( "#validarClave").val();

		 if (rut.length >= 3 && clave.length === 4 && AppValidacion.tarjetaseleccionada.length == 4) {
		/*if (rut.length >= 9 && AppValidacion.tarjetaseleccionada.length == 4) {*/
			$( "#boton").removeClass('btninactivo').removeAttr("disabled").addClass("btnactivo");
		} else {
			$( "#boton").addClass('btninactivo').attr("disabled", "disabled").removeClass("btnactivo");
		}
	};

	var validarRut = function(){

        $( "#validarRut").blur(function(){

            var rut = $.trim( $(this).val() );
			/* rut ingresado no es valido */
            if( !$.validateRut(rut) ){

                ($(this).val() == '') ? $("#login").addClass("d-none") : $("#login").removeClass("d-none");
				return true;
			}
			
			$("#login").addClass("d-none");
			rut = $.formatRut(rut, false)
			$(this).val(rut);

			if (rut == AppValidacion.rut) {
				return true;
			}

			AppValidacion.rut = rut;
			
			$('#myPopover .list-group').html('');
			$(".popover").remove();

			var tc = 0, ic=0, htmls = '', tarjetatc = [], tarjetaic= [];

			$.ajax({
				'url': AppValidacion.consultatarjeta,
				'data': {
					'rut': rut
				},
				'type': 'POST',
				'success': function(r) {

					/*if(r.estatus_cliente == '99'){/* Servicios ABCDIN no disponbles */
						/*$("#fueraservicio").removeClass("d-none");
						return;
					}*/

					if( r.estatus_cliente == "00" && r.error == false ){

						$.each(r.datos, function(id, tarjeta) {
							AppValidacion.tarjetaseleccionada = '0';
							if(tarjeta.TipoTarjeta == 'TC'){
								tc++;
								tarjetatc.push(tarjeta.tarjeta);
							}
							if(tarjeta.TipoTarjeta == 'IC'){
								ic++;
								tarjetaic.push(tarjeta.tarjeta);
							}
							htmls += '<li class="list-group-item">\
							<div class="custom-control custom-radio">\
								<input type="radio" id="customRadio'+id+'" name="customRadio'+id+'" class="custom-control-input" value="'+tarjeta.tarjeta+'">\
								<label class="custom-control-label" for="customRadio'+id+'">Tarjeta '+tarjeta.ValorProcesoFlujo+'</label>\
							</div>\
							</li>';
						});	

						if(ic === 1){
							tc = 0 ;
							AppValidacion.tarjetaseleccionada = tarjetaic[0];
						}

						if(ic > 1 || tc > 1){
							AppValidacion.popover();
							$('#myPopover .list-group').append(htmls);

							tc = 0 ;
						}

						if(tc === 1){
							AppValidacion.tarjetaseleccionada = tarjetatc[0];
						}
					}

					/* El cliente no existe */
					if( r.estatus_cliente == '01' && r.error == true || r.estatus_cliente == '1' && r.error == true){
						AppValidacion.tarjetaseleccionada = 'Ar45';
					}

					/* No hay servicio de tarjetas */
					if( r.estatus_cliente == '99' && r.error == true){
						AppValidacion.tarjetaseleccionada = 'Ar45';
					}

				},
				'beforeSend':function(){
					/*$("#validarClave").attr("readonly", "readonly");*/
					$("#boton").removeClass("btnactivo").addClass("btninactivo").attr("disabled", "disabled").children().removeClass("far fa-arrow-alt-circle-right").attr("disabled", "disabled").addClass('fa fa-spinner fa-spin fa-fw');
				},
				'complete': function(){
					AppValidacion.validarBoton();
					$("#boton")
						.children()
						.removeClass("fa fa-spinner fa-spin fa-fw")
						.addClass('far fa-arrow-alt-circle-right');
				}
			});

        });
	}

	var verificarDatos = function(){

		if(AppValidacion.tarjetaseleccionada != 'undefined'){

			var clave = $('#validarClave').val();
			var rut = $('#validarRut').val();
			var tarjeta = AppValidacion.tarjetaseleccionada;

			ajaxlogin = $.ajax({
				'url': AppValidacion.login,
				'data': {
					'rut': rut,
					'password': clave,
					'tarjeta': tarjeta
				},
				'type': 'POST',
				'success': function(r) {

					/* Servicios ABCDIN no disponbles */
					if(r.error){

						if( r.ABC ){ /* eliminacion de tarjeta abc */
							$(".mensajeAlerta").html( r.msg );
							$('#modalAlerta').modal('show');
							return;
						}

						if(r.data == '99'){ /* Servicios ABCDIN no disponbles */
							$("#fueraservicio").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == '99'){ /* Servicios ABCDIN no disponbles */
							$("#fueraservicio").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == "01"){ /* Cliente no existe */
							$("#login").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == "02"){ /* Clave invalida */
							datalayerpush({
								'event': 'rutoclaveinvalidologin',
								'category': 'click',
								'label': 'rutoclaveinvalidologin'
							});
							$("#login").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == "03"){ /* Clave bloqueada blanda */
							datalayerpush({
								'event': 'bloqueoxfallologin',
								'category': 'click',
								'label': 'bloqueoxfallologin'
							});
							$("#bloqueoblando").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == "06"){ /* Clave bloqueada duro */
							datalayerpush({
								'event': 'bloqueoxfallologin',
								'category': 'click',
								'label': 'bloqueoxfallologin'
							});
							$("#bloqueoduro").removeClass("d-none");
							return;
						}

						if(r.estatus_cliente == "04"){ /* CSI Ofsett vacio */
							$("#login").removeClass("d-none");
							return;
						}
					}

					if( r.estatus_cliente == "00" ){ /* Cliente existe */
						datalayerpush({
							'event': 'loginsitioprivado',
							'category': 'click',
							'label': 'loginsitioprivado'
						});
						window.location.href = r.url;
					}
				},
				'error': function(r) {
					/* Servicios ABCDIN no disponbles */
					$("#fueraservicio").removeClass("d-none");
					return;
				},
				'beforeSend':function(){
					if(ajaxlogin != null) {
						ajaxlogin.abort();
					}
					AppValidacion.closepopover();

					$("#validarClave, #validarRut").attr("readonly", "readonly");
					$("#boton").removeClass("btnactivo").addClass("btninactivo").attr("disabled", "disabled").children().removeClass("far fa-arrow-alt-circle-right").addClass('fa fa-spinner fa-spin fa-fw');
				},
				'complete': function(){
					ajaxlogin = null;
					$("#validarClave , #validarRut").removeAttr("readonly");
					$("#boton").removeClass("btninactivo").addClass("btnactivo").removeAttr("disabled").children().removeClass("fa fa-spinner fa-spin fa-fw").addClass('far fa-arrow-alt-circle-right');
				}
			});
		}
	}

    var checkedpopover = function() {
		$('.popover #myPopover .list-group input')
		.removeClass('checked')
		.prop('checked', false);

		$('.popover #myPopover .list-group input[value="' + AppValidacion.tarjetaseleccionada + '"]')
		.addClass('checked')
		.prop('checked', false);
	};

	var closepopover = function(){
		/* setTimeout(function () { */
			$('.popover #myPopover .list-group input')
				.removeClass('checked')
				.prop('checked', false);

			$('.popover #myPopover .list-group input[value="' + AppValidacion.tarjetaseleccionada + '"]')
				.addClass('checked')
				.prop('checked', false);
			$('.popover').popover('hide');
		/* }, 2000); */
	}

	var popover = function(){
		//$('[data-toggle="popover"]').popover(); 
		
		$('[rel="popover"]').popover({
			container: 'body',
			html: true,
			content: function () {
				var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');

			AppValidacion.checkedpopover();
				
				return clone;
			}
		}).click(function(e) {
			e.preventDefault();
			AppValidacion.checkedpopover();
		});

		$('[rel="popover"]').popover('show');
	}
	
	var bloqueoDesbloqueo= function(){

		$( ".bdchecked").click(function(){
    
			var operacion = $('.vchecked').prop('checked');
      		var text = 'desbloquear';
      
			if( operacion === false ){
				var text = 'bloquear';
			}

			swal({
				title: "",
				text: "¿Está seguro de " + text + " su tarjeta?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				confirmButtonClass: "btn-danger btn-md",
				confirmButtonText: "SI",
				cancelButtonText: "NO",
				reverseButtons: false,
				showCancelButton: true,
			}, function(OK){
				if (OK) {
					$.ajax({
						'url': urlabloqueodesbloqueo,
						'data': {
							'data': operacion
						},
						'type': 'GET',
						'success': function(respuesta) {
							//console.log(respuesta);
							if( respuesta.estatus_cliente == 99 && respuesta.error === true ){

								notificaciones('error', respuesta.msg );
							}

							if( respuesta.estatus_cliente == 1 && respuesta.error === false ){

								if(respuesta.datos.PCODRETORNO == '0'){

									notificaciones('success', respuesta.datos.PDESCRETORNO );
								}
								if(respuesta.datos.PCODRETORNO == '1' || respuesta.datos.PCODRETORNO == '2'|| respuesta.datos.PCODRETORNO == '3'|| respuesta.datos.PCODRETORNO == '4'|| respuesta.datos.PCODRETORNO == '5' ){

									$('.vchecked').prop('checked', operacion);
								}

								//notificaciones('warning', respuesta.datos.PDESCRETORNO );
							}
						},'error': function(respuesta) {
							notificaciones('error', respuesta.msg );
						}
					});
				}else{
					$('.vchecked').prop('checked', operacion);
				}
			});
		});

	}

	return {
		init:function(){
			init();
		},
		bloqueoDesbloqueo:bloqueoDesbloqueo,
		checkedpopover:checkedpopover,
    	closepopover:closepopover,
		validarBoton:validarBoton,
		verificarDatos:verificarDatos,
		popover:popover,
		tarjetaseleccionada:tarjetaseleccionada,
		rut:rut,
		clave:clave,
		validarRut:validarRut
	}
	
}();

$(document).ready(function(){
	AppValidacion.init();
});