var AppMicuenta = function(){
	var init = function(){
		AppMicuenta.actualizardatos();
	};

	var actualizardatos = function(){
		
/* 		$(".custom-control-label").click(function () {
			var t = $(this),
				id = t.attr('for');
			
			$("#" + id).prop('checked', true);

			var valor = $("[name='radiostacked']:checked", "#formactualizadatos").val();

			if( valor ){
				delete lista_errores['radiostacked'];
        		render_error();
			}
		}); */
		var msj = 'Estimado cliente, debes dirigirte a tienda para autorizar envío de SMS para actualizar datos.';
		$(".msjsmsactivo").html('');

		if( $('#smsactivo').val() != 'S'  ){
			$('.btnRojo', '#formactualizadatos').prop('disabled');
			$(".msjsmsactivo").append('<br><strong><span class="rojo" style="font-size: 13px;">' + msj + '</span></strong><br>');
		}

		if( $("#cuentaemail", "#formactualizadatos").prop('checked') ){
			$(".checkemail").removeClass('d-none');
			$("#cuentaemail", "#formactualizadatos").attr('type','radio');
		}
		
		$(".custom-control-label").click(function () {
			var t = $(this),
				id = t.attr('for'), 
				checked = $("#" + id).prop('checked');
			
			if($("#cuentaemail", "#formactualizadatos").attr('type') == 'checkbox'){
				
				$("#" + id).prop('checked', !checked);
				checked = !checked;
			}

			$("#emails").val($("#emails").data('original'));
			$("#confirmar").val($("#confirmar").data('original'));

			if(checked){
				$(".checkemail").removeClass('d-none');
			}else{
				$(".checkemail").addClass('d-none');
			}

			var valor = $("[name='radiostacked']:checked", "#formactualizadatos").val();

			if( valor ){
				$("#emails").addClass('requerido');
			}

			var serializeActual = $('#formactualizadatos').serialize();
			delete lista_errores['confirmar_mail'];
			delete lista_errores['confirmar'];
			delete lista_errores['emails'];
      		render_error();

			if ($('#smsactivo').val() == 'S') {
				$('.btnRojo', '#formactualizadatos').prop('disabled', serialize == serializeActual);
			}
			return false;
		});

		$('.boxAutorizaTransferencia').submit( function () {
			return false;
		});

		$('#ModalAutorizarTransferencia').on('show.bs.modal', function () {

			$('#clavedinamica').val('');
		});

		$('#ModalExito').on('show.bs.modal', function () {

			setTimeout(function(){ 
				location.reload();
			}, 5000);
		});

		$('#ModalError').on('show.bs.modal', function () {

			setTimeout(function(){ 
				location.reload();
			}, 5000);
		});

		$('.btnRojo', '#ModalAutorizarTransferencia').click(function( e ) {

			$('#codigo').val($("#clavedinamica").val());
   
			$.ajax({
				'url': urlactualizadatos,
				'method': 'POST',
				'data': $('#formactualizadatos').serialize(),
				'success': function(respuesta) {
					$('#ModalAutorizarTransferencia').modal('hide');
					
					if(respuesta.error == false && respuesta.datos ){
						$('#ModalExito').modal();
						datalayerpush({
							'event': 'suscribireeccdigital',
							'category': 'click',
							'label': 'suscribireeccdigital'
						});
						return;
					}
					
					if( respuesta.error ){
						$('#ModalError').modal();
						return;
					}
				},
				'error': function(respuesta) {
					$('#ModalAutorizarTransferencia').modal('hide');
					$('#ModalError').modal();
					return;
				}
			});
		});
		
		window.serialize = $('#formactualizadatos').serialize();
		$('input', '#formactualizadatos').keyup(function() {
			var serializeActual = $('#formactualizadatos').serialize();
			if ($('#smsactivo').val() == 'S') {
				$('.btnRojo', '#formactualizadatos').prop('disabled', serialize == serializeActual);
			}
		});

		$('select', '#formactualizadatos').change(function() {
			var serializeActual = $('#formactualizadatos').serialize();
			if ($('#smsactivo').val() == 'S') {
				$('.btnRojo', '#formactualizadatos').prop('disabled', serialize == serializeActual);
			}
		});
		

		$('#volverenviar').click( function () {
			$('#formactualizadatos').submit();
		});

	    $('#formactualizadatos').submit(function( e ) {
			//e.preventDefault();
			//console.log( $("[name='radiostacked']:checked", "#formactualizadatos").val() );
			/* if( $("[name='radiostacked']:checked", "#formactualizadatos").val() ){
				datalayerpush({
					'event': 'suscribireeccdigital',
					'category': 'click',
					'label': 'suscribireeccdigital'
				});
			} */
			var error = validarForm(this);
			
			/*if( !$("[name='radiostacked']:checked", "#formactualizadatos").val() ){
				lista_errores['radiostacked'] = 'Debe seleccionar un envío de estado de cuenta.';
				error = true;
			} */
			$(".msjval").html('');

			if( $('#smsactivo').val() != 'S'  ){
				$('.btnRojo', '#formactualizadatos').prop('disabled');
				$(".msjval").append('<br><strong><span class="rojo" style="font-size: 13px;">' + msj + '</span></strong><br>');
				return false;
			}

			if (Object.keys(lista_errores).length > 0 || error) {
				e.stopImmediatePropagation();
				render_error();
				return false;
			}
		
			//var data = $('#formactualizadatos').serialize();
			//console.log(data);

			$.ajax({
				'url': urlvalidar,
				'type': 'GET',
				'success': function(respuesta) {
					if( respuesta.error == false ){
						$('#ModalAutorizarTransferencia .rojo.email').html($("#email").data('original'));
						$('#ModalAutorizarTransferencia .rojo.telefono').html($("#telefonocelularoriginal").data('original'));
						$('#ModalAutorizarTransferencia').modal('show');
						
					}
					if( respuesta.error ){
						$('#ModalServiciomantenimiento').modal();
						setTimeout(function(){ 
							window.location.href = urlhome;
						}, 5000);
					}
				}
			});

			return false;
		});
	}

	return {
		init:function(){
			init();
		},
		actualizardatos:actualizardatos
	}
	
}();

$(document).ready(function(){
	AppMicuenta.init();
});
