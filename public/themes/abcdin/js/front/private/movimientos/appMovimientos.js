var AppMovimientos = function() {

    var baseurl, urlmovimientosNoFacturados;

    var init = function() {
        AppMovimientos.movimientosNoFacturados();
    };

    var movimientosNoFacturados = function() {

        $.ajax({
            'url': AppMovimientos.urlmovimientosNoFacturados,
            'type': 'GET',
            'success': function(movimientosnofacturados) {

                /*RH: Servicio no Disponible*/
                if (movimientosnofacturados.error && movimientosnofacturados.datos.estado_respuesta == '99') {
                    notificaciones('error', movimientosnofacturados.msg);
                    return;
                }

                var html = '';


                /*RH: No tiene movimientos no facturados*/
                if (!movimientosnofacturados.data) {
                    $('#movimientosnofacturados').addClass('noFacturado');
                    html += '<tr>\
                                <td class="col-xs-1 text-left" colspan="4">\
                                <div class="noFacturados">No tiene movimientos no facturados</div>\
                                </td>\
                            </tr>';
                } else {
                    html += '<thead>\
                            <tr>\
                                <td style="width: 100%;">\
                                    <a class="btnAccion" href="descargar-movimientos-no-facturados" download="Movimientos No Facturados" data-toggle="tooltip"data-placement="top" title="Descargar" id="botondescargarmov">\
                                        <img src="/themes/abcdin/img/icono-descargar.png" class="descargar">\
                                        Descargar\
                                    </a>\
                                    <br>\
                                    <div class="clearfix"></div>\
                                </td>\
                            </tr>\
                            <tr>\
                                <th class="col-xs-1 w1"></th>\
                                <th class="col-xs-7 text-center w2">MONTO</th>\
                                <th class="col-xs-1 text-center w3">CUOTA</th>\
                                <th class="col-xs-1 text-right w4">VALOR CUOTA</th>\
                            </tr>\
                        </thead>\
                        <tbody class="points_table_scrollbar">';
                }

                /*RH: si tiene movimientos*/
                if (movimientosnofacturados.data) {
                    $.each(movimientosnofacturados.datos, function(id, movimientosNF) {
                        html +=
                            '<tr>\
                                <td class="col-xs-1 w1 text-left">\
                                    <span> ' + movimientosNF.Fecha + ' / ' + movimientosNF.TipMov + '</span>\
                                    <h4>' + movimientosNF.Sucursal + '</h4>\
                                </td>\
                                <td class="col-xs-7 text-center w2">$' + (movimientosNF.Monto == '' ? '0' : movimientosNF.Monto) + '</td>\
                                <td class="col-xs-1 text-center w3">' + movimientosNF.Cuota + '</td>\
                                <td class="col-xs-1 text-right w4">$' + (movimientosNF.ValorCuota == '' ? '0' : movimientosNF.ValorCuota) + '</td>\
                            </tr>';
                    });
                }
                html += '</tbody>';

                $('#movimientosnofacturados').html(html);
                return;
            },
            'error': function(r) {
                /* Servicios ABCDIN no disponbles */
                $("#movimientosnofacturados").html('<tr><td colspan="4" class="col-xs-10 text-center" style="margin-top: 10%; margin-bottom: 10%;">' + servicionodisponible + '</tr></td>');
                return;
            },
            'beforeSend': function() {
                $("#movimientosnofacturados").html('<tr><td colspan="4" class="col-xs-10 text-center" style="margin-left: 250px; margin-top: 10%; margin-bottom: 10%;"><i class="fas fa-circle-notch fa-spin fa-3x"></i></tr></td>');

            }
        });

    }

    return {
        init: function() {
            init();
        },
        movimientosNoFacturados: movimientosNoFacturados
    }

}();

$(document).ready(function() {
    AppMovimientos.init();
});