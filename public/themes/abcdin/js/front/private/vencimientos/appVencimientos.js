var AppVencimientos = function(){

	var baseurl, urlvencimientos;

	var init = function(){
        AppVencimientos.vencimientos();
	};
  
	var vencimientos = function(){
	
        $.ajax({
            'url': AppVencimientos.urlvencimientos,
            'type': 'GET',
            'success': function(vencimientos) {
           
                if(vencimientos.error){
                    /* Servicios ABCDIN no disponbles */
                    $(".infoVencimientos").html('<div class="col-sm-12 col-md-12">\
                        <div class="box">\
                            <p><span>Sin próximos vencimientos</span></p>\
                        </div>');
                    return;
                }
                if(vencimientos.datos){
                    var datosdeflujos = vencimientos.datos.DatosFlujo;
                    var html = '';
                    $.each(datosdeflujos, function(id, datosdeflujo) {
                        html += 
                        '<div class="col-sm-12 col-md-3">\
                            <div class="box">\
                                <h3>'+datosdeflujo.FechaVctoFlujo+'</h3>\
                                <p><span>$'+ (datosdeflujo.EstadoNuevoFlujo== '' ? '0': datosdeflujo.EstadoNuevoFlujo) +'</span></p>\
                            </div>\
                        </div>';
                    });
                    /* html += '<p class="legal">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>'; */
                    $('.infoVencimientos').html(html);
                    return; 
                }
                
            },'error': function(vencimientos) {
            
                /* Servicios ABCDIN no disponbles */
                $(".infoVencimientos").html('<div class="col-sm-12 col-md-12">\
                    <div class="box">\
                        <p><span>'+servicionodisponible+ '</span></p>\
                    </div>');
                return;
            },
            'beforeSend':function(){
                $(".infoVencimientos").html('<i class="fas fa-circle-notch fa-spin fa-3x" style="margin-left: 50%; margin-top: 10%; margin-bottom: 10%;"></i>');
            }
        });
		
	}
	return {
		init:function(){
			init();
		},
        vencimientos:vencimientos
	}
	
}();

$(document).ready(function(){
	AppVencimientos.init();
});