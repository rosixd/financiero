var AppEstadoCuenta = function(){

    var baseurl, urlestadocuenta;

    var nombre_archivo  = '';
    var valorT          = $("#valorT").val();
    var valorR          = $("#valorR").val();
    var valorV          = $("#valorV").val();
    var valorhaydetalle = $("#valorhaydetalle").val();
    var urlDocPdf       = '/api/resource/estado_cuenta';
    var urlTicket       = '';

	var init = function(){

        $("#FormControlSelect2").change( function(){
            nombre_archivo = $.trim( $(this).val() );

            if( nombre_archivo != '0' && /([a-zA-Z]|\d)+/i.test( nombre_archivo ) ){
                return fnCargaEstadoIframe( nombre_archivo );
            }

            return ;
            //verestadoCuenta();
        });
        
        //Evento para el boton de imprimir
        $("body").on("click", "#btnImprimirPdf", function( e ){
            e.preventDefault();

            if( $.trim( $("#ver").attr("src") ) != '' ){
                document.getElementById("ver").focus();
                document.getElementById("ver").contentWindow.window.print();
            }

            return ;
        });

        //Evento para el boton de descarga
        $("body").on("click", "#botondescargar", function( e ){
            
            if( $.trim( $("#botondescargar").attr("href") ) == '' ){
                e.preventDefault();
                return false;    
            }

            return true;
        });

        //activando botones de 'pagina anterior' y 'pagina siguiente'
        AppEstadoCuenta.fnPrevNext();
        if (!AppEstadoCuenta.urlTicket.length) {
            return AppEstadoCuenta.estadoCuenta();
        } else {
            if( $.trim( $("#FormControlSelect2").val() ) != '' ){
                $("#FormControlSelect2").change();
            }
        }
        
        /*$("#botondescargar").click( function(){
            descargaestadoCuenta();
        });*/
	};

    var fnPrevNext = function(){

        $("body").on("click", "#btnEstadoCuentaPrev", function( e ){
            e.preventDefault();
            var objImg  = $("#imgPdf img.d-block");
            var objPrev = objImg.prev();

            $("#btnEstadoCuentaPrev")
                .parent()
                .addClass("active")
                .removeClass("disabled");

            if( objPrev.is("img") ){
                objImg.removeClass("d-block")
                    .addClass("d-none");
                
                objPrev.removeClass("d-none")
                    .addClass("d-block");
            }

                objImg  = $("#imgPdf img.d-block");
            var objNext = objImg.next();
                objPrev = objImg.prev();

            if( !objPrev.is("img") ){
                $("#btnEstadoCuentaPrev")
                    .parent()
                    .addClass("disabled")
                    .removeClass("active");
            }            

            if( objNext.is("img") ){
                $("#btnEstadoCuentaNext")
                    .parent()
                    .addClass("active")
                    .removeClass("disabled");
            }

            return ;
        });

        return $("body").on("click", "#btnEstadoCuentaNext", function( e ){
            e.preventDefault();
            var objImg  = $("#imgPdf img.d-block");
            var objNext = objImg.next();

            $("#btnEstadoCuentaNext")
                .parent()
                .addClass("active")
                .removeClass("disabled");

            if( objNext.is("img") ){
                objImg.removeClass("d-block")
                    .addClass("d-none");
                
                objNext.removeClass("d-none")
                    .addClass("d-block");
            }

                objImg  = $("#imgPdf img.d-block");
                objNext = objImg.next();
            var objPrev = objImg.prev();

            if( objPrev.is("img") ){
                $("#btnEstadoCuentaPrev")
                    .parent()
                    .addClass("active")
                    .removeClass("disabled");
            }            

            if( !objNext.is("img") ){
                $("#btnEstadoCuentaNext")
                    .parent()
                    .addClass("disabled")
                    .removeClass("active");
            }

            return ;
        });
    };

	var estadoCuenta = function(){
        return $.ajax({
            'url'    : AppEstadoCuenta.urlestadocuenta,
            'type'   : 'GET',
            'success': function(estado_de_cuenta) {
                
                //Capturando la url de estado de cuenta
                if( estado_de_cuenta.url.length ){
                    AppEstadoCuenta.urlTicket = estado_de_cuenta.url;
                    if( $.trim( $("#FormControlSelect2").val() ) != '' ){
                        $("#FormControlSelect2").change();
                    }
                }
              
                if(estado_de_cuenta.error){
                    $(".msg-notifi").removeClass("d-none");
                    return;
                }  
            } 
        });		
    }
    
    var fnCargaEstadoIframe = function( doc ){
        var l = activaLoading( false , $("#imgPdf"));
        $("#botondescargar")
            .removeAttr("href")
            .removeAttr("download");

        $("#ver").removeAttr("src");
        
        return $.ajax({
            url     : '/api/resource/estado_cuenta',
            type    : "get",
            dataType: "json",
            data    : {
                t  : AppEstadoCuenta.urlTicket,
                doc: doc
            }
        })
            .done(function( response ){
                $("#botondescargar")
                    .attr("target", "_blank")
                    .attr("href", response.url)
                    .attr("download", response.url);

                $("#ver").attr("src", response.url);

                $("#imgPdf").html('<iframe src="' + response.url + '" style="width: 100%; height: 800px;"></iframe>');
                
                return;

                $("#botondescargar")
                    .removeAttr("href")
                    .removeAttr("download");

                $("#ver").removeAttr("src");

                if( response.archivos.length ){
                    $("#imgPdf").html("");
                    $("#botondescargar")
                        .attr("href", response.urlDoc )
                        .attr("download", response.doc );

                    $("#ver").attr("src", response.urlDoc );
                    $("#btnEstadoCuentaNext")
                        .parent()
                        .addClass("active")
                        .removeClass("disabled")

                    var claseActivo = 'd-block';
                    return $.each( response.archivos, function( indice, imagen ){
                        $("#imgPdf").append('<img src="' + imagen + '" class="img-fluid ' + claseActivo + '">');
                        claseActivo = 'd-none';
                    });
                }

                return ;
            });
    }

	var descargaestadoCuenta = function(){

        var url = $("#url").val().split('/');
        url.splice(-1);
        url = url.join('/');

        if ( valorV == "No") {
            //$("#Download_frame")[0].src = url + "/DocDownload.aspx?Nombre=" + nombre_archivo;
        }else{

            $.ajax({
                'url' : url + '/XMLHttpRequest.aspx',
                'data': {
                    'Funcion': 'VT',
                    'T'      : valorT
                },
                'type'   : 'GET',
                'success': function(res) {
                    //console.log(res);
                    valorR = res;
                },
                'error': function() {
                    valorR = 'false';
                    //alert("aqui va un modal");
                }, 
                'complete': function() {
                    $("#Download_frame")[0].src = url + "/DocDownload.aspx?Nombre=" + nombre_archivo;
                }
            });
        }
		
    }

    var verestadoCuenta = function(){

        var url = $("#url").val().split('/');
        url.splice(-1);
        url = url.join('/');
        $("#botondescargar").attr('href', url + "/DocDownload.aspx?Nombre=" + nombre_archivo);
        if ( valorV == "No") {
            $("#ver")[0].src = url + "/DocDownload.aspx?Nombre=" + nombre_archivo;
        }else{

            $.ajax({
                'url' : url + '/XMLHttpRequest.aspx',
                'data': {
                    'Funcion': 'VT',
                    'T'      : nombre_archivo
                },
                'type'     : 'GET',
                'xhrFields': {
                    'withCredentials': true
                },
                'crossDomain': true,
                //'dataType': 'jsonp',
                'success': function(res) {
                    //console.log(res);
                    valorR = res;
                    //window.location.href = url + "/DocDownload.aspx?Nombre=" + nombre_archivo;
                },
                'error': function() {
                    valorR = 'false';
                    //alert("aqui va un modal");
                }, 
                'complete': function() {
                    //$("#ver")[0].src = url + "/DocDownload.aspx?Nombre=" + nombre_archivo;
                }
            });
        }
    }

	return {
		init:function(){
			init();
        },
        verestadoCuenta     : verestadoCuenta,
        descargaestadoCuenta: descargaestadoCuenta,
        estadoCuenta        : estadoCuenta,
        fnCargaEstadoIframe : fnCargaEstadoIframe,
        fnPrevNext          : fnPrevNext
	}
	
}();

$(document).ready(function(){
	AppEstadoCuenta.init();
});