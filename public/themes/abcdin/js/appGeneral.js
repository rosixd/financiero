$(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        /*         ,statusCode: {
                    404: function() {
                      alert( "page not found" );
                    }
                  } */
    });

    /* blur on modal open, unblur on close */
    $('#myModal').on('show.bs.modal', function() {
        $('.modal-open').addClass('blur');
    });

    $('#myModal').on('hide.bs.modal', function() {
        $('.modal-open').removeClass('blur');
    });

    $('.pagoNoExitoso').on('show.bs.modal', function(e) {
        $.get(window.location.protocol + '//' + window.location.hostname + '/pago-no-realizado-modal', function(r) {
            $(this).html(r);
        });
    });

    $('body').on('shown.bs.modal', '#ModalFormularioQuieroMiTarjeta2', function(e) {
        $("body").addClass("modal-open");
        datalayerpush({
            'event': 'Patallaformulariotarjeta2',
            'category': 'click',
            'label': 'Patallaformulariotarjeta2'
        });
        return;
    });

    $('body').on('shown.bs.modal', '#ModalFormularioEquifax', function (e) {
        $("body").addClass("modal-open");
        datalayerpush({
            'event': 'Pantallaformulariotarjetaequifax3',
            'category': 'click',
            'label': 'Pantallaformulariotarjetaequifax3'
        });
        return;
    });

    $('#ModalRespuestaSolicituderror').on('show.bs.modal', function() {

        setTimeout(function() {
            location.reload();
        }, 9000);
    });

    /**
     * Levanto el  modal de quiero mi tarjeta
     * 
     * @author Rosana Hernandez
     */
    $("a[href='#ModalFormularioQuieroMiTarjeta']").on('click', function(e) {
        if (!$("#ModalFormularioQuieroMiTarjeta").length) {
            $.get(window.location.protocol + '//' + window.location.hostname + '/quiero-tarjeta-modal', function(r) {
                $('body').append(r);
                /* Seteo valores predeterminados del formulario paso 1 */
                $("#ModalFormularioQuieroMiTarjeta").on('show.bs.modal', function() {
                    RutFormularioTarjeta = '';
                    $(".msjrut").html('');
                    $('#ruttarjeta').prop('readonly', false);
                    $(".terminos a").addClass('disabled');
                    $('#btnformulario1').addClass('disabled');
                    $('input.requerido:not(#ruttarjeta)', '#formulario1').prop('disabled', true);
                    $('#numerodeserie', '#formulario1').prop('disabled', true);
                    /* $('input.requerido:not(#ruttarjeta, #numerodeserie)', '#formulario1').prop('disabled', true); */
                    $(".msjnumserie").html('');
                });
                $("#ModalFormularioQuieroMiTarjeta2").on('show.bs.modal', function() {
                    $('#btnformulario2').prop('disabled', false).html('ENVIAR');
                });
                $("#ModalFormularioQuieroMiTarjeta").modal('show');
                $('.soloNumerosrut, .soloNumeros, .soloLetras, .soloLetrasinespacios, .alfanumerico').off('keypress');
                $("#rut").off('blur');
                $("#email").off('blur');
                $("#confirmar").off('blur');
                $("#ruttarjeta").off('blur');
                $("form.formValida").off('submit');
                validarForms();
                validarRutFormularioTarjeta();
                validarFormularioTarjetaPaso1();
                /*MODAL EQUIFAX*/
                validarFormularioTarjetaEquifax();
                /*MODAL PASO2*/
                validarFormularioTarjetaPaso2();
                $('#telefonocelular').mask('00000000');
            });
        } else {
            $("#ModalFormularioQuieroMiTarjeta").modal('show');
        }
    });

    $("body").on("click", "#btnQuieroMiTarjetaPaso2", function() {

        $("#quieroMiTarjetaPaso1")
            .removeClass("show active");


        $("#quieroMiTarjetaPaso2")
            .addClass("show active");

    });

    $("li a", "footer").click(function() {
        var link = $(this),
            href = link.attr('href'),
            hash = window.location.hash,
            linkLocation = window.location.href.replace(window.location.protocol + '//' + window.location.hostname, ''),
            hashmenu = href.split('#').pop();

        linkLocation = linkLocation.replace(window.location.hash, '');

        if (linkLocation == '/nuestra-empresa') {
            var linkEmpresa = href.replace(/#.+$/, "");

            if (linkLocation == linkEmpresa) {
                $('#myTab3 a[href="' + '#' + hashmenu[1] + '"]').tab('show');
                $('html,body').animate({
                    scrollTop: $("#myTab3").offset().top
                }, 'slow');
            }
        }

    });

    if (window.location.hash) {
        var hash = window.location.hash;

        if (hash == '#glosario') {
            var link = $("a[href='#glosario']").click();
            $('html,body').animate({
                scrollTop: link.offset().top
            }, 'slow');
        } else if (hash == '#preguntasfrecuentes') {
            var link = $("a[href='#tarjeta']").click();
            $('html,body').animate({
                scrollTop: link.offset().top
            }, 'slow');
        } else if (hash == '#ModalFormularioQuieroMiTarjeta') {
            //console.log($("a[href='#ModalFormularioQuieroMiTarjeta']:first"));
            $("a[href='#ModalFormularioQuieroMiTarjeta']:first").click();
        } else if (hash == '#historia' || hash == '#tiendas' || hash == '#vision') {
            $('#myTab3 a[href="' + hash + '"]').tab('show');
        } else if ($(hash + '.modal').length) {
            setTimeout(function(){
                $(hash).modal('show');
            }, 1000);
        }
    }
});


/*REQ: 07 PAGA TU CUENTA*/
window.addEventListener('message', receiveMessage);

var cod_retorno = '';

$('#modalComprobante').on('hidden.bs.modal', function() { resultPago_ABCDIN_IFrame(cod_retorno) });

function receiveMessage(e) {

    if (e.data.event_id === 'fracaso') {

        $('#modalFracaso').modal('show');
    } else if (e.data.event_id === 'exito') {

        $('#rut_comprobante').text(e.data.mcomp.rut);
        $('#nombre_comprobante').text(e.data.mcomp.nombre);
        $('#n_tarjeta_comprobante').text('XXXX XXXX XXXX ' + e.data.mcomp.n_tar);
        $('#fecha_comprobante').text(e.data.mcomp.fecha);
        $('#monto_comprobante').text(e.data.mcomp.monto);
        $('#n_trx_comprobante').text(e.data.mcomp.trx);
        $('#fecha_exito').empty();
        $('#cuenta_exito').empty();
        $('#monto_exito').empty();
        $('#n_trx_exito').empty();
        $('#fecha_exito').append('<strong>' + e.data.mexito.fecha + '</strong><br>');
        $('#cuenta_exito').append('<strong>Cuenta: XXXX XXXX XXXX ' + e.data.mexito.n_tar + '</strong><br>');
        $('#monto_exito').append('<strong>Pagado: ' + e.data.mexito.monto + '</strong><br>');
        $('#n_trx_exito').append('<strong>Número de transacción: ' + e.data.mexito.n_comp + '</strong><br>');
        $('.rojo').text(e.data.mexito.correo);
        $('#modalExito').modal('show');
    }

    if (e.data.cod_retorno != null) {

        cod_retorno = e.data.cod_retorno;
    }
}

function resultPago_ABCDIN_IFrame(codigo) {

    if (codigo == '00') {
        location.reload();

    } else if (codigo == '01') {
        $('.pagoNoExitoso').modal('show');
    } else {

    }
}

function comprobante(_e) {
    _e.preventDefault();
    $('#modalExito').modal('hide');
    $('#modalComprobante').modal('show');
    /*     if($('#dvt_notificamail').val()!=''){         
                $.ajax({
                        url: 'https://soluciones.devetel.cl/pagosabcdinpreresponsivo/notificaMail' ,type: 'GET', crossDomain: true, dataType: 'jsonp', data:{id_trx: btoa($('#n_trx_comprobante').text()) , mail: btoa($('#dvt_notificamail').val())}
                });
        } */
}
/*FIN REQ.07*/

var urlApiResourcePdf = '';
var objResultadoApiPdf = {};

function notificaciones(tipo, msg) {

    var titulo = '';

    if (tipo === "error") {
        var boton = "danger"
    }

    if (tipo === "warning") {
        titulo = "Ocurrió un error"
    }

    swal({
        title: titulo,
        text: msg,
        type: tipo,
        showCancelButton: false,
        confirmButtonClass: "btn-" + boton + " btn-md",
        /* cancelButtonClass: "btn-sm", */
        confirmButtonText: "Ok",
        /* cancelButtonText: "Cancelar", */
        closeOnConfirm: false,
        /* closeOnCancel: false, */
        html: true,
        showLoaderOnConfirm: true
    });
}

function tiemposession(timesession, tiempo, tiempodealerta) {

    var TimeSession = timesession,
        lifeSession = tiempodealerta,
        TimeInSession = TimeSession,
        advertido = false;
    recargar = false;
    //alert( TimeSession , lifeSession);
    setInterval(function() {
        TimeInSession += 1;
        if (TimeInSession > (TimeSession + lifeSession) && !advertido) {
            advertido = true;
            //alert("Se acabo la session");
            //TimeInSession = 0;
            swal({
                title: "Expirar Sesión",
                text: "Su sesión esta próxima a expirar, ¿necesitas más tiempo?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                confirmButtonClass: "btn-danger btn-md",
                confirmButtonText: "SI",
                cancelButtonText: "NO",
                reverseButtons: false,
                showCancelButton: true,
            }, function(OK) {
                if (OK) {
                    $.ajax({
                        'url': urlrenovar,
                        'type': 'GET',
                        'success': function(r) {
                            //console.log(r);
                            if (r.error) {
                                location.reload();
                            }
                            TimeInSession = TimeSession = r.tiempo;
                            advertido = false;

                        }
                    });
                }
            });

        }

        if (TimeInSession > (TimeSession + tiempo) && !recargar) {
            recargar = true;
            location.reload();
        }
    }, 1000);
}


var initModales = function() {
    
    $('.modal').on('hidden.bs.modal', function(e) {
        if ($('form', this).length) {
            lista_errores = {};
            render_error();
            $(this).find(".campo_obligatorio").removeClass("campo_obligatorio");
            $(".terminos.campo_obligatorios").removeClass("campo_obligatorios");
            $('form', this).trigger("reset");
        }
    });

    return $(".modal").each(function() {
        var objModal = $(this);
        var htmlModal = objModal[0].outerHTML;

        $("body").append(htmlModal);
        objModal.remove();

        if ($('.levantarmodal').length) {
            $('.levantarmodal').modal();
        }
        return;
    });
}

var lista_errores = {};

function render_error() {
    $(".msjval").html('');

    $.each(lista_errores, function(index, val) {
        $(".msjval").append('<span class="rojo" style="font-size: 11px;">' + val + '</span><br>');
    });
};

/**
 * Funcion para validar el rut del formulario de quiero mi tarjeta
 * 
 * @author Rosana Hernandez
 */

var validarForms = function() {
    /* Formularios en general */
    $('input, select', 'form').each(function() {
        $(this).data('original', $(this).val());
    });

    $('input:not(.sinValidar), select', "#formulario1, #formulario2").blur(function(e) {
        var form = $(this).closest('form');
        var error = false;
        $(this).removeClass("campo_obligatorio");

        var objReq = $(this),
            id = objReq.attr("id"),
            padreObj = objReq.parent(),
            valor = $.trim(objReq.val());

        if (valor == '') {
            error = true;
            /*lista_errores[id] = 'El campo '+ id + ' es requerido.';*/
            objReq.addClass("campo_obligatorio");
        }

        render_error();
    });

    //var fechaact= moment()
    if (typeof(moment) != 'undefined') {
        var fecha18 = moment();
        var fecha100 = moment();
        fecha18.add(-18, 'year');
        fecha100.add(-100, 'year');

        //datepicker
        $('#fechadenacimiento').daterangepicker({
            "showDropdowns": true,
            "minDate": fecha100,
            "maxDate": fecha18,
            "startDate": fecha18,
            "endDate": fecha100,
            locale: {
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "separator": " / ",
                "format": "DD-MM-YYYY",
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "monthOfWeek": [
                    "Ene",
                    "Feb",
                    "Mar",
                    "Abr",
                    "May",
                    "Jun",
                    "Jul",
                    "Ago",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dic"
                ],
            },
            "singleDatePicker": true,
            "autoclose": true,
            "todayHighlight": true
        });

        //datepicker
        $('#fecha, #fecha2').daterangepicker({
            "showDropdowns": true,
            "minDate": fecha100,
            "endDate": fecha100,
            locale: {
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "separator": " / ",
                "format": "DD-MM-YYYY",
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "monthOfWeek": [
                    "Ene",
                    "Feb",
                    "Mar",
                    "Abr",
                    "May",
                    "Jun",
                    "Jul",
                    "Ago",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dic"
                ],
            },
            "singleDatePicker": true,
            "autoclose": true,
            "todayHighlight": true
        });
    }

    //solo numeros con k para rut
    $('.soloNumerosrut').on('keypress', function(e) {
        if (!/([\d|kK])+/.test(e.key)) {
            return false;
        }

        return;
    });

    //solo numeros
    $('.soloNumeros').on('keypress', function(e) {
        if (!/\d+/.test(e.key)) {
            return false;
        }

        return;
    });

    //solo fecha
    $('.soloFecha').on('keypress', function(e) {
        if (!/([\d|\/|\-])+/.test(e.key)) {
            return false;
        }

        return;
    });

    //solo letras con espacios
    $('.soloLetras').on('keypress', function(e) {
        if (!/([a-z]|\s)/i.test(e.key)) {
            return false;
        }

        return;
    });

    //solo letras
    $('.soloLetrasinespacios').on('keypress', function(e) {
        if (!/([a-z])/i.test(e.key)) {
            return false;
        }

        return;
    });

    //alfanumerico
    $('.alfanumerico').on('keypress', function(e) {
        if (!/[A-Za-z0-9]+/.test(e.key)) {
            return false;
        }

        return;
    });

    //alfanumerico con espacios
    $('.alfanumericoespacios').on('keypress', function(e) {
        if (!/[A-Za-z0-9|\s]+/.test(e.key)) {
            return false;
        }

        return;
    });

    //MOntos
    var monto = $('.monto');

    function formatNumber(n) {
        n = String(n).replace(/\D/g, "");
        return n === '' ? n : Number(n).toLocaleString();
    }

    monto.on('keyup', function(e) {
        $(this).val(formatNumber($(this).val()));
    });

    //RUT
    $("#rut").blur(function() {

        var rut = $.trim($(this).val()),
            id = $(this).attr('id');

        delete lista_errores[id];
        $(this).removeClass("campo_obligatorio");

        if (!$.validateRut(rut)) {
            lista_errores[id] = '<i class="fas fa-info-circle"></i> RUT INVÁLIDO';
            $(this).addClass("campo_obligatorio");
            render_error();
        }

        render_error();
    });

    $("#email").blur(function() {
        var t = $(this),
            id = t.attr('id'),
            email = $.trim(t.val()),
            emailRegex = /^[a-zA-Z0-9\._%\+-]{1,40}@[a-zA-Z0-9\.-]{4,40}$/i;
        emailRegex2 = /^[a-zA-Z0-9\._%\+-]{1,40}@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,6}$/i;

        if (emailRegex.test(email) && emailRegex2.test(email)) {
            delete lista_errores[id];
            t.removeClass("campo_obligatorio");
        } else {
            lista_errores[id] = '<i class="fas fa-info-circle"></i> EL EMAIL ES INVÁLIDO';
            t.addClass("campo_obligatorio");
        }

        render_error();
    });

    $("#emails").blur(function() {
        var t = $(this),
            id = t.attr('id'),
            email = $.trim(t.val()),
            //emailRegex  = /^[a-zA-Z0-9\._%\+-]{1,40}@[a-zA-Z0-9\.-]{4,40}$/i;
            emailRegex2 = /^[a-zA-Z0-9\._%\+-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,6}$/i;

        if (!$(this).is(':visible')) {
            return true;
        }
        if (email.length <= 64 && emailRegex2.test(email)) {
            delete lista_errores[id];
            t.removeClass("campo_obligatorio");
        } else {
            lista_errores[id] = '<i class="fas fa-info-circle"></i> EMAIL INVÁLIDO';
            t.addClass("campo_obligatorio");
        }

        render_error();
    });

    $("#confirmar").blur(function() {
        var t = $(this),
            id = t.attr('id'),
            email = $.trim(t.val()),
            error = false,
            emailRegex = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/i;

        if (!$(this).is(':visible')) {
            return true;
        }
        if ($("#email").val() != email) {
            lista_errores['confirmar_mail'] = '<i class="fas fa-info-circle"></i> LOS CORREOS NO COINCIDEN';
            t.addClass("campo_obligatorio");
            $("#email").addClass("campo_obligatorio").blur();
            error = true;
        } else {
            delete lista_errores['confirmar_mail'];
            t.removeClass("campo_obligatorio");
            $("#email").removeClass("campo_obligatorio").blur();
        }

        if ($("#emails").val() != email) {
            lista_errores['confirmar_mail'] = '<i class="fas fa-info-circle"></i> LOS CORREOS NO COINCIDEN';
            t.addClass("campo_obligatorio");
            $("#emails").addClass("campo_obligatorio").blur();
            error = true;
        } else {
            delete lista_errores['confirmar_mail'];
            t.removeClass("campo_obligatorio");
            $("#emails").removeClass("campo_obligatorio").blur();
        }

        if (emailRegex.test(email)) {
            delete lista_errores[id];
            if (!error) {
                t.removeClass("campo_obligatorio");
            }
        } else {
            lista_errores[id] = '<i class="fas fa-info-circle"></i> EMAIL DE CONFIRMACIÓN INVÁLIDO';
            t.addClass("campo_obligatorio");
        }

        render_error();
    });

    $('[name=tienehijos]').click(function(e) {

        if ($("[name='tienehijos']:checked", "#formulario2").val() == 'no') {

            $("#nrohijos").val(0).attr('disabled', 'disabled');

        } else if ($("[name='tienehijos']:checked", "#formulario2").val() == 'si') {
            $("#nrohijos").val(1);
            $("#nrohijos option[value='0']").attr('disabled', 'disabled');
            $("#nrohijos").removeAttr('disabled');
        }
    });

    $('input[name=automovil]', "#formulario2").click(function(e) {
        var valor = $('[name=automovil]:checked', "#formulario2").val();

        if (valor == 'si') {
            $('#patente').removeClass('d-none');
            $("#nropatente")
                .prop('disabled', false)
                .addClass('requerido');
        }

        if (valor == 'no') {
            $('#patente').addClass('d-none');
            $("#nropatente")
                .prop('disabled', true)
                .removeClass('requerido')
                .val('');
        }

    });

    $("#formulario2").submit(function(e) {
        var error = validarForm(this);
        /* formulario quiero tarjeta */

        if (!$("[name='tienehijos']:checked", this).val()) {
            $("[name='tienehijos']", this).closest('.boxhijos').addClass("campo_obligatorio");
        }

        if (!$("[name='automovil']:checked", this).val()) {
            $("[name='automovil']", this).closest('.boxhijos').addClass("campo_obligatorio");
        }
        delete lista_errores['requerid'];
        if (Object.keys(lista_errores).length > 0 || error) {
            lista_errores['requerid'] = '<i class="fas fa-info-circle"></i> UPS! AUN QUEDAN PREGUNTAS POR RESPONDER';

            e.stopImmediatePropagation();
            render_error();
            return false;
        }
        render_error();
        return false;
    });

    $("#formularioequifax").submit(function(e) {
        var error = validarForm(this);

        $(".pregunta", "#formularioequifax").each(function(e) {
            if (!$('input:checked', this).val()) {
                error = true;
            }
        });

        /* formulario quiero tarjeta */
        delete lista_errores['requerid'];
        if (Object.keys(lista_errores).length > 0 || error) {
            lista_errores['requerid'] = '<i class="fas fa-info-circle"></i> UPS! AUN QUEDAN PREGUNTAS POR RESPONDER';

            e.stopImmediatePropagation();
            render_error();
            return false;
        }
        render_error();
        return false;
    });

    $("form.formValida").submit(function(e) {
        var error = validarForm(this);
        /* formulario quiero tarjeta */

        if (Object.keys(lista_errores).length > 0 || error) {
            e.stopImmediatePropagation();
            render_error();
            return false;
        }
        render_error();

    });
}

var RutFormularioTarjeta = '';
var FormularioEquifax = '';


var validarRutFormularioTarjeta = function() {

    $('#ruttarjeta').prop('readonly', false);
    $(".terminos a").addClass('disabled');
    $('input.requerido:not(#ruttarjeta)', '#formulario1').prop('disabled', true);
    /* $('input.requerido:not(#ruttarjeta, #numerodeserie)', '#formulario1').prop('disabled', true); */

    $("#btnformulario1").click(function(event) { 
        if ($(this).is('.disabled')) {
            $("#ruttarjeta").blur();
            return false;
        }
    });
    
    $("#ruttarjeta").on('blur keypress focus', function( event ) {

        if (event.key != "Enter" && event.type == 'keypress') { 
            return;
        }

        $(".msjrut").html('');
        /* $('input.requerido:not(#ruttarjeta, #numerodeserie)', '#formulario1').prop('readonly', true); */
        if (!validaRut()) {
            return true;
        }

        var rut = $.trim($(this).val());

        if (RutFormularioTarjeta == rut) {
            return true;
        }
        RutFormularioTarjeta = rut;

        $.ajax({
            'url': validaruttarjeta,
            'data': {
                'rut': rut
            },
            'type': 'POST',
            'success': function(respuesta) {

                if (respuesta.error == false && (respuesta.datos.CodigoCliente != 0 || respuesta.datos.CodigoCliente != '00')) {
                    var respuestaMayuscula = respuesta.msg.toUpperCase();
                    $(".msjrut").html('<i class="fas fa-info-circle"></i>&nbsp;' + respuestaMayuscula);
                    if (respuesta.datos.CodigoCliente == 1 || respuesta.datos.CodigoCliente == 3 || respuesta.datos.CodigoCliente == 5) {
                        datalayerpush({
                            'event': 'yaerescliente',
                            'category': 'click',
                            'label': 'yaerescliente'
                        });
                    }
                }
                if( respuesta.error && (respuesta.datos.CodigoCliente == 02 || respuesta.datos.CodigoCliente == 04) ){
                    var respuestaMayuscula = respuesta.msg.toUpperCase();
                    $(".msjrut").html('<i class="fas fa-info-circle"></i>&nbsp;' + respuestaMayuscula);
                }

                if (respuesta.error == false && respuesta.datos.CodigoCliente == 0 || respuesta.datos.CodigoCliente == '00') {
                    $('#btnformulario1').removeClass('disabled');
                    $('#ruttarjeta').prop('readonly', true);
                    $('#numerodeserie', '#formulario1').prop('disabled', false);
                    $('input.requerido:not(#ruttarjeta)', '#formulario1').prop('disabled', false);
                    /* $('input.requerido:not(#ruttarjeta, #numerodeserie)', '#formulario1').prop('disabled', false); */
                    $("input[name='token']").val(respuesta.datos.token);
                    $("input[name='id']").val(respuesta.datos.idcliente);
                    $(".terminos .disabled").removeClass('disabled');

                }
            }
        });
    });
}

var validarFormularioTarjetaPaso1 = function() {
    $('#btnformulario1').addClass('disabled');

    $('#formulario1').off('submit').submit(function(e) {
        var error = validarForm(this);
        var numerodeserie = $('#numerodeserie').val();

        $(".terminos.campo_obligatorios").removeClass("campo_obligatorios");
        /* formulario quiero tarjeta */

        if (!$("[name='terminos']:checked", this).val()) {
            $(".terminos", this).addClass("campo_obligatorios");
            lista_errores['terminos'] = '<i class="fas fa-info-circle"></i> UPS! DEBES ACEPTAR LAS CONDICIONES';
            error = true;
        } else {
            $(".terminos", this).removeClass("campo_obligatorios");
            delete lista_errores['terminos'];
        }

        delete lista_errores['requerid'];

        if (Object.keys(lista_errores).length > 0 || error) {
            lista_errores['requerid'] = '<i class="fas fa-info-circle"></i> UPS! AUN QUEDAN PREGUNTAS POR RESPONDER';

            e.stopImmediatePropagation();
            render_error();
            return false;
        }

        if (numerodeserie == '' && $(".msjnumserie").html() == '') {

            $(".msjnumserie").html('<i class="fas fa-info-circle"></i>&nbsp;' + transnumserietarjeta.toUpperCase());
            e.stopImmediatePropagation();
            render_error();
            datalayerpush({
                'event': 'sinnumerodeserie',
                'category': 'click',
                'label': 'sinnumerodeserie'
            });
            return false;
        }

        render_error();
        $.ajax({
            'url': guardarformulariotarjeta,
            'method': 'POST',
            'data': $('#formulario1').serialize(),
            'success': function(respuesta) {
                
                if (respuesta.error == false && respuesta.equifax) {
                    $('#ModalFormularioQuieroMiTarjeta').modal('hide');
                    $('#ModalFormularioEquifax').modal();

                    $('.pregunta', '.preguntasequifax').remove();

                    html = '';
                    $.each(respuesta.data.preguntas, function(index, pregunta) {
                        html += '<div class="pregunta">';
                        html += '<h3>' + pregunta.QuestionText + '&nbsp;<span class="rojo requeridospre">*</span></h3>';
                        //console.log(pregunta);
                        $.each(pregunta.AnswerChoice, function(indexr, respuesta) {
                            //console.log(pregunta.questionId, pregunta['@attributes'], respuesta, respuesta._);
                            html += '<div class="custom-control custom-radio">';
                            html += '   <input type="radio" class="custom-control-input requerido" id="questionId_' + pregunta.questionId + '_' + indexr + '" name="questionId[' + pregunta.questionId + ']" value="' + respuesta.answerId + '">';
                            html += '   <label class="custom-control-label" for="questionId_' + pregunta.questionId + '_' + indexr + '">' + respuesta._ + '</label>';
                            html += '</div>';
                        });
                        html += '</div>';
                    });

                    $('.preguntasequifax').prepend(html);

                }

                if (respuesta.error == false && respuesta.equifax == false) {
                    $('#ModalFormularioQuieroMiTarjeta').modal('hide');
                    $('#ModalFormularioQuieroMiTarjeta2').modal('show');

                    $("input[name='token']").val(respuesta.data.token);
                    $("input[name='id']").val(respuesta.data.id);
                }
            },
            'beforeSend': function() {
                var icon = $(" <i> ").addClass('fa fa-spinner fa-spin fa-fw');
                $('#btnformulario1').addClass('disabled').append(icon);
            },
            'complete': function() {
                $('#btnformulario1').removeClass('disabled').find('i').remove();
            },
            'error': function(respuesta) {
                if (respuesta.status == 422) {
                    var json = respuesta.responseJSON;
                    lista_errores = ['UPS! AUN QUEDAN PREGUNTAS POR RESPONDER'];
                    $.each(json, function(index, value){
                        $("#" + index).addClass('campo_obligatorio');
                    });
                    render_error();
                }
                //console.log(respuesta);
                return;
            }
        });

        return false;
    });
}

var validarFormularioTarjetaEquifax = function() {

    
    $('#autorizo').click(function(e) {
        $('#autorizacion').val('autorizo');
        $('#autorizo').addClass('btn-primary').removeClass('btn-outline-primary');
        $('#noautorizo').addClass('btn-outline-primary').removeClass('btn-primary');
    });
    
    $('#noautorizo').click(function(e) {
        $('#autorizacion').val('noautorizo');
        $('#noautorizo').addClass('btn-primary').removeClass('btn-outline-primary');
        $('#autorizo').addClass('btn-outline-primary').removeClass('btn-primary');
    });
    
    $('#noautorizo').click();

    $('#btnrechazarequifax').click(function(e) {
        $('#ModalFormularioEquifax').modal('hide');
        $('#ModalFormularioQuieroMiTarjeta2').modal('show');
    });

    $('#formularioequifax').submit(function(e) {
        var error = validarForm(this);

        if (Object.keys(lista_errores).length > 0 || error) {
            e.stopImmediatePropagation();
            render_error();
            return false;
        }

        FormularioEquifax = $('#formularioequifax').serialize();
        $('#ModalFormularioQuieroMiTarjeta2').modal('show');
        $('#ModalFormularioEquifax').modal('hide');
        return false;

        /*$.ajax({
            'url': guardarformulariotarjeta,
            'method': 'POST',
            'data': $('#formularioequifax').serialize(),
            'success': function(respuesta) {
                
                console.log( respuesta );
            },
            'error': function(respuesta) {
                console.log(respuesta);
                return;
            }
        }); */

    });
}

var validarFormularioTarjetaPaso2 = function() {

    $("#region").change(function() {
        var region_id = $(this).val();
        var comunasFiltradas = comunas.filter(function(comuna) {
            return comuna.ABC11_id == region_id;
        });

        $("#comuna").html("");
        for (var i = 0; i < comunasFiltradas.length; i++) {
            var comuna = comunasFiltradas[i];

            $("#comuna").append('<option value="' + comuna.ABC13_codigo + '">' + comuna.ABC13_nombre + '</option>');
        }
    });

    $('#formulario2').submit(function(e) {

        $('#btnformulario2').prop('disabled', true).html('PROCESANDO <i class="fa fa-spinner fa-spin fa-fw"></i>');


        $.ajax({
            'url': guardarformulariotarjeta,
            'method': 'POST',
            'data': $('#formulario2').serialize() + '&' + FormularioEquifax,
            'success': function(respuesta) {
                $("#ModalFormularioQuieroMiTarjeta2").modal('hide');
                $('#btnformulario2').prop('disabled', false);

                if (respuesta.error == false && (respuesta.estatus_cliente == '0' || respuesta.estatus_cliente == '00')) {
                    $("#ModalExitoTarjeta").modal('show');
                    datalayerpush({
                        'event': 'Aprobadosformulario',
                        'category': 'click',
                        'label': 'Aprobadosformulario'
                    });
                    /* $('.mensajeExito').text(respuesta.msg); */
                }
                if (respuesta.error && respuesta.estatus_cliente == '99') {
                    $("#ModalRespuestaSolicituderror").modal('show');
                    datalayerpush({
                        'event': 'Rechazadosformulario',
                        'category': 'click',
                        'label': 'Rechazadosformulario'
                    });
                    /* $('.mensajeExito').text(respuesta.msg); */
                }
                if (!respuesta.error && (respuesta.estatus_cliente == '8' || respuesta.estatus_cliente == '08')) {
                    $("#ModalErrorTarjeta").modal('show');
                    datalayerpush({
                        'event': 'Rechazadosformulario',
                        'category': 'click',
                        'label': 'Rechazadosformulario'
                    });
                    /* $('.mensajeExito').text(respuesta.msg); */
                }
            },
            'error': function(respuesta) {
                $("#ModalFormularioQuieroMiTarjeta2").modal('hide');
                $("#ModalRespuestaSolicituderror").modal('show');
                datalayerpush({
                    'event': 'Rechazadosformulario',
                    'category': 'click',
                    'label': 'Rechazadosformulario'
                });
                /* $('.mensajeExito').text(respuesta.msg); */
            }
        });

        return false;
    });
}

var validaRut = function() {

    var t = $("#ruttarjeta"),
        rut = $.trim(t.val()),
        id = t.attr('id');

    delete lista_errores[id];
    t.removeClass("campo_obligatorio");

    if (!$.validateRut(rut)) {
        lista_errores[id] = '<i class="fas fa-info-circle"></i> EL RUT NO ES VÁLIDO';
        t.addClass("campo_obligatorio");
        render_error();
        return false;
    }
    rut = $.formatRut(rut, false);
    t.val(rut);

    if (rut.length < 3) {
        lista_errores[id] = '<i class="fas fa-info-circle"></i> EL RUT NO ES VÁLIDO';
        t.addClass("campo_obligatorio");
        render_error();
        return false;
    }

    render_error();
    return true;
}

var formulariosEnvio = function() {
    $('.formulario').submit(function(e) {

        var metodo = 'GET';
        var id = $.trim( $( this ).attr("id") ).toLowerCase();
        var objForm = $(this);

        if( /formulariocontacto$/.test( id ) && /formulario\/guardar$/i.test( urlformulario ) ){
            metodo = 'POST'
        }
        
        e.preventDefault();
        //console.log( data );
        $.ajax({
            'url': urlformulario,
            'data': $(this).serialize(),
            'type': metodo,
            dataType: 'json'
        }).done(function( response ){
            $(".modal").modal('hide');
            if (response.s == 's') {
                $("#ModalRespuestaSolicitud").modal('show');
            } else if (r.s == 'n') {
                $("#ModalRespuestaSolicituderror").modal('show');
            }
            
            return ;
        }).fail(function( response ){
            
            if( (typeof response.responseJSON) == 'object'){
                var msgError = '';

                $.each( response.responseJSON, function( indice, campo ){
                    
                    $.each( campo , function( indice , error ){
                        msgError += '<span class="rojo" style="font-size: 11px;">' + 
                                '<i class="fas fa-info-circle"></i>' +
                                error.toUpperCase() +
                            '</span><br>';
                    })
                });
                
                objForm.find(".msjval").           
                    append( msgError );
            }
        });

    });
};

/**
 * Funcion global para activar un loading
 * 
 * @author Dickson Morales
 * @param Boolean enModal Al ser true levanta 
 *                  el modal myModal y le agrega el loading
 * 
 * @param Object obj Objeto jquery que apunta a donde sera
 *                  agregado el loading
 * 
 * @param Object option Objecto que contiene textos y opciones
 *      title: Titulo a agregar
 *      footer: Footer
 */
function activaLoading(enModal, obj, option) {
    var i = '';
    var objLoading;

    if (enModal) {

        i += '<div class="text-center">' +
            '<i class="fas fa-circle-notch fa-spin fa-3x"></i>' +
            '</div>';

        if (typeof option == 'object') {
            if (option.title.length) {
                $("#myModal .modal-header").html('<h5>' + option.title + '</h5>');
            }

            if (option.footer.length) {
                $("#myModal .modal-footer").html(option.footer);
            }
        }

        $("#myModal .modal-body").html(i);
        $("#myModal").modal();
        objLoading = $("#myModal");
    } else if (typeof obj == 'object') {
        i += '<div class="text-center">';

        if ((typeof option == 'object') && option.title.length) {
            i += '<h5>' + option.title + '</h5>';
        }

        i += '<i class="fas fa-circle-notch fa-spin fa-3x"></i>';

        if ((typeof option == 'object') && option.footer.length) {
            i += option.footer;
        }

        i += '</div>';

        obj.html(i);
        objLoading = obj;
    }

    return objLoading;
}

/**
 * funcionalidad para los botones de pdf
 *
 *   @autor Dickson Morales
 */

var fnPdfLightBox = function() {
    if ($(".clickLinkPdfLightBox").length) {
        return $("body").on("click", ".clickLinkPdfLightBox", function(e) {

            if ($.trim($(this).data("target")) != '') {
                e.preventDefault();
                var target = $.trim($(this).data("target"));

                var objModal = $("#garantiaModalAux");

                objModal.find(".modal-body")
                    .html("<div class='capaPdfModalGarantia'></div>");

                var objModalBody = objModal.find(".modal-body div.capaPdfModalGarantia");

                activaLoading(false, objModalBody, false);

                objModal.modal("show");
                if ((typeof objResultadoApiPdf[target]) != 'undefined') {
                    return objModalBody.append(objResultadoApiPdf[target]);
                }

                return $.ajax({
                    url: urlApiResourcePdf,
                    data: {
                        pdf: $("#" + target).attr("href")
                    },
                    dataType: "json",
                    type: 'post'
                }).done(function(r) {

                    if (r.error) {

                        objModalBody
                            .html(r.message);
                    }

                    if ((typeof r.archivos != 'undefined') && r.archivos.length) {
                        objResultadoApiPdf[target] = '';
                        $.each(r.archivos, function(indice, img) {
                            objResultadoApiPdf[target] += '<img src="' + img + '" class="">';
                        });

                        objModalBody.html('<div class="text-center capaOverFLow">' + objResultadoApiPdf[target] + '</div>');
                    }

                    return objModal.modal("show");
                });
            }

            return;
        });
    }

    return;
};

function fnPdfModal() {
    $(".pdflighbox").on("click", function(e) {
        var $this = $(this),
            src = $this.attr('href'),
            title = $this.attr('title'),
            height = $(window).height() - 140;

        var modal = $('#pdflighbox');

        title = typeof(title) === 'undefined' ? '' : title;

        if (modal.length === 0) {
            modal = $('\
            <div id="pdflighbox" class="modal" tabindex="-1" role="dialog">\
                <div class="modal-dialog" role="document" style="max-width: 90%;">\
                    <div class="modal-content">\
                        <div class="modal-header">\
                            <h5 class="modal-title">' + title + '</h5>\
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                                <span aria-hidden="true">&times;</span>\
                            </button>\
                        </div>\
                        <div class="modal-body">\
                            <iframe src="' + src + '" style="width: 100%; height: ' + height + 'px"></iframe>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            ');
    
            $('body').append(modal);
        } else {
            modal.find('.modal-title').html(title);
            modal.find('iframe').attr('src', src).css('height', height);
        }

        modal.modal('show');

        return false;
    });
}


/******** MODALES *************/
function modalRequisitoTarjeta() {
    $(".requisitostarjeta").on('click', function() {
        $.get(window.location.protocol + '//' + window.location.hostname + '/requisitos-tarjeta-modal', function(r) {
            $("#ModalRequisitos").html(r);

            $('button.close, button.btnRojo', "#ModalRequisitos").click(function() {
                $("#ModalRequisitos").modal('hide');
            });
            $("#ModalRequisitos").modal('show');
        });
        return false;
    });
}

function modalBasesLegales() {
    $(".baseslegales").on('click', function() {
        $.get(window.location.protocol + '//' + window.location.hostname + '/bases-legales-modal', function(r) {
            $("#ModalBasesLegales").html(r);

            $('button.close, button.btnRojo', "#ModalBasesLegales").click(function() {
                $("#ModalBasesLegales").modal('hide');
            });
            $("#ModalBasesLegales").modal('show');
        });
        return false;
    });
}

function modalVerGanadores() {
    $(".verganadores").on('click', function() {
        $.get(window.location.protocol + '//' + window.location.hostname + '/ver-ganadores-modal', function(r) {
            $("#ModalVerGanadores").html(r);

            $('button.close, button.btnRojo', "#ModalVerGanadores").click(function() {
                $("#ModalVerGanadores").modal('hide');
            });
            $("#ModalVerGanadores").modal('show');
        });
        return false;
    });
}

function modalSolicitarTarjetaMenu() {

    $(".solicitartarjeta").attr('href', '/tarjeta-abcvisa#ModalFormularioQuieroMiTarjeta');
    
    var uri = window.location.href.replace(window.location.protocol + '//' + window.location.host + '/', '').replace(window.location.hash, '');
    if( uri == 'tarjeta-abcvisa'){
        $(".solicitartarjeta").on('click', function() {
            $("a[href='#ModalFormularioQuieroMiTarjeta']:first").click();
            return false;

        });
    }
}

function modalGlosarioMenu() {
    $(".glosario").attr('href', '/asesoria-financiera#glosario');
}

/* function responsive() {

    var $inicio = typeof(window.$inicio) === 'undefined' ? false : window.$inicio;

    if ($inicio) {
        return false;
    }

    if ($(".navTitular").length) {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".navTitular").offset().top
        }, 500);
    }
} */

function modalPreguntasFrecuentesMenu() {
    $(".preguntasfrecuentes").attr('href', '/asesoria-financiera#preguntasfrecuentes');
}

function responsive() {

    var $inicio = typeof(window.$inicio) === 'undefined' ? false : window.$inicio;

    if ($inicio) {
        return false;
    }

    if ($(".navTitular").length) {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".navTitular").offset().top
        }, 500);
    }

    if ($(window).width() < 580) {
        $("#myTab2").click(function() {
            $('html, body').animate({
                scrollTop: $("#myTabContent2").offset().top + (-20)
            }, 500, 'swing');
        });
    }

    $(".nav-link", ".BarraSubmenu .container").click(function() {
        if (window.innerWidth > 580) {
            return true;
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#tabsJustifiedContent").offset().top
        }, 500);
    });

    $(".urgenciaa, .dentala, .hogara, .mujera, .vidaa, .clinicaa", ".boxSeguros .Asistencias").click(function() {
        if (window.innerWidth > 580) {
            return true;
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#tabsJustifiedContent").offset().top
        }, 500);
    });

    $("li a", ".preguntasFrec #myTab").click(function() {
        if (window.innerWidth > 580) {
            return true;
        }
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".tab-content").offset().top
        }, 500);
    });

    $("li a", ".bannersTabs #myTab").click(function() {
        if (window.innerWidth > 580) {
            return true;
        }
        if ($(".Contenido").length > 0) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".Contenido:visible").offset().top
            }, 500);
        } else {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".tab-content").offset().top
            }, 500);
        }
    });

}

function menuPrivado() {
    var $inicio = typeof(window.$inicio) == 'undefined' ? true : window.$inicio;

    if (!$inicio) {
        $('html,body').animate({
            scrollTop: $(".menuprivado").offset().top
        }, 'slow');
    }
}

function scrollTop() {
    $("li a", ".Garantia").click(function() {
        $('html,body').animate({
            scrollTop: $("#myTab").offset().top
        }, 'slow');
    });
}

function nuevasSecciones() {

    $('#norte1').click(function() {
        $('#my_image').attr('src', '/assets/media/img-mapanorte.jpg');
    });
    $('#centro1').click(function() {
        $('#my_image').attr('src', '/assets/media/img-mapacentro.jpg');
    });
    $('#sur1').click(function() {
        $('#my_image').attr('src', '/assets/media/img-mapasur.jpg');
    });
}

/* ********************************** */

$(document).ready(function() {

    initModales();
    $('body').on('hidden.bs.modal', '.modal', function(e) {

        if ($('form', this).length) {
            lista_errores = {};
            render_error();
            $(this).find(".campo_obligatorio").removeClass("campo_obligatorio");
            $(".terminos.campo_obligatorios").removeClass("campo_obligatorios");
            $('form', this)[0].reset();
        }
    });

    validarRutFormularioTarjeta();
    validarForms();
    validarFormularioTarjetaPaso1();
    formulariosEnvio();
    fnPdfLightBox();
    fnPdfModal();
    $('#modalExito').on('click', '#ver_comprobante', comprobante);
    modalRequisitoTarjeta();
    modalBasesLegales();
    modalVerGanadores();
    modalSolicitarTarjetaMenu();
    modalGlosarioMenu();
    modalPreguntasFrecuentesMenu();
    responsive();
    menuPrivado();
    scrollTop();
    nuevasSecciones();
    
    //$(".modal").on('hidden.bs.modal', function(){
    $('body').on('hidden.bs.modal', ".modal", function() {
        if ($(".modal.show", 'body').length > 0) {
            $('body').addClass('modal-open');
        }
    });
});

function validarForm(form) {
    var error = false;
    $(form).find(".campo_obligatorio").removeClass("campo_obligatorio");
    $(".terminos.campo_obligatorios").removeClass("campo_obligatorios");

    $("input, select", form).each(function() {
        $(this).blur();
    });

    $(form).find(".requerido").each(function() {
        var objReq = $(this),
            id = objReq.attr("id"),
            padreObj = objReq.parent(),
            valor = $.trim(objReq.val());

        if (valor == '') {
            error = true;
            /*lista_errores[id] = 'El campo '+ id + ' es requerido.';*/
            objReq.addClass("campo_obligatorio");
        }
    });


    return error;
}

function CargarCupos($url, $cupos) {
    $.ajax({
        'url': $url,
        'method': 'GET',
        'success': function(respuesta) {
            if(respuesta.error == true){
                
                return;  
            }
            $('#CuposUtilizados').html("").append('$ ' + respuesta.datos.CuposUtilizados);
            $('#CupoTotal').html("").append('$ ' + respuesta.datos.CupoTotal);
            $('#BarraPorcentaje').css('width', respuesta.datos.porcentaje + '%');
            $('#DisponibleTiendas').html("").append('$ ' + respuesta.datos.DisponibleTiendas);
            /* $('#DisponibleTiendas').html("").append('$ ' + respuesta.datos.DisponibleAvanceEnEfectivo); */
            $('#DisponibleAvanceEnEfectivo').html("").append('$ ' + respuesta.datos.DisponibleAvanceEnEfectivo);
            $('#OtrosComercios').html("").append('$ ' + respuesta.datos.DisponibleOtrosComercios.datos[0]);
            $('#disponibleotroscomercios p').html("").append('$ ' + respuesta.datos.DisponibleOtrosComercios.datos[0]);

            var ultimosTarjeta = respuesta.datos.ultimostarjeta;
            var contenido = "";
            if (ultimosTarjeta != "") {
                contenido = "<h4>Nº DE CUENTA</h4><p>*** **** **** " + ultimosTarjeta + "</p>"
            }

            $('#ultimostarjeta').html("").append(contenido);

            if (!respuesta.datos.DeudaPagar.error) {
                var tabla = "";
                var deudapagar = respuesta.datos.DeudaPagar.datos;
                tabla = '<tr><td class="infoleft border-top-0">Monto Facturado</td><td class="inforight border-top-0"><span class="first">$' + deudapagar['MontoFacturado'] + '</span>\</td></tr>';
                tabla += '<tr><td class="infoleft">Fecha Vencimiento</td><td class="inforight"><span>' + deudapagar['FechaVencimiento'] + '</span></td></tr>';
                tabla += '<tr><td class="infoleft">Deuda Total</td><td class="inforight"><span>' + deudapagar['DeudaTotal'] + '</span></td></tr>';

                if (deudapagar['DeudaTotal'] != '0' || deudapagar['MontoFacturado'] != '0') {
                    tabla += '<tr><td colspan="2" class="border-top-0 px-0 pb-0 pt-0"><button type="button" class="btn btn-primary" onclick="window.location.href=\'/private/paga-tu-cuenta/\'">PAGAR</button></td></tr>';
                }
                $('#deudapagar').html("").append(tabla);
            } else {
                $('#deudapagar tr:eq(0)').addClass('d-none');
                $('#deudapagar .d-none').removeClass('d-none');
            }

            $('#extracupotiendas p').html("$ " + respuesta.datos.DisponibleTiendas);
            $('#extracupoavances p').html("$ " + respuesta.datos.DisponibleAvanceEnEfectivo);

            if (respuesta.datos.ExtracupoTienda.error === false) {
                var ExtracupoTienda = respuesta.datos.ExtracupoTienda.datos;
                var tabla = "";
                $.each(ExtracupoTienda, function(index, data) {
                    tabla += "<tr>\
                        <td> " + data.cuotaMin + " a " + data.cuotaMax + " cuotas</td>\
                        <td>$ " + data.tope + "</td>\
                    </tr>";
                });

                if (tabla == "") {
                    $("#extracupotiendas").addClass('d-none');
                } else {
                    $("table tbody", "#extracupotiendas").html(tabla);
                }
            }

            if (respuesta.datos.ExtracupoAvance.error === false) {
                var ExtracupoAvance = respuesta.datos.ExtracupoAvance.datos;
                var tabla = "";
                $.each(ExtracupoAvance, function(index, data) {
                    tabla += "<tr>\
                        <td> " + data.cuotaMin + " a " + data.cuotaMax + " cuotas</td>\
                        <td>$ " + data.tope + "</td>\
                    </tr>";
                });

                if (tabla == "") {
                    $("#extracupoavances").addClass('d-none');
                } else {
                    $("table tbody", "#extracupoavances").html(tabla);
                }
            }

            if (respuesta.datos.ExtracupoAvance.error === true) {
                $("#disponibleotroscomercios").addClass('d-none');
            }
        },
        'error': function(respuesta) {
            $cargaCupos = false;
            return;
        }
    });
}

function datalayerpush(dataObject){
    if (typeof window.dataLayer != 'undefined') {
        window.dataLayer.push(dataObject);
    }
}