/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Entix07
 */
public class Hash {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Calendar timeStamp = Calendar.getInstance();
            timeStamp.setTime(new Date());
            String newTicket = getHash(Long.toString(timeStamp.getTimeInMillis()));
            System.out.println(newTicket);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    static String getHash(String message) throws NoSuchAlgorithmException {
    MessageDigest md;
    byte[] buffer, digest;
    String hash = "";        
    buffer = message.getBytes();
    md = MessageDigest.getInstance("SHA1");
    md.update(buffer);
    digest = md.digest();
    for(byte aux : digest) {
        int b = aux & 0xff;
        if (Integer.toHexString(b).length() == 1) hash += "0";
        hash += Integer.toHexString(b);
    }
    return hash;
  } 
}
