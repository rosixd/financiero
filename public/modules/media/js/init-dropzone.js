$( document ).ready(function() {
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(".dropzone", {
        url: Asgard.dropzonePostUrl,
        autoProcessQueue: true,
        maxFilesize: maxFilesize,
        acceptedFiles : acceptedFiles
    });
    
    myDropzone.on("queuecomplete", function(file, http) {
        /*window.setTimeout(function(){
            location.reload();
        }, 5000);*/
        return ;
    });

    myDropzone.on("sending", function(file, xhr, fromData) {
        xhr.setRequestHeader("Authorization", AuthorizationHeaderValue);
        if ($('.alert-danger').length > 0) {
            $('.alert-danger').remove();
        }

        return ;
    });

    myDropzone.on("error", function(file, errorMessage) {
        AppGeneral.notificacionErrorMedia = 'Sr. Usuario, debe ingresar un archivo valido.';

        /**
         * Capturando el mensaje de error desde el servidro
         * @autor Dickson Morales
         */
        if( !$.isEmptyObject( errorMessage ) && errorMessage.hasOwnProperty("message") ){
            AppGeneral.notificacionErrorMedia = errorMessage.message;
        }
        /* var html = '<div class="alert alert-danger" role="alert">' + errorMessage + '</div>';
        $('.col-md-12').first().prepend(html); */
        AppGeneral
            .notificaciones('error', AppGeneral.notificacionErrorMedia );

        myDropzone.removeFile(file);
        /*setTimeout(function() {
            myDropzone.removeFile(file);
        }, 5000);*/

        return ;
    });

    myDropzone.on("success", function(file, response) {
        var flag = $('#flagButtonSelect').val();

        AppGeneral.notificacionSuccessMedia = 'Sr. Usuario, el archivo multimedia se ha guardado correctamente.';
        
        /**
         * Capturando el mensaje de exito enviado por el servidor
         * autor Dickson Morales
        */
        if( !$.isEmptyObject( response ) && response.hasOwnProperty("message") ){
            AppGeneral.notificacionSuccessMedia = response.message;
        }

        AppGeneral.notificaciones('success', AppGeneral.notificacionSuccessMedia );
        myDropzone.removeFile(file);
        /*setTimeout(function() {
            myDropzone.removeFile(file);
        }, 5000);*/

        if( $(".data-table").length ){
            if( $.trim(AppGeneral.submodulo) == 'ckindex' ){
              window.location.reload();
            }else{
              $(".data-table").dataTable().ajax.reload();
            }
        }

        if($("#pathFoto-"+flag).length){
            $("#pathFoto-"+flag).val(response.path_bd);
        }

        return ;
    });

    return ;
});
