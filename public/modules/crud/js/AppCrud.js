var AppCrud = function(){

    var init = function(){

		AppGeneral.dataTable(
			AppCrud.configColumns(AppGeneral.submodulo), 
			AppGeneral.urlLanguage, 
			AppGeneral.urlData
		);
    } 

    var configColumns = function(submodulo){

        var columns = [];

        if(submodulo == 'campana'){
            columns = [ {
                "targets": 0,
                "render": function ( data, type, row ) {
                    return row.ABC06_nombre;
                },
            },{
                "targets": 1,
                "render": function ( data, type, row ) {
                    return row.ABC06_descripcion_compacta;
                },
            },{
                "targets": 2,
                "render": function ( data, type, row ) {
                    return row.ABC06_fecha_inicio;
                },
            },{
                "targets": 3,
                "render": function ( data, type, row ) {
                    return row.ABC06_fecha_fin;
                },
            },{
                "targets": 4,
                "render": function ( data, type, row ) {
                    return row.created_at;
                },
            },{
                "targets": 5,
                "searchable": false,
                "orderable": false,
                "render": function ( data, type, row ) {
                    return AppGeneral.buttonLog(row.key,row.ABC06_id) +'<a href="'+row.ruta_edit+'" id="edit'+row.ABC06_id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
                    '<input '+(row.inactivo? "":"checked")+' data-id="'+row.ABC06_id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
                },
            } ];
        }
		if(submodulo == 'servicio'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.ABC15_nombre;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.ABC15_titulo;
				},
			},{
				"targets": 2,
				"render": function ( data, type, row ) {
					return row.ABC15_descripcion_compacta;
				},
			},{
				"targets": 3,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},{
				"targets": 4,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.ABC15_id) +'<a href="'+row.ruta_edit+'" id="edit'+row.ABC15_id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-id="'+row.ABC15_id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
        }
        
        return columns;
    }

    return {
        init:function(){
            init();
        },
        configColumns:configColumns
    }
}();

$(document).ready(function(){
    AppCrud.init();
});

