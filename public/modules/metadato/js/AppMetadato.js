var AppMetadato = function(){

	var init = function(){	   
		var columns = {
			"metadato":[
				{ "data": "ABC04_contenido", "name": "ABC04_contenido" },
				{ "data": "tipometadato.ABC05_nombre", "name": "tipometadato.ABC05_nombre" },
        { "data": "ABC04_check_banner", "name": "ABC04_check_banner" },
				{ "data": "created_at", "name": "created_at" },
				null
			], 
			"tipometadato":[
				{ "data": "ABC05_nombre", "name": "ABC05_nombre" },
				{ "data": "ABC05_attributos", "name": "ABC05_attributos" },
				{ "data": "created_at", "name": "created_at" },
				null
			]
		};

		AppGeneral.dataTable(
			AppMetadato.configColumns(AppGeneral.submodulo), 
			AppGeneral.urlLanguage, 
			AppGeneral.urlData,
			{
				"columns":columns[AppGeneral.submodulo]
			}
		);
	} 	

	var configColumns = function(submodulo){

		var columns = [];

		if(submodulo == 'metadato'){
			columns = [ {
				"targets": 4,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = AppGeneral.buttonLog(row.key,row.ABC04_id);
					if (AppGeneral.permisos.editar) {
						botones += '<a href="'+row.ruta_edit+'" id="edit'+row.ABC04_id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}
					if (AppGeneral.permisos.desactivar) {
						botones += '<input '+(row.inactivo? "":"checked")+' data-id="'+row.ABC04_id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}
					return botones;
				},
			} ];
		}

		if(submodulo == 'tipometadato'){
			columns = [ {
				"targets": 3,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = AppGeneral.buttonLog(row.key,row.ABC05_id);
					if (AppGeneral.permisos.editar) {
						botones += '<a href="'+row.ruta_edit+'" id="edit'+row.ABC05_id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}
					if (AppGeneral.permisos.desactivar) {
						botones += '<input '+(row.inactivo? "":"checked")+' data-id="'+row.ABC05_id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}
					return botones;
				},
			} ];
		}

		return columns;
	}

	return {
		init:function(){
			init();
		},		
		configColumns:configColumns
	}
}();

$(document).ready(function(){
	AppMetadato.init();
});