var AppLog = function(){

	var init = function(){
		
		var columns = {
			"log":[
				{ "data": "event", "name": "event" },
				null,
				{ "data": "url", "name": "url" },
				{ "data": "ip_address", "name": "ip_address" },
				{ "data": "user_agent", "name": "user_agent" },
				{ "data": "coments", "name": "coments" },
				{ "data": "created_at", "name": "created_at" }
			]
		};

		AppGeneral.dataTable(
		  AppLog.configColumns(AppGeneral.submodulo),
			AppGeneral.urlLanguage, 
			AppGeneral.urlData,
            {
                "columns":columns[AppGeneral.submodulo]
            }
		);
	} 

	var configColumns = function(submodulo){

		var columns = [];

		if(submodulo == 'log'){

			columns = [{
				"targets": 1,
				"render": function ( data, type, row ) {

					if(jQuery.type( row.new_values ) === "string"){
						return row.new_values;
					}
					
          			return JSON.stringify(row.new_values);
				},
			}];
		}

		return columns;
	}

	return {
		init:function(){
			init();
		},
		configColumns:configColumns
	}
}();

$(document).ready(function(){
	AppLog.init();
});



