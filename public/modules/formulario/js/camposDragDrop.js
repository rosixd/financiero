
var CONTADOR=0;

function allowDrop(ev) {
	ev.dataTransfer.dropEffect = "copy";
    ev.preventDefault();
}

function drag(ev) {
	ev.dataTransfer.effectAllowed = "copy";
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if(data == "titulo_h1"){
        var campo='<div class="campo col-md-12"><div class="col-md-12" style="padding: 0px"><h1 class="input_editar">T&iacute;tulo '+CONTADOR+'</h1></div></div>'
    }
    if(data == "titulo_h2"){
        var campo='<div class="campo col-md-12"><div class="col-md-12" style="padding: 0px"><h2 class="input_editar">T&iacute;tulo '+CONTADOR+'</h2></div></div>'
    }
    if(data == "titulo_h3"){
        var campo='<div class="campo col-md-12"><div class="col-md-12" style="padding: 0px"><h3 class="input_editar">T&iacute;tulo '+CONTADOR+'</h3></div></div>'
    }
    if(data == "titulo_h4"){
        var campo='<div class="campo col-md-12"><div class="col-md-12" style="padding: 0px"><h4 class="input_editar">T&iacute;tulo '+CONTADOR+'</h4></div></div>'
    }
    if(data == "input_check"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><input type="checkbox" name="check_'+CONTADOR+'" class="input_editar" id="id_'+CONTADOR+'"><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span><label class="input_editar">Etiqueta</label></div></div>'
    }
    if(data ==  "input_text"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Etiqueta</label><input name="text_'+CONTADOR+'" id="id_'+CONTADOR+'" class="form-control input_editar" type="text" value=""><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span><span class="text-danger errorValidaRut" style="display: none"><span style="color: red;font-size:13px;">Sr. Usuario, el rut ingresado no es v&aacute;lido.</span></span><span class="text-danger erroValidaEmail" style="display: none"><span style="color: red;font-size:13px;">Sr. Usuario, el email ingresado no es v&aacute;lido.</span></span></div></div>'
    }
    if(data == "select"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Etiqueta</label><select name="selec_'+CONTADOR+'" id="id_'+CONTADOR+'" class="form-control input_editar"><option id="id_opcion_'+CONTADOR+'" value="">Opci&oacute;n</option></select><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span></div></div>'
    }
    if(data == "input_file"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Archivo:</label><input name="file_'+CONTADOR+'" type="file" id="id_'+CONTADOR+'" class="form-control input_editar"><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span></div></div>'
    }
    if(data == "input_radio"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><input name="radio_'+CONTADOR+'" class="input_editar" type="radio" id="id_'+CONTADOR+'" value="0"><label class="input_editar">Etiqueta</label></div></div>'
    }
    if(data == "text_area"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Etiqueta</label><textarea name="area_'+CONTADOR+'" id="id_'+CONTADOR+'" class="form-control input_editar"></textarea><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span></div></div>'
    }
    if(data == "calendario"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Fecha</label><input type="date" name="fecha_'+CONTADOR+'" id="id_'+CONTADOR+'" placeholder="Placeholder" class="form-control input_editar calendario"><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span></div></div>'
    }
    if(data == "prestadores"){
        var campo='<div class="form-group campo col-md-12"><div class="col-md-12" style="padding: 0px"><label class="input_editar">Prestadores</label><select name="clinica_'+CONTADOR+'" id="id_'+CONTADOR+'" class="form-control input_editar prestadores"><option id="id_opcion_'+CONTADOR+'" value="">Opci&oacute;n</option></select><span class="text-danger" style="display: none"><span style="color: red;font-size:13px;" class="spanRequerido">Sr. Usuario, este campo es obligatorio.</span></span></div></div>'
    }


    
    CONTADOR++;
    $('#contenedor_formulario').append(campo);


    var html = $('#contenedor_formulario').html();

    $('#ABC07_campos').val(html.trim());
}
function cambioCodigo(){
    var texto = $('#ABC07_campos').val();
    $('#contenedor_formulario').html(texto);
}

$(document).ready(function() {
    $("#visor_codigo").click(function(){      
        $("#ABC07_campos").val("");                                        
        $("#ABC07_campos").val($("#contenedor_formulario").html().trim());
    });

    $('body').on('click','.campo',function() {
        $("#exito_edicion").hide();  
        removeTagSelected();
        $(this).addClass('seleccionado');
        
        //addSetOpciones($(this),"id,name,class");
       $(this).append('<a href="#" id="removeTag" class="removeTag"><img alt="Eliminar" src="'+urlEliminar+'"></a>')
    });

    $("body").on('click','.removeTag',function(){    
            $(this).parent("div").fadeOut('slow',function(){                                                
                $(this).remove();
                //$("#formulario_modtool_ulopciones").empty();
                //$("#formulario_modtool_validaciones").empty();
            });
    });

    $("#enviarForm").on('click',function(){    
        if(removeTagSelected()){
            var html = $('#contenedor_formulario').html();
            $('#ABC07_campos').val(html.trim());
            $('#contenedor_formulario').html('');
            $("#form").submit();
            $('#contenedor_formulario').html($('#ABC07_campos').val());
            return true;    
        }
        
    });

});

function removeTagSelected(){                                              
        $(".seleccionado").removeClass("seleccionado");
        $(".removeTag").remove();
        return true;
}

function agregarOpcion(id_select){   

    $(id_select).append('<option id="id_opcion_'+CONTADOR+'" value="">Opci&oacute;n</option>');                       
    CONTADOR++;
}

function quitarOpcion(id_opcion){   
    $(id_opcion).remove();
}

 
$('body').on('click','.input_editar:text',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,name,class,rut_email,requerido");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"input_text");
    });
});

$('body').on('click','.calendario',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,name,class,requerido");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"calendario");
    });
});


$('body').on('click','label.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"class,html,title");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"label");
    });
});

$('body').on('click','.input_editar:radio',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,id,name,class,checked,value");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"input_radio");
    });
});

$('body').on('click','.input_editar:checkbox',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,id,name,class,checked,value");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"input_check");
    });
});

$('body').on('click','select.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,id,name,class,option,requerido");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"select");
    });
});

$('body').on('click','textarea.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,id,name,class,value,cols,rows,requerido");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"textarea");
    });
});

$('body').on('click','.input_editar:file',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,name,class,requerido");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"input_file");
    });
});

$('body').on('click','h1.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,html,class");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"titulo_h1");
    });
});

$('body').on('click','h2.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,html,class");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"titulo_h2");
    });
});

$('body').on('click','h3.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"columnas,columnas_campo,html,class");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"titulo_h3");
    });
});

$('body').on('click','h4.input_editar',function(){
    removeTagSelected();
    //addTagSelected($(this));                                     
    addSetOpciones($(this),"class,columnas,columnas_campo,html,class");
    $("#formulario_modtool_modificar").unbind('click');                     
    elemento_tool=$(this);
    $("#formulario_modtool_modificar").click(function(){
                toolSetOpciones(elemento_tool,"titulo_h4");
    });
});




function addSetOpciones(elementor,cadena_opciones){
    $("#formulario_modtool_validaciones").html("");
    console.log(cadena_opciones);
    //obtenerValidaciones();
    if($(elementor).hasClass('validaRut')){
        console.log('tiene clase rut');
        var checkedRut= 'checked';
    }else{
        checkedRut= '';
        if($(elementor).hasClass('validaEmail')){
            console.log('tiene clase mail');
            var checkedEmail= 'checked';
        }else{
            var checkedLibre= 'checked';
            checkedEmail= '';
        }
    }

    if($(elementor).hasClass('requerido')){
        console.log('tiene clase requerido');
      var checkedRequerido= 'checked';
    }else{
        checkedRequerido= '';

    }
    var opciones=cadena_opciones.toLowerCase().split(",");
    var indice=0;
    var html='';
    $("#listado_opciones").html("");
    $("#listado_opciones").append('<div class="form-group"><input type="hidden" id="formulario_modtool_idantiguo" value=""/></div>');   
    for (indice=0;indice < opciones.length;indice++){
        switch(opciones[indice]){
            case "columnas":
                html = '<div class="form-group"><label>Tama&ntilde;o Contenedor</label>';
                html = html+'<select name="toolSetColumnas" id="toolSetColumnas" class="form-control">';
                for (var i = 1; i < 13; i++) {
                    if($(elementor).parent().parent().hasClass('col-md-'+i)){
                        html = html+'<option value="'+i+'" selected>'+i+'</option>';
                    }else{
                        html = html+'<option value="'+i+'">'+i+'</option>';
                    }
                }
                html = html+'</select>';

                $("#listado_opciones").append(html);
                break;
            case "columnas_campo":
                html = '<div class="form-group"><label>Tama&ntilde;o Campo</label>';
                html = html+'<select name="toolSetColumnasCampo" id="toolSetColumnasCampo" class="form-control">';
                for (var i = 1; i < 13; i++) {
                    if($(elementor).parent().hasClass('col-md-'+i)){
                        html = html+'<option value="'+i+'" selected>'+i+'</option>';
                    }else{
                        html = html+'<option value="'+i+'">'+i+'</option>';
                    }
                }
                html = html+'</select>';

                $("#listado_opciones").append(html);
                break;
            case "id":
                $("#listado_opciones").append('<div class="form-group"><label>Id</label><input class="form-control" type="text" id="toolSetId" name="toolSetId" value="'+$(elementor).attr("id")+'"></div>');
                break;
            case "name":
                $("#listado_opciones").append('<div class="form-group"><label>Nombre</label><input class="form-control" type="text" id="toolSetName" name="toolSetName" value="'+$(elementor).attr("name")+'"/></div>');
                break;
            case "class":
                var valor_classes=$(elementor).attr("class");
                $("#listado_opciones").append('<div class="form-group"><label>Clases</label><input class="form-control" type="text" id="toolSetClass" name="toolSetClass" value="'+valor_classes+'"></div>');
                break;
            case "value":
                $("#listado_opciones").append('<div class="form-group"><label>Value</label><input class="form-control" type="text" id="toolSetVal" name="toolSetVal" value="'+$(elementor).attr("value")+'"/></div>');
                break;
            case "html":
                $("#listado_opciones").append('<div class="form-group"><label>Texto</label><input class="form-control" type="text" id="toolSetHtml" name="toolSetHtml" value="'+$(elementor).html()+'"/></div>');
                break;
            case "for":
                $("#listado_opciones").append('<div class="form-group"><label>For</label><input class="form-control" type=0"text" id="toolSetFor" name="toolSetFor" value="'+$(elementor).attr("for")+'"/></div>');
                break; 
            case "title":
                $("#listado_opciones").append('<div class="form-group"><label>Title</label><input class="form-control" type="text" id="toolSetTitle" name="toolSetTitle" value="'+$(elementor).attr("title")+'"/></div>');
                break;
            case "checked":
                $("#listado_opciones").append('<div class="form-group"><label>Checked</label><input type="checkbox" id="toolSetCheck" name="toolSetCheck" value="'+$(elementor).attr("checked")+'"/></div>');
                break;              
            case "cols":
                $("#listado_opciones").append('<div class="form-group"><label>Cols</label><input class="form-control" type="text" id="toolSetCols" name="toolSetCols" value="'+$(elementor).attr("cols")+'"/></div>');
                break;
            case "rows":
                $("#listado_opciones").append('<div class="form-group"><label>Rows</label><input class="form-control" type="text" id="toolSetRows" name="toolSetRows" value="'+$(elementor).attr("rows")+'"/></div>');
                break;
            case "rut_email":
                $("#listado_opciones").append('<div class="form-group"><input type="radio" id="toolSetRut" name="toolSetRutMail" '+checkedRut+'/><label>Rut </label><input type="radio" id="toolSetEmail" name="toolSetRutMail" '+checkedEmail+'/><label>Email </label><input type="radio" id="toolSetLibre" name="toolSetRutMail" '+checkedLibre+'/><label>Libre </label></div>');
            break;
            case "requerido":
                $("#listado_opciones").append('<div class="form-group"><input type="checkbox" id="toolSetRequerido" name="toolSetRequerido" '+checkedRequerido+'/><label>Requerido</label></div>');
            break;    
            case "option":
                var id_opcion=$("option:selected",elementor).attr("id");
                var id_select = $(elementor).attr("id");
                $("#listado_opciones").append('<h4>Editando la Opcion: '+id_opcion+'</h4>');
                $("#listado_opciones").append('<a href="#" onclick="agregarOpcion('+id_select+')"><i class="fa fa-plus-circle"></i></a>');
                $("#listado_opciones").append('<a href="#" onclick="quitarOpcion('+id_opcion+')" style="float:right"><i class="fa fa-minus-circle"></i></a>');
                $("#listado_opciones").append('<div class="form-group"><label>Value</label><input class="form-control" type="text" id="toolSetOptVal" name="toolSetOptVal" value="'+$(elementor).val()+'"/></div>');
                $("#listado_opciones").append('<div class="form-group"><label>Texto</label><input class="form-control" type="text" id="toolSetOptText" name="toolSetOptText" value="'+$("option:selected",elementor).text()+'"/></div>');
        }
    }   
    $("#listado_opciones").append('<div class="form-group"><button id="formulario_modtool_modificar" class="btn btn-primary btn-flat" type="button">Modificar</button></div>');   
}

function toolSetOpciones(elemento,tipo_elemento){           
    var valor_classes=$("#toolSetClass").val();
    var asterisco = '';
    //quitamos todas las clases de validaciones
    valor_classes=valor_classes.replace(' validaRut', '');
    valor_classes=valor_classes.replace(' validaEmail', '');
    valor_classes = valor_classes.replace(' requerido', '');

    if($("#toolSetRut").prop("checked")){
        valor_classes = valor_classes+' validaRut';
    }

    if($("#toolSetEmail").prop("checked")){
        valor_classes = valor_classes+' validaEmail';
    }

    if($("#toolSetRequerido").prop("checked")){
        var asterisco = '<span style="color: red;font-size:13px;" class="asterisco">*</span>';
        valor_classes = valor_classes+' requerido';
    }

    switch(tipo_elemento){
        case "titulo_h1":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).replaceWith('<h1 class="input_editar">'+$("#toolSetHtml").val()+'</h1>');
            break;
        case "titulo_h2":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).replaceWith('<h2 class="input_editar">'+$("#toolSetHtml").val()+'</h2>');
        break;
        case "titulo_h3":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).replaceWith('<h3 class="input_editar">'+$("#toolSetHtml").val()+'</h3>');
        break;
        case "titulo_h4":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());
            $(elemento).replaceWith('<h4 class="input_editar">'+$("#toolSetHtml").val()+'</h4>');
        break;

        case "input_text":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).parent().find('span.asterisco').remove();//borramos el asterisco
            $(elemento).parent().find('label').after(asterisco);//agregamos el asterisco si existe

            $(elemento).parent().find('span.spanRequerido').html('Sr. Usuario, debe ingresar el campo '+$("#toolSetName").val());//agregamos el asterisco si existe
            $(elemento).replaceWith('<input id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" class="'+valor_classes+'" placeholder="'+$("#toolSetName").val()+'" type="text"/>');
        break;
        case "calendario":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).parent().find('span.asterisco').remove();//borramos el asterisco
            $(elemento).parent().find('label').after(asterisco);//agregamos el asterisco si existe

            $(elemento).parent().find('span.spanRequerido').html('Sr. Usuario, debe ingresar el campo '+$("#toolSetName").val());
            console.log(valor_classes);
            $(elemento).replaceWith('<input id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" class="'+valor_classes+'" type="date"/>');
            break;
        case "label":
            //$(elemento).replaceWith("<label id='"+$("#toolSetId").val()+"' name='"+$("#toolSetName").val()+"' class='"+valor_classes+"' title='"+$("#toolSetTitle").val()+"' for='"+$("#toolSetFor").val()+"'>"+$("#toolSetHtml").val()+"</label>");
            $(elemento).replaceWith('<label id="'+$("#toolSetId").val()+'" class="'+valor_classes+'" title="'+$("#toolSetTitle").val()+'" for="'+$("#toolSetFor").val()+'">'+$("#toolSetHtml").val()+'</label>');
        break;
        case "input_radio":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            var marcado;
            if($("#toolSetCheck").prop('checked')){
                marcado="checked";
            }else{
                marcado="";
            }
            $(elemento).replaceWith('<input class="'+valor_classes+'" type="radio" name="'+$("#toolSetName").val()+'" id="'+$("#toolSetId").val()+'" value="'+$("#toolSetVal").val()+'" checked="'+marcado+'">');
        break;
        case "input_check":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());
            var marcado;
            if($("#toolSetCheck").prop('checked')){
                marcado="checked";
            }else{
                marcado="";
            }
            $(elemento).replaceWith('<input class="'+valor_classes+'" id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" value="'+$("#toolSetVal").val()+'" checked="'+marcado+'" type="checkbox">');
            break;
        case "input_file":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).parent().find('span.spanRequerido').html('Sr. Usuario, debe ingresar el campo '+$("#toolSetName").val());

            $(elemento).parent().find('span.asterisco').remove();//borramos el asterisco
            $(elemento).parent().find('label').after(asterisco);//agregamos el asterisco si existe

            $(elemento).replaceWith('<input type="file" id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" class="'+valor_classes+'">');                    
            break;
        case "select":  
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).parent().find('span.asterisco').remove();//borramos el asterisco
            $(elemento).parent().find('label').after(asterisco);//agregamos el asterisco si existe

            $(elemento).parent().find('span.spanRequerido').html('Sr. Usuario, debe ingresar el campo '+$("#toolSetName").val());

            var id_opcion = $(elemento).children(":selected").attr("id");      
            $(elemento).find("option[id='"+id_opcion+"']").text($("#toolSetOptText").val());
            $(elemento).find("option[id='"+id_opcion+"']").attr("value",$("#toolSetOptVal").val());
            $(elemento).replaceWith('<select id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" class="'+valor_classes+'">'+$(elemento).html()+'</select>');                 
            break;
        case "textarea":
            $(elemento).parent().parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().parent().addClass("col-md-"+$("#toolSetColumnas").val());
            $(elemento).parent().removeClass("col-md-1 col-md-2 col-md-3 col-md-4 col-md-5 col-md-6 col-md-7 col-md-8 col-md-9 col-md-10 col-md-11 col-md-12");
            $(elemento).parent().addClass("col-md-"+$("#toolSetColumnasCampo").val());

            $(elemento).parent().find('span.spanRequerido').html('Sr. Usuario, debe ingresar el campo '+$("#toolSetName").val());

            $(elemento).parent().find('span.asterisco').remove();//borramos el asterisco
            $(elemento).parent().find('label').after(asterisco);//agregamos el asterisco si existe

            $(elemento).replaceWith('<textarea id="'+$("#toolSetId").val()+'" name="'+$("#toolSetName").val()+'" class="'+valor_classes+'" cols="'+$("#toolSetCols").val()+'" rows="'+$("#toolSetRows").val()+'" value="'+$("#toolSetVal").val()+'"></textarea>');
            break;

        case "option":
        $(elemento).replaceWith('<option value="'+$("#toolSetOptVal").val()+'" selected="">'+$("#toolSetOptText").val()+'</option>');
        break;
    }
    $("#listado_opciones").html("");
    muestraMensaje($('#exito_edicion'),'Elemento Actualizado.','mensaje_exito');
}
function muestraMensaje(contenedor, mensaje){
    contenedor.html(mensaje);
    contenedor.show();
}


    


