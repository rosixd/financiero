    var AppFormulario = function(){
        
        var tabla = false;
        var init = function(){
        var columns = {
            "formulario":[
                { "data": "ABC07_nombre", "name": "ABC07_nombre" },
                { "data": "created_at", "name": "created_at" },
                null
            ], 
            "creditoconsumo":[
                { "data": "ABC26_nombre_archivo", "name": "ABC26_nombre_archivo" },				
                { "data": "ABC26_estado", "name": "ABC26_estado" },				
                { "data": "created_at", "name": "created_at" },
                null
            ],
            "FormularioContacto":[
                { "data": "ABC30_nombre_formulario", "name": "ABC30_nombre_formulario" },	
                { "data": "ABC30_nombrecompleto", "name": "ABC30_nombrecompleto" },	
                { "data": "ABC30_rut", "name": "ABC30_rut" },	
                { "data": "ABC30_telefono", "name": "ABC30_telefono" },	
                { "data": "ABC30_email", "name": "ABC30_email" },	
               /*  { "data": "ABC30_tiposeguro", "name": "ABC30_tiposeguro" },	 */
                {"data": "tiposeguro_str", "name": "tiposeguro_str" },	
                {"data": "ABC30_tiposolicitud", "name": "ABC30_tiposolicitud" },	
               /*  { "data": "ABC30_mensaje", "name": "ABC30_mensaje" },	 */
                { "data": "created_at", "name": "created_at" },
               
            ],
            "SolicitarTarjeta":[
                { "data": "ABC31_Rut", "name": "ABC31_Rut" },	
                { "data": "nombrecompleto", "name": "nombrecompleto" },	
                { "data": "apellidocompleto", "name": "apellidocompleto" },	
                { "data": "email", "name": "email" },	
                { "data": "fono_movil", "name": "fono_movil" },	
                { "data": "fono_fijo", "name": "fono_fijo" },	
                { "data": "crea_registro", "name": "crea_registro" },
               
            ]
        };
        AppGeneral.dataTable(
            AppFormulario.configColumns(AppGeneral.submodulo), 
            AppGeneral.urlLanguage, 
            AppGeneral.urlData,
            {
                "columns":columns[AppGeneral.submodulo],
                   "searching": false
            }
        );
    } 
    var ModalLgoFtp = function(button){
        var button = $(button);
		var id = button.data('id');
        
     
        if(AppFormulario.tabla){
            AppFormulario.tabla.destroy();
        }
        AppFormulario.tabla = $('#tableModalLog').DataTable({
            "processing": true,
            "serverSide": true,
             "searching": false,
            "language": {
				"url": AppGeneral.urlLanguage
			},
            "ajax": {
                "url":AppGeneral.urlLog,
                "type": "get",
                "data": {
                    'id': id,
                }
            },
            "columns": [
                { data: 'ABC27_descripcion', name: 'ABC27_descripcion' },
                { data: 'created_at', name: 'created_at' },
            ]
        });

		$('#myModal').modal('show');
    }
    
    var configColumns = function(submodulo){

        var columns = [];
       
        if(submodulo == 'formulario'){
            columns = [ {
                "targets": 2,
                "searchable": false,
                "orderable": false,
                "render": function ( data, type, row ) {
                    return AppGeneral.buttonLogftp(row.key,row.ABC07_id) + '<a href="'+row.ruta_edit+'" id="edit'+row.ABC07_id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
                    '<input '+(row.inactivo? "":"checked")+' data-id="'+row.ABC07_id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
                },
            } ];
        } else if (submodulo == "creditoconsumo") {
            columns = [
                {
                "targets": 3,
                "searchable": false,
                "orderable": false,
                "render": function (data, type, row) {
                    return  '<button class="btn btn-warning btn-xs" data-id="' + row.ABC26_id + '" ' +
                    'data-keymodel="' + row.key + '" onclick="AppFormulario.ModalLgoFtp(this)"> ' +
                    ' <span class="fa fa-search"></span> Historial</button>' + 
                    
                    '<a href="' + row.ruta_reenviar + '" id="edit' + row.ABC26_id + '" class="btn btn-xs btn-primary ' + (row.inactivo ? "disabled" : "") + '"><i class="fa fa-share-square-o "></i> Reenviar</a> ';
                },
            }];    
        }

        return columns;
    }

    return {
        init:function(){
            init();
        },
        configColumns:configColumns,
        ModalLgoFtp:ModalLgoFtp
    }
}();

$(document).ready(function(){
    
    $('.emails').hide(); 
    if($('#ABC07_activo_email').is(':checked')){
        $('.emails').show(); 
    }
    $('#ABC07_activo_email').on('ifChecked', function(){
        $('.emails').show(); 
    }).on('ifUnchecked', function(){
        $('.emails').hide(); 
    });
    
    $( "#rut").blur(function(){
        
        var rut = $.trim( $(this).val() );
      
        if( !$.validateRut(rut) ){
            ($(this).val() == '') ? $("#msjrut").hide() : $("#msjrut").show();
             
            return false;
        }

        $("#msjrut").hide();

        return $.formatRut(rut, true);
    });
  
    $('#start').val('');

    $('#ExportExcel').on("click",function(){
     $rut = $('input[name=rut]').val();
     
     if($rut != ''){
        if( !$.validateRut($rut) ){
            
            return false;
        }
     
     }
      
        $inicio = $('input[name=start]').val();
        $fin = $('input[name=end]').val();
        $rut = $('input[name=rut]').val();
        
        window.open(ruta_excel+'?fecha='+$inicio+'&rut='+$rut,'_blank');

    });
    
    
    AppFormulario.init();
    
    $('#dataTable_filter').remove();
});

