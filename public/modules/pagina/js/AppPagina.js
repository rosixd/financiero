var AppPagina = function() {

    var init = function() {
        AppPagina.ckeditorScript();
        AppGeneral.dataTable(
            AppPagina.configColumns(AppGeneral.submodulo),
            AppGeneral.urlLanguage,
            AppGeneral.urlData, {
                "columns": [
                    { "name": "page__page_translations.title" },
                    { "name": "page__page_translations.slug" },
                    { "data": "created_at", "name": "page__page_translations.created_at" },
                    null
                ]
            }
        );
    }

    var configColumns = function(submodulo) {

        return [{
            "targets": 0,
            "render": function(data, type, row) {
                return row.status != null ? row.title : "";
            },
        }, {
            "targets": 1,
            "render": function(data, type, row) {
                return row.status != null ? row.slug : "";
            },
        }, {
            "targets": 3,
            "searchable": false,
            "orderable": false,
            "render": function(data, type, row) {
                var botones = AppGeneral.buttonLog(row.key, row.id);
                if (AppGeneral.permisos.editar) {
                    botones += ' <a href="' + row.ruta_edit + '" id="edit' + row.id + '" ' +
                        'class="btn btn-xs btn-primary ' + (row.inactivo ? "disabled" : "") + '"> ' +
                        '<i class="glyphicon glyphicon-edit "></i> Editar</a> ';
                }
                if (AppGeneral.permisos.desactivar) {
                    botones += '<input ' + (row.inactivo ? "" : "checked") + ' data-articulo="' + row.articulo + '" data-id="' + row.id + '" ' +
                        'data-key="' + row.key + '" data-url="' + row.ruta_destroy + '" class="toggle" data-on="desactivar" ' +
                        'data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" ' +
                        'onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
                }
                return botones;
            },
        }];
    }
    var ckeditorScript = function() {

        var scriptAnalitic = $("#body").val();

        if (scriptAnalitic != undefined) {
            scriptAnalitic = scriptAnalitic.replace(/<link(.*)\/>/ig, '<div isDivConverter="lnk" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/<script(.*)><\/script>/ig, '<div isDivConverter="scr" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/<iframe(.*)><\/iframe>/ig, '<div isDivConverter="if" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/javascript\:/ig, "papascript\:");
        }

        $("form").submit(function() {
            var scriptAnalitic = CKEDITOR.instances['body'].getData();

            scriptAnalitic = scriptAnalitic.replace(/<link(.*)\/>/ig, '<div isDivConverter="lnk" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/<script(.*)><\/script>/ig, '<div isDivConverter="scr" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/<iframe(.*)><\/iframe>/ig, '<div isDivConverter="if" $1></div>');
            scriptAnalitic = scriptAnalitic.replace(/javascript\:/ig, "papascript\:");
            CKEDITOR.instances['body'].setData(scriptAnalitic);
            return true;
        });
    }
    return {
        init: function() {
            init();
        },
        configColumns: configColumns,
        ckeditorScript: ckeditorScript
    }
}();

$(document).ready(function() {
    AppPagina.init();
});