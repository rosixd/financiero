var AppAdministracion = function(){

	var init = function(){	
		
		var columns = {
			"usuario":[
				{ "data": "first_name", "name": "first_name" },
				{ "data": "last_name", "name": "last_name" },
				{ "data": "email", "name": "email" },
				{ "data": "created_at", "name": "created_at" },
				null
			], 
			"roles":[
				{ "data": "name", "name": "name" },				
				{ "data": "created_at", "name": "created_at" },
				null
			]
		};

		AppGeneral.dataTable(
			AppAdministracion.configColumns(AppGeneral.submodulo),
			AppGeneral.urlLanguage, 
			AppGeneral.urlData,
			{
				"columns":columns[AppGeneral.submodulo]
			}
		);
	} 

	var configColumns = function(submodulo){

		var columns = [];
    	submodulo = $.trim( submodulo );
    
		if(submodulo == 'usuario'){

			columns = [
				{
				"targets": 4,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = '';
					var buttonlog = AppGeneral.buttonLog(row.key,row.id);
					var edit = '';
					if (AppGeneral.permisos.editar) {
						edit = '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'">'+
						'<i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}
					if (AppGeneral.permisos.desactivar) {
						botones = '<input '+(row.inactivo? "":"checked")+' data-articulo="'+row.articulo+'" data-id="'+row.id+'" '+
						'data-key="'+row.key+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" '+
						'data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" '+
						'onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}

					return row.user ? buttonlog + edit: buttonlog + edit + botones;
				},
			} ];

		}else if(submodulo == 'roles'){

			columns = [ 
				{
				"targets": 2,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = '';
					var buttonlog = AppGeneral.buttonLog(row.key,row.id);
					var edit = '';
					if (AppGeneral.permisos.editar) {
						edit = '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'">'+
						'<i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}
					if (AppGeneral.permisos.desactivar) {
						botones = '<input '+(row.inactivo? "":"checked")+' data-articulo="'+row.articulo+'" data-id="'+row.id+'" '+
						'data-key="'+row.key+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" '+
						'data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" '+
						'onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}

					return row.user ? buttonlog + edit: buttonlog + edit + botones;
				},
			} ];
		
		}else{

			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.description;
				},
			},{
				"targets": 2,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},{
				"targets": 3,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = '';
					var buttonlog = AppGeneral.buttonLog(row.key,row.id);
					var edit = '';
					if (AppGeneral.permisos.editar) {
						edit = ' <a href="'+row.ruta_edit+'" id="edit'+row.id+'" '+
						'class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"> '+
						'<i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}

					if (AppGeneral.permisos.desactivar) {
						botones = '<input '+(row.inactivo? "":"checked")+' data-articulo="'+row.articulo+'" data-id="'+row.id+'" '+
						'data-key="'+row.key+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" '+
						'data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" '+
						'onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}

					return buttonlog + edit + botones;
				},
			} ];

		}

		return columns;
	}

	return {
		init:function(){
			init();
		},
		configColumns:configColumns
	}
}();

$(document).ready(function(){
	AppAdministracion.init();
});