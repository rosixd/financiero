var AppMenu = function(){

	var init = function(){	 
		AppGeneral.dataTable(
			AppMenu.configColumns(AppGeneral.submodulo), 
			AppGeneral.urlLanguage, 
			AppGeneral.urlData,
			{
                "columns":[
					
					{ "data": "title", "name": "menu__menu_translations.title" },
					{ "data": "created_at", "name": "menu__menus.created_at" },
					null
				]
            }
		);
	} 	

	var configColumns = function(submodulo){

		return  [ 
			{
				"targets": 2,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					var botones = AppGeneral.buttonLog(row.key,row.id);
					if (AppGeneral.permisos.editar) {
						botones += '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> ';
					}
					if (AppGeneral.permisos.desactivar) {
						botones += '<input '+(row.inactivo? "":"checked")+' data-id="'+row.id+'" data-key="'+row.key+'" data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
					}
					return botones;
				},
			} ];

		
	}

	return {
		init:function(){
			init();
		},		
		configColumns:configColumns
	}
}();

$(document).ready(function(){
	AppMenu.init();
});