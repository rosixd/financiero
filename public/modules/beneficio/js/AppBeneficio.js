var AppBeneficio = function(){

	var init = function(){	   

		AppGeneral.dataTable(
			AppBeneficio.configColumns(AppGeneral.submodulo), 
			AppGeneral.urlLanguage, 
			AppGeneral.urlData
		);
	} 	

	var configColumns = function(submodulo){

		var columns = [];

		if(submodulo == 'categoria'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},{
				"targets": 2,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.id) + '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-id="'+row.id+'" data-articulo="'+row.articulo+'" data-key="'+row.key+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
		}

		if(submodulo == 'beneficio'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.check_baner;
				},
			},{
				"targets": 2,
				"render": function ( data, type, row ) {
					return row.description;
				},
			},{
				"targets": 3,
				"render": function ( data, type, row ) {
					return row.categoria.ABC08_nombre;
				},
			},{
				"targets": 4,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},{
				"targets": 5,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.id) + '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-articulo="'+row.articulo+'" data-id="'+row.id+'" data-key="'+row.key+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
		}

		return columns;
	}

	return {
		init:function(){
			init();
		},		
		configColumns:configColumns
	}
}();

$(document).ready(function(){
	AppBeneficio.init();
});