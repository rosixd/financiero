var AppGeneral = function() {

    var urlLanguage,
        urlData,
        submodulo,
        vista,
        urlLog,
        auditId,
        keyModel,
        editor,
        clearcache,
        editable,
        datatableFilter,
        notificacionErrorMedia = '',
        notificacionSuccessMedia = '',
        permisos = {
            "crear": false,
            "editar": false,
            "desactivar": false,
            "log": false
        };

    var init = function() {
        AppGeneral.datatableFilter = false;
        AppGeneral.ckeditorScript();

        if ($('.select').length) {
            $('.select').selectize();
        }

        /* Filtro de busqueda datatablet*/
        if ($('#dateSearch').length) {
            $('#dateSearch').on('click', function() {
                $rut = $('input[name=rut]').val();
                if( $rut != ''){
                  if( !$.validateRut($rut) ){
                       
                      return false;
                  }
                }
                AppGeneral.datatableFilter = true;
                $('#dataTable').DataTable().draw();
            });
        }

        if ($('#start').length) {
            $('#start').daterangepicker({
                locale: {
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "separator": " / ",
                    "format": "DD-MM-YYYY",
                    "daysOfWeek": [
                        "Dom",
                        "Lun",
                        "Mar",
                        "Mie",
                        "Jue",
                        "Vie",
                        "Sab"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "monthOfWeek": [
                        "Ene",
                        "Feb",
                        "Mar",
                        "Abr",
                        "May",
                        "Jun",
                        "Jul",
                        "Ago",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dic"
                    ],
                },
                "autoclose": true,
                "todayHighlight": true
            });
        }

        if ($('.fechas').length) {
            $('.fechas').daterangepicker({
                locale: {
                    format: 'DD-MM-YYYY',
                    "daysOfWeek": [
                        "Dom",
                        "Lun",
                        "Mar",
                        "Mie",
                        "Jue",
                        "Vie",
                        "Sab"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "monthOfWeek": [
                        "Ene",
                        "Feb",
                        "Mar",
                        "Abr",
                        "May",
                        "Jun",
                        "Jul",
                        "Ago",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dic"
                    ],
                },
                singleDatePicker: true,
            });
        }

        if ($('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').length) {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        }

        if ($('#modalLog').length) {
            $('#modalLog').on('shown.bs.modal', function(e) {
                AppGeneral.loadLogInTable(AppGeneral.columnsLogTable(), AppGeneral.urlLanguage, AppGeneral.urlLog);
            });
        }

        AppGeneral.valiFormularioUsuario();
        return AppGeneral.initGrapJs();
    }

    var dataTable = function(columns, urlLanguage, urlData, opciones) {
        var opciones = opciones || {};


        opciones = $.extend({}, {
            processing: true,
            serverSide: true,
            searchDelay: 350,
            "ajax": {
                "url": urlData,
                "data": function(d) {
                    if (AppGeneral.datatableFilter) {
                        d.inicio = $('input[name=start]').val();
                        d.rut = $('input[name=rut]').val();
                        d.fin = $('input[name=end]').val();
                    }
                }
            },
            "columnDefs": columns,
            "language": {
                "sEmptyTable": "Sr. Usuario, no se encontraron resultados.",
                "infoEmpty": "Sr. Usuario, no se encontraron resultados.",
                "url": urlLanguage
            },
            "drawCallback": function(settings) {
                $('.toggle').bootstrapToggle();

                if ($('.tableTranslation').length) {
                    $('a.translation').editable({
                        url: function(params) {
                            var splitKey = params.pk.split("__-__");
                            var locale = splitKey[0];
                            var key = splitKey[1];
                            var value = params.value;

                            if (!locale || !key) {
                                return false;
                            }

                            if (value != "") {
                                $.ajax({
                                    url: AppGeneral.editable,
                                    method: 'POST',
                                    data: {
                                        locale: locale,
                                        key: key,
                                        value: value,
                                        _token: $("[name='csrf-token']").attr('content')
                                    },
                                    success: function(res) {
                                        notificaciones('success', 'Sr. Usuario, los datos se han actualizado correctamente.');
                                    },
                                    error: function(res) {
                                        if (res.status == '500') {
                                            notificaciones('error', 'Sr. Usuario, no se ha podido modificar el mensaje, por favor inténtelo nuevamente.');
                                        }
                                    },
                                })
                            }
                        },
                        type: 'textarea',
                        name: 'editartexto',
                        mode: 'inline',
                        send: 'always',
                        /* Always send, because we have no 'pk' which editable expects */
                        inputclass: 'translation_input',
                        emptytext: 'Sr. Usuario, se debe ingresar un mensaje.',
                        validate: function(value) {
                            if ($.trim(value) == '')
                                return 'Sr. Usuario, se debe ingresar un mensaje.';
                        }
                    });
                }
            }
        }, opciones);

        var order = $('#dataTable').data('order') || [];
        opciones.order = order;

        $('#dataTable').DataTable(opciones);
    }

    var modalFileInput = function(button) {

        var button = $(button);
        var id = $(button).data('id');
        var url = button.data('url');
        var id_img = button.data('idimg');

        $('#flagButtonSelect').val(id);

        $('#seleccionaImagen-' + id).hide("slow", function() {

            button.addClass('disabled');
            $('.dropzone-img').html("");

            $("#loadingSpin-" + id).addClass('fa fa-spin fa-spinner');

            $.ajax({
                url: url,
                data: { idbutton: id, idimg: id_img },
                context: document.body
            }).done(function(response) {
                $('#seleccionaImagen-' + id).show("slow", function() {
                    $('#seleccionaImagen-' + id).html(response);
                    $('#buttonLoadImg-' + id).data('dropzone', 0);
                    button.removeClass('disabled');
                    $("#loadingSpin-" + id).removeClass('fa fa-spin fa-spinner');
                });
            });
        });
    }

    var selectImage = function(button) {

        var name = $(button).data('name');
        var id = $(button).data('id'); //id imagen cargada
        var button = $(button).data('button');

        $("#pathFoto-" + button).val(name);
        $('#pathFotoId-' + button).val(id);
        $('#seleccionaImagen-' + button).hide("slow", function() {
            $('#seleccionaImagen-' + button).html("");
        });
    }

    var inactivarRegistro = function(input) {

        var switsh = $(input);
        var status = switsh.prop('checked');
        var url = switsh.data('url');
        var id = switsh.data('id');
        var key = switsh.data('key');
        var articulo = switsh.data('articulo');

        swal({
                title: "",
                text: "Sr. Usuario: \u00BFEst\u00E1 seguro de " + ((!status) ? 'desactivar' : 'activar') + " " + articulo + " " + key + "?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger btn-sm",
                cancelButtonClass: "btn-sm",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false,
                html: true,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: url,
                        type: "get",
                        data: {
                            status: !status
                        }
                    }).done(function(response) {
                        //console.log(response);
                        if (response.asignado != null && response.asignado) {
                            (!status) ? switsh.bootstrapToggle('on'): switsh.bootstrapToggle('off');
                            swal({
                                title: "",
                                text: response.msn,
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonClass: "btn-warning btn-sm",
                                confirmButtonText: "Aceptar",
                                closeOnConfirm: true,
                                html: true
                            });
                            return false;
                        }

                        if (response.success) {

                            (response.status) ? $('#edit' + id).addClass('disabled'): $('#edit' + id).removeClass('disabled');

                            swal({
                                title: "",
                                text: response.msn,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success btn-sm",
                                confirmButtonText: "Aceptar",
                                closeOnConfirm: true,
                                html: true
                            });

                        } else {
                            swal('Atenci\u00F3n!', response.msn, 'danger');
                        }

                    }).complete(function(x, e, o) {

                        switch (x.status) {
                            case 401:
                                break;
                            case 404:
                                //console.log('Sr. Usuario, no se encuentran registros para los datos ingresados');
                                break;
                            case 422:
                                //console.log('Sr. Usuario, no se encuentran registros para los datos ingresados');
                                break;
                            case 500:
                                console.log(x);
                                //console.log(e);
                                //console.log(o);
                                break;
                        }
                    });

                } else {
                    (!status) ? switsh.bootstrapToggle('on'): switsh.bootstrapToggle('off');
                    swal.close();
                }
            });
    }

    var notificaciones = function(tipo, msg) {

        if (tipo === "error") {
            var boton = "danger"
        }

        swal({
            title: "",
            text: msg,
            type: tipo,
            showCancelButton: false,
            confirmButtonClass: "btn-" + boton + " btn-md",
            /* cancelButtonClass: "btn-sm", */
            confirmButtonText: "Ok",
            /* cancelButtonText: "Cancelar", */
            closeOnConfirm: false,
            /* closeOnCancel: false, */
            html: true,
            showLoaderOnConfirm: true
        });
    }

    var buttonLog = function(keymodel, auditid) {
        if (!AppGeneral.permisos.log) {
            return '';
        }

        return ' <button class="btn btn-warning btn-xs" data-auditid="' + auditid + '" ' +
            ' data-keymodel="' + keymodel + '" onclick="AppGeneral.activeModalLog(this)"> ' +
            ' <span class="fa fa-search"></span> Historial</button> ';
    }

    var buttonLogftp = function(keymodel, auditid) {
        console.log('hola nuevo');
        return ' <button class="btn btn-warning btn-xs" data-auditid="' + auditid + '" ' +
            ' data-keymodel="' + keymodel + '" onclick="AppGeneral.activeModalLogftp(this)"> ' +
            ' <span class="fa fa-search"></span> Historial</button> ';
    }

    var activeModalLog = function(button) {

        var button = $(button);
        AppGeneral.auditId = button.data('auditid');
        AppGeneral.keyModel = button.data('keymodel');

        $('#modalLog').modal('show');
    }

    var activeModalLogftp = function(button) {
        console.log('hola nuevo');
        var button = $(button);
        AppGeneral.auditId = button.data('auditid');
        AppGeneral.keyModel = button.data('keymodel');

        $('#modalLog').modal('show');
    }

    var loadLogInTable = function(columns, urlLanguage, urlData) {

        $('#tableModalLog').dataTable().fnDestroy();

        $('#tableModalLog').DataTable({
            processing: true,
            serverSide: true,
            "searching": false,
            "lengthChange": false,
            "ajax": {
                "url": urlData,
                "data": function(d) {
                    d.key_model = AppGeneral.keyModel;
                    d.audit_id = AppGeneral.auditId;
                }
            },
            "columnDefs": columns,
            "language": {
                "url": urlLanguage
            }
        });
    }

    var columnsLogTable = function() {

        return columns = [{
            "targets": 0,
            "render": function(data, type, row) {
                return row.user;
            },
        }, {
            "targets": 1,
            "render": function(data, type, row) {
                return row.event;
            },
        }, {
            "targets": 2,
            "render": function(data, type, row) {
                return row.created_at;
            },
        }, {
            "targets": 3,
            "render": function(data, type, row) {
                return row.coments;
            },
        }, {
            "targets": 4,
            "render": function(data, type, row) {
                return row.browser_name;
            },
        }, {
            "targets": 5,
            "render": function(data, type, row) {
                return row.browser_version;
            },
        }, {
            "targets": 6,
            "render": function(data, type, row) {
                return row.ip_address;
            },
        }];
    }

    var initGrapJs = function() {

        if ($('#gjs').length) {
            AppGeneral.editor = grapesjs.init({
                container: '#gjs',
                components: '',

            });

            var blockManager = AppGeneral.editor.BlockManager;

            $.each(['row', 'columns'], function(index, value) {
                blockManager.add('new-' + value, {
                    label: value,
                    content: '<div class="row"></div>',
                });
            });
        }
    }

    /**
     * Valida si existe un mensaje de error en el formulario de usuarios
     * 
     * @autor DMT
     */
    var valiFormularioUsuario = function() {

        if (AppGeneral.submodulo == 'usuario' && /(create|edit|update)+/i.test(AppGeneral.vista)) {
            //Si existe un error 
            var verMsgPerfil = true;
            var verMsgPass = false;

            //Recorriendo todos los inputs del formulario
            $("form input, form select").each(function() {
                var objPadre = $(this).parent(); //padre del input
                var msgError = $.trim(objPadre.find("span").text()); //mensaje de error
                var attrNameThis = $.trim($(this).attr('name')).toLowerCase();
                //si el padre del input tiene la clase has-error y 
                //si hay un mensaje de error 
                // se cambia el valor de verMsgPerfil 
                if (attrNameThis != '_token') {
                    if ($.trim(AppGeneral.vista).toLowerCase() == 'edit') {
                        if (
                            attrNameThis == 'roles[]' &&
                            msgError != ''
                        ) {
                            verMsgPerfil = false;
                        }

                        if (
                            /(password|password_confirmation)/i.test(attrNameThis) &&
                            verMsgPerfil &&
                            msgError != ''
                        ) {
                            verMsgPass = true;
                        }
                    } else {
                        if (attrNameThis != 'roles[]' && msgError != '') {
                            verMsgPerfil = false;
                        }
                    }
                }
            });

            //Buscando el padre del select de roles
            var objPadreRol = $("form select[name='roles[]']").parent();
            var msgError = $.trim(objPadreRol.find("span").text());

            /**
             * Si los inputs no tienen error, ademas
             * se muestra un error en roles, se hace click
             * a la pestaña de roles
             */
            if (
                verMsgPerfil &&
                msgError != ''
            ) {
                return $("form .nav-tabs a[href='#tab_2-2']").click();
            }

            if (verMsgPass) {
                return $("form .nav-tabs a[href='#password_tab']").click();
            }

            return;
        }
    };

    var GenerateSlug = function(str) {
        //replace all special characters | symbols with a space

        str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();

        // trim spaces at start and end of string
        str = str.replace(/^\s+|\s+$/gm, '');

        // replace space with dash/hyphen
        str = str.replace(/\s+/g, '-');
        //document.getElementById("slug").innerHTML= str;
        return str;
    }

    var _dirty = function($original, $datos) {
        var dirty = false;

        $.each($original, function(id, valor) {
            /*var dato = $($datos[id]).val();
            if(!$.isNumeric(dato)){
            	dato = AppGeneral.GenerateSlug($($datos[id]).val());
            	
            } else{

            	console.log( valor , dato);
            }*/
            if (valor != AppGeneral.GenerateSlug($($datos[id]).val())) {

                dirty = true;
            }
        });

        return dirty

    }

    var ckeditorScript = function() {


        $("form#ValidacionEditar").submit(function() {
            
            var el = document.forms[0]['core::analytics-script'];
            if (el == undefined) {
                return true;
            }

            var scriptAnalitic = el.value;
            scriptAnalitic = scriptAnalitic.replace(/<script\s+async src=\"([\w\/\.\_\-\d\=\?\:\s]+)\"><\/script>/ig, "<div async src=\"$1\"></div>");
            scriptAnalitic = scriptAnalitic.replace(/<script>/ig, "<div>");
            scriptAnalitic = scriptAnalitic.replace(/<\/script>/ig, "</div>");
            el.value = scriptAnalitic;
            return true;
        });
    }

    return {
        init: function() {
            init();
        },
        permisos: permisos,
        notificaciones: notificaciones,
        modalFileInput: modalFileInput,
        selectImage: selectImage,
        inactivarRegistro: inactivarRegistro,
        dataTable: dataTable,
        buttonLog: buttonLog,
        loadLogInTable: loadLogInTable,
        columnsLogTable: columnsLogTable,
        activeModalLog: activeModalLog,
        initGrapJs: initGrapJs,
        valiFormularioUsuario: valiFormularioUsuario,
        GenerateSlug: GenerateSlug,
        _dirty: _dirty,
        ckeditorScript: ckeditorScript

    }

}();

$(document).ready(function() {
    AppGeneral.init();
});