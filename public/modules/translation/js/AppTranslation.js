var AppTranslation = function(){

    var init = function(){

        var columns = {
			"translation":[
				{ "data": "key"},
				{ "data": "value", "name": "translation__translation_translations.value"  },
                /* { "data": "created_at", "name": "created_at" }, */
                null
			]
		};
        AppTranslation.borrarCache();
        
		AppGeneral.dataTable(
            AppTranslation.configColumns(AppGeneral.submodulo),
            AppGeneral.urlLanguage, 
            AppGeneral.urlData,
            {
                "columns":columns[AppGeneral.submodulo]
            }
		);
    } 

    var configColumns = function(submodulo){

        var columns = [];

        if(submodulo == 'translation'){
            columns = [ {
                "targets": 1,
                "render": function ( data, type, row ) {
                    if (AppGeneral.permisos.editar) {
                        return '<a class="translation" data-pk="es__-__'+row.llave+'">'+row.value+'</a>';
                    }
                    return row.value;
                },
            },{
                "targets": 2,
               
                "render": function ( data, type, row ) {
                    return AppGeneral.buttonLog('Translation',row.id);
                },
            } ];
        }       
        return columns;
    }

    var borrarCache = function(){
        $('.jsClearTranslationCache').on('click',function (event) {
            event.preventDefault();
            var $self = $(this);
            $self.find('i').toggleClass('fa-spin');
            var token_id = $("[name='csrf-token']").attr('content');
            $.ajax({
                type: 'POST',
                url: AppGeneral.clearcache,
                data: {_token: token_id},
                success: function() {
                    $self.find('i').toggleClass('fa-spin');
                }
            });
        });
    }


    return {
        init:function(){
            init();
        },
        configColumns:configColumns,
        borrarCache:borrarCache
    }
}();

$(document).ready(function(){
    AppTranslation.init();
});
