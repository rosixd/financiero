var AppTestService = {
    urlApiReq: '',
    urlloaderimg: '',
    urltrueimg: '',
    urlfalseimg: '',
    

    init: function(){
        return $("#reqTestService li a").click(function( e ){
            e.preventDefault();

            return AppTestService.getInfoReq( $( this ).data("id") );
        });
    },

    getInfoReq: function( i ){
        $('#resultTestService').html("");

        var loader = '<div class="text-center" id="loader-img" style="" >' + 
            '<img src="'+AppTestService.urlloaderimg+'" alt=""  class="Responsive image"style="width: 30%; " >' +
        '</div>';
        $('#resultTestService').append(loader);
       
        return $.ajax({
            url: AppTestService.urlApiReq,
            data:{
                i: i
            },
            dataType: 'json'
        }).done(function( r ){
           
            AppTestService.getTablas(r);
        }).complete(function (r) {
            $('#loader-img').css('display', 'none');
        }).error(function(r){
          var responseText = r.responseText.split("<!DOCTYPE html>")[0],
              r = JSON.parse(responseText);
          AppTestService.getTablas(r);
        })
    },
    getTablas: function(r) {
        var Table = "";
        
        $.each(r, function(index, valor) {
            Table = "";
            
            Table += '<table class="table-responsive table table-hover" style="width: 100%;"> ';

            Table += AppTestService.getTablaDatos(valor);
          
            Table += "</table><hr>";

            $('#resultTestService').append(Table);
        });
    },
    getTablaDatos:function (r) {
        var Header = "";
        
        $.each(r, function(index, valores) {

            if(index == 'Estatus'){
                var img = AppTestService.urlfalseimg;
                if(valores == true){
                    img = AppTestService.urltrueimg;
                }
                valores = '<img src="'+img+'" alt=""  class="Responsive image" style="width: 50px;">';
            }

            Header += "<tr>" +
                "<td>" +
                    "<b>" + index + ":</b>" +
                "</td>" + 
                "<td>" + AppTestService.getValores( valores ) + "</td>";
                
           /*  if( typeof valores == 'object' ){
                $.each(valores, function( indice, valor ){
                    Header += '<p>' + 
                        indice + " : " + valor
                    '</p>';
                });
            }else{
                Header += valores;
            }  */
            
            Header += // "</td>" + 
                "<tr>";
        });

        return Header;
    },

    getValores: function( valores ){
        var fila = '';

        if( typeof valores == 'object' ){
            fila += '<ul>';
            $.each(valores, function( indice, valor ){
                fila += '<li>' + 
                    indice + ': ' +
                    AppTestService.getValores( valor ) +
                '</li>';
            });
            fila += '</ul>';
        }else{
            fila = valores;
        }

        return fila;
    }
};

$(document).ready(function(){
    return AppTestService.init();
});