var AppSeguro = function(){

	var init = function(){	   

		AppGeneral.dataTable(
			AppSeguro.configColumns(AppGeneral.submodulo), 
			AppGeneral.urlLanguage, 
			AppGeneral.urlData
		);
	} 	

	var configColumns = function(submodulo){

		var columns = [];

		if(submodulo == 'categoriaseguro'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.nombre_imagen;
				},
			},{
				"targets": 2,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},{
				"targets": 3,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.id) +'<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-id="'+row.id+'" data-key="'+row.key+'" data-url="'+row.ruta_destroy+'"   data-articulo="'+row.articulo+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
		}else if(submodulo == 'poliza'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.ABC16_codigo;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},
			{
				"targets": 2,
				"render": function ( data, type, row ) {
					return row.ABC16_plan_1;
				},
			},
			{
				"targets": 3,
				"render": function ( data, type, row ) {
					return row.ABC16_plan_2;
				},
			},
			{
				"targets": 4,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},
			{
				"targets": 5,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.id) + '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-id="'+row.id+'" data-key="'+row.key+'"  data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
		}else if(submodulo == 'seguro'){
			columns = [ {
				"targets": 0,
				"render": function ( data, type, row ) {
					return row.name;
				},
			},{
				"targets": 1,
				"render": function ( data, type, row ) {
					return row.categoria;
				},
			},
			{
				"targets": 2,
				"render": function ( data, type, row ) {
					
					return row.ABC17_descripcion_corta;
				},
			},
		
			{
				"targets": 3,
				"render": function ( data, type, row ) {
					return row.created_at;
				},
			},
			{
				"targets": 4,
				"searchable": false,
				"orderable": false,
				"render": function ( data, type, row ) {
					return AppGeneral.buttonLog(row.key,row.id) + '<a href="'+row.ruta_edit+'" id="edit'+row.id+'" class="btn btn-xs btn-primary '+(row.inactivo? "disabled":"")+'"><i class="glyphicon glyphicon-edit "></i> Editar</a> '+
					'<input '+(row.inactivo? "":"checked")+' data-id="'+row.id+'" data-key="'+row.key+'"  data-articulo="'+row.articulo+'" data-url="'+row.ruta_destroy+'" class="toggle" data-on="desactivar" data-off="activar" data-onstyle="success" data-offstyle="danger" data-size="mini" onchange="AppGeneral.inactivarRegistro(this)" type="checkbox">';
				},
			} ];
		}
		
		return columns;
	}
	var subMenu = function(){
		if($("#registros").val() > 0){
			iCnt = $("#registros").val()
		}else{
		   iCnt = 1; 
		}
	
		// var iCnt = 0;
		$('#btnAdd').click(function(){
		iCnt++;
		
		var contenido = '<div class="panel panel-primary" id="panel' + iCnt +'">'
				+'<div class="panel-heading">'
					+'<h3 class="panel-title"></h3>'
				+'</div>'
				+'<div class="panel-body">'
					+'<div class="form-group">'
						+'<label>Complemento pregunta</label>'
						+'<input type="text" class="form-control" name="ABC18_pregunta[]" id="ABC18_pregunta' + iCnt +'"/>'
						+'<span class="text-danger" style="display: none">Sr. Usuario, debe ingresar Nombre.</span>'
					+'</div>'
					+'<div class="form-group">'
						+'<label>Complemento Descripci&oacute;n</label>'
						+'<textarea class="form-control ckeditor requerido " name="ABC18_respuesta[]" id="ABC18_respuesta'+ iCnt +'"></textarea>'
						+'<span class="text-danger" style="display: none">Sr. Usuario, debe ingresar Descripci&oacuten.</span>'
					+'</div>'
				+'</div>'
				+'<div class="panel-footer">'
					+'<button type="button" class="btn btn-danger" onclick="return eliminar('+ iCnt +');"><span class="glyphicon glyphicon-trash" aria-hidden="true">'
					+'</span>Eliminar</button>'
				+'</div>'
			+'</div>';
		$('#subMenu').append(contenido);
		CKEDITOR.replace('ABC18_respuesta' + iCnt);
		});
	};

	return {
		init:function(){
			init();
		},		
		configColumns:configColumns,
		subMenu:subMenu
	}
}();

$(document).ready(function(){
	AppSeguro.init();
	AppSeguro.subMenu();

});

function eliminar(iCnt) {
	
	if (iCnt != 0) { 
		console.log('#panel' + iCnt);
		$('#panel' + iCnt).remove(); 
	}
}