/** 
 * @author Ing. Miguelangel Gutierrez, Dickson Morales
 * @description validacion de formulario y tipos de campos.
 */


$(document).ready(function () {
    //variables globales
   

    // eventos de validacion
    
    //RUT
    $( ".validaRut").blur(function(){
        
        var rut = $.trim( $(this).val() );
        
        if( !$.validateRut(rut) ){
            ($(this).val() == '') ? $("#msjrut").hide() : $("#msjrut").show();
            return true;
        }

        
        $("#msjrut").hide();
        return $.formatRut(rut, false);
    });
    //validacion password
    $("#password").keypress(function (e) { if (/\w/.test(e.key)) { return true; } return false; })


    //validacion de formulario

    //valida los formularios que tengan la clase formValida
    //valida los formularios que tengan 
    if ($("form.formValida").length) {
        $("form.formValida").submit(function () {

            var error = true;
            $(this).find("span.text-danger").hide();

            //buscando todos los campos que 
            //contengan la clase requerido 
            var focus = true;
            $(this).find(".requerido").each(function () {
                var objReq = $(this);
                var padreObj = objReq.parent();
                var valor = $.trim(objReq.val());

                if (objReq.hasClass("ckeditor")) {

                    valor = $.trim(CKEDITOR.instances[objReq.attr("id")].getData());
                } else if (objReq.hasClass("selectized")) {
                    var valorSlectize = $(this).selectize().val();
                    valor = '';

                    if (valorSlectize) {
                        valor = $.trim(valorSlectize.join(','));

                    }
                } else if (objReq.hasClass("validaRut")){
                    var rut = $.trim($(this).val());
                        $(this).rut();
                        if (!$.validateRut(rut)) {
                            ($(this).val() == '') ? $("#msjrut").hide() : $("#msjrut").show();
                            //error = false;
                            console.log(error);
                        }

                }

                if (valor != '') {
                    if (objReq.hasClass("validaRut") && !$.validateRut(valor)) {
                        error = false;
                        padreObj.find("span.text-danger.errorValidaRut").show();
                    }

                    if (objReq.hasClass("validaEmail") && !validarEmail(valor)) {
                        error = false;
                        padreObj.find("span.text-danger.erroValidaEmail").show();
                    }

                    if (objReq.hasClass("validaPassword") &&
                        (
                            valor.length < 8 ||
                            valor.length > 15 ||
                            !/\d+/.test(valor) ||
                            !/[a-z]+/.test(valor) ||
                            !/[A-Z]/.test(valor)
                        )
                    ) {
                        error = false;
                        padreObj.find("span.text-danger.errorValidaPassword").show();
                    }
                } else {
                    error = false;
                    padreObj
                        .find("span.text-danger")
                        .not(".erroValidaEmail")
                        .not(".errorValidaPassword")
                        .not(".errorValidaRut")
                        .show();

                    if (objReq.hasClass("archivo")) {
                        padreObj.parent().find("span.text-danger").show();
                    }
                }

                if (!error && focus) {
                    if (objReq.hasClass("archivo")) {

                        $(window).scrollTop(padreObj.parent().find("label").position().top);
                        focus = false;
                    } else if (objReq.hasClass("fileInput")) {

                        $(window).scrollTop(padreObj.parent().parent().position().top);
                        focus = false;
                    } else {
                        $(window).scrollTop(padreObj.find("label").position().top);
                        focus = false;
                    }
                }

            });
            $("#contenedor_formulario").find("span.text-danger").hide();

            if (!error) {
                return false;
            }

            return true;
        });
    }


})