<!DOCTYPE html>
<html>
    <head lang="es">
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <title>
            @section('title')@setting('core::site-name')@show
        </title>
        <link rel="shortcut icon" href="{{ Theme::url('favicon.ico') }}">

        <!--estilos CSS -->
        <link rel="stylesheet" href="{{ Theme::url('font-awesome/css/fontawesome-all.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Theme::url('font-awesome/css/fontawesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Theme::url('css/estilos.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.css') }}"/>
        <link rel="stylesheet" href="{{ asset('themes/adminlte/css/vendor/alertify/alertify.core.css') }}"/>
        <!-- bootstrap CSS -->
        <link rel="stylesheet" href="{{ Theme::url('css/bootstrap/css/bootstrap.min.css') }}">
    </head>
    <body id="inicio">
        
        <!-- contenidos -->
        <section>
            @yield('content')
        </section>    

        @stack('html')

        <!-- jQuery library -->
        <script src="{{ Theme::url('js/jquery/jquery.min.js') }}"></script>

        <!-- Popper JS -->
        <script src="{{ Theme::url('js/popper/popper.min.js') }}"></script>

        <!-- Latest compiled JavaScript -->
        <script src="{{ Theme::url('css/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('themes/adminlte/js/vendor/alertify/alertify.js') }}"></script>
        <script src="{{ asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
        @php
            $RequestRouteAction = request()->route()->getAction();
        @endphp

        @if(array_key_exists('script',$RequestRouteAction))
            {!! Theme::script('js/front/private/'.$RequestRouteAction['modulo'].'/'.$RequestRouteAction['script'].'.js') !!}
        @endif
        
        {!! Theme::script('js/appGeneral.js') !!}
        {!! Theme::script('js/appValidacion.js') !!}
        
        <!-- {{ route('api.testSerices.req05') }} -->
        <!-- Rut JS -->
        <script src="{{ url('js/jquery.rut.js') }}"></script>
        <script>
            AppValidacion.baseurl = "{{ env('APP_URL') }}";
            AppValidacion.consultatarjeta = "{{ route('Public.private.ConsultaTarjetas') }}";
            AppValidacion.login = "{{ route('Public.private.login') }}";
            var appListScript = appListScript || [];
            for (var i = 0; i < appListScript.length; i++) {
                $("body").append('<script src="' + appListScript[i] + '"><\/script>');
            }

            @if( isset($session['TimeSession']) )
                var urlrenovar = "{{ route('Public.private.renovar') }}";
		
                tiemposession( {{ $session['TimeSession'] }} , {{ $session['tiempo'] }}, {{ $session['tiempodealerta'] }});
            @endif

            $(document).ready(function(){
                if(( typeof AppEstadoCuenta) == 'object'){
                    AppEstadoCuenta.urlDocPdf = "{{ route('api.resource.estado_cuenta') }}";
                }

                return ;
            });
        </script>
        @stack('scripts')
        
    </body>
</html>
