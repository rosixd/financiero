<!DOCTYPE html>
<html>
    <head lang="es">
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
	    @yield('metadatos')
        @php
            $polifyllCssBootstrap = '';
            $polifyllJsBootstrap = '';            
            $polifyll = '';

            $metaIE = Agent::is("ie") ? '<meta http-equiv="X-UA-Compatible" content="IE=edge,11,10,9,8,7">' : '';

            /* Si se detecta internet explorer version menor a 10 se cargan polifyll */
            if( Agent::is("ie") && (Agent::version( Agent::browser() ) < 10) ){
                $polifyllCssBootstrap = '<link href="' . Theme::url( 'IE/css/bootstrap-ie9.css' ) . '" rel="stylesheet">';
                $polifyllJsBootstrap = '<script src="' . Theme::url( 'IE/js/bootstrap-ie9.js' ) . '"></script>';
                $polifyll .= '<script src="' . Theme::url( 'IE/js/html5shiv.js' ) . '"></script>' .
                    '<script src="' . Theme::url( 'IE/js/html5shiv-printshiv.js' ) . '"></script>';
            }

            if( Agent::is("ie") && (Agent::version( Agent::browser() ) > 10) ){
                $polifyllCssBootstrap .= '<link href="' . Theme::url( 'IE/css/hackIE.css' ) . '" rel="stylesheet">';

                $polifyllJsBootstrap .= '<script src="' . Theme::url( 'IE/js/hackIE.js' ) . '"></script>';
            }

            // DMT: Detectanto si es ios el dispositivo
            $IOScss = '';
            $IOSjs = '';
            if( trim( strtolower( Agent::platform() ) ) == 'ios' ) {
                $IOScss = '<link href="' . Theme::url( 'css/bootstrap-modal-ios.css' ) . '" rel="stylesheet">';
                $IOSjs = '<script src="' . Theme::url( 'js/bootstrap-modal-ios.js' ) . '"></script>';          
            }
        @endphp
	    {!! $metaIE !!}
        
        <link rel="shortcut icon" href="{{ Theme::url('favicon.ico') }}">

        <!--estilos CSS -->
        <link rel="stylesheet" type="text/css" href="{{ Theme::url('font-awesome/css/fontawesome-all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Theme::url('css/estilos.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Theme::url('css/responsive.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.css') }}"/>
        <link rel="stylesheet" href="{{ asset('themes/adminlte/css/vendor/alertify/alertify.core.css') }}"/>
        
        <!-- bootstrap CSS -->
        {!! $polifyllCssBootstrap !!}
        {!! $polifyll !!}
        <link rel="stylesheet" href="{{ Theme::url( 'css/bootstrap/css/bootstrap.css' ) }}">
        
        <!-- datepicker -->
        <link rel="stylesheet" href="{{ asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker_v3/daterangepicker.css') }}">
        {!! $IOScss !!}
       
	<?php if (Setting::has('core::analytics-script')): ?>
    {!! Setting::get('core::analytics-script') !!}
<?php endif; ?>
    
    @php 
        $url_sitio = str_replace(env('APP_URL'), '', URL::current());
        $_url_sitio = explode('/', $url_sitio);
        $_url_sitio = end($_url_sitio);
	
        $paginas = [
            ''                     => 'home',
            'paga-tu-cuenta'       => 'paga-tu-cuenta',
            'tarjeta-abcvisa'      => 'tarjeta-abcvisa',
            'tarjeta-app'          => 'abcvisa-app',
            'seguros'              => 'seguros',
            'asesoria-financiera'  => 'asesoria-financiera',
        ];

        $valor = isset($paginas[$_url_sitio]) ? $paginas[$_url_sitio] : false;
    @endphp
    @if ($valor !== false)
        <script>
        (function(i,s,o,g,r,a,m){i['InstanaEumObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//eum.instana.io/eum.min.js','ineum');
        ineum('reportingUrl', 'https://eum-us-west-2.instana.io');
        ineum('key', 'eJNWJHzRRCSnqHBtFevCHw');
        ineum('page', '{{ $valor }}');
        </script>
    @endif
    <title>
            @if ( $valor == 'tarjeta-abcvisa' )
               Tarjeta abcvisa | Solicítala en Línea
            @elseif( $valor == 'home' )
                Tarjeta de Crédito abcvisa
            @else
                @section('title')@setting('core::site-name')@show
            @endif
    </title>
    </head>
    <body id="inicio">
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N8BXZBT" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        @include('partials.navigation')
        
        <!-- contenidos -->
        <section>
            @yield('content')
        </section>    

        @include('partials.footer')
        @stack('html')

        <aside id="chatwidget"></aside>
	
        <!-- jQuery library -->
        <script src="{{ Theme::url('js/jquery/jquery.min.js') }}"></script>

        <!-- Popper JS -->
        <script src="{{ Theme::url('js/popper/popper.min.js') }}"></script>

        <!-- Latest compiled JavaScript -->
        <script src="{{ Theme::url( 'css/bootstrap/js/bootstrap.min.js' ) }}"></script>
        {!! $polifyllJsBootstrap !!}
        <script src="{{ asset('themes/adminlte/js/vendor/alertify/alertify.js') }}"></script>
        <script src="{{ asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
        <script src="{{ asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker/moment.min.js') }}"></script>
        <script src="{{ asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker_v3/daterangepicker.js') }}"></script>
        <script src="{{ asset('themes/adminlte/vendor/jQuery-Mask/jquery.mask.min.js') }}"></script>
        <script src="{{ asset('themes/abcdin/js/chatwidget.bundle.js') }}"></script>
        {!! $IOSjs !!}
	
        {!! Theme::script('js/appGeneral.js') !!}
        {!! Theme::script('js/appValidacion.js') !!}
        
        @php
            $RequestRouteAction = request()->route()->getAction();
        @endphp

        @if(array_key_exists('script',$RequestRouteAction))
            {!! Theme::script('js/front/private/'.$RequestRouteAction['modulo'].'/'.$RequestRouteAction['script'].'.js') !!}
        @endif
	        
        <script>
            var urlformulario = "{{ route('Public.formulario.guardar') }}";
        </script>

        
        <!-- Rut JS -->
        <script src="{{ url('js/jquery.rut.js') }}"></script>
        <script>
            servicionodisponible = "{{ trans('page::global.servicios no disponible') }}";
            transnumserietarjeta = "{{ trans('user::SolicitudDeTarjeta.no ingresado serie') }}";
            validaruttarjeta = "{{ route('Public.solicitudtajerta.validacliente') }}";
            guardarformulariotarjeta = "{{ route('Public.solicitudtajerta.guardar') }}";
            AppValidacion.baseurl = "{{ env('APP_URL') }}";
            AppValidacion.consultatarjeta = "{{ route('Public.private.ConsultaTarjetas') }}";
            AppValidacion.login = "{{ route('Public.private.login') }}";
            var appListScript = appListScript || [];
            for (var i = 0; i < appListScript.length; i++) {
                $("body").append('<script src="' + appListScript[i] + '"><\/script>');
            }

            @if( isset($session['TimeSession']) )
                var urlrenovar = "{{ route('Public.private.renovar') }}";
		
                tiemposession( {{ $session['TimeSession'] }} , {{ $session['tiempo'] }}, {{ $session['tiempodealerta'] }});
            @endif

            $(document).ready(function(){
                if(( typeof AppEstadoCuenta) == 'object'){
                    AppEstadoCuenta.urlDocPdf = "{{ route('api.resource.estado_cuenta') }}";
                }
                
                urlApiResourcePdf = "{{ route('api.resource.getPdfImg') }}";
                return ;
            });
        </script>
        <script>$('#chatwidget').chatwidget()</script>
        @stack('scripts')

    </body>
</html>
