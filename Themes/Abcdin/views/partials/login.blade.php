
<div class="col-md-12 col-lg-12">
    <div class="row bgGriss">
        <div class="acceso">
            <span>
                <p><i class="fas fa-sign-out-alt"></i> Tu Cuenta</p>
                {{-- <a href="#" >Recuperar clave</a> --}}
            </span>

            <form class="form-inline" action="">
                <div class="input-group">
                  <span class="etiqueta">RUT</span>
                  <input type="text" class="form-control" id="validarRut" placeholder="12.345.678-9" rel="popover" data-popover-content="#myPopover" data-placement="bottom">                      
                </div>
                <div class="input-group">
                    <span class="etiqueta">CLAVE</span>
                    <input type="password" class="form-control" id="validarClave" placeholder="*********" maxlength="4">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btninactivo" id="boton" type="button" onclick="AppValidacion.verificarDatos()" disabled="true">
                            <i class="far fa-arrow-alt-circle-right"></i>
                        </button>
                    </div>
                </div>
                <div class="msg-notifi notificacioneslogin">
                    <p id="login" class="d-none"><span class="rojo login">{{ trans('user::Autenticacion.rut no valido') }}</span></p>
                    <p id="fueraservicio" class="d-none"><span class="rojo fueraservicio">{{ trans('page::global.servicios no disponible') }}</span></p>
                    <p id="bloqueoduro" class="d-none"><span class="rojo bloqueoduro">{{ trans('user::Autenticacion.bloqueo duro') }}</span></p>
                    <p id="bloqueoblando" class="d-none"><span class="rojo bloqueoblando">{{ trans('user::Autenticacion.bloqueo blando') }}</span></p>
                </div>
            </form>
        </div>
    </div>
    <div class="olvidoClave hidden" >

        <div class="boxPaso1 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>Enviaremos una nueva clave de internet a su mail registrado</p>

            <div class="form-group">
            <input type="text" class="form-control mb-3" id="FormControlInput1" placeholder="Rut">
            <button class="btn btn-primary btn-block btnRojo" type="submit">ENVIAR NUEVA CLAVE</button>
            </div>
        </div>

        <div class="boxPaso2 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>Para obtener tu Clave Internet debes solicitarla en cualquiera de nuestras
            tiendas abcdin.</p>
            <a href="#">VER TIENDAS</a>  
        </div>

        <div class="boxPaso3 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>No es posible recuperar clave, debido a que no tienes un email registrado.
            Para solicitar el reinicio de tu clave puedes ir a cualquiera de nuestras tiendas
            abcdin</p>
            <a href="#">VER TIENDAS</a>  
        </div>

        <div class="boxPaso4 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>No tienes activada la clave de internet, para poder habilitarla debes ir a cualquiera
            de nuestras tiendas abcdin</p>
            <a href="#">VER TIENDAS</a>  
        </div>

        <div class="boxPaso5 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>Para recuperar tu clave de internet hemos enviado una clave provisoria al correo
            <span class="rojo">JES***732@gmail.com</span> y por SMS al <span class="rojo">XXXX 1234</span></p>

            <div class="form-group">
            <input type="text" class="form-control mb-3" id="FormControlInput11" placeholder="Ingresa tu clave provisoria">
            <button class="btn btn-primary btn-block btnRojo" type="submit">CONTINUAR</button>
            </div>
        </div>

        <div class="boxPaso6 hidden">
            <h1>¿Olvidó su clave?</h1>
            <p>Ingresa una nueva clave de internet</p>

            <div class="form-group">
                <input type="text" class="form-control mb-3" id="FormControlInput12" placeholder="Ingresa tu nueva contraseña">
                <input type="text" class="form-control mb-3" id="FormControlInput2" placeholder="Repite tu contraseña">
                <p><span class="rojo">Contraseña invalida o no coincide.<br>
                    Ingresa nuevamente tu clave.</span></p>
                <button class="btn btn-primary btn-block btnRojo" type="submit">CONTINUAR</button>
            </div>
        </div>

        <div class="boxPaso7 hidden">
            <h1><span class="rojo">¡Tu clave de internet ha sido actualizada!</span></h1>
            <p>Ya puedes utilizarla para compras o ingresar a Tu Cuenta</p>

        </div>

    </div>

</div> 

<div id="myPopover" class="hide">
    <h1>SELECCIONA TU TARJETA</h1>
    <ul class="list-group">
    </ul>
</div>

<div id="modalAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="{{ Theme::url('img/icono-alerta.png') }}">
                    <h2 class="mensajeAlerta"></h2>
                </div>
            </div>
        
        </div>
    </div>
</div>