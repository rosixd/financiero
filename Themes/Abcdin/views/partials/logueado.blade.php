<div class="col-md-12 col-lg-12">
    <div class="row bgGriss">
        <div class="acceso logeado">
            <span>
                @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                    <a href="{{ route('Public.private.ic.movimientos') }}" class="cerrar">
                @else
                    <a href="{{ route('Public.private.pagarcuenta') }}" class="cerrar">
                @endif
                        <i class="fas fa-sign-out-alt"></i> 
                        Tu Cuenta
                    </a>
            </span>
            <span>
                <a href="{{ route('Public.private.logout') }}" class="cerrar">
                    <i class="far fa-times-circle"></i> 
                    Cerrar Sesi&oacute;n
                </a>
            </span>
        </div>
    </div>
</div> 
