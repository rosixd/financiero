<!-- footer -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-4">
                        <div>
                            <img class="logoAbc-footer" src="{{ Theme::url('img/logotipo-abc-footer.png') }}" width="179" heigth="53">
                            <ul>
                                {!! Helper::menu(4) !!}
                            </ul>
                        </div>  
                    </div>
                    <div class="col-lg-4">
                        <div>
                            @if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('5')) )
                                <h3>{{ strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('5')['title']) }}</h3>
                            @endif
                            <ul>
                               
                                {!! Helper::menu(5) !!}
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div>
                            @if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('6')) )
                                <h3>{{ strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('6')['title']) }}</h3>
                            @endif
                            <ul>
                                {!! Helper::menu(6) !!}
                            </ul>
                            @if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('7')) )
                                <h3>{{ strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('7')['title']) }}</h3>
                            @endif
                            <ul>
                                @foreach( \Modules\Menu\Entities\Menu::BuscarMenu('7') as $menu )
                                    <li>
                                        <i class="fas fa-trophy"></i>
                                        <a href="{{ $menu['url'] }}" {!! isset($menu['target']) ? $menu["target"] : '' !!} {!! trim( $menu["class"] ) != '' ? ' class="' . $menu["class"] . '"' : '' !!}>
                                            {!! ( array_key_exists("icon", $menu ) && trim( $menu["icon"] ) != '' ? '<i class="' .$menu["icon"] . '"></i>' : '' ) !!} 
                                            {{ strtoupper($menu['title']) }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="float-left">
                    <a href="tel:6008302222" style="text-decoration: none;">
                        <h2>SERVICIO AL CLIENTE</h2>
                        <h1>600 830 22 22</h1>
                    </a>
                    <ul class="redes-footer">
                        <li>
                            <a href="https://www.facebook.com/TarjetaAbcvisa/" target="_blank">
                                <img src="{{ Theme::url('img/icono-facebook.png') }}">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ Theme::url('img/icono-correo.png') }}">
                            </a>
                        </li>
                    </ul> 
                </div>
                <img src="{{ Theme::url('img/tarjeta-sin-info.png') }}" class="tarjeta-footer">
                <div class="clearfix"></div>
                <div class="info_perdida">En caso de p&eacute;rdida, robo, hurto, adulteraci&oacute;n o falsificaci&oacute;n debes bloquear tu tarjeta 
                  llamando al <a href="#ModalEmergencias" style="text-decoration: none;" data-toggle="modal" ><strong>800 911 140</strong></a>, de lunes a domingo las 24 hrs. del día.
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col legales">
                <strong>Abcdin y Din - Copyright © 2018 - Todos los derechos reservados.</strong>
                <p>
                    Instituci&oacute;n Regulada por Superintendencia de Bancos e Instituciones Financieras Chile.<br>
                    Inf&oacute;rmese sobre las entidades autorizadas para emitir Tarjetas de Pago en el país, quienes se encuentran inscritas en 
                    los Registros de Emisores de Tarjetas que lleva la SBIF, en 
                    <a href="http://www.sbif.cl" target="_blank">www.sbif.cl</a>
                </p>
            </div>
        </div>
    </div>
</footer>
  <!-- fin footer -->
<!-- modal requisitos de tarjeta -->
<div id="ModalRequisitos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">  
</div>
<!-- modal bases legales -->
<div id="ModalBasesLegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
</div>
<!-- modal ver ganadores -->
<div id="ModalVerGanadores" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
</div>

<div id="ModalEmergencias" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente ContenidoSeguro">
                    <h3 class="modal-title" style="color: #B40D15;">Emergencias</h3><br>
                    <p style="font-size: 16px;">En caso de pérdida, robo, hurto, adulteración o falsificación debes bloquear tu tarjeta llamando al:</p>
                    <div class="row datosTabla">
                        <table class="table table-responsive mt-3">
                            <thead style="background-color: #d5012c;">
                                <tr>
                                    <th style="padding: 0.75rem;">
                                        <a href="tel:800911140" style="text-decoration: none; color:#ffffff;">
                                            <img src="{{ Theme::url('img/mini_telefono.png') }}" style="vertical-align: unset; margin-right: 5px;">
                                            <span style="text-decoration: none; color:#ffffff; font-size: 20px;">800 911 140</span>
                                        </a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="padding: 0.75rem; font-size: 14px; text-align: center;">Desde red Fija y Celulares. De lunes a domingo las 24 hrs. del día</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <p style="font-size: 16px;">También puedes acudir a una de nuestras tiendas abcdin o Dijon, a lo largo del país, y solicitar que bloqueen tu tarjeta.</p>
                </div>
            </div>
        </div>
    </div>
</div>