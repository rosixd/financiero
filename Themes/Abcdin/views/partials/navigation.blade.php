<!-- header -->

<header class="header" id="header">
    <div class="header-top container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="info-servicio-top float-left negrita">
                    <img src="{{ Theme::url('img/icono-servicios-top.png') }}"  width="29" height="27">
                    <span>
                        <a href="tel:6008302222" style="text-decoration: none;">
                            <p>servicio al cliente</p>
                            <p class="info-fono">600 830 22 22</p>
                        </a>
                    </span>
                </div>
                <ul class="link-tiendas-top float-right">
                    <li><a href="https://www.dijon.cl" target="_blank">Dijon.cl</a></li>
                    <li><a href="https://www.abcdin.cl" target="_blank">abcdin.cl</a></li>
                </ul>
            </div>

            <div class="col-md-12 col-lg-4">
                <div class="row login-top">
                    {{--<div class="col-md-12 col-lg-4">
                        <div class="row">
                            <div>
                                 <a class="bloqueo negrita" href="#">
                                    <i class="fas fa-lock"></i> 
                                    bloquear mi tarjeta
                                </a> 
                            </div>
                        </div>
                    </div>--}}
                    @if (isset($session['Logueado']) && $session['Logueado'] === 'true')
                        @include('partials.logueado')
                    @else
                        @include('partials.login')
                    @endif
                </div>
            </div>
        </div>
     </div>

    <nav class="navbar navbar-expand-xl">
        <a class="navbar-brand pl-4" href="{{ route("homepage") }}">
            <img class="marca-top" src="{{ Theme::url('img/logotipo-abc-nav-top.png') }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMobile" aria-controls="navbarsMobile" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-align-justify"></i></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsMobile">
            <ul class="navbar-nav mr-auto">
                {!! Helper::menu( 3 ) !!}	    	 
            </ul>
        </div>
    </nav>
</header>
<!-- fin navegacion -->
