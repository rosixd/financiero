@extends('layouts.master')

@section('title')
    {{ $page->title }}
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('metadatos')
	@php

		$metasGlobales = \Helper::metadatosGlobales();

		$metadatos = $page->Metadato()
			->getResults(); 
		
		$tagsMetadatos = '' . $metasGlobales["tags"];

		if( $metadatos->count() > 0 ){
			foreach( $metadatos as $indice => $metadato ){
				
				if( !in_array( $metadato->ABC04_id, $metasGlobales["ids"] ) ){
					$tipoMetadato = $metadato
						->tipometadato()
						->getResults();

					if( $tipoMetadato->count() > 0 ){
						$tagsMetadatos .= '<meta name="' . $tipoMetadato->ABC05_nombre . '" ' . 
							$tipoMetadato->ABC05_attributos . '="' . $metadato->ABC04_contenido . '" >';
					}
				}
			}
		}
	@endphp
	
	{!! $tagsMetadatos !!}
@stop

@section('content')
        {!! $page->body !!}
	@section('components.slider')
       		@ 
    	@endsection
@stop