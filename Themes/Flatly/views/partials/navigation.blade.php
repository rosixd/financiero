<nav class="navbar navbar-default @guest navbar-fixed-top @endguest" style="@auth margin-top: -52px @endauth">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">@setting('core::site-name')</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            @menu('main')
            <div class="nav navbar-nav navbar-right">
                {!! Form::open(['route' => ['Public.private.login'], 'method' => 'post','class' => 'form-inline formValida', 'autocomplete'=>"off"]) !!}
                    <div class="form-group">
                        <label class="sr-only" for="Rut">RUT</label>
                        <input id="Rut" name="rut" class="form-control validaRut requerido" type="text" placeholder="RUT" value="" >
                        <br/>
                        <span class="text-danger"  id="msjrut" style="display: none;">
                           Rut no valido.
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="clave">Clave</label>
                        <input type="password" name ="password" class="form-control requerido" id="password" placeholder="Clave" value="" >
                        <br/>
                        <span class="text-danger" style="display: none;">
                        	Campo Requerido
                        </span>
                    </div>  
                    <button type="submit" class="btn btn-default">Sign in</button>
               {!! Form::close() !!}
            </div>
        </div>
    </div>
</nav>
