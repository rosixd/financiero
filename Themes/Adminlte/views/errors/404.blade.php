<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>404 abcvisa</title>

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/style.css" />

</head>

<body>

	<div id="">
		<div class="divcontent">
			<h2 style="margin-top: 40px; text-align: center;">ERROR - 404</h2>
			<h1>UPS! AQUÍ NO HAY NADA :(</h1>
			<h2 class="h2perdido" style="text-align: center;">No hemos podido encontrar la página que estás buscando</h2>
			<div style="text-align: center;">
				<a class="atarjeta" href="https://www.abcserviciosfinancieros.cl/"><img  alt="Tarjeta abcvisa" style="margin-left: 10px; margin-right: 10px;" src="img/Tarjeta_404.png" ></a>
			</div>
			<p class="pparrafo" >Pero aquí tienes algunas páginas que te podrían interesar:</p>
			<ul class="ullista" style="list-style-type:none;">
				<li><a href="https://www.abcserviciosfinancieros.cl/tarjeta-abcvisa">Tarjeta abcvisa</a></li>
				<li><a href="https://www.abcserviciosfinancieros.cl/seguros">Seguros</a></li>
				<li><a href="https://www.abcserviciosfinancieros.cl/beneficios-abcvisa">Beneficios</a></li>
				<li><a href="https://www.abcserviciosfinancieros.cl/garantia">Garantía Extendida</a></li>
				<li><a href="https://www.abcserviciosfinancieros.cl/paga-tu-cuenta">Paga tu cuenta</a></li>
				<li><a href="https://www.abcserviciosfinancieros.cl/asesoria-financiera">Asesoría financiera</a></li>
			</ul>
		</div>
	</div>

</body>

</html>
