@if (Session::has('success'))
    @push('notificaciones')
        <script>
            AppGeneral.notificaciones("success",  "{{ Session::get('success') }}" );
        </script>   
    @endpush

{{--      <div class="alert alert-success fade in alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('success') }}
    </div>  --}}
@endif

@if (Session::has('error'))
    @push('notificaciones')
        <script>
            AppGeneral.notificaciones("error",  "{{ Session::get('error') }}");
        </script>   
    @endpush

{{--      <div class="alert alert-danger fade in alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('error') }}
    </div>  --}}
@endif

@if (Session::has('warning'))
    @push('notificaciones')
        <script>
            AppGeneral.notificaciones("warning",  "{{ Session::get('warning') }}");
        </script>   
    @endpush
{{--      <div class="alert alert-warning fade in alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ Session::get('warning') }}
    </div>  --}}
@endif
