<?php

if (function_exists('current_permission_value') === false) {
    function current_permission_value($model, $permissionTitle, $permissionAction)
    {
        $value = array_get($model->permissions, "$permissionTitle.$permissionAction");
        if ($value === true) {
            return 1;
        }
        if ($value === false) {
            return -1;
        }

        return 0;
    }
}
function permisos_mostrar( $permiso ){
    return 'Permiso: ' . $permiso; 
}

/**
 * 
 */
function mostrar_permiso( $permiso ){

    if( preg_match( "/^(core|dashboard|workshop|account\.api\-keys)/i" , $permiso ) ){
        return false;
    }

    return true;
}

function permiso_activo_modulo( $modelo ){
    
    if( preg_match( "/^(core|dashboard|workshop|account\.api\-keys)/i" , $modelo ) ){
        return 1;    
    }

    return false;
}