<?php
// app/Models/Auth/User.php
namespace Modules\User\Entities\PublicUser;

use Modules\User\Entities\Sentinel\User;

class PublicUser extends User
{
    protected $table = 'ABC08_PublicUser';
    protected $fillable = [
        'email',
        'password',
        'Offset',
        'Token'
    ];
    
    /**
     * The Eloquent persistences model name.
     *
     * @var string
     */
    protected static $persistencesModel = 'Modules\User\Entities\PublicUser\EloquentPersistence';

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }
}
