<?php

namespace Modules\User\Entities\PublicUser;

use Illuminate\Database\Eloquent\Model;

class EloquentPersistence extends Model implements PersistenceInterface
{
    protected $table = 'persistences_public';

    /**
     * The users model name.
     *
     * @var string
     */
    protected static $usersModel = 'Modules\User\Entities\PublicUser\PublicUser';
}
