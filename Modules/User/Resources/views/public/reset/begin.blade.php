@extends('layouts.account')

@section('title')
    {{ trans('user::auth.reset password') }} | @parent
@stop

@section('content')
    <div class="login-logo">
        <a href="{{ url('/') }}">{{ setting('core::site-name') }}</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">{{ trans('user::auth.to reset password complete this form') }}</p>
        @include('partials.notifications')

        {!! Form::open(['route' => 'reset.post']) !!}
            <div class="form-group has-feedback">
                <input type="text" class="form-control" autofocus
                       name="email" placeholder="{{ trans('user::auth.email') }}" value="{{ old('email')}}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		        <span class="text-danger">
                    {!! $errors->first('email', ':message') !!}
                    {{--  {{ $errors->has('email') ? trans('user::auth.validation.emails') : '' }}  --}}
                </span>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-md pull-right">
                        {{ trans('user::auth.resetpassword') }}
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
	<br>
        <a href="{{ route('login') }}" class="text-center">{{ trans('user::auth.Iremembered my password') }}</a>
    </div>
@stop
