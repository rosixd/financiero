@extends('layouts.account')

@section('title')
    {{ trans('user::auth.resetpassword') }} | @parent
@stop

@section('content')
    <div class="login-logo">
        <a href="{{ url('/') }}">{{ setting('core::site-name') }}</a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">{{ trans('user::auth.resetpassword') }}</p>
        @include('partials.notifications')

        {!! Form::open() !!}
        <div class="alert alert-dismissible alert-danger" style="display: {{ $errors->first('password') == 's' ? 'block' : 'none' }}">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
            Sr. Usuario, las contrase&ntilde;as no coinciden, por favor ingr&eacute;selas nuevamente.
		</div>
       

        <div class="form-group has-feedback">
            <input type="password" class="form-control alfanumerico" autofocus
                   name="password" placeholder="{{ trans('user::auth.password') }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <span class="text-danger">
                {{--  {!! $errors->first('password', ':message') !!}  --}}
                {{ $errors->first('password') != 's' ? $errors->first('password', ':message') : '' }}
            </span>
        </div>
        <div class="form-group has-feedback ">
            <input type="password" name="password_confirmation" class="form-control alfanumerico" placeholder="{{ trans('user::auth.passwordconfirmation') }}">
           <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
           <span class="text-danger">
                {!! $errors->first('password_confirmation', ':message') !!}
               {{--   {{ $errors->has('password_confirmation') ? trans('user::auth.validation.confirmcomplete') : '' }}  --}}
            </span>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-md pull-right">
                    Actualizar{{-- {{ trans('user::auth.resetpassword') }} --}}
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
