@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('user::users.title.users') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('user::users.breadcrumb.users') }}</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
				@if ($currentUser->hasAccess('user.users.create'))
                <a href="{{ URL::route('admin.user.user.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                    <i class="fa fa-pencil"></i> {{ trans('user::users.button.new-user') }}
				</a>
				@endif
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive" style="width: 100%;">
					<table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 3, "desc" ]]' style="width: 100%;">
						<thead>
							<tr>
								
								<th>{{ trans('user::users.table.first-name') }}</th>
								<th>{{ trans('user::users.table.last-name') }}</th>
								<th>{{ trans('user::users.table.email') }}</th>
								<th>{{ trans('user::users.table.created-at') }}</th>
								<th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					<!--	<tfoot>
							<tr>
								<th>{{ trans('user::users.table.first-name') }}</th>
								<th>{{ trans('user::users.table.last-name') }}</th>
								<th>{{ trans('user::users.table.email') }}</th>
								<th>{{ trans('user::users.table.created-at') }}</th>
								<th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
							</tr>
						</tfoot> -->
                </table>
            <!-- /.box-body -->
            </div>
			</div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
@include('core::modalLog')
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('user.users.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('user.users.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('user.users.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('user.users.index') ? 'true' : 'false' }}
    };
</script>
@endprepend
