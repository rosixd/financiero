@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('user::roles.title.roles') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('user::roles.breadcrumb.roles') }}</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
				@if ($currentUser->hasAccess('user.roles.create'))
                <a href="{{ URL::route('admin.user.role.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                    <i class="fa fa-pencil"></i> {{ trans('user::roles.button.new-role') }}
                </a>
				@endif
            </div>
        </div>
        <div class="box box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 1, "desc" ]]'>
                    <thead>
                        <tr>
                            
                            <th>{{ trans('user::roles.table.name') }}</th>
                            <th>{{ trans('user::users.table.created-at') }}</th>
                            <th data-sortable="false">{{ trans('user::users.table.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <!--<tfoot>
                        <tr>
                            
                            <th>{{ trans('user::roles.table.name') }}</th>
                            <th>{{ trans('user::users.table.created-at') }}</th>
                            <th>{{ trans('user::users.table.actions') }}</th>
                        </tr>
                    </tfoot> -->
                </table>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
@include('core::partials.delete-modal')
@include('core::modalLog')
@stop

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('user.roles.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('user.roles.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('user.roles.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('user.roles.index') ? 'true' : 'false' }}
    };
</script>
@endprepend
