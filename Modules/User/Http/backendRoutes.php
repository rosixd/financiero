<?php

use Illuminate\Routing\Router;

use Modules\User\Repositories\Eloquent\EloquentUserTokenRepository;
use Modules\User\Entities\UserToken;
use Ramsey\Uuid\Uuid;

/** @var Router $router */
$router->group(['prefix' => '/user'], function (Router $router) {

    $router->get('newAccessToken', function(){
    	try{
		$uuid4 = Uuid::uuid4();
	$user = \Auth::user();
	$User = UserToken::where('user_id', $user->id )
		->first();
	
	if( !$User ){
		$User = UserToken::create([
			'user_id' => $user->id, 
			'access_token' => $uuid4	
		]);	
	}
//	dd( $User->generateFor( $user->id ) );
//	$EloquentUserTokenRepository = new UserToken;
//	$EloquentUserTokenRepository->generateFor( $user->id );
	//dd( $user );
        //$token = new AccessToken($user->id);
	
	echo "token: ";
	dd( $User );
	}catch( Exception $e ){
		echo "Error: ";
		dd( $e );
	}
    });

    $router->get('users', [
        'as' => 'admin.user.user.index',
        'uses' => 'UserController@index',
        'middleware' => 'can:user.users.index',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'usuario',
        'datatable' => 'admin.user.user.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('users/create', [
        'as' => 'admin.user.user.create',
        'uses' => 'UserController@create',
        'middleware' => 'can:user.users.create',
        'script' => 'AppAdministracion',
        'view' => 'create',
        'modulo' => 'administracion',
        'submodulo' => 'usuario',
        'datatable' => 'admin.user.user.datatable'
    ]);
    $router->post('users', [
        'as' => 'admin.user.user.store',
        'middleware' => 'can:user.users.create',
        'uses' => 'UserController@store',
    ]);
    $router->get('users/{users}/edit', [
        'as' => 'admin.user.user.edit',
        'uses' => 'UserController@edit',
        'middleware' => 'can:user.users.edit',
        'script' => 'AppAdministracion',
        'view' => 'edit',
        'modulo' => 'administracion',
        'submodulo' => 'usuario',
        'datatable' => 'admin.user.user.datatable'

    ]);
    $router->put('users/{users}/edit', [
        'as' => 'admin.user.user.update',
        'middleware' => 'can:user.users.edit',
        'uses' => 'UserController@update',
    ]);
    $router->get('users/{users}/sendResetPassword', [
        'as' => 'admin.user.user.sendResetPassword',
        'uses' => 'UserController@sendResetPassword',
        'middleware' => 'can:user.users.index',
    ]);
    $router->get('users/destroy/{users}', [
        'as' => 'admin.user.user.destroy',
        'uses' => 'UserController@destroy',
        'middleware' => 'can:user.users.destroy',
    ]);

    $router->get('users/datatable', [
        'as' => 'admin.user.user.datatable',
        'uses' => 'UserController@datatable',
        'middleware' => 'can:user.users.index',
    ]);

    $router->get('users/logdatatable', [
        'as' => 'admin.user.user.logdatatable',
        'uses' => 'UserController@logdatatable',
        'middleware' => 'can:user.users.index',
    ]);

    $router->get('roles', [
        'as' => 'admin.user.role.index',
        'uses' => 'RolesController@index',
        'middleware' => 'can:user.roles.index',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'roles',
        'datatable' => 'admin.role.roles.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('roles/create', [
        'as' => 'admin.user.role.create',
        'uses' => 'RolesController@create',
        'middleware' => 'can:user.roles.create',
        'script' => 'AppAdministracion',
        'view' => 'create',
        'modulo' => 'administracion',
        'submodulo' => 'roles',
        'datatable' => 'admin.role.roles.datatable'
    ]);
    $router->post('roles', [
        'as' => 'admin.user.role.store',
        'uses' => 'RolesController@store',
        'middleware' => 'can:user.roles.create',
    ]);
    $router->get('roles/{roles}/edit', [
        'as' => 'admin.user.role.edit',
        'uses' => 'RolesController@edit',
        'middleware' => 'can:user.roles.edit',
        'script' => 'AppAdministracion',
        'view' => 'edit',
        'modulo' => 'administracion',
        'submodulo' => 'roles',
        'datatable' => 'admin.role.roles.datatable'
    ]);
    $router->put('roles/{roles}/edit', [
        'as' => 'admin.user.role.update',
        'uses' => 'RolesController@update',
        'middleware' => 'can:user.roles.edit',
    ]);
    $router->get('roles/destroy/{roles}', [
        'as' => 'admin.user.role.destroy',
        'uses' => 'RolesController@destroy',
        'middleware' => 'can:user.roles.destroy',
    ]);

    $router->get('roles/datatable', [
        'as' => 'admin.role.roles.datatable',
        'uses' => 'RolesController@datatable',
        'middleware' => 'can:user.roles.index',
    ]);
});

$router->group(['prefix' => '/account'], function (Router $router) {
    $router->get('profile', [
        'as' => 'admin.account.profile.edit',
        'uses' => 'Account\ProfileController@edit',
        
    ]);
    $router->put('profile', [
        'as' => 'admin.account.profile.update',
        'uses' => 'Account\ProfileController@update',
    ]);
    $router->bind('userTokenId', function ($id) {
        return app(\Modules\User\Repositories\UserTokenRepository::class)->find($id);
    });
    $router->get('api-keys', [
        'as' => 'admin.account.api.index',
        'uses' => 'Account\ApiKeysController@index',
    ]);
    $router->get('api-keys/create', [
        'as' => 'admin.account.api.create',
        'uses' => 'Account\ApiKeysController@create',
    ]);
    $router->delete('api-keys/{userTokenId}', [
        'as' => 'admin.account.api.destroy',
        'uses' => 'Account\ApiKeysController@destroy',
    ]);
});
