<?php
namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use GuzzleHttp\Client;
use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use SoapClient;
use Storage;
use Spatie\PdfToImage\Pdf;

class ResourceController 
{

   /**
     * Devuelve imagenes a partir de la url de un pdf 
     * 
     * @autor Dickson Morales
     * @param Request
     * @return Json
     */
    public function getPdfImg( Request $request ){

        //Estado de la respuesta ajax
        $estadoRespuesta = 200;

        $resultado = [];

        if( !$request->has("pdf") ){
            return Response::json([
                "error" => true,
                "messaje" => "Pdf no disponible"
            ]);
        }

        $url = env('APP_URL'). $request->get("pdf");
        $sepUrl = explode("/", $url );
        $doc = last( $sepUrl );

        try{
                    //Estableciendo conexion con la url del doc
            $client = new Client (); 
            $requestAbcDin = $client
                ->get( $url );

            //Si no existe la carpeta pdfs la crea
            if( count(Storage::disk("public")->directories("pdfs")) == 0 ){
                Storage::disk("public")->makeDirectory("pdfs");
            }

            //Si no existe la carpeta pdfs la crea
            if( count(Storage::disk("public")->directories('pdfs/garantias')) == 0 ){
                Storage::disk("public")->makeDirectory("pdfs/garantias");
            }

            //Obteniendo el path de eecc
            $pathGarantias = Storage::disk("garantias")
                ->getDriver()
                ->getAdapter()
                ->getPathPrefix();
            
            //Guardando archivo pdf 
            $client->request('GET', $url, [
                'save_to' => $pathGarantias. $doc
            ]);

            //transformando pdf a imagen png
            $carpetaPdf = preg_replace("/\.pdf/i", "", $doc );
            $imagen = $carpetaPdf . ".png"; //cambiando la extencion del archivo
            
            //Si no existe la carpeta del pdf la crea
            if( count(Storage::disk("garantias")->directories( $carpetaPdf )) == 0 ){
                Storage::disk("garantias")->makeDirectory( $carpetaPdf );
            }

            //buscando la lista de archivos y eliminando 
            if( count( Storage::disk("garantias")->allFiles( $carpetaPdf ) ) > 0 ){
                foreach( Storage::disk("garantias")->allFiles( $carpetaPdf ) as $indice => $pathImg ){
                    unlink( $pathGarantias . $pathImg ); 
                }
            }

            $pathDirectorio = $pathGarantias . $carpetaPdf;

            //comenzando la transformacion de pdf a img
            $pdfToImage = new Pdf( $pathGarantias. $doc );

            if( $pdfToImage->getNumberOfPages() > 0 ){
                for( $indice = 1; $indice <= $pdfToImage->getNumberOfPages() ; $indice++ ){
                    $pdfToImage->setPage( $indice )
                        ->saveImage( $pathDirectorio );
                }
            }
            // Fin de la transformacion

            //Devolviendo respuesta y nombre del documento
            $resultado = [
                "doc" => $doc,
                "urlDoc" => Storage::disk("garantias")->url( $doc ),
                'img' => Storage::disk("garantias")->url( $imagen ),
                "error" => false
            ];

            //buscando la lista de archivos y juntando las url de cada uno de ellos 
            if( count( Storage::disk("garantias")->allFiles( $carpetaPdf ) ) > 0 ){
                foreach( Storage::disk("garantias")->allFiles( $carpetaPdf ) as $indice => $pathImg ){
                    $resultado["archivos"][] = Storage::disk("garantias")->url( $pathImg  ); 
                }
            }
        }catch( \Exception $e ){
            $estadoRespuesta = 200;

            $resultado = [
                "error" => true,
                "message" => "Error en la comunicaci&oacute;n de servicios",
                "exception" => $e
            ];
        }
        
        //Devolviendo respuesta
        return Response::json( $resultado, $estadoRespuesta );
    }

    public function test_miliways(){
        $url = 'http://192.168.50.254:9090/Milliways/index.jsp';

         //Estableciendo conexion con la url del doc
         $client = new Client (); 
         $requestAbcDin = $client
             ->get( $url );

         $respuesta = $requestAbcDin
             ->getBody()
             ->getContents();

        dd( $respuesta );
    }
}