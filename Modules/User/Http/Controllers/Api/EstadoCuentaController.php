<?php
namespace Modules\User\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use GuzzleHttp\Client;
use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use SoapClient;
use Storage;
use Spatie\PdfToImage\Pdf;

class EstadoCuentaController 
{

   /**
     * Devuelve la url del documento pdf a mostrar 
     * 
     * @autor Dickson Morales
     * @param Request
     * @return Json
     */
    public function req05( Request $request ){

        //Estado de la respuesta ajax
        $estadoRespuesta = 200;

        $resultado = [];

        //Eliminando url antes de las variables get
        $url = preg_replace("/.*\?/i", "" , $request->get("t") );
        $ticket = preg_replace("/.*ticket\=/i", "", $url ); //obteniendo la variable ticket
        $doc = $request->get("doc");        
        
        //Url base de los documentos pdf
        //$urlBase = "http://abcdin.jp2.cl:8091/ext/JRDNSLN01PR01/XMLHttpRequest.aspx?";
        $urlBase = "https://abcdin.jp2.cl/JRDNSLN01PR01/XMLHttpRequest.aspx?";
        
        //$url = $urlBase . "Funcion=VT&T=" . $ticket;
        $url = $urlBase . "Funcion=VD&T=" . $doc;

        try{
            //Estableciendo conexion con la url del doc
            $client = new Client (); 
            $requestAbcDin = $client
                ->get( $url );

            $respuesta = $requestAbcDin
                ->getBody()
                ->getContents();
            
            //Si no existe la carpeta pdfs la crea
            if( count(Storage::disk("public")->directories("pdfs")) == 0 ){
                Storage::disk("public")->makeDirectory("pdfs");
            }

            //Si no existe la carpeta pdfs la crea
            if( count(Storage::disk("public")->directories('pdfs/eecc')) == 0 ){
                Storage::disk("public")->makeDirectory("pdfs/eecc");
            }
            
            //Obteniendo el path de eecc
            $pathEecc = Storage::disk("eecc")
                ->getDriver()
                ->getAdapter()
                ->getPathPrefix();

            //Guardando pdf
            $cliente = new Client;
            //Guardando archivo pdf 
            $cliente->request('GET', $respuesta, [
                'save_to' => $pathEecc. $doc
            ]);

            return [
                "url" => url("storage/pdfs/eecc/" . $doc)
            ];
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //transformando pdf a imagen png
            $carpetaPdf = preg_replace("/\.pdf/i", "", $doc );
            $directorioPdf = 'pdfs/eecc' . $carpetaPdf;
            $imagen = $carpetaPdf . ".png"; //cambiando la extencion del archivo
            
            //Si no existe la carpeta del pdf la crea
            if( count(Storage::disk("eecc")->directories( $carpetaPdf )) == 0 ){
                Storage::disk("eecc")->makeDirectory( $carpetaPdf );
            }
            
            //buscando la lista de archivos y eliminando 
            if( count( Storage::disk("eecc")->allFiles( $carpetaPdf ) ) > 0 ){
                foreach( Storage::disk("eecc")->allFiles( $carpetaPdf ) as $indice => $pathImg ){
                    unlink( $pathEecc . $pathImg ); 
                }
            }

            $pathDirectorio = $pathEecc . $carpetaPdf;

            //comenzando la transformacion de pdf a img
            $pdfToImage = new Pdf( $pathEecc. $doc );

            if( $pdfToImage->getNumberOfPages() > 0 ){
                for( $indice = 1; $indice <= $pdfToImage->getNumberOfPages() ; $indice++ ){
                    $pdfToImage->setPage( $indice )
                        ->saveImage( $pathDirectorio );
                }
            }
            // Fin de la transformacion
            
            //Devolviendo respuesta y nombre del documento
            $resultado = [
                "doc" => $doc,
                "urlDoc" => Storage::disk("eecc")->url( $doc ),
                'img' => Storage::disk("eecc")->url( $imagen ),
                "error" => false
            ];

            //buscando la lista de archivos y juntando las url de cada uno de ellos 
            if( count( Storage::disk("eecc")->allFiles( $carpetaPdf ) ) > 0 ){
                foreach( Storage::disk("eecc")->allFiles( $carpetaPdf ) as $indice => $pathImg ){
                    $resultado["archivos"][] = Storage::disk("eecc")->url( $pathImg  ); 

                    //estableciendo permiso publico al archivo
                    if( Storage::exists( ( $pathEecc . $pathImg ) ) ){
                        Storage::setVisibility( ( $pathEecc . $pathImg ) , 'public');
                    }
                }
            }

        }catch( \Exception $e ){
            $estadoRespuesta = 500;

            $resultado = [
                "error" => true,
                "message" => "Error en la comunicaci&oacute;n de servicios",
                "exception" => $e
            ];
        }
        
        //Devolviendo respuesta
        return Response::json( $resultado, $estadoRespuesta );
    }

    public function test_miliways(){
        $url = 'http://192.168.50.254:9090/Milliways/index.jsp';

         //Estableciendo conexion con la url del doc
         $client = new Client (); 
         $requestAbcDin = $client
             ->get( $url );

         $respuesta = $requestAbcDin
             ->getBody()
             ->getContents();

        dd( $respuesta );
    }
}