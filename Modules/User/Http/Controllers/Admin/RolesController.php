<?php

namespace Modules\User\Http\Controllers\Admin;

use Cartalyst\Sentinel\Roles\EloquentRole;
use Modules\User\Http\Requests\RolesRequest;
use Modules\User\Http\Requests\UpdateRoleRequest;
use Modules\User\Permissions\PermissionManager;
use Modules\User\Repositories\RoleRepository;
use DB;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
class RolesController extends BaseUserModuleController
{
    /**
     * @var RoleRepository
     */
    private $role;

    public function __construct(PermissionManager $permissions, RoleRepository $role)
    {
        parent::__construct();

        $this->permissions = $permissions;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('user::admin.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('user::admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RolesRequest $request
     * @return Response
     */
    public function store(RolesRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);

        $this->role->create($data);

        return redirect()->route('admin.user.role.index')
            ->withSuccess(trans('user::messages.role created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        if (!$role = $this->role->find($id)) {
            return redirect()->route('admin.user.role.index')
                ->withError(trans('user::messages.role not found'));
        }

        return view('user::admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int          $id
     * @param  RolesRequest $request
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);

        /*$this->role->fill($request->permissions);
        dd( $this->role );
        if(!$this->role->isDirty()){
            return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.updated sin'));
        }*/
        
        
        $this->role->update($id, $data);

        return redirect()->route('admin.user.role.index')
            ->withSuccess(trans('user::messages.role updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    { 
        try {
		
            $rol = EloquentRole::withTrashed()->find($id);
            $deleted_at = $rol->deleted_at;

            $json = null;
            $pivot =  DB::table('role_users')->where('role_id', $id)->get();
                $asignado = false;
            if(count($pivot) > 0){
                $asignado = true;
            }
            //$asignado = (is_null($deleted_at) && Poliza::with('seguro')->find($poliza->ABC16_id)->seguro)? true:false;

            if($asignado){

                $json = array(
                    'asignado' => $asignado,
                    'msn' => trans('user::roles.messages.resource assigned')
                );

            }else{

                $success  = is_null($deleted_at)? $rol->delete():$rol->restore();
            
                $json = array(
                    'success' => $success? true:false,
                    'msn' => is_null($deleted_at)? trans('user::roles.messages.rol desactivate'):trans('user::roles.messages.rol activate'),
                    'status' => is_null($deleted_at)? true:false
                );
            }
        } catch (Exception $e) {
            $json = array(
                'asignado' => false,
                'msn' => trans('user::roles.messages.resource error')
            );
        }catch (PDOException $e) {
            $json = array(
                'asignado' => false,
                'msn' => trans('user::roles.messages.resource error')
            );
        }

        return response()->json($json);
    }

    public function datatable()
    {
        $role = EloquentRole::withTrashed();

        return Datatables::of($role)
        ->editColumn('created_at', function($role) {
            return Carbon::parse($role->created_at)->format('d/m/Y H:i:s');
        })
        ->addColumn('ruta_edit', function ($role) {
            return route("admin.user.role.edit",[$role->id]);
        })
        ->addColumn('ruta_destroy', function($role){
            return route("admin.user.role.destroy",[$role->id]);
        })
        ->addColumn('inactivo', function ($role) {
            return is_null($role->deleted_at)? false:true;
        })
        ->make(true);
    }
}
