<?php

namespace Modules\User\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Contracts\Authentication;
use Modules\User\Events\UserHasBegunResetProcess;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;
use Modules\User\Permissions\PermissionManager;
use Modules\User\Repositories\RoleRepository;
use Modules\User\Repositories\UserRepository;
use Modules\User\Entities\Sentinel\User;
use DB;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Models\Audit;
use Jenssegers\Agent\Agent;
use Modules\Media\Entities\FileTranslation;

class UserController extends BaseUserModuleController
{
    /**
     * @var UserRepository
     */
    private $user;
    /**
     * @var RoleRepository
     */
    private $role;
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param PermissionManager $permissions
     * @param UserRepository    $user
     * @param RoleRepository    $role
     * @param Authentication    $auth
     */
    public function __construct( 
        PermissionManager $permissions,
        UserRepository $user,
        RoleRepository $role,
        //$role = '',
        Authentication $auth
    ) {
        parent::__construct();

        $this->permissions = $permissions;
        $this->user = $user;
        $this->role = $role;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('user::admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = $this->role->all();

        return view('user::admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUserRequest $request
     * @return Response
     */
    public function store(CreateUserRequest $request, Authentication $auth)
    {
        $data = $this->mergeRequestWithPermissions($request);

        $this->user->createWithRoles($data, $request->roles, false);

        return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.user created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit( Request $request )
    {
        $roles = $this->role->all();
        $user = User::find( $request->users );
        return view('user::admin.users.edit',compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int               $id
     * @param  UpdateUserRequest $request
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    { 
    
        $data = $this->mergeRequestWithPermissions($request);
/*         dd($data);
        $data->fill($request->roles);
        
        if(!$id->isDirty()){
            return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.updated sin'));
        }  */

        $this->user->updateAndSyncRoles($id, $data, $request->roles);

       // if ($request->get('button') === 'index') {
            return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.user updated'));
       // }

        return redirect()->back()
        ->withSuccess(trans('user::messages.user updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {

        $user = User::withTrashed()->find($id);
        $deleted_at = $user->deleted_at;

        $success  = is_null($deleted_at)? $user->delete():$user->restore();
		
        return response()->json(array(
            'success' => $success? true:false,
            'msn' => is_null($deleted_at)? trans('user::users.messages.user desactivate'):trans('user::users.messages.user activate'),
            'status' => is_null($deleted_at)? true:false
        ));
    }

    public function sendResetPassword($user, Authentication $auth)
    {
        $user = $this->user->find($user);
        $code = $auth->createReminderCode($user);

        event(new UserHasBegunResetProcess($user, $code));

        return redirect()->route('admin.user.user.edit', $user->id)
        ->withSuccess(trans('user::auth.reset password email was sent'));
    }  

    /**
     * @author Ruben Morales
     *
     * @param  \Illuminate\Http\Request  $request  The request
     *
     * @return Data Usuarios
     */

    public function datatable(Request $request)
    {
        $user = User::withTrashed();
        
        $auth = app(\Modules\User\Contracts\Authentication::class);
        
        return Datatables::of($user)
        ->editColumn('created_at', function($user) {
            return Carbon::parse($user->created_at)->format('d/m/Y H:i:s');
        })
        ->addColumn('ruta_edit', function ($user) {
            return route("admin.user.user.edit",[$user->id]);
        })        
        ->addColumn('ruta_destroy', function($user){
            return route("admin.user.user.destroy",[$user->id]);
        })
        ->addColumn('inactivo', function ($user) {
            return is_null($user->deleted_at)? false:true;
        })
        ->addColumn('user', function ($user) use ($auth) {
            return ($auth->user()->id == $user->id)? true:false;
        })->make(true);
    }

    /**
     * @author Ruben Morales
     *
     * @param  \Illuminate\Http\Request  $request  The request
     *
     * @return Data Table Log
     */

    public function logdatatable(Request $request)
    {
        $audit = Audit::where([
            [ 'auditable_id', "=", $request->audit_id ],
            [ 'key_model', "like", $request->key_model ] 
        ]);
            
        return Datatables::of($audit) 
            ->addColumn('user', function ($audit) {
                
                if($audit->user_id == ''){
                    return ''; 
                } 

                $user = User::where('id', $audit->user_id) 
                    ->withTrashed() 
                    ->first();
                
                if( !$user ){
                    return ""; 
                } 

                return $user->first_name." ".$user->last_name; 
            }) 
            ->addColumn('browser_name', function ($audit) {
                $agent = new Agent(); 
                $agent->setUserAgent($audit->user_agent); 

                return $agent->browser();
            }) 
            ->addColumn('browser_version', function ($audit) {
                $agent = new Agent();
                $agent->setUserAgent($audit->user_agent);
                $browser = $agent->browser();
                
                return $agent->version($browser);
            }) 
            ->addColumn('file_translation', function($audit){
                if(
                    $audit->key_model == "archivo multimedia" && 
                    html_entity_decode($audit->event) == "Actualización"
                ){
                    $filteTranslation = FileTranslation::where('file_id', $audit->auditable_id)
                        ->first(); 

                    if( !$filteTranslation ){
                        return ""; 
                    }

                    $audit_fileTranslation = Audit::where([
                        [ 'auditable_id', "=", $filteTranslation->id ],
                        [ 'key_model', "like", "FileTranslation" ]
                    ])->first(); 

                    if( !$audit_fileTranslation ){
                        return ""; 
                    } 

                    return !is_null($audit_fileTranslation)? $audit_fileTranslation->old_values:""; 
                }else{
                    return ""; 
                } 
            })->make(true); 
    }
}
