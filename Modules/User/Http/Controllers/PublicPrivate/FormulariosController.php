<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Storage;
use Illuminate\Http\Response;
use Modules\Formularios\Entities\Formularios;
use Modules\Formularios\Entities\Controltxtftp;
use Modules\Formularios\Entities\Controltxthistorial;
use DB;

class FormulariosController
{

    public function index()
    {   
        $_formularios = Config::get('ConfigFormularios');
        $formularios= [];
        foreach ($_formularios as $key => $value) {
            $formularios[] = $key;
        }

       $generate_txt =  $this->exportForm($formularios);
       $carga_ftp =  $this->cargaftp();

       return [
           'generate_txt' => $generate_txt,
           'carga_ftp'    => $carga_ftp
       ]; 
    }
    protected function cargaftp(){
        DB::beginTransaction();
        try {
            $control_form = Controltxtftp::whereIn('ABC26_estado',['error-ftp', 'pendiente'])
            ->where('ABC26_iteraciones','<', '3')
            ->get();
            foreach ($control_form as $key => $value) {
                
                $carga_ftp = $this->UptateFTP($value);
                $msj = 'ARCHIVO CARGADO CON ÉXITO';
                $iteracion = $value->ABC26_iteraciones;
                $estado = 'cargado';

                if($carga_ftp['error']){
                    
                    $iteracion = $value->ABC26_iteraciones + 1;
                    $estado = 'pendiente';
                    if($iteracion == 3){
                        $estado = 'error-ftp';
                        $msj = 'ERROR AL CARGAR EL ARCHIVO AL SERVIDOR';
                    }
                    if($carga_ftp['msg']){
                        $msj = $carga_ftp['msg'];
                    }
                }

                Controltxtftp::where('ABC26_id', $value->ABC26_id)
                ->update([
                    "ABC26_estado"          => $estado, 
                    "ABC26_iteraciones"     => $iteracion, 
                ]);

                Controltxthistorial::create([
                    "ABC26_id" => $value->ABC26_id,
                    "ABC27_descripcion" => $msj,
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => ''
            ];
        }

        DB::commit();
        return [
            'error' => false,
            'msg'   => 'carga realizada',
            'formulario' => ''
        ];
    }

    protected function exportForm($formularios = []){
      
        DB::beginTransaction();
        try {
            if(empty($formularios)){
                return [
                    'error' => false,
                    'msg' =>  "Formulario no registrado"
                ];
            }
            
            foreach ($formularios as $key => $value) {
                $fechas = Formularios::select(\DB::raw('DISTINCT(CAST(created_at AS DATE)) as fecha'))
                    ->where('ABC07_slug', $value)
                    ->get()
                    ->pluck('fecha');
      
                foreach ($fechas as $fecha) {
                    $disco = Storage::disk('public'); 
                    $archivo = $this->filename($value, $fecha);
                    if ($disco->exists($archivo)) {
                        echo $archivo . "<br>";
                        continue;
                    } 

                    $contenido = '';
                    $formularios = Formularios::where('ABC07_slug', $value)
                        ->where('ABC07_exportar', 0)
                        ->whereBetween('created_at', [$fecha . ' 00:00:00', $fecha . ' 23:59:59']);

                    foreach ($formularios->get() as $form) {
                        $contenido .= $form->ABC07_campos."\n";
                    }

                    if ($contenido != '') {
                        $respuesta = $this->generateTXT($value, $contenido, $fecha);
                      
                        if (!$respuesta['error']) {
                            $formularios->update([
                                'ABC07_exportar' => 1
                            ]);
                            $control = Controltxtftp::create([
                                "ABC26_nombre_archivo"  => $respuesta['archivo'],
                                "ABC26_formulario"      => $respuesta['formulario'],
                                "ABC26_estado"          => 'pendiente', 
                                "ABC26_ruta_archivo"    => $respuesta['ruta'], 
                            ]);
                            Controltxthistorial::create([
                                "ABC26_id" => $control['ABC26_id'] ,
                                "ABC27_descripcion" => 'TXT CREADO CON EXITO' ,
                            ]);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => $formularios
            ];
        }

        DB::commit();
        return [
            'error' => false,
            'msg'   => 'Txt creados',
            'formulario' => ''
        ];
    }

    protected function UptateFTP($formulario){
        try {
            
            //conexion ftp
            $disco = Storage::disk('ftp'); 
             
            //configuraciond de formulario
            $config = Config::get('ConfigFormularios.'.$formulario->ABC26_formulario);

            //ruta directorio creado local
            $ruta_archivo  = Config::get($config['ruta_local']);
            
            //nombre archivo local
            $archivo_nombre_local = $config['nombre_archivo'];
           // $ruta_archivo_local = 
            $ruta_archivo_local = $ruta_archivo .'/'.$formulario->ABC26_ruta_archivo;
          
            //seleccion de archivo local
            $archivo_local  = file_get_contents($ruta_archivo_local);
            //Nombre carpeta
            //$directorio_ftp = $config['directorio'];
            // $ruta_archivo_ftp = date('Y').'/'.date('m').'/'.$config['directorio'].'/'.$archivo_nombre_local;
            $ruta_archivo_ftp = $formulario->ABC26_ruta_archivo;
           
            //subir archivo al ftp
            $resul =  $disco->put($ruta_archivo_ftp, $archivo_local);
            
            //echo "subo el archivo ".$ruta_archivo_ftp;
        } catch (\ErrorException $e) {
            
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => $formulario
            ];
        } catch (\RuntimeException $e) {
            
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => $formulario
            ];

        } catch (Exception $e) {

		    return [
                'error' => true,
                'msg'   => $e,
                'formulario' => $formulario
            ];
        }
        
        return [
            'error' => false,
            'msg'   => $resul,
            'formulario' => $formulario
        ];
    }

    protected function generateTXT($formulario, $texto, $fecha){
        try {
            
            //conexion ftp
            $disco = Storage::disk('public'); 
            $config = Config::get('ConfigFormularios.'.$formulario);

            //ruta directorio creado local
            $ruta_archivo  = Config::get($config['ruta_local']);
            
            $ruta_archivo_local = $this->filename($formulario, $fecha);
            $archivo_nombre_local = $config['nombre_archivo'];
            $ruta_archivo  = Config::get($config['ruta_local']);
            $resul =  $disco->put($ruta_archivo_local, $texto);

            if($resul){
                chmod($ruta_archivo.'/'.$ruta_archivo_local, 0777);
            }

        } catch (\ErrorException $e) {
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => $formulario,
                'ruta'      => ''
            ];
        } catch (\RuntimeException $e) {
            
            return [
                'error' => true,
                'msg'   => $e->getMessage(),
                'formulario' => $formulario,
                'archivo'   => $archivo_nombre_local,
                'ruta'      => ''
            ];

        } catch (Exception $e) {

		    return [
                'error' => true,
                'msg'   => $e,
                'formulario' => $formulario,
                'archivo'   => $archivo_nombre_local,
                'ruta'      => ''
            ];
        }
        
        return [
            'error' => false,
            'msg'   => $resul,
            'formulario' => $formulario,
            'archivo'   => $archivo_nombre_local,
            'ruta'      => $ruta_archivo_local
        ];
    }

    protected function filename($formulario, $fecha) {
        $fecha = explode('-', $fecha);
        //configuraciond de formularioConfigFormularios
        $config = Config::get('ConfigFormularios.'.$formulario);

        //ruta directorio creado local
        $ruta_archivo  = Config::get($config['ruta_local']);
        
        //nombre archivo local
        $archivo_nombre_local = $config['nombre_archivo'];

        return 'formularios/'.$fecha[0].'/'.$fecha[1].'/'.$fecha[2].'/'.$archivo_nombre_local;
    }
}

