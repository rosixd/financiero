<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Illuminate\Http\Response;

use Modules\User\Http\Requests\ActualizaDatosRequest;

class ActualizacionDatosController 
{
	
	public $prueba = false;
    public function index()
    {
        error_reporting(E_ALL);
        try {
            

          
            $Datos = [];
            if($this->valicarIC()){
            ///return $this->retorno(true,'', []);
			    $Datos['error'] = true;
                $Datos['msg'] = "cuenta del cliente no es IC";
			    return  $Datos;
            }
            $Datos['msg'] = '';
            $Datos['error'] = false;
            $DireccionFacturacion = $this->DireccionFacturacion();//coinfo03
            $DatosContacto        = $this->DatosContacto();//coinfo03
            
          
            $getMaileecc = $this->getMaileecc(Session::get('TarjetaSeleccionada')['RutTitular']);
            $DatosLegal = $this->DatosLegal(Session::get('TarjetaSeleccionada')['RutTitular']); 
           
            $Datos['datos'] = [
                'DireccionFacturacion' => $DireccionFacturacion,
                'DatosContacto'        => $DatosContacto,
                'getMaileecc'          => $getMaileecc,
                'DatosLegal'           => $DatosLegal
            ];

	    
        } catch (\Exception $e) {


            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();

            return $Datos;
        }catch (\FatalErrorException $e) {
            
            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();

            return $Datos;
            
        }
		catch (\ErrorException $e) {
            
            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();

            return $Datos;
            
        }
        
        //dd($Datos);
       return $Datos;

    }
    
    //public function guardar(Request $request){
    public function guardar(ActualizaDatosRequest $request){
      	//dd($request->all());
    
       $rut = Session::get('TarjetaSeleccionada')['RutTitular'];
       $clave = $request->codigo;
       
        
        $DatosLegal = $this->DatosLegal($rut); 
        
        if($DatosLegal['error']){
   
            return $DatosLegal;
        }

        if($DatosLegal['datos']['codError'] == 0 && $DatosLegal['datos']['smsActivo'] == 'S'){

            $DatosContact['numFono'] = $DatosLegal['datos']['numFono'];
            $DatosContact['tipFono'] = $DatosLegal['datos']['tipFono'];
            
			
			if($this->prueba){
				$DatosContact['numFono'] = '942365466';
	            		$DatosContact['tipFono'] = 'b';
		        	$DatosLegal['datos']['email'] = 'proyectosabcdin@netred.cl';
			}
           

          /*   $DatosContact['tipFono'] = 'B'; */

            $mail =  trim($DatosLegal['datos']['email']);

           $validaCodigo = $this->ValidaSmsWS($rut, $mail, $DatosContact, $clave);
	  
            if($validaCodigo['error']){
                return $validaCodigo;
            }
            if($validaCodigo['datos']['OperationResponse']['statusDescription'] != "Operation completed successfully"){
                $validaCodigo['error'] = true;
                return $validaCodigo;
            }
            $_datos  = $request->all();
            
            $PINDEECCEMAIL = 'N';
            $PESLEGAL = 1;
	    
            /* if($request->radiostacked == 'fisico'){
                $PINDEECCEMAIL = '1';
            }else */ 
            
            if($request->radiostacked == 'email'){
                $PINDEECCEMAIL = 'S';

                $mail  = trim($request->emails);	
           
                $mergeEmail = $this->mergeMaileecc($rut, $mail, $PINDEECCEMAIL, $PESLEGAL, $rut);
    
                if($mergeEmail['error']){
                    return $mergeEmail;
                }
            }

           /*  $mail  = trim($request->emails);	
           
            $mergeEmail = $this->mergeMaileecc($rut, $mail, $PINDEECCEMAIL, $PESLEGAL, $rut);

            if($mergeEmail['error']){
                return $mergeEmail;
            }
                 */
            
		
            $datos = $this->Datos($_datos);
           
	   
            $datosActualizado = $this->ActualizaClienteIC($datos);
            
            if($datosActualizado['error']){

               
                return $datosActualizado;
            }

            $this->success();

            $indece = substr( Session::get('TarjetaSeleccionada')['NumeroTarjeta'],-4);
			$tarjeta = str_pad($indece, 4, "0", STR_PAD_LEFT);
            
            $datos_user =  $this->BuscarUser($rut); //Busca Usuario ConInfoCuenta3
            $datos_tarjeta = $datos_user['datos']['tarjetas'][$tarjeta];
            
            session()->put('DatosCLiente', $datos_user); //datos clientes ConInfoCuenta3
            session()->put('TarjetaSeleccionada', $datos_tarjeta); //datos tarjeta seleccionada por el cliente
            if(!$datosActualizado['error']){ 
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 0, 
                    "deTransaccion"        => 'Actualiza Datos Clientes IC', 
                    "cdError"              => '',
                    "cd_accion"            => 11,
                    "de_accion"            => '', 
                    "entradaTransaccion"   => "", 
                    "salidaTransaccion"    => "", 
                
                ];
                $this->Trazabilidad($datos);
            }
            return [
                'error' =>false,
                'msg'   => "Datos actualizados",
                'datos' => []
            ];
        }
       
        return [
            'error' =>true,
            'msg'   => "",
            'datos' => []
        ];
        
    }
    public function BuscarUser ($rut)
    {
       // $_rut = $request->rut;
        $_rut = $rut;
        $datos_usuarios = false;

        $rut = $this->RutFormat($_rut);
        //se formatea el rut
        $datos_usuarios = WsSocket::ConInfoCuenta3($rut);	
        
        return $datos_usuarios;
    }

    protected function RutFormat ($rut)
    {
        //quitar puntos . -
        //separa DV
        $rut  = str_replace('.','', $rut);
        $rut  = str_replace('-','', $rut);

        $dv = substr($rut, -1, 1);

        if(!is_numeric($dv)){
          $rut = substr($rut, 0, -1).strtoupper($dv);
        }
       
        return $rut;
    }

    protected function datospersona($rut){
        $datos = WsSocket::datospersona($rut);
       
        $datos['datos'] = $this->userdatos($datos['datos'], 'MW-datospersona');
       
        return $datos;
    }

      protected function userdatos ($data = 0, $config)
    {
        $_datos_usuarios = Config::get("FormatoDataUser.".$config);
      
        $datos_usuarios = [];
      
        
        $datos_usuarios['original-data'] = $data; //datos originales de la consulta

        //valida el estatus el cliente
        $estatus = false;
       
        switch ($data[1]) {
            case '00':
                //dd('Cliente tiene cuenta ABCDIN');
                $estatus = 00;
                break;
            case '01':
                //dd('Cliente no tiene cuenta ABCDIN');
                $estatus = 01;
                break;
            case '99':
                //dd('Problemas de comunicación (Milliways)');
                $estatus = 99;
                break;
        }
         
        if (count($data) == 0) {
            foreach ($_datos_usuarios as $key => $datos_usuario) {
                $datos_usuarios[$datos_usuario] = "";
            }
        } elseif ($estatus == '00') {
		    //dd($data, $datos_usuarios);
	        foreach ($_datos_usuarios as $key => $datos_usuario) {
                if (isset($data[$key + 1])) {
                    $datos_usuarios[$datos_usuario] = $data[$key + 1];
                }
	        }
        }
		
        return [
            'datos_usuario'   => $datos_usuarios,
            'estatus_cliente' => $estatus 
        ];
    }
    public function validar(){
      
        $DatosLegal = $this->DatosLegal(Session::get('TarjetaSeleccionada')['RutTitular']); 
        
     
        if($DatosLegal['error']){
            return [
                'error' => true,
                'msg'   => 'error',
                'estatus_cliente' => 01,
                'datos' => []
            ]; 
        }
        $error = true;
        //if($DatosLegal['datos']['msjError'] == 0 && $DatosLegal['datos']['smsActivo'] == 'S'){
	    if($DatosLegal['datos']['codError'] === '0' && $DatosLegal['datos']['smsActivo'] == 'S'){
        //if(true){ 
            $DatosContact['numFono'] = $DatosLegal['datos']['numFono'];
            $DatosContact['tipFono'] = $DatosLegal['datos']['tipFono'];
			if($this->prueba){
				$DatosContact['numFono'] = '942365466';
           	 	$DatosContact['tipFono'] = 'B';
	        	$DatosLegal['datos']['email'] = 'proyectosabcdin@netred.cl';
            }
            $DatosContact['tipFono'] = 'B';
		    $respuesta = $this->SmsWs(Session::get('TarjetaSeleccionada')['RutTitular'], $DatosLegal['datos']['email'], $DatosContact);
            
			if($respuesta['error']){
                return $respuesta;
            }
            
           if($respuesta['datos']['OperationResponse']['statusCode'] == 0) {
               $respuesta['datos'] = true;
           }else{
                $respuesta['datos'] = false;
           }
           $error = false;
        }
       
        return [
            'error' => $error,
            'msg'   => '',
            'datos' => false
        ]; 
    }

    protected function getMaileecc($rut){

        $parametros = array(
            'PCODPERSONA' => $rut
        ); 
        $respuesta = WsSocket::getMaileecc($rut, 'getMaileecc',$parametros);
        //dd($respuesta);
        if($respuesta['datos']['PMENSERROR'] == 1){
            $this->error();
            $respuesta['error'] = true; 
        }
     
        return $respuesta;
    }

    protected function DireccionFacturacion(){
        
        $tarjetas_seleccionada = $this->DatosTarjeta();
        
        return [
            'CodigoComuna'      =>$tarjetas_seleccionada['CodigoComuna'],
            'CodigoRegion'      =>$tarjetas_seleccionada['CodigoRegion'],
            'NombreRegion'      =>$tarjetas_seleccionada['NombreRegion'],
            'NombreComuna'      =>$tarjetas_seleccionada['NombreComuna'],
            'Calle'             =>$tarjetas_seleccionada['Calle'],
            'NumDireccion'      =>$tarjetas_seleccionada['NumDireccion'],
            'DetalleDireccion'  =>$tarjetas_seleccionada['DetalleDireccion'],
        ];

    }

    protected function DatosTarjeta(){
        $tarjetas_seleccionada = Session::get('TarjetaSeleccionada');
        //dd($tarjetas_seleccionada);
        return  $tarjetas_seleccionada;
    }

    protected function DatosContacto(){
        $tarjetas_seleccionada = $this->DatosTarjeta();
       
        $telefonos =  $tarjetas_seleccionada['Telefonos'];

        $factoresXML = new SimpleXMLElement($telefonos);
        $telefonos = json_decode(json_encode($factoresXML), true);
        
        $dataosContacto = [];
        foreach ($telefonos['@attributes'] as $key => $value) {
            $dataosContacto[$key] = $value;
        }
        
        return $dataosContacto;
    }
    protected function valicarIC(){
        if(Session::get('TarjetaSeleccionada')['OrigenDeLosDatos'] == 'IC'){
            return false;
        }
        return true;
    }

    protected function DatosLegal($rut){

        $repuesta = WsSocket::getDatosLegal($rut);

       return $repuesta;

       
    }

    protected function SmsWs($rut, $mail, $DatosContacto){
        
        $repuesta['error'] = false;
        $repuesta['msg'] = "";
        
        $repuesta = WsSocket::SmsWs($rut, $mail, $DatosContacto);
     
        if($repuesta['error']){
            $this->error();
            return $repuesta;
        }
        
        
        return $repuesta;
    }
    protected function ValidaSmsWS($rut, $mail, $DatosContacto, $clave){
        $repuesta['error'] = false;
        $repuesta['msg'] = "";
        
        $repuesta = WsSocket::ValidaSmsWS($rut, $mail, $DatosContacto, $clave);
        
        if($repuesta['error']){
            $this->error();
            return $repuesta;
        }

        if($repuesta['datos']['OperationResponse']['statusCode'] != 0){
            $this->error();
            $repuesta['error'] = true;
            $repuesta['msg'] = $repuesta['datos']['OperationResponse']['statusDescription'];
        }

        return $repuesta;
    }

    protected function ActualizaClienteIC($datos){
        $respuesta['error'] = false;
        $respuesta['msg']   = '';

        $respuesta = WsSocket::ActualizaClienteIC($datos);
       
        
        if($respuesta['datos']['updateClienteICResponse']['ESTADO_PROCESO'] != 0){
            $respuesta['error'] = true;
            $respuesta['msg']   = 'sus datos no se actualizaron';
        }
        
        return  $respuesta;
    }

    protected function mergeMaileecc($PCODPERSONA, $PEMAIL, $PINDEECCEMAIL, $PESLEGAL, $PINCLUIDOPOR){
        
        $respuesta = WsSocket::mergeMaileecc($PCODPERSONA, $PEMAIL, $PINDEECCEMAIL, $PESLEGAL, $PINCLUIDOPOR );
        
        if($respuesta['datos']['PCODERROR'] == 1){
            $this->error();
            $respuesta['error'] = true;
            $respuesta['msg'] = $respuesta['datos']['PMENSERROR'];

        }
        return $respuesta;
    }

    protected function error(){
        
        $info = [
            "rut"                  => Session::get('TarjetaSeleccionada')['RutTitular'], 
            "cdTransaccion"        => '1', 
            "deTransaccion"        => 'Error desconocido', 
            "cdError"              => 99, 
            "entradaTransaccion"   => '', 
            "salidaTransaccion"    => '', 
        ];
        //$this->trazabilidad($info);
    }

    protected function success(){
        
        $info = [
            "rut"                  => Session::get('TarjetaSeleccionada')['RutTitular'], 
            "cdTransaccion"        => '0', 
            "deTransaccion"        => 'Éxito', 
            "cdError"              => 00, 
            "entradaTransaccion"   => '', 
            "salidaTransaccion"    => '', 
        ];
        //$this->trazabilidad($info);
    }

    protected function trazabilidad($info){
       
         WsSocket::Trazabilidad($info);
    }

    protected function Datos($datos){
        $user_data= Session::get('DatosPersonales')['datos']['datos_usuario'];
        $numtelefono = str_replace(" ", "", $datos['telefonocelular']);
		
        $numero  = $numtelefono ;
        $codigo = 56;
        if(strlen( $numtelefono ) == 11 ){
            $numero  = substr( $numtelefono, 2);
        }
	
		$datos_correo = explode("@",trim($datos['emails']));
		
        return $data = [
            'RUT_CLIENTE'                   => $user_data['Rut'], //REQ
            'EST_CIVIL'                     => '',
            'SEXO'                          => '',
            'FEC_NACIMIENTO'                => '',
            'PRIMER_APELLIDO'               => $user_data['PrimerApellido'],//REQ
            'SEGUNDO_APELLIDO'              => $user_data['SegundoApellido'],//REQ
            'PRIMER_NOMBRE'                 => $user_data['PrimerNombre'],//REQ
            'SEGUNDO_NOMBRE'                => $user_data['SegundoNombre'],//REQ
            'PROFESION'                     => '',
            'CONYUGUE'                      => '',
            'NACIONALIDAD'                  => '',
            'EMAIL_USUARIO'                 => 	$datos_correo[0],
            'EMAIL_SERVIDOR'                => 	isset($datos_correo[1]) ? $datos_correo[1] : '',
            'NIVEL_ESTUDIOS'                => '',
            'TIPO_VIVIENDA'                 => '',
            'NUM_HIJOS'                     => '',
            'NUM_DEPENDIENTES'              => '',
            'ES_RESIDENTE'                  => '',
            'TIEMPO_VIVIEN_ACT'             => '',
            'TOTAL_INGRESOS'                => '',
            'COD_PAIS'                      => '',
            'SCORING'                       => '',
            'ACTIVIDAD'                     => '',
            'TIPO_SOC_CONYUGAL'             => '',
            'NOM_CONYUGUE'                  => '',
            'FEC_MATRIMONIO'                => '',
            'IND_SOL_PATRIMONIO'            => '',
            'IND_EECC_EMAIL'                => 'S',//REQ
            'COD_DIRECCION'                 => '1',//REQ
            'TIP_DIRECCION'                 => '1',//REQ
            'COD_POSTAL'                    => '',
            'DETALLE'                       => $datos['depto'],
            'COD_PAIS_CNT'                  => '',
            'COD_REGION'                    => $datos['cod_region'],
            'COD_COMUNA'                    => $datos['cod_comuna'],
            'CALLE'                         => $datos['calle'],
            'NUMERO'                        => $datos['numero'], 
            'ES_DIR_EECC'                   => 'S',
            'IND_ACCION_BLOQ_DIR'           => '',
            'MOTIVO_RECHZ'                  => '',
            'IND_ESTADO'                    => '',
            'COD_FUENTE'                    => '',
            'COD_AREA'                      => $codigo,
            'NUM_TELEFONO'                  => $numero,
            'TIP_TELEFONO'                  => 'C',
            'TEL_UBICACION'                 => '',
            'EXTENSION'                     => '',
            'ES_DEFAULT'                    => '',
            'INCLUIDO_POR'                  => '',
            'FEC_INCLUSION'                 => '',
            'MODIFICADO_POR'                => '',
            'FEC_MODIFICACION'              => '',
            'TEL_ACTIVO'                    => '',
            'COD_FUENTE_FONO'               => '',
        ];
    }
}



/* 
'RUT_CLIENTE'                   => '',
'EST_CIVIL'                     => '',
'SEXO'                          => '',
'FEC_NACIMIENTO'                => '',
'PRIMER_APELLIDO'               => '',
'SEGUNDO_APELLIDO'              => '',
'PRIMER_NOMBRE'                 => '',
'SEGUNDO_NOMBRE'                => '',
'PROFESION'                     => '',
'CONYUGUE'                      => '',
'NACIONALIDAD'                  => '',
'EMAIL_USUARIO'                 => '',
'EMAIL_SERVIDOR'                => '',
'NIVEL_ESTUDIOS'                => '',
'TIPO_VIVIENDA'                 => '',
'NUM_HIJOS'                     => '',
'NUM_DEPENDIENTES'              => '',
'ES_RESIDENTE'                  => '',
'TIEMPO_VIVIEN_ACT'             => '',
'TOTAL_INGRESOS'                => '',
'COD_PAIS'                      => '',
'SCORING'                       => '',
'ACTIVIDAD'                     => '',
'TIPO_SOC_CONYUGAL'             => '',
'NOM_CONYUGUE'                  => '',
'FEC_MATRIMONIO'                => '',
'IND_SOL_PATRIMONIO'            => '',
'IND_EECC_EMAIL'                => '',
'COD_DIRECCION'                 => '',
'TIP_DIRECCION'                 => '',
'COD_POSTAL'                    => '',
'DETALLE'                       => '',
'COD_PAIS_CNT'                  => '',
'COD_REGION'                    => '',
'COD_COMUNA'                    => '',
'CALLE'                         => '',
'NUMERO'                        => '',
'ES_DIR_EECC'                   => '',
'IND_ACCION_BLOQ_DIR'           => '',
'MOTIVO_RECHZ'                  => '',
'IND_ESTADO'                    => '',
'COD_FUENTE'                    => '',
'COD_AREA'                      => '',
'NUM_TELEFONO'                  => '',
'TIP_TELEFONO'                  => '',
'TEL_UBICACION'                 => '',
'EXTENSION'                     => '',
'ES_DEFAULT'                    => '',
'INCLUIDO_POR'                  => '',
'FEC_INCLUSION'                 => '',
'MODIFICADO_POR'                => '',
'FEC_MODIFICACION'              => '',
'TEL_ACTIVO'                    => '',
'COD_FUENTE_FONO'               => '',
*/