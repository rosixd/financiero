<?php

namespace Modules\User\Http\Controllers\PublicPrivate;

use Carbon\Carbon;
use Modules\Administracion\Entities\Acreditacion;
use Modules\Administracion\Entities\ActivdadEconomica;
use Modules\Administracion\Entities\Comuna;
use Modules\Administracion\Entities\CondicionHabitacional;
use Modules\Administracion\Entities\EstadoCivil;
use Modules\Administracion\Entities\Region;
use Modules\Formularios\Entities\SolicitudTarjeta;
use Modules\User\Http\Requests\solicitudtajertaGuardarRequest;
use Modules\User\Http\Requests\ValidateClientRequest;
use Session;
use WsSocket;

class SolicitudTarjetaController
{
    public function index()
    {
    }

    public function IDverifier()
    {
        $repuesta                                   = WsSocket::IDverifier();
    }

    public function Guardar(solicitudtajertaGuardarRequest $request)
    {
        try {
            if ($request->lugar == 1) {
                $Previred                           = $this->ValidaHora();
                $_Equifax                           = false;
                $Equifax                            = [];
                $prueba                             = [];
                if ($request->numerodeserie != "") {
                    if ($Previred) {
                        $solicitud                  = SolicitudTarjeta::where('id', $request->id)
                        ->where('ABC31_TOKEN', $request->token)->first();
                        $rut                        = $solicitud->rut . $solicitud->dv;
                        $Equifax                    = $this->preguntas($rut, $request->numerodeserie);
                        // $Equifax['error'] = true;
                        if ($Equifax['error']) {
                            $error                  = false;
                            $Equifax                = '';
                        } else {
                            $prueba                 = $Equifax;
                            $Equifax                = $Equifax['preguntas'];

                            $_Equifax               = true;
                        }
                    }
                }
                $fecha                              = Carbon::createFromFormat('d-m-Y', $request->fechadenacimiento);

                $terminos                           = 0;
                if ($request->terminos == 'on') {
                    $terminos                       = 1;
                }

                SolicitudTarjeta::where('id', $request->id)
                ->where('ABC31_TOKEN', $request->token)
                ->update([
                    'serie'                         => $request->numerodeserie === "" ? 'NO':'SI',
                    'primer_nombre'                 => $request->primernombre,
                    'segundo_nombre'                => $request->segundonombre,
                    'apepat'                        => $request->apellidopaterno,
                    'apemat'                        => $request->apellidomaterno,
                    'email'                         => $request->email,
                    'fecnac'                        => $fecha,
                    'fono_movil'                    => $request->telefonocelular,
                    'fono_fijo'                     => $request->telefonofijo,
                    'acepta_termycond'              => $terminos,
                    'horario_previred'              => $Previred ? 1:0,
                    'responde_preguntas'            => $_Equifax ? 1:0,
                ]);
                $datos = [
                    'error'                         => false,
                    'msg'                           => "",
                    'equifax'                       => $_Equifax,
                    'prueba_equifax'                => $prueba,
                    'data' =>[
                        'id'                        => $request->id,
                        'token'                     => $request->token,
                        'preguntas'                 => $Equifax,
                    ],
                ];
            }

            if ($request->lugar == 2) {
                $repuesta_Equifax                   = false;
                $transactionKey                     = '';
                $RiskStrategyDecision               = '';

                $datos                              = SolicitudTarjeta::where('id', $request->id)
                ->where('ABC31_TOKEN', $request->token)
                ->first();

                if (isset($request->questionId)) {
                    $repuesta_Equifax               = true;
                    $rut                            = $datos->rut . $datos->dv;
                    $respuesta                      = $this->RespuestasEquifax($request->questionId, $rut);

                    if (!$respuesta['error']) {
                        $transactionKey         = Session::get('IDverifier')['datos']['transactionKey'];
                        if ($respuesta['datos']['AssessmentComplete']['RiskStrategyDecision'] == 'Y') {
                            $RiskStrategyDecision   = $respuesta['datos']['AssessmentComplete']['RiskStrategyDecision'];
                        }
                    }
                }

                $hijos                              = 0;
                if ($request->tienehijos === 'si') {
                    $hijos                          = $request->nrohijos;
                }

                $patente                            = '';
                if ($request->automovil == 'si') {
                    $patente                        = $request->nropatente;
                }

                $fecha                              = Carbon::createFromFormat('d-m-Y', $request->fecingreso);
                $fecha2                             = Carbon::createFromFormat('d-m-Y', $request->fecocupviv);

                $actividad_Economica                = $this->ActivdadEconomica();
                $regiones                           = $this->BuscarNombreRegion();
                $comunas                            = $this->BuscarNombreComuna();
                $cond_vivienda                      = $this->CondicionVivienda();
                $acreditacion                       = $this->acreditacion();
                $estado_civil                       = $this->EstadoCivil();

                SolicitudTarjeta::where('id', $request->id)
                ->where('ABC31_TOKEN', $request->token)
                ->update([
                    'cod_sexo'                      => strtoupper($request->sexo),
                    'desc_sexo'                     => strtoupper($request->sexo) == 'M' ? "MASCULINO" : "FEMENINO",
                    'q_hijos'                       => $hijos,
                    'cod_profe'                     => array_key_exists($request->actividad, $actividad_Economica) ? $request->actividad : '' ,
                    'profesion'                     => array_key_exists($request->actividad, $actividad_Economica) ? $actividad_Economica[$request->actividad] : '' ,
                    'fec_ingreso'                   => $fecha,
                    'cod_region'                    => $request->region,
                    'nom_region'                    => $regiones[$request->region],
                    'cod_comuna'                    => $request->comuna,
                    'nom_comuna'                    => $comunas[$request->comuna],
                    'calle'                         => $request->calle,
                    'numcalle'                      => $request->numero,
                    'detdir'                        => $request->detdireccionp,
                    'cod_tipovi'                    => $request->tipoviv,
                    'desc_tipovi'                   => $request->tipoviv == 'C' ? 'Casa' : 'Departamento',
                    'fecvivienda'                   => $fecha2,
                    'cod_vivienda'                  => $request->condhabit,
                    'desc_vivienda'                 => $cond_vivienda[$request->condhabit],
                    'pagomensual'                   => str_replace('.', '', $request->arriendodivmensual),
                    'patente'                       => $patente,
                    'cod_propiedad'                 => $request->propiedades,
                    'desc_propiedad'                => $acreditacion[$request->propiedades],
                    'desc_nacionalidad'             => $request->nacionalidad,
                    "cod_estado"                    => $request->estadocivil,
                    "desc_estado"                   => $estado_civil[$request->estadocivil],
                ]);

                $datos                              = SolicitudTarjeta::where('id', $request->id)
                ->where('ABC31_TOKEN', $request->token)
                ->first();

                //paso 7
                $datos['transactionKey']            = $transactionKey;
                $rest                               = $this->TC2000EvaluacionExpress($datos);
                $codRetorno                         = '';
                $codRechazo                         = '';
                $descrRechazo                       = '';
                $descrRechazoregex                  = false;
                /* $estatus = 0; */
                $rest['error']                      = false;
                $estatus                            = 8;

                if (!$rest['error']) {
                    if (isset($rest['datos']) &&
                        isset($rest['datos']['codRetorno']) &&
                        isset($rest['datos']['codRechazo']) &&
                        isset($rest['datos']['descrRechazo'])) {
                        $codRetorno                 = (int) $rest['datos']['codRetorno'];
                        $codRechazo                 = (int) $rest['datos']['codRechazo'];
                        $descrRechazo               = $rest['datos']['descrRechazo'];
                        $descrRechazoregex          = preg_match("/aprobada/im", $descrRechazo);
                    }

                    /*Para forzar aprobacion de tarjeta y envio de email*/
                    /* $codRetorno = 0;
                    $codRechazo                     = 0;
                    $descrRechazo                   = 'aprobada';
                    $descrRechazoregex              = preg_match("/aprobada/im", $descrRechazo); */

                    if ($codRechazo === 0 && $codRetorno === 0 && $descrRechazoregex) {
                        $estatus                    = 0;
                        $email                      = $datos->email;
                        $this->EnvioEmail($email);
                    } /* else { */
                       /*  $estatus = 8; */
                       /*  $rut = $datos->ABC31_Rut; */

                      /*   $_datos = [
                            "rut"                   => $rut,
                            "cdTransaccion"         => 6,
                            "deTransaccion"         => 'TC2000_EvaluacionExpress_WEB',
                            "cd_accion"             => 10,
                            "cdError"               => $codRechazo,
                            "entradaTransaccion"    => '',
                            "salidaTransaccion"     => '',
                        ];
                        WsSocket::Trazabilidad($_datos); */
                    /* } */
                    /* $descrRechazo = $rest['datos']['descrRechazo']; */
                }

                SolicitudTarjeta::where('id', $request->id)
                ->where('ABC31_TOKEN', $request->token)
                ->update([
                    'evaluacion_codretorno'         => $codRetorno,
                    'evaluacion_codrechazo'         => $codRechazo,
                    'evaluacion_desrechazo'         => $descrRechazo,
                    'respuestas_correctas'          => $RiskStrategyDecision,
                    'codigo_previred'               => $datos['transactionKey'],
                ]);

                $rut                                = $datos->rut . $datos->dv;

                $_datos = [
                    "rut"                           => $rut,
                    "cdTransaccion"                 => 0,
                    "deTransaccion"                 => 'Éxito',
                    "cd_accion"                     => 10,
                    "cdError"                       => '',
                    "entradaTransaccion"            => '',
                    "salidaTransaccion"             => '',
                ];
                WsSocket::Trazabilidad($_datos);

                $datos = [
                    'error'                         => false,
                    'msg'                           => '',
                    'estatus_cliente'                     => $estatus,
                    'datos'                         => [],
                ];
            }
        } catch (\Exception $e) {
            return [
                'error'                             => true,
                'msg'                               => $e->getMessage(),
                'equisfax'                          => '',
                'datos'                             => [],
            ];
        }

        return $datos;
    }

    public function RespuestasEquifax($respuestas, $rut = 19)
    {
        return   $_preguntas                          = WsSocket::IDverifierEnvioPreguntas($respuestas, $rut);
    }

    protected function ValidaHora()
    {
        $valido                                     = false;
        $hora                                       = Carbon::now();

        $am                                         = clone $hora;
        $am->hour(8);
        $am->minute(0);
        $am->second(0);

        $pm                                         = clone $am;
        $pm->hour(23);

        if ($hora->gt($am) && $hora->lt($pm)) {
            $valido                                 = true;
        } else {
            $rut                                    = '19';
            $datos = [
                "rut"                               => $rut,
                "cdTransaccion"                     => 3,
                "deTransaccion"                     => 'Fuera del horario de Previred',
                "cdError"                           => '',
                "cd_accion"                         => 10,
                "entradaTransaccion"                => '',
                "salidaTransaccion"                 => '',
            ];
            WsSocket::Trazabilidad($datos);
        }

        return $valido;
    }

    protected function TC2000EvaluacionExpress($datos)
    {
        //WsSocket::Trazabilidad($datos);
        return WsSocket::TC2000EvaluacionExpress($datos);
    }

    protected function BuscarNombreRegion()
    {
        return Region::pluck('ABC11_nombre', 'ABC11_codigo')->toArray();
    }
    protected function CondicionVivienda()
    {
        return CondicionHabitacional::pluck('ABC23_nombre', 'ABC23_codigo')->toArray();
    }

    protected function acreditacion()
    {
        return Acreditacion::pluck('ABC22_nombre', 'ABC22_codigo')->toArray();
    }

    protected function BuscarNombreComuna()
    {
        return Comuna::pluck('ABC13_nombre', 'ABC13_codigo')->toArray();
    }

    public function preguntas($rut, $numSerie)
    {
        try {
            $_preguntas                             = WsSocket::IDverifier($rut, $numSerie);

            if ($_preguntas['error'] == true) {
                return  $respuesta = [
                    'error'                         => true,
                    'preguntas'                     => [],
                ];
            }

            $preguntas                              = [];
            $error                                  = true;

            if (array_key_exists('InteractiveQuery', $_preguntas['datos'])) {
                $preguntas                          = $_preguntas['datos']['InteractiveQuery']['Question'];
                session()->put('IDverifier', $_preguntas);
                $error                              = false;
            }
            $respuesta                              = [];
            $respuesta = [
                'error'                             => $error,
                'preguntas'                         => $preguntas,
            ];
        } catch (\Exception $e) {
            $respuesta = [
                'error'                             => true,
                'preguntas'                         => [],
                'Exception'                         => true,
                'msj'                               => $e->getMessage(),
            ];

            return $respuesta;
        }

        return  $respuesta;
    }

    public function ValidateClient(ValidateClientRequest $request)
    {
        try {
            $rut                                    = $this->RutFormat($request->rut);
            $respuesta                               = WsSocket::EsClienteValidoWeb($rut);
            $CodigoRetornoDeFuncion                 = substr($respuesta['datos'][0], 1, -5);
            $error = false;
            $token                          = str_random(32);

            switch ($respuesta['datos'][1]) {
                //switch ('00') {
                case '00':
                    $codigo                         = '00';
                    $msg                            = trans('user::SolicitudDeTarjeta.ok cliente');
                    $RetornoFuncion                 = '';
                    break;
                case '01':
                    $codigo                         = '01';
                    $msg                            = trans('user::SolicitudDeTarjeta.rut es de un cliente');
                    $RetornoFuncion                 = '';
                    break;
                case '02':
                    $codigo                         = '02';
                    $msg                            = trans('user::SolicitudDeTarjeta.rut no valido');
                    $RetornoFuncion                 = '';
                    $error = true;
                    break;
                case '03':
                    $codigo                         = '03';
                    $msg                            = trans('user::SolicitudDeTarjeta.rut es de un cliente');
                    $RetornoFuncion                 = '';
                    break;
                case '04':
                    $codigo                         = '04';
                    $msg                            = trans('user::SolicitudDeTarjeta.rut no valido');
                    $RetornoFuncion                 = '';
                    $error = true;
                    break;
                case '05':
                    $codigo                         = '05';
                    $msg                            = trans('user::SolicitudDeTarjeta.rut es de un cliente');
                    $RetornoFuncion                 = '';
                    break;
                case '99':

                    return [
                        'error'                         => false,
                        'msg'                           => trans('page::global.servicios no disponible'),
                        'datos' =>  [
                            'CodigoCliente' => '99',
                            'token'         => $token,
                            'msg'           => trans('page::global.servicios no disponible'),
                        ],
                    ];
                    break;
            }

            $respuesta['datos']             = array_map(function ($valor) {
                return str_replace([chr(2), chr(3)], '', $valor);
            }, $respuesta['datos']);

            $crea_registro                  = Carbon::now();
            $rut_sin_DV                     = substr($rut, 0, -1);
            $dv                             = substr($rut, -1, 1);

            $Solicitud = SolicitudTarjeta::create([
                'crea_registro'             => $crea_registro,
                'rut'                       => $rut_sin_DV,
                'dv'                        => $dv,
                /* 'escliente'                 => $respuesta['original-data'], */
                /* 'escliente_desc'            => $codigo, */
                'escliente'                 => $codigo,
                'escliente_desc'            => isset($respuesta['datos']) && isset($respuesta['datos'][2]) ? $respuesta['datos'][2] : "",
                'ABC31_TOKEN'               => $token,
            ]);

            $datos = [
                'error'                     => $error,
                'msg'                       => $msg,
                'datos' =>  [
                    'CodigoCliente'         => $codigo,
                    'token'                 => $token,
                    'idcliente'             => $Solicitud->id,
                    'msg'                   => $msg,
                ],
            ];
        } catch (\Exception $e) {
            return [
                'error'                             => true,
                'msg'                               => $e->getMessage(),
                'datos'                             => [],
            ];
        }

        return $datos;
    }

    /**
     * MA: formatea rut
    */
    protected function RutFormat($rut)
    {
        //quitar puntos . -
        //separa DV
        $rut                                        = str_replace('.', '', $rut);
        $rut                                        = str_replace('-', '', $rut);

        $dv                                         = substr($rut, -1, 1);

        if (!is_numeric($dv)) {
            $rut                                      = substr($rut, 0, -1) . strtoupper($dv);
        }

        return $rut;
    }

    public function EstadoCivil()
    {
        return EstadoCivil::pluck('ABC24_nombre', 'ABC24_codigo')->toArray();
    }
    public function ActivdadEconomica()
    {
        return ActivdadEconomica::pluck('ABC21_nombre', 'ABC21_codigo')->toArray();
    }

    protected function pruebaTC2000()
    {
        return WsSocket::TC2000EvaluacionExpressPrueba();
    }

    protected function EnvioEmail($email = "")
    {
        try {
            $datos                                  = [];
            \Mail::send('emails.SolicitudTarjeta', [
                'datos'                             => $datos,
            ], function ($message) use ($datos, $email) {
                $message->subject("Bienvenid@ a tu Tarjeta abcvisa!");
                /*  $message->subject("Solicitud de tarjeta aprobada"); */
                $message->from(env('MAIL_FROM_ADDRESS'));

                $message->to($email);
            });
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
