<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Illuminate\Http\Response;

//use Modules\User\Http\Requests\PublicPrivateLoginRequest;

class EstadoCuentaController 
{

    public function index()
    {
        
    
    
    //return $this->EstadoCuenta();
    //return $this->vencimientos();
    //$this->canal($ProcesoFlujo);
    // $this->UserChanel($ProcesoFlujo);

    //return 'estado de cuenta';
    }

    /**
     * Estado de cuenta 
     *
     * @return array
     */
    public function EstadoCuenta(){
         //espera de mas informacion
        //return $this->retorno(true,'espera de mas informacion WS no podemos llegar', []);
		$rs = [];
        $ProcesoFlujo = Session::get('TarjetaSeleccionada')['ValorProcesoFlujo'];
        $rut = Session::get('TarjetaSeleccionada')['RutTitular'];
        $respuesta =  WsSocket::SitioretfinInterfazCreaticketeecc($rut, $this->UserChanel($ProcesoFlujo));
       $ruta = Config::get("configSocket.EstadoCuenta");

        $rs['url'] =  $ruta['url']. 
            "RUT=" . $rut . 
            "&CANAL=" . $this->canal($ProcesoFlujo) . 
            "&TICKET=" .$respuesta['datos']['Ticket'];

       //$rs = "http://abcdin.jp2.cl:8091/ext/JRDNSLN01PR01/EstadoCuenta.aspx?RUT=".$rut."&CANAL=VISA&TICKET=".$respuesta['datos']['Ticket'];
        $rs['TimeSession'] = Session('TimeSession');

       /*  */
	
        return $rs;
    }



    public function cur($url){

        //iniciamos curl
        $ch = curl_init();
        
        //seteamos la url
        curl_setopt($ch, CURLOPT_URL, $url);
       
        /* //seteamos los datos post asociados a la variable xmlData
        $xml = trim(preg_replace("/\s+/", " ", $xml));
		$xml = "<?xml version='1.0' encoding='ISO-8859-1'?>\n" . 
				str_replace("> <", "><", $xml);
		//dd($xml);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml); */
     
            
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        //tiempo de espera, 0 infinito
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
         
        //ejecutamos la petición y asignamos el resultado a $data
   
        $data = curl_exec($ch);
        
        //cerramos la petición curl
        curl_close($ch);

        return $data;
    }
    /**
     * Estado de Cuenta: Vencimientos
     *
     * @return void
     */
    public function vencimientos(){
        	
        if($this->valicarIC()){
            return $this->retorno(true,'cuenta del cliente no es IC', []);
        }
        $rut = Session::get('TarjetaSeleccionada')['RutTitular'];
        $EmpresaId = Session::get('TarjetaSeleccionada')['CodigoEmpresa'];
        $Cuenta = Session::get('TarjetaSeleccionada')['Cuenta'];
        $PrimerVencimiento = Session::get('TarjetaSeleccionada')['PrimerVencimiento'];

        $respuesta = WsSocket::ConsultaFlujoEECC($EmpresaId,$Cuenta, $PrimerVencimiento, $Plazo = 0, $ValorCuota = 0 );

		return $this->retorno( $respuesta['error'], $respuesta['msg'],$respuesta['datos']);

    }

    protected function valicarIC(){
        if(Session::get('TarjetaSeleccionada')['OrigenDeLosDatos'] == 'IC'){
            return false;
        }
        return true;
    }
    
    /**
     * Ma: metodo para obtener el canal
     *
     * @param string $ProcesoFlujo
     * @return string
    */
    protected function canal($ProcesoFlujo){

        $canal = false;
        /* switch ($ProcesoFlujo) {
            case 'ABCDIN':
                $canal = 'DIN';
                break;
            case 'ABCVISA':
                $canal = 'VISA';
                break;
            case 'ABC':
                $canal = 'DIN';
                break;
        } */
        if ($ProcesoFlujo == "ABCVISA" || $ProcesoFlujo == "DIN") {
            if ($ProcesoFlujo == "ABCVISA") {
                 $canal= "VISA";
                } 
            else {
                 $canal= "DIN";
                }
            } 
        else {
             $canal= "ABC";
        }

		return $canal;
    }
    
    /**
     * Ma: metodo para obtener el UserChanel
     *
     * @param string $ProcesoFlujo
     * @return string
    */
    protected function UserChanel($ProcesoFlujo){

        $UserChanel = 22;
        
        if($ProcesoFlujo == 'ABCDIN' || $ProcesoFlujo == 'ABCVISA' ){
            $UserChanel = 1;
        }
        
		return $UserChanel;
    }

    /**
     * Ma: retorno
     *
     * @param boolean $error
     * @param string $msg
     * @param array $datos
     * @return json
     */
    protected function retorno($error, $msg, $datos){
        
        $datos= [
            'error' => $error,
            'msg'   => $msg,
            'datos' => $datos,
			'TimeSession'  => Session('TimeSession')
        ];
        
       return response()->json($datos);
    }
      
    protected function trazabilidad($info){
     
        WsSocket::Trazabilidad($info);
    }
}
