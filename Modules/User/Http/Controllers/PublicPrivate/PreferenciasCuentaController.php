<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;

use Illuminate\Http\Response;

/**
 * MA: movimientos no facturados del cliente
 * @author Miguelangel Gutierrez
 *  
 */

class PreferenciasCuentaController  
{
    public function index()
    {
      
      if($this->valicarIC()){
        return [
          'error' => false,
          'msj'  => 'cuenta del cliente no es IC',
        ];
      }
      
      dd($this->BuscarPreferenciasCuenta());
      
      return 'hola';
    } 
  
  
    /**
     * MA: valida si la cuenta es IC
     * @author Miguelangel Gutierrez
     * @return boolean 
	  */
    protected function valicarIC(){
      if(Session::get('DatosCLiente')['datos_usuario']['OrigenDeLosDatos'] == 'IC'){
        return false;
      }
      return true;
    }

    /**
     * MA: consulta al WS.
     *
     * @param [type] $RUT
     * @return Array Preferecias del usuario 
     */
    protected function BuscarPreferenciasCuenta(){
      $datos = WsSocket::getMaileecc(Session::get('DatosCLiente')['datos_usuario']['RutCliente']);
      return  $datos;
    }
}
