<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Illuminate\Http\Response;

use Modules\User\Http\Requests\BloqueoTarjetaRequest;

class BloqueoTarjetaController 
{

    public function index(BloqueoTarjetaRequest $request)
    {
        error_reporting(E_ALL);
        try {

            $Datos = [];
            $Datos['error'] = false;
            $Datos['msg'] = "";

            if($this->valicarIC()){

			    $Datos['error'] = true;
                $Datos['msg'] = "cuenta del cliente no es IC";
                $Datos['datos'] = [];
			    return  $Datos;
            }

            $respuesta = WsSocket::processMessage(Session::get('TarjetaSeleccionada')['RutTitular'], Session::get('TarjetaSeleccionada')['Cuenta'], Session::get('TarjetaSeleccionada')['NumeroTarjeta'] );
            $xml = new \SimpleXMLElement($respuesta['datos']['return']);
            $respuesta['datos']['return'] = (array) $xml;
            $respuesta['datos']['return']['BLOQUEOS'] = (array) $respuesta['datos']['return']['BLOQUEOS'];
            $datos = $respuesta['datos']['return'];

        if($datos['COD_ERROR'] == 0 && $datos['BLOQUEOS']['ESTADO'] == "ACTIVO"){
            $tarjeta_bloqueo = true;
        }
            //dd( $datos);
        $respuesta =  $this->wsBloqueodes($request->data);
        //$respuesta['msg'] = "prueba";
        
        } catch (\Exception $e) {

            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();
            return $Datos;
        }catch (\FatalErrorException $e) {

            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();
            return $Datos;
        }
        catch (\ErrorException $e) {

            $Datos['error'] = true;
            $Datos['msg'] = $e->getMessage();
            return $Datos;
        }

        return  $respuesta; 

    }
    

    protected function valicarIC(){
        if(Session::get('TarjetaSeleccionada')['OrigenDeLosDatos'] == 'IC'){
            return false;
        }
        return true;
    }

    protected function DatosTarjeta(){
        $tarjetas_seleccionada = Session::get('TarjetaSeleccionada');
        //dd($tarjetas_seleccionada);
        return  $tarjetas_seleccionada;
    }

    protected function wsBloqueodes($accion){
        
        if($accion){
            $accion = 'D';
        }else{
            $accion = 'B';
        }
        
        $parametros = array(
            'RUTCLIENTE'    => Session::get('TarjetaSeleccionada')['RutTitular'],
            'NROCUENTA'     => Session::get('TarjetaSeleccionada')['Cuenta'],
            'NROTARJETA'    => Session::get('TarjetaSeleccionada')['NumeroTarjeta'],
            'CODPRODUCTO'   => '24101',
            'CODIGOBLOQUEO' => '66',
            'ACCION'        => $accion,
            'FECHABLOQUEO'  => date('d-m-Y H:i'),
            'CODSUCURSAL'   => '601',
            'RUTUSUARIOACT' => Session::get('TarjetaSeleccionada')['RutTitular'],
        );
        
        $respuesta = WsSocket::wsBloqueodes($parametros);
        
        return $respuesta;
    }

    protected function trazabilidad($info){
       
         WsSocket::Trazabilidad($info);
    }
    
}
