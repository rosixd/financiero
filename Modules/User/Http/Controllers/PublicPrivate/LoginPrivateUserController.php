<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Illuminate\Http\Response;
use Modules\User\Entities\PublicUser\PublicUser;
use Modules\User\Http\Requests\PublicPrivateLoginRequest;
use Modules\User\Http\Requests\ConsultaTarjetasRequest;

class LoginPrivateUserController 
{

    protected $ConInfoCuenta3 = '';
    
    public $meses = [
        1 => 'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];

    public function index()
    {
        $rut = '108152494'; //IC
        $tarjeta =  '5012'; //IC
        //$rut = '143482049'; //TC;
        //$tarjeta =  '4229'; //TC

        $password = "5103";
        //"TipoTarjeta" => "TC"
        //"ValorProcesoFlujo" => "ABCDIN"
        
        
         return $this->login($rut, $tarjeta, $password );
        // return false;
    }

    public function validacionUsuarioRutPass()
    {

        $rut = '14.348.204-9';
        $rut = $this->RutFormat($rut);//Formatea Rut para el consumo de los WS

        $password = "5103";
        $password = "6787";

        //$datos_user =  $this->BuscarUser($rut); //Busca Usuario ConInfoCuenta3
        
        $datos =  WsSocket::EntRetfConsultaticketeccService($rut);

        if ($datos['datos']['CodError'] != 0) {
            $datos['error']  = true;
            $datos['msg']    = $datos['datos']['MsgError'];

            return response()->json($datos);
        }
        $_datos = [0];
        foreach ($datos['datos']['Tarjetas']['Tarjeta'] as $key => $value) {
            if ($value['Origen'] == 'TC') {
                $offset = $value['Offset'];
                break;
            }
        }

        //$offset = "0745FFFFFFFF"; //esto esta bueno para un ambiente
        //$offset = "1745FFFFFFFF";
        
        $csi = $this->csi($offset, $rut, $password);
	
        if ($csi === false) {
	        
            return [
                'error' => 'Error de CSI',
            ];
        }
	   
        return "Todo bien";

        // return false;
    }

    /**
     * MA: metodo para logueo de usuario
     *
     * @param request Rut y contraseña del usuario
     * @return session del usuario con sus datos
     */
    
    public function login(PublicPrivateLoginRequest $request){
        try {
            
            $estadoRespuesta = 200;
            $password = $request->password;
            $rut      = $this->RutFormat($request->rut);
            $tarjeta  = $request->tarjeta; 
            
            
            if(!is_numeric($password) || strlen($password) != 4 ){

                $datos = [
                    'error' =>  true,
                    'msg'   => trans('user::Autenticacion.clave no numericos'),
                    'estatus_cliente' => '02',
                    'datos' => [],
                    'abc' => false
                ];  
                return response()->json($datos);
            }
            
            $datos_user =  $this->BuscarUser($rut); //Busca Usuario ConInfoCuenta3
         
            if($datos_user['error']){
                unset($datos_user['original-data']);
                return response()->json($datos_user);
            } 
                
            if (!array_key_exists($tarjeta, $datos_user['datos']['tarjetas'])){
                return response()->json([
                    'datos' => [],
                    'error' => true,
                    'msg' => 'El usuario no tiene tarjetas',
                    'abc' => false
                ]);
            } 
            $ic = 0;
            $tc = 0;
	    
            foreach( $datos_user['datos']['tarjetas'] as $_tarjeta){
	  
                if($_tarjeta['OrigenDeLosDatos'] == 'TC'){
                    $tc++;
                }else if($_tarjeta['OrigenDeLosDatos'] == 'IC'){
                    $ic++;
                }
            }
		
            $datos_tarjeta = $datos_user['datos']['tarjetas'][$tarjeta];
            
        	if($datos_user['datos']['RutCliente'] != $datos_tarjeta['RutTitular'] ){
				return response()->json([
                    'error' =>  true,
                    'msg'   => trans('user::Autenticacion.clave no numericos'),
                    'estatus_cliente' => '02',
                    'datos' => [],
                    'abc' => false
                ]);
            }
            
            //define tipo de tarjeta del usuario
            /*  
                TC: Login del cliente RUT + Clave PINPAD.
                IC: Rut + clave internet.  
                "OrigenDeLosDatos" => "TC"
                "ValorProcesoFlujo" => "ABCDIN"
            */
       
            $TipoTarjeta = '';
            switch ($datos_tarjeta['ValorProcesoFlujo']) {
                case 'ABCDIN':
                    #  Si es tarjeta tradicional
                    $TipoTarjeta = 'TC';
                    break;
                case 'ABC':
                    # si es tarjeta cerrada con bancario...
                    $TipoTarjeta = 'TC';
                    break;
                case 'ABCVISA':
                    $TipoTarjeta = 'IC';
                    break;
            } 
            
            $offset = '';
            $ruta_private = '/';
            
            switch ($TipoTarjeta) {
                case 'IC':
                    $EstadoBoqueoCliente = WsSocket::TaEstadoBloqClienteService($rut);
                    
                    if($EstadoBoqueoCliente['error']){
                        return response()->json($EstadoBoqueoCliente);
                    } 

                    
                    if($EstadoBoqueoCliente['datos']['PINDICADOR_BLOQ_BLANDO'] == 1 || $EstadoBoqueoCliente['datos']['PINDICADOR_BLOQ_DURO'] == 1){

                        if($EstadoBoqueoCliente['datos']['PINDICADOR_BLOQ_BLANDO'] == 1 ){

                            $datos_user['msg'] = trans('user::Autenticacion.bloqueo blando');
                            $datos_user['estatus_cliente'] = '03';
                        
                        }else if($EstadoBoqueoCliente['datos']['PINDICADOR_BLOQ_DURO'] == 1){

                            $datos_user['msg'] = trans('user::Autenticacion.bloqueo duro');
                            $datos_user['estatus_cliente'] = '06';

                        }
                        $datos_user['error'] = true;
                        $datos_user['datos'] = '';
                        unset($datos_user['original-data']);
                        return response()->json($datos_user);
                    }

                    $datos =  WsSocket::EntRetfDatosclienteService($rut);
                    if($datos['error']){
                        return response()->json($datos);
                    }
                    if($datos['datos']['PN_COD_ERROR'] != 0){
                        $datos['error']  = true;
                        $datos['msg']    = $datos['datos']['PC_MENS_ERROR'];
                      
                        return response()->json($datos);
                    }

                    $offset = $datos['datos']['PC_COD_SEGURIDAD'];

                    if($offset == ''){
                        $datos['estatus_cliente']  = '04';
                        $datos['error']  = true;
                        $datos['msg']    = trans('user::Autenticacion.clave no numericos');
                     
                        return response()->json($datos);
                    }
                    $ruta_private =route('Public.private.ic.movimientos');
                    
                    break;
                case 'TC':

                    $datos =  WsSocket::EntRetfConsultaticketeccService($rut);
                    if($datos['error']){
                        return response()->json($datos);
                    }
                    if($datos['datos']['CodError'] != 0 ){
                        $datos['error']  = true;
                        $datos['msg']    = $datos['datos']['MsgError'];
                      
                        return response()->json($datos);
                    }
                    $__datos = [];
                    
                    if(!isset($datos['datos']['Tarjetas']['Tarjeta'][0])){
                        $datos['datos']['Tarjetas']['Tarjeta'] = [
                            $datos['datos']['Tarjetas']['Tarjeta']
                        ];
                    }
                    
                    foreach ($datos['datos']['Tarjetas']['Tarjeta'] as $key => $value) {
                        if (substr($value['Tarjeta'], -4) != $tarjeta) {
                            continue;
                        }
                        
                        if($value['Origen'] == 'TC'){
                            $offset = $value['Offset'];
                            break;
                        }
                    }
			
		            if($offset == '' && $ic == 0 ){
                        return  [
                            'error' => true,
                            'msg'   => 'Estimado cliente, este sitio web es exclusivo para tarjeta abcvisa. Cámbiate en cualquiera de nuestras tiendas abcdin',
                            'datos'   => [],
                            'ABC'	=> true
                        ];
                    } 
                   
                    if($offset == ''){
                        $datos['estatus_cliente']  = '04';
                        $datos['error']  = true;
                        $datos['msg']    = trans('user::Autenticacion.clave no numericos') ;

                        return response()->json($datos);
                    }
                    
                    $ruta_private = route('Public.private.pagarcuenta');
                
                break;
                default:
                    return  [
                        'error' => true,
                        'msg'   => 'Error tipo tarjeta',
                    ];
                break;
            }
            
            /**
             * viene la validacion CSI 
            */
            
            $csi = $this->csi($offset, $datos_tarjeta['NumeroTarjeta'], $rut, $password, $TipoTarjeta);
			
			if(env("SALTAR_CSI", false)){
				$csi = true;
			}
            
            if ($csi === false) {
                if($TipoTarjeta  == 'IC'){
                    WsSocket::EntRetfActualizaestatusclienteService($rut, 00);
                    $_datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 3, 
                        "deTransaccion"        => 'Incrementar Bloqueo', 
                        "cdError"              => '',
                        "cd_accion"            => 0,
                        "de_accion"            => 'Incrementar Bloqueo', 
                        "entradaTransaccion"   => "", 
                        "salidaTransaccion"    => "", 
                    ];
                    $this->trazabilidad($_datos);

                }

                $datos = [
                    'error' =>  true,
                    'msg'   => trans('user::Autenticacion.clave no numericos'),
                    'estatus_cliente' => '02',
                    'datos' => []
                ];
                return response()->json($datos);
            }

            if($TipoTarjeta  == 'IC'){
                WsSocket::EntRetfActualizaestatusclienteService($rut, 1);
            }
        
            $datos_personales = $this->datospersona($rut);
        
            if($datos_personales['error']){
                $datos_personales['msg'] = $datos_personales['msg'].' [DATOS PERSONALES]'; 
                $datos_personales['abc'] = false;
                return response()->json($datos_personales);
            }

            $tarjeta_bloqueo = false;

            //$response = WsSocket::processMessage($rut,$NROCUENTA,$NROTARJETA );
            $respuesta = WsSocket::processMessage($datos_tarjeta['RutTitular'], $datos_tarjeta['Cuenta'], $datos_tarjeta['NumeroTarjeta'], false);
            
            $xml = new \SimpleXMLElement($respuesta['datos']['return']);
        
            $respuesta['datos']['return'] = (array) $xml;
            $respuesta['datos']['return']['BLOQUEOS'] = (array) $respuesta['datos']['return']['BLOQUEOS'];
            
            $datos = $respuesta['datos']['return'];

            if($datos['COD_ERROR'] == 0 && $datos['BLOQUEOS']['ESTADO'] == "ACTIVO"){
                $tarjeta_bloqueo = true;
            }
            
            $random = str_random(40);
            session()->put('sessionID', $random);
            session()->put('Tipo', $TipoTarjeta );
            session()->put('DatosPersonales', $datos_personales);
            session()->put('TarjetaBloqueo', $tarjeta_bloqueo);
            session()->put('DatosCLiente', $datos_user); //datos clientes ConInfoCuenta3
            session()->put('TarjetaSeleccionada', $datos_tarjeta); //datos tarjeta seleccionada por el cliente
            session()->put('TimeSession', time());
            session()->put('tiempo', Config::get("SessionPublicPrivate.Time_Session"));								
            session()->put('tiempodealerta', Config::get("SessionPublicPrivate.Alert_Time"));								
            session()->put('Logueado', 'true');
            session()->put('index', true);
            
            $_datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 0, 
                "deTransaccion"        => 'Exito', 
                "cdError"              => '',
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => "", 
                "salidaTransaccion"    => "", 
               
            ];
            $this->trazabilidad($_datos);
            
            $datos = [
                'error'=> false,
                'datos' => [
                    'data' => false
                ],
                'msg' => ''
            ];
            
            session()->put('CuposTarjetas', $datos); 
            //session()->put('CuposTarjetas',  $this->CupoTarjeta($datos_tarjeta, $datos_user['datos'])); //busca y calcula cupos de tarjeta
           
            $_respuesta = [
                'error' => false,
                'msg'   => '',
                'estatus_cliente'   => '00',
                'url'   => $ruta_private,
                'abc'   =>false	
            ];

        } catch (\Exception $e) {

            $_respuesta = [
                'error' => false,
                'msg'   => $e->getMessage(),
                'estatus_cliente'   => '99',
                'Exception' => true,
                'url'   => '',
		        'abc'   =>false	
            ];

            $info = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 'Error desconocido', 
                "deTransaccion"        => 1, 
                "cdError"              => 99,
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => $rut, 
                "salidaTransaccion"    => $e->getMessage(), 
            ];
            $this->trazabilidad($info);

        }catch (\FatalErrorException $e) {
            $_respuesta = [
                'error' => false,
                'msg'   => $e->getMessage(),
                'estatus_cliente'   => '99',
                'url'   => '',
		        'abc'   =>false	
            ];

            
            $info = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 'Error desconocido', 
                "deTransaccion"        => 1, 
                "cdError"              => 99,
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => $rut, 
                "salidaTransaccion"    => $e->getMessage(), 
            ];
            $this->trazabilidad($info);
            
        }
        
        return response()
            ->json($_respuesta, $estadoRespuesta);
    }

	/**
	 * MA: Busqueda de informacion del usuario ConInfoCuenta3
	 * 
	 * @param String Rut
	 * @return Array Informacion del Cliente lista para login
    */
    protected function datospersona($rut){
        $datos = WsSocket::datospersona($rut);
       
        $datos['datos'] = $this->userdatos($datos['datos'], 'MW-datospersona');
        /* 
        if($datos['datos']['datos_usuario']['estatus_cliente'] == '99'){
            $datos['error'] = true;
            $datos['msg'] = 99;
        } */
        return $datos;
    }

	/**
	 * MA: Busqueda de informacion del usuario ConInfoCuenta3
	 * 
	 * @param String Rut
	 * @return Array Informacion del Cliente lista para login
	*/
    //public function BuscarUser (Response $request)
    public function BuscarUser ($rut)
    {
       // $_rut = $request->rut;
        $_rut = $rut;
        $datos_usuarios = false;

        $rut = $this->RutFormat($_rut);
        //se formatea el rut
        $datos_usuarios = WsSocket::ConInfoCuenta3($rut);	
        
        return $datos_usuarios;
    }

    /**
     * MA: meotod que busca y formatea tarjetas del cliene
     *
     * @param string $rut
     * @return array tarjtas
     */

    public function ConsultaTarjetas(ConsultaTarjetasRequest $request){
         try {
                
            $rut  = $this->RutFormat($request->rut);
            
            $datos_usuario =  $this->BuscarUser($rut);
		
            if($datos_usuario['error']){
                return response()->json($datos_usuario)->header("Content-type","application/json; charset=utf-8");
            	
			}
            $tarjetas = [];
            $estado = $datos_usuario['datos']['FuncionRetornoCodigo'];
            foreach ($datos_usuario['datos']['tarjetas'] as $key => $value) {    
                $TipoTarjeta = '';
                switch ($value['ValorProcesoFlujo']) {
                    case 'ABCDIN':
                        #  Si es tarjeta tradicional
                        $TipoTarjeta = 'TC';
                        break;
                    case 'ABC':
                        # si es tarjeta cerrada con bancario...
                        $TipoTarjeta = 'TC';
                        break;
                    case 'ABCVISA':
                        $TipoTarjeta = 'IC';
                        break;
                    default:
                        # code...
                        break;
                } 
                $tarjetas[] = [
                    'tarjeta' => strval($key),
                    'TipoTarjeta' => $TipoTarjeta,
                    'ValorProcesoFlujo' => $value['ValorProcesoFlujo']
                ];
            }
          
            $datos_usuario['datos'] = $tarjetas;
			
            $datos_usuario['estatus_cliente'] = $estado;
            //dd($datos_usuario['datos']);
		
            unset($datos_usuario['original-data']);

        } catch (\ErrorException $e) {
            $datos_usuario['error'] = true;
            $datos_usuario['msg'] =$e->getMessage();

            return response()->json($datos_usuario)->header("Content-type","application/json; charset=utf-8");

        } catch (\RuntimeException $e) {
            $datos_usuario['error'] = true;
            $datos_usuario['msg'] =$e->getMessage();
            
            return response()->json($datos_usuario)->header("Content-type","application/json; charset=utf-8");

        } catch (Exception $e) {
            $datos_usuario['error'] = true;
            $datos_usuario['msg'] =$e->getMessage();

		    return response()->json($datos_usuario)->header("Content-type","application/json; charset=utf-8");
        }

        return response()->json($datos_usuario)->header("Content-type","application/json; charset=utf-8");
    }
  	/**
   	 * MA: Formatear Rut para el web service
   	 * 
   	 * @param String Rut
   	 * @return String Rut formateado para el uso del servicio
    */
       
    protected function RutFormat ($rut)
    {
        //quitar puntos . -
        //separa DV
        $rut  = str_replace('.','', $rut);
        $rut  = str_replace('-','', $rut);

        $dv = substr($rut, -1, 1);

        if(!is_numeric($dv)){
          $rut = substr($rut, 0, -1).strtoupper($dv);
        }
       
        return $rut;
    }
  
    /**
   	 * MA: metodos se encarga de dar formato a los datos de usuarios
   	 * 
   	 * @param Array_DatosUsuarios Datos del usuarios
   	 * @param config nombre del formato
   	 * @return Array
   	*/
    protected function userdatos ($data = 0, $config)
    {
        $_datos_usuarios = Config::get("FormatoDataUser.".$config);
      
        $datos_usuarios = [];
      
        
        $datos_usuarios['original-data'] = $data; //datos originales de la consulta

        //valida el estatus el cliente
        $estatus = false;
       
        switch ($data[1]) {
            case '00':
                //dd('Cliente tiene cuenta ABCDIN');
                $estatus = 00;
                break;
            case '01':
                //dd('Cliente no tiene cuenta ABCDIN');
                $estatus = 01;
                break;
            case '99':
                //dd('Problemas de comunicación (Milliways)');
                $estatus = 99;
                break;
        }
         
        if (count($data) == 0) {
            foreach ($_datos_usuarios as $key => $datos_usuario) {
                $datos_usuarios[$datos_usuario] = "";
            }
        } elseif ($estatus == '00') {
		    //dd($data, $datos_usuarios);
	        foreach ($_datos_usuarios as $key => $datos_usuario) {
                if (isset($data[$key + 1])) {
                    $datos_usuarios[$datos_usuario] = $data[$key + 1];
                }
	        }
        }
		
        return [
            'datos_usuario'   => $datos_usuarios,
            'estatus_cliente' => $estatus 
        ];
    }
    /**
   	 * MA: metodos se encarga de calcular los cupos disponibles
   	 * 
   	 * @param Array_DatosUsuarios Datos del usuarios
   	 * @return Array 
    */
       
    //protected function CupoTarjeta($datos_tarjeta, $ConInfoCuenta3){
    public function CupoTarjeta(){

        error_reporting(E_ALL);
        try {
            $datos_tarjeta  = session::get('TarjetaSeleccionada');
            $ConInfoCuenta3 = session::get('DatosCLiente')['datos'];
    	    
            $error = false;
            $msg   = '';
            $datos = [];
            $CupoUtilizado = [];
            $DeudaPagar=[];
            $RutCliente           = $ConInfoCuenta3['RutCliente'];
            $CupoTotaldeLaCuenta  = $datos_tarjeta['CupoTotaldeLaCuenta'];
            $SaldoDisponible      = $datos_tarjeta['SaldoDisponible'];
            $DisponibleAvance     = $datos_tarjeta['DisponibleAvance'];
            $FactoresSobrecupo    = $datos_tarjeta['FactoresSobrecupo'];
            $OrigenDeLosDatos     = $datos_tarjeta['OrigenDeLosDatos'];
            
            $CupoUtilizado = $this->CuposUtilizados($OrigenDeLosDatos, $datos_tarjeta, $CupoTotaldeLaCuenta, $SaldoDisponible);
    
            $ultimostarjeta= 0000;
            if (array_key_exists("NumeroTarjeta",$datos_tarjeta)){
                $ultimostarjeta = substr($datos_tarjeta['NumeroTarjeta'],-4);
            }else{
                $ultimostarjeta = str_pad($indece, 4, "0", STR_PAD_LEFT);
            }
            
            $ExtracupoTienda = $this->ExtracupoTienda($RutCliente, $SaldoDisponible, $CupoTotaldeLaCuenta, $FactoresSobrecupo, $OrigenDeLosDatos);
            $DisponibleOtrosComercios = $this->DisponibleOtrosComercios($datos_tarjeta, $OrigenDeLosDatos);
            $ExtracupoAvance = $this->ExtracupoAvance($OrigenDeLosDatos, $datos_tarjeta,$RutCliente);

            if(!$ExtracupoTienda['error'] && !$DisponibleOtrosComercios['error'] && !$ExtracupoAvance['error']){
                $_datos = [
                    "rut"                  => $RutCliente, 
                    "cdTransaccion"        => 0,
                    "deTransaccion"        => 'Exito',
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'CUPOS',
                    "entradaTransaccion"   => "",
                    "salidaTransaccion"    => "",
                   
                ];
                $this->trazabilidad($_datos);
            }

            $datos = [
                'CuposUtilizados'            => $CupoUtilizado['datos']['CupoUtilizado'],
                'porcentaje'                 => $CupoUtilizado['datos']['porcentaje'],
                'ExtracupoTienda'            => $ExtracupoTienda,
                'DisponibleOtrosComercios'   => $DisponibleOtrosComercios,
                'ExtracupoAvance'            => $ExtracupoAvance,
                'CupoTotal'                  => number_format($CupoTotaldeLaCuenta, 0, ',', '.'),
                'DisponibleTiendas'          => number_format($SaldoDisponible, 0, ',','.'),
                'DisponibleAvanceEnEfectivo' => number_format($DisponibleAvance, 0,',','.'),
                'ultimostarjeta'             => $ultimostarjeta,
                'DeudaPagar'                 => $this->DeudaPagar($datos_tarjeta, $OrigenDeLosDatos),
                'data'                       => true,
            ];
          
             
        } catch (\Exception $e) {

            $info = [
                "rut"                  => $RutCliente, 
                "cdTransaccion"        => 1, 
                "deTransaccion"        => 'Error desconocido', 
                "cdError"              => '',
                "cd_accion"            => 1,
                "de_accion"            => 'Cupos', 
                "entradaTransaccion"   => $RutCliente, 
                "salidaTransaccion"    =>  $e->getMessage(), 
            ];
            $this->trazabilidad($info);

            return [
                'error' => true,
                'datos' =>  $datos,
                'msg'   => $e->getMessage()
            ];
        }catch (\FatalErrorException $e) {
            $info = [
                "rut"                  => $RutCliente, 
                "cdTransaccion"        => 1, 
                "deTransaccion"        => 'Error desconocido', 
                "cdError"              => '',
                "cd_accion"            => 1,
                "de_accion"            => 'Cupos', 
                "entradaTransaccion"   => $RutCliente, 
                "salidaTransaccion"    =>  $e->getMessage(), 
            ];
            $this->trazabilidad($info);

            return [
                'error' => true,
                'datos' => $datos,
                'msg'   => $e->getMessage()
            ];
        }catch (\FatalThrowableError $e) {
            $info = [
                "rut"                  => $RutCliente, 
                "cdTransaccion"        => 1, 
                "deTransaccion"        => 'Error desconocido', 
                "cdError"              => '',
                "cd_accion"            => 1,
                "de_accion"            => 'Cupos', 
                "entradaTransaccion"   => $RutCliente, 
                "salidaTransaccion"    =>  $e->getMessage(), 
            ];
            $this->trazabilidad($info);

            return [
                'error' => true,
                'datos' => $datos,
                'msg'   => $e->getMessage()
            ];
        
        }catch (\ErrorException $e) {
            $info = [
                "rut"                  => $RutCliente, 
                "cdTransaccion"        => 1, 
                "deTransaccion"        => 'Error desconocido', 
                "cdError"              => '',
                "cd_accion"            => 1,
                "de_accion"            => 'Cupos', 
                "entradaTransaccion"   => $RutCliente, 
                "salidaTransaccion"    =>  $e->getMessage(), 
            ];
            $this->trazabilidad($info);

            return [
                'error' => true,
                'datos' => $datos,
                'msg'   => $e->getMessage()
            ];
        }

        $salida  = [
            'error' => $error,
            'datos' => $datos,
            'msg'   => $msg
        ];
        
        session()->put('CuposTarjetas',  $salida); //busca y calcula cupos de tarjeta
        return $salida;

    }
    
    public function ExtracupoTienda($RutCliente, $SaldoDisponible, $CupoTotaldeLaCuenta, $FactoresSobrecupo, $OrigenDeLosDatos){
        
        error_reporting(E_ALL);
        try {
            $respuesta = [
                'error' =>  false,
                'datos' => [],
                "estatus_cliente" =>'',
                'msg'   => '',
            ];

            if($FactoresSobrecupo != ''){
                $respuesta = WsSocket::SitioretfinInterfazCalculoextracupo($RutCliente, $SaldoDisponible, $CupoTotaldeLaCuenta, $FactoresSobrecupo, $OrigenDeLosDatos);
            }
                
        } catch (Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            return $respuesta;
        }catch (FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            return $respuesta;
        }
        
        return  $respuesta;
    }

    public function CuposUtilizados($OrigenDeLosDatos, $datos_tarjeta, $CupoTotaldeLaCuenta, $SaldoDisponible){
        error_reporting(E_ALL);
        try {
            $error = true;
            $msg = '';
            $CupoUtilizado = [
                'CupoUtilizado' => 0,
                'porcentaje'    => 0,
            ];
			$_CupoUtilizado = 0;
            if($OrigenDeLosDatos == 'IC'){
                $movimientos = WsSocket::EECC_TAR( $datos_tarjeta['NumeroTarjeta'], 'cupoutilizadotarjeta', $datos_tarjeta['RutTitular'], false );
                $arrMovimientos = explode("\n", $movimientos["datos"]);
                $data = [];
                $error = $movimientos['error'];
                $msg = $movimientos['msg'];
               
                if(!$error){
                    foreach ($arrMovimientos as $linea) {
                        preg_match('/\|([\w\s\.]+)\s*[\+\-\=]?\:\s*([\w\d\.\,\/\s\:]+).+\|/', $linea, $match);
                        if (!empty($match)) {
                            $data[str_slug($match[1])] = trim($match[2]);
                        }
                    }
                    $_CupoUtilizado = str_replace(',', '',$data['cupo-utilizado']);

                    $CupoUtilizado['CupoUtilizado'] =   number_format($_CupoUtilizado, 0,',','.');
                }
              
            }
    
            if($OrigenDeLosDatos == 'TC'){
                //cupo utilizado de la tarjeta
               if($CupoTotaldeLaCuenta != '' && $SaldoDisponible != ''){
                   $error = false;
                   $_CupoUtilizado = $CupoTotaldeLaCuenta - $SaldoDisponible;

                   $CupoUtilizado['CupoUtilizado'] =  number_format($_CupoUtilizado, 0,',','.');
               }
            }
            $porcentaje  = 0;
	    
			if( $CupoTotaldeLaCuenta != 0 ){
			 	$porcentaje  = ($_CupoUtilizado * 100)/$CupoTotaldeLaCuenta;
			}
           
            $CupoUtilizado['porcentaje'] = intval($porcentaje);

        } catch (Exception $e) {
            return [
                'error' => true,
                'datos' => $CupoUtilizado,
                'msg'   => $e->getMessage()
            ];
        }catch (FatalErrorException $e) {
            return [
                'error' => true,
                'datos' => $CupoUtilizado,
                'msg'   => $e->getMessage()
            ];
        }catch (FatalThrowableError $e) {
            return [
                'error' => true,
                'datos' => $CupoUtilizado,
                'msg'   => $e->getMessage()
            ];
        }

        return [
            'error' => $error,
            'datos' => $CupoUtilizado,
            'msg'   => $msg
        ];

    }

    public function DisponibleOtrosComercios($datos_tarjeta, $OrigenDeLosDatos){
        error_reporting(E_ALL);
        try {
           
            $msg = '';
            $valor_TcObtDisponible =  [];
            //Recupera Cupo otros comercios

            $respuesta_TcObtDisponible = WsSocket::TcObtDisponible($datos_tarjeta['CodigoEmpresa'], $datos_tarjeta['Cuenta'], $datos_tarjeta['Clasificacion'], $OrigenDeLosDatos,$datos_tarjeta['RutTitular'] );
            
            $error = $respuesta_TcObtDisponible['error'];
            $msg = $respuesta_TcObtDisponible['msg'];

            if($respuesta_TcObtDisponible['datos']['1'] == 99){
                 $error = true;
                 $msg = trans('page::global.servicios no disponible');
            }else if($respuesta_TcObtDisponible['datos']['1'] == 1){
                 $error = true;
                 $msg = "1 : Error, Avance no puede realizarse";
            }else if($respuesta_TcObtDisponible['datos']['1'] == 00){
            
                $valor_TcObtDisponible[] =  $respuesta_TcObtDisponible['datos']['2'];
            } 

            $valor_TcObtDisponible[0] = number_format($valor_TcObtDisponible[0], 0,',','.');

        } catch (\Exception $e) {
            return [
                'error' => true,
                'datos' => $valor_TcObtDisponible,
                'msg'   => $e->getMessage()
            ];
        }catch (\FatalErrorException $e) {
            return [
                'error' => true,
                'datos' => $valor_TcObtDisponible,
                'msg'   => $e->getMessage()
            ];
          
        }

        return [
            'error' => $error,
            'datos' => $valor_TcObtDisponible,
            'msg'   => $msg
        ];
    }

    public function ExtracupoAvance($OrigenDeLosDatos, $datos_tarjeta, $rut= 19){
        error_reporting(E_ALL);
        try {
            
            $ExtracupoAvance = [
                'error' => false,
                'datos' => [],
                'msg'   => ''

            ];

            if($OrigenDeLosDatos == 'IC'){
                //extracupo avance
                //10s
                $respuesta_OBTENERDUPLICA = WsSocket::OBTENERDUPLICA($datos_tarjeta['CodigoEmpresa'],$datos_tarjeta['Cuenta'], $rut);
               
                if($respuesta_OBTENERDUPLICA['error']){
                    $error = true;
                    $msg = $respuesta_OBTENERDUPLICA['msg'];
                }else{
                    $stringXml = $respuesta_OBTENERDUPLICA['datos']['1'];
                    $factoresXML = new SimpleXMLElement($stringXml);
                    $_ExtracupoAvance = json_decode(json_encode($factoresXML), true);
                    $ExtracupoAvance['datos'] =  $_ExtracupoAvance['@attributes'];
                    $ExtracupoAvance['datos']['tope'] = number_format($ExtracupoAvance['datos']['tope'], 0, ',', '.');
                    $ExtracupoAvance['datos'] = [$ExtracupoAvance['datos']];
                }
                $ExtracupoAvance['error'] = $respuesta_OBTENERDUPLICA['error'];
                $ExtracupoAvance['msg'] = $respuesta_OBTENERDUPLICA['msg'];

            }

        } catch (\Exception $e) {


            $ExtracupoAvance['error'] = true;
            $ExtracupoAvance['msg'] = $e->getMessage();

            return $ExtracupoAvance;
        }catch (\FatalErrorException $e) {
            
            $ExtracupoAvance['error'] = true;
            $ExtracupoAvance['msg'] = $e->getMessage();

            return $ExtracupoAvance;
            
        }
        return $ExtracupoAvance;

    }

    public function DeudaPagar($datos_tarjeta, $OrigenDeLosDatos){
        error_reporting(E_ALL);
        try {
            $respuesta_totobligdia5 = [];
            $DeudaPagar=[];
            //REQ3 B) Deuda por Pagar
            //10s
            $error = false;
            $msg = '';
            $respuesta_totobligdia5= WsSocket::totobligdia5($datos_tarjeta['CodigoEmpresa'], $datos_tarjeta['NumeroTarjeta'], $OrigenDeLosDatos, $datos_tarjeta['RutTitular']);
         
            if($respuesta_totobligdia5['error']){
                $error = true;
                $msg = $respuesta_totobligdia5['msg'];
            }else{
                $DeudaPagar =[
                    'MontoFacturado'      => number_format($respuesta_totobligdia5['datos']['DeudaFacturada'], 0, ',', '.') ,
                    'FechaVencimiento'    => $respuesta_totobligdia5['datos']['FechaVencimiento'],
                    'DeudaTotal'          => number_format($respuesta_totobligdia5['datos']['DeudaTotal'], 0, ',', '.'),
                ];
            }

        } catch (\Exception $e) {

            return [
                'error' => true,
                'datos' =>  $DeudaPagar,
                'msg'   => $e->getMessage()
            ];
        }catch (\FatalErrorException $e) {
            
            return [
                'error' => true,
                'datos' => $DeudaPagar,
                'msg'   => $e->getMessage()
            ];
            
        }
        return [
            'error' => $error,
            'datos' => $DeudaPagar,
            'msg'   => $msg
        ];
    }

    /**
   	 * MA: Destruye session logout 
   	 * 
   	 * 
   	 * @return redirect home 
    */
    public function logout($redireccion = true){
        Session::flush();

        if($redireccion){
            return redirect('/');
        }
        
    }
    /**
     * MA y AM : metodo para invicacion de CSI
     *
     * @param string $offset
     * @param string $rut
     * @param string $password
     * @return bool
     */
    protected function csi($offset, $tarjeta, $rut, $password, $TipoTarjeta)
    {
        return WsSocket::CSILogin($offset, $tarjeta, $rut, $password, $TipoTarjeta);
    }

    public function prueba(){
        dd('hola mundo!!!!!!!',session::all());
    }

    public function renovar(){
        $error = true;
        $tiempo = 0;
        if(session('Logueado')){
            $tiempo = time();
            session()->put('TimeSession', $tiempo);
            $error = false;
        }
         
        return [
            'error'  => $error,
            'tiempo' =>  $tiempo
        ];
    }

    
    protected function trazabilidad($info){
     
        WsSocket::Trazabilidad($info);
    }
}
