<?php
namespace Modules\User\Http\Controllers\PublicPrivate;

use Auth;
use Session;
use WsSocket;
use Config;
use SimpleXMLElement;
use Illuminate\Http\Response;


class PagaTuCuentaController 
{

    public function index()
    {
        $rut   = WsSocket::aesExec('Encriptar', Session::get('TarjetaSeleccionada')['RutTitular']);
        
        $TipoTarjeta = '';
        switch (Session::get('TarjetaSeleccionada')['ValorProcesoFlujo']) {
            case 'ABCDIN':
            #  Si es tarjeta tradicional
            $TipoTarjeta = 'TC';
            break;
            case 'ABC':
            # si es tarjeta cerrada con bancario...
            $TipoTarjeta = 'TC';
            break;
            case 'ABCVISA':
            $TipoTarjeta = 'IC';
            break;
            default:
            # code...
            break;
        } 
        $tipo_tarjeta = WsSocket::aesExec('Encriptar',$TipoTarjeta);

        $conexion = Config::get("configSocket.pagatucuenta");

        $num_tarjeta  = WsSocket::aesExec('Encriptar', Session::get('TarjetaSeleccionada')['NumeroTarjeta']);
        
        $url_base = $conexion['url']."?R=".$rut."&T=".$tipo_tarjeta."&N=".$num_tarjeta;
    
        $datos  = [
            'error' => false,
            'msg'   => "",
            'data'  => [
                "url" => $url_base
            ],
        ];

        return $datos;
    }

}
