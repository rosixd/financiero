<?php

namespace Modules\User\Http\Controllers\PublicPrivate;

use Illuminate\Http\Response;
use Session;
use WsSocket;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * MA: movimientos no facturados del cliente
 */
class MovimientosNoFacturadosController
{
    public $meses = [
     1 => 'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];

    public function index()
    {
        if ($this->validarIC()) {
            $datos= [
              'error'       => false,
              'msj'         => 'cuenta del cliente no es IC',
              'TimeSession' => Session('TimeSession'),
            ];

            return response()->json($datos);
        }

               $NumeroTarjeta = Session::get('TarjetaSeleccionada')['NumeroTarjeta'];
               $datos         = $this->BuscarMovimientosNoFacturados($NumeroTarjeta);
        $datos['TimeSession'] = Session('TimeSession');

        if (empty($datos['datos'])) {
            $datos['data'] = false;
        }

        return response()->json($datos);
    }
    /**
     * MA: valida si la cuenta es IC
     * @return boolean
      */
    protected function validarIC()
    {
        if (Session::get('TarjetaSeleccionada')['OrigenDeLosDatos'] == 'IC') {
            return false;
        }

        return true;
    }

    protected function BuscarMovimientosNoFacturados($NumeroTarjeta)
    {
        $movimientos = WsSocket::EECC_TAR($NumeroTarjeta, 'movimientosnofacturados', Session::get('TarjetaSeleccionada')['RutTitular']);
        if ($movimientos['data']) {
            $movimientos['datos'] = $this->FormatMovimientosNoFacturados($movimientos['datos']);
        }
        unset($movimientos['original-data']);

        return  $movimientos;
    }

    /**
     * MA: Metodo de conexion para formatear movimientos no facturados
     * @param string $datos
     * @return array $datos
     */
    protected function FormatMovimientosNoFacturados($datos)
    {
        $movimientos = '';

        $valores = explode("\n", $datos);

        $inicio  = false;
        $bandera = 0;

        for ($i = 0, $c = count($valores); $i < $c; $i++) {
            $linea = $valores[$i];

            if ($bandera === 0 && preg_match('/\-/', $linea)) {
                $bandera = 'Inicion';

                continue;
            }

            if ($bandera === 0) {
                continue;
            }

            if (preg_match('/\-/', $linea) && $inicio) {
                break;
            }
            $inicio = true;
            if ($inicio) {
                $movimientos .= $linea . "\n";
            }
        }

        $movimientos = explode("\n", $movimientos);

        $data  = [];
        $index = 0;
        $filas = [];

        foreach ($movimientos as $key => $value) {
            $data =  explode(
          "|",
        substr($movimientos[$key], 1, -1)
      );

            if (array_key_exists($index, $filas)) {
                $filas[ $index ] = array_merge($filas[ $index ], $data);
            } else {
                $filas[ $index ] = $data;
            }

            if ($key > 0 && ($key %2) != 0) {
                $index++;
            }
        }

        $movimientos = [];

        foreach ($filas as $key => $value) {
            if (count($value) == 10) {
                $fecha = '';

                $_fecha = explode("/", trim($value[5]));

                $fecha = $_fecha[0] . ' ' . $this->meses[intval($_fecha[1])] . ' ' . ($_fecha[2] > date('Y') + 2 ? '19' : '20') . $_fecha[2];
                
                if ($this->tipomovimiento(trim($value[0]))) {
                    $movimientos[] = [
                    'TipMov'     => trim($value[0]),
                    'Fecha'      => $fecha,
                    'Sucursal'   => trim($value[6]),
                    'Monto'      => trim($value[7]),
                    'Cuota'      => trim($value[8]),
                    'ValorCuota' => trim($value[9]),
                    'NumeroDocumento' => trim($value[1]),
                ];
                }
            }
        }

        return $movimientos;
    }

    /**
     *MA: TIPO DE MOVIMIENTO
     *
     * @param string $tipo
     * @return string
    */
    protected function tipomovimiento($tipo = '')
    {
        switch ($tipo) {
      case 'COMPRA': 
        $resultado = true;
        break;
      case 'PREPAG': 
        $resultado = false;
        break;
      case 'GC': 
        $resultado = false;
        break;
      case 'G.C': 
        $resultado = false;
        break;
      case 'AJUSTE': 
        $resultado = false;
        break;
      case 'PAGO': 
        $resultado = true;
        break;
      case 'REPACT': 
        $resultado = true;
        break;
      case 'CANCEL': 
        $resultado = false;
        break;
      case 'AVANCE': 
        $resultado = true;
        break;
      case 'PAT': 
        $resultado = true;
        break;
      case 'RENEGO': 
        $resultado = true;
        break;
      case 'PAGO-R': 
        $resultado = false;
        break;
      case 'INTERES': 
        $resultado = false;
        break;
      case 'ITE': 
        $resultado = false;
        break;

      default: 
        $resultado = '';
        break;
    }

        return $resultado;
    }

    protected function trazabilidad($info)
    {
        /*  $info = [
             "rut"                => '19',
             "idSesion"           => '19',
             "numTelefono"        => '19',
             "cdCanal"            => '',
             "cdTienda"           => '19',
             "cdTransaccion"      => '19',
             "deTransaccion"      => '19',
             "cdAplicacion"       => '19',
             "cdError"            => '19',
             "entradaTransaccion" => '19',
             "salidaTransaccion"  => '19',
         ]; */
        WsSocket:: Trazabilidad($info);
    }

    /**
     *RH: Método para generar excel de movimientos no facturados.
     *
     * @version 1.0.1
     * 
    */
    public function generarExcel()
    {
        try{
            ini_set('memory_limit', '2048M');
            set_time_limit(0);
    
            $_datos = json_decode(json_encode($this->index()), true);

            if( !isset($_datos) && !isset( $_datos['original'] ) && !isset( $_datos['original']['datos'] ) ){
                return false;
            }

            $datos = $_datos['original']['datos'];
            
            $columnas = [
                'Tipo Transacción',
                'Fecha',
                'Comercio',
                'Monto',
                'N° Cuotas',
                'Valor Cuota',
                'N° Documento',
            ];
            
            $spreadsheet = new Spreadsheet();
            
            $filename = 'MovimientosNofacturados.xlsx';

            $sheet = $spreadsheet->getActiveSheet();

            array_unshift($datos, $columnas);

            $sheet->fromArray($datos, null, 'A1');

            $writer = new Xlsx($spreadsheet);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $filename .'"');
            header('Cache-Control: max-age=0');
            
            $writer->save("php://output");
        }catch (\Exception $e) {

            return $e;
        }

    }
}
