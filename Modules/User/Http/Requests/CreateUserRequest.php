<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/i',
            'roles' => 'required',
            'password' => 'regex:/^[\w]+$/si|confirmed',
	        'password_confirmation' => 'regex:/^[\w]+$/si',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'last_name.required' => 'Sr. Usuario, debe ingresar al menos un apellido.',
            'email.required' => 'Sr. Usuario, debe ingresar un correo electrónico.',
            'email.unique' => 'Sr. Usuario, ya existe correo electrónico ingresado.',
            'email.regex' => 'Sr. Usuario, el correo electrónico ingresado no es válido.',
            'roles.required' => 'Sr. Usuario, debe ingresar un perfil.',
            'password.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
            'password_confirmation.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
            'password.confirmed' => 'Sr. Usuario, las contraseñas no coinciden, por favor ingréselas nuevamente.',
	    
        ];
    }
}
