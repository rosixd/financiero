<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolesRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => "required",
            'slug' => "unique:roles,slug",
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => "Sr. Usuario, debe ingresar el  Nombre.",
            'slug.unique' => "Sr. Usuario, el valor del campo Nombre ya está en uso.",
        ];
    }
}
