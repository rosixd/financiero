<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Contracts\Authentication;

class UpdateProfileRequest extends FormRequest
{
    public function rules()
    {
        $userId = app(Authentication::class)->id();

        return [
/*             'email' => "required|email|unique:users,email,{$userId}",
            'first_name' => 'required',
            'last_name' => 'required',*/
            'password' => 'regex:/^[\w]+$/si|confirmed', 
            'password_confirmation' => 'regex:/^[\w]+$/si',
            'email' => "required|email|unique:users,email,{$userId}|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/i",
            'first_name' => 'required',
            'last_name' => 'required',
            'roles' => 'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'last_name.required' => 'Sr. Usuario, debe ingresar al menos un apellido.',
            'email.required' => 'Sr. Usuario, debe ingresar un correo electrónico.',
            'email.unique' => 'Sr. Usuario, ya existe correo electrónico ingresado.',
            'roles.required' => 'Sr. Usuario, debe ingresar un perfil.',
            'password.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
            'password_confirmation.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
            'password.confirmed' => 'Sr. Usuario, las contraseñas no coinciden, por favor ingréselas nuevamente.',
        ];
    }
}
