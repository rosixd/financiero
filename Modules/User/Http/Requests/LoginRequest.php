<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/i',
            'password' => 'required|regex:/^[\w]+$/si',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'email.required' => 'Sr. Usuario, debe ingresar el correo electr&oacute;nico para acceder.',
            'password.required' => 'Sr. Usuario, debe ingresar su contrase&ntilde;a para acceder',
            'email.regex' => 'Sr. Usuario, debe ingresar un correo electr&oacute;nico v&aacute;lido.',
            'password.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
	    ];
    }
}
