<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    public function rules()
    {
        //$role = $this->route('role');
        $role = $this->route()->parameter('roles');
	
        return [
            'name' => 'required',
            'slug' => "unique:roles,slug,{$role}",
			//'slug' => 'required|unique:roles,slug',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => 'Sr. Usuario, el campo nombre es obligatorio.',
			'slug.unique' => "Sr. Usuario, el valor del campo Nombre ya está en uso.",
        ];
    }
}
