<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetCompleteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'regex:/^[\w]+$/si|required|confirmed',
            'password_confirmation' => 'regex:/^[\w]+$/si|required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'password.required' => 'Sr. Usuario, debe ingresar la contrase&ntilde;a solicitada.',
	        'password.confirmed' => 'Sr. Usuario, las contrase&ntilde;as no coinciden, por favor ingr&eacute;selas nuevamente.',
            'password_confirmation.required' => 'Sr. Usuario, debe ingresar nuevamente la contrase&ntilde;a.',
            'password.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
            'password_confirmation.regex' => 'Sr. Usuario, favor ingresar solo caracteres alfanuméricos.',
	];
    }
}
