<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required|unique:roles,slug',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
			'name.required' => 'Sr. Usuario, el campo nombre es obligatorio.',
        		'slug.unique' => "Sr. Usuario, el valor del campo Nombre ya está en uso.",
		];
    }
}
