<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group([ 'prefix' => '/user'
    , 'middleware' => [
        // 'api.token', 
        'auth.admin'
    ]], function (Router $router) {
    $router->group(['prefix' => 'roles'], function (Router $router) {
        $router->bind('role', function ($id) {
            return app(\Modules\User\Repositories\RoleRepository::class)->find($id);
        });
        $router->get('/', [
            'as' => 'api.user.role.index',
            'uses' => 'RoleController@index',
        ]);
        $router->get('all', [
            'as' => 'api.user.role.all',
            'uses' => 'RoleController@all',
        ]);
        $router->post('/', [
            'as' => 'api.user.role.store',
            'uses' => 'RoleController@store',
        ]);
        $router->post('find/{role}', [
            'as' => 'api.user.role.find',
            'uses' => 'RoleController@find',
        ]);
        $router->post('find-new', [
            'as' => 'api.user.role.find-new',
            'uses' => 'RoleController@findNew',
        ]);
        $router->post('{role}/edit', [
            'as' => 'api.user.role.update',
            'uses' => 'RoleController@update',
        ]);
        $router->delete('{role}', [
            'as' => 'api.user.role.destroy',
            'uses' => 'RoleController@destroy',
        ]);
    });

    $router->group(['prefix' => 'users'], function (Router $router) {
        $router->bind('user', function ($id) {
            return app(\Modules\User\Repositories\UserRepository::class)->find($id);
        });
        $router->get('/', [
            'as' => 'api.user.user.index',
            'uses' => 'UserController@index',
        ]);
        $router->post('/', [
            'as' => 'api.user.user.store',
            'uses' => 'UserController@store',
        ]);
        $router->post('find/{user}', [
            'as' => 'api.user.user.find',
            'uses' => 'UserController@find',
        ]);
        $router->post('find-new', [
            'as' => 'api.user.user.find-new',
            'uses' => 'UserController@findNew',
        ]);
        $router->post('{user}/edit', [
            'as' => 'api.user.user.update',
            'uses' => 'UserController@update',
        ]);
        $router->get('{user}/sendResetPassword', [
            'as' => 'api.user.user.sendResetPassword',
            'uses' => 'UserController@sendResetPassword',
        ]);
        $router->delete('{user}', [
            'as' => 'api.user.user.destroy',
            'uses' => 'UserController@destroy',
        ]);
    });

    $router->group(['prefix' => '/account'], function (Router $router) {
        $router->get('profile', [
            'as' => 'api.account.profile.find-current-user',
            'uses' => 'ProfileController@findCurrentUser',
        ]);
        $router->post('profile', [
            'as' => 'api.account.profile.update',
            'uses' => 'ProfileController@update',
        ]);

        $router->bind('userTokenId', function ($id) {
            return app(\Modules\User\Repositories\UserTokenRepository::class)->find($id);
        });

        $router->get('api-keys', [
            'as' => 'api.account.api.index',
            'uses' => 'ApiKeysController@index',
        ]);
        $router->get('api-keys/create', [
            'as' => 'api.account.api.create',
            'uses' => 'ApiKeysController@create',
        ]);
        $router->delete('api-keys/{userTokenId}', [
            'as' => 'api.account.api.destroy',
            'uses' => 'ApiKeysController@destroy',
        ]);
    });

    $router->get('permissions', [
        'as' => 'api.user.permissions.index',
        'uses' => 'PermissionsController@index',
    ]);
});

//Rutas para recursos
$router->group([ 'prefix' => 'resource' ], function (Router $rutas) {
    
    //estado de cuenta
    $rutas->get('estado_cuenta', [
        'as' => 'api.resource.estado_cuenta',
        'uses' => 'EstadoCuentaController@req05',
    ]);

    //pruebas miliways
    $rutas->get('test_miliways', [
        'as' => 'api.resource.test_miliways',
        'uses' => 'EstadoCuentaController@test_miliways',
    ]);

    //ruta para transformar pdfs en imagenes
    $rutas->post('getPdfImg', [
        'as' => 'api.resource.getPdfImg',
        'uses' => 'ResourceController@getPdfImg',
    ]);
});
