<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Config;

class PublicUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if(Session('Logueado')){
            $time_Session =  time() - Session('TimeSession');
          
            if($time_Session > Config::get("SessionPublicPrivate.Time_Session")){
                session()->put('Logueado', 'false');
                Session::flush();
                return redirect()->To('/');

            }

            session()->put('TimeSession', time());
            return $next($request);
        }
        
        session()->put('Logueado', 'false');
        Session::flush();
        return redirect()->To('/');
    }
}
