@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="panel-title">
                            Test de servicios
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                            <ul class="nav nav-pills nav-stacked" id="reqTestService">
                                @if( isset($requerimientos) && count($requerimientos) > 0 )
                                    @foreach( $requerimientos as $indice => $requerimiento )
                                        <li>
                                            <a href="#" data-id="{{ $indice }}">
                                                {{ $requerimiento }}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="resultTestService" style="overflow:  auto;">
                       		
					    </div>   
                    </div>   
                </div>
            </div>        
        </div>
    </div>
@stop

@section('scripts')
    <script src="{{ Module::asset('dashboard:js/AppTestService.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            AppTestService.urlApiReq = '{{ route("api.testSerices.getReq") }}';
            AppTestService.urlloaderimg = "{{ asset('themes/adminlte/img/loading.gif') }}";
            AppTestService.urltrueimg = "{{ asset('themes/adminlte/img/true.png') }}";
            AppTestService.urlfalseimg = "{{ asset('themes/adminlte/img/false.png') }}";
            return ;
        });
    </script>
@stop