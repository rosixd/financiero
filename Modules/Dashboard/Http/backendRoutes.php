<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->get('/', [
    'as' => 'dashboard.index',
    'uses' => 'DashboardController@index',
]);

$router->group(['prefix' => '/dashboard'], function (Router $router) {
    $router->post('grid', [
        'as' => 'dashboard.grid.save',
        'uses' => 'DashboardController@save',
    ]);
    $router->get('grid', [
        'as' => 'dashboard.grid.reset',
        'uses' => 'DashboardController@reset',
    ]);
});

/*
DMT: Esta ruta solo es visible para el ambiente de desarrollo
*/
$router->group(['prefix' => '/testServices'], function (Router $router) {
    $router->get('/', [
        'as' => 'testSerices.index',
        'uses' => 'TestServiceController@index',
    ]);
});

