<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => '/testServices'], function (Router $router) {
    $router->get('getReq', [
        'as' => 'api.testSerices.getReq',
        'uses' => 'TestServiceController@getReq',
    ]);

    $router->get('req05', [
        'as' => 'api.testSerices.req05',
        'uses' => 'TestServiceController@req05',
    ]);
});
