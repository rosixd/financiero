<?php

namespace Modules\Dashboard\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Dashboard\Repositories\WidgetRepository;
use Modules\User\Contracts\Authentication;
use Nwidart\Modules\Repository;
use Config;
use WsSocket;
use SoapClient;
use SoapHeader;
use GuzzleHttp\Client;
use Storage;
use Carbon\Carbon;
use Symfony\Component\Debug\Exception\FatalErrorException;

class TestServiceController extends AdminBaseController
{
    /**
     * Devuelve la url del documento pdf a mostrar 
     * 
     * @autor Dickson Morales
     * @param Request
     * @return Json
     */
    public function req05( Request $request ){

        //Eliminando url antes de las variables get
        $url = preg_replace("/.*\?/i", "" , $request->get("t") );
        $ticket = preg_replace("/.*ticket\=/i", "", $url ); //obteniendo la variable ticket
        $doc = $request->get("doc");        
        
        //Url base de los documentos pdf
        $urlBase = "http://abcdin.jp2.cl:8091/ext/JRDNSLN01PR01/XMLHttpRequest.aspx?";
        //$url = $urlBase . "Funcion=VT&T=" . $ticket;
        $url = $urlBase . "Funcion=VD&T=" . $doc;
        
        //Estableciendo conexion con la url del doc
        $client = new Client (); 
        $requestAbcDin = $client
            ->get( $url );

        $respuesta = $requestAbcDin
            ->getBody()
            ->getContents();

        if( count(Storage::disk("public")->directories('pdfs')) == 0 ){
            Storage::disk("public")->makeDirectory("pdfs");
        }

/*        dd( storage_path('app/public/pdfs') );
        dd( Storage::disk("public") );
*/

        //Guardando pdf
        $cliente = new Client;
        //$stream = GuzzleHttp\Psr7\stream_for($resource);
        $cliente->request('GET', $respuesta, [
            'save_to' => storage_path('app/public/pdfs/' . $doc )
        ]);

        //Devolviendo respuesta y nombre del documento
        return Response::json([
            "doc" => $doc,
            "urlDoc" => Storage::disk("public")->url( 'pdfs/' . $doc )
        ]);
    }

    /**
     * Devuelve informacion del requerimiento
     * 
     * æautor Dickson Morales
     * @return Response Json
     */
    public function getReq( Request $request ){    
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set("default_socket_timeout", 5);

        
        if( !$request->has("i") ){
            return Response::json([
                "error" => true,
                "message" => trans("dashboard::testservice.messages.errors.no req")
            ], 500 );
        }
        
        //buscando la configuracion del requerimiento
        $requerimiento = Config::get("configReq")[ $request->get("i") ];

        if( $request->get("i") >= 0 ){

            switch( $request->get("i") ){

                //REQ01
                case 0:
                   return $this->TestREQ01();
                break;
                case 1:
                   return $this->TestREQ02();
                break;
                case 2:
                   return $this->TestREQ03();
                break;
                case 3:
                   return $this->TestREQ04();
                break;
                case 4: 
                   return $this->TestREQ05();
                break;
                case 5: 
                   return $this->TestREQ06();
                break;
                case 6: 
                   return $this->TestREQ07();
                break;
                case 7: 
                   return $this->TestREQ08();
                break;
                case 8: 
                   return $this->TestREQ09();
                break;
                case 9: 
                   return $this->TestREQ10();
                break;
                case 10: 
                   return $this->TestREQ11();
                break;
                case 11: 
                   return $this->TestREQ12();
                break;
                case 12: 
                   return $this->TestREQ13();
                break;
                case 13: 
                   return $this->TestREQ14();
                break;
              

            }

            return Response::json( $respuesta );
        }

        return Response::json([
            "error" => true,
            "message" => "Error"
        ], 200 );        
    } 
    
    /**
     * Devuelve informacion del requerimiento
     * 
     * æautor Dickson Morales
     * @return Response Json
     */
    public function getReq_old( Request $request ){    

        if( !$request->has("i") ){
            return Response::json([
                "error" => true,
                "message" => trans("dashboard::testservice.messages.errors.no req")
            ], 500 );
        }
        
        //buscando la configuracion del requerimiento
        $requerimiento = Config::get("configReq")[ $request->get("i") ];

        if( $request->get("i") > 0 ){

            $ruts = [ 
                "59194496"
            ];
            $respuesta = [];

            foreach( $ruts as $indice => $rut ){

                $conInfo = WsSocket::ConInfoCuenta3( $rut );
            
                //dd( $conInfo );
                $respuesta[ $indice ] = [
                    "rut" => $rut,
                    "resultado" => []
                ];

                if( !array_key_exists("datos", $conInfo ) ){
                    $respuesta[ $indice ]["resultado"] = [
                        "mensaje" => "Error no trae datos la cuenta"
                    ];
                }else{
                    $tarjeta = array_first( $conInfo["datos"]["tarjetas"] , function( $item, $indice ){
                        return $item;
                    });
    
                    //dd( $tarjeta );
    
                    $conexion = Config::get("configSocket.ws-Bloqueodes");
                    //$conexion["url"] = "http://vipabcqa.adretail.cl:11777/osb/EntRetfBloqueodesbloqicService?wsdl";
                    
                    $cliente = new SoapClient( $conexion["url"] );
                    
                    $parametros = [
                        [
                            "RUTCLIENTE" => $rut,
                            "NROCUENTA" => $tarjeta["Cuenta"],
                            "NROTARJETA" => $tarjeta["NumeroTarjeta"],
                            "CODPRODUCTO" => "00001",
                            "ACCION" => ( preg_match("/bloqueada/i", $tarjeta["EstadoDeLaTarjeta"]) ? "D" : "B" ),
                            "CODIGOBLOQUEO" => "66",
                            "FECHABLOQUEO" => date("d-m-Y H:i"),
                            "CODSUCURSAL" => "601",
                            "RUTUSUARIOACT" => ''
                        ],
    
                        /* 'PCODPERSONA'   => $PCODPERSONA,
                        'PEMAIL'        => $PEMAIL,
                        'PINDEECCEMAIL' => $PINDEECCEMAIL,
                        'PESLEGAL'      => $PESLEGAL,
                        'PINCLUIDOPOR'  => $PINCLUIDOPOR, */
                    ]; 
                    
                    $respuesta[ $indice ]["resultado"] = json_decode( 
                        json_encode(
                            $cliente->__soapCall( 'NewOperation', $parametros )
                        ), true );

                }
            }

            return Response::json( $respuesta );
            //$metodo = "NewOperation";
            //$respuesta = WsSocket::conexionWebService($conexion, $parametros, $metodo, true);
			//dd($respuesta);
      
            return false;
        }

        //verificando que tenga flujo(s) el requerimiento
        if( !array_key_exists( "flujo", $requerimiento) ){
            return Response::json([
                "error" => false,
                "message" => trans("dashboard::testservice.messages.no flujo")
            ], 200 );
        }

        $app = false;
        foreach( $requerimiento["flujo"] as $indice => $flujo ){
            //$app = app( $flujo["controlador"] )->{$flujo["metodo"]}();
            //preguntando si existe request como llave 
            if( array_key_exists("request", $flujo )){

                $request
                    ->request
                    ->add(
                        $flujo["request"]
                    ); 

                //dd( $request->all() );
            }

            //dd( app() );

            $clase = new \ReflectionClass( $flujo["controlador"] );
            dd( 
                $clase
                    ->getMethod( $flujo["metodo"] )
                    ->getParameters()
                    ->getType()
            );

            $app = app( $flujo["controlador"] )
                //->makeWith( $flujo["controlador"]  , $request->all() )
                ->{ $flujo["metodo"] }
                //->needs("rut")
                //->give("108152494")
                //->make( [ $flujo["controlador"], $flujo["metodo"]] , $request->all() )
                //->getNamespace()
                //( $request->all() )
                ;

            dd( $app );
            echo "<br>" . $indice . "";
            print_r( $flujo );
        }
        //$app => app()
        //dd()
        dd( $requerimiento );
    } 


    /*** 
     * Metodo que testea conexion con una url
     * 
     * @autor Dickson Morales
     * @param String 
     * @return Mixes
    */
    protected function testUrl( $url = '' ){
        
        //Estableciendo conexion con la url 
        $client = new Client (); 
        $request = $client
            ->get( $url );

        $respuesta = $request
            ->getBody()
            ->getContents();

        return $respuesta;
    }

    /**
     * Metodo para Test de servicios del REQU01 
     *
     * @return void
     */
    protected function TestREQ01(){

        $rut = '144541235';
        $retorno = [];
        $conexion = Config::get("configSocket.processMessage");
        $soapvar = new \SoapVar('<input>
            <![CDATA[
                <CONSCuentaBlq_REQ_COMP_IN>
                    <ACCION>C</ACCION>
                    <RUTCLIENTE>124943043</RUTCLIENTE>
                    <NROCUENTA>4000008277</NROCUENTA>
                    <NROTARJETA>4946110108365012</NROTARJETA>
                    <CODPRODUCTO>24101</CODPRODUCTO>
                    <BLOQUEOS>66</BLOQUEOS>
                </CONSCuentaBlq_REQ_COMP_IN>
            ]]>
            </input>', XSD_ANYXML, null);

        $parametros = array("input" => $soapvar);

        $metodo = "processMessage";

        $respuesta = WsSocket::setConexionWebService($conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'processMessage', 
                'processMessage', 
                '',$conexion['url'], 
                 false,
                $respuesta['msg']
            );
        }else{
			$retorno[] = $this->FormatSalidaWS(
                'processMessage', 
                'processMessage', 
                 '',$conexion['url'], 
                true
            );
		}

        $TipoTarjeta = 'IC';
        $password = '1234';
        $offset = '1234FFFFFFFsss';
        $rut = '119484626';

        $data = [
            'pan'             => '',
            'ctype'           => 'CC',
            'ctext'           => $password . $offset,
            'interface_id'    => 'XMLCSI',
            'customer_id'     => Config::get("configSocket.CSI.usuario"),
            'client_ip'       => Config::get("configSocket.CSI.ip"),
        ];

        // VALPINCDI el parámetro pan debe ser el rut 
        // VALPIN el parámetro pan debe ser la tarjeta
        if ($TipoTarjeta == 'TC') {
            $servicio = 'VALPIN';
            $data['pan'] = $tarjeta;
        } else {
            $servicio = 'VALPINCDI';
            $data['pan'] = substr($rut, 0, -1);
        }

        $dataXml = WsSocket::csiEncryptArrayToXml($data);

        $xml =  "<request_auth>
            <service_id>" . $servicio . "</service_id>
            <subtype_service>00" . Config::get("configSocket.CSI.llave") . "</subtype_service>
            " . $dataXml . "</request_auth>";

        $respuesta = $this->conexionCurlXml(Config::get("configSocket.CSI.url"), $xml);
        
        
        if( strlen( $respuesta ) > 10 ){
            $retorno[] = $this->FormatSalidaWS(
                'CSI', 
                'CSI', 
                 '',Config::get("configSocket.CSI.url"), 
                true,
                '',
                'CURL'
            );
        }else {
            $retorno[] = $this->FormatSalidaWS(
                'CSI', 
                'CSI', 
                 '',Config::get("configSocket.CSI.url"), 
                false,
                '',
                'CURL'
            );
        }
       

   
        //CONINFOCUENTA3 
        $rut = '56467785';
       
        $conexion = Config::get("configSocket.MW-CONINFOCUENTA3");

        $conexion["trama"] = str_replace('{RUT}',$rut,$conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion );
      	//dd( $respuesta);
        if($respuesta['error']){
            $respuesta['estatus_cliente'] = 99;
            $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3",  false,
                $respuesta['msg']);
        }else{
            
            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
            if(strlen($valores[1]) > 2){
                $valores[1] = substr($valores[1], 0, 2);
            }
            switch ($valores[1]) {
                case '00':
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", true);
                    break;
                    case '01':
                    //dd('Cliente no tiene cuenta ABCDIN');
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", true);
                    break;
                case '99':
                    //dd('Problemas de comunicación (Milliways)');
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", false,
                $respuesta['msg'] );
                    break;
            }
        }

        //$rut = '19';
        //TaEstadoBloqClienteService
        $conexion = Config::get("configSocket.WS-TaEstadoBloqClienteService");
        $parametros = array(
            'PCOD_PERSONA' => $rut,
            'PCOD_CANAL' => '1'
        ); 
        $_parametros = [ 'PCOD_PERSONA','PCOD_CANAL']; 
        
        $metodo = "TaEstadoBloqClienteService";
        
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'TaEstadoBloqClienteService', 
                'TaEstadoBloqClienteService', 
                $_parametros,$conexion['url'], 
                 false,
                $respuesta['msg']
            );
        }else{
			$retorno[] = $this->FormatSalidaWS(
                'TaEstadoBloqClienteService', 
                'TaEstadoBloqClienteService', 
                $_parametros,$conexion['url'], 
                true
            );
		}

        $conexion = Config::get("configSocket.WS-NewOperation");
        
        $parametros = array(
            'PC_COD_PERSONA' => $rut,
        );
        $_parametros = ['PC_COD_PERSONA'];
        $metodo = "NewOperation";
        
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        //$datos =  WsSocket::EntRetfDatosclienteService($rut);
       
        if($respuesta['error']){
           	$retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
        }else{
            if($respuesta['datos']['PN_COD_ERROR'] != 0){
                $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $_parametros,
                $conexion['url'], 
                false
            );
            }else{
                $retorno[] = $this->FormatSalidaWS(
                    'NewOperation', 
                    'NewOperation', 
                    $_parametros,
                    $conexion['url'], 
                    true
                );
            }
        }

        //getTicketeecc

        $conexion = Config::get("configSocket.WS-getTicketeecc");
        $parametros = array(
            'Rut' => $rut,
        );
        $_parametros = ['Rut'];

        $metodo = "getTicketeecc";

        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if($respuesta['error']){
           
            $retorno[] = $this->FormatSalidaWS(
                'getTicketeecc', 
                'getTicketeecc', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
            
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getTicketeecc', 
                'getTicketeecc', 
                $_parametros,
                $conexion['url'], 
                true
            );
        }

        //TaActualizaEstatusClienteService
        $conexion = Config::get("configSocket.WS-EntRetfActualizaestatusclienteService");
        
        $parametros = array(
            'PCOD_PERSONA'  => $rut,
            'PCOD_CANAL'    => 1,
            'PACCION'       => 1
        ); 
        
        $_parametros =[ 
            'PCOD_PERSONA',
            'PCOD_CANAL'  ,
            'PACCION'     
        ]; 
        
        $metodo = "TaActualizaEstatusClienteService";
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
		if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'TaActualizaEstatusClienteService', 
                'TaActualizaEstatusClienteService', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
		}else{

            $retorno[] = $this->FormatSalidaWS(
                'TaActualizaEstatusClienteService', 
                'TaActualizaEstatusClienteService', 
                $_parametros,
                $conexion['url'], 
                true
            );

        }
        
       
        //datospersona

        $conexion = Config::get("configSocket.MW-datospersona");
        $conexion["trama"] = str_replace('{RUT}','97752125',$conexion["trama"]);
        //$respuesta = self::conexionSocket( $conexion );
       
        $respuesta = WsSocket::setConexionSocket( $conexion );

        if($respuesta['error']){
            $respuesta['estatus_cliente'] = 99;
            $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", false);
        }else{
            
            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);

            switch ($valores[1]) {
                case '00':
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", true);
                    break;
                case '01':
                    //dd('Cliente no tiene cuenta ABCDIN');
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", true);
                    break;
                case '99':
                    //dd('Problemas de comunicación (Milliways)');
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona",  false, $respuesta['msg']);
                    break;
                default:
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona",  false, $respuesta['msg']);
                    break;
            }
        }

        return  $retorno;
    }

    protected function TestREQ02(){
       $conexion['host'] = '';
       $conexion['puerto'] = '';
       $conexion['trama'] = '';
       $retorno = [];
       $retorno[] = $this->FormatSalidaMW($conexion, 'Sin integracion', false) ;
       return $retorno;
    }
    protected function TestREQ03(){
     
        //EECC_TAR
        $retorno = [];
        $conexion = Config::get("configSocket.MW-EECC_TAR");
        $conexion["trama"] = str_replace('{numerotarjera}','123',$conexion["trama"]);
        
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        if($respuesta['error']){
           $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR',  false, $respuesta['msg'] );
        }
        $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR', true);

        //CalculateExtracupo

        $conexion = Config::get("configSocket.WS-SitioretfinInterfazCalculoextracupo");
        
        $parametros = array(
            'RutCliente'    => '19',
            'CupoTotal'     => '200',
            'DispTienda'    => '200',
            'FactExtracupo' => '200',
            'TipoTarjeta'   => 'TC',
            'Canal'         => 'POS'
        ); 
        $_parametros = [
            'RutCliente'    ,
            'CupoTotal'     ,
            'DispTienda'    ,
            'FactExtracupo' ,
            'TipoTarjeta'   ,
            'Canal'         
        ];
        

        $metodo = "CalculateExtracupo";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);

        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'CalculateExtracupo', 
                'CalculateExtracupo', 
                $_parametros,$conexion['url'], 
                 false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'CalculateExtracupo', 
                'CalculateExtracupo', 
                $_parametros,$conexion['url'], 
                true
            );
        }

        //TcObtDisponible-IC
        $conexion = Config::get("configSocket.MW-TcObtDisponible-IC");
        
        $conexion["trama"] = str_replace('{COD_EMPRESA}','06', $conexion["trama"]);
        $conexion["trama"] = str_replace('{CUENTA_CLIENTE}','123',$conexion["trama"]);
        $conexion["trama"] = str_replace('{CLASIFICACION}','adc',$conexion["trama"]);

        $fecha = Carbon::now()->format('d/m/Y');
        //$fecha = Carbon::now();;
        $conexion["trama"] = str_replace('{FECHA}',$fecha, $conexion["trama"]);
        
        
        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
        }else{

            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
    
            if($valores['1'] == 99){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
            }else if($valores['1'] == 1){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC', true);
            }else if($valores['1'] == 00){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC', true);
            } 
        }
       
        //TcObtDisponible-TC

        $conexion = Config::get("configSocket.MW-TcObtDisponible-TC");
        $conexion["trama"] = str_replace('{COD_EMPRESA}','06', $conexion["trama"]);
        $conexion["trama"] = str_replace('{CUENTA_CLIENTE}','123',$conexion["trama"]);
        $conexion["trama"] = str_replace('{CLASIFICACION}','adc',$conexion["trama"]);

        $fecha = Carbon::now()->format('d/m/Y');
        //$fecha = Carbon::now();;
        $conexion["trama"] = str_replace('{FECHA}',$fecha, $conexion["trama"]);
        

        $respuesta = WsSocket::setConexionSocket( $conexion, true );


        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
        }else{

            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
    
            if($valores['1'] == 99){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC',  false, $respuesta['msg']);
            }else if($valores['1'] == 1){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC', true);
            }else if($valores['1'] == 00){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC', true);
            } 
        }


        //OBTENERDUPLICA
        $conexion = Config::get("configSocket.MW-OBTENERDUPLICA");
        //'trama' => "TEXETTC.TC_INTERFAZPOS3.OBTENERDUPLICA('{empresa}','{cuenta}','{comercio}','')"
        
        $conexion["trama"] = str_replace('{empresa}', str_pad('65', 4, "0", STR_PAD_LEFT) ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{cuenta}',  '123', $conexion["trama"]);
        $conexion["trama"] = str_replace('{comercio}','648', $conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion, true );

        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;

        if($respuesta['error']){
             $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA',   false,
                $respuesta['msg']);
        }

        $respuesta['codigo'] = substr($respuesta['datos'][0], -2);

        switch (substr($respuesta['datos'][0], -2)) {
            case '99':
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA', true);
                break;
            default:
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA', true);
                break;
        }


        //totobligdia5-IC"
        $conexion = Config::get("configSocket.MW-totobligdia5-IC");
        
        $conexion["trama"] = str_replace('{codEmpresa}', '056' ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{NroTarjeta}',  '1234', $conexion["trama"]);

        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );


        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC',  false,
                $respuesta['msg']);
        }
        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;
        
        $cod_respuesta = substr($respuesta["datos"][0], -3);
     
        switch ($cod_respuesta) {
            case '99':
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC', true);
                break;
            default:
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC', true);
                break;
        }

        //totobligdia5-TC
        $conexion = Config::get("configSocket.MW-totobligdia5-TC");
         
        $conexion["trama"] = str_replace('{codEmpresa}', '056' ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{NroTarjeta}',  '1234', $conexion["trama"]);

        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );


        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC',  false,
                $respuesta['msg']);
        }

        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;
        
        $cod_respuesta = substr($respuesta["datos"][0], -3);
     
        switch ($cod_respuesta) {
            case '99':
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC', true);
                break;
            default:
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC', true);
                break;
        }

        return  $retorno;

    }
    protected function TestREQ04(){
         //EECC_TAR
        $retorno = [];
        $conexion = Config::get("configSocket.MW-EECC_TAR");
        $conexion["trama"] = str_replace('{numerotarjera}','123',$conexion["trama"]);
        
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        if($respuesta['error']){
           $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR',  false, $respuesta['msg']);
        }
        $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR', true);

        return $retorno;

    }

    protected function TestREQ05(){
        $retorno = [];
        $conexion = Config::get("configSocket.WS-SitioretfinInterfazCreaticketeecc");
        $rut = '144541235';
        $userChannel = 'ABC';
    
        $parametros = array(
            'userId' => $rut,
            'userChannel' => $userChannel,
        );
    
        $_parametros = ['userId','userChannel'];
        
        
        $metodo = "createTicketByRut";

       // $respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);

        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'createTicketByRut', 
                'createTicketByRut', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'createTicketByRut', 
                'createTicketByRut', 
                $_parametros,
                $conexion['url'], 
                true
            );
        }    
        
        $conexion = Config::get("configSocket.MW-ConsultaFlujoEECC");
        $conexion["trama"] = str_replace('{EmpresaId}','1',$conexion["trama"]);
        $conexion["trama"] = str_replace('{NumCuenta}','4000000001',$conexion["trama"]);
        
        $conexion["trama"] = str_replace('{PrimerVcto}','20/01/2018',$conexion["trama"]);
        $conexion["trama"] = str_replace('{Plazo}','0',$conexion["trama"]);
        $conexion["trama"] = str_replace('{ValorCuota}','0',$conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        //dd($respuesta);
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaMW($conexion, 'ConsultaFlujoEECC', false, $respuesta['msg']);
        }else{
            $retorno[] = $this->FormatSalidaMW($conexion, 'ConsultaFlujoEECC', true, $respuesta['msg']);
        }
        
        return $retorno;
    }

    protected function conexionCurlJSON($url,$datos = "")
    {
            //iniciamos curl
            $ch = curl_init();
            
            //seteamos la url
            curl_setopt($ch, CURLOPT_URL, $url);
        
            //seteamos los datos post asociados a la variable xmlData
       
	       /* if(is_array($datos)){
	            $json = json_encode($datos);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	        }*/ 
                
            //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            
            //tiempo de espera, 0 infinito
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            
            //ejecutamos la petición y asignamos el resultado a $data
    
            $data = curl_exec($ch);
            
            //cerramos la petición curl
            curl_close($ch);

            
            
            return $data;
    }

    protected function TestREQ06(){
        $conexion['host'] = '';
        $conexion['puerto'] = '';
        $conexion['trama'] = '';
        $retorno = [];
        $retorno[] = $this->FormatSalidaMW($conexion, 'Sin integracion', false) ;
        return $retorno;
    }
    protected function TestREQ07(){
        
        $rut = '106348235';
       
        $NROTARJETA = '4946110100002084';

        
        $rut  = WsSocket::aesExec('Encriptar', $rut);
        
        $TipoTarjeta = 'IC';
       
        $tipo_tarjeta = WsSocket::aesExec('Encriptar',$TipoTarjeta);

        $conexion = Config::get("configSocket.pagatucuenta");

        $num_tarjeta  = WsSocket::aesExec('Encriptar',  $NROTARJETA);
        
        $url_base = $conexion['url']."?R=".$rut."&T=".$tipo_tarjeta."&N=".$num_tarjeta;
        
       
        //$respuesta = $this->conexionCurlJSON($url_base);
        $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$url_base."'";
        $respuesta = shell_exec($prueba);
       
        if( $respuesta != 200){
            $retorno[] = $this->FormatSalidaWS(
                'paga tu cuenta', 
                'paga tu cuenta', 
                 '',
                $conexion['url'], 
                false,
                '',
                'CURL'
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'paga tu cuenta', 
                'paga tu cuenta', 
                 '',
                $conexion['url'], 
                true,
                '',
                'CURL'
            );

        }

       return $retorno;
    }
    protected function TestREQ08(){
        $conexion['host'] = '';
       $conexion['puerto'] = '';
       $conexion['trama'] = '';
       $retorno = [];
       $retorno[] = $this->FormatSalidaMW($conexion, 'Sin integracion', false) ;
       return $retorno;
    }

    protected function TestREQ09(){
        $retorno = [];
        
        $conexion = Config::get("configSocket.MW-EsClienteValidoWeb");
        $conexion["trama"] = str_replace('{PCODPERSONA}','99690577', $conexion["trama"]);
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        if($respuesta['error']){
            $retorno[] =  $this->FormatSalidaMW($conexion, 'EsClienteValidoWeb', false);
        }else{

            $retorno[] = $this->FormatSalidaMW($conexion, 'EsClienteValidoWeb', true);
        }

        $conexion  =  Config::get("configSocket.WS-TC2000_EvaluacionExpress");
        $respuesta =  WsSocket::TC2000EvaluacionExpressPrueba();

        if($respuesta['error']){
            $retorno[] =  $this->FormatSalidaMW($conexion, 'TC2000', false);
	     $retorno[] = $this->FormatSalidaWS(
                'TC2000EvaluacionExpress', 
                'TC2000EvaluacionExpress', 
                '',$conexion['url'], 
                 false,
                ''
            );
        }else{

            $retorno[] = $this->FormatSalidaWS(
                'TC2000EvaluacionExpress', 
                'TC2000EvaluacionExpress', 
                '',$conexion['url'], 
                 true,
                ''
            );
        } 

        $conexion = Config::get("configSocket.ws-IDverifier-pro");

        $rut = '';
        $numSerie = '';
        $parametros = array(
            'Identity' => [
                'RUT'   => $rut,
                'IDCardSerialNumber' => $numSerie
            ],
            'ProcessingOptions'=> [
                'Language'=> 'ES',
                'EnvironmentOverride' => 'PROD'
            ]
        ); 
        
        $metodo = "startTransaction";

        //$conexion['url'] = "http://abcdin.desarrollo.netred.cl/uru/soap-latam/ut/70/chile?WSDL";
        // nueva url
        $conexion['url'] = config('configSocket.ws-IDverifier-pro.url');

        $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$conexion['url']."'";

        $respuesta = shell_exec($prueba);

        if( $respuesta != 200){
            $retorno[] = $this->FormatSalidaWS(
                'idverifier', 
                'idverifier', 
                '',$conexion['url'], 
                 false,
                ''
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'idverifier', 
                'idverifier', 
                '',$conexion['url'], 
                 true,
                ''
            );
        }

        return $retorno;
    }

    protected function TestREQ10(){
       $conexion['host'] = '';
       $conexion['puerto'] = '';
       $conexion['trama'] = '';
       $retorno = [];
       $retorno[] = $this->FormatSalidaMW($conexion, 'Sin integracion', false) ;
       return $retorno;
    }
        
    protected function TestREQ11(){
        $retorno = [];
        $rut = '119484626';
        /*4398456K

        144547373
        */
	 //SmsGenerateChallenge
     



        //getMaileecc
        $conexion = Config::get("configSocket.WS-getMaileecc");

        $parametros = array(
            'codPersona' => $rut
        ); 
        $_parametros = [ 
            'codPersona'
        ];

        $metodo = "getMaileecc";

        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
      
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'getMaileecc', 
                'getMaileecc', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getMaileecc', 
                'getMaileecc', 
                $parametros,
                $conexion['url'], 
                true
            );
        }    
        
        //getDatosLegal
        $conexion = Config::get("configSocket.wS-getDatosLegal");
        
        $parametros = [
            'codPersona' => $rut
        ]; 
        
        $metodo = "getDatosLegal";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, false, false, $conexion["opciones"] );
        
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'getDatosLegal', 
                'getDatosLegal', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getDatosLegal', 
                'getDatosLegal', 
                $parametros,
                $conexion['url'], 
                true,
                $respuesta
            );
        }   
       
        //SmsGenerateChallenge
        $conexion = Config::get("configSocket.wS-SmsWs");
        $userPhone = '942365466'; 
        $email = 'proyectosabcdin@netred.cl';
        
        //<ns1:userId>'. substr($rut, 0, -1) .'</ns1:userId> para desarrollo 
        $soapvar = new \SoapVar('
            <ns1:SmsTransactionData>
                <ns1:userId>'. $rut.'</ns1:userId>
                <ns1:operationId>ActualizarEmail</ns1:operationId>
                <ns1:userEmail>'.$email.'</ns1:userEmail>
                <ns1:userPhone>'. $userPhone.'</ns1:userPhone>
                <ns1:smsOperationData>
                    <ns1:smsParameter>
                        <ns1:key>telefono</ns1:key>
                        <ns1:value>'.$userPhone.'</ns1:value>
                    </ns1:smsParameter>
                </ns1:smsOperationData>
            </ns1:SmsTransactionData>
            ', XSD_ANYXML, 'null');

        $parametros = array("SmsTransactionData" => $soapvar);

        $metodo = "SmsGenerateChallenge"; 	
            
       
		//$metodo = "SmsGenerateChallenge"; 
    
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo, false, true);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = true);
       
	
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'SmsGenerateChallenge', 
                'SmsGenerateChallenge', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'SmsGenerateChallenge', 
                'SmsGenerateChallenge', 
                $_parametros,
                $conexion['url'], 
                true
            );
        } 
      /*   //ValidateTransaction
        $conexion = Config::get("configSocket.wS-SmsWs");
        $clave = '1234';
        $parametros = array(
            'userId'      => substr($rut, 0, -1),
            'operationId' => 'ActualizarEmail',
                'operationData'   =>  [
                    'mail'    => $email,
                    'celular' => $userPhone
            ], 
            'confirmationCode'=> $clave
        ); 
        
        $metodo = "ValidateTransaction";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'ValidateTransaction', 
                'ValidateTransaction', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'ValidateTransaction', 
                'ValidateTransaction', 
                $_parametros,
                $conexion['url'], 
                true
            );
        } 
         */
        //ActualizaClienteIC
        $conexion = Config::get("configSocket.wS-ActualizaClienteIC");
		
        $parametros = array(
            'updateClienteICRequest' => $this->Datos()
        ); 
        
        $metodo = "updateClienteIC";
        
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
       // dd( $respuesta);
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'updateClienteIC', 
                'updateClienteIC', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'updateClienteIC', 
                'updateClienteIC', 
                $_parametros,
                $conexion['url'], 
                true
            );
        } 
        //mergeMaileecc
        $conexion = Config::get("configSocket.wS-mergeMaileecc");
		
        $parametros = array(
            'PCODPERSONA'   => $rut,
            'PEMAIL'        => '',
            'PINDEECCEMAIL' => '',
            'PESLEGAL'      => '',
            'PINCLUIDOPOR'  => '',
        ); 
        
        $metodo = "mergeMaileecc";

        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'mergeMaileecc', 
                'mergeMaileecc', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'mergeMaileecc', 
                'mergeMaileecc', 
                $_parametros,
                $conexion['url'], 
                true
            );
        } 
        return $retorno;
    }

    protected function TestREQ12(){
       $conexion['host'] = '';
       $conexion['puerto'] = '';
       $conexion['trama'] = '';
       $retorno = [];
       $retorno[] = $this->FormatSalidaMW($conexion, 'Sin integracion', false) ;
       return $retorno;
    }
    protected function TestREQ13(){
     
        $rut = '144541235';
        $retorno = [];
        
        $conexion = Config::get("configSocket.MW-CONINFOCUENTA3");
        $conexion["trama"] = str_replace('{RUT}',$rut,$conexion["trama"]);
        //$respuesta = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", false);

        $respuesta = WsSocket::ConInfoCuenta3( $rut );
        
      

        //Bloqueodes
        $conexion = Config::get("configSocket.ws-Bloqueodes");

        $parametros = array(
            'RUTCLIENTE'    => $rut,
            //'NROCUENTA'     => $respuesta["datos"]['tarjetas']['5000']["Cuenta"],
            'NROCUENTA'     => '0',
            //'NROTARJETA'    => $respuesta["datos"]['tarjetas']['5000']["NumeroTarjeta"],
            'NROTARJETA'    => '0',
            'CODPRODUCTO'   => '00001',
            'ACCION'        => 'B',
            'CODIGOBLOQUEO' => '66',
            'FECHABLOQUEO'  => date("d-m-Y H:m"),
            'CODSUCURSAL'   => '601',
            'RUTUSUARIOACT' => $rut,
        ); 
        
        $metodo = "NewOperation";

        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo, true);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo);
      
        if($respuesta['error']){
            $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $parametros,
                $conexion['url'], 
                true
            );
        }    

        return $retorno;
    }

    protected function TestREQ14(){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.Trazabilidad");            
         
            
            $datos = [
                "rut"                  => '', 
                "idSesion"             => '',
                "IP"                   => '', 
                "infoBrowser"          => '', 
                "infoSO"               => '', 
                "numTelefono"          => '', 
                "cdCanal"              => $conexion['cdCanal'],
                "cdAccion"             => '', 
                "cdTienda"             => $conexion['CdTienda'], 
                "cdTransaccion"        => '', 
                "deTransaccion"        => '', 
                "cdAplicacion"         => $conexion['cdAplicacion'],
                "cdError"              => '', 
                "entradaTransaccion"   => '', 
                "salidaTransaccion"    => '', 
            ];
        
            $respuesta ="";
            //$respuesta = self::conexionCurlJSON($conexion['url'], $datos);
           
            $respuesta = json_decode($respuesta, true);
            $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$conexion['url']."'";
            $respuesta = shell_exec($prueba);
          
            if( $respuesta != 200){
                $retorno[] = $this->FormatSalidaWS(
                    'Trazabilidad', 
                    'Trazabilidad', 
                    '', 
                    $conexion['url'], 
                    false,
                    '',
                    'CURL'
                );
            }else{
                $retorno[] = $this->FormatSalidaWS(
                    'Trazabilidad', 
                    'Trazabilidad', 
                     '',
                    $conexion['url'], 
                    true,
                    '',
                    'CURL'
                );
            }
   
          
            return $retorno;
           
           
        } catch (\Exception $e) {
           
  
            $retorno[] = $this->FormatSalidaWS(
                'Trazabilidad', 
                'Trazabilidad', 
                '',
                $conexion['url'], 
                true,
                '',
                'CURL'
            );

            return $retorno;
        }
        
    }

    protected function Datos(){
        //dd(session::all());
        return $data = [
            'RUT_CLIENTE'                   => '203542361', //REQ
            'EST_CIVIL'                     => 'm',
            'SEXO'                          => 'prueba',
            'FEC_NACIMIENTO'                => '-',
            'PRIMER_APELLIDO'               => '-',//REQ
            'SEGUNDO_APELLIDO'              => '-', 
            'PRIMER_NOMBRE'                 => '-', 
            'SEGUNDO_NOMBRE'                => '-',
            
            'PROFESION'                     => '-',
            'CONYUGUE'                      => '-',
            'NACIONALIDAD'                  => '-',
            'EMAIL_USUARIO'                 => '-',
            'EMAIL_SERVIDOR'                => '-',

            'NIVEL_ESTUDIOS'                => '-',
            'TIPO_VIVIENDA'                 => '-',
            'NUM_HIJOS'                     => '-',
            'NUM_DEPENDIENTES'              => '-',
            'ES_RESIDENTE'                  => '-',
            'TIEMPO_VIVIEN_ACT'             => '-',
            'TOTAL_INGRESOS'                => '-',
            'COD_PAIS'                      => '-',
            'SCORING'                       => '-',
            'ACTIVIDAD'                     => '-',
            'TIPO_SOC_CONYUGAL'             => '-',
            'NOM_CONYUGUE'                  => '-',
            'FEC_MATRIMONIO'                => '-',
            'IND_SOL_PATRIMONIO'            => '-',
            'IND_EECC_EMAIL'                => '--',//REQ
            'COD_DIRECCION'                 => '-01',//REQ
            'TIP_DIRECCION'                 => '-01',//REQ
            'COD_POSTAL'                    => '-',
            'DETALLE'                       => '-',
            'COD_PAIS_CNT'                  => '-',
            'COD_REGION'                    => '-',
            'COD_COMUNA'                    => '-',
            'CALLE'                         => '-',
            'NUMERO'                        => '-', 
            'ES_DIR_EECC'                   => '--',
            'IND_ACCION_BLOQ_DIR'           => '-',
            'MOTIVO_RECHZ'                  => '-',
            'IND_ESTADO'                    => '--',
            'COD_FUENTE'                    => '-',
            'COD_AREA'                      => '-',
            'NUM_TELEFONO'                  => '-',
            'TIP_TELEFONO'                  => '-',
            'TEL_UBICACION'                 => '-',
            'EXTENSION'                     => '-',
            'ES_DEFAULT'                    => '-',
            'INCLUIDO_POR'                  => '-',
            'FEC_INCLUSION'                 => '-',
            'MODIFICADO_POR'                => '-',
            'FEC_MODIFICACION'              => '-',
            'TEL_ACTIVO'                    => '-',
            'COD_FUENTE_FONO'               => '-',
        ];
    }

    protected function FormatSalidaMW($conexion, $nombre, $estatus, $msg = ''){
		return [
            'NombreServicio' => $nombre,
            'TipoServicio'   => 'MW',
            'Host'           => $conexion['host'],
            'Puerto'         => $conexion['puerto'],
            'Trama'          => $conexion['trama'],
            'Estatus'        => $estatus,
            'Mensaje'        => $msg
        ];
    }
    protected function FormatSalidaWS($nombre, $metodos, $parametros, $url, $estatus, $msg = '', $tipo = "ws" ){
		return [
            'NombreServicio' => $nombre,
            'TipoServicio'   => $tipo,
            'Metodo'         => $metodos ,
            'Parametros'     => $parametros,
            'Url'            => $url,
            'Estatus'        => $estatus,
            'Mensaje'        => $msg
        ];
    }

    /**
     * DMT: Metodo de conexion general 
     * 
     * @param String Trama que se envia al socket
     * @return Array
     */
    protected static function conexionSocket($conexion1 = '', $contador = false, $expresionRegular = null)
    {
        error_reporting(E_ALL);

        try {
            //$conexion1 = Config::get("configSocket.MW-CONINFOCUENTA3");
        
            $trama = $conexion1['trama'];
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                              
            if( $contador ) {
                $contador = ( $contador > 0 ) ? $contador : 2;

                socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => $contador, 'usec' => 0]);
            }
            
            if ($socket === false) {
                return [
                    "error" => true,
                    "msg" => "Error al tratar de crear la conexion: " . socket_strerror(socket_last_error()) 
                ];
            } 

            $result = socket_connect($socket, $conexion1["host"], $conexion1["puerto"]);
            if ($result === false) {
                return [
                    "error" => true,
                    "msg" => "Error al tratar de conectar: ( $result ) " . socket_strerror(socket_last_error($socket)) 
                ];
            } 
            $out = '';

            /*Enviamos el comando o peticion al Fiscal printer por via TCP*/
            $trama = chr(2) . $trama . chr(10) . chr(3);
            
            socket_write($socket, $trama, strlen($trama));
            $ite = 0;
            
            $cont = 0;
            $respuesta = '';
            $array_rep= [];
            
            while ($out = socket_read($socket, 8192)) {
                $respuesta .= $out;
                $array_rep[$ite++] = $out;

                if ($expresionRegular && preg_match($expresionRegular, $respuesta, $match)) {
                    //echo $ite . "--" . $out . "--\n";
                    break;
                }
            }
            $errorSocket = @socket_last_error($socket);
            $errormsg = @socket_strerror($errorSocket);

            socket_close($socket);
        
            $error = false;
            $msg = '';
            //dd($respuesta );
            //dd($respuesta, $errorSocket, $errormsg);
        
            if($respuesta == ''){
                $error = true;
                $msg = 'error servicio';
            }
       
        } catch (\Exception $ex) {
            
            return [
                'error' => true,
                'msg'  => $ex
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
        /* 
        self::log([
            'trama' => $trama,
            "error" => $error,
            "msg" => $msg,
            'original-data' => mb_detect_encoding($respuesta) != 'UTF-8' ? $respuesta : utf8_encode($respuesta),
            //"original-data" =>  utf8_encode($respuesta),
            "datos" => $array_rep
        ]);
        */
        return [
            "error" => $error,
            "msg" => $msg,
            'original-data' => mb_detect_encoding($respuesta) != 'UTF-8' ? $respuesta : utf8_encode($respuesta),
            //"original-data" =>  utf8_encode($respuesta),
            "datos" => $array_rep
        ];
    }

    protected  function conexionCurlXml($url, $xml)
    {
        //iniciamos curl
        $ch = curl_init();
        
        //seteamos la url
        curl_setopt($ch, CURLOPT_URL, $url);
       
        //seteamos los datos post asociados a la variable xmlData
        $xml = trim(preg_replace("/\s+/", " ", $xml));
		$xml = "<?xml version='1.0' encoding='ISO-8859-1'?>\n" . 
				str_replace("> <", "><", $xml);
    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
     
            
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        //tiempo de espera, 0 infinito
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
         
        //ejecutamos la petición y asignamos el resultado a $data
   
        $data = curl_exec($ch);
        
        //cerramos la petición curl
        curl_close($ch);

		
        return $data;
    }
}