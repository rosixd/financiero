<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Dashboard\Repositories\WidgetRepository;
use Modules\User\Contracts\Authentication;
use Nwidart\Modules\Repository;
use Config;

class TestServiceController extends AdminBaseController
{

    /**
     * Muestra la vista inicial con la lista de los requerimientos
     * 
     * æautor Dickson Morales
     * @return view
     */
    public function index(){    

        $requerimientos = array_pluck( Config::get("configReq"), 'name');

        return view("dashboard::test_services.index", compact('requerimientos') );
    }
}