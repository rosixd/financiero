<?php

namespace Modules\Log\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Log\Entities\Log;
use Modules\Log\Http\Requests\CreateLogRequest;
use Modules\Log\Http\Requests\UpdateLogRequest;
use Modules\Log\Repositories\LogRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use OwenIt\Auditing\Models\Audit;
//use Modules\User\Contracts\Authentication;

class LogController extends AdminBaseController
{
    /**
     * @var LogRepository
     */
    private $log;

    /**
     * @var Authentication
     */
    protected $auth;

    public function __construct(LogRepository $log)
    {
        parent::__construct();

        $this->log = $log;

        //dd(auth()->user()->hasAccess('formularios.formularios.index'));

       /*  $auth = app('Modules\User\Contracts\Authentication');
        dd($auth->hasAccess('log.logs.index'));  */
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$logs = $this->log->all();

        return view('log::admin.logs.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('log::admin.logs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLogRequest $request
     * @return Response
     */
    public function store(CreateLogRequest $request)
    {
        $this->log->create($request->all());

        return redirect()->route('admin.log.log.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('log::logs.title.logs')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Log $log
     * @return Response
     */
    public function edit(Log $log)
    {
        return view('log::admin.logs.edit', compact('log'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Log $log
     * @param  UpdateLogRequest $request
     * @return Response
     */
    public function update(Log $log, UpdateLogRequest $request)
    {
        $this->log->update($log, $request->all());

        return redirect()->route('admin.log.log.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('log::logs.title.logs')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Log $log
     * @return Response
     */
    public function destroy(Log $log)
    {
        $this->log->destroy($log);

        return redirect()->route('admin.log.log.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('log::logs.title.logs')]));
    }
        /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
       

        $log =  Audit::select('*'); 
        $inicio = $request->inicio;
        if (!preg_match('/^\d{2}-\d{2}-\d{4} \/ \d{2}-\d{2}-\d{4}$/', $inicio)) {
            return Datatables::of($log)
                ->make(true);    
        }

        $a = explode("/",$inicio);
        $inicio = trim($a[0]) . " 00:00:00";
        $fin = trim($a[1]) . " 23:59:59";
        $formato = 'd-m-Y H:i:s';
	
        $inicio = Carbon::createFromFormat($formato, $inicio);
        $fin = Carbon::createFromFormat($formato, $fin);
        //dd( $inicio );
        $log->whereBetween('created_at', [
                $inicio,
                $fin
        ]);
        
        return Datatables::of($log)
        ->make(true); 
 
    }
}
