<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/log'], function (Router $router) {
    $router->bind('log', function ($id) {
        return app('Modules\Log\Repositories\LogRepository')->find($id);
    });
    $router->get('logs', [
        'as' => 'admin.log.log.index',
        'uses' => 'LogController@index',
        'script' => 'AppLog',
        'view' => 'index',
        'modulo' => 'log',
        'submodulo' => 'log',
        'datatable' => 'admin.log.datatable',
        'editable' => 'api.translation.translations.update',
        'middleware' => 'can:log.logs.index',
        'clearcache' => 'api.translation.translations.clearCache'
    ]);
    $router->get('logs/create', [
        'as' => 'admin.log.log.create',
        'uses' => 'LogController@create'
    ]);
    $router->post('logs', [
        'as' => 'admin.log.log.store',
        'uses' => 'LogController@store'
    ]);
    $router->get('logs/{log}/edit', [
        'as' => 'admin.log.log.edit',
        'uses' => 'LogController@edit'
    ]);
    $router->put('logs/{log}', [
        'as' => 'admin.log.log.update',
        'uses' => 'LogController@update'
    ]);
    $router->delete('logs/{log}', [
        'as' => 'admin.log.log.destroy',
        'uses' => 'LogController@destroy'
    ]);
// append
    $router->get('logs/datatable', [
        'as' => "admin.log.datatable",
        'middleware' => 'can:log.logs.index',
        "uses" => "LogController@datatable"
    ]);
});
