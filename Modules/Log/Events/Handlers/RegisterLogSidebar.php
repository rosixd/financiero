<?php

namespace Modules\Log\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterLogSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
	
	
	
	
		if( 
            $this->auth->hasAccess('log.logs.index') 
        ){
	        $menu->group(trans('core::sidebar.content'), function (Group $group) {
	            $group->item(trans('log::logs.permisos.name'), function (Item $item) {
	                $item->icon('fa fa-folder-open-o');
	                $item->weight(10);
	                $item->authorize(
	                     /* append */
	                );
	                $item->item(trans('log::logs.permisos.name'), function (Item $item) {
	                    $item->icon('fa fa-copy');
	                    $item->weight(0);
	                    $item->append('admin.log.log.create');
	                    $item->route('admin.log.log.index');

	                });
	// append

	            });
	        });
		}
        return $menu;
    }
}
