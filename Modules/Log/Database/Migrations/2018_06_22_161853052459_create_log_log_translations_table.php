<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogLogTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log__log_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('log_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['log_id', 'locale']);
            $table->foreign('log_id')->references('id')->on('log__logs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log__log_translations', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
        });
        Schema::dropIfExists('log__log_translations');
    }
}
