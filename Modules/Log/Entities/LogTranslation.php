<?php

namespace Modules\Log\Entities;

use Illuminate\Database\Eloquent\Model;

class LogTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'log__log_translations';
}
