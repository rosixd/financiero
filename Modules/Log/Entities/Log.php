<?php

namespace Modules\Log\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Log extends Model
{
    use SoftDeletes;

    protected $table = 'ABC10_Log';
    protected $primaryKey = 'ABC10_id';
    protected $fillable = [
        "ABC10_rut",
        "ABC10_sesion",
        "ABC10_ip",
        "ABC10_mac",
        "ABC10_info_browser",
        "ABC10_fecha",
        "ABC10_codigo_transaccion",
        "ABC10_entrada_transaccion",
        "ABC10_salida_transaccion",       
    ];

    public function getABC10_fechaAttribute($value){

		return Carbon::parse($this->attributes["ABC10_fecha"])->format('d-m-Y');
	}

	public function setABC10_fechaAttribute($value)
	{
		$formato = 'd-m-Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		$this->attributes['ABC10_fecha'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function scopeBetween($query, Carbon $from, Carbon $to)
    {
        $query->whereBetween('ABC10_fecha', [$from, $to]);
    }
}
