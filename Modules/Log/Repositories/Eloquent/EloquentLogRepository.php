<?php

namespace Modules\Log\Repositories\Eloquent;

use Modules\Log\Repositories\LogRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentLogRepository extends EloquentBaseRepository implements LogRepository
{
}
