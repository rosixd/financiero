<?php

namespace Modules\Log\Repositories\Cache;

use Modules\Log\Repositories\LogRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheLogDecorator extends BaseCacheDecorator implements LogRepository
{
    public function __construct(LogRepository $log)
    {
        parent::__construct();
        $this->entityName = 'log.logs';
        $this->repository = $log;
    }
}
