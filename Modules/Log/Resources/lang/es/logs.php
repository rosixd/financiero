<?php

return [
    'list resource' => 'Consultar Log',
    'create resource' => 'Crear Log',
    'edit resource' => 'Editar Log',
    'destroy resource' => 'Desactivar Log',
    
    'title' => [
        'logs' => 'Trazabilidad',
    ],
    'button' => [
    ],
    'table' => [
        'event' => 'Evento',
        'new_values' => 'Descripci&oacute;n',
        'url' => 'URL',
        'ip' => 'IP',
        'info_browser' => 'Información del navegador',
        'coments' => 'Tipo de evento',
        'created_at' => 'Fecha',
    ],
    'form' => [
    ],
    'messages' => [
    	'updated sin' => 'Sr. Usuario, no ha realizado cambios para el log.',
    ],
    'validation' => [
    ],
    
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
        'name' => 'Trazabilidad', 
        'log logs' => 'Trazabilidad'
        ]
    ];
    