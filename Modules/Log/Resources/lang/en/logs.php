<?php

return [
	'name' => 'Log',
    'list resource' => 'Consultar Log',
    'title' => [
        'logs' => 'Log',
    ],
    'button' => [
    ],
    'table' => [
        'event' => 'Evento',
        'new_values' => 'Descripci&oacute;n',
        'url' => 'URL',
        'ip' => 'IP',
        'info_browser' => 'Información del navegador',
        'coments' => 'Tipo de evento',
        'created_at' => 'Fecha',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
