@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('log::logs.title.logs') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('log::logs.title.logs') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
{{--                 <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.log.log.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('log::logs.button.create log') }}
                    </a>
                </div> --}}
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4 form-inline">
                            <div class="input-group date" id="datepicker">
                                <input type="text" class="input-sm form-control" name="start" id="start" />
                        {{--          <span class="input-group-addon">Hasta</span>
                                <input type="text" class="input-sm form-control" name="end" id="end"/>  --}}
                            </div>
                            <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Buscar</button>
                        </div>
                    </div>
			        <br>
                    <div class="table-responsive">

                        <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 6, "desc" ]]'>
                            <thead>
                            <tr>
                                <th>{{ trans('log::logs.table.event') }}</th>
                                <th>{{ trans('log::logs.table.new_values') }}</th>
                                <th>{{ trans('log::logs.table.url') }}</th>
                                <th>{{ trans('log::logs.table.ip') }}</th>
                                <th>{{ trans('log::logs.table.info_browser') }}</th>
                                <th>{{ trans('log::logs.table.coments') }}</th>
                                <th>{{ trans('log::logs.table.created_at') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('log::logs.title.create log') }}</dd>
    </dl>
@stop
