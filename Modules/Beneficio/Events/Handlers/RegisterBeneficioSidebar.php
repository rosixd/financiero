<?php

namespace Modules\Beneficio\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterBeneficioSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        // $menu->group(trans('core::sidebar.content'), function (Group $group) {
        //     $group->item(trans('beneficio::beneficios.title.beneficios'), function (Item $item) {
        //         $item->icon('fa fa-thumbs-o-up');
        //         $item->weight(10);
        //         $item->item(trans('beneficio::categorias.title.categorias'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.beneficio.categoria.create');
        //             $item->route('admin.beneficio.categoria.index');
        //         });
        //         $item->item(trans('beneficio::beneficios.title.beneficios'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.beneficio.beneficio.create');
        //             $item->route('admin.beneficio.beneficio.index');
        //         });
        //     });
        // });

        return $menu;
    }
}
