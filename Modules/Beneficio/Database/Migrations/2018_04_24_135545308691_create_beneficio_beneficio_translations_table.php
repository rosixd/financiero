<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficioBeneficioTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficio__beneficio_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('beneficio_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['beneficio_id', 'locale']);
            $table->foreign('beneficio_id')->references('id')->on('beneficio__beneficios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficio__beneficio_translations', function (Blueprint $table) {
            $table->dropForeign(['beneficio_id']);
        });
        Schema::dropIfExists('beneficio__beneficio_translations');
    }
}
