<?php

namespace Modules\Beneficio\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoriaTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'beneficio__categoria_translations';
}
