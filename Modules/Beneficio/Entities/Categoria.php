<?php

namespace Modules\Beneficio\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Categoria extends Model implements Auditable
{
	use SoftDeletes, \OwenIt\Auditing\Auditable;

	protected $table = 'ABC08_Categorias_Beneficios';
	protected $primaryKey = 'ABC08_id';
	
	protected $fillable = [
		"ABC08_nombre",
		"ABC08_imagen",
		"deleted_at",
	];

	public function imagen()
	{
		return $this->hasMany('Modules\Media\Entities\File','id','ABC08_imagen');
	}

	public function beneficio()
	{
		return $this->belongsTo('Modules\Beneficio\Entities\Beneficio', 'ABC08_id', 'ABC08_id');
	}

	protected $appends = ['name','id','key','articulo'];

	public function getNameAttribute()
	{
		return $this->ABC08_nombre;
	}

	public function getIdAttribute()
	{
		return $this->ABC08_id;
	}

	public function getKeyAttribute()
	{
		return "Categor&iacute;a";
	}

	public function getArticuloAttribute()
	{
		return "la";
	}

	public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
