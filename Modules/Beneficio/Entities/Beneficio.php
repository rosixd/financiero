<?php

namespace Modules\Beneficio\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Beneficio extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;
    
    protected $table = 'ABC09_Beneficio';

	protected $primaryKey = 'ABC09_id';

    protected $fillable = [
		"ABC09_nombre",
		"ABC09_imagen",
		"ABC09_imagen_compacta",
		"ABC09_descripcion",
		"ABC09_check_banner",
		"ABC08_id",
    ];

    protected $appends = ['name','id','key','check_baner','description','articulo'];   

    public function getArticuloAttribute()
    {
        return "el";
    }

    public function getNameAttribute()
    {
        return $this->ABC09_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC09_id;
    }

    public function getKeyAttribute()
    {
        return "Beneficio";
    }

    public function getCheckBanerAttribute()
    {
        return ($this->ABC09_check_banner == 1)? "Si":"No";
    }

    public function getDescriptionAttribute()
    {
        return strip_tags($this->ABC09_descripcion);
    }

    public function imagen()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC09_imagen');
    }

    public function imagencompacta()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC09_imagen_compacta');
    }

    public function categoria()
    {
      return $this->hasOne('Modules\Beneficio\Entities\Categoria','ABC08_id','ABC08_id')->withTrashed();
    }

    public function categoriaasig()
    {
      return $this->hasOne('Modules\Beneficio\Entities\Categoria','ABC08_id','ABC08_id');
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
