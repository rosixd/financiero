<?php

namespace Modules\Beneficio\Entities;

use Illuminate\Database\Eloquent\Model;

class BeneficioTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'beneficio__beneficio_translations';
}
