<?php

use Illuminate\Routing\Router;
use Modules\Beneficio\Entities\Categoria;
use Modules\Beneficio\Entities\Beneficio;
/** @var Router $router */

$router->group(['prefix' =>'/beneficio'], function (Router $router) {
    $router->bind('categoria', function ($id) {
        $repo = app('Modules\Beneficio\Repositories\CategoriaRepository')->find($id);
        return (is_null($repo))? Categoria::withTrashed()->find($id):$repo;
    });
    $router->get('categorias', [
        'as' => 'admin.beneficio.categoria.index',
        'uses' => 'CategoriaController@index',
        'script' => 'AppBeneficio',
        'view' => 'index',
        'modulo' => 'beneficio',
        'submodulo' => 'categoria',
        'datatable' => 'admin.categoria.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('categorias/create', [
        'as' => 'admin.beneficio.categoria.create',
        'uses' => 'CategoriaController@create',
        'script' => 'AppBeneficio',
        'view' => 'create',
        'modulo' => 'beneficio',
        'submodulo' => 'categoria',
        'datatable' => 'admin.categoria.datatable'
    ]);
    $router->post('categorias', [
        'as' => 'admin.beneficio.categoria.store',
        'uses' => 'CategoriaController@store',
    ]);
    $router->get('categorias/{categoria}/edit', [
        'as' => 'admin.beneficio.categoria.edit',
        'uses' => 'CategoriaController@edit',
        'script' => 'AppBeneficio',
        'view' => 'edit',
        'modulo' => 'beneficio',
        'submodulo' => 'categoria',
        'datatable' => 'admin.categoria.datatable'
    ]);
    $router->put('categorias/{categoria}', [
        'as' => 'admin.beneficio.categoria.update',
        'uses' => 'CategoriaController@update',
    ]);
    $router->get('categorias/destroy/{categoria}', [
        'as' => 'admin.beneficio.categoria.destroy',
        'uses' => 'CategoriaController@destroy',
    ]);

    $router->bind('beneficio', function ($id) {
        $repo = app('Modules\Beneficio\Repositories\BeneficioRepository')->find($id);
        return (is_null($repo))? Beneficio::withTrashed()->find($id): $repo;
    });

    $router->get('beneficios', [
        'as' => 'admin.beneficio.beneficio.index',
        'uses' => 'BeneficioController@index',
        'script' => 'AppBeneficio',
        'view' => 'index',
        'modulo' => 'beneficio',
        'submodulo' => 'beneficio',
        'datatable' => 'admin.beneficio.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('beneficios/create', [
        'as' => 'admin.beneficio.beneficio.create',
        'uses' => 'BeneficioController@create',
        'script' => 'AppBeneficio',
        'view' => 'create',
        'modulo' => 'beneficio',
        'submodulo' => 'beneficio',
        'datatable' => 'admin.beneficio.datatable'
    ]);
    $router->post('beneficios', [
        'as' => 'admin.beneficio.beneficio.store',
        'uses' => 'BeneficioController@store',
    ]);
    $router->get('beneficios/{beneficio}/edit', [
        'as' => 'admin.beneficio.beneficio.edit',
        'uses' => 'BeneficioController@edit',
        'script' => 'AppBeneficio',
        'view' => 'edit',
        'modulo' => 'beneficio',
        'submodulo' => 'beneficio',
        'datatable' => 'admin.beneficio.datatable'
    ]);
    $router->put('beneficios/{beneficio}', [
        'as' => 'admin.beneficio.beneficio.update',
        'uses' => 'BeneficioController@update',
    ]);
    $router->get('beneficios/destroy/{beneficio}', [
        'as' => 'admin.beneficio.beneficio.destroy',
        'uses' => 'BeneficioController@destroy',
    ]);
    
    $router->get('beneficios/datatable', [
        'as' => "admin.beneficio.datatable",
        "uses" => "BeneficioController@datatable",
    ]);

    $router->get('categorias/datatable', [
        'as' => "admin.categoria.datatable",
        "uses" => "CategoriaController@datatable",
    ]);

});
