<?php

namespace Modules\Beneficio\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Beneficio\Entities\Categoria;
use Modules\Beneficio\Entities\Beneficio;
use Modules\Beneficio\Http\Requests\CreateCategoriaRequest;
use Modules\Beneficio\Http\Requests\UpdateCategoriaRequest;
use Modules\Beneficio\Repositories\CategoriaRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class CategoriaController extends AdminBaseController
{
    /**
     * @var CategoriaRepository
     */
    private $categoria;

    public function __construct(CategoriaRepository $categoria)
    {
        parent::__construct();

        $this->categoria = $categoria;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$categorias = $this->categoria->all();

        return view('beneficio::admin.categorias.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('beneficio::admin.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoriaRequest $request
     * @return Response
     */
    public function store(CreateCategoriaRequest $request)
    {
        $this->categoria->create($request->all());

        return redirect()->route('admin.beneficio.categoria.index')
        ->withSuccess(trans('beneficio::categorias.messages.resource created', ['name' => trans('beneficio::categorias.title.categorias')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Categoria $categoria
     * @return Response
     */
    public function edit(Categoria $categoria)
    {
       $categoria = Categoria::with('imagen')->find($categoria->ABC08_id);
       return view('beneficio::admin.categorias.edit', compact('categoria'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  Categoria $categoria
     * @param  UpdateCategoriaRequest $request
     * @return Response
     */
    public function update(Categoria $categoria, UpdateCategoriaRequest $request)
    {   
        $this->categoria->update($categoria, $request->all());

        return redirect()->route('admin.beneficio.categoria.index')
        ->withSuccess(trans('beneficio::categorias.messages.resource updated', ['name' => trans('beneficio::categorias.title.categorias')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Categoria $categoria
     * @return Response
     */
    public function destroy(Categoria $categoria)
    {   
        $deleted_at = $categoria->deleted_at;
        $json = null;

        $asignado = (is_null($deleted_at) && 
           Categoria::with('beneficio')->find($categoria->ABC08_id)->beneficio)? true:false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('beneficio::categorias.messages.resource assigned')
            );

        }else{

            $success = is_null($deleted_at)? $categoria->delete():$categoria->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('beneficio::categorias.messages.resource desactivate'):trans('beneficio::categorias.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }      

        return response()->json($json);
    }
    
    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response
     */
    
    public function datatable( Request $request )
    {        
        $categoria = Categoria::withTrashed()->get();

        return Datatables::of($categoria)
        ->addColumn('ruta_edit', function ($categoria) {
            return route("admin.beneficio.categoria.edit",[$categoria->ABC08_id]);
        })
        ->addColumn('ruta_destroy', function($categoria){
            return route("admin.beneficio.categoria.destroy",[$categoria->ABC08_id]);
        })
        ->addColumn('inactivo', function ($categoria) {
            return is_null($categoria->deleted_at)? false:true;
        })
        ->make(true); 
    }
}
