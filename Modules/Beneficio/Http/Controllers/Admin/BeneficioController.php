<?php

namespace Modules\Beneficio\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Beneficio\Entities\Beneficio;
use Modules\Beneficio\Entities\Categoria;
use Modules\Beneficio\Http\Requests\CreateBeneficioRequest;
use Modules\Beneficio\Http\Requests\UpdateBeneficioRequest;
use Modules\Beneficio\Repositories\BeneficioRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class BeneficioController extends AdminBaseController
{
    /**
     * @var BeneficioRepository
     */
    private $beneficio;

    public function __construct(BeneficioRepository $beneficio)
    {
        parent::__construct();

        $this->beneficio = $beneficio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {   
       return view('beneficio::admin.beneficios.index', compact(''));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoria = Categoria::all();
        return view('beneficio::admin.beneficios.create', compact('categoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBeneficioRequest $request
     * @return Response
     */
    public function store(CreateBeneficioRequest $request)
    {   
        $this->beneficio->create($request->all());

        return redirect()->route('admin.beneficio.beneficio.index')
        ->withSuccess(trans('beneficio::beneficios.messages.resource created', ['name' => trans('beneficio::beneficios.title.beneficios')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Beneficio $beneficio
     * @return Response
     */
    public function edit(Beneficio $beneficio)
    {
        $categoria = Categoria::all();
        $beneficio = Beneficio::with('imagen','imagencompacta')->find($beneficio->ABC09_id);
        return view('beneficio::admin.beneficios.edit', compact('beneficio', 'categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Beneficio $beneficio
     * @param  UpdateBeneficioRequest $request
     * @return Response
     */
    public function update(Beneficio $beneficio, UpdateBeneficioRequest $request)
    {                   
        $this->beneficio->update($beneficio, $request->all());

        return redirect()->route('admin.beneficio.beneficio.index')
        ->withSuccess(trans('beneficio::beneficios.messages.resource updated', ['name' => trans('beneficio::beneficios.title.beneficios')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Beneficio $beneficio
     * @return Response
     */
    public function destroy(Beneficio $beneficio)
    {
        $deleted_at = $beneficio->deleted_at;
        
        $asignado = (!is_null($deleted_at) && 
            is_null(
                $beneficio->with('categoriaasig')
                ->withTrashed()
                ->find($beneficio->ABC09_id)->categoriaasig
            ))? true:false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('beneficio::beneficios.messages.resource categoria')
            );

        }else{

            $success  = is_null($deleted_at)? $beneficio->delete():$beneficio->restore();
            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('beneficio::beneficios.messages.resource deactivate'):trans('beneficio::beneficios.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        } 

        return response()->json($json);       
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
        $beneficio = Beneficio::with('categoria')->withTrashed()->get();

        return Datatables::of($beneficio)
        ->addColumn('ruta_edit', function ($beneficio) {
            return route("admin.beneficio.beneficio.edit",[$beneficio->ABC09_id]);
        })        
        ->addColumn('ruta_destroy', function($beneficio){
            return route("admin.beneficio.beneficio.destroy",[$beneficio->ABC09_id]);
        })
        ->addColumn('inactivo', function ($beneficio) {
            return is_null($beneficio->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
