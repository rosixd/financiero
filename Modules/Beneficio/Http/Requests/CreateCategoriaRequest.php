<?php

namespace Modules\Beneficio\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCategoriaRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC08_nombre' => 'required',
            'ABC08_imagen' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
