<?php

namespace Modules\Beneficio\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateBeneficioRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC09_nombre' => 'required',
            'ABC08_id' => 'required',
            'ABC09_imagen' => 'required',
            'ABC09_imagen_compacta' => 'required',
            'ABC09_descripcion' => 'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
