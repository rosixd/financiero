<div class="box-body">

	<div class="form-group">
		<label>{{ trans('beneficio::categorias.form.name') }}</label>
		<input type="text" class="form-control requerido soloLetras" name="ABC08_nombre" id="ABC08_nombre">
		<span class="text-danger">
			{{ $errors->has('ABC08_nombre') ? trans('beneficio::categorias.validation.name') : '' }}
		</span>
	</div>

	@include('core::selectImg',
		[
			'name'=>'ABC08_imagen', 
			'label' => trans('beneficio::categorias.form.logo'),
			'required' => trans('beneficio::categorias.validation.image')
		]
	)
	
</div>
