<div class="box-body">
	<div class="form-group">
		<label>{{ trans('beneficio::beneficios.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC09_nombre" id="ABC09_nombre" value="{{ $beneficio->ABC09_nombre }}">
		<span class="text-danger">
			 {{ $errors->has('ABC09_nombre') ? trans('beneficio::beneficios.validation.name') : '' }}
		</span>
	</div>

	@include('core::selectImg',
		['name'=>'ABC09_imagen', 
		 'label' => trans('beneficio::beneficios.form.image'),
		 'img' => $beneficio->imagen->first()->filename,
		 'id' => $beneficio->ABC09_imagen,
		 'required' => trans('beneficio::beneficios.validation.image')]
		 )
    
    @include('core::selectImg',
    	['name'=>'ABC09_imagen_compacta', 
    	 'label' => trans('beneficio::beneficios.form.compact_image'),
    	 'img' => $beneficio->imagencompacta->first()->filename,
    	 'id' => $beneficio->ABC09_imagen_compacta,
    	 'required' => trans('beneficio::beneficios.validation.compact_image') ])

	<div class="form-group">
		<label>{{ trans('beneficio::beneficios.form.long_description') }}</label>
		<textarea class="form-control ckeditor" name="ABC09_descripcion" id="ABC09_descripcion">{{$beneficio->ABC09_descripcion}}</textarea>
		<span class="text-danger">
			{{ $errors->has('ABC09_descripcion') ? trans('beneficio::beneficios.validation.long_description') : '' }}
		</span>
	</div>

	<div class="form-group">

		<select name="ABC08_id" class="select">

			<option value=""> Seleccione </option>

			@foreach($categoria as $cat)
				<option {{ ($beneficio->ABC08_id == $cat->ABC08_id)? "selected":"" }}  value="{{$cat->ABC08_id}}">{{$cat->ABC08_nombre}}</option>
			@endforeach

		</select>

		<span class="text-danger">
			{{ $errors->has('ABC08_id') ? trans('beneficio::beneficios.validation.category') : '' }}
		</span>

	</div>
	<div class="form-check">
		<input type="checkbox" {{($beneficio->ABC09_check_banner == 1) ? 'checked':''}} class="form-check-input" id="ABC09_check_banner" name="ABC09_check_banner" value="1">
		<label class="form-check-label" for="exampleCheck1">{{ trans('beneficio::beneficios.form.check_banner') }}
		</label>
	</div>
</div>
