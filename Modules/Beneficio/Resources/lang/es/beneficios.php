<?php

return [
    'list resource' => 'Consultar Beneficios',
    'create resource' => 'Crear Beneficios',
    'edit resource' => 'Editar Beneficios',
    'destroy resource' => 'Eliminar Beneficios',
    'title' => [
        'beneficios' => 'Beneficios',
        'create beneficio' => 'Crear beneficio',
        'edit beneficio' => 'Editar beneficio',
    ],
    'button' => [
        'create beneficio' => 'Crear beneficio',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [
        'name' =>'Nombre',
        'check_banner' =>'Check Banner',
        'long_description' =>'Descripci&oacute;n',
        'category' =>'Categor&iacute;a',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'check_banner' =>'Check Banner',
        'long_description' =>'Descripci&oacute;n',
        'category' =>'Categor&iacute;a',
        'image' =>'Imagen',
        'compact_image' =>'Imagen compactada',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource deactivate' => 'Sr. Usuario el beneficio se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, el  beneficio se ha activado correctamente.',
        'resource categoria' => 'Sr. Usuario, el  beneficio no se puede desactivar ya que contiene datos asociados.'
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'image' => 'Sr. Usuario, debe ingresar la imagen.',
        'compact_image' => 'Sr. Usuario, debe ingresar la imagen compactada.',
        'category' => 'Sr. Usuario, debe ingresar una categor&iacute;a.',
        'long_description' =>'Sr. Usuario, debe ingresar una descripci&oacute;n',
    ],
];
