<?php

return [
    'list resource' => 'Consultar categor&iacute;as',
    'create resource' => 'Crear categor&iacute;a',
    'edit resource' => 'Editar categor&iacute;a',
    'destroy resource' => 'Eliminar categor&iacute;a',
    'title' => [
        'categorias' => 'Categor&iacute;as',
	'category' => 'Categor&iacute;a',
        'create categoria' => 'Crear Categor&iacute;a',
        'edit categoria' => 'Editar Categor&iacute;a',
    ],
    'button' => [
        'create categoria' => 'Crear Categor&iacute;a',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
        'select img' => 'seleccionar imagen'
    ],
    'table' => [
        'name' =>'Nombre',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'logo' =>'Logo',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la categor&iacute;a se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario, categor&iacute;a no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource desactivate' => '<p>Categor&iacute;a desactivada &eacute;xitosamente.</p>',
        'resource activate' => 'Sr. Usuario, la categor&iacute;a se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, la categor&iacute;a est&aacute; asignada a un beneficio.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'image' => 'Sr. Usuario, debe ingresar la imagen.',
        'compact_image' => 'Sr. Usuario, debe ingresar la imagen compactada.',
        'category' => 'Sr. Usuario, debe ingresar una categor&iacute;a.',
        'long_description' =>'Sr. Usuario, debe ingresar una descripci&oacute;n',
    ],
];
