<?php

namespace Modules\Beneficio\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Beneficio\Events\Handlers\RegisterBeneficioSidebar;

class BeneficioServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterBeneficioSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('categorias', array_dot(trans('beneficio::categorias')));
            $event->load('beneficios', array_dot(trans('beneficio::beneficios')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('beneficio', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Beneficio\Repositories\CategoriaRepository',
            function () {
                $repository = new \Modules\Beneficio\Repositories\Eloquent\EloquentCategoriaRepository(new \Modules\Beneficio\Entities\Categoria());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Beneficio\Repositories\Cache\CacheCategoriaDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Beneficio\Repositories\BeneficioRepository',
            function () {
                $repository = new \Modules\Beneficio\Repositories\Eloquent\EloquentBeneficioRepository(new \Modules\Beneficio\Entities\Beneficio());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Beneficio\Repositories\Cache\CacheBeneficioDecorator($repository);
            }
        );
// add bindings


    }
}
