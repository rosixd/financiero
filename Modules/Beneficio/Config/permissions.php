<?php

return [
    'beneficio.categorias' => [
        'index' => 'beneficio::categorias.list resource',
        'create' => 'beneficio::categorias.create resource',
        'edit' => 'beneficio::categorias.edit resource',
        'destroy' => 'beneficio::categorias.destroy resource',
    ],
    'beneficio.beneficios' => [
        'index' => 'beneficio::beneficios.list resource',
        'create' => 'beneficio::beneficios.create resource',
        'edit' => 'beneficio::beneficios.edit resource',
        'destroy' => 'beneficio::beneficios.destroy resource',
    ],
// append


];
