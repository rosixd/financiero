<?php

namespace Modules\Beneficio\Repositories\Cache;

use Modules\Beneficio\Repositories\CategoriaRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCategoriaDecorator extends BaseCacheDecorator implements CategoriaRepository
{
    public function __construct(CategoriaRepository $categoria)
    {
        parent::__construct();
        $this->entityName = 'beneficio.categorias';
        $this->repository = $categoria;
    }
}
