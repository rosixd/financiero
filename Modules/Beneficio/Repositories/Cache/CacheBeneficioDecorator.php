<?php

namespace Modules\Beneficio\Repositories\Cache;

use Modules\Beneficio\Repositories\BeneficioRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBeneficioDecorator extends BaseCacheDecorator implements BeneficioRepository
{
    public function __construct(BeneficioRepository $beneficio)
    {
        parent::__construct();
        $this->entityName = 'beneficio.beneficios';
        $this->repository = $beneficio;
    }
}
