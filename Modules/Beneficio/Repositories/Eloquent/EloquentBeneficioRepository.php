<?php

namespace Modules\Beneficio\Repositories\Eloquent;

use Modules\Beneficio\Repositories\BeneficioRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBeneficioRepository extends EloquentBaseRepository implements BeneficioRepository
{
}
