<?php

namespace Modules\Beneficio\Repositories\Eloquent;

use Modules\Beneficio\Repositories\CategoriaRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoriaRepository extends EloquentBaseRepository implements CategoriaRepository
{
}
