<?php

namespace Modules\Translation\Entities;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property string value
 */
class TranslationTranslation extends Model implements Auditable
{
    use RevisionableTrait;
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $fillable = ['value'];
    protected $table = 'translation__translation_translations';

    protected $revisionEnabled = true;
    protected $revisionCleanup = true;
    protected $historyLimit = 100;
    protected $revisionCreationsEnabled = true;
    protected $appends = [ 'llave_audit'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->historyLimit = config('asgard.translation.config.revision-history-limit', 100);
    }

    public function getLlaveAuditAttribute()
    {
        return 'translation_translation';
    }
    public function setLlaveAuditAttribute()
    {
        return 'translation_translation';
    }
    public static function boot()
    {
        parent::boot();
    }

    

}
