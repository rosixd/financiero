<?php

namespace Modules\Translation\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Translation extends Model implements Auditable
{
    use Translatable, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'translation__translations';
    public $translatedAttributes = ['value'];
    protected $fillable = ['key', 'value'];
    protected $appends = ['articulo', 'llave', 'llave_audit'];  
    

    public function getArticuloAttribute()
    {
        return "el";
    }    

    public function getLlaveAttribute()
    {
        return $this->key;
    }
    public function getLlaveAuditAttribute()
    {
        return 'translation';
    }
    public function setLlaveAuditAttribute()
    {
        return 'translation';
    }
    
    public function translationtranslation()
    {
      return $this->hasOne('Modules\Translation\Entities\TranslationTranslation','id','id');
    }
    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }

}
