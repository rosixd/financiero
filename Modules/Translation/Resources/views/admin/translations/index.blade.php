@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('translation::translations.title.translations') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('translation::translations.title.translations') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="" class="btn btn-md btn-primary jsClearTranslationCache"><i class="fa fa-refresh"></i> {{ trans('translation::translations.Clear translation cache') }}</a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover tableTranslation" id="dataTable" >
                        <thead>
                        <tr>
                            <th>Llave</th>
                            <th>Mensaje</th>
			                {{-- <th>{{ trans('user::users.table.created-at') }}</th> --}}
                            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                       <!-- <tfoot>
                        <tr>
                            <th>Llave</th>
                            <th>Mensaje</th>
			                {{-- <th>{{ trans('user::users.table.created-at') }}</th> --}}
                            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                        </tr>
                        </tfoot> -->
                    </table>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>

@include('core::modalLog')
@stop

@push('js-stack')
    <?php if ($errors->has('file')): ?>

    <?php endif; ?>
    <?php $locale = locale(); ?>
@endpush

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('translation.translations.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('translation.translations.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('translation.translations.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('translation.translations.index') ? 'true' : 'false' }}
    };
</script>
@endprepend