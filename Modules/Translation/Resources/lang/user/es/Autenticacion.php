<?php

return [

    /* Autenticacion */
    'clave incorrecta'          => 'Rut y/o clave inv&aacute;lida.',
    'rut no valido'             => 'Rut y/o clave inv&aacute;lida.',
    'clave no numericos'        => 'Rut y/o clave inv&aacute;lida.',
    'cliente no existe'         => 'Rut y/o clave inv&aacute;lida.',
    'bloqueo duro'              => 'Tu clave internet ha sido bloqueda por superar máximo de reintentos permitidos. Para solicitar desbloqueo llamar al 600 830 22 22',
    'bloqueo blando'            => 'Tu clave internet ha sido bloqueda por superar máximo de reintentos permitidos.',
    'expira sesion'             => 'Su sesión está próxima a expirar, Necesitas mas tiempo?',
    'clave dinamica no correcta'=> 'Clave dinámica no es correcta. Favor intentar nuevamente.',
    'clave dinámica expirada'   => 'Clave dinámica expiró. Debe solicitarla nuevamente',

];