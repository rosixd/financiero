<?php

return [

    /* Recuperar clave internet */
    'cliente no es visa'                           => 'Para obtener tu Clave Internet debes solicitarla en cualquiera de nuestras tiendas abcdin.',
    'cliente es visa y no tiene email'             => 'No es posible recuperar clave, debido a que no tienes un email registrado. Para solicitar el reinicio de tu clave puedes ir a cualquiera de nuestras tiendas abcdin.',
    'cliente es visa y no tiene clave de internet' => 'No tienes activada la clave de internet, para poder habilitarla debes ir a cualquiera de nuestras tiendas abcdin.',
    'rut ingresado no es valido'                   => 'Rut ingresado no es válido.',
    'retorno app 301'                              => 'Debes ingresar una clave internet numérica de 4 dígitos.',
    'retorno app 305'                              => 'Tu clave se encuentra bloqueada. Acércate a Servicio de Atención al Cliente de tu tienda ABCDin más cercana.',
    'retorno app 307'                              => 'No podemos enviarle el código de confirmación, no posee datos de contacto asociado a su cuenta. Regularice en cualquier tienda abcdin.',
    'retorno app 308'                              => 'No tienes activada la clave de internet. Para poder habilitarla debes ir a cualquiera de nuestras tiendas abcdin.',
    'retorno app 303'                              => 'Las claves ingresadas no coinciden, favor intentar nuevamente.',
    'retorno app 309'                              => 'El código ingresado no es correcto, favor ingresarlo nuevamente.',
    'retorno app 302'                              => 'La clave de internet antigua es incorrecta.',
    'retorno app 304'                              => 'No podemos enviarle el código de confirmación, no posee datos de contacto asociado a su cuenta. Regularice en cualquier tienda abcdin.',
    'fuera de servicio'                            => 'Servicio no disponible. Favor intente más tarde.',
    'clave exito'                                  => 'Tu clave ha sido modificada con éxito.'.
                                                        ' Hemos enviado una notificación del cambio a tu email.' ,
    'obligatorio'                                  => 'Debe ingresar todos los datos marcados como obligatorios.',
    'recuperarclave rut no valido'                 => 'Rut inv&aacute;lido.',
    'recuperarclave claves no coinciden'           => 'Las claves ingresadas no coinciden, favor intentar nuevamente.',
    'expiredgrecaptcha'                            => 'La verificación ha caducado, por favor marque nuevamente la casilla de verificación.',
    'errorgrecaptcha'                              => 'No pudimos completar la verificación, por favor intentarlo nuevamente.',
];