<?php

return [
    'new-role' => 'Perfil',
    'button' => [
        'new-role' => 'Crear',
    'new-roles' => 'Guardar',
    ],
    'title' => [
        'roles' => 'Perfiles',
        'edit' => 'Editar perfil',
        'users-with-roles' => 'Usuarios con este perfil',
    ],
    'breadcrumb' => [
        'roles' => 'Pefiles',
        'new' => 'Nuevo',
        'edit' => 'Editar perfil',
    ],
    'table' => [
        'id' => 'Id',
        'slug' => 'perfil',
        'name' => 'Nombre',
        'created at' => 'Fecha Creaci&oacute;n',
        'action' => 'Acci&oacute;n',
    ],
    'tabs' => [
        'data' => 'Datos',
        'permissions' => 'Permisos',
    ],
    'form' => [
        'name' => 'Nombre',
        'slug' => 'URL amigable del perfil',
    ],
    'navigation' => [
        'back to index' => 'Regresar',
    ],
    'messages' => [
        'rol desactivate' => 'Sr. Usuario, el perfil se ha desactivado correctamente.',
        'rol activate'  => 'Sr. Usuario, el perfil se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, el perfil no se puede desactivar ya que contiene datos asociados.',
        'resource error' => 'Sr. Usuario, no se ha podido desactivar el perfil, por favor inténtelo nuevamente.',
        'validate edit' => 'Sr. Usuario, no ha realizado cambios para el perfil.',
    ],
    'select all' => 'Seleccionar todos',
    'deselect all' => 'Deseleccionar todos',
    'swap' => 'Cambiar',
    'allow all' => 'Permitir todo',
    'deny all' => 'Denegar todo',
    'inherit all' => 'Heredar todo',
    'allow' => 'Permitir',
    'deny' => 'Denegar',
    'inherit' => 'Heredar de perfil',
    'list resource' => 'Listar perfil',
    'create resource' => 'Crear perfil',
    'edit resource' => 'Editar perfil',
    'destroy resource' => 'Desactivar perfil',

    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'name' => 'Perfiles',
    ],
];
