<?php

return [

    /* Actualiza tus datos */
   'bloqueo'        => 'Tu tarjeta abcvisa ha sido bloqueada con éxito. Las compras, avances y super avances se encuentran desactivadas.',
   'desbloqueo'     => 'Tu tarjeta abcvisa ha sido desbloqueada con éxito. Las compras, avances y super avances se encuentran activadas.',
   'otro'           => 'Algo ha salido mal, estamos trabajando para resolverlo, por favor intenta de nuevo más tarde.',
];
