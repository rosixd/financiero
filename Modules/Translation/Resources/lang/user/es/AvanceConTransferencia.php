<?php

return [
    /* Avance con transferencia (público) */
    'rut ingresado no es valido'        =>  'Rut ingresado no es válido',
    'falta ingresar dato obligatorio'   =>  'Debes ingresar todos los datos marcados como obligatorios',
    'cliente visa'                      =>  'Ya eres cliente, ingresa a Tu Cuenta y solicita tu avance en línea',
    'cliente no tiene visa'             =>  '¿Aún no tienes tu abcVISA? Pídela ahora, y evalúate en línea!',

    /*Avance con transferencia (privado) */
    'no tiene cuentas disponibles para realizar la transferencia'   =>  'Debes agregar al menos una cuenta para realizar avance con transferencia',
    'falta dato obligatorio para crear cuenta'                      =>  'Debes ingresar todos los datos marcados como obligatorios',
    'cuenta creada exitosamente'                                    =>  'Cuenta destino ingresada correctamente',
    'no selecciona cuenta para transferencia'                       =>  'Debes elegir una cuenta destino para realizar avance con transferencia ',
];