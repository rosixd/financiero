<?php

return [
 
    /* Próximos vencimientos */
    'jordan no disponible'              => 'Los Servicios no se encuentran disponibles. Favor reintentar en los próximos minutos.',
    'eecc no disponible'                => 'Los Servicios no se encuentran disponibles. Favor reintentar en los próximos minutos.',
    'cliente no tiene eecc disponibles' => 'No tienes estados de cuenta emitidos en los últimos seis meses.',
    
];