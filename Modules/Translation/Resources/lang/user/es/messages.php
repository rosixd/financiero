<?php

return [
    /* Authentication */
    'successfully logged in' => 'La identificaci&oacute;n se ha realizado con &eacute;xito',
    'account created check email for activation' => 'Cuenta creada. Por favor verifique su correo electr&oacute;nico para activar su cuenta.',
    'account activated you can now login' => 'Sr. Usuario, su cuenta ha sido activada. Ahora puede iniciar sesi&oacute;n.',
    'there was an error with the activation' => 'Sr. Usuario, su cuenta ya ha sido activada. Ingrese con sus credenciales.',
    'no user found' => 'Sr. Usuario, el correo electrónico ingresado no se encuentra en nuestros registros.',
    'check email to reset password' => 'Sr. Usuario, revise su correo electr&oacute;nico para restablecer su contrase&ntilde;a.',
    'user no longer exists' => 'El usuario ya no existe.',
    'invalid reset code' => 'C&oacute;digo de restablecimiento inv&aacute;lido o caducado.',
    'password reset' => 'La contrase&ntilde;a ha sido restablecida. Ahora ya puede iniciar sesi&oacute;n con su nueva contrase&ntilde;a.',
    'password reset2' => 'Sr. Usuario, su contraseña se ha guardado exitosamente.',

    'welcome' => 'Bienvenido.',

    /* User management */
    'reset password' => 'Restablecer Contraseña.',
    'user created' => 'El Usuario fue creado correctamente.',
    'user not found' => 'Usuario no encontrado.',
    'user updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
    'user deleted' => 'Usuario eliminado exitosamente.',
    /* Role management */
    'role created' => 'Rol creado exitosamente.',
    'role not found' => 'Rol no encontrado.',
    'role updated' => 'Rol actualizado exitosamente.',
    'role deleted' => 'Rol eliminado exitosamete.',

    'messages' => [
        'welcome' => 'Bienvenido.',
        'updated sin' => 'Sr. Usuario, no ha realizado cambios.',
    ],

    //darse de alta
    'dar_alta' => [
        'titulo' => 'Titulo',
        'asunto' => 'Usuario Creado ABCDIN',
        'cabecera' => 'Sr. Usuario',
        'cuerpo' => 'Se ha creado un nuevo usuario con sus datos ' .
            'en el sistema administrador de contenido de ABCDIN, ' .
            'por favor ingrese {{ url }} para agregar su contrase&ntilde;a y acceder a la plataforma.',
        'pie' => 'Saludos cordiales.',
    ],
];
