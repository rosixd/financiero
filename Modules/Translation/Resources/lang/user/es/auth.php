<?php

return [
    'logi' => 'Ingresar',
    'login' => 'Login',
    'email' => 'Correo electr&oacute;nico',
    'rememberme' => 'Recordarme',
    'forgotpassword' => 'Restablecer Contrase&ntilde;a',
    'register' => 'Registrar un nuevo usuario',
    'password' => 'Contrase&ntilde;a',
    'passwordconfirmation' => 'Confirmaci&oacute;n de contrase&ntilde;a',
    'register me' => 'Registrarme',
    'I already have a membership' => 'Ya soy miembro',
    'resetpassword' => 'Restablecer',
    'reset password' => 'Actualizar contrase&ntilde;a',
    'Iremembered my password' => 'Volver',
    'toresetpasswordcompletethis form' => 'Sr. Usuario: '.
        'Ud. ha solicitado restablecer su contraseña. Por favor ingrese {{ url }}.',
    'to reset password complete this form' => 'Para restablecer la contraseña, complete este formulario:',
    'welcome title' => 'Bienvenido',
    'welcome title2' => 'Usuario Creado ABCDIN',
    'please confirm email' => 'Sr. Usuario, por favor confirmar email',
    'additionalconfirm email message' => 'Sr. Usuario. se ha creado un nuevo usuario con sus datos ' . 
                            'en el sistema administrador de contenido de ABCDIN, ' . 
                            'por favor ingrese {{ url }}.',
    'confirm email button' => 'Confirmar correo electr&oacute;nico',
    'end greeting' => 'Saludos cordiales.',
    'sign in welcome message' => 'Iniciar sesi&oacute;n',
    'reset password email was sent' => 'Sr. Usuario, se ha enviado un correo electrónico para restablecer contraseña',
    'validation' => [
        'email' => 'Sr. Usuario, debe ingresar el correo electr&oacute;nico para acceder.',
        'password' => 'Sr. Usuario, debe ingresar su contrase&ntilde;a para acceder.',
        'emails' => 'Sr. Usuario, debe ingresar el correo electr&oacute;nico solicitado.',
        'passwordcomplete' => 'Sr. Usuario, debe ingresar la contrase&ntilde;a solicitada.',
	    'confirmcomplete' => 'Sr. Usuario, debe ingresar nuevamente la contrase&ntilde;a.',
    ],
];
