<?php

return [

    /* Actualiza tus datos */
   'mail ingresado para envío de eecc es erroneo'        =>	'El mail ingresado no es válido. Favor reingrese',
   'error en ingreso de tipo de dato'                    => 'Debes ingresar correctamente los datos a actualizar',
   'cliente modifica parcialmente datos de direccion'    => 'Para modificar la dirección debes ingresar todos los datos requeridos',
];