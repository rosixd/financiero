<?php

return [
    
    /* Crédito de Consumo */
    'rut no valido'                     => 'Rut ingresado no es válido.',
    'falta ingresar dato obligatorio'   => 'Debes ingresar todos los datos marcados como obligatorios.',
];