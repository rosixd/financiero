<?php

return [
    'cancel' => 'Cancelar',
    'delete' => 'Desactivar',
    'create' => 'Crear',
    'update' => 'Actualizar',
];
