<?php

return [
    
    /* Bloqueo tarjeta */
    'bloqueo exitoso'    =>	'Tu tarjeta ha sido bloqueada exitosamente',
    'desbloqueo exitoso' => 'Tu tarjeta ha sido desbloqueada exitosamente',
    'tarjeta no emitida' => 'No se puede bloquear Tarjeta ya que no ha sido emitida',

    /*Paga tu tarjeta*/ 
    'no ha seleccionado medio de pago'                   => 'Debes seleccionar el medio de pago',
	'ingresa monto a abonar mayor que la deuda total'	 => 'Debes ingresar un monto menor a la deuda total',
	'selecciona abonar y no agrega monto'	             => 'Debes ingresar el valor a abonar',
	'pago exitoso'	                                     => 'Tu pago ha sido realizado exitosamente',
	'pago fallido'	                                     => 'Tu pago ha sido rechazado. Favor vuelve a intentarlo con otro medio de pago ',
];

