<?php

return [

    /* Solicitud de Tarjeta */
    'cliente rechazado'                   => 'TU TARJETA ABCVISA NO FUE APROBADA.',
    'cliente aprobado'                    => 'TU TARJETA ABCVISA FUE APROBADA!.',
    'rut es de un cliente'                => 'YA ERES CLIENTE! SI NECESITAS MÁS INFORMACIÓN DE TU CUENTA LLAMA AL 600 830 2222 O DIRÍGETE A TU TIENDA MÁS CERCANA. DESCUBRE LOS BENEFICIOS EXCLUSIVOS DE TU TARJETA EN WWW.ABCVISA.CL',
    'rut ingresado no es valido'          => 'EL RUT NO ES VÁLIDO',
    'dato obligatorio'                    => 'DEBES INGRESAR TODOS LOS DATOS MARCADOS COMO OBLIGATORIOS.',
    'no acepta terminos y condiciones'    => 'DEBES ACEPTAR LOS TÉRMINOS Y CONDICIONES.',
    'ok cliente'                          => 'NO ES CLIENTE, NO ES FUNCIONARIO, NO ES LISTA NEGRA.',
    'no ingresado serie'                  => 'NO HAS INGRESADO EL NÚMERO DE SERIE DE TU CÉDULA DE IDENTIDAD, ESTE DATO ES OPCIONAL, PERO NOS AYUDA A COMPROBAR TU IDENTIDAD Y EVALUARTE MEJOR. SI DESEAS CONTINUAR SIN ESTE DATO HAZ CLICK EN “SIGUIENTE"',
    'rut no valido'                       => 'EL RUT NO ES VÁLIDO',
];