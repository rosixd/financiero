<?php

return [
    /* Authentication */
    'successfully logged in' => 'Successfully logged in',
    'account created check email for activation' => 'Account created. Please check your email to activate your account.',
    'account activated you can now login' => 'Account activated. You can now login.',
    'there was an error with the activation' => 'User already activated. You can already login with your credentials.',
    'no user found' => 'No user with that email address belongs in our system.',
    'check email to reset password' => 'Check your email to reset your password.',
    'user no longer exists' => 'The user no longer exists.',
    'invalid reset code' => 'Invalid or expired reset code.',
    'password reset' => 'Password has been reset. You can now login with your new password.',
    /* Email subjects */
    'welcome' => 'Welcome.',
    'reset password' => 'Reset your account password.',
    /* User management */
    'user created' => 'User successfully created.',
    'user not found' => 'User not found.',
    'user updated' => 'User successfully updated.',
    'user deleted' => 'User successfully deleted.',
    /* Role management */
    'role created' => 'Role successfully created.',
    'role not found' => 'Role not found.',
    'role updated' => 'Role successfully updated.',
    'role deleted' => 'Role successfully deleted.',
    /* Profile management */
    'profile updated' => 'Profile successfully updated.',
    'usuario AbcDin' => 'Esta es una prueba de mensajes',
    'user'=> 'Usuario',
    'messages' => [
        'welcome' => 'Welcome.',
    ],
    //darse de alta
    'dar_alta' => [
        'asunto' => 'Usuario Creado ABCDIN',
        'cabecera' => 'Sr. Usuario',
        'cuerpo' => 'Se ha creado un nuevo usuario con sus datos ' .
            'en el sistema administrador de contenido de ABCDIN, ' .
            'por favor ingrese ' .
            '<a href="' . URL::to("auth/reset/" . $user['id'] . "/" . $code) . '">' .
                'AQU&Iacute;' .
            '</a> para agregar su contraseña y acceder a ' .
            'la plataforma.',
        'pie' => 'Saludos cordiales.',
    ],
];
