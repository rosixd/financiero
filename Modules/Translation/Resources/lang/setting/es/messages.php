<?php

return [
    'settings saved' => 'Sr. Usuario, la configuración se ha creado correctamente.',
    'updated sin' => 'Sr. Usuario, no ha realizado cambios para la configuración.',
    'settings update' => 'Sr. Usuario, los datos se han actualizado correctamente.',
];
