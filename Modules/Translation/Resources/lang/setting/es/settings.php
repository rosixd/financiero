<?php

return [
    'title' => [
        'settings' => 'Configuración',
        'general settings' => 'Configuración general',
        'module settings' => 'Configuración de módulos',
        'module name settings' => 'Configuración del módulo :module',
    ],
    'breadcrumb' => [
        'settings' => 'Configuración',
        'module settings' => 'Configuración del módulo :module',
    ],
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'name' => 'Configuraci&oacute;n',
        'setting settings' => 'Configuraci&oacute;n',
    ],
    'list resource' => 'Consultar Configuraci&oacute;n',
    'create resource' => 'Crear Configuraci&oacute;n',
    'edit resource' => 'Editar Configuraci&oacute;n',
    'destroy resource' => 'Desactivar Configuraci&oacute;n',
];
