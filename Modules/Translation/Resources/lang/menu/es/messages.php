<?php

return [
    /* Menu management */
    'menu created' => 'Sr. Usuario, el menú se ha creado correctamente.',
    'menu not found' => 'Sr. Usuario, el menú no encontrado.',
    'menu updated' => 'Sr. Usuario, el menú se ha actualizado correctamente.',
    'menu deleted' => 'Menú eliminado éxitosamente.',
    /* MenuItem management */
    'menuitem created' => 'Sr. Usuario, el ítem se ha creado exitosamente',
    'menuitem not found' => 'Ítem del menú no encontrado.',
    'menuitem updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
    'menuitem deleted' => 'Ítem del menú eliminado éxitosamente.',
    'resource nodesactivate' => 'Sr. Usuario, el menú no se puede desactivar ya que contiene datos asociados.',
    'updated sin' => 'Sr. Usuario, no ha realizado cambios para menú',

    'resource desactivate' => 'Sr. Usuario, el menú se ha desactivado correctamente.',
    'resource activate' => 'Sr. Usuario, el menú se ha activado correctamente.',
];
