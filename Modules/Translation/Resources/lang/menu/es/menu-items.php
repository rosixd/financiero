<?php

return [
    'list resource' => 'Consultar &Iacute;tem men&uacute;',
    'create resource' => 'Crear &Iacute;tem men&uacute;',
    'edit resource' => 'Editar &Iacute;tem men&uacute;',
    'destroy resource' => 'Desactivar &Iacute;tem men&uacute;',
    
    'form' => [
        'page' => 'Página',
        'module' => 'Módulo',
        'target' => 'Abrir en',
        'same tab' => 'En la misma pestaña',
        'new tab' => 'En una nueva pestaña',
        'icon' => 'Icono',
        'parent menu item' => 'Ítem del menú superior',
        'class' => 'Clase CSS',
    ],
    'link-type' => [
        'link type' => 'Tipo de enlace',
        'page' => 'Página',
        'internal' => 'Interna',
        'external' => 'Externa',
    ],
];
