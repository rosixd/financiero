<?php

return [
    'title'  => 'Menú', 
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'name' => 'Men&uacute;',
    'list menus' => 'Consultar menus',
    'create menus' => 'Crear menus',
    'edit menus' => 'Editar menus',
    'destroy menus' => 'Eliminar menus',

    'titles' => [
        'menu' => 'Menú',
        'create menu' => 'Crear',
        'edit menu' => 'Editar menú',
        'create menu item' => 'Crear ítem del menú',
        'edit menu item' => 'Editar ítem del menú',
    ],
    'breadcrumb' => [
        'menu' => 'Menú',
        'create menu' => 'Crear menú',
        'edit menu' => 'Editar menú',
        'create menu item' => 'Crear ítem del menú',
        'edit menu item' => 'Editar ítem del menú',
    ],
    'button' => [
        'create menu item' => 'Crear ítem del menú',
        'create menu' => 'Crear',
    ],
    'table' => [
        'name' => 'Nombre',
        'title' => 'Título',
	    'create-at' => 'Fecha Creaci&oacute;n',
    ],
    'form' => [
        'title' => 'Título',
        'name' => 'Nombre',
        'status' => 'Mostrar',
        'uri' => 'URI',
        'url' => 'URL',
        'primary' => 'Menú principal (usado para rutas del sitio)',
    ],
    'navigation' => [
        'back to index' => 'Regresar',
    ], 
    
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'menu menus' => 'Men&uacte;',
        'menu menuitems' => 'Items Men&uacte;',
    ],
    'list resource' => 'Consultar Men&uacute;',
    'create resource' => 'Crear Men&uacute;',
    'edit resource' => 'Editar Men&uacute;',
    'destroy resource' => 'Desactivar Men&uacute;',
];
