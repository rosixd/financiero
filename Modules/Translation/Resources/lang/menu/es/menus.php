<?php

return [
    'title'  => 'Menú',

    'list menus' => 'Consultar menus',
    'create menus' => 'Crear menus',
    'edit menus' => 'Editar menus',
    'destroy menus' => 'Eliminar menus',

    'list menu items' => 'Consultar item menus',
    'create menu items' => 'Crear item menus',
    'edit menu items' => 'Editar item menus',
    'destroy menu items' => 'Eliminar item menus',

    'list resource' => 'Consultar &Iacute;tem men&aacute;',
    'create resource' => 'Crear &Iacute;tem men&aacute;',
    'edit resource' => 'Editar &Iacute;tem men&aacute;',
    'destroy resource' => 'Desactivar &Iacute;tem men&aacute;',

    'titles' => [
        'menu' => 'Administración del menú',
        'create menu' => 'Crear un menú',
        'edit menu' => 'Editar un menú',
        'create menu item' => 'Crear un ítem del menú',
        'edit menu item' => 'Editar un ítem del menú',
    ],
    'breadcrumb' => [
        'menu' => 'Administración del menú',
        'create menu' => 'Crear un menú',
        'edit menu' => 'Editar un menú',
        'create menu item' => 'Crear un ítem del menú',
        'edit menu item' => 'Editar un ítem del menú',
    ],
    'button' => [
        'create menu item' => 'Crear un ítem del menú',
        'create menu' => 'Crear un menú',
    ],
    'table' => [
        'name' => 'Nombre',
        'title' => 'Título',
    ],
    'form' => [
        'title' => 'Título',
        'name' => 'Nombre',
        'status' => 'Mostrar',
        'uri' => 'URI',
        'url' => 'URL',
        'primary' => 'Menú principal (usado para rutas del sitio)',
    ],
    'navigation' => [
        'back to index' => 'Regresar',
    ],

    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'name' => 'Men&uacute;',
        'menu menus' => 'Men&uacute;',
        'menu menuitems' => 'Items Men&uacute;',
    ],
];
