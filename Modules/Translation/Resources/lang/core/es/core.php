<?php

return [
    'modal' => [
        'title' => 'Confirmación',
        'confirmation-message' => '¿Estás seguro que quieres eliminar este registro?',
        'title log' => 'Log Actividades',

    ],
    'table' => [
        'created at' => 'Fecha Creaci&oacute;n',
        'actions' => 'Acciones',
        'thumbnail' => 'Miniaturas',
        'resource nombre usuario' => 'Nombre Usuario',
        'resource accion' => 'Acci&oacute;n',
        'resource fecha' => 'Fecha',
        'resource comentario' => 'Comentario',
        'resource navegador' => 'Navegador',
        'resource version' => 'Versi&oacute;n Navegador',
        'resource ip' => 'Ip',
    ],
    'tab' => [
        'english' => 'Inglés',
        'french' => 'Frances',
        'dutch' => 'Holandes',
        'italian' => 'Italiano',
        'greek' => 'Griego',
        'spanish' => 'Español',
        'polish' => 'Polaco',
        'portuguese' => 'Portugués',
        'arabic' => 'Árabe',
        'macedonian' => 'Macedónio',
        'turkish' => 'Turco',
        'czech' => 'Checo',
        'swedish' => 'Sueco',
        'korean' => 'Korean',
    ],
    'button' => [
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Guardar',
        'delete' => 'Desactivar',
       'activate' => 'Activar',
        'reset' => 'Reiniciar',
        'update and back' => 'Guardar y regresar',
        'select img' => 'seleccionar imagen',
    ],
    'breadcrumb' => [
        'home' => 'Inicio',
    ],
    'title' => [
        'translatable fields' => 'Campos a traducir',
        'non translatable fields' => 'Campos no traducibles',
        'create resource' => 'Crear :name',
        'edit resource' => 'Editar :name',
    ],
    'general' => [
        'available keyboard shortcuts' => 'Atajos de teclado disponibles en esta página',
        'view website' => 'Ver sitio',
        'complete your profile' => 'Complete su perfil',
        'sign out' => 'Salir',
        'profile' => 'Mi Perfil',
    ],
    'messages' => [
        'resource created' => ':name creado éxitosamente.',
        'resource not found' => ':name no encontrado.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource deleted' => ':name eliminado éxitosamente.',
        'globalErrorBd' => "Sr. Usuario, se ha producido un error interno en el servidor, por favor intente mas tarde",
    ],
    'back' => 'Atrás',
    'back to index' => 'Regresar a :name',
    'permission denied' => 'Permiso denegado. (permiso requerido: ":permission")',

    'error token mismatch' => 'Se agotó el tiempo de espera de la sesión, envíe el formulario nuevamente.',
    'error 404' => '404',
    /* 'error 404 title' => 'Oops! Esta página no fue encontrada.', */
    /* 'error 404 description' => 'La página que estás buscando no fue encontrada.', */
    'error 500' => '500',
    'error 500 title' => 'Oops! Algo salió mal.',
    'error 500 description' => 'Un administrador fue notificado.',

    /*nueva pagina administrable */
    'error 404 nuevo' => 'ERROR - 404',
    'error 404 title' => 'UPS! AQUÍ NO HAY NADA :(',
    'error 404 description' => 'No hemos podido encontrar la página que estás buscando',
    'error 404 description1' => 'Pero aquí tienes algunas páginas que te podrían interesar:',
];
