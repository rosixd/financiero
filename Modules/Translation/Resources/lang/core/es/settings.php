<?php

return [
    'site-name' => 'Nombre del Sitio',
    'site-description' => 'Descripción del sitio',
    'template' => 'Plantilla del sitio',
    'google-analytics' => 'Código de Google Analytics',
    'locales' => 'Idiomas',

    'list resource' => 'Consultar Configuraci&oacute;n',
    'create resource' => 'Crear Configuraci&oacute;n',
    'edit resource' => 'Editar Configuraci&oacute;n',
    'destroy resource' => 'Desactivar Configuraci&oacute;n',
    'site-name-mini'   => 'Nombre minimizado',
    'analytics-script' => 'Script de Google Analytics',
];
