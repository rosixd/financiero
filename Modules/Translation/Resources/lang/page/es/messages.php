<?php

return [
    'page created' => 'P&aacute;gina creada exitosamente.',
    'page not found' => 'P&aacute;gina no encontrada.',
    'page updated' => 'P&aacute;gina actualizada exitosamente.',
    'page updated sin' => 'Sr. Usuario, no ha realizado cambios para p&aacute;gina',
    'page deleted' => 'P&aacute;gina eliminada exitosamente.',

    'template is required' => 'Sr. Usuario, el nombre de la plantilla es requerido.',
    'title is required' => 'Sr. Usuario, debe ingresar un t&iacute;tulo.',
    'slug is required' => 'Sr. Usuario, debe ingresar una URL amigable.',
    'body is required' => 'Sr. Usuario, el cuerpo es requerido.',
    'only one homepage allowed' => 'Solo es permitida una p&aacute;gina principal.',
    'resource desactivate' => 'Sr. Usuario, la p&aacute;gina se ha desactivada correctamente.',
    'resource activate' => 'Sr. Usuario, la p&aacute;gina se ha activada correctamente.',
];
