<?php

return [
    'servicios no disponible'   => 'Los Servicios no se encuentran disponibles. Favor reintentar en los próximos minutos',
    'base de datos no responde' => 'Los Servicios no se encuentran disponibles. Favor reintentar en los próximos minutos',
    'sitio caido'               => 'mensaje sitio caido',
    'no selecciona motivo del contacto' => 'Debes seleccionar el motivo de tu contacto',
    'falta dato obligatorio' => 'Debes ingresar todos los datos marcados como obligatorios',
];
