<?php

return [
    'pages' => 'P&aacute;ginas',
    'create' => 'Crear',
    'create page' => 'Crear p&aacute;gina',
    'edit page' => 'Editar p&aacute;gina',
    'name' => 'Nombre',

    'slug' => 'URL amigable',
    'title' => 'T&iacute;tulo',
    'image' => 'Imagen',

    'meta_data' => 'Metadatos',
    'meta_title' => 'T&iacute;tulo para el Meta',
    'meta_description' => 'Descripci&oacute;n para el Meta',
    'facebook_data' => 'Datos de Facebook',
    'og_title' => 'Título para Facebook',
    'og_description' => 'Descripci&oacute;n para Facebook',
    'og_type' => 'Tipo Facebook',
    'template' => 'Nombre de la plantilla de la p&aacute;gina',
    'is homepage' => '¿Es la P&aacute;gina principal?',
    'body' => 'Contenido',
    'status' => 'Status',
    'pages were updated' => 'Las P&aacute;ginas fueron actualizadas',

    'back to index' => 'Volver a la lista de p&aacute;ginas',
    'list resource' => 'Lista p&aacute;ginas',
    'create resource' => 'Crear p&aacute;ginas',
    'edit resource' => 'Editar p&aacute;ginas',
    'destroy resource' => 'Desactivar p&aacute;ginas',

    'view-page' => 'Ver  p&aacute;gina',
    'edit-page' => 'Editar p&aacute;gina',
    'validation' => [
        'attributes' => [
            'title' => 'T&iacute;tulo',
            'body' => 'Contenido',
        ],
    ],
    'facebook-types' => [
        'website' => 'P&aacute;gina web',
        'product' => 'Producto',
        'article' => 'Art&iacute;culo',
    ],
    'table' => [
        'resource id' => 'Id',
        'resource status' => 'Estatus',
        'resource titulo' => 'T&iacute;tulo',
        'resource url' => 'Url Amigable',
        'resource creado el' => 'Fecha Creaci&oacute;n',
        'resource accion' => 'Acciones',
    ],
    'validation' => [
        'image' => 'Sr. usuario la imagen es requerida.',
    ],
    'button' => [
        'save' => 'Guardar',
        'cancel' => 'Cancelar',
    ],
    'messages' => [
        'body is required' => 'Sr. usuario el Contenido de la pagina es requerido',
    ],

    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'name' => 'Paginas',
        'page pages' => 'Paginas',
    ],
];
