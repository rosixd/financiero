<?php

return [
    'file updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
    'file deleted' => 'Archivo eliminado',
    'file created' => 'Sr. Usuario, el archivo multimedia se ha guardado correctamente.',
    'updated sin' => 'Sr. Usuario, no ha realizado cambios para el archivo.',
    'resource desactivate' => 'Sr. Usuario, el archivo ha sido desactivado correctamente.',
    'resource activate' => 'Sr. Usuario, el archivo ha sido activado correctamente.',
];
