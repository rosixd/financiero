<?php

return [
    'list media' => 'Consultar multimedia',
    'create media' => 'Subir multimedia',
    'edit media' => 'Editar',
    'destroy media' => 'Desactivar',
    'title' => [
        'media' => 'Multimedia',
        'edit media' => 'Editar multimedia',
    ],
    'breadcrumb' => [
        'media' => 'Multimedia',
    ],
    'table' => [
        'filename' => 'Nombre Archivo',
        'width' => 'Ancho',
        'height' => 'Alto',
    ],
    'form' => [
        'alt_attribute' => 'Atributo Alt',
        'description' => 'Descripción',
        'keywords' => 'Palabras claves',
    ],
    'validation' => [
        'max_size' => 'Tamaño (:size) máximo alcanzado de la carpeta multimedia.',
    ],
    'file-sizes' => [
        'B' => 'Bytes',
        'KB' => 'Kb',
        'MB' => 'Mb',
        'GB' => 'Gb',
        'TB' => 'Tb',
    ],
    'choose file' => 'Elija un archivo',
    'insert' => 'Insertar este archivo',
    'file picker' => 'Selector de archivo',
    'Browse' => 'Examinar...',
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
        'name' => 'Multimedia',
        'media medias' => 'Multimedia',
    ],
];
