<?php

return [
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'name' => 'Multimedia',
    'list media' => 'Consultar multimedia',
    'create media' => 'Subir multimedia',
    'edit media' => 'Editar',
    'destroy media' => 'Eliminar',
    'title' => [
        'media' => 'Multimedia',
        'edit media' => 'Editar multimedia',
    ],
    'breadcrumb' => [
        'media' => 'Multimedia',
    ],
    'table' => [
        'filename' => 'Nombre archivo',
        'width' => 'Ancho',
        'height' => 'Alto',
    ],
    'form' => [
        'alt_attribute' => 'Atributo Alt',
        'description' => 'Descripción',
        'keywords' => 'Palabras claves',
    ],
    'validation' => [
        'max_size' => 'Tamaño (:size) máximo alcanzado de la carpeta multimedia.',
    ],
    'file-sizes' => [
        'B' => 'Bytes',
        'KB' => 'Kb',
        'MB' => 'Mb',
        'GB' => 'Gb',
        'TB' => 'Tb',
    ],
    'choose file' => 'Elija un archivo',
    'insert' => 'Insertar este archivo',
    'file picker' => 'Selector de archivo',
    'Browse' => 'Examinar...',
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'media medias' => 'Multimedia',
    ],
    'list resource' => 'Consultar Multimedia',
    'create resource' => 'Crear Multimedia',
    'edit resource' => 'Editar Multimedia',
    'destroy resource' => 'Desactivar Multimedia',

];
