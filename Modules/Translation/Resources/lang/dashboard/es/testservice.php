<?php

return [
    "messages" => [
        "errors" => [
            "no req" => "Sr. Usuario. Debe seleccionar un requerimiento",
        ],

        "no flujo" => "Sr. Usuario. El requerimiento no tiene flujo configurado",
    ],
];
