<?php

return [
    'title' => [
        'translations' => 'Administrador de Texto',
        'edit translation' => 'Editar',
    ],
    'button' => [
        'create translation' => 'Crear',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
        'updated sin' => 'Sr. Usuario, no ha realizado cambios para administrador de mensajes',
    ],
    'validation' => [
    ],
    'Clear translation cache' => 'Limpiar cach&eacute;',
    'Export' => 'Exportar',
    'Import' => 'Importar',
    'Import csv translations file' => 'Importar archivo CSV',
    'only for developers' => 'Will require git management!',
    'Translations exported' => 'Traducciones exportadas.',
    'Translations imported' => 'Traducciones importadas.',
    'select CSV file' => 'Seleccionar el archivo CSV...',
    'csv only allowed' => 'Por favor, solo suba un archivo CSV.',
    'time' => 'Hora',
    'author' => 'Autor',
    'history' => 'Historial',
    'No Revisions yet' => 'No hay revisiones',
    'event' => 'Evento',
    'created' => 'Creado',
    'edited' => 'Editado',

    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        'name' => 'Administrador de mensajes',
        'translation translations' => 'Administrador de mensajes',
    ],
    'list resource' => 'Consultar Administrador de mensajes',
    'create resource' => 'Crear CAdministrador de mensajes',
    'edit resource' => 'Editar Administrador de mensajes',
    'destroy resource' => 'Desactivar Administrador de mensajes',
    'import resource' => 'Importar Administrador de mensajes',
    'export resource' => 'Exportar Administrador de mensajes',
];
