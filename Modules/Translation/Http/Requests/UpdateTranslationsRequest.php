<?php

namespace Modules\Translation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class UpdateTranslationsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'file' => ['required', 'extensions:csv'],
            'value' => ['required'],
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'file.extensions' => trans('translation::translations.csv only allowed'),
            'value' => 'Sr. Usuario, se debe ingresar un mensaje.',
        ];
    }
}