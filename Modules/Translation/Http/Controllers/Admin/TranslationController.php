<?php

namespace Modules\Translation\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Translation\Entities\TranslationTranslation;
use Modules\Translation\Entities\Translation;
use Modules\Translation\Exporters\TranslationsExporter;
use Modules\Translation\Http\Requests\ImportTranslationsRequest;
use Modules\Translation\Importers\TranslationsImporter;
use Modules\Translation\Repositories\TranslationRepository;
use Modules\Translation\Services\TranslationsService;
use Nwidart\Modules\Facades\Module;
use DB;
use Yajra\Datatables\Datatables;

class TranslationController extends AdminBaseController
{
    /**
     * @var TranslationsService
     */
    private $translationsService;
    /**
     * @var TranslationRepository
     */
    private $translation;

    public function __construct(TranslationsService $translationsService, TranslationRepository $translation)
    {
        parent::__construct();

        $this->translationsService = $translationsService;
        $this->translation = $translation;
        $this->requireAssets();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $translations = $this->translationsService->getFileAndDatabaseMergedTranslations();

        return view('translation::admin.translations.index', compact('translations'));
    }

    public function update(TranslationTranslation $translationTranslation, Request $request)
    {
    	$translationTranslation->fill($request->all());
        
        if(!$translationTranslation->isDirty()){
            return redirect()->route('admin.translation.translation.index')
            ->withSuccess(trans('translation::translations.messages.updated sin'));
        }
        $this->translation->updateTranslationToValue($translationTranslation, $request->get('oldValue'));

        return redirect()->route('admin.translation.translation.index')->withSuccess('Translation saved.');
    }

    public function export(TranslationsExporter $exporter)
    {
        return response()->csv($exporter->export(), $exporter->getFileName());
    }

    public function import(ImportTranslationsRequest $request, TranslationsImporter $importer)
    {
        $importer->import($request->file('file'));

        return redirect()->route('admin.translation.translation.index')->withSuccess(trans('translation::translations.Translations imported'));
    }

    private function requireAssets()
    {
        $this->assetManager->addAsset('bootstrap-editable.css', Module::asset('translation:vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'));
        $this->assetManager->addAsset('bootstrap-editable.js', Module::asset('translation:vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js'));
        $this->assetPipeline->requireJs('bootstrap-editable.js');
        $this->assetPipeline->requireCss('bootstrap-editable.css');
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernández
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
        //$translations = $this->translationsService->getFileAndDatabaseMergedTranslations();
        $traducciones = Translation::withTrashed()
            ->select('translation__translations.id', 'translation__translations.key', 'translation__translation_translations.value')
            ->join('translation__translation_translations', 'translation__translation_translations.translation_id', '=', 'translation__translations.id')
            ->where('translation__translations.key', 'NOT LIKE', 'core::%')
			->where('translation__translations.key', 'NOT LIKE', 'dashboard::%')
			->where('translation__translations.key', 'NOT LIKE', 'media::%')
			->where('translation__translations.key', 'NOT LIKE', 'menu::%')
			->where('translation__translations.key', 'NOT LIKE', 'setting::%')
			->where('translation__translations.key', 'NOT LIKE', 'tag::%')
			->where('translation__translations.key', 'NOT LIKE', 'user::auth%')
			->where('translation__translations.key', 'NOT LIKE', 'user::button%')
			->where('translation__translations.key', 'NOT LIKE', 'user::messages%')
			->where('translation__translations.key', 'NOT LIKE', 'user::roles%')
			->where('translation__translations.key', 'NOT LIKE', 'user::users%')
			->where('translation__translations.key', 'NOT LIKE', 'workshop::%')
			->where('translation__translations.key', 'NOT LIKE', 'translation::%')
			->where('translation__translations.key', 'NOT LIKE', 'page::%')
			->where('translation__translations.key', 'NOT LIKE', 'page::messages%')
			;
	
	$trans = array();
        $locales = array();

        /*foreach ($translations->all() as $key => $translationGroup){ 
		    foreach (config('laravellocalization.supportedLocales') as $locale => $language){  
	        	array_push($locales, array("locale"=>$locale));
        	} 
	        array_push($trans, array("llave"=>$key, "msn" => $translationGroup["es"], "locale"=> $locale, "traducciones"=> $traducciones));
        }*/


        return Datatables::of($traducciones)
        ->make(true);   
    }
}
