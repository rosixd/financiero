<?php

use Illuminate\Routing\Router;
use Modules\Translation\Entities\TranslationTranslation;

/** @var Router $router */
$router->group(['prefix' =>'/translation'], function (Router $router) {
    $router->bind('translations', function ($id) {
        $repo = app('Modules\Translation\Repositories\TranslationRepository')->find($id);
        return (is_null($repo))? TranslationTranslation::withTrashed()->find($id):$repo;
        //return \Modules\Translation\Entities\TranslationTranslation::find($id);
    });
    $router->get('translations', [
        'uses' => 'TranslationController@index',
        'middleware' => 'can:translation.translations.index',
        'as' => 'admin.translation.translation.index',
        'script' => 'AppTranslation',
        'view' => 'index',
        'modulo' => 'translation',
        'submodulo' => 'translation',
        'datatable' => 'admin.translation.datatable',
        'log' => 'admin.user.user.logdatatable',
        'editable' => 'api.translation.translations.update',
        'clearcache' => 'api.translation.translations.clearCache',


    ]);
    $router->get('translations/update/{translations}', [
        'uses' => 'TranslationController@update',
        'middleware' => 'can:translation.translations.edit',
        'as' => 'admin.translation.translation.update',
    ]);
    $router->get('translations/export', [
        'uses' => 'TranslationController@export',
        'middleware' => 'can:translation.translations.export',
        'as' => 'admin.translation.translation.export',
    ]);
    $router->post('translations/import', [
        'uses' => 'TranslationController@import',
        'middleware' => 'can:translation.translations.import',
        'as' => 'admin.translation.translation.import',
    ]);
    // append
    $router->get('translations/datatable', [
        'middleware' => 'can:translation.translations.index',
        'as' => "admin.translation.datatable",
        "uses" => "TranslationController@datatable",
    ]);
});
