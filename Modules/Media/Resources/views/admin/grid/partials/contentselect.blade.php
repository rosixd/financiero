<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ trans('media::media.file picker') }}</title>

    <link href="{!! Module::asset('media:css/dropzone.css') !!}" rel="stylesheet" type="text/css" />
    <style>
    body {
        background: #ecf0f5;
        margin-top: 20px;
    }
    .dropzone {
        border: 1px dashed #CCC;
        min-height: 227px;
        margin-bottom: 20px;
        display: none;
    }
</style>
<script>
    AuthorizationHeaderValue = 'Bearer {{ $currentUser->getFirstApiKey() }}';
</script>
@include('partials.asgard-globals')
</head>
<body>
    <div class="">
        <div class="row">
            <form method="POST" class="dropzone">
                {!! Form::token() !!}
            </form>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ trans('media::media.choose file') }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool jsShowUploadForm" data-toggle="tooltip" title="" data-original-title="Subir nuevo">
                            <i class="fa fa-cloud-upload"></i>
                            Subir nuevo
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover jsFileList data-table">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>{{ trans('core::core.table.thumbnail') }}</th>
                                <th>{{ trans('media::media.table.filename') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <?php $locale = App::getLocale(); ?>

    <script type="text/javascript">

        $(function () {
            $('.data-table').dataTable({
                "processing": true,
                "serverSide": true,
                "paginate": true,
                "ajax": "<?= route("media.grid.datatableselect") ?>",
                "columnDefs":[
                {
                    "targets": 0,
                    "render": function ( data, type, row ) {
                        return row.id;
                    },
                },
                {
                    "targets": 1,
                    "render": function ( data, type, row ) {

                        if(row.is_image){
                            return '<img src="'+row.thumbnail+'" alt=""/>';
                        }else{
                            return '<i class="fa '+row.filehelper+'" style="font-size: 20px;"></i>';
                        }                                    

                    },
                },
                {
                    "targets": 2,
                    "render": function ( data, type, row ) {
                        return row.filename;
                    },
                },
                {
                    "targets": 3,
                    "render": function ( data, type, row ) {
                        return ' <button type="button" onclick="AppGeneral.selectImage(this)" class="btn btn-primary btn-md" data-name="'+row.filename+'" data-id="'+row.id+'" data-button ="{{$idbutton}}" >'+row.button_txt+' </button>';
                    },
                }
                ],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });

        </script>


        <script src="{!! Module::asset('media:js/dropzone.js') !!}"></script>
        <?php $config = config('asgard.media.config'); ?>
        <script>
        var maxFilesize = '<?php echo $config['max-file-size'] ?>',
        acceptedFiles = '<?php echo $config['allowed-types'] ?>';
        </script>
        <script src="{!! Module::asset('media:js/init-dropzone.js') !!}"></script>
        <script>
        $( document ).ready(function() {
            $('.jsShowUploadForm').on('click',function (event) {
                event.preventDefault();
                $('.dropzone').fadeToggle();
            });
        });
        </script>


