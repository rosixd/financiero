<?php

use Illuminate\Routing\Router;
use Modules\Media\Entities\File;

/** @var Router $router */
$router->bind('media', function ($id) {
    $repo = app(\Modules\Media\Repositories\FileRepository::class)->find($id);
    return (is_null($repo))? File::withTrashed()->find($id):$repo; 
});

$router->group(['prefix' => '/media'], function (Router $router) {
    $router->get('media', [
        'as' => 'admin.media.media.index',
        'middleware' => 'can:media.medias.index',
        'uses' => 'MediaController@index',
        'script' => 'AppMedia',
        'view' => 'index',
        'modulo' => 'media',
        'submodulo' => 'media',
        'datatable' => 'admin.media.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
	
    $router->get('media/getid/{id}', function( $id ){
    	$media = \Modules\Media\Entities\File::find( $id );
	/*dd( $media->toArray() );*/
    });
    
    $router->get('media/create', [
        'as' => 'admin.media.media.create',
        'middleware' => 'can:media.medias.create',
        'uses' => 'MediaController@create',
        'script' => 'AppMedia',
        'view' => 'create',
        'modulo' => 'media',
        'submodulo' => 'media',
    ]);
    $router->post('media', [
        'as' => 'admin.media.media.store',
        'middleware' => 'can:media.medias.create',
        'uses' => 'MediaController@store',
    ]);
    $router->get('media/{media}/edit', [
        'as' => 'admin.media.media.edit',
        'middleware' => 'can:media.medias.edit',
        'uses' => 'MediaController@edit',
        'script' => 'AppMedia',
        'view' => 'edit',
        'modulo' => 'media',
        'submodulo' => 'media',
        'datatable' => 'admin.media.datatable',
    ]);
    $router->put('media/{media}', [
        'as' => 'admin.media.media.update',
        'middleware' => 'can:media.medias.edit',
        'uses' => 'MediaController@update',
    ]);
    $router->get('media/destroy/{media}', [
        'as' => 'admin.media.media.destroy',
        'middleware' => 'can:media.medias.destroy',
        'uses' => 'MediaController@destroy',
    ]);

    $router->get('media/datatable', [
        'as' => "admin.media.datatable",
		'middleware' => 'can:media.medias.index',
        "uses" => "MediaController@datatable",
    ]);

    $router->get('media-grid/index', [
        'uses' => 'MediaGridController@index',
        'as' => 'media.grid.select',
        'middleware' => 'can:media.medias.index',
    ]);
    $router->get('media-grid/ckIndex', [
        'uses' => 'MediaGridController@ckIndex',
        'as' => 'media.grid.ckeditor',
        'middleware' => 'can:media.medias.index',
    ]);
    $router->get('media-grid/select', [
        'uses' => 'MediaGridController@select',
        'as' => 'media.grid.select',
        'middleware' => 'can:media.medias.index',
    ]);
    $router->get('media-grid/datatableselect', [
        'uses' => 'MediaGridController@datatableselect',
        'as' => 'media.grid.datatableselect',
        'middleware' => 'can:media.medias.index',
    ]);
});
