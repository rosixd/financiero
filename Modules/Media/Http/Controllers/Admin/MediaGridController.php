<?php

namespace Modules\Media\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Media\Image\ThumbnailManager;
use Modules\Media\Repositories\FileRepository;
use Illuminate\Http\Request;
use DB;
use Imagy;
use FileHelper;
use Yajra\Datatables\Datatables;

class MediaGridController extends AdminBaseController
{
    /**
     * @var FileRepository
     */
    private $file;
    /**
     * @var ThumbnailManager
     */
    private $thumbnailsManager;

    public function __construct(FileRepository $file, ThumbnailManager $thumbnailsManager)
    {
        parent::__construct();

        $this->file = $file;
        $this->thumbnailsManager = $thumbnailsManager;
    }

    /**
     * A grid view for the upload button
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $files = $this->file->all();
        $thumbnails = $this->thumbnailsManager->all();

        return view('media::admin.grid.general', compact('files', 'thumbnails'));
    }

    /**
     * A grid view of uploaded files used for the wysiwyg editor
     * @return \Illuminate\View\View
     */
    public function ckIndex()
    {
        $files = $this->file->all();
        $thumbnails = $this->thumbnailsManager->all();

        return view('media::admin.grid.ckeditor', compact('files', 'thumbnails'));
    }

    /**
     * A grid view for the upload button
     * @return \Illuminate\View\View
     */
    public function select(Request $request)
    {
        $files = $this->file->all();
        $thumbnails = $this->thumbnailsManager->all();
        $idbutton = $request->idbutton;
        $idimg = $request->idimg;
        return view('media::admin.grid.select', compact('files', 'thumbnails', 'idbutton','idimg'));
    }

    public function datatableselect(){

        $files = $this->file->all();

        return Datatables::of($files)
        ->addColumn('is_image', function ($files) {
            return $files->isImage()? true:false;
        })
        ->addColumn('button_txt', function ($files) {
            return trans('media::media.insert');
        })
        ->addColumn('thumbnail', function ($files) {
            return  Imagy::getThumbnail($files->path, "smallThumb");
        })
        ->addColumn('filehelper', function ($files) {
            return  FileHelper::getFaIcon($files->media_type);
        })
        ->make(true); 
    }
}
