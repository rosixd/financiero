<?php

namespace Modules\Media\Http\Controllers\Admin;

use Illuminate\Contracts\Config\Repository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Media\Entities\File;
use Modules\Media\Http\Requests\UpdateMediaRequest;
use Modules\Media\Image\Imagy;
use Modules\Media\Image\ThumbnailManager;
use Modules\Media\Repositories\FileRepository;
use DB;
use Yajra\Datatables\Datatables;
use Modules\Media\Helpers\FileHelper;

class MediaController extends AdminBaseController
{
    /**
     * @var FileRepository
     */
    private $file;
    /**
     * @var Repository
     */
    private $config;
    /**
     * @var Imagy
     */
    private $imagy;
    /**
     * @var ThumbnailManager
     */
    private $thumbnailsManager;

    public function __construct(FileRepository $file, Repository $config, Imagy $imagy, ThumbnailManager $thumbnailsManager)
    {
        parent::__construct();
        $this->file = $file;
        $this->config = $config;
        $this->imagy = $imagy;
        $this->thumbnailsManager = $thumbnailsManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $files = $this->file->all();

        $config = $this->config->get('asgard.media.config');

        return view('media::admin.index', compact('files', 'config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // return view('media.create');
        return redirect()->route('media::admin.index', ['files' => $files, 'config' => $config ])
            ->withSuccess(trans('media::messages.file create'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  File     $file
     * @return Response
     */
    public function edit(File $file)
    {
        $thumbnails = $this->thumbnailsManager->all();

        return view('media::admin.edit', compact('file', 'thumbnails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  File               $file
     * @param  UpdateMediaRequest $request
     * @return Response
     */
    public function update(File $file, UpdateMediaRequest $request)
    {
        //dd($request->all());
        try {
            $this->file->update($file, $request->all());
        } catch (FatalErrorException $e) {
            dd($e);
        } catch (\Exception $e) {
            dd($e);
        }

        return redirect()->route('admin.media.media.index')
            ->withSuccess(trans('media::messages.file updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  File     $file
     * @internal param int $id
     * @return Response
     */
    public function destroy(File $file)
    {
        $deleted_at = $file->deleted_at;

        //$this->file->destroy($file);
        //$this->imagy->deleteAllFor($file);

        $success  = is_null($deleted_at)? $file->delete():$file->restore();
        
        $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('media::messages.resource desactivate'):trans('media::messages.resource activate'),
                'status' => is_null($deleted_at)? true:false);

        return response()->json($json); 
        //return redirect()->route('admin.media.media.index')->withSuccess(trans('media::messages.file deleted'));
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response

    */
    public function datatable()
    {	 
        
        $files = File::withTrashed();

        return Datatables::of($files)
        ->addColumn('ruta_edit', function ($files) {
            return route("admin.media.media.edit",[$files->id]);
        })        
        ->addColumn('ruta_destroy', function($files){
            return route("admin.media.media.destroy",[$files->id]);
        })
        ->addColumn('inactivo', function ($files) {
            return is_null($files->deleted_at)? false:true;
        })
        ->addColumn('thumbnails', function($file){
            return ($file->isImage())? $this->imagy->getThumbnail($file->path, 'smallThumb'):FileHelper::getFaIcon($file->media_type);
        })
        ->addColumn('is_image', function($file){
            return ($file->isImage())? true:false;
        })
        ->make(true);
    }
}
