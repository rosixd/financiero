<?php

namespace Modules\Media\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FileTranslation extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    
    public $timestamps = false;
    protected $fillable = ['description', 'alt_attribute', 'file_trans_audit'];
    protected $table = 'media__file_translations';

    protected $appends = ['key'];

    public function getKeyAttribute()
    {
        return "FileTranslation";
    }
}
