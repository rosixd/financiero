$( document ).ready(function() {
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(".dropzone", {
        url: Asgard.dropzonePostUrl,
        autoProcessQueue: true,
        maxFilesize: maxFilesize,
        acceptedFiles : acceptedFiles
    });
    myDropzone.on("queuecomplete", function(file, http) {
        window.setTimeout(function(){
            location.reload();
        }, 5000);
    });
    myDropzone.on("sending", function(file, xhr, fromData) {
        xhr.setRequestHeader("Authorization", AuthorizationHeaderValue);
        if ($('.alert-danger').length > 0) {
            $('.alert-danger').remove();
        }
    });
    myDropzone.on("error", function(file, errorMessage) {
        /* var html = '<div class="alert alert-danger" role="alert">' + errorMessage + '</div>';
        $('.col-md-12').first().prepend(html); */
        AppGeneral.notificaciones('error', 'Sr. Usuario, debe ingresar un archivo valido.');
        setTimeout(function() {
            myDropzone.removeFile(file);
        }, 5000);
    });
    myDropzone.on("success", function(file, response) {
        var flag = $('#flagButtonSelect').val();

        AppGeneral.notificaciones('success', 'Sr. Usuario, el archivo multimedia se ha guardado correctamente.');

        setTimeout(function() {
            myDropzone.removeFile(file);
        }, 5000);

        if( $(".data-table").length ){
            $(".data-table").dataTable().ajax.reload();
        }
    /*$(".data-table").DataTable().ajax.reload();*/
        if($("#pathFoto-"+flag).length){
            $("#pathFoto-"+flag).val(response.path_bd);
        }
    });
});
