<label></label>
<input type="hidden" id="flagLoadingImg">

<div class="form-group">
	<div class="input-group">
		<div class="input-group-btn">
			<button type="button" class="btn btn-danger " id="buttonLoadImg-RDS45_foto" data-id="RDS45_foto" onclick="AppGeneral.modalFileInput(this)" >{{ trans('convenio::convenios.button.selectfile') }} <span id="loadingSpin-RDS45_foto" class=""></span></button>
		</div>
		<input type="text" class="form-control" name="RDS45_foto" value="{{ isset($convenio)? $convenio->RDS45_foto:'' }}" id="pathFoto-RDS45_foto" readonly="">  
	</div>
</div>

<div class="form-group dropzone-img" id="seleccionaImagen-RDS45_foto" hidden="">

</div>