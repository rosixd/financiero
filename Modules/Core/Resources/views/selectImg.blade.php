<label>{{ $label }}</label>
<input type="hidden" id="flagLoadingImg">

<div class="form-group">
	
	<div class="input-group">
		<div class="input-group-btn">
			<button type="button" class="btn btn-danger" id="buttonLoadImg-{{$name}}" data-url="{{ route("media.grid.select") }}" data-id="{{$name}}" data-idimg="{{ isset($id)? $id:'' }}" onclick="AppGeneral.modalFileInput(this)" >
				{{ trans('core::core.button.select img') }} 
				<span id="loadingSpin-{{$name}}" class=""></span>
			</button>
		</div>
		<input type="text" class="form-control" name="" value="{{ isset($img)? $img:'' }}" id="pathFoto-{{$name}}" readonly=""> 
	</div>

	<span class="text-danger">
		{{ $errors->has($name) ? $required : '' }}
	</span> 

</div>

<input type="hidden" class="form-control" name="{{$name}}" value="{{ isset($id)? $id:'' }}" id="pathFotoId-{{$name}}" readonly="">

<div class="form-group dropzone-img" id="seleccionaImagen-{{$name}}" hidden="">

</div>