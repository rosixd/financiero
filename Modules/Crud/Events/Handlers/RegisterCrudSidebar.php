<?php

namespace Modules\Crud\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterCrudSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        // $menu->group(trans('core::sidebar.content'), function (Group $group) {
        //     $group->item(trans('crud::cruds.title.cruds'), function (Item $item) {
        //         $item->icon('fa fa-pie-chart');
        //         $item->weight(10);
        //         $item->item(trans('crud::campanas.title.campanas'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.crud.campana.create');
        //             $item->route('admin.crud.campana.index');
        //         });
        //         $item->item(trans('crud::servicios.title.servicios'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.crud.servicio.create');
        //             $item->route('admin.crud.servicio.index');
        //         });
        //     });
        // });

        return $menu;
    }
}
