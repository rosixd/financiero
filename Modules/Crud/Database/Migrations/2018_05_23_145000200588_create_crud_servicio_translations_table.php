<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrudServicioTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud__servicio_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('servicio_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['servicio_id', 'locale']);
            $table->foreign('servicio_id')->references('id')->on('crud__servicios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crud__servicio_translations', function (Blueprint $table) {
            $table->dropForeign(['servicio_id']);
        });
        Schema::dropIfExists('crud__servicio_translations');
    }
}
