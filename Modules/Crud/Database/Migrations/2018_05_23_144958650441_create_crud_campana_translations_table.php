<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrudCampanaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud__campana_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('campana_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['campana_id', 'locale']);
            $table->foreign('campana_id')->references('id')->on('crud__campanas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crud__campana_translations', function (Blueprint $table) {
            $table->dropForeign(['campana_id']);
        });
        Schema::dropIfExists('crud__campana_translations');
    }
}
