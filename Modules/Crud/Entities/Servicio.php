<?php

namespace Modules\Crud\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Servicio extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC15_Servicio';
    protected $primaryKey = 'ABC15_id';
    protected $fillable = [
      "ABC15_id",
      "ABC15_nombre",
      "ABC15_imagen",   
      "ABC15_titulo",
      "ABC15_descripcion_compacta",
      "ABC15_icono",  
      "ABC15_condiciones",
      "ABC15_descripcion",       
    ];
    
    protected $appends = ['key', 'articulo'];
    
    public function getKeyAttribute()
    {
        return "Servicio";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }
    public function imagen()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC15_imagen');
    }

    public function icono()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC15_icono');
    }
}
