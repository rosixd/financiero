<?php

namespace Modules\Crud\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;
class Campana extends Model implements Auditable

{
	use SoftDeletes, \OwenIt\Auditing\Auditable;

	protected $table = 'ABC06_Campanas';
	protected $primaryKey = 'ABC06_id';
	protected $fillable = [
		"ABC06_id",
		"ABC06_nombre",
		"ABC06_slug",
		"ABC06_descripcion_compacta",
		"ABC06_contenido_compacto",
		"ABC06_descripcion",
		"ABC06_fecha_inicio",
		"ABC06_fecha_fin",
		"ABC06_imagen",
		"ABC06_imagen_compacta",
		"ABC06_pie_campana",
		"ABC06_default",
	];
	protected $appends = ['name', 'id', 'key', 'articulo'];
	
	public function getKeyAttribute()
	{
	        return "Campana";
	}
	public function getArticuloAttribute()
	{
		return "la";
	}
	public function getABC06FechaInicioAttribute($value){

		return Carbon::parse($this->attributes["ABC06_fecha_inicio"])->format('d-m-Y');
	}

	public function getABC06FechaFinAttribute($value){

		return Carbon::parse($this->attributes["ABC06_fecha_fin"])->format('d-m-Y');
	}


	public function setABC06FechaInicioAttribute($value)
	{
		$formato = 'd-m-Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		$this->attributes['ABC06_fecha_inicio'] = Carbon::createFromFormat($formato, $value);
	}

	public function setABC06FechaFinAttribute($value)
	{
		$formato = 'd-m-Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}

		$this->attributes['ABC06_fecha_fin'] = Carbon::createFromFormat($formato, $value);
	}

	public function getNameAttribute()
	{
		return $this->ABC06_nombre;
	}
	public function getIdAttribute()
    {
        return $this->ABC06_id;
	}

    public function imagen()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC06_imagen');
    }

    public function imagencompacta()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC06_imagen_compacta');
    }
}

