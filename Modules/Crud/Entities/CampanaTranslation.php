<?php

namespace Modules\Crud\Entities;

use Illuminate\Database\Eloquent\Model;

class CampanaTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'crud__campana_translations';
}
