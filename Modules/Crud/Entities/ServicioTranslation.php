<?php

namespace Modules\Crud\Entities;

use Illuminate\Database\Eloquent\Model;

class ServicioTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'crud__servicio_translations';
}
