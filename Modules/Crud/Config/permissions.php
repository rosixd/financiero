<?php

return [
    'crud.campanas' => [
        'index' => 'crud::campanas.list resource',
        'create' => 'crud::campanas.create resource',
        'edit' => 'crud::campanas.edit resource',
        'destroy' => 'crud::campanas.destroy resource',
    ],
    'crud.servicios' => [
        'index' => 'crud::servicios.list resource',
        'create' => 'crud::servicios.create resource',
        'edit' => 'crud::servicios.edit resource',
        'destroy' => 'crud::servicios.destroy resource',
    ],
// append


];
