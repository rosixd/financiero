<div class="box-body">
        <div class="form-group">
            <label>{{ trans('crud::campanas.form.name') }}</label>
            <input type="text" class="form-control soloLetras" name="ABC06_nombre" id="ABC06_nombre">
            <span class="text-danger">
                {{ $errors->has('ABC06_nombre') ? trans('crud::campanas.validation.name') : '' }}
            </span>
        </div>
    
        @include('core::selectImg',
        ['name'=>'ABC06_imagen', 
         'label' => trans('crud::campanas.form.image'),
         'required' => trans('crud::campanas.validation.image')
        ])
    
        @include('core::selectImg',
            ['name'=>'ABC06_imagen_compacta', 
            'label' => trans('crud::campanas.form.compact_image'),
            'required' => trans('crud::campanas.validation.compact_image')
        ])
    
        <div class="form-group ">
            <label>{{ trans('crud::campanas.form.short_description') }}</label>
            <input type="text" class="form-control" name="ABC06_descripcion_compacta" id="ABC06_descripcion_compacta">
            <span class="text-danger" >
                {{ $errors->has('ABC06_descripcion_compacta') ? trans('crud::campanas.validation.short_description') : '' }}
            </span>
        </div>
        <div class="form-group">
            <label>{{ trans('crud::campanas.form.compact_content') }}</label>
            <input type="text" class="form-control" name="ABC06_contenido_compacto" id="ABC06_contenido_compacto">
            <span class="text-danger">
                {{ $errors->has('ABC06_contenido_compacto') ? trans('crud::campanas.validation.compact_content') : '' }}
            </span>
        </div>
        <div class="form-group">
            <label>{{ trans('crud::campanas.form.long_description') }}</label>
            <textarea class="form-control ckeditor" name="ABC06_descripcion" id="ABC06_descripcion"></textarea>
            <span class="text-danger">
                {{ $errors->has('ABC06_descripcion') ? trans('crud::campanas.validation.long_description') : '' }}
            </span>
        </div>
        <div class="form-group">
            <label>{{ trans('crud::campanas.form.footer') }}</label>
            <input type="text" class="form-control" name="ABC06_pie_campana" id="ABC06_pie_campana">
            <span class="text-danger">
                {{ $errors->has('ABC06_pie_campana') ?trans('crud::campanas.validation.footer') : '' }}
            </span>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                <label>{{ trans('crud::campanas.form.start_date') }}</label>
                    <div class='input-group date fechas'>
                        <input type="text" class="form-control fechas" name="ABC06_fecha_inicio" id="ABC06_fecha_inicio">
                        <span class="input-group-addon btn" for="ABC06_fecha_inicio">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <span class="text-danger">
                            {{ $errors->has('ABC06_fecha_inicio') ?trans('crud::campanas.validation.start_date') : '' }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>{{ trans('crud::campanas.form.end_date') }}</label>
                    <div class='input-group date fechas'>
                        <input type="text" class="form-control fechas" name="ABC06_fecha_fin" id="ABC06_fecha_fin">
                        <span class="input-group-addon btn" for="ABC06_fecha_fin">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <span class="text-danger">
                            {{ $errors->has('ABC06_fecha_fin') ?trans('crud::campanas.validation.end_date') : '' }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    