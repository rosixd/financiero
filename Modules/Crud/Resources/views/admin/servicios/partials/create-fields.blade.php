<div class="box-body">
        <div class="form-group">
            <label>{{ trans('crud::servicios.form.name') }}</label>
            <input type="text" class="form-control soloLetras" name="ABC15_nombre" id="ABC15_nombre">
            <span class="text-danger">
                {{ $errors->has('ABC15_nombre') ? trans('crud::servicios.validation.name') : '' }}
            </span>
        </div>
    
        @include('core::selectImg',
        ['name'=>'ABC15_imagen', 
            'label' => trans('crud::servicios.form.image'),
            'required' => trans('crud::servicios.validation.image')
        ])
    
        <div class="form-group ">
            <label>{{ trans('crud::servicios.form.tittle') }}</label>
            <input type="text" class="form-control" name="ABC15_titulo" id="ABC15_titulo">
            <span class="text-danger" >
                {{ $errors->has('ABC15_titulo') ? trans('crud::servicios.validation.tittle') : '' }}
            </span>
        </div>
        <div class="form-group">
            <label>{{ trans('crud::servicios.form.short_description') }}</label>
            <input type="text" class="form-control" name="ABC15_descripcion_compacta" id="ABC15_descripcion_compacta">
            <span class="text-danger">
                {{ $errors->has('ABC15_descripcion_compacta') ? trans('crud::servicios.validation.short_description') : '' }}
            </span>
        </div>
    
        @include('core::selectImg',
        ['name'=>'ABC15_icono', 
            'label' => trans('crud::servicios.form.icon'),
            'required' => trans('crud::servicios.validation.icon')
        ])
    
        <div class="form-group">
            <label>{{ trans('crud::servicios.form.condintions') }}</label>
            <textarea class="form-control ckeditor" name="ABC15_condiciones" id="ABC15_condiciones"></textarea>
            <span class="text-danger">
                {{ $errors->has('ABC15_condiciones') ? trans('crud::servicios.validation.condintions') : '' }}
            </span>
        </div>
        <div class="form-group">
            <label>{{ trans('crud::servicios.form.long_description') }}</label>
            <textarea class="form-control ckeditor" name="ABC15_descripcion" id="ABC15_descripcion"></textarea>
            <span class="text-danger">
                {{ $errors->has('ABC15_descripcion') ? trans('crud::servicios.validation.long_description') : '' }}
            </span>
        </div>
    </div>
    