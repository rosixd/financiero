@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('crud::servicios.title.servicios') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('crud::servicios.title.servicios') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.crud.servicio.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('crud::servicios.button.create servicio') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th>{{ trans('crud::servicios.table.name') }}</th>
                                <th>{{ trans('crud::servicios.table.short_description') }}</th>
                                <th>{{ trans('crud::servicios.table.tittle') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('crud::servicios.table.name') }}</th>
                                <th>{{ trans('crud::servicios.table.short_description') }}</th>
                                <th>{{ trans('crud::servicios.table.tittle') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
   @include('core::modalLog')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('crud::servicios.title.create servicio') }}</dd>
    </dl>
@stop