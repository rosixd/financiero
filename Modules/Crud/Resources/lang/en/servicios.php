<?php

return [
    'list resource' => 'Consultar Servicio',
    'create resource' => 'Crear Servicio',
    'edit resource' => 'Editar Servicio',
    'destroy resource' => 'Eliminar Servicio',
    'title' => [
        'servicios' => 'Servicios',
        'create servicio' => 'Crear Servicio',
        'edit servicio' => 'Editar Servicio',
    ],
    'table' => [
        'name' =>'Nombre',
        'short_description' =>'Descripci&oacute;n corta',
        'created at' =>'Fecha de Creaci&oacute;n',
        'tittle' =>'T&iacute;tulo',
    ],

    'form' => [
        'name' =>'Nombre',
        'image' =>'Imagen',
        'tittle' =>'T&iacute;tulo',
        'icon' =>'&Iacute;cono',
        'long_description' =>'Descripci&oacute;n',
        'condintions' =>'Condiciones',
        'short_description' =>'Descripci&oacute;n corta', 
    ],
    'breadcrumb' => [
        'home' => 'servicio',
    ],
    'button' => [
    	'create servicio' => 'Crear Servicio',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'messages' => [
        'resource created' => ':name creado &eacute;xitosamente.',
        'resource not found' => ':name no encontrado.',
        'resource updated' => ':name actualizado &eacute;xitosamente.',
	    'resource desactivate' => 'Sr. Usuario el servicio se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, el servicio se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, el servicio no se puede desactivar ya que contiene datos asociados.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'image' => 'Sr. Usuario, debe ingresar la imagen.',
        'tittle' => 'Sr. Usuario, debe ingresar el t&iacute;tulo.',
        'icon' => 'Sr. Usuario, debe ingresar el &iacute;cono.',
        'short_description' => 'Sr. Usuario, debe ingresar una descripci&oacute;n corta.',
    ],
];
