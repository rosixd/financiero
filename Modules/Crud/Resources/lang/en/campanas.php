<?php

return [
    'list resource' => 'Consultar Campa&ntilde;a',
    'create resource' => 'Crear Campa&ntilde;a',
    'edit resource' => 'Editar Campa&ntilde;a',
    'destroy resource' => 'Eliminar Campa&ntilde;a',
    'title' => [
        'campanas' => 'Campa&ntilde;as',
        'create campana' => 'Crear Campa&ntilde;a',
        'edit campana' => 'Editar Campa&ntilde;a',
    ],
    'table' => [
        'name' =>'Nombre',
        'short_description' =>'Descripci&oacute;n corta',
        'start_date' =>'Fecha Inicio',
        'end_date' =>'Fecha Fin',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],

    'form' => [
        'name' =>'Nombre',
        'image' =>'Imagen',
        'compact_image' =>'Imagen compactada',
        'short_description' =>'Descripci&oacute;n corta',
        'long_description' =>'Descripci&oacute;n',
        'compact_content' =>'Contenido compacto',
        'footer' =>'Pie de campa&ntilde;a',
        'start_date' =>'Fecha inicio',
        'end_date' =>'Fecha fin',
        'default' =>'Por defecto',
        
    ],
    'breadcrumb' => [
        'home' => 'campa&ntilde;a',
    ],
    'button' => [
    	'create campana' => 'Crear Campa&ntilde;a',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'messages' => [
        'resource created' => ':name creada &eacute;xitosamente.',
        'resource not found' => ':name no encontrada.',
        'resource updated' => ':name actualizada &eacute;xitosamente.',
	'resource desactivate' => 'Sr. Usuario la campa&ntilde;a se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, la campa&ntilde;a se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, la campa&ntilde;a asignada est&aacute; desactivada.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'image' => 'Sr. Usuario, debe ingresar la imagen.',
        'compact_image' => 'Sr. Usuario, debe ingresar la imagen compactada.',
        'short_description' => 'Sr. Usuario, debe ingresar una descripci&oacute;n corta.',
        'start_date' => 'Sr. Usuario, debe ingresar una fecha de inicio para la vigencia de la campa&ntilde;a.',
        'end_date' => 'Sr. Usuario, debe ingresar una fecha de fin para el t&eacute;rmino de vigencia de la campa&ntilde;a.',
    ],
];
