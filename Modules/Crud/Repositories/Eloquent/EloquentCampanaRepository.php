<?php

namespace Modules\Crud\Repositories\Eloquent;

use Modules\Crud\Repositories\CampanaRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCampanaRepository extends EloquentBaseRepository implements CampanaRepository
{
}
