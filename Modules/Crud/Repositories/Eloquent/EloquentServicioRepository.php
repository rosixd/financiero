<?php

namespace Modules\Crud\Repositories\Eloquent;

use Modules\Crud\Repositories\ServicioRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentServicioRepository extends EloquentBaseRepository implements ServicioRepository
{
}
