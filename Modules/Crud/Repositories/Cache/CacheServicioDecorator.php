<?php

namespace Modules\Crud\Repositories\Cache;

use Modules\Crud\Repositories\ServicioRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheServicioDecorator extends BaseCacheDecorator implements ServicioRepository
{
    public function __construct(ServicioRepository $servicio)
    {
        parent::__construct();
        $this->entityName = 'crud.servicios';
        $this->repository = $servicio;
    }
}
