<?php

namespace Modules\Crud\Repositories\Cache;

use Modules\Crud\Repositories\CampanaRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCampanaDecorator extends BaseCacheDecorator implements CampanaRepository
{
    public function __construct(CampanaRepository $campana)
    {
        parent::__construct();
        $this->entityName = 'crud.campanas';
        $this->repository = $campana;
    }
}
