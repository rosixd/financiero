<?php

namespace Modules\Crud\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateServicioRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC15_nombre' => 'required',
            'ABC15_imagen' => 'required',
            'ABC15_titulo' => 'required',
            'ABC15_descripcion_compacta' => 'required',
            'ABC15_icono' => 'required',
            'ABC15_descripcion' => 'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
