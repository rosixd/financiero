<?php

namespace Modules\Crud\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCampanaRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC06_nombre' => 'required',
            'ABC06_imagen' => 'required',
            'ABC06_imagen_compacta' => 'required',
            'ABC06_descripcion_compacta' => 'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
