<?php

namespace Modules\Crud\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Crud\Entities\Campana;
use Modules\Crud\Http\Requests\CreateCampanaRequest;
use Modules\Crud\Http\Requests\UpdateCampanaRequest;
use Modules\Crud\Repositories\CampanaRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class CampanaController extends AdminBaseController
{
    /**
     * @var CampanaRepository
     */
    private $campana;

    public function __construct(CampanaRepository $campana)
    {
        parent::__construct();

        $this->campana = $campana;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$campanas = $this->campana->all();

        return view('crud::admin.campanas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('crud::admin.campanas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCampanaRequest $request
     * @return Response
     */
    public function store(CreateCampanaRequest $request)
    {
        $data = $request->all();
        $data['ABC06_slug']= str_slug($data['ABC06_nombre'], '-');

        $Campana = $this->campana->create( $data );

        return redirect()->route('admin.crud.campana.index')
            ->withSuccess(trans('crud::campanas.messages.resource created', ['name' => trans('crud::campanas.title.campanas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Campana $campana
     * @return Response
     */
    public function edit(Campana $campana)
    {
        $campana = Campana::with('imagen','imagencompacta')->find($campana->ABC06_id);
        return view('crud::admin.campanas.edit', compact('campana'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Campana $campana
     * @param  UpdateCampanaRequest $request
     * @return Response
     */
    public function update(Campana $campana, UpdateCampanaRequest $request)
    {
        $data = $request->all();
        $data['ABC06_slug']= str_slug($data['ABC06_nombre'], '-');
        $this->campana->update($campana, $data);

        return redirect()->route('admin.crud.campana.index')
            ->withSuccess(trans('crud::campanas.messages.resource updated', ['name' => trans('crud::campanas.title.campanas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Campana $campana
     * @return Response
     */
    public function destroy(Campana $campana)
    {
    	$deleted_at = $campana->deleted_at;
        $json = null;

        $asignado = false;

        if($asignado){
            $json = array(
                'asignado' => $asignado,
                'msn' => trans('crud::campanas.messages.resource assigned')
            );
        }else{
            $success = is_null($deleted_at)? $campana->delete():$campana->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('crud::campanas.messages.resource desactivate'):trans('crud::campanas.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }      

        return response()->json($json);   
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {

        $campanas =  Campana::withTrashed()->get();
        return Datatables::of($campanas)
        ->addColumn('ruta_edit', function ($campanas) {
            return route("admin.crud.campana.edit",[$campanas->ABC06_id]);
        })        
        ->addColumn('ruta_destroy', function($campanas){
            return route("admin.crud.campana.destroy",[$campanas->ABC06_id]);
        })
        ->addColumn('inactivo', function ($campanas) {
            return is_null($campanas->deleted_at)? false:true;
        })
        ->make(true); 
 
    }
}
