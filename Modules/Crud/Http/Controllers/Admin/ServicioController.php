<?php

namespace Modules\Crud\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Crud\Entities\Servicio;
use Modules\Crud\Http\Requests\CreateServicioRequest;
use Modules\Crud\Http\Requests\UpdateServicioRequest;
use Modules\Crud\Repositories\ServicioRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class ServicioController extends AdminBaseController
{
    /**
     * @var ServicioRepository
     */
    private $servicio;

    public function __construct(ServicioRepository $servicio)
    {
        parent::__construct();

        $this->servicio = $servicio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$servicios = $this->servicio->all();

        return view('crud::admin.servicios.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('crud::admin.servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateServicioRequest $request
     * @return Response
     */
    public function store(CreateServicioRequest $request)
    {
        $this->servicio->create($request->all());

        return redirect()->route('admin.crud.servicio.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('crud::servicios.title.servicios')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Servicio $servicio
     * @return Response
     */
    public function edit(Servicio $servicio)
    {
        $servicio = Servicio::with('imagen','icono')->find($servicio->ABC15_id);
        return view('crud::admin.servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Servicio $servicio
     * @param  UpdateServicioRequest $request
     * @return Response
     */
    public function update(Servicio $servicio, UpdateServicioRequest $request)
    {
        $this->servicio->update($servicio, $request->all());

        return redirect()->route('admin.crud.servicio.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('crud::servicios.title.servicios')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Servicio $servicio
     * @return Response
     */
    public function destroy(Servicio $servicio)
    {
        $deleted_at = $servicio->deleted_at;
        $json = null;

        $asignado = false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('crud::servicios.messages.resource assigned')
            );

        }else{

            $success = is_null($deleted_at)? $servicio->delete():$servicio->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('crud::servicios.messages.resource desactivate'):trans('crud::servicios.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }      

        return response()->json($json);
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {

        $servicios =  Servicio::withTrashed()->get();
        return Datatables::of($servicios)
        ->addColumn('ruta_edit', function ($servicios) {
            return route("admin.crud.servicio.edit",[$servicios->ABC15_id]);
        })        
        ->addColumn('ruta_destroy', function($servicios){
            return route("admin.crud.servicio.destroy",[$servicios->ABC15_id]);
        })
        ->addColumn('inactivo', function ($servicios) {
            return is_null($servicios->deleted_at)? false:true;
        })
        ->make(true); 
 
    }
}
