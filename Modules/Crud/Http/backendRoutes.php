<?php

use Illuminate\Routing\Router;
use Modules\Crud\Entities\Campana;
use Modules\Crud\Entities\Servicio;
/** @var Router $router */

$router->group(['prefix' =>'/crud'], function (Router $router) {
    $router->bind('campana', function ($id) {
        $repo = app('Modules\Crud\Repositories\CampanaRepository')->find($id);
        return (is_null($repo))? Campana::withTrashed()->find($id):$repo;
    });
    $router->get('campanas', [
        'as' => 'admin.crud.campana.index',
        'uses' => 'CampanaController@index',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'campana',
        'datatable' => 'admin.campana.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('campanas/create', [
        'as' => 'admin.crud.campana.create',
        'uses' => 'CampanaController@create',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'campana',
        'datatable' => 'admin.campana.datatable'
    ]);
    $router->post('campanas', [
        'as' => 'admin.crud.campana.store',
        'uses' => 'CampanaController@store',
    ]);
    $router->get('campanas/{campana}/edit', [
        'as' => 'admin.crud.campana.edit',
        'uses' => 'CampanaController@edit',
    ]);
    $router->put('campanas/{campana}', [
        'as' => 'admin.crud.campana.update',
        'uses' => 'CampanaController@update',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'campana',
        'datatable' => 'admin.campana.datatable'
    ]);
    $router->get('campanas/destroy/{campana}', [
        'as' => 'admin.crud.campana.destroy',
        'uses' => 'CampanaController@destroy',
    ]);
    $router->bind('servicio', function ($id) {
        $repo = app('Modules\Crud\Repositories\ServicioRepository')->find($id);
        return (is_null($repo))? Servicio::withTrashed()->find($id):$repo;
    });
    $router->get('servicios', [
        'as' => 'admin.crud.servicio.index',
        'uses' => 'ServicioController@index',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'servicio',
        'datatable' => 'admin.servicio.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('servicios/create', [
        'as' => 'admin.crud.servicio.create',
        'uses' => 'ServicioController@create',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'servicio',
        'datatable' => 'admin.servicio.datatable'
    ]);
    $router->post('servicios', [
        'as' => 'admin.crud.servicio.store',
        'uses' => 'ServicioController@store',
    ]);
    $router->get('servicios/{servicio}/edit', [
        'as' => 'admin.crud.servicio.edit',
        'uses' => 'ServicioController@edit',
        'script' => 'AppCrud',
        'view' => 'index',
        'modulo' => 'crud',
        'submodulo' => 'servicio',
        'datatable' => 'admin.servicio.datatable'
    ]);
    $router->put('servicios/{servicio}', [
        'as' => 'admin.crud.servicio.update',
        'uses' => 'ServicioController@update',
    ]);
    $router->get('servicios/delete/{servicio}', [
        'as' => 'admin.crud.servicio.destroy',
        'uses' => 'ServicioController@destroy',
    ]);
// append
    $router->get('campana/datatable', [
        'as' => "admin.campana.datatable",
        "uses" => "CampanaController@datatable",
    ]);
    $router->get('servicio/datatable', [
        'as' => "admin.servicio.datatable",
        "uses" => "ServicioController@datatable",
    ]);
});
