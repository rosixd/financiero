<?php

namespace Modules\Crud\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Crud\Events\Handlers\RegisterCrudSidebar;

class CrudServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterCrudSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('campanas', array_dot(trans('crud::campanas')));
            $event->load('servicios', array_dot(trans('crud::servicios')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('crud', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Crud\Repositories\CampanaRepository',
            function () {
                $repository = new \Modules\Crud\Repositories\Eloquent\EloquentCampanaRepository(new \Modules\Crud\Entities\Campana());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Crud\Repositories\Cache\CacheCampanaDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Crud\Repositories\ServicioRepository',
            function () {
                $repository = new \Modules\Crud\Repositories\Eloquent\EloquentServicioRepository(new \Modules\Crud\Entities\Servicio());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Crud\Repositories\Cache\CacheServicioDecorator($repository);
            }
        );
// add bindings


    }
}
