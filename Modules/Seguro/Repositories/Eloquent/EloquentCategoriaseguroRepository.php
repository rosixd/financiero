<?php

namespace Modules\Seguro\Repositories\Eloquent;

use Modules\Seguro\Repositories\CategoriaseguroRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoriaseguroRepository extends EloquentBaseRepository implements CategoriaseguroRepository
{
}
