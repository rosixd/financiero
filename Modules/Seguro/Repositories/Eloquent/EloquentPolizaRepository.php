<?php

namespace Modules\Seguro\Repositories\Eloquent;

use Modules\Seguro\Repositories\PolizaRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPolizaRepository extends EloquentBaseRepository implements PolizaRepository
{
}
