<?php

namespace Modules\Seguro\Repositories\Eloquent;

use Modules\Seguro\Repositories\SeguroRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSeguroRepository extends EloquentBaseRepository implements SeguroRepository
{
}
