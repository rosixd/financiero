<?php

namespace Modules\Seguro\Repositories\Eloquent;

use Modules\Seguro\Repositories\DetalleseguroRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDetalleseguroRepository extends EloquentBaseRepository implements DetalleseguroRepository
{
}
