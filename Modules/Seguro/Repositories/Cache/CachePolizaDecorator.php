<?php

namespace Modules\Seguro\Repositories\Cache;

use Modules\Seguro\Repositories\PolizaRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePolizaDecorator extends BaseCacheDecorator implements PolizaRepository
{
    public function __construct(PolizaRepository $poliza)
    {
        parent::__construct();
        $this->entityName = 'seguro.polizas';
        $this->repository = $poliza;
    }
}
