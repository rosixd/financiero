<?php

namespace Modules\Seguro\Repositories\Cache;

use Modules\Seguro\Repositories\SeguroRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSeguroDecorator extends BaseCacheDecorator implements SeguroRepository
{
    public function __construct(SeguroRepository $seguro)
    {
        parent::__construct();
        $this->entityName = 'seguro.seguros';
        $this->repository = $seguro;
    }
}
