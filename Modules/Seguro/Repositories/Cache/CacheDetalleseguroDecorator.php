<?php

namespace Modules\Seguro\Repositories\Cache;

use Modules\Seguro\Repositories\DetalleseguroRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDetalleseguroDecorator extends BaseCacheDecorator implements DetalleseguroRepository
{
    public function __construct(DetalleseguroRepository $detalleseguro)
    {
        parent::__construct();
        $this->entityName = 'seguro.detalleseguros';
        $this->repository = $detalleseguro;
    }
}
