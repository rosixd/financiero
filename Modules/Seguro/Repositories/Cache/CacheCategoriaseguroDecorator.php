<?php

namespace Modules\Seguro\Repositories\Cache;

use Modules\Seguro\Repositories\CategoriaseguroRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCategoriaseguroDecorator extends BaseCacheDecorator implements CategoriaseguroRepository
{
    public function __construct(CategoriaseguroRepository $categoriaseguro)
    {
        parent::__construct();
        $this->entityName = 'seguro.categoriaseguros';
        $this->repository = $categoriaseguro;
    }
}
