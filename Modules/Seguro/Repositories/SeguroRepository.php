<?php

namespace Modules\Seguro\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface SeguroRepository extends BaseRepository
{
}
