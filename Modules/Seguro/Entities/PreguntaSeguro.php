<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class PreguntaSeguro extends Model implements Auditable
{
    //use SoftDeletes, \OwenIt\Auditing\Auditable;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'ABC18_Pregunta_Seguro';

    protected $fillable = [
        'ABC17_id',
        'ABC18_pregunta',
        'ABC18_respuesta'
    ];

    protected $primaryKey = 'ABC18_id';

}
