<?php

namespace Modules\Seguro\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seguro extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC17_Seguro';

    protected $fillable = [
        'ABC17_nombre',
        'ABC17_descripcion_corta',
        'ABC17_img_principal',
        'ABC17_logo_principal',
        'ABC17_logo_compacto',
        'ABC14_id',
        'ABC17_condiciones_legales'
    ];

    protected $primaryKey = 'ABC17_id';

    protected $appends = ['name','id','key'];

    public function getNameAttribute()
    {
        return $this->ABC17_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC17_id;
    }

    public function imagenprincipal()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC17_img_principal');
    }

    public function logoprincipal()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC17_logo_principal');
    }

    public function logocompacto()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC17_logo_compacto');
    }

    public function categoria()
    {
        return $this->hasMany('Modules\Seguro\Entities\Categoriaseguro','ABC14_id','ABC14_id');
    }


    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }

    public function getKeyAttribute()
    {
        return "Seguro";
    }
}