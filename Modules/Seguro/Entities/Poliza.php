<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class Poliza extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC16_Poliza';

    protected $fillable = [
        'ABC16_codigo',
        'ABC16_nombre',
        'ABC16_plan_1',
        'ABC16_plan_2'
    ];

    protected $primaryKey = 'ABC16_id';

    protected $appends = ['name','id','key'];

    public function getNameAttribute()
    {
        return $this->ABC16_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC16_id;
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    public function getKeyAttribute()
    {
        return "Poliza";
    }
}
