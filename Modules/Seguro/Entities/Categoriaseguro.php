<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class Categoriaseguro extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC14_Categorias_seguro';
    protected $primaryKey = 'ABC14_id';
    protected $fillable = [
        "ABC14_nombre",
        "ABC14_imagen",
        
    ];

    public function imagen()
    {
        return $this->hasMany('Modules\Media\Entities\File','id','ABC14_imagen');
    }

    protected $appends = ['name','id','key'];

    public function getNameAttribute()
    {
        return $this->ABC14_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC14_id;
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    public function getKeyAttribute()
    {
        return "Categor&iacute;a Seguro";
    }

}
