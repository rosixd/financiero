<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\Model;

class SeguroTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'seguro__seguro_translations';
}
