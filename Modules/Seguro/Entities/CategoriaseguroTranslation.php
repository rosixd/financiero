<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoriaseguroTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'seguro__categoriaseguro_translations';
}
