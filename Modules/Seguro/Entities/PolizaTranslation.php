<?php

namespace Modules\Seguro\Entities;

use Illuminate\Database\Eloquent\Model;

class PolizaTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'seguro__poliza_translations';
}
