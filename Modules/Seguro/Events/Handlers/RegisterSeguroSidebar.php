<?php

namespace Modules\Seguro\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterSeguroSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        // $menu->group(trans('core::sidebar.content'), function (Group $group) {
        //     $group->item(trans('seguro::seguros.title.seguros'), function (Item $item) {
        //         $item->icon('fa fa-gittip');
        //         $item->weight(10);
        //         $item->item(trans('seguro::seguros.title.seguros'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.seguro.seguro.create');
        //             $item->route('admin.seguro.seguro.index');
        //         });
        //         $item->item(trans('seguro::categoriaseguros.title.categoriaseguros'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.seguro.categoriaseguro.create');
        //             $item->route('admin.seguro.categoriaseguro.index');
        //         });
        //         $item->item(trans('seguro::polizas.title.polizas'), function (Item $item) {
        //             $item->icon('fa fa-circle-o');
        //             $item->weight(0);
        //             $item->append('admin.seguro.poliza.create');
        //             $item->route('admin.seguro.poliza.index');
        //         });
        //     });
        // });

        return $menu;
    }
}
