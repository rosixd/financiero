<?php

return [
    'list resource' => 'Listar categor&iacute;a seguros',
    'create resource' => 'Crear categor&iacute;a seguros',
    'edit resource' => 'Editar categor&iacute;a seguros',
    'destroy resource' => 'Eliminar categor&iacute;a seguros',
    'title' => [
        'categoriaseguros' => 'Categor&iacute;a seguro',
        'create categoriaseguro' => 'Create a categor&iacute;a seguro',
        'edit categoriaseguro' => 'Editar  categor&iacute;a seguro',
    ],
    'button' => [
        'create categoriaseguro' => 'Crear categor&iacute;a seguro',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset'  => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [
        'name' =>'Nombre',
        'image' =>'Imagen',
    ],
    'form' => [
        'name'  => 'Nombre',
        'image' => 'Logo'
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource desactivate' => 'Sr. Usuario la categor&iacute;a seguro se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, el categor&iacute;a seguro se ha activado correctamente.',
    ],
    'validation' => [
        'name'  => 'Sr. Usuario, debe ingresar el nombre.',
        'image' => 'Sr. Usuario, debe ingresar la imagen.',
    ],
];
