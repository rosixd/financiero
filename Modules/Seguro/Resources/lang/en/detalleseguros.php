<?php

return [
    'list resource' => 'Listar  detalle de seguros',
    'create resource' => 'Crear detalle de seguros',
    'edit resource' => 'Editar detalle de seguros',
    'destroy resource' => 'Eliminar etalle seguros',
    'title' => [
        'detalleseguros' => 'Detalle seguro',
        'create detalleseguro' => 'Crear detalle seguro',
        'edit detalleseguro' => 'Editar detalle seguro',
    ],
    'button' => [
        'create detalleseguro' => 'Crear detalle seguro',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset'  => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [
        'name' =>'Nombre',
        'descripcion' =>'Descripci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'descripcion' =>'Descripci&oacute;n',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource desactivate' => 'Sr. Usuario el detalle seguro se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario,  el detalle seguro se ha activado correctamente.',
    ],
    'validation' => [
        'name' =>'Sr. Usuario, debe ingresar el nombre',
        'descripcion' =>'Sr. Usuario, debe seleccionar una descripci&oacute;n',
    ],
];
