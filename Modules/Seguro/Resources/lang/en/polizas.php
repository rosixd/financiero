<?php

return [
    'list resource' => 'Listar p&oacute;lizas',
    'create resource' => 'Crear p&oacute;lizas',
    'edit resource' => 'Editar p&oacute;lizas',
    'destroy resource' => 'Eliminar p&oacute;lizas',
    'title' => [
        'polizas' => 'P&oacute;liza',
        'create poliza' => 'Crear  p&oacute;liza',
        'edit poliza' => 'Editar  p&oacute;liza',
    ],
    'button' => [
        'create poliza' => 'Create p&oacute;liza',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Eliminar',
        'reset'  => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [

        'codigo' => 'C&oacute;digo',
        'nombre' => 'Nombre',
        'plan 1' => 'Plan 1',
        'plan 2' => 'Plan 2',
    ],
    'form' => [

        'codigo' => 'C&oacute;digo',
        'nombre' => 'Nombre',
        'plan 1' => 'Plan 1',
        'plan 2' => 'Plan 2',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource desactivate' => 'Sr. Usuario la p&oacute;liza  se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, la p&oacute;liza se ha activado correctamente.',
    ],
    'validation' => [
        'codigo' => 'Sr. Usuario, debe ingresar el C&oacute;digo',
        'nombre' => 'Sr. Usuario, debe ingresar un nombre',
        'plan 1' => 'Sr. Usuario, debe ingresar un plan 1',
        'plan 2' => 'Sr. Usuario, debe ingresar un plan 2',
    ],
];
