<div class="box-body">

	<div class="form-group">
        <label>{{ trans('seguro::polizas.form.codigo') }}</label>
        
		<input type="text" class="form-control alfanumerico" name="ABC16_codigo" id="ABC16_codigo" maxlength="3" >
		<span class="text-danger">
			{{ $errors->has('ABC16_codigo') ? trans('seguro::polizas.validation.codigo') : '' }}
		</span>
    </div>
    
	<div class="form-group">
        <label>{{ trans('seguro::polizas.form.nombre') }}</label>
        
        <input type="text" class="form-control" name="ABC16_nombre" id="ABC16_nombre" maxlength="100">
		<span class="text-danger">
			{{ $errors->has('ABC16_nombre') ? trans('seguro::polizas.validation.nombre') : '' }}
		</span>
	</div>

    <div class="form-group">
        <label>{{ trans('seguro::polizas.form.plan 1') }}</label>
        
		<textarea class="form-control ckeditor" name="ABC16_plan_1" id="ABC16_plan_1"></textarea>
		<span class="text-danger">
			{{ $errors->has('ABC16_plan_1') ? trans('seguro::polizas.validation.plan 1') : '' }}
		</span>
    </div>

    <div class="form-group">
		<label>{{ trans('seguro::polizas.form.plan 2') }}</label>
		<textarea class="form-control ckeditor" name="ABC16_plan_2" id="ABC16_plan_2"></textarea>
		<span class="text-danger">
			{{ $errors->has('ABC16_plan_2') ? trans('seguro::polizas.validation.plan 2') : '' }}
		</span>
	</div>

</div>

