<div class="box-body">

	<div class="form-group">
        <label>{{ trans('seguro::seguros.form.nombre') }}</label>
        
        <input type="text" class="form-control" name="ABC17_nombre" id="ABC17_nombre" maxlength="100">
		<span class="text-danger">
			{{ $errors->has('ABC17_nombre') ? trans('seguro::seguros.validation.nombre') : '' }}
		</span>
    </div>
    
    @include('core::selectImg',
    ['name'=>'ABC14_imagen', 
     'label' => trans('seguro::categoriaseguros.form.image'),
     'required' => trans('seguro::categoriaseguros.validation.image'),
     'img' =>$categoriaseguro->imagen->first()->filename,
     'id' => $categoriaseguro->ABC14_imagen,
    
    ])
    @include('core::selectImg',
		['name'=>'ABC17_img_principal', 
		 'label' => trans('seguro::seguros.form.imagen principal'),
		 'required' => trans('seguro::seguros.validation.imagen principal')
    ])

    @include('core::selectImg',
		['name'=>'ABC17_logo_principal', 
		 'label' => trans('seguro::seguros.form.logo principal'),
		 'required' => trans('seguro::seguros.validation.logo principal')
    ])

    @include('core::selectImg',
		['name'=>'ABC17_logo_compacto', 
		 'label' => trans('seguro::seguros.form.logo compacto'),
		 'required' => trans('seguro::seguros.validation.logo compacto')
    ])
    <div class="form-group">
        <label>{{ trans('seguro::seguros.form.categoria seguro') }}</label>
		<select name="ABC14_id" class="form-control">

			<option value="">Seleccione</option>)

			@foreach($categoria as $cat)
				<option value="{{$cat->ABC14_id}}">{{$cat->ABC14_nombre}}</option>
			@endforeach

		</select>

		<span class="text-danger">
			{{ $errors->has('ABC14_id') ? trans('seguro::seguros.validation.categoria seguro') : '' }}
		</span>

	</div>

    <div class="form-group">
        <label>{{ trans('seguro::seguros.form.descripcion corta') }}</label>
        
		<textarea class="form-control ckeditor" name="ABC17_descripcion_corta" id="ABC17_descripcion_corta"></textarea>
		<span class="text-danger">
			{{ $errors->has('ABC17_descripcion_corta') ? trans('seguro::seguros.validation.descripcion corta') : '' }}
		</span>
    </div>

    <div class="form-group">
		<label>{{ trans('seguro::seguros.form.condiciones legales') }}</label>
		<textarea class="form-control ckeditor" name="ABC17_condiciones_legales" id="ABC17_condiciones_legales"></textarea>
		<span class="text-danger">
			{{ $errors->has('ABC17_condiciones_legales') ? trans('seguro::seguros.validation.condiciones legales') : '' }}
		</span>
    </div>
    <hr>
    <div class="form-group">
        <button type="button" class="btn btn-primary btn-xs" id="btnAdd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
        <label>{{ trans('seguro::seguros.form.agregar complemento') }}</label>
      </div>
      <div id="subMenu"></div>
</div>
