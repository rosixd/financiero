<div class="box-body">

	<div class="form-group">
		<label>{{ trans('seguro::categoriaseguros.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC14_nombre" id="ABC14_nombre" maxlength="100" value="{{ $categoriaseguro->ABC14_nombre }}">
		<span class="text-danger">
			{{ $errors->has('ABC14_nombre') ? trans('seguro::categoriaseguros.validation.name') : '' }}
		</span>
	</div>
	
	@include('core::selectImg',
		['name'=>'ABC14_imagen', 
		 'label' => trans('seguro::categoriaseguros.form.image'),
         'required' => trans('seguro::categoriaseguros.validation.image'),
         'img' =>$categoriaseguro->imagen->first()->filename,
    	 'id' => $categoriaseguro->ABC14_imagen,
    	
    ])

    
</div>