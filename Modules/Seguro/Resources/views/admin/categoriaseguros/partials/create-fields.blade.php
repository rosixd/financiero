<div class="box-body">

	<div class="form-group">
		<label>{{ trans('seguro::categoriaseguros.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC14_nombre" id="ABC14_nombre" maxlength="100">
		<span class="text-danger">
			{{ $errors->has('ABC14_nombre') ? trans('seguro::categoriaseguros.validation.name') : '' }}
		</span>
	</div>
	
	@include('core::selectImg',
		['name'=>'ABC14_imagen', 
		 'label' => trans('seguro::categoriaseguros.form.image'),
		 'required' => trans('seguro::categoriaseguros.validation.image')
    ])
    
</div>