<?php

return [
    'seguro.seguros' => [
        'index' => 'seguro::seguros.list resource',
        'create' => 'seguro::seguros.create resource',
        'edit' => 'seguro::seguros.edit resource',
        'destroy' => 'seguro::seguros.destroy resource',
    ],
    'seguro.categoriaseguros' => [
        'index' => 'seguro::categoriaseguros.list resource',
        'create' => 'seguro::categoriaseguros.create resource',
        'edit' => 'seguro::categoriaseguros.edit resource',
        'destroy' => 'seguro::categoriaseguros.destroy resource',
    ],
    'seguro.polizas' => [
        'index' => 'seguro::polizas.list resource',
        'create' => 'seguro::polizas.create resource',
        'edit' => 'seguro::polizas.edit resource',
        'destroy' => 'seguro::polizas.destroy resource',
    ],
    'seguro.detalleseguros' => [
        'index' => 'seguro::detalleseguros.list resource',
        'create' => 'seguro::detalleseguros.create resource',
        'edit' => 'seguro::detalleseguros.edit resource',
        'destroy' => 'seguro::detalleseguros.destroy resource',
    ],
// append




];
