<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguroDetalleseguroTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguro__detalleseguro_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('detalleseguro_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['detalleseguro_id', 'locale']);
            $table->foreign('detalleseguro_id')->references('id')->on('seguro__detalleseguros')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seguro__detalleseguro_translations', function (Blueprint $table) {
            $table->dropForeign(['detalleseguro_id']);
        });
        Schema::dropIfExists('seguro__detalleseguro_translations');
    }
}
