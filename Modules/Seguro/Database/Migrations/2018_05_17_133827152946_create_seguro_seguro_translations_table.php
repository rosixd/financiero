<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguroSeguroTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguro__seguro_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('seguro_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['seguro_id', 'locale']);
            $table->foreign('seguro_id')->references('id')->on('seguro__seguros')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seguro__seguro_translations', function (Blueprint $table) {
            $table->dropForeign(['seguro_id']);
        });
        Schema::dropIfExists('seguro__seguro_translations');
    }
}
