<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguroCategoriaseguroTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguro__categoriaseguro_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('categoriaseguro_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['categoriaseguro_id', 'locale']);
            $table->foreign('categoriaseguro_id')->references('id')->on('seguro__categoriaseguros')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seguro__categoriaseguro_translations', function (Blueprint $table) {
            $table->dropForeign(['categoriaseguro_id']);
        });
        Schema::dropIfExists('seguro__categoriaseguro_translations');
    }
}
