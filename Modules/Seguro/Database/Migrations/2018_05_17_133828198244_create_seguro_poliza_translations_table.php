<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguroPolizaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguro__poliza_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('poliza_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['poliza_id', 'locale']);
            $table->foreign('poliza_id')->references('id')->on('seguro__polizas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seguro__poliza_translations', function (Blueprint $table) {
            $table->dropForeign(['poliza_id']);
        });
        Schema::dropIfExists('seguro__poliza_translations');
    }
}
