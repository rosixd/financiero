<?php

namespace Modules\Seguro\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Seguro\Events\Handlers\RegisterSeguroSidebar;

class SeguroServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterSeguroSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('seguros', array_dot(trans('seguro::seguros')));
            $event->load('categoriaseguros', array_dot(trans('seguro::categoriaseguros')));
            $event->load('polizas', array_dot(trans('seguro::polizas')));
            $event->load('detalleseguros', array_dot(trans('seguro::detalleseguros')));
            // append translations




        });
    }

    public function boot()
    {
        $this->publishConfig('seguro', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Seguro\Repositories\SeguroRepository',
            function () {
                $repository = new \Modules\Seguro\Repositories\Eloquent\EloquentSeguroRepository(new \Modules\Seguro\Entities\Seguro());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Seguro\Repositories\Cache\CacheSeguroDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Seguro\Repositories\CategoriaseguroRepository',
            function () {
                $repository = new \Modules\Seguro\Repositories\Eloquent\EloquentCategoriaseguroRepository(new \Modules\Seguro\Entities\Categoriaseguro());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Seguro\Repositories\Cache\CacheCategoriaseguroDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Seguro\Repositories\PolizaRepository',
            function () {
                $repository = new \Modules\Seguro\Repositories\Eloquent\EloquentPolizaRepository(new \Modules\Seguro\Entities\Poliza());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Seguro\Repositories\Cache\CachePolizaDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Seguro\Repositories\DetalleseguroRepository',
            function () {
                $repository = new \Modules\Seguro\Repositories\Eloquent\EloquentDetalleseguroRepository(new \Modules\Seguro\Entities\Detalleseguro());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Seguro\Repositories\Cache\CacheDetalleseguroDecorator($repository);
            }
        );
// add bindings




    }
}
