<?php

namespace Modules\Seguro\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePolizaRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC16_codigo' =>'required',
            'ABC16_nombre' =>'required',
            'ABC16_plan_1' =>'required',
            'ABC16_plan_2' =>'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
