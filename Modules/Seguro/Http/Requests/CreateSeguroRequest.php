<?php

namespace Modules\Seguro\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateSeguroRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC17_nombre'              => 'required',
            'ABC17_descripcion_corta'   => 'required',
            'ABC17_img_principal'       => 'required',
            'ABC17_logo_principal'      => 'required',
            'ABC17_logo_compacto'       => 'required',
            'ABC14_id'                  => 'required',
            'ABC17_condiciones_legales' => 'required',
        ];
    }
      
    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
