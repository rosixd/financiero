<?php

namespace Modules\Seguro\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateCategoriaseguroRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            "ABC14_nombre" => 'required',
            "ABC14_imagen" => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
