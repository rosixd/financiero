<?php

namespace Modules\Seguro\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Seguro\Entities\Poliza;
use Modules\Seguro\Http\Requests\CreatePolizaRequest;
use Modules\Seguro\Http\Requests\UpdatePolizaRequest;
use Modules\Seguro\Repositories\PolizaRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class PolizaController extends AdminBaseController
{
    /**
     * @var PolizaRepository
     */
    private $poliza;

    public function __construct(PolizaRepository $poliza)
    {
        parent::__construct();

        $this->poliza = $poliza;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$polizas = $this->poliza->all();

        return view('seguro::admin.polizas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('seguro::admin.polizas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePolizaRequest $request
     * @return Response
     */
    public function store(CreatePolizaRequest $request)
    {
        $this->poliza->create($request->all());

        return redirect()->route('admin.seguro.poliza.index')
            ->withSuccess(trans('seguro::polizas.messages.resource created', ['name' => trans('seguro::polizas.title.polizas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Poliza $poliza
     * @return Response
     */
    public function edit(Poliza $poliza)
    {
        return view('seguro::admin.polizas.edit', compact('poliza'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Poliza $poliza
     * @param  UpdatePolizaRequest $request
     * @return Response
     */
    public function update(Poliza $poliza, UpdatePolizaRequest $request)
    {
        $this->poliza->update($poliza, $request->all());

        return redirect()->route('admin.seguro.poliza.index')
            ->withSuccess(trans('seguro::polizas.messages.resource updated', ['name' => trans('seguro::polizas.title.polizas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Poliza $poliza
     * @return Response
     */
    public function destroy(Poliza $poliza)
    {
        $deleted_at = $poliza->deleted_at;
        $json = null;
    
            $success = is_null($deleted_at)? $poliza->delete():$poliza->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('seguro::polizas.messages.resource desactivate'):trans('seguro::polizas.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false,
                'asignado' => false
            );      

        return response()->json($json);
    }
    /**
     * Datetable
     *
     * @param  request $request
     * @return Response
     */
    public function datatable(Request $request)
    {
        $Poliza = Poliza::withTrashed()->get();
       

        return Datatables::of($Poliza)
        ->addColumn('ruta_edit', function ($Poliza) {
            return route("admin.seguro.poliza.edit",[$Poliza->ABC16_id]);
        })        
        ->addColumn('ruta_destroy', function($Poliza){
            return route("admin.seguro.poliza.destroy",[$Poliza->ABC16_id]);
        })
        ->addColumn('inactivo', function ($Poliza) {
            return is_null($Poliza->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
