<?php

namespace Modules\Seguro\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Seguro\Entities\Categoriaseguro;
use Modules\Seguro\Http\Requests\CreateCategoriaseguroRequest;
use Modules\Seguro\Http\Requests\UpdateCategoriaseguroRequest;
use Modules\Seguro\Repositories\CategoriaseguroRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class CategoriaseguroController extends AdminBaseController
{
    /**
     * @var CategoriaseguroRepository
     */
    private $categoriaseguro;

    public function __construct(CategoriaseguroRepository $categoriaseguro)
    {
        parent::__construct();

        $this->categoriaseguro = $categoriaseguro;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$categoriaseguros = $this->categoriaseguro->all();

        return view('seguro::admin.categoriaseguros.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('seguro::admin.categoriaseguros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoriaseguroRequest $request
     * @return Response
     */
    public function store(CreateCategoriaseguroRequest $request)
    {
        $this->categoriaseguro->create($request->all());

        return redirect()->route('admin.seguro.categoriaseguro.index')
            ->withSuccess(trans('seguro::categoriaseguros.messages.resource created', ['name' => trans('seguro::categoriaseguros.title.categoriaseguros')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Categoriaseguro $categoriaseguro
     * @return Response
     */
    public function edit(Categoriaseguro $categoriaseguro)
    { 
        return view('seguro::admin.categoriaseguros.edit', compact('categoriaseguro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Categoriaseguro $categoriaseguro
     * @param  UpdateCategoriaseguroRequest $request
     * @return Response
     */
    public function update(Categoriaseguro $categoriaseguro, UpdateCategoriaseguroRequest $request)
    {
        $this->categoriaseguro->update($categoriaseguro, $request->all());

        return redirect()->route('admin.seguro.categoriaseguro.index')
            ->withSuccess(trans('seguro::categoriaseguros.resource updated', ['name' => trans('seguro::categoriaseguros.title.categoriaseguros')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Categoriaseguro $categoriaseguro
     * @return Response
     */
    public function destroy(Categoriaseguro $categoriaseguro)
    {
       
	$deleted_at = $categoriaseguro->deleted_at;
        $json = null;
    
            $success = is_null($deleted_at)? $categoriaseguro->delete():$categoriaseguro->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('seguro::categoriaseguros.messages.resource desactivate'):trans('seguro::categoriaseguros.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false,
                'asignado' => false
            );      

        return response()->json($json);
    }
    /**
     * Datetable
     *
     * @param  request $request
     * @return Response
     */
    public function datatable(Request $request)
    {
        $Categoriaseguro = Categoriaseguro::with('imagen')->withTrashed()->get();
       

        return Datatables::of($Categoriaseguro)
        ->addColumn('ruta_edit', function ($Categoriaseguro) {
            return route("admin.seguro.categoriaseguro.edit",[$Categoriaseguro->ABC14_id]);
        })        
        ->addColumn('ruta_destroy', function($Categoriaseguro){
            return route("admin.seguro.categoriaseguro.destroy",[$Categoriaseguro->ABC14_id]);
        })
        ->addColumn('nombre_imagen', function($Categoriaseguro){
            return $Categoriaseguro->imagen->first()->filename;
        })
        ->addColumn('inactivo', function ($Categoriaseguro) {
            return is_null($Categoriaseguro->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
