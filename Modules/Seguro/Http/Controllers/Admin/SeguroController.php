<?php

namespace Modules\Seguro\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Seguro\Entities\Seguro;
use Modules\Seguro\Http\Requests\CreateSeguroRequest;
use Modules\Seguro\Http\Requests\UpdateSeguroRequest;
use Modules\Seguro\Repositories\SeguroRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Seguro\Entities\Categoriaseguro;
use Modules\Seguro\Entities\PreguntaSeguro;
use DB;
use Yajra\Datatables\Datatables;
class SeguroController extends AdminBaseController
{
    /**
     * @var SeguroRepository
     */
    private $seguro;

    public function __construct(SeguroRepository $seguro)
    {
        parent::__construct();

        $this->seguro = $seguro;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$seguros = $this->seguro->all();

        return view('seguro::admin.seguros.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoria = Categoriaseguro::all();
        return view('seguro::admin.seguros.create',compact('categoria') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSeguroRequest $request
     * @return Response
     */
    public function store(CreateSeguroRequest $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->nombre);

        $seguro =  $this->seguro->create($data);
        if(isset($request->ABC18_pregunta)){
            foreach ($request->ABC18_pregunta as $key => $value) {
                PreguntaSeguro::create([
                    'ABC17_id'          => $seguro->ABC17_id,
                    'ABC18_pregunta'    => $value,
                    'ABC18_respuesta'   => $request->ABC18_respuesta[$key]
                ]);
            }
        }
        return redirect()->route('admin.seguro.seguro.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('seguro::seguros.title.seguros')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Seguro $seguro
     * @return Response
     */
    public function edit(Seguro $seguro)
    {
        return view('seguro::admin.seguros.edit', compact('seguro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Seguro $seguro
     * @param  UpdateSeguroRequest $request
     * @return Response
     */
    public function update(Seguro $seguro, UpdateSeguroRequest $request)
    {
        $this->seguro->update($seguro, $request->all());

        return redirect()->route('admin.seguro.seguro.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('seguro::seguros.title.seguros')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Seguro $seguro
     * @return Response
     */
    public function destroy(Seguro $seguro)
    {
        $this->seguro->destroy($seguro);

        return redirect()->route('admin.seguro.seguro.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('seguro::seguros.title.seguros')]));
    }

    /**
     * Datetable
     *
     * @param  request $request
     * @return Response
     */
    public function datatable(Request $request)
    {
        $Seguro = Seguro::withTrashed()->get();
       
       
        return Datatables::of($Seguro)
        ->addColumn('ruta_edit', function ($Seguro) {
            return route("admin.seguro.seguro.edit",[$Seguro->ABC17_id]);
        })        
        ->addColumn('ruta_destroy', function($Seguro){
            return route("admin.seguro.seguro.destroy",[$Seguro->ABC17_id]);
        })
        ->addColumn('categoria', function($Seguro){
           
                return $Seguro->categoria->first()->ABC14_nombre;
          
        })
        ->addColumn('inactivo', function ($Seguro) {
            return is_null($Seguro->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
