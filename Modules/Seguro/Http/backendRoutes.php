<?php

use Illuminate\Routing\Router;
use Modules\Seguro\Entities\Categoriaseguro;
use Modules\Seguro\Entities\Detalleseguro;
use Modules\Seguro\Entities\Poliza;
use Modules\Seguro\Entities\Seguro;

/** @var Router $router */

$router->group(['prefix' =>'/seguro'], function (Router $router) {
    $router->bind('seguro', function ($id) {
        $repo = app('Modules\Seguro\Repositories\SeguroRepository')->find($id);
        return (is_null($repo))? Seguro::withTrashed()->find($id):$repo;
        
    });
    $router->get('seguros', [
        'as' => 'admin.seguro.seguro.index',
        'uses' => 'SeguroController@index',
        'script' => 'AppSeguro',
        'view' => 'index',
        'modulo' => 'seguro',
        'submodulo' => 'seguro',
        'datatable' => 'admin.seguro.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);

    $router->get('seguros/create', [
        'as' => 'admin.seguro.seguro.create',
        'uses' => 'SeguroController@create',
        'script' => 'AppSeguro',
        'view' => 'create',
        'modulo' => 'seguro',
        'submodulo' => 'seguro',
        'datatable' => 'admin.seguro.datatable'

    ]);

    $router->post('seguros', [
        'as' => 'admin.seguro.seguro.store',
        'uses' => 'SeguroController@store',
    ]);

    $router->get('seguros/{seguro}/edit', [
        'as' => 'admin.seguro.seguro.edit',
        'uses' => 'SeguroController@edit',
        'script' => 'AppSeguro',
        'view' => 'edit',
        'modulo' => 'seguro',
        'submodulo' => 'seguro',
        'datatable' => 'admin.seguro.datatable'
        
    ]);
    $router->put('seguros/{seguro}', [
        'as' => 'admin.seguro.seguro.update',
        'uses' => 'SeguroController@update',
    ]);
   
    $router->get('seguros/destroy/{seguro}', [
        'as' => 'admin.seguro.seguro.destroy',
        'uses' => 'SeguroController@destroy',
    ]);

    $router->get('seguros/datatable', [
        'as' => "admin.seguro.datatable",
        "uses" => "SeguroController@datatable",
    ]);

    $router->bind('categoriaseguro', function ($id) {
        $repo = app('Modules\Seguro\Repositories\CategoriaseguroRepository')->find($id);
        return (is_null($repo))? Categoriaseguro::withTrashed()->find($id):$repo;
    });
    $router->get('categoriaseguros', [
        'as' => 'admin.seguro.categoriaseguro.index',
        'uses' => 'CategoriaseguroController@index',
        'script' => 'AppSeguro',
        'view' => 'index',
        'modulo' => 'seguro',
        'submodulo' => 'categoriaseguro',
        'datatable' => 'admin.categoriaseguro.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('categoriaseguros/create', [
        'as' => 'admin.seguro.categoriaseguro.create',
        'uses' => 'CategoriaseguroController@create',
        'script' => 'AppSeguro',
        'view' => 'create',
        'modulo' => 'seguro',
        'submodulo' => 'categoriaseguro',
        'datatable' => 'admin.categoriaseguro.datatable',
    ]);
    $router->post('categoriaseguros', [
        'as' => 'admin.seguro.categoriaseguro.store',
        'uses' => 'CategoriaseguroController@store',
    ]);
    $router->get('categoriaseguros/{categoriaseguro}/edit', [
        'as' => 'admin.seguro.categoriaseguro.edit',
        'uses' => 'CategoriaseguroController@edit',
        'script'    => 'AppSeguro',
        'view'      => 'edit',
        'modulo'    => 'seguro',
        'submodulo' => 'categoriaseguro',
        'datatable' => 'admin.categoriaseguro.datatable'
    ]);
    $router->put('categoriaseguros/{categoriaseguro}', [
        'as' => 'admin.seguro.categoriaseguro.update',
        'uses' => 'CategoriaseguroController@update',
    ]);
    $router->get('categoriaseguros/destroy/{categoriaseguro}', [
        'as' => 'admin.seguro.categoriaseguro.destroy',
        'uses' => 'CategoriaseguroController@destroy',
    ]);

    $router->get('categoriaseguros/datatable', [
        'as' => "admin.categoriaseguro.datatable",
        "uses" => "CategoriaseguroController@datatable",
    ]);
    $router->bind('poliza', function ($id) {
        $repo = app('Modules\Seguro\Repositories\PolizaRepository')->find($id);
        return (is_null($repo))? Poliza::withTrashed()->find($id):$repo;  
    });
    $router->get('polizas', [
        'as' => 'admin.seguro.poliza.index',
        'uses' => 'PolizaController@index',
        'script' => 'AppSeguro',
        'view' => 'index',
        'modulo' => 'seguro',
        'submodulo' => 'poliza',
        'datatable' => 'admin.polizas.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('polizas/create', [
        'as' => 'admin.seguro.poliza.create',
        'uses' => 'PolizaController@create',
        'script' => 'AppSeguro',
        'view' => 'create',
        'modulo' => 'seguro',
        'submodulo' => 'poliza',
        'datatable' => 'admin.polizas.datatable'
    ]);
    $router->post('polizas', [
        'as' => 'admin.seguro.poliza.store',
        'uses' => 'PolizaController@store',
    ]);
    $router->get('polizas/{poliza}/edit', [
        'as' => 'admin.seguro.poliza.edit',
        'uses' => 'PolizaController@edit',
        'script' => 'AppSeguro',
        'view' => 'edit',
        'modulo' => 'seguro',
        'submodulo' => 'poliza',
        'datatable' => 'admin.polizas.datatable'
    ]);
    $router->put('polizas/{poliza}', [
        'as' => 'admin.seguro.poliza.update',
        'uses' => 'PolizaController@update',
    ]);
   
    $router->get('polizas/destroy/{poliza}', [
        'as' => 'admin.seguro.poliza.destroy',
        'uses' => 'PolizaController@destroy',
    ]);

    $router->get('polizas/datatable', [
        'as' => "admin.polizas.datatable",
        "uses" => "PolizaController@datatable",
    ]);
});
