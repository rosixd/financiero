<?php

namespace Modules\Setting\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Sidebar\AbstractAdminSidebar;

class RegisterSettingSidebar extends AbstractAdminSidebar
{
    /**
     * Method used to define your sidebar menu groups and items
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        if( $this->auth->hasAccess('setting.settings.index') ){
            $menu->group(trans('core::sidebar.content'), function (Group $group) {
                $group->weight(100);
                $group->item(trans('setting::settings.permisos.name'), function (Item $item) {
                    $item->icon('fa fa-cog');
                    $item->weight(100);
                    $item->route('admin.setting.settings.index');                    
                });
            });
        }

        return $menu;
    }
}
