<?php

namespace Modules\Setting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    public function rules()
    {
        return [
            'core::site-name.es'=>'required',
            'core::site-description.es'=>'required',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'core::site-name.es.required'=>'Sr. Usuario, debe ingresar un nombre.',
            'core::site-description.es.required'=>'Sr. Usuario, debe ingresar una descripción.',
        ];
    }
}
