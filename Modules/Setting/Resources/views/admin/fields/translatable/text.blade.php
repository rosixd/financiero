<div class='form-group'>
    {!! Form::label($settingName . "[$lang]", trans($moduleInfo['description'])) !!}
    <?php if (isset($dbSettings[$settingName])): ?>
        <?php $value = $dbSettings[$settingName]->hasTranslation($lang) ? $dbSettings[$settingName]->translate($lang)->value : ''; ?>
        {!! Form::text($settingName . "[$lang]", old($settingName . "[$lang]", $value), ['class' => 'form-control', 'maxlength' => '100', 'placeholder' => trans($moduleInfo['description'])]) !!}
        @if($settingName === 'core::site-name')
            <span class="text-danger">
                {!! $errors->first('core::site-name.es') !!}
            </span>
        @endif
    <?php else: ?>
        {!! Form::text($settingName . "[$lang]", old($settingName . "[$lang]"), ['class' => 'form-control', 'placeholder' => trans($moduleInfo['description'])]) !!}
    <?php endif; ?>
</div>
