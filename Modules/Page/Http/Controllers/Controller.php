<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Page\Repositories\PageRepository;
use Carbon\Carbon;
abstract class Controller extends BasePublicController
{
    /**
     * @var PageRepository
     */
    protected $page;
    /**
     * @var Application
     */
    protected $app;
    public $activarmenu = '';

    public function __construct(PageRepository $page, Application $app)
    {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
    }

    public function view($template, $variables = [])
    {
       
        $session = \Session::all();
        
        $variables = array_merge([
            'session' => $session,
            'controller' => $this
        ], $variables);
	
        if (isset($session['Logueado']) && $session['Logueado'] === 'true') {
            
            $cupostarjetas = $this->cupostarjetas();
            $saludo        =  $this->saludo();
            $variables = array_merge([
                'tarjetaBloqueo' => $session['TarjetaBloqueo'],
                'datospersonales' => $session['DatosPersonales']['error'] ? [] : $session['DatosPersonales']['datos']['datos_usuario'],
                'datoscliente' => $session['DatosCLiente']['error'] ? [] : $session['DatosCLiente']['datos'],
                'datosclientetarjeta' => $session['DatosCLiente']['error'] ? [] : $session['DatosCLiente']['datos']['tarjetas'],
                
                'cupostarjetas' => isset( $session['CuposTarjetas'] ) && isset( $session['CuposTarjetas']['error'] ) && $session['CuposTarjetas']['error'] == false
                    ? $cupostarjetas
                    : [],

                'extracupotienda' => isset( $cupostarjetas["ExtracupoTienda"] ) 
                    ? $cupostarjetas["ExtracupoTienda"]["datos"] 
                    : [],

                'disponibleotroscomercios' => isset( $cupostarjetas["DisponibleOtrosComercios"] ) && isset( $cupostarjetas["DisponibleOtrosComercios"]['datos'] ) 
                    ? $cupostarjetas['DisponibleOtrosComercios']['datos']
                    : [],

                'extracupoavance' => isset( $cupostarjetas["ExtracupoAvance"] ) && isset( $cupostarjetas["ExtracupoAvance"]['datos'] ) 
                    ? $cupostarjetas['ExtracupoAvance']['datos']
                    : [],

                'deudapagar' => isset( $cupostarjetas["DeudaPagar"] ) && isset( $cupostarjetas["DeudaPagar"]['datos'] ) 
                    ? $cupostarjetas['DeudaPagar']['datos']
                    : [],

                'tarjetaseleccionada' => $session['DatosCLiente']['error'] ? [] : $this->tarjetaseleccionada(),
                'saludos'           => $saludo,
            ], $variables);

            /**
             * DMT: Codigo original
             * 
             * $variables = array_merge([
             *  'datospersonales' => $session['DatosPersonales']['error'] ? [] : $session['DatosPersonales']['datos']['datos_usuario'],
             *  'datoscliente' => $session['DatosCLiente']['error'] ? [] : $session['DatosCLiente']['datos'],
             *  'datosclientetarjeta' => $session['DatosCLiente']['error'] ? [] : $session['DatosCLiente']['datos']['tarjetas'],
             *  'cupostarjetas' => $session['CuposTarjetas']['error'] ? [] : $this->cupostarjetas(),
             *
             *  'extracupotienda' => $this->cupostarjetas()['ExtracupoTienda']['error'] ? [] : $this->cupostarjetas()['ExtracupoTienda']['datos'],
             *  // 'extracupotienda' => '', 
             *  'disponibleotroscomercios' => $this->cupostarjetas()['DisponibleOtrosComercios']['error'] ? [] : $this->cupostarjetas()['DisponibleOtrosComercios']['datos'],
             *  // 'disponibleotroscomercios' =>'',
             *  'extracupoavance' => $this->cupostarjetas()['ExtracupoAvance']['error'] ? [] : $this->cupostarjetas()['ExtracupoAvance']['datos'],
             *  // 'extracupoavance' => '', 
             *  'deudapagar' => $this->cupostarjetas()['DeudaPagar']['error'] ? [] : $this->cupostarjetas()['DeudaPagar']['datos'],
             *  // 'deudapagar' => '', 
             *  'tarjetaseleccionada' => $session['DatosCLiente']['error'] ? [] : $this->tarjetaseleccionada()
             * ], $variables);
             */
        }
        

        return view($template, $variables);
    }

    public function tarjetaseleccionada()
    {
        $session = \Session::all();
        $tarjetaseleccionada = $session['TarjetaSeleccionada'];

        return $tarjetaseleccionada;
    }

    public function cupostarjetas()
    {
        $session = \Session::all();
        $cupostarjetas = isset($session['CuposTarjetas']) ? $session['CuposTarjetas']['datos'] : [];
        return $cupostarjetas;
    }

    public function saludo()
    {
        $ahora = Carbon::now();
        $horario1 = Carbon::now()->hour(6)->minute(0)->second(0);
        $horario2 = Carbon::now()->hour(12)->minute(0)->second(0);
        $horario3 = Carbon::now()->hour(21)->minute(0)->second(0);

        if ($ahora->between($horario1, $horario2)) {
            $saludo = "!Buenos días";
        } elseif ($ahora->between($horario2, $horario3)) {
            $saludo = "!Buenas tardes";
        } else {
            $saludo = "!Buenas noches";
        }
        return $saludo;
    }



}
