<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Page\Entities\Page;
use Modules\Page\Entities\PageTranslation;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Repositories\PageRepository;
use DB;
use Yajra\Datatables\Datatables;
use Modules\Metadato\Entities\Metadato;
use Modules\Metadato\Entities\Tipometadato;
use Illuminate\Http\Request;
use Modules\Menu\Entities\Menuitem;
class PageController extends AdminBaseController
{
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(PageRepository $page)
    {
        parent::__construct();

        $this->page = $page;
    }

    public function index()
    {
        return view('page::admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $metadatos = Metadato::all();
        return view('page::admin.create', compact('metadatos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePageRequest $request
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $data = $request->all();
	
        $data['body'] = preg_replace(
            '/<div isDivConverter="if" ([^>]*)><\/div>/i',
            '<iframe $1></iframe>',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/<div isDivConverter="lnk" ([^>]*)><\/div>/i',
            '<link $1 />',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/<div isDivConverter="scr" ([^>]*)><\/div>/i',
            '<script $1></script>',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/papascript:/i', 
            'javascript:',
            $data['body']
        );
        
        $pagina =  $this->page->create( $data );
        $_pagina = Page::find($pagina->id);
        if($_pagina){
            if (count($request->ABC04_id) > 0) {
                $_pagina->Metadato()->sync($request->ABC04_id);
            }
        }

        return redirect()
            ->route('admin.page.page.index')
            ->withSuccess(trans('page::messages.page created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return Response
     */
    public function edit(Page $page)
    {
        $page = Page::with('metadato')
            ->find($page->id);
        
        $metadatos = Metadato::all();
        
        $metadatos_datos = [];

        if($page){
            $metadatos_datos = $page->metadato->pluck('ABC04_id');
            if( $metadatos_datos->count() > 0 ){
                $metadatos_datos = $metadatos_datos->toArray();
            }
        }

        return view('page::admin.edit', compact('page', 'metadatos', 'metadatos_datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Page $page
     * @param  UpdatePageRequest $request
     * @return Response
     */
    public function update(Page $page, UpdatePageRequest $request)
    {
        $page->fill($request->all());
        
        /*if(!$page->isDirty()){
            return redirect()->route('admin.page.page.index')
            ->withSuccess(trans('page::messages.page updated sin'));
        }*/
	
        $data = $request->all();
	
        $data['body'] = preg_replace(
            '/<div isDivConverter="if" ([^>]*)><\/div>/i',
            '<iframe $1></iframe>',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/<div isDivConverter="lnk" ([^>]*)><\/div>/i',
            '<link $1 />',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/<div isDivConverter="scr" ([^>]*)><\/div>/i',
            '<script $1></script>',
            $data['body']
        );
        $data['body'] = preg_replace(
            '/papascript:/i', 
            'javascript:',
            $data['body']
        );
	
        $this->page->update($page, $data);
        
       
        if (count($request->ABC04_id) > 0) {
            $page->Metadato()->sync($request->ABC04_id);
        }else{
			  $page->Metadato()->sync([]);
		}
        

        if ($request->get('button') === 'index') {
            return redirect()->route('admin.page.page.index')
            ->withSuccess(trans('page::messages.page updated'));
        }
        
        return redirect()->route('admin.page.page.index')
        ->withSuccess(trans('page::messages.page updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author Ruben Morales
     * @param Page $page
     * @return Response
     */
    public function destroy(Page $page)
    {
         $menu = Menuitem::where('page_id',$page->id)->get();
		 
		 if($menu->count() != 0 ){
            $json = array(
                'asignado' => true,
                'msn' => "Sr. Usuario, la pagina no se puede desactivar ya que contiene datos asociados."
            );
			return response()->json($json); 
		 }
			
        $deleted_at = $page->deleted_at;

        $success    = is_null($deleted_at)? $page->delete():$page->restore();
		
        return response()->json(array(
            'success' => $success? true:false,
            'msn' => is_null($deleted_at)? trans('page::messages.resource desactivate'):trans('page::messages.resource activate'),
            'status' => is_null($deleted_at)? true:false
        ));
    }

    /**
     * Este m�todo es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response

     */
    public function datatable()
    {
        $page = Page::select('page__page_translations.page_id as id','page__page_translations.title', 'page__page_translations.slug','page__page_translations.created_at','page__pages.deleted_at')->withTrashed()->leftJoin('page__page_translations', 'page__page_translations.page_id', '=', 'page__pages.id');

        return Datatables::of($page)
        ->addColumn('ruta_edit', function ($page) {
            return route("admin.page.page.edit",[$page->id]);
        })        
        ->addColumn('ruta_destroy', function($page){
            
            return route("admin.page.page.destroy",[$page->id]);
        })
        ->addColumn('inactivo', function ($page) {
            return is_null($page->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
