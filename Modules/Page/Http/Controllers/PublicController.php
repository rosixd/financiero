<?php

namespace Modules\Page\Http\Controllers;

use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Administracion\Entities\Region;
use Modules\Administracion\Entities\Comuna;
use Modules\Administracion\Entities\ActivdadEconomica;
use Modules\Administracion\Entities\EstadoCivil;
use Modules\Administracion\Entities\CondicionHabitacional;
use Modules\Administracion\Entities\Acreditacion;

class PublicController extends Controller
{
    public function indexPublic()
    {
        return $this->view('page::front.home.home');
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug)
    {
        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);

        if ($slug == 'requisitos-tarjeta-modal') {
            return $page->body;
        }elseif( $slug == 'bases-legales-modal' ){
            return $page->body;
        }elseif( $slug == 'ver-ganadores-modal' ){
            return $page->body;
        }

        $template = $this->getTemplateForPage($page);
        
        $view = $this->view($template, compact('page'));
        
        if ($slug == 'paga-tu-cuenta') {
            return response($view)
                ->header('X-Content-Type-Options', 'nosniff')
                ->header('X-Frame-Options', 'SAMEORIGIN')
                //->header('X-XSS-Protection', '1;mode=block')
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        }

        return $view;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage()
    {
        $page = $this->page->findHomepage();

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        return $this->view($template, compact('page'));
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug)
    {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page)
    {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }

    public function requisitostarjetaPublic()
    {
        $regiones = Region::all();
        $comunas = Comuna::select('ABC11_id', 'ABC13_codigo', 'ABC13_nombre')->get();
        $estadosciviles = EstadoCivil::all();
        $actividadeseconomicas = ActivdadEconomica::all();
        $condicioneshabitacionales = CondicionHabitacional::all();
        $acreditaciones = Acreditacion::all();

        return $this->view('page::public.quiero_mi_tarjeta', compact('regiones', 'comunas', 'estadosciviles', 'actividadeseconomicas', 'condicioneshabitacionales', 'acreditaciones' ));
    }

    public function pagonorealizadoPublic()
    {
        return $this->view('page::private.pagatucuenta.pagonorealizado');
    }
}
