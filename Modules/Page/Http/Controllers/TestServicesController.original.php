<?php

namespace Modules\Page\Http\Controllers;

use Modules\Dashboard\Http\Controllers\Api\TestServiceController;
use Illuminate\Http\Request;
use Config;
use WsSocket;
use SoapClient;
use SoapHeader;
use GuzzleHttp\Client;
use Storage;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use Symfony\Component\Debug\Exception\FatalErrorException;

class TestServicesController extends Controller
{
    public function index(Request $request, $servicio)
    {

        $no_pasar = true;

        if(isset($_GET['hash'])){
            if($_GET['hash'] === "iVLSNZihFw7FHUhXAzB0gSQPokM9Eqtn"){
                $no_pasar= false;
            }
        }else{
	  return abort('404');
	}

        if($no_pasar){
            return abort('404');
        }
        
        return $this->{$servicio}($request);
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Invocar servicio para determinar si tarjeta está bloqueada por la WEB
     * @return Response true o false
     */
    public function polaris()
    {
        $rut = '124943043';
        $NROCUENTA = '4000008277';
        $NROTARJETA = '4946110108365012';

        try {
            
            $conexion = Config::get("configSocket.processMessage");
	
            $soapvar = new \SoapVar('<input>
            <![CDATA[
                <CONSCuentaBlq_REQ_COMP_IN>
                    <ACCION>C</ACCION>
                    <RUTCLIENTE>'.$rut.'</RUTCLIENTE>
                    <NROCUENTA>'.$NROCUENTA.'</NROCUENTA>
                    <NROTARJETA>'.$NROTARJETA.'</NROTARJETA>
                    <CODPRODUCTO>24101</CODPRODUCTO>
                    <BLOQUEOS>66</BLOQUEOS>
                </CONSCuentaBlq_REQ_COMP_IN>
            ]]>
            </input>', XSD_ANYXML, null);

            $parametros = array("input" => $soapvar);

            $metodo = "processMessage";
            $respuesta = WsSocket::setConexionWebService($conexion, $parametros, $metodo, false);

            if( $respuesta['error'] ){
                return 'ERROR';
            }else{
                return 'OK';
            }
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return 'ERROR';
        }

    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Cliente ingresa a su cuenta Sitio Privado de tarjeta
     * @return Response true o false
     */
    public function processmessage()
    {
        $rut = '144541235';
        $retorno = [];
        $conexion = Config::get("configSocket.processMessage");
        $soapvar = new \SoapVar('<input>
            <![CDATA[
                <CONSCuentaBlq_REQ_COMP_IN>
                    <ACCION>C</ACCION>
                    <RUTCLIENTE>124943043</RUTCLIENTE>
                    <NROCUENTA>4000008277</NROCUENTA>
                    <NROTARJETA>4946110108365012</NROTARJETA>
                    <CODPRODUCTO>24101</CODPRODUCTO>
                    <BLOQUEOS>66</BLOQUEOS>
                </CONSCuentaBlq_REQ_COMP_IN>
            ]]>
            </input>', XSD_ANYXML, null);

        $parametros = array("input" => $soapvar);

        $metodo = "processMessage";

        $respuesta = WsSocket::setConexionWebService($conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'processMessage', 
                'processMessage', 
                '',$conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
			$retorno[] = $this->FormatSalidaWS(
                'processMessage', 
                'processMessage', 
                 '',$conexion['url'], 
                true
            );
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Datos de la persona y la tarjeta
     * @return Response true o false
     */
    public function coninfocuenta3($request)
    {
        $rut = '56467785';
       
        $conexion = Config::get("configSocket.MW-CONINFOCUENTA3");

        $conexion["trama"] = str_replace('{RUT}',$rut,$conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion );

        if( $respuesta['error'] ){
            $respuesta['estatus_cliente'] = 99;
            $retorno[] = $this->FormatSalidaMW(
                $conexion, 
                "CONINFOCUENTA3",  
                false,
                $respuesta['msg']);
            return 'ERROR';
        }else{

            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
            if(strlen($valores[1]) > 2){
                $valores[1] = substr($valores[1], 0, 2);
            }
            switch ( $valores[1] ) {
                case '00':
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", true);
                    break;
                    case '01':
                    //dd('Cliente no tiene cuenta ABCDIN');
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", true);
                    break;
                case '99':
                    //dd('Problemas de comunicación (Milliways)');
                    $retorno[] = $this->FormatSalidaMW($conexion, "CONINFOCUENTA3", false,
                    $respuesta['msg'] );
                    break;
            }
	        return 'OK';
        }

    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Metodo Safesigner para envio de msj y email
     * @return Response true o false
     */
    public function smsws()
    {
        $rut = '144541235';
        $retorno = [];
        $conexion = Config::get("configSocket.processMessage");
        $soapvar = new \SoapVar('<input>
            <![CDATA[
                <CONSCuentaBlq_REQ_COMP_IN>
                    <ACCION>C</ACCION>
                    <RUTCLIENTE>124943043</RUTCLIENTE>
                    <NROCUENTA>4000008277</NROCUENTA>
                    <NROTARJETA>4946110108365012</NROTARJETA>
                    <CODPRODUCTO>24101</CODPRODUCTO>
                    <BLOQUEOS>66</BLOQUEOS>
                </CONSCuentaBlq_REQ_COMP_IN>
            ]]>
            </input>', XSD_ANYXML, null);

        $parametros = array("input" => $soapvar);

        $metodo = "processMessage";

        $respuesta = WsSocket::setConexionWebService($conexion, $parametros, $metodo, $prueba = false, $login = false);
        if( $respuesta['error'] ){
            return 'ERROR';
        }
        return 'OK';
    }
    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Validar el estado de bloqueo de contraseña internet 
     * @return Response true o false
     */
    public function taestadobloqclienteservice()
    {
        $rut = '56467785';
        $conexion = Config::get("configSocket.WS-TaEstadoBloqClienteService");
        $parametros = array(
            'PCOD_PERSONA' => $rut,
            'PCOD_CANAL' => '1'
        ); 
        $_parametros = [ 'PCOD_PERSONA','PCOD_CANAL']; 
        
        $metodo = "TaEstadoBloqClienteService";
        
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'TaEstadoBloqClienteService', 
                'TaEstadoBloqClienteService', 
                $_parametros,$conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
			$retorno[] = $this->FormatSalidaWS(
                'TaEstadoBloqClienteService', 
                'TaEstadoBloqClienteService', 
                $_parametros,$conexion['url'], 
                true
            );
            return 'OK';
		}
       
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Recupera offset -> Se recupera offset de cliente tarjeta abierta 
     * @return Response true o false
     */
    public function newoperation()
    {
        $rut = '56467785';
        $conexion = Config::get("configSocket.WS-NewOperation");
        $parametros = array(
            'PC_COD_PERSONA' => $rut,
        );
        $_parametros = ['PC_COD_PERSONA'];
        $metodo = "NewOperation";
        
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
       
        if( $respuesta['error'] ){
           	$retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            if($respuesta['datos']['PN_COD_ERROR'] != 0){
                $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $_parametros,
                $conexion['url'], 
                false);
            }else{
                $retorno[] = $this->FormatSalidaWS(
                    'NewOperation', 
                    'NewOperation', 
                    $_parametros,
                    $conexion['url'], 
                    true
                );
            }
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Recupera offset -> Se recupera offset de cliente tarjeta cerrada
     * @return Response true o false
     */
    public function getticketeecc()
    {
        $rut = '56467785';
        $conexion = Config::get("configSocket.WS-getTicketeecc");
        $parametros = array(
            'Rut' => $rut,
        );
        $_parametros = ['Rut'];

        $metodo = "getTicketeecc";

        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'getTicketeecc', 
                'getTicketeecc', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getTicketeecc', 
                'getTicketeecc', 
                $_parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Actualiza bloqueo tarjeta IC
     * @return Response true o false
     */
    public function entretfactualizaestatusclienteservice()
    {
        $rut = '56467785';
        $conexion = Config::get("configSocket.WS-EntRetfActualizaestatusclienteService");
        
        $parametros = array(
            'PCOD_PERSONA'  => $rut,
            'PCOD_CANAL'    => 1,
            'PACCION'       => 1
        ); 
        
        $_parametros =[ 
            'PCOD_PERSONA',
            'PCOD_CANAL'  ,
            'PACCION'     
        ]; 
        
        $metodo = "TaActualizaEstatusClienteService";
        //$respuesta = self::conexionWebService($conexion, $parametros, $metodo);
        
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
		if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaWS(
                'TaActualizaEstatusClienteService', 
                'TaActualizaEstatusClienteService', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
		}else{

            $retorno[] = $this->FormatSalidaWS(
                'TaActualizaEstatusClienteService', 
                'TaActualizaEstatusClienteService', 
                $_parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 01
     * 
     * @autor Rosana Hernandez
     * Invocar servicio para recupera los datos personales de la persona autenticada
     * @return Response true o false
     */
    public function datospersona()
    {
        $rut = '144541235';
        $conexion = Config::get("configSocket.MW-datospersona");
        $conexion["trama"] = str_replace('{RUT}','97752125',$conexion["trama"]);
       
        $respuesta = WsSocket::setConexionSocket( $conexion );

        if( $respuesta['error'] ){
            $respuesta['estatus_cliente'] = 99;
            $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", false);
            return 'ERROR';
        }else{
            
            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);

            switch ($valores[1]) {
                case '00':
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", true);
                    break;
                case '01':
                    //dd('Cliente no tiene cuenta ABCDIN');
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona", true);
                    break;
                case '99':
                    //dd('Problemas de comunicación (Milliways)');
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona",  false, $respuesta['msg']);
                    break;
                default:
                    $retorno[] = $this->FormatSalidaMW($conexion, "datospersona",  false, $respuesta['msg']);
                    break;
            }
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 03 y 04
     * 
     * @autor Rosana Hernandez
     * Recupera cupo utilizado de la tarjeta
     * @return Response true o false
     */
    public function eecc_tar()
    {
        $retorno = [];
        $conexion = Config::get("configSocket.MW-EECC_TAR");
        $conexion["trama"] = str_replace('{numerotarjera}','123',$conexion["trama"]);
        $respuesta = WsSocket::setConexionSocket( $conexion, true );

        if( $respuesta['error'] ){
           $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR',  false, $respuesta['msg'] );
           return 'ERROR';
        }

        $retorno[] = $this->FormatSalidaMW($conexion, 'EECC_TAR', true);
        return 'OK';
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * Si hay Factores de Extracupo Tienda
     * @return Response true o false
     */
    public function sitioretfininterfazcalculoextracupo()
    {
        $conexion = Config::get("configSocket.WS-SitioretfinInterfazCalculoextracupo");
        
        $parametros = array(
            'RutCliente'    => '19',
            'CupoTotal'     => '200',
            'DispTienda'    => '200',
            'FactExtracupo' => '200',
            'TipoTarjeta'   => 'TC',
            'Canal'         => 'POS'
        ); 
        $_parametros = [
            'RutCliente'    ,
            'CupoTotal'     ,
            'DispTienda'    ,
            'FactExtracupo' ,
            'TipoTarjeta'   ,
            'Canal'         
        ];
        

        $metodo = "CalculateExtracupo";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);

        if( $respuesta['error'] ){
            
            $retorno[] = $this->FormatSalidaWS(
                'CalculateExtracupo', 
                'CalculateExtracupo', 
                $_parametros,$conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'CalculateExtracupo', 
                'CalculateExtracupo', 
                $_parametros,$conexion['url'], 
                true
            );
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * c) Recupera Cupo otros comercios IC
     * @return Response true o false
     */
    public function tcobtdisponible_ic()
    {
        $conexion = Config::get("configSocket.MW-TcObtDisponible-IC");
        
        $conexion["trama"] = str_replace('{COD_EMPRESA}','06', $conexion["trama"]);
        $conexion["trama"] = str_replace('{CUENTA_CLIENTE}','123',$conexion["trama"]);
        $conexion["trama"] = str_replace('{CLASIFICACION}','adc',$conexion["trama"]);

        $fecha = Carbon::now()->format('d/m/Y');
        $conexion["trama"] = str_replace('{FECHA}',$fecha, $conexion["trama"]);
        
        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
            return 'ERROR';
        }else{

            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
    
            if($valores['1'] == 99){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
            }else if($valores['1'] == 1){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC', true);
            }else if($valores['1'] == 00){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC', true);
            } 
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * Recupera Cupo otros comercios TC
     * @return Response true o false
     */
    public function tcobtdisponible_tc()
    {
        $conexion = Config::get("configSocket.MW-TcObtDisponible-TC");
        $conexion["trama"] = str_replace('{COD_EMPRESA}','06', $conexion["trama"]);
        $conexion["trama"] = str_replace('{CUENTA_CLIENTE}','123',$conexion["trama"]);
        $conexion["trama"] = str_replace('{CLASIFICACION}','adc',$conexion["trama"]);

        $fecha = Carbon::now()->format('d/m/Y');
        $conexion["trama"] = str_replace('{FECHA}',$fecha, $conexion["trama"]);
        $respuesta = WsSocket::setConexionSocket( $conexion, true );

        if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-IC',  false, $respuesta['msg']);
            return 'ERROR';
        }else{

            $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
            $valores = explode("\n", $datos);
    
            if($valores['1'] == 99){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC',  false, $respuesta['msg']);
            }else if($valores['1'] == 1){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC', true);
            }else if($valores['1'] == 00){
                $retorno[] = $this->FormatSalidaMW($conexion, 'TcObtDisponible-TC', true);
            } 
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * Si es IC, recupera Extracupo para Avance
     * @return Response true o false
     */
    public function obtenerduplica()
    {
        $conexion = Config::get("configSocket.MW-OBTENERDUPLICA");
        //'trama' => "TEXETTC.TC_INTERFAZPOS3.OBTENERDUPLICA('{empresa}','{cuenta}','{comercio}','')"
        
        $conexion["trama"] = str_replace('{empresa}', str_pad('65', 4, "0", STR_PAD_LEFT) ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{cuenta}',  '123', $conexion["trama"]);
        $conexion["trama"] = str_replace('{comercio}','648', $conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion, true );

        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;

        if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA',   false,
                $respuesta['msg']);
            return 'ERROR';
        }

        $respuesta['codigo'] = substr($respuesta['datos'][0], -2);

        switch (substr($respuesta['datos'][0], -2)) {
            case '99':
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA', true);
                break;
            default:
                $retorno[] = $this->FormatSalidaMW($conexion, 'OBTENERDUPLICA', true);
                break;
        }
        return 'OK';
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * Para Tarjeta seleccionada, Se obtiene valores adeudados IC
     * @return Response true o false
     */
    public function totobligdia5_ic()
    {
        $conexion = Config::get("configSocket.MW-totobligdia5-IC");
        
        $conexion["trama"] = str_replace('{codEmpresa}', '056' ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{NroTarjeta}',  '1234', $conexion["trama"]);

        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );


        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC',  false,
                $respuesta['msg']);
            return 'ERROR';
        }
        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;
        
        $cod_respuesta = substr($respuesta["datos"][0], -3);
     
        switch ($cod_respuesta) {
            case '99':
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC', true);
                break;
            default:
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-IC', true);
                break;
        }
        return 'OK';
    }

    /**
     * Devuelve informacion del requerimiento 03
     * 
     * @autor Rosana Hernandez
     * Para Tarjeta seleccionada, Se obtiene valores adeudados TC
     * @return Response true o false
     */
    public function totobligdia5_tc()
    {
        $conexion = Config::get("configSocket.MW-totobligdia5-TC");
         
        $conexion["trama"] = str_replace('{codEmpresa}', '056' ,$conexion["trama"]);
        $conexion["trama"] = str_replace('{NroTarjeta}',  '1234', $conexion["trama"]);

        //$respuesta = self::conexionSocket( $conexion );
        $respuesta = WsSocket::setConexionSocket( $conexion, true );


        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC',  false,
                $respuesta['msg']);
            return 'ERROR';
        }

        $datos = preg_replace('/\\r/', "",$respuesta['original-data']);
        $valores = explode("\n", $datos);
        $respuesta['datos'] = $valores;
        
        $cod_respuesta = substr($respuesta["datos"][0], -3);
     
        switch ($cod_respuesta) {
            case '99':
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC',  false,
                $respuesta['msg']);
                break;
            case '01':
                $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC', true);
                break;
            default:
               $retorno[] = $this->FormatSalidaMW($conexion, 'totobligdia5-TC', true);
                break;
        }
        return 'OK';
    }

    /**
     * Devuelve informacion del requerimiento 05
     * 
     * @autor Rosana Hernandez
     * Para Tarjeta seleccionada, Crear ticket para llamado de servicio
     * @return Response true o false
     */
    public function sitioretfininterfazcreaticketeecc()
    {
        $retorno = [];
        $conexion = Config::get("configSocket.WS-SitioretfinInterfazCreaticketeecc");
        $rut = '144541235';
        $userChannel = 'ABC';
    
        $parametros = array(
            'userId' => $rut,
            'userChannel' => $userChannel,
        );
    
        $_parametros = ['userId','userChannel'];
                
        $metodo = "createTicketByRut";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);

        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'createTicketByRut', 
                'createTicketByRut', 
                $_parametros,
                $conexion['url'], 
                 false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'createTicketByRut', 
                'createTicketByRut', 
                $_parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        }    
    }

    /**
     * Devuelve informacion del requerimiento 05
     * 
     * @autor Rosana Hernandez
     * Para tarjeta seleccionada recupera Próximos vencimientos para él cliente
     * @return Response true o false
     */
    public function consultaflujoeecc()
    {
        $conexion = Config::get("configSocket.MW-ConsultaFlujoEECC");
        $conexion["trama"] = str_replace('{EmpresaId}','1',$conexion["trama"]);
        $conexion["trama"] = str_replace('{NumCuenta}','4000000001',$conexion["trama"]);
        
        $conexion["trama"] = str_replace('{PrimerVcto}','20/01/2018',$conexion["trama"]);
        $conexion["trama"] = str_replace('{Plazo}','0',$conexion["trama"]);
        $conexion["trama"] = str_replace('{ValorCuota}','0',$conexion["trama"]);

        $respuesta = WsSocket::setConexionSocket( $conexion, true );
       
        if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaMW($conexion, 'ConsultaFlujoEECC', false, $respuesta['msg']);
            return 'ERROR';
        }else{

            $retorno[] = $this->FormatSalidaMW($conexion, 'ConsultaFlujoEECC', true, $respuesta['msg']);
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 09
     * 
     * @autor Rosana Hernandez
     * Se invoca servicio pre-validación abcdin:
     * @return Response true o false
     */
    public function esclientevalidoweb()
    {
        $retorno = [];
        
        $conexion = Config::get("configSocket.MW-EsClienteValidoWeb");
        $conexion["trama"] = str_replace('{PCODPERSONA}','99690577', $conexion["trama"]);
        $respuesta = WsSocket::setConexionSocket( $conexion, true );
        
        if( $respuesta['error'] ){

            $retorno[] =  $this->FormatSalidaMW($conexion, 'EsClienteValidoWeb', false);
            return 'ERROR';
        }else{

            $retorno[] = $this->FormatSalidaMW($conexion, 'EsClienteValidoWeb', true);
            return 'OK';
        }
    }

    /**
     * Devuelve informacion del requerimiento 09
     * 
     * @autor Rosana Hernandez
     * Se invoca servicio de evaluación express en abcdin
     * @return Response true o false
     */
    public function tc2000_evaluacionexpress()
    {
        $conexion = Config::get("configSocket.MW-EsClienteValidoWeb");
        $conexion["trama"] = str_replace('{PCODPERSONA}','99690577', $conexion["trama"]);

        $respuesta =  WsSocket::TC2000EvaluacionExpressPrueba();

        if( $respuesta['error'] ){

            $retorno[] =  $this->FormatSalidaMW($conexion, 'TC2000', false);
            return 'ERROR';
        }else{

            $retorno[] = $this->FormatSalidaMW($conexion, 'TC2000', true);
            return 'OK';
        }   
    }

    /**
     * Devuelve informacion del requerimiento 09
     * 
     * @autor Rosana Hernandez
     * Se invoca servicio Equifax eIDverifier
     * @return Response true o false
     */
    public function idverifier()
    {
        $rut = '' ;
        $numSerie = '';
        try {
            
            $conexion = Config::get("configSocket.ws-IDverifier-pro");
  

            $parametros = array(
                'Identity' => [
                    'RUT'   => $rut,
                    'IDCardSerialNumber' => $numSerie
                ],
                'ProcessingOptions'=> [
                    'Language'=> 'ES',
                    'EnvironmentOverride' => 'PROD'
                ]
            ); 
            
            $metodo = "startTransaction";

            //$archivo =  public_path('wsdl/wsdl-xsd-des/eidverifier.wsdl');
            //$conexion['url'] = $archivo;
        
            //$conexion['url'] = "http://abcdin.desarrollo.netred.cl/uru/soap-latam/ut/70/chile?WSDL";

            $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$conexion['url']."'";
            $respuesta = shell_exec($prueba);
          
            if( $respuesta != 200){
                return 'ERROR';
            }
   
            return 'OK';

            
        } catch (\Exception $e) {

            return 'ERROR';
        }

    }

    /**
     * Devuelve informacion del requerimiento 11
     * 
     * @autor Rosana Hernandez
     * Entrega el mail del EECC de un Cliente
     * @return Response true o false
     */
    public function getmaileecc()
    {
        $rut = '119484626';
        $conexion = Config::get("configSocket.WS-getMaileecc");

        $parametros = array(
            'codPersona' => $rut
        ); 
        $_parametros = [ 
            'codPersona'
        ];

        $metodo = "getMaileecc";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
      
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'getMaileecc', 
                'getMaileecc', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getMaileecc', 
                'getMaileecc', 
                $parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        }    
    }

    /**
     * Devuelve informacion del requerimiento 11
     * 
     * @autor Rosana Hernandez
     * Se recupera datos legales
     * @return Response true o false
     */
    public function getdatoslegal()
    {
        $rut = '119484626';
        $conexion = Config::get("configSocket.wS-getDatosLegal");
        $parametros = [
            'codPersona' => $rut
        ]; 
        
        $metodo = "getDatosLegal";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, false, false, $conexion["opciones"] );
        
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'getDatosLegal', 
                'getDatosLegal', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'getDatosLegal', 
                'getDatosLegal', 
                $parametros,
                $conexion['url'], 
                true,
                $respuesta
            );
            return 'OK';
        }   
    }

    /**
     * Devuelve informacion del requerimiento 11
     * 
     * @autor Rosana Hernandez
     * Se invoca servicio de actualización de datos de contacto para cliente IC
     * @return Response true o false
     */
    public function actualizaclienteic()
    {
        $retorno = [];
        $rut = '119484626';
        $parametros = array(
            'codPersona' => $rut
        ); 

        $_parametros = [ 
            'codPersona'
        ];

        $conexion = Config::get("configSocket.wS-ActualizaClienteIC");
		
        $parametros = array(
            'updateClienteICRequest' => $this->Datos()
        ); 
        
        $metodo = "updateClienteIC";
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);

        if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaWS(
                'updateClienteIC', 
                'updateClienteIC', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'updateClienteIC', 
                'updateClienteIC', 
                $_parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        } 
    }

    /**
     * Devuelve informacion del requerimiento 11
     * 
     * @autor Rosana Hernandez
     * Se invoca servicio de actualización de mail
     * @return Response true o false
     */
    public function mergemaileecc()
    {
        $retorno = [];
        $rut = '119484626';
        $parametros = array(
            'codPersona' => $rut
        ); 

        $_parametros = [ 
            'codPersona'
        ];
        $conexion = Config::get("configSocket.wS-mergeMaileecc");
		
        $parametros = array(
            'PCODPERSONA'   => $rut,
            'PEMAIL'        => '',
            'PINDEECCEMAIL' => '',
            'PESLEGAL'      => '',
            'PINCLUIDOPOR'  => '',
        ); 
        
        $metodo = "mergeMaileecc";

        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo, $prueba = false, $login = false);
        
        if( $respuesta['error'] ){
            $retorno[] = $this->FormatSalidaWS(
                'mergeMaileecc', 
                'mergeMaileecc', 
                $_parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{
            $retorno[] = $this->FormatSalidaWS(
                'mergeMaileecc', 
                'mergeMaileecc', 
                $_parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        } 
    }

    /**
     * Devuelve informacion del requerimiento 13
     * 
     * @autor Rosana Hernandez
     * El WS implementado en Weblogic de ABCDin, para la acción de bloquear o desbloquear la tarjeta
     * @return Response true o false
     */
    public function bloqueodes()
    {
        $rut = '144541235';
        $retorno = [];
        $conexion = Config::get("configSocket.ws-Bloqueodes");

        $parametros = array(
            'RUTCLIENTE'    => $rut,
            //'NROCUENTA'     => $respuesta["datos"]['tarjetas']['5000']["Cuenta"],
            'NROCUENTA'     => '0',
            //'NROTARJETA'    => $respuesta["datos"]['tarjetas']['5000']["NumeroTarjeta"],
            'NROTARJETA'    => '0',
            'CODPRODUCTO'   => '00001',
            'ACCION'        => 'B',
            'CODIGOBLOQUEO' => '66',
            'FECHABLOQUEO'  => date("d-m-Y H:m"),
            'CODSUCURSAL'   => '601',
            'RUTUSUARIOACT' => $rut,
        ); 
        
        $metodo = "NewOperation";
        $respuesta = WsSocket::setConexionWebService( $conexion, $parametros, $metodo);
      
        if( $respuesta['error'] ){

            $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $parametros,
                $conexion['url'], 
                false,
                $respuesta['msg']
            );
            return 'ERROR';
        }else{

            $retorno[] = $this->FormatSalidaWS(
                'NewOperation', 
                'NewOperation', 
                $parametros,
                $conexion['url'], 
                true
            );
            return 'OK';
        }    
    }

    
    /** 
     * Prueba de Trazabilidad
     * @autor Rosana Hernandez y Miguelangel Gutierrez
     * El servicio rest que envia la traza de fallos y exitos.
     * @return Response true o false
    */
    public function trazabilidad()
    {
        error_reporting(E_ALL);
        try {
            $user_agent = $_SERVER['HTTP_USER_AGENT'];          
            $conexion = Config::get("configSocket.Trazabilidad");            
            $agent = new Agent();
            $browser = $agent->browser();
            
            $datos = [
                "rut"                  => '', 
                "idSesion"             => '',
                "IP"                   => '', 
                "infoBrowser"          => '', 
                "infoSO"               => '', 
                "numTelefono"          => '', 
                "cdCanal"              => $conexion['cdCanal'],
                "cdAccion"             => '', 
                "cdTienda"             => $conexion['CdTienda'], 
                "cdTransaccion"        => '', 
                "deTransaccion"        => '', 
                "cdAplicacion"         => $conexion['cdAplicacion'],
                "cdError"              => '', 
                "entradaTransaccion"   => '', 
                "salidaTransaccion"    => '', 
            ];
        
            $respuesta ="";
            //$respuesta = self::conexionCurlJSON($conexion['url'], $datos);
           
            $respuesta = json_decode($respuesta, true);
            $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$conexion['url']."'";
            $respuesta = shell_exec($prueba);
            
            if( $respuesta != 200){
                return 'ERROR';
            }
   
            return 'OK';
            
           
        } catch (\Exception $e) {
           
  
            return 'ERROR';
        }
        
    }

    public function pagatucuenta()
    {
        
        //NOTA: se debo tener datos reales de un usuario para poder realizar las pruebas estos mismos cambian entre Produccion a QA o desarrollo
        $rut = '106348235';
       
        $NROTARJETA = '4946110100002084';

        
        $rut  = WsSocket::aesExec('Encriptar', $rut);
        
        $TipoTarjeta = 'IC';
       
        $tipo_tarjeta = WsSocket::aesExec('Encriptar',$TipoTarjeta);

        $conexion = Config::get("configSocket.pagatucuenta");

        $num_tarjeta  = WsSocket::aesExec('Encriptar',  $NROTARJETA);
        
        $url_base = $conexion['url']."?R=".$rut."&T=".$tipo_tarjeta."&N=".$num_tarjeta;
        
       
        //$respuesta = $this->conexionCurlJSON($url_base);
        $prueba=  "curl -s -o /dev/null -w '%{http_code}' '".$url_base."'";
        $respuesta = shell_exec($prueba);
       
        if( $respuesta != 200){
            return 'ERROR';
        }
   
        return 'OK';
    }

    public function csi()
    {
        $TipoTarjeta = 'IC';
        $password = '1234';
        $offset = '1234FFFFFFFsss';
        $rut = '119484626';

        $data = [
            'pan'             => '',
            'ctype'           => 'CC',
            'ctext'           => $password . $offset,
            'interface_id'    => 'XMLCSI',
            'customer_id'     => Config::get("configSocket.CSI.usuario"),
            'client_ip'       => Config::get("configSocket.CSI.ip"),
        ];

        // VALPINCDI el parámetro pan debe ser el rut 
        // VALPIN el parámetro pan debe ser la tarjeta
        if ($TipoTarjeta == 'TC') {
            $servicio = 'VALPIN';
            $data['pan'] = $tarjeta;
        } else {
            $servicio = 'VALPINCDI';
            $data['pan'] = substr($rut, 0, -1);
        }

        $dataXml = WsSocket::csiEncryptArrayToXml($data);

        $xml =  "<request_auth>
            <service_id>" . $servicio . "</service_id>
            <subtype_service>00" . Config::get("configSocket.CSI.llave") . "</subtype_service>
            " . $dataXml . "</request_auth>";

        $respuesta = $this->conexionCurlXml(Config::get("configSocket.CSI.url"), $xml);
        
        
        if( strlen( $respuesta ) > 10 ){
            return 'OK';
        }
        return 'ERROR';
    }

    protected function FormatSalidaMW($conexion, $nombre, $estatus, $msg = ''){
		return [
            'NombreServicio' => $nombre,
            'TipoServicio'   => 'MW',
            'Host'           => $conexion['host'],
            'Puerto'         => $conexion['puerto'],
            'Trama'          => $conexion['trama'],
            'Estatus'        => $estatus,
            'Mensaje'        => $msg
        ];
    }

    protected function FormatSalidaWS($nombre, $metodos, $parametros, $url, $estatus, $msg = '' ){
		return [
            'NombreServicio' => $nombre,
            'TipoServicio'   => 'WS',
            'Metodo'         => $metodos ,
            'Parametros'     => $parametros,
            'Url'            => $url,
            'Estatus'        => $estatus,
            'Mensaje'        => $msg
        ];
    }

    protected function Datos(){
        return $data = [
            'RUT_CLIENTE'                   => '203542361', //REQ
            'EST_CIVIL'                     => 'm',
            'SEXO'                          => 'prueba',
            'FEC_NACIMIENTO'                => '-',
            'PRIMER_APELLIDO'               => '-',//REQ
            'SEGUNDO_APELLIDO'              => '-', 
            'PRIMER_NOMBRE'                 => '-', 
            'SEGUNDO_NOMBRE'                => '-',
            
            'PROFESION'                     => '-',
            'CONYUGUE'                      => '-',
            'NACIONALIDAD'                  => '-',
            'EMAIL_USUARIO'                 => '-',
            'EMAIL_SERVIDOR'                => '-',

            'NIVEL_ESTUDIOS'                => '-',
            'TIPO_VIVIENDA'                 => '-',
            'NUM_HIJOS'                     => '-',
            'NUM_DEPENDIENTES'              => '-',
            'ES_RESIDENTE'                  => '-',
            'TIEMPO_VIVIEN_ACT'             => '-',
            'TOTAL_INGRESOS'                => '-',
            'COD_PAIS'                      => '-',
            'SCORING'                       => '-',
            'ACTIVIDAD'                     => '-',
            'TIPO_SOC_CONYUGAL'             => '-',
            'NOM_CONYUGUE'                  => '-',
            'FEC_MATRIMONIO'                => '-',
            'IND_SOL_PATRIMONIO'            => '-',
            'IND_EECC_EMAIL'                => '--',//REQ
            'COD_DIRECCION'                 => '-01',//REQ
            'TIP_DIRECCION'                 => '-01',//REQ
            'COD_POSTAL'                    => '-',
            'DETALLE'                       => '-',
            'COD_PAIS_CNT'                  => '-',
            'COD_REGION'                    => '-',
            'COD_COMUNA'                    => '-',
            'CALLE'                         => '-',
            'NUMERO'                        => '-', 
            'ES_DIR_EECC'                   => '--',
            'IND_ACCION_BLOQ_DIR'           => '-',
            'MOTIVO_RECHZ'                  => '-',
            'IND_ESTADO'                    => '--',
            'COD_FUENTE'                    => '-',
            'COD_AREA'                      => '-',
            'NUM_TELEFONO'                  => '-',
            'TIP_TELEFONO'                  => '-',
            'TEL_UBICACION'                 => '-',
            'EXTENSION'                     => '-',
            'ES_DEFAULT'                    => '-',
            'INCLUIDO_POR'                  => '-',
            'FEC_INCLUSION'                 => '-',
            'MODIFICADO_POR'                => '-',
            'FEC_MODIFICACION'              => '-',
            'TEL_ACTIVO'                    => '-',
            'COD_FUENTE_FONO'               => '-',
        ];
    }

    protected  function conexionCurlJSON( $url, $datos = "")
    {
        //iniciamos curl
        $ch = curl_init();
        
        //seteamos la url
        curl_setopt($ch, CURLOPT_URL, $url);
    
        //seteamos los datos post asociados a la variable xmlData
    
        if(is_array($datos)){
            $json = json_encode($datos);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        }
            
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        //tiempo de espera, 0 infinito
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        
        //ejecutamos la petición y asignamos el resultado a $data

        $data = curl_exec($ch);
        
        //cerramos la petición curl
        curl_close($ch);

        
        return $data;
    }

    public  function log($arr)
    {
        if (is_array($arr)) {
            $arr = json_encode($arr, JSON_PRETTY_PRINT);
        }

        \Log::debug($arr);
        return;
    }

    protected  function conexionCurlXml($url, $xml)
    {
        //iniciamos curl
        $ch = curl_init();
        
        //seteamos la url
        curl_setopt($ch, CURLOPT_URL, $url);
       
        //seteamos los datos post asociados a la variable xmlData
        $xml = trim(preg_replace("/\s+/", " ", $xml));
		$xml = "<?xml version='1.0' encoding='ISO-8859-1'?>\n" . 
				str_replace("> <", "><", $xml);
    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
     
            
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        //tiempo de espera, 0 infinito
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
         
        //ejecutamos la petición y asignamos el resultado a $data
   
        $data = curl_exec($ch);
        
        //cerramos la petición curl
        curl_close($ch);

        self::log([
            'url'  => $url,
            'request'  => $xml,
            'response' => $data
        ]);
		
        return $data;
    }
}
