<?php

namespace Modules\Page\Http\Controllers;

use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use GuzzleHttp\Exception\GuzzleException; 
use GuzzleHttp\Client;
use Modules\Administracion\Entities\Region;
use Modules\Administracion\Entities\Comuna;
use Modules\Administracion\Entities\CodigoArea;

class PrivateController extends Controller
{

    public function indexPrivate()
    {
/*         if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'movimientos';
            return $this->view('page::private.movimientos.index');
        }
        return $this->indexPrivate(); */
	    return $this->view('page::private.index');
    }

    protected function tipoTarjeta()
    {

        $session = \Session::all();
        $tipo = $session['TarjetaSeleccionada']['OrigenDeLosDatos'];
        return $tipo;
    }

    public function actualizadatosPrivate()
    {
        $region = Region::all();
        $comuna = Comuna::all();
        $codigoarea = CodigoArea::all();
        $controller = app('Modules\User\Http\Controllers\PublicPrivate\ActualizacionDatosController');
        $datos = $controller->index();
        $direccionfacturacion = [];
        $datoscontacto = [];
        $getmaileecc = [];

        if( $datos['error'] === false ){
            if( array_key_exists('DireccionFacturacion', $datos['datos']) ){
                $direccionfacturacion = $datos['datos']['DireccionFacturacion'];
            }
    
            if( array_key_exists('DatosContacto', $datos['datos']) ){
                $datoscontacto = $datos['datos']['DatosContacto'];
            }
    
            if( array_key_exists('getMaileecc', $datos['datos']) ){
                $getmaileecc = $datos['datos']['getMaileecc'];
            }
			 if( array_key_exists('DatosLegal', $datos['datos']) ){
                $DatosLegal = $datos['datos']['DatosLegal']['datos'];
				
            }
        }
        
        if ($this->tipoTarjeta() == 'IC') {

            return $this->view( 'page::private.micuenta.index', compact('region', 'DatosLegal','comuna', 'codigoarea', 'direccionfacturacion', 'datoscontacto', 'getmaileecc', 'datos') );
        }
        return $this->indexPrivate();
	    
    }

    public function movimientosPrivate()
    {
        if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'movimientos';
            return $this->view('page::private.movimientos.index');
        }

        if ($this->tipoTarjeta() == 'TC') {

            $this->activarmenu = 'avance';
            return $this->view('page::private.movimientos.index');
        }
        
        /* return $this->indexPrivate(); */
    }
    
    public function estadosdecuentaPrivate()
    {   
        $controller = app('Modules\User\Http\Controllers\PublicPrivate\EstadoCuentaController');
	    $url = $controller->EstadoCuenta();
        
        $client = new Client (); 
        $request = $client->get($url['url']);
        $response = $request->getBody()->getContents();
        
        //fechas de Vencimiento
        preg_match_all('/<td class="ResultadoDetalleABC" align="center">([\d]{2}\s[\w]{3}\s[\d]{4})+<\/td>/i', $response, $match);
        $vencimientos = $match[1];
        //$data['vencimientos'] = $match[1];

        //Tarjeta
        preg_match_all('/<td class="ResultadoDetalleABC" align="center">([\w\-]+)<\/td>/i', $response, $match);
	    $tarjeta = $match[1];
	
        //pdf nombre del archivo
        /* preg_match_all('/<td class="ResultadoDetalleABC" align="center"><A href=\'javascript:verDoc\("([\w\-\.\d]+)"\);\'>/i', $response, $match); */
        preg_match_all('/<td class="ResultadoDetalleABC" align="center"><A href=\'javascript:(verDoc\("([\w\-\.\d]+)"\)|MostrarModal\(()\))?;\'>/i', $response, $match);
        $nombre_archivo = $match[2];
        //$data['nombre_archivo'] = $match[1];

	    //parametro T
        preg_match('/<input name="T" type="hidden" id="T" value="([\w\d]+)" \/>/i', $response, $match);
	    $valorT= $match[1];
	
        //parametro R
        preg_match('/<input id="R" type="hidden" value="([\w\d]*)" name="R" \/>/i', $response, $match);
	    $valorR= $match[1];
	
        //parametro V
        preg_match('/<input name="V" type="hidden" id="V" value="([\w\d]*)" \/>/i', $response, $match);
	    $valorV= $match[1];
	
        //parametro HayDetalle
        preg_match('/<input name="HayDetalle" type="hidden" id="HayDetalle" value="([\w\d]*)" \/>/i', $response, $match);
	    $valorhaydetalle= $match[1];

        $data = [];

        if( count( $nombre_archivo) >= count( $vencimientos ) ){
            foreach($vencimientos as $key => $vencimiento){
                $data[$key] = [
                        'vencimiento' => $vencimiento,
                        'nombre_archivo' => $nombre_archivo[$key]
                    ];
            }
        }
       
        $this->activarmenu = 'estado';
        return $this
            ->view('page::private.estado_cuenta.index', compact('data', 'valorT', 'valorR', 'valorV', 'valorhaydetalle', 'url') );

    }
    
    public function vencimientosPrivate()
    {
        if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'vencimientos';
            return $this->view('page::private.proximos_vencimientos.index');
        }
        return $this->indexPrivate();

    }

    public function avancePrivate()
    {
        if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'avance';
            return $this->view('page::private.proximos_vencimientos.index');
        }
        return $this->indexPrivate();

    }

    public function misofertasPrivate()
    {
        if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'ofertas';
            return $this->view('page::private.mis_ofertas.index');
        }
        return $this->indexPrivate();

    }

    public function pagatucuentaPrivate()
    {
        $controller = app('Modules\User\Http\Controllers\PublicPrivate\PagaTuCuentaController');
        $datos = $controller->index();
	    $url = '';
        
        if( $datos['error'] == false &&  array_key_exists('url', $datos['data']) ){
            $url = $datos['data'];
        }

        $this->activarmenu = 'avance';

        if ($this->tipoTarjeta() == 'IC') {

            $this->activarmenu = 'movimientos';
        }
        return $this->view('page::private.pagatucuenta.index', compact('url'));
        /* return $this->indexPrivate(); */

    }

}
