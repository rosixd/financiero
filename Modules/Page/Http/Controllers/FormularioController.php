<?php

namespace Modules\Page\Http\Controllers;

use Carbon\Carbon;
use DB;
use Modules\Formularios\Entities\FormularioContacto;
use Modules\Page\Http\Requests\FormularioRequest;

class FormularioController extends Controller
{
    public function guardar(FormularioRequest $request)
    {
        DB::beginTransaction();
        try {
            $crea_registro = Carbon::now();
            $data = $request->all();
            unset($data['formulario']);
             $contacto = FormularioContacto::create([

                'ABC30_nombre_formulario'       => $request->formulario,
                'ABC30_nombre_formulario_slug'  => str_slug($request->formulario),
                'ABC30_nombrecompleto'          => $request->nombrecompleto,
                'ABC30_rut'                     => $request->rut,
                'ABC30_telefono'                => $request->telefono,
                'ABC30_email'                   => $request->email,
                'ABC30_tiposeguro'              => $request->tiposeguro,
                'ABC30_tiposolicitud'           => $request->tiposolicitud,
                'ABC30_mensaje'                 => $request->mensaje,
                'ABC30_fecha_creacion'          => $crea_registro,
            ]);
            
            $email =[];
            switch (strtolower($request->tiposolicitud)) {
                case 'consulta':
                    $email[] = "gestion.deincidentes@abcdin.cl";
                    break;
                case 'reclamo':
                    $email[] = "gestion.deincidentes@abcdin.cl";
                    break;
                case 'masinfo':
                    //$email[] = "gestion.deincidentes@abcdin.cl";
                    switch ($request->tiposeguro) {
                        case '01':
                            $email[] = "pia.ordonez@abcdin.cl";
                            break;
                        case '02':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '03':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '04':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '05':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '06':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '07':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                        case '08':
                            $email[] = "Kristy.kohler@abcdin.cl";
                            break;
                    }
                    $email[] = "Consultas.Seguros@abcdin.cl";
                    break;
            }


            if ( !empty($email) ) {
                $this->EnvioEmail($email, $contacto);
            }
        } catch (Exception $e) {
            DB::rollback();

            return [
                's' => 'n',
            ];
        }

        DB::commit();

        return [
            's' => 's',
        ];
    }

    protected function EnvioEmail($email = "", $contacto = "")
    {
        try {
            $datos = $contacto;
            if ($contacto != "") {
                $datos = $contacto;
                \Mail::send('emails.Contacto', [
                    'datos' => $datos,
                ], function ($message) use ($datos, $email) {
                    $message->subject("Contacto");
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    foreach( $email as $emails ){
                        $message->to($emails);
                    }
                });
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
