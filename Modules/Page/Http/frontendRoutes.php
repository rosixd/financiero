<?php

use Illuminate\Routing\Router;
use Modules\Page\Entities\Page;
use Modules\Page\Entities\PageTranslation;

/** @var Router $router **/
if (! App::runningInConsole()) {
/*
    $segments = Request::segments();
    $slug_url = implode('/', $segments);

    $pagina = PageTranslation::where('slug', $slug_url)->first();
    //dd($pagina);
    if ($pagina) {
        $router->get($slug_url, function () use ($pagina) {
            return $this->view('page::front.front', ['page' => $pagina]);
        });
    }*/
    /**/


    /* RH esto es para pagina principal */
     $router->get('/', 'PublicController@homepage')->name('homepage'); 

    /*
    $router->get('/', function() {
        return view('page::front.home.home');
    })->name('homepage');
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
        'middleware' => config('asgard.page.config.middleware'),
    ]);
    */

    //Urls para consultar los servicios y miliways
    $router->get('testService/{servicio}', [
        'uses' => '\Modules\Page\Http\Controllers\TestServicesController@index',
        'as'   => 'Public.testService.test'
    ]);
    
    /*$router->get('ftp',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\FormulariosController@index',
        'as'   => 'Public.private.logiguradar'
    ]);
    $router->get('/prueba', [
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@index',
        'as' => 'ligunn',
    ]);*/

    $router->get('quiero-tarjeta-modal',[
        'uses' => '\Modules\Page\Http\Controllers\PublicController@requisitostarjetaPublic',
        'as'   => 'Public.quierotarjetamodal'
    ]);

    $router->get('pago-no-realizado-modal',[
        'uses' => '\Modules\Page\Http\Controllers\PublicController@pagonorealizadoPublic',
        'as'   => 'Public.pagonorealizadomodal'
    ]);

    /**
   	 * MA: Grupo de rutas de Public-Private
   	*/
    $router->group(['middleware' => 'PublicPrivate', 'prefix' => ''], function(Router $ruta) {

        /************RH ruta privado tc e ic FRONT******/
        $ruta->get('private/', [
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@indexPrivate',
            'as'   => 'Public.private'
        ]);
        $ruta->get('private/renovar', [
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@renovar',
            'as'   => 'Public.private.renovar'
        ]);

        $ruta->get('private/actualiza-datos',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@actualizadatosPrivate',
            'as'   => 'Public.private.actualizadatos',
            'modulo' => 'micuenta',
            'script' => 'appMicuenta'
        ]);

        $ruta->get('private/movimientos',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@movimientosPrivate',
            'as'   => 'Public.private.ic.movimientos',
            'modulo' => 'movimientos',
            'script' => 'appMovimientos'
        ]);

        $ruta->get('private/estados-de-cuenta',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@estadosdecuentaPrivate',
            'as'   => 'Public.private.estadosdecuenta',
            'modulo' => 'estado_cuenta',
            'script' => 'appEstadoCuenta'
        ]); 

        $ruta->get('private/proximos-vencimientos',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@vencimientosPrivate',
            'as'   => 'Public.private.ic.vencimientos',
            'modulo' => 'vencimientos',
            'script' => 'appVencimientos'
        ]);

        $ruta->get('private/mis-ofertas',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@misofertasPrivate',
            'as'   => 'Public.private.misofertas'
        ]);

        $ruta->get('private/paga-tu-cuenta',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@pagatucuentaPrivate',
            'as'   => 'Public.private.pagatucuenta'
        ]);

        $ruta->get('private/pagar-cuenta',[
            'uses' => '\Modules\Page\Http\Controllers\PrivateController@movimientosPrivate',
            'as'   => 'Public.private.pagarcuenta'
        ]);

        /* --------------------------MG RUTAS WS--------------------------- */
        $ruta->get('private/actualizadatos',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\ActualizacionDatosController@index',
            'as'   => 'Public.private.ic.actualizadatos',
            //'modulo' => 'actualizadatos',
            //'script' => ''
        ]);

        $ruta->get('private/buscarcupos',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@CupoTarjeta',
            'as'   => 'Public.private.ic.buscarcupos',
            //'modulo' => 'actualizadatos',
            //'script' => ''
        ]);
        
    	$ruta->get('private/pagatucuenta',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\PagaTuCuentaController@index',
            'as'   => 'Public.private.pagacuenta',
            //'modulo' => 'actualizadatos',
            //'script' => ''
        ]);
        
        

        $ruta->get('private/actualizadatos/validar',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\ActualizacionDatosController@validar',
            'as'   => 'Public.private.ic.actualizadatos.validar',
            //'modulo' => 'actualizadatos',
            //'script' => ''
        ]);

        $ruta->post('private/actualizadatos/guardar',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\ActualizacionDatosController@guardar',
            'as'   => 'Public.private.ic.actualizadatos.guardar',
            //'modulo' => 'actualizadatos',
            //'script' => ''
        ]);

        $ruta->get('private/bloqueodesbloqueo',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\BloqueoTarjetaController@index',
            'as'   => 'Public.private.ic.bloqueodesbloqueo',
        ]);
     
        $ruta->get('private/movimientos-no-facturados',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\MovimientosNoFacturadosController@index',
            'as'   => 'Public.private.cuposnofacturados'
        ]);
        $ruta->get('private/descargar-movimientos-no-facturados',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\MovimientosNoFacturadosController@generarExcel',
            'as'   => 'Public.private.descargarcuposnofacturados'
        ]);
        
        $ruta->get('private/estado-de-cuenta',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\EstadoCuentaController@EstadoCuenta',
            'as'   => 'Public.private.estadocuenta'
        ]);

        $ruta->get('private/vencimientos',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\EstadoCuentaController@vencimientos',
            'as'   => 'Public.private.vencimientos'
        ]);
        
        $ruta->get('private/preferencias-cuenta',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\PreferenciasCuentaController@index',
            'as'   => 'Public.private.preferenciascuenta'
        ]);

        $ruta->get('private/logout',[
            'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@logout',
            'as'   => 'Public.private.logout'
        ]);
     
    });

    //Url de test de conexion socket
    $router->get("testSocket", function() {
        $offset   = "";
        $rut      = "143482049"; // 14.348.204-9
        $password = "5103";

        $csi = WsSocket::CSI($offset, $rut, $password);
        dd($csi);
    });

    $router->post('formulario/guardar',[
        'uses' => '\Modules\Page\Http\Controllers\FormularioController@guardar',
        'as'   => 'Public.formulario.guardar',
        //'modulo' => 'actualizadatos',
        //'script' => ''
    ]);
  
    //Url de test de conexion socket
    $router->get("comando", function() {
        dd(\Artisan::call('cache:clear'));
    });
    
    //Url para borrar las caches
    $router->get("borraCache", function() {
        
        //dd( \Storage::disk("framework")->directories("cache") );
        //Eliminando archivos
        $dirCaches = \Storage::disk("framework")->directories("cache");
        if( $dirCaches && count( $dirCaches ) > 0 ){
            foreach( $dirCaches as $dirCache ){
                \Storage::disk("framework")
                    ->deleteDirectory( $dirCache );
            }
        }

        //Eliminando los archivos de views
        $archivosViews = \Storage::disk("framework")->files("views");
        if( $archivosViews && count( $archivosViews ) > 0 ){    
            \Storage::disk("framework")
                ->delete( $archivosViews );
        }
        
        \Artisan::call('view:clear');
    	\Artisan::call('config:clear');
    	\Artisan::call('cache:clear');
	    return "Se ha vaciado la cache";
    }); 

    $router->post('private/login',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@login',
        'as'   => 'Public.private.login'
    ]);
    /*$router->get('solicitud',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\SolicitudTarjetaController@index',
        'as'   => 'Public.solicitudtajerta'
    ]);*/
    $router->get('estadocivil',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\SolicitudTarjetaController@EstadoCivil',
        'as'   => 'Public.solicitudtajerta.EstadoCivil'
    ]);
	
	 $router->get('activdadeconomica',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\SolicitudTarjetaController@ActivdadEconomica',
        'as'   => 'Public.solicitudtajerta.ActivdadEconomica'
    ]);
    
    $router->post('ValidaCliente',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\SolicitudTarjetaController@ValidateClient',
        'as'   => 'Public.solicitudtajerta.validacliente'
    ]);
    
    $router->post('solicitudtajerta/Guardar',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\SolicitudTarjetaController@guardar',
        'as'   => 'Public.solicitudtajerta.guardar'
    ]);
    

    $router->post('private/ConsultaTarjetas',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@ConsultaTarjetas',
        'as'   => 'Public.private.ConsultaTarjetas'
    ]);

    $router->get('private/loginPrueba',[
        'uses' => '\Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController@validacionUsuarioRutPass',
        'as'   => 'Public.private.validacionUsuarioRutPass'
    ]);
    
    $router->any('{uri}', [
        'uses' => 'PublicController@uri',
        'as' => 'page',
        'middleware' => config('asgard.page.config.middleware'),
    ])->where('uri', '.*');
}
