<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePageRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'page::pages.validation.attributes';

    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required' => 'Sr. Usuario, debe ingresar un Título',
            'slug.required' => 'Sr. Usuario, debe ingresar una URL amigable',
        ];
    }

    public function translationMessages()
    {
        return [
            'title.required' => 'Sr. Usuario, debe ingresar un Título',
            'slug.required' => 'Sr. Usuario, debe ingresar una URL amigable',
        ];
    }
}
