<?php

namespace Modules\Page\Http\Requests;

//use Modules\Core\Internationalisation\BaseFormRequest;
use Illuminate\Foundation\Http\FormRequest;

class FormularioRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "formulario" => "required|string",
            "nombrecompleto" => "required|string",
            "rut" => "required|string|min:8",
            "telefono" => "required|string|min:1",
            "email" => "required|email",
            "tiposeguro" => "required|string",
            "tiposolicitud" => "required"
        ];
    }    

    /**
    * Configure the validator instance.
    *
    * @param  \Illuminate\Validation\Validator  $validator
    * @return void
    */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $datos = $this->validationData();
            //Validando rut
            if( array_key_exists("rut", $datos ) ){
                $rut = $datos["rut"];
    
                if( !\Helper::validaRut( $rut ) ){
                    $validator
                        ->errors()
                        ->add("rut", "RUT INVÁLIDO");
                }
            }
        });
    }

    public function messages()
    {
        return [
            'formulario.required' => 'Error de formulario',
            'formulario.string' => 'Error de formulario',
            "nombrecompleto.required" => "Debe ingresar el nombre",
            "nombrecompleto.string" => "Debe ingresar el nombre",
            "rut.required" => "RUT INVÁLIDO",
            "rut.string" => "RUT INVÁLIDO",
            "rut.min" => "RUT INVÁLIDO",
            "telefono.required" => "Debe ingresar el telefono",
            "telefono.string" => "Debe ingresar el telefono",
            "telefono.min" => "Debe ingresar el telefono",
            "email.required" => "el email es inválido",
            "email.email" => "el email es inválido",
            "tiposeguro.required" => "Debe seleccionar el seguro que quieres contratar",
            "tiposeguro.string" => "Debe seleccionar el seguro que quieres contratar",
            "tiposolicitud.required" => "Debe seleccionar el tipo de solicitud",
            "tiposolicitud.string" => "Debe seleccionar el tipo de solicitud",
        ];
    }
}
