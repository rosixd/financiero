<?php

use Illuminate\Routing\Router;
use Modules\Page\Entities\Page;
use Modules\Page\Entities\PageTranslation;

/** @var Router $router */
$router->bind('page', function ($id) {
    $repo = app(\Modules\Page\Repositories\PageRepository::class)->find($id);
    //dd(Page::where('page_id', $id)->withTrashed());
    /*$condicion = PageTranslation::withTrashed()->find($id);*/
     return is_null($repo)? Page::withTrashed()->find($id):$repo; 
   /* return is_null($repo)? $condicion : $repo;*/
});

$router->group(['prefix' => '/page'], function (Router $router) {
    $router->get('pages', [
        'as' => 'admin.page.page.index',
        'uses' => 'PageController@index',
        'middleware' => 'can:page.pages.index',
        'script' => 'AppPagina',
        'view' => 'index',
        'modulo' => 'pagina',
        'submodulo' => 'pagina',
        'datatable' => 'admin.page.page.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('pages/create', [
        'as' => 'admin.page.page.create',
        'middleware' => 'can:page.pages.create',
        'uses' => 'PageController@create',
        'script' => 'AppPagina',
        'view' => 'index',
        'modulo' => 'pagina',
        'submodulo' => 'pagina',
        'datatable' => 'admin.page.page.datatable'
    ]);
    $router->post('pages', [
        'as' => 'admin.page.page.store',
        'middleware' => 'can:page.pages.create',
        'uses' => 'PageController@store',
    ]);
    $router->get('pages/{page}/edit', [
        'as' => 'admin.page.page.edit',
        'middleware' => 'can:page.pages.edit',
        'uses' => 'PageController@edit',
        'script' => 'AppPagina',
        'view' => 'index',
        'modulo' => 'pagina',
        'submodulo' => 'pagina',
        'datatable' => 'admin.page.page.datatable'
    ]);
    $router->put('pages/{page}/edit', [
        'as' => 'admin.page.page.update',
        'middleware' => 'can:page.pages.edit',
        'uses' => 'PageController@update',
    ]);
    $router->get('pages/destroy/{page}', [
        'as' => 'admin.page.page.destroy',
        'middleware' => 'can:page.pages.destroy',
        'uses' => 'PageController@destroy',
    ]);

    $router->get('pages/datatable', [
        'as' => 'admin.page.page.datatable',
        'middleware' => 'can:page.pages.index',
        'uses' => 'PageController@datatable',
    ]);
});
