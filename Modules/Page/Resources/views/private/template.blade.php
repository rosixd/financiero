@extends('layouts.master')

@section('content')
    @component('page::components.slider', [
        'img' => [
            'slide-micuenta.jpg',
            'slide-micuenta.jpg'
        ]
    ])
    @endcomponent
    @component('page::components.barra_saludo', [
        'datospersonales' => $datospersonales,
        'saludo'          => $saludos
    ])
    @endcomponent

    @component('page::components.barra_info_titular', [
        'nombrecompleto' => array_key_exists('PrimerNombre', $datospersonales) && array_key_exists('PrimerApellido', $datospersonales) ? $datospersonales['PrimerNombre'] . ' ' . $datospersonales['PrimerApellido'] : '' ,
        'ultimostarjeta' => isset( $cupostarjetas['ultimostarjeta'] ) ? $cupostarjetas['ultimostarjeta'] : [] ,
	    'tarjetaseleccionada' => $tarjetaseleccionada,
        'cupostarjetas' => $cupostarjetas,
        'disponibleotroscomercios' => $disponibleotroscomercios,
        'extracupotienda' => $extracupotienda,
        'extracupoavance' => $extracupoavance,
        'tarjetaBloqueo' => $tarjetaBloqueo

    ])
    @endcomponent
    @component('page::components.barra_navegacion_titular', ['controller'=>$controller, 'tarjetaseleccionada'=>$tarjetaseleccionada])
    @endcomponent

    @yield('contentPrivate')

    @component('page::components.barra_banners_doble', [
        'banner' => [
            (object)[
                'img' => 'banner-uso-responsable.jpg',
                'url' => '/asesoria-financiera'
            ],
            (object)[
                'img' => 'banner-beneficios2.jpg',
                'url' => '/beneficios-abcvisa'
            ]
        ]
    ])
    @endcomponent

    @php
        $inicio = false;

        if (session()->get('index')) {
            $inicio = true;
            session()->put('index', false);
        }
        $cargaCupos = false;
        if(array_key_exists("CuposTarjetas",session()->all())){
            if(!session()->get('CuposTarjetas')['datos']['data']){
                $cargaCupos = true;
            }
        }
       
    @endphp
    <script>
        var $inicio = {{ $inicio ? 'true' : 'false' }};
        var $cargaCupos = {{ $cargaCupos ? 'true' : 'false' }};

    </script>
@endsection
@push('scripts')
    <script>
    $(document).ready(function(){
        $url  = "{{route('Public.private.ic.buscarcupos')}}";
	if($cargaCupos){
		CargarCupos($url, $cargaCupos);
	}
        
    });
        
    </script>
@endpush
 
