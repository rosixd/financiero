<!-- contenidos -->
<section class="contenidosLogeado EstadoCuentaPrivado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="{{ Theme::url('img/icono-estado-cuenta-activo.jpg') }}"> Estado de cuenta
                </h1>
            </div>
        </div>

        <form class="boxInfoMovimientos">
            <div class="row contenidoFormulario1 boxActualizaDatos">
                <div class="col-md-6 estado_cuenta">
                    <h2>Último estado de cuenta</h2>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                    <div class="form-group">   
		                     
                        <select class="form-control custom-select" id="FormControlSelect2">
				            @if( empty( $data ) )
                                <option value="0">Sin Per&iacute;odos</option>
                            @endif
                            @foreach ( $data as $periodo_vencimiento)
                                <option value="{{ $periodo_vencimiento['nombre_archivo'] }}">Per&iacute;odo {{ $periodo_vencimiento['vencimiento'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <input type="hidden" id="valorT" value="{{ $valorT }}">
                <input type="hidden" id="valorR" value="{{ $valorR }}">
                <input type="hidden" id="valorV" value="{{ $valorV }}">
                <input type="hidden" id="valorhaydetalle" value="{{ $valorhaydetalle }}">
                <input type="hidden" id="url" value="{{ $url['url'] }}">

                <div class="col-md-6">
                    <div class="selectCorreo bgBlanco mb-3 pl-0">
                        <a class="btnAccion" href="" download="" data-toggle="tooltip"
                        data-placement="top" title="Descargar" id="botondescargar">
                            <img src="{{ Theme::url('img/icono-descargar.png') }}" class="descargar">
                            Descargar
                        </a>
                        <a class="btnAccion" href="#imgPdf" data-toggle="tooltip"
                        data-placement="top" title="Imprimir" id="btnImprimirPdf">
                            <img src="{{ Theme::url('img/icono-impresion.png') }}" class="imprimir">
                            Imprimir
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@include('page::private.estado_cuenta.detalle_cuenta')
@push('scripts')   
    <script>
        AppEstadoCuenta.urlestadocuenta = "{{ route('Public.private.estadocuenta') }}";
        AppEstadoCuenta.urlTicket = "{{ $url['url'] }}";
    </script>
@endpush