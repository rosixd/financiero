<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                    <h1>
                        <img src="{{ Theme::url('img/icono-movimientos-activo.png') }}"> Movimientos
                    </h1>
                @else
                    <h1>
                        <img src="{{ Theme::url('img/icono-pago-cuenta.png') }}"> Paga tu cuenta
                    </h1>
                @endif
            </div>
        </div>

        <div class="row boxInfoMovimientos">
            @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                <div class="col-md-6">
            @else
                <div class="col-md-6 offset-md-3">
            @endif
                <h2>Facturados</h2>
                <div class="tablaDeuda">
                    <table class="table table-reflow">
                        <tbody id="deudapagar">
			
                            @if($deudapagar)
                                <tr>
                                    <td class="infoleft border-top-0">Monto Facturado</td>
                                    <td class="inforight border-top-0">
                                        <span class="first">${{ $deudapagar['MontoFacturado'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="infoleft">Fecha Vencimiento</td>
                                    <td class="inforight">
                                        <span>{{ $deudapagar['FechaVencimiento'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="infoleft">Deuda Total</td>
                                    <td class="inforight">
                                        <span>${{ $deudapagar['DeudaTotal'] }}</span>
                                    </td>
                                </tr>
                                @if( $deudapagar['DeudaTotal'] != '0' || $deudapagar['MontoFacturado'] != '0' )
                                    <tr>
                                        <td colspan="2" class="border-top-0 px-0 pb-0 pt-0">
                                            <button type="button" class="btn btn-primary" onclick="window.location.href='/private/paga-tu-cuenta/'">PAGAR</button>
                                        </td>
                                    </tr>
                                @endif
                            @else
                                <tr>
                                    <td colspan="2" class="col-xs-10 text-center" style="margin-left: 250px; margin-top: 10%; margin-bottom: 10%;">
                                        <i class="fas fa-circle-notch fa-spin fa-3x"></i>
                                    </td>
                                </tr>
                                <tr class="d-none">
                                    <td colspan="2" class="infoleft border-top-0">{{ trans('page::global.servicios no disponible') }}</td>
                                </tr>
                            @endif 
                        </tbody>
                    </table>
                </div>  
            </div>
            @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                <div class="col-md-6">
                    <div class="container boxTablaMovimientos">
                        <div class="row">
                            <h2>No facturado</h2>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>

                            <table class="points_table" id="movimientosnofacturados">
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>
@if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
    @push('scripts')
        <script>
            AppMovimientos.urlmovimientosNoFacturados = "{{ route('Public.private.cuposnofacturados') }}";
        </script>
    @endpush
@endif