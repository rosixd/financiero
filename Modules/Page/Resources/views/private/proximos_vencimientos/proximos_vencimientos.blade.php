<!-- contenidos -->
<section class="contenidosLogeado EstadoCuentaPrivado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="{{ Theme::url('img/icono-proximos-vencimientos-activo.png') }}"> Pr&oacute;ximos vencimientos
                </h1>
            </div>
        </div>

        <div class="boxInfoMovimientos">
            <div class="row boxActualizaDatos">
                <div class="col-md-12">
                    <h2>Vencimientos pr&oacute;ximos 4 meses</h2>
                    <div class="progress">
                        <div class="progress-bar vencimientos" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row infoVencimientos">

            </div>
        </div>
    </div>
</section>
@push('scripts')
    <script>
        AppVencimientos.urlvencimientos = "{{ route('Public.private.vencimientos') }}";
    </script>
@endpush