<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="img/icono-simula-activo.png"> Simula tu avance
                </h1>
            </div>
        </div>

        <form class="boxInfoMovimientos infoSimulaAvances">
            <div class="row contenidoFormulario1 boxActualizaDatos">

                <h2>Detalle de la simulaci&oacute;n</h2>
                <div class="progress">
                    <div class="progress-bar simula" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="clearfix">

                </div>

                <div class="col-md-6">
                    <div class="simulaUsuario">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="infoLeft">Nombre: <span class="gris">Sebastian Chaparro</span></td>
                                    <td class="infoRight">$400.000</td>
                                </tr>
                                <tr>
                                    <td class="infoLeft">Número de cuotas</td>
                                    <td class="infoRight">12</td>
                                </tr>
                                <tr>
                                    <td class="infoLeft cuota">Valor de la cuota</td>
                                    <td class="infoRight cuota">$36.987</td>
                                </tr>
                                <tr>
                                    <td class="infoLeft">Primer vencimiento</td>
                                    <td class="infoRight">03 de abril 2018</td>
                                </tr>
                                <tr>
                                    <td><div class="cae">CAE: <span class="gris">5%</span></div></td>
                                    <td><div class="cae">CTC: <span class="gris">$12.750</span></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                </div>

                <div class="col-md-6">
                    <div class="simulaTools">
                        <div class="mb-3">
                        <ul>  
                            <li><a href="#"><i class="fas fa-plus rojo"></i> Agregar cuenta para realizar avance</a></li>
                            <li><a href="#"><i class="fas fa-pencil-alt rojo"></i> Editar cuentas guardadas</a></li>
                        </ul>     
                        </div>

                        <div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1"><strong>Acepto los terminos y condiciones de seguro B569</strong></label>
                        </div>
                            
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-block btnRojo">SOLICITAR AVANCE</button>  
                </div>

            </div>
            <p class="info_legal">(1) Monto referencial sujeto al saldo de l&iacute;nea de cr&eacute;dito al solicitarlo. La cuota se otorgar&aacute; al momento de solicitar el Avance. Promoci&oacute;n v&aacute;lida desde el 01 hasta el 31 de marzo de 2018. V&aacute;lido con Tarjetas Scotiabank Cencosud, Tarjetas Cencosud o Tarjetas M&aacute;s emitidas por CAT Administradora de Tarjetas S.A. (2) Cuota calculada al 01 de febrero de 2018.</p>
        </form>
    </div>
</section>