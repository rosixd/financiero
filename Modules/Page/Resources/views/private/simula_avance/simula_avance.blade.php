<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="img/icono-simula-activo.png"> Simula tu avance
                </h1>
            </div>
        </div>

        <form class="boxInfoMovimientos infoSimulaAvances">
            <div class="row contenidoFormulario1 boxActualizaDatos">
                <div class="col-sm-12 col-md-6 mt-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                            <label for="FormControlInput1">MONTO</label>
                            <input type="text" class="form-control" id="FormControlInput1" placeholder="$800.000">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label for="FormControlInput1">CUOTAS</label>
                                <input type="text" class="form-control" id="FormControlInput10" placeholder="00">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label for="FormControlInput1">MESES DIFERIDOS</label>
                                <input type="text" class="form-control" id="FormControlInput11" placeholder="0">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="info_legal">(1) Monto referencial sujeto al saldo de línea de crédito al solicitarlo. La cuota se otorgará al momento de solicitar el Avance. Promoción válida desde el 01 hasta el 31 de marzo de 2018.</p>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div>
                    <h3 class="tituDatos float-left">Seguros</h3>
                    <div class="clearfix"></div>
                        <div class="form-group">
                            <select class="form-control custom-select" id="FormControlSelect2">
                                <option>Seguro de vida PLUS + Seguro de Cesantía </option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <a href="#" class="rojo">Ver condiciones seguros</a>
                </div>

                <div class="col-sm-12 col-md-2">
                    <div>
                        <button class="btn btn-primary btn-block btnRojo mt-2 simula" type="submit">SIMULAR</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@component('page::components.barra_banners_simple', [
    'banners' => [
        (object)[
            'img' => 'banner-campana-avance.jpg',
            'url' => '#'
        ]
    ]
])
@endcomponent