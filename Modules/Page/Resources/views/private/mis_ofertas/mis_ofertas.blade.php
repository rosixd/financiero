<!-- contenidos -->
<section class="contenidosLogeado EstadoCuentaPrivado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="{{ Theme::url('img/icono-mis-ofertas-activo.png') }}"> Mis ofertas
                </h1>
            </div>
        </div>

        <div class="boxInfoMovimientos">
            <div class="row boxActualizaDatos">
                <div class="col-md-12">
                    <h2>Tenemos las siguientes ofertas especiales para ti</h2>
                    <div class="progress">
                        <div class="progress-bar misOfertas" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="row infoMisOfertas">
                <div class="bannerTop">
                    <a href="/tarjeta-app">
                        <img src="/assets/media/bannerpagina-app.jpg" class="img-fluid">
                    </a>
                </div>
                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalJetsmart" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/jetsmart-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalRecorrido" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/recorrido-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalTicketpro" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/ticketpro-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>   
            </div>

            <div class="row infoMisOfertas">
                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalSavory" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/savory-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>
                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBencineras" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/bencineras-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalHoyts" class="bannerHome" data-toggle="modal">
                        <img src="/assets/media/hoyts-526x480.jpg" class="img-thumbnail img-fluid">
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>
<div id="ModalHoyts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/hoyts2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalTicketpro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/ticketpro2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalRecorrido" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/recorrido2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalSavory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/savory2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalBencineras" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/bencineras2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalJetsmart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/jetsmart2-1166x739.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>