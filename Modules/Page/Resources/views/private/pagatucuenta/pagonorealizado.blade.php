<!-- Modal pago no realizado -->
<div id="modalFracaso" class="modal fade pagoNoExitoso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body contenidoModal">
                <img src='{{ Theme::url('img/icono-alerta.png') }}' class="mb-4">
                {{-- <img src='<c:url value="resources/img/icono-alerta.png"/>' class="mb-4"> --}}
                <h3 class="mb-3">Pago no realizado</h3>
                <p>Lo sentimos, el pago no fue realizado con éxito.<br>
                Por favor intente nuevamente.</p>

                <div class="clearfix"></div>
                <button type="button" class="btn btn-primary btnRojo mt-2" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
