<div id="modalComprobante" class="modal fade modalComprobantePago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a class="btnAccion" href="javascript:printModal();">
                        <img src='{{ Theme::url('img/icono-imprimir-doc.jpg') }}'>
                        {{-- <img src='<c:url value="resources/img/icono-imprimir-doc.jpg"/>'> --}}
                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div id="printContentModal" class="modal-body contenidoModal">
                <img  src='{{ Theme::url('img/logo-abc-comprobante.jpg') }}'>
                {{-- <img  src='<c:url value="resources/img/logo-abc-comprobante.jpg"/>'> --}}
                <h3>COMPROBANTE DE PAGO DE TARJETA</h3>
                <p class="text-center">Este es el comprobante de pago correspondiente 
                a los datos que se presentan a continuación:</p>

                <table class="table">
                    <tbody>
                        <tr>
                            <td>RUT Titular:</td>
                            <td id="rut_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Nombre:</td>
                            <td id="nombre_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Número de Tarjeta:</td>
                            <td id="n_tarjeta_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Fecha de Transferencia:</td>
                            <td id="fecha_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Monto:</td>
                            <td id="monto_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Número de Transacción:</td>
                            <td id="n_trx_comprobante"></td>
                        </tr>
                    </tbody>
                </table>
                <img src='{{ Theme::url('img/sello-comprobante.jpg') }}'>
                {{-- <img src='<c:url value="resources/img/sello-comprobante.jpg"/>'> --}}

            </div>

            <p class="mensajeFooter text-center">GUARDA O IMPRIME EL COMPROBANTE</p>
        </div>
    </div>
</div>

<style>
@media screen {
  #printSection {
      display: none;
  }
}

@media print {
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}
</style>
<script>
function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}
function printModal() {
    printElement(document.getElementById("printContentModal"));
}
</script>