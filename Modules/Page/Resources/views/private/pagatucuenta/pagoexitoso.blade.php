<div id="modalExito" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal pagoExitoso">
            
                <img src='{{ Theme::url('img/icono-pago-ok.png') }}' class="mb-4">
                {{-- <img src='<c:url value="resources/img/icono-pago-ok.png"/>' class="mb-4"> --}}
                <h3 class="mb-3">Pago realizado con éxito</h3>
                <p id="fecha_exito" ></p>
                <p id="cuenta_exito"></p>
                <p id="monto_exito"></p>
                <p id="n_trx_exito"></p>
                <p>¿Desea agregar un email<br>
                para el envío de su comprobante?</p>
            
                <form class="contenidoFormulario1">
                    <div class="form-group">
                        <label for="FormControlInput1">EMAIL</label>
                        <input type="text" class="form-control" id="FormControlInput12" placeholder="josefernandez@email.com">
                        <button class="btn btn-primary btn-block btnRojo mt-2" id="ver_comprobante" type="submit">VER COMPROBANTE</button>
                    </div>
                </form>
                <img class="img-fluid" src='{{ Theme::url('img/banners/banner-nuevaApp.jpg') }}' class="mt-4">
                {{-- <img class="img-fluid" src='<c:url value="resources/img/banners/banner-nuevaApp.jpg"/>' class="mt-4"> --}}
            </div>  
        </div>
    </div>  
</div>        