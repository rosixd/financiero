<section class="Banners">
    <div class="container-fluid">
        <div class="row">
            @foreach ( $banner as $banners )
                <div class="col-md-6 p0">
                    {{-- <a href="{{ route($banners->url) }}" class="bannerAnchoCompleto"> --}}
                    <a href="{{ $banners->url }}" class="bannerAnchoCompleto">
                        <img src="{{ Theme::url('img/banners/'.$banners->img) }}" class="img-fluid">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>