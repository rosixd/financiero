<section class="Banners mb-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p0">
                <a href="{{ $banners->url }}" class="bannerAnchoCompleto">
                    <img src="{{ Theme::url('img/banners/'.$banners->img) }}" class="img-fluid">
                </a>
            </div>
        </div>
    </div>
</section>
