<!-- barra Info Titular -->
<section class="barraTitular">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 bggris1">
                <div>
                    {{--tarjeta ABCDIN --}}
                    @if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABCDIN" )
                        @if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") )
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjetaabcdin-habilitada.png') }}">
                                @if ($tarjetaBloqueo)
                                    <i class="fas fa-lock"></i>
                                @else
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                @endif
                            </div>
                        @elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T']))
                            <div class="float-left">
                                <img src="{{ Theme::url('img/tarjetaabcdin-habilitada.png') }}">
                            </div>
                        @else
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjetaabcdin-habilitada.png') }}">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        @endif
                    @endif
                    
                    {{--tarjeta ABC --}}
                    @if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABC" )
                        @if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") )
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjetaabc-habilitada.png') }}">
                                @if ($tarjetaBloqueo)
                                    <i class="fas fa-lock"></i>
                                @else
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                @endif
                            </div>
                        @elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T']))
                            <div class="float-left">
                                <img src="{{ Theme::url('img/tarjetaabc-habilitada.png') }}">
                            </div>
                        @else
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjetaabc-habilitada.png') }}">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        @endif
                    @endif
                    
                    {{--tarjeta ABCVISA  --}}
                    @if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABCVISA" )
                        @if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") )
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjeta-sin-info.png') }}">
                                @if ($tarjetaBloqueo)
                                    <i class="fas fa-lock"></i>
                                @else
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                @endif
                            </div>
                        @elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T']))
                            <div class="float-left">
                                <img src="{{ Theme::url('img/tarjeta-sin-info.png') }}">
                            </div>
                        @else
                            <div class="float-left tarjetaBloqueada">
                                <img src="{{ Theme::url('img/tarjeta-sin-info.png') }}">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        @endif
                    @endif
	
                    <div class="boxtitular">
                        <h4>TITULAR</h4>
                        <h3>{{ $nombrecompleto }}</h3>
                        
                        <div id ="ultimostarjeta">
                            @if( !empty($ultimostarjeta) )
                                <h4>Nº DE CUENTA</h4>
                                <p>*** **** **** {{ $ultimostarjeta }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <ul>
                    @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                        <li style="margin-top: 10px;"><i class="fas fa-sync-alt"></i><a href="{{ route('Public.private.actualizadatos') }}">Actualizar Datos</a></li>
                    @endif
                    <li style="margin-top: 10px;">
                        <i class="fas fa-file-alt"></i>
                        @if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABCVISA" )
                            <a href="/assets/media/contrato-apertura-05062018.pdf" download="Contrato Abcvisa.pdf">Consultar Contrato</a>
                        @else
                            <a href="/assets/media/contrato-tarjeta-abc.pdf" download="Contrato ABC.pdf">Consultar Contrato</a>
                        @endif
                    </li>
                </ul>        
            </div>
            <div class="col-md-7 bggris2">
               
                <div class="boxCupo">
                    <div class="float-left text-left">
                        <h2>Cupo Utilizado</h2>
                        <h1 id="CuposUtilizados">$ {{ array_key_exists('CuposUtilizados', $cupostarjetas) && $cupostarjetas['CuposUtilizados'] ? $cupostarjetas['CuposUtilizados'] : '0' }}</h1>
                    </div>
                    <div class="float-right text-right">
                        <h2>Cupo Total</h2>
                        <h1 id="CupoTotal">$ {{ array_key_exists('CupoTotal', $cupostarjetas) && $cupostarjetas['CupoTotal'] ? $cupostarjetas['CupoTotal'] : '0' }}</h1>
                    </div>
                    <div class="clearfix"></div>
                    <div class="progress">
                        <div class="progress-bar"  id="BarraPorcentaje" role="progressbar" style="width:{{ array_key_exists('porcentaje', $cupostarjetas) && $cupostarjetas['porcentaje'] ? $cupostarjetas['porcentaje'] : '0' }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="float-left text-left box diagnalBar1">
                        <p>Cupo para compras en<br>
                        Tiendas abcdin y dijon</p>
                        <h3 id="DisponibleTiendas">$ {{ array_key_exists('DisponibleTiendas', $cupostarjetas) && $cupostarjetas['DisponibleTiendas'] ? $cupostarjetas['DisponibleTiendas'] : '0' }}</h3>
                    </div>
                    <div class="float-left text-left box diagnalBar2">
                        <p>Cupo para avances <br>en efectivo</p>
                        <h3 id="DisponibleAvanceEnEfectivo">$ {{ array_key_exists('DisponibleAvanceEnEfectivo', $cupostarjetas) && $cupostarjetas['DisponibleAvanceEnEfectivo'] ? $cupostarjetas['DisponibleAvanceEnEfectivo'] : '0' }}</h3>
                    </div>

                    <div class="float-left text-left box">
                        <p>Cupo para <br> otros comercios</p>
                        <h3 id="OtrosComercios">$ {{ array_key_exists('0', $disponibleotroscomercios) && $disponibleotroscomercios[0] ? $disponibleotroscomercios['0'] : '0' }}</h3>
                    </div>

                    <div class="clearfix"></div>
                   {{--  @if( $tipo == 'IC') --}}
                        <a href="#myModal" class="btn btn-primary btn-sm btn-extracupo" role="button" data-toggle="modal">VER EXTRACUPO</a>
                    {{-- @endif --}}
                </div>
            </div>
            
            <div class="col-md-2 bggris3">
                @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                    {{--<div class="boxBloqueo">
                        <i class="fas fa-lock"></i>
                        <h2>¿PERDISTE<br>TU TARJETA?</h2>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input vchecked" {{ $tarjetaBloqueo ? 'checked="checked"' : '' }}>
                            <span class="custom-control-indicator bdchecked"></span>
                        </label>
                        <p>BLOQUEAR</p>
                    </div>--}}
                @endif
            </div> 
        </div>
    </div>
</section>
<!-- fin barra info Titular -->
@push('html')
    @include('page::private.extracupo.index', ['cupostarjetas' => $cupostarjetas, 'disponibleotroscomercios' => $disponibleotroscomercios, 'extracupotienda' => $extracupotienda, 'extracupoavance' => $extracupoavance ])
@endpush
@push('scripts')
    <script>
        urlabloqueodesbloqueo = "{{ route('Public.private.ic.bloqueodesbloqueo') }}";
    </script>
@endpush