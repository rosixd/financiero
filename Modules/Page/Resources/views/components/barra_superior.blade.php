<!-- barra superior -->
<section class="barraSlide">
    <div class="container">
        <div class="row flex-parent">
            @foreach ( $card as $cards)
                <div class="col-sm-6">
                    <div class="box">
                        <img src="{{ Theme::url('img/'.$cards->img) }}" width="{{ $cards->width }}" height="{{ $cards->height }}">
                    </div>  
                    <div class="box2">
                        <h1>{{ $cards->titulo }}</h1>
                        <h2>{{ $cards->descripcion }}</h2>
                    </div>
                </div>           
            @endforeach
        </div>
    </div>
</section>
<!-- fin barra superior -->