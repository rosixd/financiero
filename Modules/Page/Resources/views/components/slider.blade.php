<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    {{-- <ol class="carousel-indicators">
        @foreach ( $img as $imagen)
            <li data-target="#carouselIndicators" data-slide-to="{{  $loop->iteration }}" {{ $loop->first ? 'class="active"' : '' }}></li>           
        @endforeach
    </ol> --}}
    {{-- <div class="carousel-inner" role="listbox">
        @foreach ( $img as $imagen)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <img class="d-block w-100" src="{{ Theme::url('img/slider/'.$imagen) }}"  alt="{{  $loop->iteration }} slider">
            </div>            
        @endforeach
    </div> --}}
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slide-micuenta.jpg"  alt="slider">
        </div>
    </div>
    {{-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
    </a> --}}
</div>

<!-- fin slider central -->