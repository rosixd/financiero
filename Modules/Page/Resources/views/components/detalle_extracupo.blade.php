<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <h3>Detalle de Extracupo</h3>
                <div id="extracupotiendas">
                    <h4 class="text-center linea">Cupo para Compras en Tiendas abcdin y Dijon</h4>
                    <p class="text-center">$ {{ isset($cupostarjetas['DisponibleTiendas']) ? $cupostarjetas['DisponibleTiendas'] : "0" }}</p>
                    <h4 class="text-left">Y puedes aumentarlo si:</h4>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>COMPRAS ENTRE</th>
                                <th>MONTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( !empty( $extracupotienda ) )
                                @foreach ( $extracupotienda as $extracupotiendas)
                                <tr>
                                    <td>{{ $extracupotiendas['cuotaMin'] }} a {{ $extracupotiendas['cuotaMax'] }} cuotas</td>
                                    <td>$ {{ $extracupotiendas['tope'] }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div id="extracupoavances">
                    <h4 class="text-center linea">Cupo para Avances en efectivo</h4>
                    <p class="text-center">$ {{ isset($cupostarjetas['DisponibleAvanceEnEfectivo']) ? $cupostarjetas['DisponibleAvanceEnEfectivo'] : "0" }}</p>
                    <h4 class="text-left">Y puedes aumentarlo si:</h4>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>COMPRAS ENTRE</th>
                                <th>MONTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( !empty($extracupoavance) )
                                @foreach ( $extracupoavance as $extracupoavances)
                                <tr>
                                    <td>{{ $extracupoavances['cuotaMin'] }} a {{ $extracupoavances['cuotaMax'] }} cuotas</td>
                                    <td>$ {{ $extracupoavances['tope'] }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>

                <div id="disponibleotroscomercios">
                    <h4 class="text-center linea">Cupo para otros comercios</h4>
                    <p class="text-center">$ {{ isset($disponibleotroscomercios[0]) ? $disponibleotroscomercios[0] : "0" }}</p>
                </div>
            </div>

        </div>
    </div>
</div>