
<!-- barra saludo -->
<section class="barraSaludo">
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            @if(!empty($datospersonales) && array_key_exists('PrimerNombre', $datospersonales) && array_key_exists('PrimerApellido', $datospersonales) )
            <h1>{{$saludo}}, {{ $datospersonales['PrimerNombre'] . ' ' . $datospersonales['PrimerApellido'] }} !</h1>
            @endif
        </div>

    </div>
    </div>
</section>
<!-- fin barra saludo -->