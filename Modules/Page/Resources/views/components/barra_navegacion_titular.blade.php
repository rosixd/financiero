<!-- barra navegacion titular -->
<section>
    
    <div class="container-fluid navTitular menuprivado">
        <div class="row justify-content-center align-content-center">
            <ul>
                @if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC')
                    <li>
                        <a href="{{ route('Public.private.ic.movimientos') }}" class="{{ $controller->activarmenu == 'movimientos' ? 'activo movimientos' : ''}}">
                            <img src="{{ Theme::url('img/icono-movimientos.png') }}"> Movimientos
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Public.private.estadosdecuenta') }}" class="{{ $controller->activarmenu == 'estado' ? 'activo estado' : ''}}">
                            <img src="{{ Theme::url('img/icono-estado.png') }}" > Estados de cuenta
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Public.private.ic.vencimientos') }}" class="{{ $controller->activarmenu == 'vencimientos' ? 'activo vencimientos' : ''}}">
                            <img src="{{ Theme::url('img/icono-proximos.png') }}"> Pr&oacute;ximos vencimientos
                        </a>
                    </li>
{{--                     <li>
                        <a href="#" class="{{ $controller->activarmenu == 'avance' ? 'activo avance' : ''}}">
                            <img src="{{ Theme::url('img/icono-avance.png') }}"> Solicita tu avance
                        </a>
                    </li> --}}
                    <li>
                        <a href="{{ route('Public.private.misofertas') }}" class="{{ $controller->activarmenu == 'ofertas' ? 'activo ofertas' : ''}}">
                            <img src="{{ Theme::url('img/icono-misofertas.png') }}"> Mis ofertas
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('Public.private.pagarcuenta') }}" class="{{ $controller->activarmenu == 'avance' ? 'activo avance' : ''}}">
                            <img src="{{ Theme::url('img/icono-avance.png') }}" > Paga tu cuenta
                        </a>
                    </li> 
                    <li>
                        <a href="{{ route('Public.private.estadosdecuenta') }}" class="{{ $controller->activarmenu == 'estado' ? 'activo estado' : ''}}">
                            <img src="{{ Theme::url('img/icono-estado.png') }}" > Estados de cuenta
                        </a>
                    </li>                
                @endif
            </ul>
        </div>
    </div>
</section>
<!-- fin barra navegacion titular -->