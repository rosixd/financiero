<link rel="stylesheet" href="/themes/abcdin/css/style.css" />
<link rel="stylesheet" href="/themes/abcdin/css/responsive.css" />
<script src="/themes/abcdin/js/function.js' ) }}"></script>
<section style="background-color: #999999; ">
    <div class="container clearfix" style="">
        <div id="zb_tree" style="background: none;">
            <div class="col_full center">
                <ul id="myTab2" class="nav nav-pills boot-tabs" style="display: inline-block !important;">
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/nuestra-empresa"  style="border-radius: 0px; width: auto!important;" class="button2">
                            <div class="">Nuestra Empresa</div>
                        </a>
                    </li>
                    <li class=" toptabsli toptabsliwidth" id="ropaid1" style="display: inline-block !important;">
                        <a href="/accionista"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Accionistas</div>
                        </a>
                    </li>
                    <li class="active toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/directores"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Directorio</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/infofinanciera"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Información Financiera</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/organigrama"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Organigrama</div>
                        </a>
                    </li>
                    <!-- <li class="toptabsli"><a href="#ofertadi" data-toggle="tab" style="padding: 0px;border-radius: 8px; width: auto!important;" class="button2"><div class="iconos_izq3"><img alt="Seguro de Vida" src="img/Menu/blanco_saludPage_1.png"></div><div class="texto_mid3">SEGURO <span style="color:#de004a;">AUTOMOTRIZ</span></div></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<div id="myTabContent2" class="tab-content">
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix">
            <div class="col_full left" >
                <div style="width: 100%;margin: 0 auto; display:block; ">
                    <h3 style="color: #333333;">
                        <img src="/assets/media/icon-directores-rojo.png" alt="abcdin">Directores
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="center_movil" style="background-color: #5e5e5e;">
        <div class="clearfix" style="width:100%; padding: 0;" >
            <div class="col_full center">
                <div class="col_half contenedor_directores">
                    <img src="/assets/media/logo-abcdin-blanco.png" alt="abcdin">
                </div>
                <div class="col_half col_last directorio_footer">
                    <br>
                    <div class="col_half padding_directores">
                        <h4 style="color: #fff; margin-bottom: 0;">Presidente</h4>
                        <p style="margin-bottom: 0;">Pablo Turner G.</p>
                        <p style="margin-bottom: 0;">Ingeniero Comercial, PUC</p>
                        <p>MBA Chicago</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Jaime Santa Cruz N. </p>
                        <p>Ingeniero Civil, PUC</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Pablo Santa Cruz Negri</p>
                        <p>Ingeniero Comercial , PUC</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Roberto Piriz Simonetti</p>
                        <p style="margin-bottom: 0;">Abogado, U. de Chile</p>
                    </div>	            		
                    <div class="col_half col_last padding_directores">            			
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Cristian Neely Barbieri</p>
                        <p style="margin-bottom: 0;">Ingeniero Civil, PUC</p>
                        <p>&nbsp</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Juan M. Santa Cruz Munizaga</p>
                        <p>Ingeniero Civil, PUC</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p style="margin-bottom: 0;">Margarita Henkces</p>
                        <p>&nbsp</p>
                        <h4 style="color: #fff; margin-bottom: 0;">Director</h4>
                        <p>Alfredo Guardiola</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
