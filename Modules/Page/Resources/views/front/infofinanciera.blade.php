<link rel="stylesheet" href="/themes/abcdin/css/style.css" />
<link rel="stylesheet" href="/themes/abcdin/css/responsive.css" />
<script src="/themes/abcdin/js/function.js' ) }}"></script>
<section style="background-color: #999999; ">
    <div class="container clearfix" style="">
        <div id="zb_tree" style="background: none;">
            <div class="col_full center">
                <ul id="myTab2" class="nav nav-pills boot-tabs" style="display: inline-block !important;">
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/nuestra-empresa" style="border-radius: 0px; width: auto!important;" class="button2">
                            <div class="">Nuestra Empresa</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" id="ropaid1" style="display: inline-block !important;">
                        <a href="/accionista" style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Accionistas</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/directores" style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Directorio</div>
                        </a>
                    </li>
                    <li class="active toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/infofinanciera" style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Información Financiera</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/organigrama" style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Organigrama</div>
                        </a>
                    </li>
                    <!-- <li class="toptabsli"><a href="#ofertadi" data-toggle="tab" style="padding: 0px;border-radius: 8px; width: auto!important;" class="button2"><div class="iconos_izq3"><img alt="Seguro de Vida" src="img/Menu/blanco_saludPage_1.png"></div><div class="texto_mid3">SEGURO <span style="color:#de004a;">AUTOMOTRIZ</span></div></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<div id="myTabContent2" class="tab-content">
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix">
            <div class="col_full left">
                <div style="width: 100%; margin: 0 auto; display:block; ">
                    <h3 style="color: #333333;">
                        <img src="/assets/media/icon-inffinaciera.png" alt="abcdin">Información Financiera
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px;">
        <div class="container clearfix" style="background-color: #fff; padding: 40px; border-top-right-radius: 20px; border-top-left-radius: 20px; max-width: 1170px !important;">
            <div class="col_full center">
                <div class="col_one_fourth center">
                    <img src="/assets/media/icon-memoria.png" alt="abcdin" style="margin-bottom:10px;">
                    <h4>Memoria Anual</h4>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="box-shadow: none !important">
                        Selecciona un documento
                        <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a target="_blank" href="/assets/media/memoriaabcdin2011.pdf">Memoria Anual 2011</a></li>
                            <li><a target="_blank" href="/assets/media/memoria-abcdin2012.pdf">Memoria Anual 2012</a></li>
                            <li><a href="#">Memoria Anual 2013</a></li>
                            <li><a href="#">Memoria Anual 2014</a></li>
                            <li><a target="_blank" href="/assets/media/memoria-cofisa2014.pdf">Memoria Cofisa 2014</a></li>
                            <li><a href="#">Memoria Anual 2015</a></li>
                            <li><a href="#">Memoria Anual 2016</a></li>
                            <li><a target="_blank" href="/assets/media/memoria-2017.pdf">Memoria Anual 2017</a></li>
                            <li><a target="_blank" href="/assets/media/cofisa-memoria2017.pdf">Memoria Cofisa 2017</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col_one_fourth center" style="color: #777;">
                    <img src="/assets/media/icon-estadofinanciero.png" alt="abcdin" style="margin-bottom:10px;">
                    <h4>Estados Financieros</h4>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="box-shadow: none !important">
                        Selecciona un documento
                        <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="height: 300px; overflow-y: scroll;">
                            <li><a target="_blank" href="/assets/media/1t11-eeff-ifrs-ad-retail.pdf">1T11 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t11-eeff-ifrs-ad-retail.pdf">2T11 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t11-eeff-ifrs-ad-retail.pdf">3T11 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t11-eeff-ifrs-ad-retail.pdf">4T11 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/1t-12eeff-ifrs-ad-retail.pdf">1T12 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t-12eeff-ifrs-ad-retail.pdf">2T12 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t-12eeff-ifrs-ad-retail.pdf">3T12 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t12-eeff-ad-retail.pdf">4T12 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/1t13-eeff-ad-retail.pdf">1T13 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t13-eeff-ad-retail.pdf">2T13 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t13-eeff-ad-retail.pdf">3T13 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t13-eeff-ad-retail.pdf">4T13 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/1t14-eeff-ad-retail.pdf">1T14 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t14-eeff-ad-retail.pdf">2T14 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t14-eeff-abcinversiones.pdf">2T14 - EEFF ABC INVERSIONES</a></li>
                            <li><a target="_blank" href="/assets/media/2t1-eeff-cofisa.pdf">2T14 - EEFF COFISA</a></li>
                            <li><a target="_blank" href="/assets/media/3t14-eeff-ifrs-ad-retail.pdf">3T14 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t14-eeff-cofisa.pdf">3T14 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/3t14-eeff-abcinversiones.pdf">3T14 - EEFF ABC INVERSIONES</a></li>
                            <li><a target="_blank" href="/assets/media/4t14-eeff-ifrs-ad-retail.pdf">4T14 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t14-eeff-ifrs-cofisa.pdf">4T14 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/adretail-eeff-2015-q.pdf">1T15 – EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/cofisa-eeff2015-1q.pdf">1T15 – EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/adretaileeff201506.pdf">2T15 – EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t15-eef-cofisa.pdf">2T15 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/estado-financiero-cfisa0715vf.pdf">3T15 - EEFF AD Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/ad-retail-eeff-20153q.pdf">3T15 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t15-eeff-adretail.pdf">4T15 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/4t15-eeff-cofisa.pdf">4T15 – EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/1t16-eeff-ad-retail.pdf">1T16 – EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/cofisa-eeff-sbif-2015-12.pdf">4T15 – EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/adretail-eeff-2015-12.pdf">4T15 – EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/1t16-eeff-ad-retail.pdf">1T16 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/1t16-eeff-cofisa.pdf">1T16 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/2t16-analisis-razonado.pdf">2T16 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/2t16eeffcofisa.pdf">2T16 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-adretail-092016.pdf">3T16 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-092016.pdf">3T16 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/ad-retail-eeff-2016-12.pdf">4T16 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-042017.pdf">4T16 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-adretail-032017.pdf">1T17 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-sbif-ifrs-032017.pdf">1T17 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-sbif-ifrs-062017.pdf">2T17 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeffadr20172q.pdf">2T17 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/eeffcofisa3t17new.pdf">3T17 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeffadr20173q.pdf">3T17 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/adretail122017.pdf">4T17 - EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/cofisa122017.pdf">4T17 - EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-ad-reatil-31-03-2018.pdf">1T18 EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-ifrs-03-2018.pdf">1T18 EEFF Cofisa</a></li>
                            <li><a target="_blank" href="/assets/media/2t18-eeff-adretail.pdf">2T18 EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t18-eeff-ad-retail.pdf">3T18 EEFF AD Retail</a></li>
                            <li><a target="_blank" href="/assets/media/3t18-eeff-cofisa.pdf">3T18 EEFF Cofisa</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col_one_fourth center" style="color: #777;">
                    <img src="/assets/media/icon-analisisrazonado.png" alt="abcdin" style="margin-bottom:10px;">
                    <h4>Análisis Razonado</h4>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="box-shadow: none !important">
                        Selecciona un documento
                        <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="height: 300px; overflow-y: scroll;">
                            <li><a target="_blank" href="/assets/media/1t11-analisisrazonado-adretail.pdf">1T11 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t11-analisisrazonado-adretail.pdf">2T11 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/3t11-analisisrazonado-adretail.pdf">3T11 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/4t11-analisisrazonado-adretail.pdf">4T11 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/1t12analisisrazonado-adretail.pdf">1T12 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t12analisisrazonado-adretail.pdf">2T12 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/3t12analisisrazonado-adretail.pdf">3T12 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/4t12-analisis-razonado.pdf">4T12 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/1t13-analisis-razonado.pdf">1T13 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t13-analisis-razonado.pdf">2T13 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/3t13-analisis-razonado.pdf">3T13 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/4t13-analisis-razonado.pdf">4T13 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/1t14-analisis-razonado.pdf">1T14 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t14-analisis-razonado.pdf">2T14 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/3t14-analisisrazonado-adretail.pdf">3T14 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/4t14-analisisrazonado-adretail.pdf">4T14 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/4t14presentacioncorporativa.pdf">4T14 – Presentación corporativa</a></li>
                            <li><a target="_blank" href="/assets/media/adretaill-ar-2015-1q.pdf">1T15 – Análisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/adretailanalisisrazonado201506.pdf">2T15 – Análisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/adretailpresresultados201506.pdf">2T15 – Presentación Corporativa</a></li>
                            <li><a target="_blank" href="/assets/media/ad-retail-ar-20153q.pdf">3T15 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/adretail-analisis-razonado-2015-12.pdf">4T15 – Análisis razonado</a></li>
                            <li><a target="_blank" href="/assets/media/1t16-analisis-razonado.pdf">1T16 Análisis razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t16-eeff-adretail.pdf">2T16 - Análisis razonado</a></li>
                            <li><a target="_blank" href="/assets/media/razonado-092016.pdf">3T16 -  Análisis razonado</a></li>
                            <li><a target="_blank" href="/assets/media/ad-retail-razonado-2016-12.pdf">4T16 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/1t2017adretail-032017.pdf">1T17 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/ar2t17.pdf">2T17 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/adretailarazonado20173q.pdf">3T17 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/adretailrazonado2017.pdf">4T17 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/ad-retail-analisis-razonado-2018-1q.pdf">1T18 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/2t18-analisis-razonado.pdf">2T18 - Analisis Razonado</a></li>
                            <li><a target="_blank" href="/assets/media/3t18-analisis-razonado.pdf">3T18 - Análisis Razonado</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col_one_fourth col_last center" style="color: #777;">
                    <img src="/assets/media/icon-infointeres.png" alt="abcdin" style="margin-bottom:10px;">
                    <h4>Información de Interés</h4>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="box-shadow: none !important">
                        Selecciona un documento
                        <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="height: 300px; overflow-y: scroll;">
                            <li><a target="_blank" href="/assets/media/ad-he-fip-iv-2018.pdf">HE Registro de valores Nª1079</a></li>
                            <li><a target="_blank" href="/assets/media/eeff-cofisa-ifrs-03-2018.pdf">EEFF Cofisa IFRS 03-2018</a></li>
                            <li><a target="_blank" href="/assets/media/emol050118.jpg">Mercurio(05-ene-18)HE</a></li>
                            <li><a target="_blank" href="/assets/media/he-negociacion-colectiva.pdf">HE Negociación Colectiva</a></li>
                            <li><a target="_blank" href="/assets/media/he-adquisicion-del-negocio-dijon.pdf">HE Adquisición Negocio Dijon</a></li>
                            <li><a target="_blank" href="/assets/media/he-colocacion-bono.pdf">HE Colocacion Bono</a></li>
                            <li><a target="_blank" href="/assets/media/hes-2012060088140.pdf">HE Suscripcion-Pago Acciones</a></li>
                            <li><a target="_blank" href="/assets/media/hes-2012060077833.pdf">HE Prorroga de Obligación</a></li>
                            <li><a target="_blank" href="/assets/media/quinta-junta.ext.de-accionias.pdf">Quinta Junta.Ext.de Accionias</a></li>
                            <li><a target="_blank" href="/assets/media/septima-junta.ext.deaccionias-adretail.pdf">Septima Junta.Ext.de Accionias</a></li>
                            <li><a target="_blank" href="/assets/media/junta-extraordinaria-accionistas.pdf">Junta Extraordinaria Accionista</a></li>
                            <li><a target="_blank" href="/assets/media/he-adquisicion-del-negocio-dijon (1).pdf">HE Adquisición Negocio Dijon</a></li>
                            <li><a target="_blank" href="/assets/media/manual-info-mcdo.pdf">Manual de Inf. para el Mcdo</a></li>
                            <li><a target="_blank" href="/assets/media/2013-06-04-diario-el-pulso.pdf">Diario el Pulso 04-06-13</a></li>
                            <li><a target="_blank" href="/assets/media/2013-06-04-diario-financiero.pdf">Diario Financiero 04-06-13</a></li>
                            <li><a target="_blank" href="/assets/media/hes-es-ctto-de-pr-sc.pdf">HE Colocación Bono SerieC/Jun</a></li>
                            <li><a target="_blank" href="/assets/media/he-colocacion-bono-c.pdf">HE Colocación Bono SerieC/Jul</a></li>
                            <li><a target="_blank" href="/assets/media/emision-bonos-des.pdf">Emisión Bonos Desm.</a></li>
                            <li><a target="_blank" href="/assets/media/he-bono-securitizado-ps26.pdf">HE Bono Securitizado PS26</a></li>
                            <li><a target="_blank" href="/assets/media/lista-candidatos-junta-ord-accionista.pdf">Candidatos Junta Ord.Accionista</a></li>
                            <li><a target="_blank" href="/assets/media/oficioordinario-23462svs.pdf">Oficio Ordinario 23.462 SVS</a></li>
                            <li><a target="_blank" href="/assets/media/2014-12-9df-dijon.pdf">Diario Financiero 9Dic14Dijon</a></li>
                            <li><a target="_blank" href="/assets/media/df-abcdin.pdf">Diario Financiero 5Dic14correc.</a></li>
                            <li><a target="_blank" href="/assets/media/dpulsoabcdin.jpg">Diario Pulso 26Agosto2015</a></li>
                            <li><a target="_blank" href="/assets/media/multa-cofisa-2015-10.pdf">Multa Cofisa 2015-10</a></li>
                            <li><a target="_blank" href="/assets/media/diario-pulso111215.pdf">Diario-Pulso 11-12-15</a></li>
                            <li><a target="_blank" href="/assets/media/diario-financiero111215.png">Diario-Financiero 11-12-15</a></li>
                            <li><a target="_blank" href="/assets/media/elpulsopabloturner2016abr05.pdf">Diario Pulso 2016-abr-05</a></li>
                            <li><a href="#">Diario Financiero 2016-oct-21</a></li>
                            <li><a target="_blank" href="/assets/media/emol0417.png">El Mercurio 2017 -mayo-02</a></li>
                            <li><a href="#">La Tercera 2017-abr-18</a></li>
                            <li><a target="_blank " href="/assets/media/emol3117.pdf ">El Mercurio 2017 -Julio-31</a></li>
                            <li><a target="_blank " href="/assets/media/emol050118.jpg ">Mercurio(05-ene-18)HE</a></li>
                            <li><a target="_blank " href="/assets/media/20180114.jpg ">La Tercera 2018-01-14</a></li>
                            <li><a target="_blank " href="/assets/media/adretail-potencia-dijon-con-aperturas-y-remodelaciones.pdf ">AD Retail potencia Dijon</a></li>
                            <li><a target="_blank " href="/assets/media/adretail-hhee-2018-11-28.pdf ">Registro de valores Nº1079</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container clearfix " style="width: 100%; margin: 0 auto; text-align: center; ">
            <img src="/assets/media/img-iformacionfinanciera.jpg " alt="abcdin " style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; max-width: 1170px; ">
        </div>
    </section>
    <section class="center_movil " style="background-color: #e4e4e4; padding-bottom: 20px; ">
        <div class="container clearfix " style="padding:0px; width: 50%; padding-top: 40px; ">
            <div class="col_half ">
                <h4 class="info_footer " style="border-bottom: 1px solid #B2121D; color: #7f7f7f !important; padding-bottom: 20px; ">Contacto Inversionistas</h4>
                <h4 style="margin-bottom: 10px; color: #7f7f7f !important; ">Ignacio Iratchet</h4>
                <h4 style="margin-bottom: 10px; color: #7f7f7f !important; ">Subgerente de Finanzas Corporativas</h4>
                <a href="mailto:inversionistas@abcdin.cl ">
                    <h4 style="margin-bottom: 10px; color: #7f7f7f !important; ">inversionistas@abcdin.cl</h4>
                </a>
                <a href="tel:+56 2 2898 3025 ">
                    <h4 style="color: #7f7f7f !important; ">+56 2 2898 3025</h4>
                </a>
            </div>
            <div class="col_half col_last padding_izq ">
                <h4 class="info_footer " style="border-bottom: 1px solid #B2121D; color: #7f7f7f !important; padding-bottom: 20px; ">Contacto Clientes</h4>
                <a href="tel:600 830 222 ">
                    <h4 style="margin-bottom: 10px; color: #7f7f7f !important; ">600 830 222</h4>
                </a>
                <a href="https://www.abcdin.cl/tienda/AbcdinContactenosView?storeId=10001&catalogId=10001&langId=-1000 " target="_blank ">
                    <h4 style="margin-bottom: 10px; color: #7f7f7f !important; ">Contacto clientes</h4>
                </a>
                <a href="https://www.abcdin.cl/wcsstore/ABCDIN/form/form-despacho.html " target="_blank ">
                    <h4 style="color: #7f7f7f !important; ">¿Problemas con tu despacho?</h4>
                </a>
            </div>
        </div>
    </section>
</div>