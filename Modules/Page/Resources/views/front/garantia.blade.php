<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slider-garantia-100.jpg" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/assets/media/slider-garantia-100.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->

<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular Garantia">
        <div class="row justify-content-around align-content-center">
            <ul class="nav nav-tabs justify-content-around">
                <li class="ligarantia">
                    <!-- <a href="#" class="float-left active clickLinkPdfLightBox" data-target="garantia_extendida_pdf"> -->
                    <a href="/" class="float-left imagengarantia" data-target="#infogarantiaextendida" data-toggle="tab">
                        <img src="/assets/media/icono-garantia-extendida.png" class="iconogarantia"> Garant&iacute;a Extendida
                    </a>
                    <span class="descargaCertificado">
                        <a href="/assets/media/garantia-extendida-web-2018.pdf" download="garantia-extendida-web-2018.pdf" id="garantia_extendida_pdf" style="width: 170px !important;">
                            <img src="/assets/media/icono-descarga-certificado.png" class="imgdescargacertificado"> <p style="margin-top: 5px;">Descargar Certificado</p>
                        </a>
                    </span>
                </li>
                <li class="ligarantia garantiaresponsive">
                    <!-- <a href="#" class="float-left clickLinkPdfLightBox" data-target="garantia_extendida_plus_pdf"> -->
                    <a href="/" class="float-left imagengarantia" data-target="#infogarantiaextendidaplus" data-toggle="tab">
                        <img src="/assets/media/icono-garantia-extendida-plus.png" class="iconogarantia"> Garant&iacute;a Extendida Plus
                    </a>
                    <span class="descargaCertificado">
                        <a href="/assets/media/garantia-extendida-plus-web-2018.pdf" download="garantia-extendida-plus-web-2018.pdf" id="garantia_extendida_plus_pdf" style="width: 170px !important;">
                            <img src="/assets/media/icono-descarga-certificado.png" class="imgdescargacertificado"> <p style="margin-top: 5px;">Descargar Certificado</p>
                        </a>
                    </span>
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- fin barra superior -->
<div class="container-fluid contenidoGarantia">
    <div class="row">
        <div class="col-lg-4 p-0 Gris1 infoCallCenter d-flex">
            <div class="infoBox d-flex">
                <h1>¿Dudas? Ll&aacute;manos</h1>
                <a href="tel:+56800380911" class="btn_solicitalo">
                    <img src="/assets/media/icono-fono.png"> 800 380 911
                </a>
                <h1 class="mt-5">Contacto asistencia</h1>
                <a href="tel:+56222002952" class="btn_solicitalo">
                    <img src="/assets/media/icono-call.png"> +56 2 2200 2952
                </a>
            </div>
        </div>
        <div class="col-lg-8 p-0">
            <div class=videoBox>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/E7nc9tKb12w" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid px-0 py-4 moduloAsistencia">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>ASISTENCIA</h1>
                <p>Contrata tu Garant&iacute;a Extendida <br> y obt&eacute;n este beneficio exclusivo</p>
            </div>
            <div class="col-md-6 d-flex align-items-center justify-content-center">
                <a style="text-decoration: none; box-shadow: none;" class="btn btn-outline-primary" href="#ModalGarantia" role="button" data-toggle="modal">
                    <i class="fas fa-plus float-left pt-1"></i> Con&oacute;celo
                </a>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid p0">
    <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
        <!-- tab 1 -->
        <section class="bannersTabs tab-pane p0 fade active show" id="infogarantiaextendida">
            <div class="container-fluid">
                <div class="row">
                    <div class="board">
                        <div class="board-inner">
                            <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                <li>
                                    <a href="#uno" data-toggle="tab" class="active show">
                                        <span class="round-tabs one">¿Qu&eacute; es la Garant&iacute;a Extendida?</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#dos" data-toggle="tab">
                                        <span class="round-tabs two">¿C&oacute;mo se usa?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tres" data-toggle="tab">
                                        <span class="round-tabs three">Preguntas Frecuentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>

                        <div class="tab-content Contenido">
                            <div class="tab-pane p0 fade in active" id="uno">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    El Servicio de Garantía Extendida ABCDIN, permite prolongar la garantía original del producto que usted adquirió una vez expirado el servicio otorgado por el fabricante, quedando completamente protegido frente a desperfectos o fallas. Esto cubre cualquier
                                                    reparación necesaria durante el período que fue contratada, indicado en la boleta de compra o factura.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <h1>Beneficios</h1>
                                                <ul class="p0">
                                                    <p class="textjustify">
                                                        1.- Controles remotos de productos con Garantía Extendida ABCDIN protegidos en caso de fallas. Su cobertura comenzará una vez expirada la garantía otorgada por el fabricante del producto con Garantía Extendida y vencerá al término de esta. En caso que
                                                        el control remoto no tenga reparación queda establecido que al cliente se le entregará un control remoto universal en su reemplazo.
                                                    </p>

                                                    <p class="textjustify">
                                                        2.- Fallas provocadas por variaciones de voltaje están cubiertas por Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        3.- Atención a domicilio para productos equivalentes desde el tamaño de un LCD de 25”, y en general para productos de volumen o peso mayores, siempre que haya estado comprendido en la garantía del fabricante.
                                                    </p>

                                                    <p class="textjustify">
                                                        4.- Si el producto es regalado o vendido, se mantiene la Garantía Extendida ABCDIN contratada; sin embargo, para hacerla exigible, el cliente y los dueños posteriores deben cumplir con las obligaciones del cliente que aquí se señalan.
                                                    </p>

                                                    <p class="textjustify">
                                                        5.- Red de servicios técnicos en todo Chile, desde Arica a Punta Arenas.
                                                    </p>

                                                    <p class="textjustify">
                                                        6.- Garantía de 90 días del servicio técnico ante la reparación realizada y cubierta por Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        7.- Línea telefónica gratuita para asesoría y atención de clientes Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        8.- Garantía de satisfacción de compra. Devolución del 100% del valor pagado por la Garantía Extendida ABCDIN hasta 10 días corridos después de la compra.
                                                    </p>

                                                    <p class="textjustify">
                                                        9.- Beneficio de Asistencia Garantía Extendida.
                                                    </p>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p0 fade in" id="dos">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    Sólo debes llamar al 800 380 911, o desde celulares al fono 2 2826 8992 o acudir a cualquier tienda ABCDIN, una vez que el producto presente una falla.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p0 fade in" id="tres">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    <strong>¿SI ME ARREPIENTO DE LA COMPRA?</strong>
                                                </p>
                                                <p class="textjustify">
                                                    Puedes solicitar la devolución del 100% del valor pagado por la Garantía Extendida hasta 10 días corridos después de la Compra.
                                                </p>
                                                <br>
                                                <p class="textjustify">
                                                    <strong>¿SI COMPRÉ UN PRODUCTO SIN GARANTÍA EXTENDIDA?</strong>
                                                </p>
                                                <p class="textjustify">
                                                    Tienes un plazo para comprar la Garantía Extendida Abcdin de 30 días corridos, contados desde la fecha de compra del producto.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- fin tab 1 -->
        <!-- tab 2 -->
        <section class="bannersTabs tab-pane p0 fade" id="infogarantiaextendidaplus">
            <div class="container-fluid">
                <div class="row">
                    <div class="board">
                        <div class="board-inner">
                            <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                <li>
                                    <a href="#unoa" data-toggle="tab" class="active show">
                                        <span class="round-tabs one">¿Qu&eacute; es la Garant&iacute;a Extendida Plus?</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#dosa" data-toggle="tab">
                                        <span class="round-tabs two">¿C&oacute;mo se usa?</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tresa" data-toggle="tab">
                                        <span class="round-tabs three">Preguntas Frecuentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>

                        <div class="tab-content Contenido">
                            <div class="tab-pane p0 fade in active" id="unoa">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    El Servicio de Garantía Extendida Plus ABCDIN, permite prolongar la garantía original del producto que usted adquirió una vez expirado el servicio otorgado por el fabricante, quedando completamente protegido frente a desperfectos o fallas. Esto cubre
                                                    cualquier reparación necesaria durante el período que fue contratada, indicado en la boleta de compra o factura.
                                                    <br> Además incluye robo por un año con una carencia de 30 días.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <h1>Beneficios</h1>
                                                <ul class="p0">
                                                    <p class="textjustify">
                                                        1.- Controles remotos de productos con Garantía Extendida ABCDIN protegidos en caso de fallas. Su cobertura comenzará una vez expirada la garantía otorgada por el fabricante del producto con Garantía Extendida y vencerá al término de esta. En caso que
                                                        el control remoto no tenga reparación queda establecido que al cliente se le entregará un control remoto universal en su reemplazo.
                                                    </p>

                                                    <p class="textjustify">
                                                        2.- Fallas provocadas por variaciones de voltaje están cubiertas por Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        3.- Atención a domicilio para productos equivalentes desde el tamaño de un LCD de 25”, y en general para productos de volumen o peso mayores, siempre que haya estado comprendido en la garantía del fabricante.
                                                    </p>

                                                    <p class="textjustify">
                                                        4.- Si el producto es regalado o vendido, se mantiene la Garantía Extendida ABCDIN contratada; sin embargo, para hacerla exigible, el cliente y los dueños posteriores deben cumplir con las obligaciones del cliente que aquí se señalan.
                                                    </p>

                                                    <p class="textjustify">
                                                        5.- Red de servicios técnicos en todo Chile, desde Arica a Punta Arenas.
                                                    </p>

                                                    <p class="textjustify">
                                                        6.- Garantía de 90 días del servicio técnico ante la reparación realizada y cubierta por Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        7.- Línea telefónica gratuita para asesoría y atención de clientes Garantía Extendida ABCDIN.
                                                    </p>

                                                    <p class="textjustify">
                                                        8.- Garantía de satisfacción de compra. Devolución del 100% del valor pagado por la Garantía Extendida ABCDIN hasta 10 días corridos después de la compra.
                                                    </p>

                                                    <p class="textjustify">
                                                        9.- Beneficio de Asistencia Garantía Extendida.
                                                    </p>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p0 fade in" id="dosa">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    Sólo debes llamar al 800 380 911, o desde celulares al fono 2 2826 8992 o acudir a cualquier tienda ABCDIN, una vez que el producto presente una falla.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane p0 fade in" id="tresa">
                                <div class="container-fluid gris">
                                    <div class="row">
                                        <div class="container py-5">
                                            <div>
                                                <p class="textjustify">
                                                    <strong>¿SI ME ARREPIENTO DE LA COMPRA?</strong>
                                                </p>
                                                <p class="textjustify">
                                                    Puedes solicitar la devolución del 100% del valor pagado por la Garantía Extendida hasta 10 días corridos después de la Compra.
                                                </p>
                                                <br>
                                                <p class="textjustify">
                                                    <strong>¿SI COMPRÉ UN PRODUCTO SIN GARANTÍA EXTENDIDA?</strong>
                                                </p>
                                                <p class="textjustify">
                                                    Tienes un plazo para comprar la Garantía Extendida Abcdin de 30 días corridos, contados desde la fecha de compra del producto.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- fin tab 2 -->
    </div>
</section>
<!-- fin contenidos -->
<div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="garantiaModalAux" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    &times;
                </button>
            </div>
            <div class="modal-body contenidoModal"></div>
        </div>
    </div>
</div>
<!-- Modal garantias  -->
<div id="ModalGarantia" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModals px-2">
                <div class="infomodalserviClientes mb-3">
                    <h2>Garantía Asistencia</h2>
                    <p style="font-size: 14px;">(<span class="rojo">*</span>) Servicios de asistencia solo para Garantía Extendida.</p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive-sm">
                                    <table class="table table-reflow tablaServicios tablamodal tablagarantia">
                                        <thead>
                                            <tr>
                                                <th>Servicios Asistencia Garantía Extendida Normal</th>
                                                <th>Descripción</th>
                                                <th>Tope por evento</th>
                                                <th>Límite Eventos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Armado de muebles</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    La prestación del Servicio se realiza en horario hábil y se deberá coordinar con 48 horas de anticipación, el mueble debe contar con todas las partes y piezas. 
                                                    <br>Servicio no incluye traslado dentro y fuera de la cada del mueble armado, instalación, montaje y anclaje.
                                                    <br>El mueble no podrá tener una antiguedad superior a 1 año y deberá contar con el manual de armado.
                                                </td>
                                                <td>1,5 UF</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Visita Técnico</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Instalación de iluminarias:
                                                    <ul>
                                                        <li>
                                                            El servicio de instalación o reubicación de lámparas y apliqué se realzia en el lugar donde está la red eléctrica, no incluye realizar una nueva instalación eléctrica.
                                                        </li>
                                                    </ul>
                                                    Perforaciones menores:
                                                    <ul>
                                                        <li>
                                                            Incluye perforaciones en muro para cuadro o materiales decorativos, y adicionalmente incluye la instalacióin de estructura de televisores.
                                                        </li>
                                                    </ul>
                                                    Instalación de cortinas:
                                                    <ul>
                                                        <li>
                                                            Las cortinas y accesorios que necesiten ser reemplazadas o instaladas deben ser provistas o comprada por el beneficiario. La instalación se llevará a cabo siempre que existan las condiciones necesarias para su instalación.
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>1, UF</td>
                                                <td>1 a elección</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Visita Técnico.</a>
                                                </td>
                                                <td style="text-align: justify;">Visita Técnico para lavado de alfombra, limpieza campana extractora o limpieza vidrios de terrazas.</td>
                                                <td>2 UF</td>
                                                <td>1 a elección</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Mantención</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Mantención preventiva de refrigerador, lavadora o cocina.
                                                </td>
                                                <td>1,5 UF</td>
                                                <td>1 a elección</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Mantención de Calefont</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Esta mantención preventiva se llevará a cabo siempre y cuando el calefont se encuentre en funcionamiento.
                                                    <br>Queda excluído de la mantención preventiva el cambio o reparación de piexzas dañadas.
                                                    <br>La antigüedad del producto no deberá superar los 5 años.
                                                </td>
                                                <td>1,5 UF</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Mantención de calefont</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Si el cliente necesita realizar mantención preventiva del calefont de su domicilio, se pondrá a disposición de técnicos calificados para realizar la mantención de este.
                                                </td>
                                                <td>1,5 UF</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Limpieza de Cañon:</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Calefacción a combustión lenta/cocina a leña.
                                                </td>
                                                <td>2 UF</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Configuración:</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Configuración en línea de PC e impresoras.
                                                </td>
                                                <td>Ilimitado</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Servicio instalación prodcutos de Línea Blanca exclusivo para productos comprados con Garantía Extendida en Abcdin:</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Desde la compra de Garantía Extendida el plazo de corrdinación es de 2 días desde la compra.
                                                </td>
                                                <td>4</td>
                                                <td>1</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div id="ModalGarantia" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModals px-2">
                <div class="infomodalserviClientes mb-3">
                    <h2>Garantía Asistencia</h2>
                    <p style="font-size: 14px;">(<span class="rojo">*</span>) Servicios de asistencia solo para Garantía Extendida.</p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive-sm">
                                    <table class="table table-reflow tablaServicios tablamodal tablagarantia">
                                        <thead>
                                            <tr>
                                                <th>Asistencia</th>
                                                <th>Descripción</th>
                                                <th>Eventos al año</th>
                                                <th>Tope por evento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Servicio de armado de mueble</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Mano de obra para realizar armado de algún mueble nuevo que haya adquirido, para esto se enviará un técnico especialista para realizar la tareas del armado de muebles dentro de la vivienda del cliente, previa autorización del cliente y que estén dentro
                                                    de los montos y límites que se señalan más adelante.
                                                    <br>Los muebles respecto de los cuales se puede requerir el servicio son aquellos que en su compra adjuntaron un instructivo o catálogo original de armado o bien que sea provisto exclusivamente por el cliente,
                                                    o que son de complejidad su instalación.
                                                    <br>El servicio se limita a la mano de obra de un solo técnico por evento, si el servicio del cliente requiriera técnicos adicionales por las condiciones de armado del mueble, el costo de la mano de obra
                                                    adicional se otorgará a costo preferencial para el cliente.
                                                </td>
                                                <td>1</td>
                                                <td>1,5 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Maestro por un día a domicilio (Mano de obra)</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    El cliente podrá solicitar o agendar cuando necesite la mano de obra de un técnico de hogar para efectuar cualquiera de los siguientes trabajos:
                                                    <br>a) Plomería básica.
                                                    <br>b) Servicios de electricidad.
                                                    <br>c) Instalación o reubicación de insumos o electrodomésticos.
                                                </td>
                                                <td>1</td>
                                                <td>10 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Servicio de instalación de luminarias o cortinas o perforaciones en muro</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    El cliente podrá solicitar o agendar cuando necesite la mano de obra de un técnico hogar para efectuar cualquiera de los trabajos escogiendo uno a elección como: instalación de luminarias o lámparas o perforaciones en muro o instalación de cortinas.
                                                </td>
                                                <td>1</td>
                                                <td>1,5 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Servicio de limpieza: alfombras o campana extractora o vidrios de terraza</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    El cliente podrá solicitar o agendar cuando necesite la mano de obra de un técnico hogar para efectuar cualquiera de los trabajos escogiendo uno a elección como: limpieza de alfombras o limpieza de campana extractora de cocina o limpieza vidrios de terraza.
                                                </td>
                                                <td>1</td>
                                                <td>1,5 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Mantención preventiva de refrigerador o lavadora o cocina</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    El cliente podrá solicitar o agendar cuando necesite la mano de obra de un técnico hogar para efectuar cualquiera de los trabajos escogiendo uno a elección: mantención preventiva del refrigerador, lavadora o cocina.
                                                </td>
                                                <td>1</td>
                                                <td>1,5 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Mantención de calefont</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    Si el cliente necesita realizar mantención preventiva del calefont de su domicilio, se pondrá a disposición de técnicos calificados para realizar la mantención de este.
                                                </td>
                                                <td>1</td>
                                                <td>1,5 UF</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a class="enlace">Limpieza de cañón</a>
                                                </td>
                                                <td style="text-align: justify;">
                                                    El cliente podrá solicitar la limpieza preventiva del cañón de calefacción correspondiente al ducto de combustión lenta o cocina a leña de su domicilio.
                                                    <br>Este servicio incluye la limpieza desde el Castillón hacia el interior del cañón, despejando el conducto para la salida de los gases eliminando los residuos de cenizas, evitando así la obstrucción del
                                                    drenaje.
                                                </td>
                                                <td>1</td>
                                                <td>2 UF</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->