<link rel="stylesheet" href="/themes/abcdin/css/style.css" />
<link rel="stylesheet" href="/themes/abcdin/css/responsive.css" />
<script src="/themes/abcdin/js/function.js' ) }}"></script>
<section style="background-color: #999999; ">
    <div class="container clearfix" style="">
        <div id="zb_tree" style="background: none;">
            <div class="col_full center">
                <ul id="myTab2" class="nav nav-pills boot-tabs" style="display: inline-block !important;">
                    <li class=" toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/nuestra-empresa"  style="border-radius: 0px; width: auto!important;" class="button2">
                            <div class="">Nuestra Empresa</div>
                        </a>
                    </li>
                    <li class=" toptabsli toptabsliwidth" id="ropaid1" style="display: inline-block !important;">
                        <a href="/accionista"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Accionistas</div>
                        </a>
                    </li>
                    <li class=" toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/directores"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Directorio</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/infofinanciera"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Información Financiera</div>
                        </a>
                    </li>
                    <li class="active toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/organigrama"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Organigrama</div>
                        </a>
                    </li>
                    <!-- <li class="toptabsli"><a href="#ofertadi" data-toggle="tab" style="padding: 0px;border-radius: 8px; width: auto!important;" class="button2"><div class="iconos_izq3"><img alt="Seguro de Vida" src="img/Menu/blanco_saludPage_1.png"></div><div class="texto_mid3">SEGURO <span style="color:#de004a;">AUTOMOTRIZ</span></div></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<div id="myTabContent2" class="tab-content">
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix">
            <div class="col_full left" >
                <div style="width: 100%;margin: 0 auto; display:block; ">
                    <h3 style="color: #333333;">
                        <img src="/assets/media/icon-organigrama.png" alt="abcdin">Organigrama
                    </h3>
                </div>
            </div>
        </div>
        <div class="container clearfix" style="background-color: #fff; width: 70%; padding-bottom: 50px; padding-top: 50px; text-align: center;">
            <div class="col_full center">                    							
                <div style="background-color: #fff;">
                    <img src="/assets/media/img-organigrama.png">
                </div>
            </div>
        </div>
    </section>
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix" style="width: 70%; padding-left: 45px;" >
            <!-- Inicio Fila 1 de Gerencia -->
            <div class="col_full center">
                <div class="col_half center">
                    <div class="" style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 5px 20px; border-radius:15px;">
                    <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px; margin-top: 15px;">
                        <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente General</b></h3>
                        <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Alejandro Danús</p>
                    </div>
                    </div>
                </div>               
                <div class="col_half col_last center"  style="margin-bottom: 20px;">
                        <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Retail Dijon</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sra. Macarena Prieto Méndez<br>Licenciada en Artes</p>
                        </div>
                        </div>
                </div>
            </div>
            <!-- End of Fila 1 de Gerencia -->
            <!-- Inicio Fila 2 de Gerencia -->
            <div class="col_full center" >
                <div class="col_half center">
                    <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 5px 20px; border-radius:15px;">
                    <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px; margin-top: 15px;">
                        <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Finanzas</b></h3>
                        <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Rodrigo Libano</p>
                    </div>
                    </div>
                </div>               
                <div class="col_half col_last center" style="margin-bottom: 20px;">
                        <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Retail Financiero</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Gonzalo Ceballos Guzmán<br>Ingeniero Civil Industrial</p>
                        </div>
                        </div>
                </div>
            </div>
            <!-- End of Fila 2 de Gerencia -->
            <!-- Inicio Fila 3 de Gerencia -->
            <div class="col_full center" >
                <div class="col_half center">
                    <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Contraloría</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sra. Alejandra Bahamonde<br>Analista de Sistemas</p>
                        </div>
                    </div>
                </div>               
                <div class="col_half col_last center" style="margin-bottom: 20px;">
                    <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Retail Abcdin</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Andrés Cazorla Rivera<br>Ingeniero Civil Industrial</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Fila 3 de Gerencia -->
            <!-- Inicio Fila 4 de Gerencia -->
            <div class="col_full center" >
                <div class="col_half center">
                    <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Retail Abcdin</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sra. Lisa Pacholec Amed<br>Ingeniero Informático</p>
                        </div>
                    </div>
                </div>               
                <div class="col_half col_last center" style="margin-bottom: 20px;">
                        <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                            <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                                <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Operaciones</b></h3>
                                <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Fernando Andrade<br>Ingeniero Industrial y de Sistemas</p>
                            </div>
                        </div>
                </div>
            </div>
            <!-- End of Fila 4 de Gerencia -->
            <!-- Inicio Fila 5 de Gerencia -->
            <div class="col_full center" >
                <div class="col_half center">
                    <div style="display: table; background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Sistemas</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. José Antonio Caracci<br>Ingeniero Industrial Mecánica</p>
                        </div>
                    </div>
                </div>               
                <div class="col_half col_last center" style="margin-bottom: 20px;">
                    <div style="background-color: #fff; width: 450px; padding: 20px 10px 0px 20px; border-radius:15px;">
                        <div style="display: table-cell; background-color: #fff; border-left:5px solid #B2121D; padding-left:10px;">
                            <h3 style="color: #7F7F7F; text-align: left; margin-bottom: 5px; font-size: 20px;"><b>Gerente División Supply Chain</b></h3>
                            <p style="color: #7f7f7f; text-align: left; font-size: 16px;">Sr. Cristián Castro Morales<br>Ingeniero Civil Industrial</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Fila 5 de Gerencia -->
        </div>
    </section>
</div>