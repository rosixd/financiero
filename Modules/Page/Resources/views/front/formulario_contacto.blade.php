<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo</label>
                                        <input type="text" class="form-control px-3 pb-1" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS=">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut</label>
                                                <input type="text" class="form-control px-3 pb-1" id="rut" name="rut" placeholder="12.345.678-9">
                                                <div class="msg-notifi d-none">
                                                    <p><span class="rojo">Sr. Usuario, ingrese un rut valido</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono</label>
                                                <input type="text" class="form-control px-3 pb-1" id="telefono" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email</label>
                                        <input type="text" class="form-control  px-3 pb-1" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>                    

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar</label>
                                        <select class="form-control custom-select px-3 pb-1" id="tiposeguro" name="tiposeguro">
                                            <option>ELIGE TU SEGURO</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" name="mensaje" rows="5" placeholder="Comentarios="></textarea>
                                    </div>
                                    <button class="btn btn-primary btn-block btnRojo" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>