<link rel="stylesheet" href="/themes/abcdin/css/style.css" />
<link rel="stylesheet" href="/themes/abcdin/css/responsive.css" />
<script src="/themes/abcdin/js/function.js' ) }}"></script>
<section style="background-color: #999999; ">
    <div class="container clearfix" style="">
        <div id="zb_tree" style="background: none;">
            <div class="col_full center">
                <ul id="myTab2" class="nav nav-pills boot-tabs" style="display: inline-block !important;">
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/nuestra-empresa"  style="border-radius: 0px; width: auto!important;" class="button2">
                            <div class="">Nuestra Empresa</div>
                        </a>
                    </li>
                    <li class="active toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/accionista"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Accionistas</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/directores"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Directorio</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/infofinanciera"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Información Financiera</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/organigrama"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Organigrama</div>
                        </a>
                    </li>
                    <!-- <li class="toptabsli"><a href="#ofertadi" data-toggle="tab" style="padding: 0px;border-radius: 8px; width: auto!important;" class="button2"><div class="iconos_izq3"><img alt="Seguro de Vida" src="img/Menu/blanco_saludPage_1.png"></div><div class="texto_mid3">SEGURO <span style="color:#de004a;">AUTOMOTRIZ</span></div></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>

<div id="myTabContent2" class="tab-content">
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix">
            <div class="col_full left">
                <div style="width: 100%;margin: 0 auto; display:block; ">
                    <h3 style="color: #333333; font-weight:normal; text-align: center;">
                        <img src="/assets/media/icon-accionistas.png" alt="abcdin"><b>Cuadro de accionistas abcdin</b> / Listado de 12 principales accionistas
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
        <div class="container clearfix contenedor_tabla">
            <div class="col_full center">
                <table style="display: table;" class="table-responsive">
                    <thead>
                        <tr style="color: #B2121D; text-transform: uppercase; font-size: 18px; font-weight: bold;">
                            <th style="text-align: left; padding-left: 20px; width: 40%;">Accionistas</th>
                            <th style="text-align: center; width: 15%;">RUT</th>
                            <th style="text-align: center; width: 20%;">% de Participación</th>
                            <th style="text-align: center; width: 25%;">Total Accionistas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr style="color: #B2121D; text-transform: uppercase; font-size: 18px; font-weight: bold;">
                            <td height="30"></td>
                        </tr> -->
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Inversiones Baracaldo Ltda.</td>
                            <td>88.606.800-K</td>
                            <td>30,71%</td>
                            <td>668.069.313</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Cia de Rentas e Inversiones San Ignacio Comercial SPA.</td>
                            <td>96.854.110-2</td>
                            <td>14,93%</td>
                            <td>324.857.130</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Cia de Rentas e Inversiones San Ignacio Comercial Dos S.A.</td>
                            <td>76.920.050-9</td>
                            <td>14,93%</td>
                            <td>324.857.130</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Inversiones Nuena Consult Ltda.</td>
                            <td>96.730.307-6</td>
                            <td>4,00%</td>
                            <td>87.015.433</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Asesorías Varias e Inversiones Ltda.</td>
                            <td>89.126.200-0</td>
                            <td>3,86%</td>
                            <td>83.200.061</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Inversiones y Asesorías Josefa Ltda.</td>
                            <td>76.127.090-7</td>
                            <td>1,80%</td>
                            <td>39.200.061</td>
                        </tr>
                        <tr style="border-top: 1px solid #777; line-height: 80px;">
                            <td style="text-align: left; padding-left: 20px;">Inversiones y Asesorías La Villa Ltda.</td>
                            <td>78.156.760-4</td>
                            <td>0,90%</td>
                            <td>19.600.001</td>
                        </tr>
                        <tr style="border-top: 1px solid #fff; line-height: 80px; background-color: #B2121D; color: #fff;">
                            <td style="text-align: left; padding-left: 20px;">TOTALES</td>
                            <td></td>
                            <td>100%</td>
                            <td>2.175.384.617</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>