
<!-- modal requisitos de tarjeta -->
<div id="ModalRequisitos" class="modal fade levantarmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3 moduloRequisitosTarjeta">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-requisitos-tarjeta.png">
                    <h2 class="mensajeExito">REQUISITOS TARJETA ABCVISA</h2>
                    <ul>
                        <li>Persona natural</li>
                        <li>Nacionalidad Chilena o Extranjero con residencia definitiva (se puede acceder sin residencia definitiva pero debe comprobar 1 año de antigüedad laboral).</li>
                        <li>Cédula de identidad vigente y sin bloqueos.</li>
                        <li>Sin morosidades y/o protestos informados vigentes.</li>
                        <li>Domicilio Acreditado y Verificado.</li>
                        <li>Teléfono particular Acreditado de red fija o celular.</li>
                        <li>Cumplir con los niveles de aprobación previstos en los análisis de Riesgo utilizados por la empresa.</li>
                        <li>Encontrarse vigente en la promoción u oferta establecida por la empresa que permita la contratación de la tarjeta de crédito.</li>
                        <li>No poseer incumplimientos de deuda previos con ABCDIN ni DIJON.</li>
                        <li>Edad mínima: Dependiente 18 años, Independiente y Jubilado 24 años.</li>
                        <li>Edad máxima: 75 años y 364 días.</li>
                        <li>Acreditar un ingreso suficiente y estable en el tiempo.</li>
                        <li>Hablar y entender el idioma español.</li>
                        <li>Acreditar toda la información precedente de manera oportuna y exacta mediante la entrega de la documentación permitida para cada caso.</li>
                    </ul>
            
                    <p class="text-left"><strong>ABCDIN se reserva el derecho de realizar modificaciones a su Política de Crédito en el momento que estime necesario. </strong> </p>
            
                    <h3>REQUISITOS ESPECIFICOS</h3>
            
                    <h5>Clientes con Acreditación de Ingresos</h5>
                    <ul>
                        <li>Dependientes: Edad mayor o igual a 18 años y hasta 75 con 364 días.</li>
                        <li>Renta mínima acreditada de 100.000</li>
                        <li>Antigüedad mínima de 6 meses.</li>
                    </ul>
            
                    <h5>Independientes:</h5>
            
                    <ul>
                        <li>Edad mayor o igual a 24 años y 75 con 364 días.</li>
                        <li>Ingresos mínimos acreditados de 100.000</li>
                        <li>Antigüedad en el giro mínima de 1 año.</li>
                    </ul>
            
                    <h5>Jubilado - Pensionado:</h5>
            
                    <ul>
                        <li>Edad mayor o igual a 24 años y 75 con 364 días</li>
                        <li>Pensión mínima acreditada de 80.000</li>
                    </ul>
            
                    <p class="text-left">Nota: Dependiendo del Flujo de Solicitudes nuestro proceso de evaluación y verificación puede tomar hasta 45 días.</p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>