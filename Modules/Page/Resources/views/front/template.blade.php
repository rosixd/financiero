@extends('layouts.master')

@section('content')
    @component('page::components.slider', [
        'img' => [
            'slide-micuenta.jpg',
            'slide-micuenta.jpg'
        ]
    ])
    @endcomponent
    @component('page::components.barra_saludo', [
        'datospersonales' => $datospersonales
    ])

    @endcomponent
    @component('page::components.barra_info_titular', [
        'nombrecompleto' => $datospersonales['datos_usuario']['PrimerNombre'] . ' ' . $datospersonales['datos_usuario']['PrimerApellido'],
        'tarjeta' => $tarjetaseleccionada['ultimostarjeta'],
        'cupostarjetas' => $cupostarjetas
	
    ])
    @endcomponent
    @component('page::components.barra_navegacion_titular')
    @endcomponent

    @yield('contentPublic')

    @component('page::components.barra_banners', [
        'banner' => [
            (object)[
                'img' => 'banner-uso-responsable.jpg',
                'url' => '#'
            ],
            (object)[
                'img' => 'banner-beneficios2.jpg',
                'url' => '#'
            ]
        ]
    ])
    @endcomponent
@endsection
