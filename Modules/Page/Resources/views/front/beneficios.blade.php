<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slider-beneficios-100.jpg" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/assets/media/slider-beneficios-100.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- contenidos -->
<section class="Banners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p0">
                <a href="/tarjeta-app" class="bannerAnchoCompleto bgAzul">
                    <img src="/assets/media/banner-beneficios.jpg" class="img-fluid">
                </a>
            </div>
        </div>

    </div>
</section>
<!-- selector de beneficios -->
<section class="bannersTabs">
    <div class="container-fluid">
        <div class="row">
            <div class="board">
                <div class="board-inner">
                    <ul class="nav nav-tabs justify-content-around align-content-center selectorBeneficios" id="myTab">
                        <li>
                            <a href="#entretencion" data-toggle="tab">
                                <img class="entretencion" src="/assets/media/icono-entretencion.png">
                                <span class="round-tabs two">Entretención</span>
                            </a>
                        </li>
                        <li>
                            <a href="#belleza" data-toggle="tab">
                                <img class="belleza" src="/assets/media/icono-belleza.png">
                                <span class="round-tabs three">Belleza y Salud</span>
                            </a>
                        </li>
                        <li>
                            <a href="#vestuario" data-toggle="tab">
                                <img class="vestuario" src="/assets/media/icono-vestuario.png">
                                <span class="round-tabs four">Vestuario y calzado</span>
                            </a>
                        </li>
                        <li>
                            <a href="#ofertas" data-toggle="tab">
                                <img class="ofertas" src="/assets/media/icono-ofertas.png">
                                <span class="round-tabs five">Ofertas Web</span>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#ayd" data-toggle="tab">
                                <img class="ayd" src="/assets/media/icono-ayd.png">
                                <span class="round-tabs five">Abcdin y Dijon</span>
                            </a>
                        </li> -->
                        <li>
                            <a href="#equipamiento" data-toggle="tab">
                                <img class="equipamiento" src="/assets/media/icono-equipamiento.png">
                                <span class="round-tabs five">Equipamiento y hogar</span>
                            </a>
                        </li>

                        <li>
                            <a href="#todos" data-toggle="tab" class="active show">
                                <img class="todos" src="/assets/media/icono-todos.png">
                                <span class="round-tabs one">Todos</span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>

                <div class="tab-content">

                    <div class="tab-pane fade in active" id="todos">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalUnoSalud" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/uno-salud-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalUnimarc" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/unimarc-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalTickTicketpro" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/ticketpro-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalSalcobrand" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/salcobrand-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalRecorrido" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/recorrido-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalRecargafacil" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/recargafacil-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalPreunic" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/preunic-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalMercadolibre" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/mercadolibre-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalBata" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/bata-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalKFC" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/kfc-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalJetsmart" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/jetsmart-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalHoyts" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalPacific" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/pacific-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalBencineras" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/bencineras-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="entretencion">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalHoyts" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalTicketpro" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/ticketpro-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalKFC" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/kfc-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalPacific" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/pacific-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="belleza">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalUnoSalud" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/uno-salud-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalPreunic" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/preunic-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalSalcobrand" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/salcobrand-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="vestuario">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalBata" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/bata-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="ofertas">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalMercadolibre" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/mercadolibre-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalRecorrido" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/recorrido-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalRecargafacil" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/recargafacil-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalJetsmart" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/jetsmart-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="tab-pane fade" id="ayd">
                            <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                            </div>

                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                                <div class="col-lg-4">
                                <a href="#ModalBeneficios" role="button" data-toggle="modal" class="bannerHome" >
                                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                                </a>
                                </div>
                            </div>
                        </div>

                    </div> -->

                    <div class="tab-pane fade" id="equipamiento">
                        <div class="container-fluid">
                            <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalUnimarc" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/unimarc-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-4">
                                    <a href="#ModalConstrumart" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/construmart-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                                <!-- <div class="col-lg-4">
                                    <a href="#ModalBencineras" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/bencineras-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div> -->
                            </div>
                            <!-- <div class="row filaBanner">
                                <div class="col-lg-4">
                                    <a href="#ModalPedrodeValdivia" role="button" data-toggle="modal" class="bannerHome">
                                        <img src="/assets/media/pedrodevaldivia-minibox.jpg" class="img-thumbnail img-fluid">
                                    </a>
                                </div>
                            </div> -->
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<!-- fin contenidos -->

<div id="ModalBeneficios" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">

                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/banner-beneficio-ejemplo.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="ModalHoyts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/hoyts-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalTicketpro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/ticketpro-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalUnoSalud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/uno-salud-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalUnimarc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/unimarc-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalSalcobrand" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/salcobrand-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalRecorrido" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/recorrido-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalRecargafacil" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/recargafacil-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalMercadolibre" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/mercadolibre-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalMaicao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/maicao-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalKFC" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/kfc-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalJetsmart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/jetsmart-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalPedrodeValdivia" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/pedrodevaldivia-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalConstrumart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/construmart-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalBencineras" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/bencineras-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalPreunic" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/preunic-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalBata" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/bata-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalPacific" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/pacific-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>