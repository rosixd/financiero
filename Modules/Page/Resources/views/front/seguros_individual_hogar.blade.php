<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img alt="First slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div>

        <!-- <div class="carousel-item">
            <img alt="Second slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>

<!-- fin slider central -->

<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular SeguroIndividual">
        <div class="row justify-content-around align-content-center">
                <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-vida">Vida</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-vida">Vida</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-salud">Salud</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-salud">Salud</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-hogar_1.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a class="active" href="/seguros-hogar">Hogar</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-hogar_1.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a class="active" href="/seguros-hogar">Hogar</a>
                            </h1>
                        </div>
                    </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->

<section class="BarraSubmenu">
    <div class="container">
        <ul id="tabsJustified" class="nav nav-tabs justify-content-around">
            <li class="nav-item">
                <a href="/" data-target="#infoseguroHogar" data-toggle="tab" class="nav-link small text-uppercase active">
                    <img class="hogar" src="/assets/media/icono-hogar-bonificacion.png"> Hogar
                </a>
            </li>
        </ul>
    </div>
</section>


<section class="container-fluid p0">
    <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
        <!-- tab 1 -->
        <div id="infoseguroHogar" class="tab-pane p0 fade active show">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro que protege tu hogar frente a incendios y robo contenido, además, te bonifica con un 30% de lo pagado si no lo usas al tercer año.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$5.300</p>
                            <p class="uf">UF 0,1978</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSHcondicioneslegales" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>

                                <table class="table table-responsive mt-3">
                                    <thead>
                                        <tr>
                                            <th>Cobertura</th>
                                            <th>Póliza</th>
                                            <th>Capital Asegurado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Incendio contenido</td>
                                            <td>POL 1 2013 1384 (Letra A)</td>
                                            <td>UF 200</td>
                                        </tr>
                                        <tr>
                                            <td>Robo Contenido</td>
                                            <td>POL 1 2013 1384 (Letra B)</td>
                                            <td>UF 50</td>
                                        </tr>
                                        <tr>
                                            <td>Prima Mensual</td>
                                            <td></td>
                                            <td>UF 0,1978 ( $5.300)</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">
                                    <a href="#ModalFormularioContactoSeguroInfoSeguroHogar" role="button" data-toggle="modal" class=" btn_solicitalo btn btn-primary btn-lg btnRojo">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#uno" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dos" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tres" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatro" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="uno">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Cobertura para el contenido de casas y/o departamentos de uso habitacional (para riesgos urbanos y rurales). Además, cuenta con una completa asistencia domiciliaria. </p>
                                                    <p class="textjustify">Seguro cuenta con cobertura inmediata, no requiere inspección del inmueble y sin límite de antigüedad para la propiedad. dirigido a propietarios, arrendatarios y deudores hipotecarios. </p>
                                                    <p class="textjustify">Incluye el beneficio de "bonificación del 30%", que consiste en el reembolso de las primas efectivamente pagadas por haber permanecido vigente (y de manera ininterrumpida), durante todo el periodo de
                                                        vigencia de treinta y seis (36) meses, contados desde la aceptación de su solicitud de incorporación. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <p>Este Seguro incluye beneficio adicional de Asistencia Hogar.</p>
                                                    <ul class="p0">
                                                        <li>Servicio de Plomería: Limite de UF 2,5 por evento, máximo 2 eventos por cada año de vigencia.</li>
                                                        <li>Servicio de Cerrajería: Limite de UF 2,5 por evento, máximo 2 eventos por cada año de vigencia.</li>
                                                        <li>Servicio de vidriería: Limite de UF 2,5 por evento, máximo 2 eventos por cada año de vigencia.</li>
                                                        <li>Servicio electricista: Limite de UF 2,5 por evento, máximo 2 eventos por cada año de vigencia.</li>
                                                        <li>Visita Técnico Hogar Reparación: Cliente podrá solicitar luego de 3 meses de permanencia en el seguro, el servicio:
                                                            <ul>
                                                                <li style="border-bottom: 0px !important;">Reparaciones menores en refrigerador, lavadora y calefón.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <p>Límite de UF 1,5 por evento, máximo 1 evento por cada año de vigencia.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por BCI Seguros Generales S.A (www.bciseguros.cl), mediante póliza colectiva N° WP8619443-0 contratada por Distribuidora de Industrias Nacionales S.A. La información presentada es solo un resumen de las principales características
                                                        del Seguro Hogar con Bonificación y no reemplaza a la póliza.
                                                    </p>
                                                    <p class="textjustify">
                                                        Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 120131384.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,1978 IVA incluido, precio referencia en pesos $5.373, calculado con valor de UF $27.161,48,94, UF fecha 01/07/2018. Oferta de Seguro válida
                                                        al 01 de diciembre de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade p0 in" id="dos">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <ul>
                                                        <li class="textjustify">Los daños a la materia asegurada que hayan sido efectuados en forma deliberada por el asegurado o el beneficiario, ya sea directa o indirectamente. </li>
                                                        <li class="textjustify">Los daños materiales que directa o indirectamente tuvieren por origen o fueren consecuencia de guerra; invasión; actos de enemigos extranjeros; hostilidades u operaciones bélicas, sea que haya habido
                                                            o no declaración de guerra; guerra civil, insurrección, sublevación, rebelión, sedición, motín o hechos que las leyes califican como delitos contra la seguridad del Estado.
                                                            <br><br>
                                                            <p>Este seguro no cubre los daños que sufran los siguientes bienes o materias:</p>
                                                            <li>Los vehículos motorizados terrestres, aéreos o marítimos, incluyendo sus accesorios o remolques.</li>
                                                            <li>Los efectos de comercio, tales como dinero, cheques, bonos, acciones, letras y pagarés.</li>
                                                            <li>Los animales terrestres, aves o peces.</li>
                                                            <br><br>
                                                            <p>Exclusiones Cobertura de Incendio:</p>
                                                            <li>Este seguro no cubre los objetos robados o hurtados durante o después del incendio.</li>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="tres">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <ul>
                                                        <li class="textjustify">Viviendas con chapas de seguridad en puertas exteriores.</li>
                                                        <li class="textjustify">Viviendas de uso habitacional.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="cuatro">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Dentro de un plazo de 30 días corridos, contados desde la fecha de ocurrencia del siniestro, el asegurado u otro legítimo interesado deberá ir directamente a cualquier sucursal ABCDIN o dar aviso de
                                                        los hechos a BCI Seguros Generales S.A., al teléfono 600 6000 292 y desde celulares 02 267 997 00 por internet en www.bci.cl o, indicando día, lugar, hora y circunstancias en que ocurrió el siniestro,
                                                        sus causas y toda otra información que a juicio asegurado y/o la compañía sea relevante.
                                                    </p>
                                                    <p class="textjustify">Con esta información la compañía de seguros, procederá a realizar todos los trámites necesarios para la liquidación del siniestro y comunicará la aprobación o rechazo del mismo. Sin perjuicio de lo anterior,
                                                        la compañía aseguradora podrá solicitar cualquier otro antecedente que sea necesario para poder analizar y liquidar el respectivo siniestro.
                                                    </p>
                                                    <ul>
                                                        <li>Dejar constancia policial de los hechos en forma inmediata en caso de incendio, denuncia en caso de robo y denuncia en bomberos en caso de incendio.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 1 -->
    </div>
</section>

<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoSeguros">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-sm py-3">
                    <a href="#">
                        <img src="/assets/media/bannericonseguros.png" style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->

<!-- Modales -->

<!-- Modal seguro hogar condiciones legales-->
<div id="ModalSHcondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-hogar2.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por BCI Seguros Generales S.A (www.bciseguros.cl), mediante póliza colectiva N° WP8619443-0 contratada por Distribuidora de Industrias Nacionales S.A.
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del Seguro Hogar con Bonificación y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 120131384.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,1978 IVA incluido, precio referencia en pesos $5.300, calculado con valor de UF $27.161,48,94, UF fecha 01/07/2018. Oferta de Seguro válida a Diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguroInfoSeguroHogar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto" class="formulario formValida">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control px-3 pb-1 soloLetras requerido" maxlength="100" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS">
                                    </div>
                                    <input type="hidden" value="LandingContato" name="formulario">
                                    <div class= "clearfix"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="rut" name="rut" placeholder="12.345.678-9">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" id="telefono" maxlength="20" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control  px-3 pb-1 requerido" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar  <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposeguro" name="tiposeguro">
                                            <option disabled selected>Elige tu seguro</option>
                                            <option value="01">Seguro Vida Avance</option>
                                            <option value="02">Seguro Catastrófico familiar</option>
                                            <option value="03">Seguro Salud Dental</option>
                                            <option value="04">Seguro Escolaridad</option>
                                            <option value="05">Seguro Vida</option>
                                            <option value="06">Seguro Cesantía Cuentas Básicas</option>
                                            <option value="07">Seguro Contra robo con bonificación</option>
                                            <option value="08">Seguro Hogar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposolicitud">Tipo de solicitud <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposolicitud" name="tiposolicitud">
                                            <option disabled selected>Elige tu tipo de solicitud</option>
                                            <option value="consulta">Consulta</option>
                                            <option value="reclamo">Reclamo</option>
                                            <option value="masinfo">Más información comercial</option>
                                        </select>
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" maxlength="5000" name="mensaje" rows="5" placeholder="Comentarios"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 msjval" style="text-align: justify;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: justify;">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="btn btn-primary btn-block btnRojo" id="botonformulariocontacto" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal de respuesta -->
<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-info-mensaje.png">
                    <h2 class="mensajeExito">Hemos recibido tu solicitud.<br> Nos contactaremos contigo en menos de 48 horas hábiles.
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal de respuesta error -->
<div id="ModalRespuestaSolicituderror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    Error
                </div>
            </div>
        </div>
    </div>
</div>