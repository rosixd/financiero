@extends('layouts.master')

@section('content')
    @component('page::components.slider', [
        'img' => [
            'slide1.jpg',
            'slide1.jpg'
        ]
    ])
    @endcomponent
    @component('page::components.barra_superior', [
        'card' => [
            (object)[
                'img' => 'icono_tarjeta_visa.png',
                'width' => '85',
                'height' => '55',
                'titulo' => 'Quiero mi tarjeta!',
                'descripcion' => 'Cientos de beneficios'
            ],
            (object)[
                'img' => 'icono_app.png',
                'width' => '49',
                'height' => '82',
                'titulo' => 'Conoce nuestra APP!',
                'descripcion' => 'Todo en la palma de tu mano'
            ]
        ]
    ])
    @endcomponent
    @component('page::components.barra_beneficios', [
        'titulo' => 'CONOCE TODOS LOS BENEFICIOS'
    ])
    @endcomponent
    @include('page::front.home.beneficios')
@endsection
