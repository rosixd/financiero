<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img alt="First slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div>

        <!-- <div class="carousel-item">
            <img alt="Second slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular SeguroIndividual">
        <div class="row justify-content-around align-content-center">
                <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-vida">Vida</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-vida">Vida</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-salud_1.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a class="active" href="/seguros-salud">Salud</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-salud_1.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a class="active" href="/seguros-salud">Salud</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-hogar">Hogar</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-hogar">Hogar</a>
                            </h1>
                        </div>
                    </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->
<section class="BarraSubmenu">
    <div class="container">
        <ul id="tabsJustified" class="nav nav-tabs justify-content-around">
            <li class="nav-item">
                <a href="/" data-target="#infoseguroSalud" data-toggle="tab" class="nav-link small text-uppercase active">
                    <img class="salud" src="/assets/media/icono-individual-salud.png"> Salud Dental
                </a>
            </li>
            <li class="nav-item">
                <a href="/" data-target="#infoseguroSaludCatastrofico" data-toggle="tab" class="nav-link small text-uppercase">
                    <img class="salud" src="/assets/media/icono-individual-salud.png"> Catastrófico Familiar
                </a>
            </li>
        </ul>
    </div>
</section>


<section class="container-fluid p0">
    <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
        <!-- tab 1 -->
        <div id="infoseguroSalud" class="tab-pane p0 fade active show">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro entrega protección ante Lesiones Dentales por accidente de UF 3,5 (hasta 2 eventos anuales) y asistencia dental al 100%.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$7.990</p>
                            <p class="uf">UF 0,294</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSScondicioneslegales" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>
                                <div class="table-responsive-sm">
                                    <table class="table mt-3">
                                        <thead>
                                            <tr>
                                                <th>Coberturas</th>
                                                <th>Póliza</th>
                                                <th>Plan 1 (Titular)</th>
                                                <th>Plan 2 (Titular + Carga)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Lesiones Dentales</td>
                                                <td>POL 320130511</td>
                                                <td>UF 3,5</td>
                                                <td>UF 3,5</td>
                                            </tr>
                                            <tr>
                                                <td>Prima Mensual</td>
                                                <td></td>
                                                <td>UF 0,294 ( $7.990)</td>
                                                <td>UF 0,405 ( $10.990)</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">
                                    <a href="#ModalFormularioContactoSeguroInfoSeguroSalud" role="button" data-toggle="modal" class=" btn_solicitalo btn btn-primary btn-lg btnRojo">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#uno" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dos" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tres" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatro" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="uno">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Seguro entrega cobertura ante Lesiones Dentales por accidente de UF 3,5 (hasta 2 eventos anuales) y asistencia dental al 100%, de acuerdo al siguiente detalle: </p>
                                                    <ul>
                                                        <li>Urgencia: Diagnóstico de urgencia dental y derivación a especialista.</li>
                                                        <li>Prevención: Rayos X (Periapicales para diagnóstico), Examen clínico y diagnóstico.</li>
                                                        <li>Endodoncia: Endodoncia en dientes anteriores.</li>
                                                        <li>Operatoria.</li>
                                                        <li>Cirugía Oral: Exodoncia simple.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <p><strong>Este seguro incluye para ambos planes:</strong>
                                                    </p>
                                                    <p><strong>Asistencia Dental al 100%</strong>
                                                    </p>
                                                    <div class="table-responsive-sm">
                                                        <table class="table mt-3">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 70%;">Asistencia Dental</th>
                                                                    <th>Copagos Máximos</th>
                                                                    <th>Límite Anual</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="textjustify">a) Urgencia: Diagnóstico de urgencia dental y derivación a especialista, Ferulización en caso de trauma dientes anteriores, Retiro de cuerpo extraño, Tratamiento de pericoronaritis aguda,
                                                                        Tratamiento de gingivitis úlcero necrótica aguda, Radiografía pieza afectada (periapical), Alivio de oclusión (diente sintomático), Colocación de cemento temporal, Drenaje de absceso
                                                                        intraoral, Trepanación de urgencia (pulpitis irreversible), Complicaciones post-exodoncia: hemorragia y alveolitis, Exodoncia a colgajo de urgencia (excluye terceros molares), Exodoncia
                                                                        simple de urgencia (excluye terceros molares), Tratamiento sintomático de aftas menores en cavidad oral.</td>
                                                                    <td>$0</td>
                                                                    <td>Sin Tope</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="textjustify">a) Prevención: Rayos X (Periapicales para diagnóstico), Examen clínico y diagnóstico, Profilaxis, Remoción de cálculos supra gingivales.</td>
                                                                    <td class="textjustify">$9.000 remoción de cálculos supragingiviales. Resto de prestaciones sin copago.</td>
                                                                    <td>
                                                                        <li>Remoción de cálculos supra gingiviales 1 anual.</li>
                                                                        <li>Profilaxs 1 al año.</li>
                                                                        <li>Resto prestaciones sin tope.</li>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="textjustify">c) Endodoncia: Endodoncia en dientes anteriores.</td>
                                                                    <td>Sin Copago</td>
                                                                    <td>Sin Tope</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="textjustify">d) Operatoria: Obturación resina simple pieza anterior o posterior, Obturación resina compuesta, pieza anterior o posterior, Obturación resina compleja, pieza anterior o posterior, Tratamiento
                                                                        de sensibilidad cervical sin cavidad (con ionomeros solo en caso de sensibilidad), Resina cervical, pieza anterior o posterior (lesiones por caries, erosiones o abrasiones).</td>
                                                                    <td>$9.000</td>
                                                                    <td>4 obturaciones</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="textjustify">a) Cirugía Oral: Exodoncia simple (excluye terceros molares), exodoncia a colgajo (excluye terceros molares).</td>
                                                                    <td>$0</td>
                                                                    <td>Sin Tope</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 276 y 277 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                                    </p>
                                                    <p class="textjustify">
                                                        La información presentada es solo un resumen de las principales características del Seguro Salud Dental y no reemplaza a la póliza. Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva
                                                        y en las Condiciones Generales depositadas en Comisión para el Mercado FInanciero (CMF) bajo códigos POL 320130511.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,294 IVA incluido, precio referencia en pesos $7.990, calculado con valor de UF $27.161,48, UF fecha 01/1/2018. Oferta de Seguro válida al 01
                                                        de diciembre de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade p0 in" id="dos">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Los tratamientos estéticos plásticos, dentales, de ortodoncia, ortopédicos y otros tratamientos que sean para fines de embellecimiento o para corregir malformaciones producidas por enfermedades o accidentes
                                                        anteriores a la fecha de vigencia de esta póliza. </p>
                                                    <p class="textjustify">Participación del asegurado en guerras o actos delictivos.</p>
                                                    <p class="textjustify">Acto delictivo cometido, en calidad de autor o cómplice, por un beneficiario o quien pudiese reclamar la cantidad asegurada o la indemnización. </p>
                                                    <p class="textjustify">Participación activa del Asegurado en acto terrorista.</p>
                                                    <p class="textjustify">Participación del Asegurado en actos temerarios y deportes riesgosos.</p>
                                                    <p class="textjustify">Encontrarse el asegurado en estado de ebriedad, o bajo los efectos de cualquier narcótico a menos que hubiese sido administrado por prescripción médica.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="tres">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Las edades de cobertura para fallecimiento e Invalidez Total y permanente 2/3 son las siguientes:</p>
                                                    <p class="textjustify">Edad mínima de ingreso: 18 años.</p>
                                                    <p class="textjustify">Edad máxima de ingreso: 65 años y 364 días.</p>
                                                    <p class="textjustify">Edad máxima de permanencia: 69 años y 364 días.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="cuatro">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Para la cobertura de Lesiones Dentales: Copia de cédula de identidad o certificado de nacimiento, documentación que acredite la ocurrencia de un accidente y boletas que acrediten el gasto incurrido.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 1 -->
        <!-- tab 2 -->
        <div id="infoseguroSaludCatastrofico" class="tab-pane p0 fade">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro entrega protección frente a Fallecimiento, enfermedades graves e intervenciones.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$6.000</p>
                            <p class="uf">UF 0,221</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSScondicioneslegales" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>
                                <div class="table-responsive-sm">
                                    <table class="table mt-3">
                                        <thead>
                                            <tr>
                                                <th>Coberturas</th>
                                                <th>Plan 1</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Fallecimiento</td>
                                                <td>UF 100</td>
                                            </tr>
                                            <tr>
                                                <td>Enfermedades graves e intervenciones</td>
                                                <td>UF 300</td>
                                            </tr>
                                            <tr>
                                                <td>Cónyuge e hijos</td>
                                                <td>UF 300</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>
                                <div class="clearfix"></div>
                                <div class="py-5 text-center">
                                    <a href="#ModalFormularioContactoSeguroInfoSeguroSalud" role="button" data-toggle="modal" class=" btn_solicitalo btn btn-primary btn-lg btnRojo">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#unoa" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dosa" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tresa" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatroa" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="unoa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Seguro entrega protección frente al diagnóstico de una de las siguientes enfermedades graves para el asegurado titular y su grupo familiar: Infarto Cardíaco, Derrame Cerebral, Insuficiencia renal, Parálisis
                                                        Múltiple. Además de intervenciones quirúrgicas: Cirugía de Válvulas cardíacas, cirugía de cerebro por aneurisma/o tumores, cirugía de angioplastia de dos o más arterias.</p>
                                                    <p class="textjustify">Adicionalmente entrega cobertura al asegurado titular por fallecimiento o muerte accidental.</p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Este seguro no está sujeto a DPS (Declaración Personal de Salud) y no tendrá carencias, enfermedades preexistentes NO tendrán cobertura.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <p>Chequeo Preventivo Anual, que incluye los siguientes exámenes: Hemograma, Perfil Lipídico, Perfil Bioquímico, Orina Completa, Urocultivo, T3, T4, TSH, Radiografía de Tórax. (Sin tope por evento, 2 eventos
                                                        al año).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 272 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                                    </p>
                                                    <p class="textjustify">
                                                        La información presentada es solo un resumen de las principales características del Seguro Catastrófico Familiar y no reemplaza a la póliza. Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza
                                                        colectiva y en las Condiciones Generales depositadas en la en Comisión para el Mercado Financiero (CMF) bajo códigos POL 220130289 y CAD320130290.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,221 IVA incluido, precio referencia en pesos $6.000, calculado con valor de UF $27.161,48, UF fecha 01/07/2018. Oferta de Seguro válida al 01
                                                        de diciembre de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade p0 in" id="dosa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <ul>
                                                        <li>Alcoholismo o drogadicción.</li>
                                                        <li>Cánceres a la piel excepto melanomas malignos.</li>
                                                        <li>Síndrome de Inmunodeficiencia Adquirida, SIDA.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="tresa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Las edades de cobertura para fallecimiento e Invalidez Total y permanente 2/3 son las siguientes:</p>
                                                    <ul>
                                                        <li>Edad mínima de ingreso: 18 años.</li>
                                                        <li>Edad máxima de ingreso: 65 años y 364 días.</li>
                                                        <li>Edad máxima de permanencia: 69 años y 364 días.</li>
                                                        <li>Cargas hasta los 24 años.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="cuatroa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>En caso de siniestro, el asegurado o los beneficiarios deberán realizar la declaración del siniestro en cualquiera de las tiendas ABCDIN, en un plazo máximo de 180 días contados desde ocurrido.
                                                        Deberá presentar la siguiente documentación:
                                                    </p>
                                                    <ul>
                                                        <strong>FALLECIMIENTO:</strong>
                                                        <li>Certificado Original de Nacimiento.</li>
                                                        <li>Certificado Original de Defunción.</li>
                                                        <li>Fotocopia cédula de Identidad del asegurado y beneficiario.</li>
                                                        <li>Copia de parte policial e informe de autopsia en caso de muerte accidental.</li>
                                                    </ul>
                                                    <br>
                                                    <ul>
                                                        <strong>ENFERMEDADES GRAVES:</strong>
                                                        <li>Certificado Original de Nacimiento asegurado.</li>
                                                        <li>Fotocopia de la cédula de identidad del asegurado.</li>
                                                        <li>Informe del médico tratante y/o ficha clínica.</li>
                                                    </ul>
                                                    <p>La compañía se reserva el derecho a solicitar mayores antecedentes si lo estima necesario.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 2 -->
    </div>
</section>

<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoSeguros">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-sm py-3">
                    <a href="#">
                        <img src="/assets/media/bannericonseguros.png" style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->

<!-- Modales -->

<!-- Modal seguro salud condiciones legales-->
<div id="ModalSScondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-salud2.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 276 y 277 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del Seguro Salud Dental y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en Comisión para el Mercado FInanciero (CMF) bajo códigos POL 320130511.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,294 IVA incluido, precio referencia en pesos $7.990, calculado con valor de UF $27.161,48, UF fecha 01/1/2018. Oferta de Seguro válida a Diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal de respuesta -->
<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-info-mensaje.png">
                    <h2 class="mensajeExito">Hemos recibido tu solicitud.<br> Nos contactaremos contigo en menos de 48 horas hábiles.
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal de respuesta error -->
<div id="ModalRespuestaSolicituderror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    Error
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguroInfoSeguroSalud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto" class="formulario formValida">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control px-3 pb-1 soloLetras requerido" maxlength="100" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS">
                                    </div>
                                    <input type="hidden" value="LandingContato" name="formulario">
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="rut" name="rut" placeholder="12.345.678-9">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" id="telefono" maxlength="20" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control  px-3 pb-1 requerido" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar  <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposeguro" name="tiposeguro">
                                            <option disabled selected>Elige tu seguro</option>
                                            <option value="01">Seguro Vida Avance</option>
                                            <option value="02">Seguro Catastrófico familiar</option>
                                            <option value="03">Seguro Salud Dental</option>
                                            <option value="04">Seguro Escolaridad</option>
                                            <option value="05">Seguro Vida</option>
                                            <option value="06">Seguro Cesantía Cuentas Básicas</option>
                                            <option value="07">Seguro Contra robo con bonificación</option>
                                            <option value="08">Seguro Hogar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposolicitud">Tipo de solicitud <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposolicitud" name="tiposolicitud">
                                            <option disabled selected>Elige tu tipo de solicitud</option>
                                            <option value="consulta">Consulta</option>
                                            <option value="reclamo">Reclamo</option>
                                            <option value="masinfo">Más información comercial</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" maxlength="5000" name="mensaje" rows="5" placeholder="Comentarios"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 msjval" style="text-align: justify;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: justify;">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="btn btn-primary btn-block btnRojo" id="botonformulariocontacto" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>