<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
        <ol class="carousel-indicators">
            <li class="active" data-slide-to="0" data-target="#carouselIndicators">&nbsp;</li>
            <li data-slide-to="1" data-target="#carouselIndicators">&nbsp;</li>
        </ol>
    
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active"><img alt="First slide" class="d-block w-100" src="http://abcdin.trunk.netred.cl/assets/media/slider-tarjeta-100.jpg" /></div>
    
            <div class="carousel-item"><img alt="Second slide" class="d-block w-100" src="http://abcdin.trunk.netred.cl/assets/media/slider-tarjeta-100.jpg" /></div>
        </div>
        <a class="carousel-control-prev" data-slide="prev" href="#carouselIndicators" role="button"><span class="sr-only">Previous</span> </a>
        <a class="carousel-control-next" data-slide="next" href="#carouselIndicators" role="button"> <span class="sr-only">Next</span> </a>
    </div>
    <!-- fin slider central -->
    <!-- barra superior -->
    
    <section class="barraSlide">
        <div class="container">
            <div class="row flex-parent">
                <div class="col-sm-6">
                    <div class="box">
                        <a data-toggle="modal" href="#ModalFormularioQuieroMiTarjeta" id="solicitarTarjetaMenu" style="text-decoration: none;"><img height="55" src="http://abcdin.trunk.netred.cl/assets/media/icono-tarjeta-visa.png" style="text-decoration: none;" width="85" /> </a>
                    </div>
    
                    <div class="box2">
                        <h1><a data-toggle="modal" href="#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">Quiero mi tarjeta! </a></h1>
    
                        <h2><a data-toggle="modal" href="#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">Cientos de beneficios </a></h2>
                    </div>
                </div>
    
                <div class="col-sm-6">
                    <div class="box">
                        <a href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;"><img height="82" src="http://abcdin.trunk.netred.cl/assets/media/icono-app_2.png" width="49" /> </a>
                    </div>
    
                    <div class="box2">
                        <h1><a href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;">Conoce nuestra APP </a></h1>
    
                        <h2><a href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;">Todo en la palma de tu mano </a></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- fin barra superior -->
    <!-- barra beneficios -->
    
    <section class="barraTitulos">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1><span>Descubre</span> que puedes hacer<br />
    con tu tarjeta <img src="http://abcdin.trunk.netred.cl/assets/media/tit-abcvisa.png" /></h1>
                </div>
            </div>
        </div>
    </section>
    <!-- fin barra beneficios -->
    <!-- contenidos -->
    
    <section class="contenidos" id="contenidos">
        <div class="container-fluid contenidoTarjeta">
            <div class="row">
                <div class="col-lg-4">
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/beneficios" style="text-decoration: none;">
                        <img src="http://abcdin.trunk.netred.cl/assets/media/icono-beneficios.png" /> 
                    </a>
    
                    <h2>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/beneficios" style="text-decoration: none;">Beneficios Exclusivos</a>
                    </h2>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/beneficios" style="text-decoration: none;"> </a>
    
                    <p>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/beneficios" style="text-decoration: none;">Puedes llegar a ahorrar hasta $35.000 mensual!<br />
                            <strong>Descubre c&oacute;mo</strong>
                        </a>
                    </p>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/beneficios" style="text-decoration: none;"> </a>
                </div>
    
                <div class="col-lg-4">
                    <a class="cajaBeneficio" href="https://www.abcdin.cl/" style="text-decoration: none;" target="_blank">
                        <img src="http://abcdin.trunk.netred.cl/assets/media/icono-compra.png" /> 
                    </a>
    
                    <h2>
                        <a class="cajaBeneficio" href="https://www.abcdin.cl/" style="text-decoration: none;" target="_blank">Compra online</a>
                    </h2>
                    <a class="cajaBeneficio" href="https://www.abcdin.cl/" style="text-decoration: none;" target="_blank"> </a>
    
                    <p>
                        <a class="cajaBeneficio" href="https://www.abcdin.cl/" style="text-decoration: none;" target="_blank">Con tu segunda Clave compra seguro en sitios nacionales, excluye Spotifi, Netflix, Uber.</a>
                    </p>
                    <a class="cajaBeneficio" href="https://www.abcdin.cl/" style="text-decoration: none;" target="_blank"> </a>
                </div>
    
                <div class="col-lg-4">
                    <a class="cajaBeneficio" style="text-decoration: none;"> 
                        <img src="http://abcdin.trunk.netred.cl/assets/media/icono-comercios.png" /> 
                    </a>
    
                    <h2><a class="cajaBeneficio" style="text-decoration: none;">Comercios asociados</a></h2>
                    <a class="cajaBeneficio" style="text-decoration: none;"> </a>
    
                    <p style="text-decoration: none;"><a class="cajaBeneficio" style="text-decoration: none;">Usa tu abcvisa en toda la red transbank del pa&iacute;s.</a></p>
                    <a class="cajaBeneficio" style="text-decoration: none;"> </a>
                </div>
            </div>
    
            <div class="row">
                <div class="col-lg-4">
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;">
                        <img src="http://abcdin.trunk.netred.cl/assets/media/icono-app_1.png" /> 
                    </a>
    
                    <h2>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;">Abcvisa APP</a>
                    </h2>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;"> </a>
    
                    <p>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;">Desc&aacute;rgala, paga tu cuenta, revisa tu cupo disponible, revisa tu estado de cuenta, recibe notificaciones y mucho m&aacute;s!</a>
                    </p>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/tarjeta-app" style="text-decoration: none;"> </a>
                </div>
    
                <div class="col-lg-4">
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/paga-tu-cuenta" style="text-decoration: none;" target="_blank"><img src="http://abcdin.trunk.netred.cl/assets/media/icono-pago.png" /> </a>
    
                    <h2>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/paga-tu-cuenta" style="text-decoration: none;" target="_blank">Opciones de pago</a>
                    </h2>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/paga-tu-cuenta" style="text-decoration: none;" target="_blank"> </a>
    
                    <p>
                        <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/paga-tu-cuenta" style="text-decoration: none;" target="_blank">Necesitas otras alternativas para pagar la deuda de este mes?</a>
                    </p>
                    <a class="cajaBeneficio" href="http://abcdin.trunk.netred.cl/paga-tu-cuenta" style="text-decoration: none;" target="_blank"> </a>
                </div>
            </div>
        </div>
    </section>
    
    <div class="spacer1">&nbsp;</div>
    
    <section class="Banners">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 p0">
                    <a class="bannerAnchoCompleto bgRosado" href="#" style="text-decoration: none;"><img class="img-fluid" src="http://abcdin.trunk.netred.cl/assets/media/banner-koke-1600x357.jpg" /> </a>
                </div>
            </div>
    
            <div class="row">
                <div class="col-lg-12 p0">
                    <a class="bannerAnchoCompleto bgGris" href="#" style="text-decoration: none;"><img class="img-fluid" src="http://abcdin.trunk.netred.cl/assets/media/banner-beneficios-1600x357.jpg" /> </a>
                </div>
            </div>
        </div>
    </section>
    <!-- fin contenidos -->