<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img alt="First slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div>

        <!-- <div class="carousel-item">
            <img alt="Second slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>

<!-- fin slider central -->

<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular SeguroIndividual">
        <div class="row justify-content-around align-content-center">
                <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-vida_1.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a class="active" href="/seguros-vida">Vida</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-vida_1.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a class="active" href="/seguros-vida">Vida</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-salud">Salud</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-salud">Salud</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-financiera2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm col-lg-2 col-2">
                        <div class="d-none d-sm-block">
                            <div class="box">
                                <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                                <h1>
                                    <a href="/seguros-hogar">Hogar</a>
                                </h1>
                            </div>
                        </div>
                        <div class="d-block d-sm-none text-center">
                            <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-hogar">Hogar</a>
                            </h1>
                        </div>
                    </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->

<section class="BarraSubmenu">
    <div class="container">
        <ul id="tabsJustified" class="nav nav-tabs justify-content-around">
            <li class="nav-item">
                <a href="/" data-target="#infoSeguroVida" data-toggle="tab" class="nav-link small text-uppercase active">
                    <img class="seguroVida" src="/assets/media/icono-seguro-vida.png"> Vida
                </a>
            </li>
            <li class="nav-item">
                <a href="/" data-target="#segurosEscolaridad" data-toggle="tab" class="nav-link small text-uppercase">
                    <img class="escolaridad" src="/assets/media/icono-escolaridad.png"> Escolaridad
                </a>
            </li>
        </ul>
    </div>
</section>


<section class="container-fluid p0">
    <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
        <!-- tab 1 -->
        <div id="infoSeguroVida" class="tab-pane p0 fade active show">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro que cubre en caso de fallecimiento, muerte Accidental e invalidez total y permanente 2/3 por accidente o enfermedad.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$5.500</p>
                            <p class="uf">UF 0,2025</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSVcondicioneslegales" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>

                                <table class="table table-responsive mt-3">
                                    <thead>
                                        <tr>
                                            <th>Coberturas</th>
                                            <th>Póliza</th>
                                            <th>Plan 1</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fallecimiento</td>
                                            <td>POL 220130289</td>
                                            <td>UF 150 ($4.074.222)</td>
                                        </tr>
                                        <tr>
                                            <td>Invalidez Total y Permanente 2/3</td>
                                            <td>CAD 320130293</td>
                                            <td>UF 300 ($8.148.444)</td>
                                        </tr>
                                        <tr>
                                            <td>Muerte Accidental</td>
                                            <td>CAD 320130292</td>
                                            <td>UF 200 ($5.432.296)</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">

                                    <a href="#ModalFormularioContactoSeguroSegurosEscolaridad" role="button" data-toggle="modal" class=" btn_solicitalo btn btn-primary btn-lg btnRojo">QUIERO SER CONTACTADO</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#uno" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dos" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tres" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatro" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="uno">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Fallecimiento por causas naturales y en caso de Invalidez Total y Permanente 2/3 provocado por accidente o enfermedad.</p>
                                                    <p class="textjustify"><span class="rojo">*</span> Este seguro no está sujeto a DPS (Declaración Personal de Salud) y no tendrá carencias, enfermedades preexistentes NO tendrán cobertura.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <ul class="p0">
                                                        <li>Descuento en farmacia cruz Verde. Con tope mensual de hasta $20.000.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 270 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                                    </p>
                                                    <p class="textjustify">
                                                        La información presentada es solo un resumen de las principales características del seguro de vida y no reemplaza a la póliza. Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva
                                                        y en las Condiciones Generales depositadas en la CMF bajo códigos POL 220130289, CAD 320130293 y CAD 320130292.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,2025 IVA incluido, precio referencia en pesos $5.500, calculado con valor de UF $27161,48, UF fecha 01/07/2018. Oferta de Seguro válida a Diciembre
                                                        de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade p0 in" id="dos">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Suicidio e intento de suicidio (excepto con pago de 1 año de prima ininterrumpida).</p>
                                                    <p class="textjustify">Pena de muerte o participación del asegurado en guerras o actos delictivos.</p>
                                                    <p class="textjustify">Acto delictivo cometido, en calidad de autor o cómplice, por un beneficiario o quien pudiese reclamar la cantidad asegurada o la indemnización.</p>
                                                    <p class="textjustify">Participación activa del Asegurado en acto terrorista.</p>
                                                    <p class="textjustify">Participación del Asegurado en actos temerarios y deportes riesgosos.</p>
                                                    <p class="textjustify">Fisión o fusión nuclear o contaminación radioactiva.</p>
                                                    <p class="textjustify">Encontrarse el asegurado en estado de ebriedad, o bajo los efectos de cualquier narcótico a menos que hubiese sido administrado por prescripción médica.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="tres">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Las edades de cobertura para fallecimiento e Invalidez Total y permanente 2/3 son las siguientes:</p>
                                                    <p class="textjustify">Edad mínima de ingreso: 18 años.</p>
                                                    <p class="textjustify">Edad máxima de ingreso: 65 años y 364 días.</p>
                                                    <p class="textjustify">Edad Máxima de Permanencia: 69 años y 364 días.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p0 in" id="cuatro">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Una vez ocurrido el siniestro, el asegurado o los beneficiarios deberán realizar la declaración del siniestro en cualquiera de las tiendas ABCDIN, en un plazo máximo de 180 días contados desde ocurrido.
                                                        Deberá presentar la siguiente documentación, junto con el formulario de notificación de siniestros.
                                                    </p>
                                                    <ul><strong>MUERTE ACCIDENTAL:</strong>
                                                        <li>Certificado Original de Nacimiento y Defunción.</li>
                                                        <li>Fotocopia cédula de Identidad</li>
                                                        <li>Parte policial si el sinistro ocurre a consecuencia de un accidente.</li>
                                                    </ul>
                                                    <br>
                                                    <ul><strong>INVALIDEZ TOTAL Y PERMANENTE 2/3:</strong>
                                                        <li>Certificado original de nacimiento.</li>
                                                        <li>Dictamen de invalidez.</li>
                                                        <li>Fotocopia cédula de Identidad.</li>
                                                        <li>Informe médico tratante.</li>
                                                        <li>Copia del parte policial, si la invalidez ocurre a raiz de un accidente.</li>
                                                    </ul>
                                                    <br>
                                                    <p>
                                                        La compañía se reserva el derecho a solicitar mayores antecedentes si lo estima necesario.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 1 -->
        <!-- tab 2 -->
        <div id="segurosEscolaridad" class="tab-pane p0 fade">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Asegura el pago anual de la educación de hasta dos (2) hijos en caso de fallecimiento, muerte accidental o invalidez total y permanente 2/3 (por enfermedad o Accidente).
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$9.990</p>
                            <p class="uf">UF 0,368</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSEcondicioneslegales" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>

                                <table class="table table-responsive mt-3">
                                    <thead>
                                        <tr>
                                            <th>Coberturas</th>
                                            <th>Póliza</th>
                                            <th>Plan 1</th>
                                            <th>Plan 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Renta Anual por Escolaridad</td>
                                            <td>POL 220160370</td>
                                            <td>UF 60</td>
                                            <td>UF 60</td>
                                        </tr>
                                        <tr>
                                            <td>Muerte Accidental</td>
                                            <td>CAD 320130642</td>
                                            <td>UF 60</td>
                                            <td>UF 60</td>
                                        </tr>
                                        <tr>
                                            <td>Invalidez Total y Permanente 2/3</td>
                                            <td>CAD 320130292</td>
                                            <td>UF 60</td>
                                            <td>UF 60</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Prima Mensual</strong></td>
                                            <td></td>
                                            <td><strong>UF 0,368 ($9.990)</strong></td>
                                            <td><strong>UF 0,549 ($14.900) </strong></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">
                                    <a href="#ModalFormularioContactoSeguroSegurosEscolaridad" role="button" data-toggle="modal" class=" btn_solicitalo btn btn-primary btn-lg btnRojo" style="text-decoration: none; box-shadow: none;">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#unoa" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dosa" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tresa" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatroa" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="unoa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Asegura el pago anual por carga de la educación de hasta 2 hijos que estén en edad escolar o estudien en Institutos profesionales o universidad hasta los 24 años.</p>
                                                    <p>Cubre en caso de fallecimiento, muerte Accidental e Invalidez total y permanente por accidente.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <ul class="p0">
                                                        <li>Atención médica de urgencia por evento traumatológico.</li>
                                                        <li>Traslado de urgencia en caso de accidente.</li>
                                                        <li>Radiografía de extremidad superior o inferior.</li>
                                                        <li>Procedimiento de urgencia.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 273 y 278 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                                    </p>
                                                    <p class="textjustify">
                                                        La información presentada es solo un resumen de las principales características del Seguro Escolaridad y no reemplaza a la póliza. Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva
                                                        y en las Condiciones Generales depositadas en Comisión para el Mercado Financiero (CMF) bajo códigos POL 220160370, CAD 320130642 y CAD 320130292.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,368 IVA incluido, precio referencia en pesos $9.990, calculado con valor de UF $27.161,48, UF fecha 01/1/2018. Oferta de Seguro válida a Diciembre
                                                        de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane p0 fade in" id="dosa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Suicidio (excepto con pago de 1 año de prima ininterrumpida).</p>
                                                    <p>Pena de muerte.</p>
                                                    <p>Participación de actos delictivos y/o temerarios.</p>
                                                    <p>Conducción en estado de ebriedad.</p>
                                                    <p>Pelas o riñas, entre otros.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="tresa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Las edades de cobertura para fallecimiento e Invalidez Total y permanente 2/3 son las siguientes:</p>
                                                    <p class="textjustify">Edad mínima de ingreso: 18 años.</p>
                                                    <p class="textjustify">Edad máxima de ingreso: 65 años y 364 días.</p>
                                                    <p class="textjustify">Edad Máxima de Permanencia: 69 años y 364 días.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="cuatroa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <ul>
                                                        <strong>FALLECIMIENTO:</strong>
                                                        <li>Certificado Original de Nacimiento.</li>
                                                        <li>Certificado Original de Defunción.</li>
                                                        <li>Fotocopia de la Cédula de Identidad del asegurado y beneficiario.</li>
                                                        <li>Copia de Parte Policial si el fallecimiento ocurre a raíz de un accidente.</li>
                                                    </ul>
                                                    <br>
                                                    <ul>
                                                        <strong>MUERTE ACCIDENTAL:</strong>
                                                        <li>Formulario de Notificación.</li>
                                                        <li>Certificado de notificación de siniestros.</li>
                                                        <li>Certificado original de nacimiento y defunción del asegurado.</li>
                                                        <li>Fotocopia de cédula de identidad.</li>
                                                        <li>Certificado original de nacimiento por cada Beneficiario.</li>
                                                        <li>Copia parte policial.</li>
                                                        <li>Copia de informe de alcoholemia.</li>
                                                        <li>Copia de informe de autopsia.</li>
                                                        <li>Estado de la causa.</li>
                                                    </ul>
                                                    <br>
                                                    <ul>
                                                        <strong>INVALIDEZ TOTAL Y PERMANENTE 2/3:</strong>
                                                        <li>Certificado Original de Nacimiento.</li>
                                                        <li>Dictamen de Invalidez.</li>
                                                        <li>Informe del Médico Tratante.</li>
                                                        <li>Copia del Parte Policial si la invalidez ocurre a raíz de un accidente.</li>
                                                        <li>Fotocopia de la Cédula de Identidad del Asegurado.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 2 -->
    </div>
</section>

<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoSeguros">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-sm py-3">
                    <a href="#">
                        <img src="/assets/media/bannericonseguros.png" style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->

<!-- Modales -->

<!-- Modal seguro vida condiciones legales-->
<div id="ModalSVcondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-vida.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 270 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del seguro de vida y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 220130289, CAD 320130293 y CAD 320130292.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,2025 IVA incluido, precio referencia en pesos $5.500, calculado con valor de UF $27161,48, UF fecha 01/07/2018. Oferta de Seguro válida al 01 de diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal seguro ESCOLARES condiciones legales-->
<div id="ModalSEcondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-vida.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 273 y 278 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del Seguro Escolaridad y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en Comisión para el Mercado Financiero (CMF) bajo códigos POL 220160370, CAD 320130642
                                    y CAD 320130292.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,368 IVA incluido, precio referencia en pesos $9.990, calculado con valor de UF $27.161,48, UF fecha 01/1/2018. Oferta de Seguro válida al 01 de diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguroSegurosEscolaridad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto" class="formulario formValida">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control px-3 pb-1 soloLetras requerido" maxlength="100" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS">
                                    </div>
                                    <input type="hidden" value="LandingContato" name="formulario">
                                    <div class= "clearfix"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="rut" name="rut" placeholder="12.345.678-9">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" id="telefono" maxlength="20" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control  px-3 pb-1 requerido" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar  <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposeguro" name="tiposeguro">
                                            <option disabled selected>Elige tu seguro</option>
                                            <option value="01">Seguro Vida Avance</option>
                                            <option value="02">Seguro Catastrófico familiar</option>
                                            <option value="03">Seguro Salud Dental</option>
                                            <option value="04">Seguro Escolaridad</option>
                                            <option value="05">Seguro Vida</option>
                                            <option value="06">Seguro Cesantía Cuentas Básicas</option>
                                            <option value="07">Seguro Contra robo con bonificación</option>
                                            <option value="08">Seguro Hogar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposolicitud">Tipo de solicitud <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposolicitud" name="tiposolicitud">
                                            <option disabled selected>Elige tu tipo de solicitud</option>
                                            <option value="consulta">Consulta</option>
                                            <option value="reclamo">Reclamo</option>
                                            <option value="masinfo">Más información comercial</option>
                                        </select>
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" maxlength="5000" name="mensaje" rows="5" placeholder="Comentarios"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 msjval" style="text-align: justify;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: justify;">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="btn btn-primary btn-block btnRojo" id="botonformulariocontacto" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal de respuesta -->
<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-info-mensaje.png">
                    <h2 class="mensajeExito">Hemos recibido tu solicitud.<br> Nos contactaremos contigo en menos de 48 horas hábiles.
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal de respuesta error -->
<div id="ModalRespuestaSolicituderror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    Error
                </div>
            </div>
        </div>
    </div>
</div>