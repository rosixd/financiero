
<!-- modal ver ganadores -->
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body contenidoModal">
            <div class="infomodalserviCliente mb-3 moduloRequisitosTarjeta boxSeguros">
                <h2 class="mensajeExito">Ganadores por Campaña</h2>
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-descarga-certificado.png">
                            <a href="http://abcdin.desarrollo.netred.cl/assets/media/ganadores-concurso-gitfcard-facenote.pdf" download="Ganadores concurso giftcard.pdf" >
                                Ganadores concurso giftcard
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal">CERRAR</button>
        </div>
    </div>
</div>
