<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slider-pagatucuenta-100_3.jpg" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/assets/media/slider-pagatucuenta-100_3.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlideSola hidden-sm"></section>
<!-- fin barra superior -->
<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoPagarCuenta">
        <div class="container py-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="/assets/media/icono-pago-cuenta.png" class="float-left mr-3">
                    <h1>Paga tu cuenta</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <form>
                <div class="row">
                    <div class="col-md-12 contedorpagatc">
                        <div class="row formulario">
                            <div class="col-md-12">
                                <iframe src="https://soluciones.devetel.cl/pagosabcdinpreresponsivo/iframe_publico" style="width: 100%; height: 570px; border: none;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="Banner-pago">
        <img src="/assets/media/bg-banner-pagos.jpg" width="100%">
    </div>
</section>
<!-- fin contenidos -->
<!-- Modal pago no realizado -->
<div id="modalFracaso" class="modal fade pagoNoExitoso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body contenidoModal">
                <img src='/assets/media/icono-alerta.png' class="mb-4">

                <h3 class="mb-3">Pago no realizado</h3>
                <p>Lo sentimos, el pago no fue realizado con éxito.<br> Por favor intente nuevamente.</p>

                <div class="clearfix"></div>
                <button type="button" class="btn btn-primary btnRojo mt-2" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal pago exitoso -->
<div id="modalExito" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal pagoExitoso">
                <img src='/assets/media/icono-pago-ok.png' class="mb-4">

                <h3 class="mb-3">Pago realizado con éxito</h3>
                <p id="fecha_exito"></p>
                <p id="cuenta_exito"></p>
                <p id="monto_exito"></p>
                <p id="n_trx_exito"></p>
                <p>Hemos enviado el comprobante<br>a tu correo electrónico <span class="rojo"></span>
                </p>
                <form class="contenidoFormulario1">
                    <div class="form-group">
                        <button class="btn btn-primary btn-block btnRojo mt-2" id="ver_comprobante" type="submit">VER COMPROBANTE</button>
                    </div>
                </form>
                <img class="img-fluid" src='/assets/media/banner-nuevaapp.jpg' class="mt-4">

            </div>
        </div>
    </div>
</div>

<!-- Modal comprobante de pago -->
<div id="modalComprobante" class="modal fade modalComprobantePago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a class="btnAccion" href="ruta/al/archivo.pdf" download="nombre-comprobante">
                        <img src='/assets/media/icono-guardar-doc.jpg'>

                    </a>
                    <a class="btnAccion" href="javascript:window.print();">
                        <img src='/assets/media/icono-imprimir-doc.jpg'>

                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <img src='/assets/media/logo-abc-comprobante.jpg'>

                <h3>COMPROBANTE DE PAGO DE TARJETA</h3>
                <p class="text-center">Este es el comprobante de pago correspondiente a los datos que se presentan a continuación:</p>

                <table class="table">
                    <tbody>
                        <tr>
                            <td>RUT Titular:</td>
                            <td id="rut_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Nombre:</td>
                            <td id="nombre_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Número de Tarjeta:</td>
                            <td id="n_tarjeta_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Fecha de Transferencia:</td>
                            <td id="fecha_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Monto:</td>
                            <td id="monto_comprobante"></td>
                        </tr>
                        <tr>
                            <td>Número de Transacción:</td>
                            <td id="n_trx_comprobante"></td>
                        </tr>
                    </tbody>
                </table>
                <img src='/assets/media/sello-comprobante.jpg'>


            </div>

            <p class="mensajeFooter text-center">GUARDA O IMPRIME EL COMPROBANTE</p>
        </div>
    </div>
</div>