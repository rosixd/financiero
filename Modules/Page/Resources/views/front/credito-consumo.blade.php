<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="img/slider/slide-credito-consumo.jpg"  alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="img/slider/slide-credito-consumo.jpg" alt="Second slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- fin slider central -->


<!-- barra superior -->
<section class="barraSlide">
    <div class="container">
        <div class="row flex-parent justify-content-around">
            <div class="col-lg-3">
                <div class="box">
                    <img src="img/icono-caracteristicas.png">
                </div>  
                
                <div class="box2">
                    <h1><a href="#">Caracteristicas</a></h1>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box">
                    <img src="img/icono-requisitos.png">
                </div>
                
                <div class="box2">
                    <h1><a href="#">Requisitos</a></h1>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box">
                    <img src="img/icono-consultas.png">
                </div>
                <div class="box2">
                    <h1><a href="#">Consultas</a></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->


<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container contenidoFormulario1 my-5 formularioSeguros">
        <div class="row">
            <div class="col-md-12">
                <h2>Evaluate aquí y te contactaremos</h2>

                <div class="steps">
                    <div class="row bs-wizard" style="border-bottom:0;">
                        <div class="col-xs-3 bs-wizard-step complete">
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a class="bs-wizard-dot"><i class="far fa-user"></i></a>
                            <div class="bs-wizard-info text-center">Datos personales</div>
                        </div>
                        
                        <div class="col-xs-3 bs-wizard-step disabled">
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a class="bs-wizard-dot"><i class="fas fa-at"></i></a>
                            <div class="bs-wizard-info text-center">Datos de contacto</div>
                        </div>
                        
                        <div class="col-xs-3 bs-wizard-step disabled">
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a class="bs-wizard-dot"><i class="far fa-credit-card"></i></a>
                            <div class="bs-wizard-info text-center">Datos del crédito</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="FormControlInput1">Nombre Completo</label>
                        <input type="text" class="form-control" id="FormControlInput1" placeholder="JOSE LUIS">
                        <span class="obligatorio">*</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect2">Nacionalidad</label>
                                <select class="form-control custom-select" id="FormControlSelect2">
                                    <option>ELIGE</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <span class="obligatorio">*</span>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect3">Sexo</label>
                                <select class="form-control custom-select" id="FormControlSelect3">
                                    <option>ELIGE</option>
                                    <option>MASCULINO</option>
                                    <option>FEMENINO</option>
                                </select>
                                <span class="obligatorio">*</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect4">Apellido Paterno</label>
                                <input type="text" class="form-control" id="FormControlInput4" placeholder="FERNANDEZ">
                                <span class="obligatorio">*</span>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect5">Apellido Materno</label>
                                <input type="text" class="form-control" id="FormControlInput5" placeholder="VALDÉZ">
                                <span class="obligatorio">*</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="FormControlInput6">Nivel Educacional</label>
                        <select class="form-control custom-select" id="FormControlSelect6">
                            <option>ELIGE</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        <span class="obligatorio">*</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect7">Rut</label>
                                <input type="text" class="form-control" id="FormControlInput7" placeholder="12.345.678-9">
                                <span class="obligatorio">*</span>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect8">Fecha de nacimiento</label>
                                <div class="form-group"> <!-- Date input -->
                                    <input class="form-control" id="date" name="date" placeholder="DD/MM/AAAA" type="text"/>
                                </div>
                                <span class="obligatorio">*</span>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect9">Estado civil</label>
                                <select class="form-control custom-select" id="FormControlSelect9">
                                    <option>ELIGE</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <span class="obligatorio">*</span>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="FormControlSelect10">Regimen matrimonial</label>
                                <select class="form-control custom-select" id="FormControlSelect10">
                                    <option>ELIGE</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <span class="obligatorio">*</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <p><span class="obligatorio ml-4">*</span> CAMPOS OBLIGATORIOS</p>
                    </div>
                </div>

                <div class="col-md-3 offset-md-9">
                    <div class="row">
                        <button class="btn btn-primary btn-block btnRojo" type="submit">SIGUIENTE</button>
                    </div>
                </div>
            </div>
        </form> 
    </div>

    <div class="container-fluid moduloFaqs pt-5 pb-5"> 
        <div class="row">
            <div class="col">
                <div class="container pt-5 pb-5"> 
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary btn-lg btn-block btnGris"><i class="fas fa-plus"></i> Preguntas Frecuentes</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary btn-lg btn-block btnGris"><i class="fas fa-plus"></i> Glosario</button>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container moduloLegales pt-5 pb-5"> 
        <div class="row">
            <div class="col-12">
                <h3>Textos Legales</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint 
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </p>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->