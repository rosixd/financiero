<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                <img src="img/icono-info-mensaje.png">
                <h2 class="mensajeExito">Hemos recibido tu solicitud.<br>
                    Nos contactaremos contigo en menos de 48 horas hábiles.
                </h2>
                </div> 
            </div>
        
        </div>
    </div>
</div>