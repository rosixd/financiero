<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid">
        <div class="row flex-parent justify-content-around" style="padding-top: 15px; padding-bottom: 15px;">
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box"><img src="/assets/media/icono-vida_1.png" style="width: 55px; height: 57px;" />
                            <h1><a href="/seguros-vida">Vida</a></h1>
                        </div>
                    </div>
    
                    <div class="d-block d-sm-none text-center"><img src="/assets/media/icono-vida_1.png" style="width: 55px; height: 57px;" />
                        <h1><a href="/seguros-vida">Vida</a></h1>
                    </div>
                </div>
    
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box"><img src="/assets/media/icono-salud.png" style="width: 55px; height: 57px;" />
                            <h1><a href="/seguros-salud">Salud</a></h1>
                        </div>
                    </div>
    
                    <div class="d-block d-sm-none text-center"><img src="/assets/media/icono-salud.png" style="width: 55px; height: 57px;" />
                        <h1><a href="/seguros-salud">Salud</a></h1>
                    </div>
                </div>
    
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box"><img src="/assets/media/icono-financiera_1.png" style="width: 55px; height: 57px;" />
                            <h1><a href="/seguros-proteccion-financiera">Protecci&oacute;n<br />
                            Financiera</a></h1>
                        </div>
                    </div>
    
                    <div class="d-block d-sm-none text-center"><img src="/assets/media/icono-financiera_1.png" style="width: 55px; height: 57px;" />
                        <h1><a href="/seguros-proteccion-financiera">Protecci&oacute;n<br />
                        Financiera</a></h1>
                    </div>
                </div>
    
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box"><img src="/assets/media/icono-hogar_1.png" style="width: 55px; height: 57px;" />
                            <h1><a href="/seguros-hogar">Hogar</a></h1>
                        </div>
                    </div>
    
                    <div class="d-block d-sm-none text-center"><img src="/assets/media/icono-hogar_1.png" style="width: 55px; height: 57px;" />
                    <h1><a href="/seguros-hogar">Hogar</a></h1>
                </div>
            </div>
            <!-- <div class="col-sm col-lg-2">
                <div class="box">
                    <img src="/assets/media/icono-automotriz.png">
                    <h1><a href="/seguros-automotriz">Automotriz</a></h1>
                </div>          
            </div>-->
        </div>
    </div>
</section>
<!-- fin barra superior -->
<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoSeguros">
        <div class="row">
            <div class="col-lg-4 p-0 Gris1 infoSolicitaSeguro align-content-center">
                <div class="infoBox mx-auto mt-5">
                    <h1>¿Necesitas más información? ¿Tienes dudas?</h1>
                    <a href="#ModalFormularioContactoSeguro" role="button" data-toggle="modal" class="btn_solicitalo">Contáctanos</a>
                </div>
            </div>
            <div class="col-lg-8 p-0">
                <div class=videoBox>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/6n4yNlamhqc" allowfullscreen class="embed-responsive-item seguro"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row Crema">
            <div class="col-md-4 offset-md-2 cajaServiciosAdicionales">
                <h1 class="rojo mt-4" style="text-align: center;">
                    <strong>Servicios<br>
                        <span class="Gris2">Adicionales</span>
                    </strong>
                </h1>
            </div>
            <div class="col-md-3 cajaServiciosAdicionales">
                <div style="margin-top: 35px; text-align: center;" class="productosasistencia">
                    <h2 class="rojo">PRODUCTOS DE <strong>ASISTENCIA</strong></h2>
                    <a class="btn btn-primary btn-sm shadow-sm btn-info" href="/asistencia" style="text-decoration: none; box-shadow: none;">MÁS INFORMACIÓN</a>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-4 cajaServiciosAdicionales">
                <div class="d-flex">
                    <div class="mr-3 mt-3">
                        <img src="/assets/media/icono-envio-dinero.png">
                    </div>
                    <div>
                        <h2 class="rojo">ENVÍOS DE <strong>DINERO</strong></h2>
                        <a class="btn btn-primary btn-sm shadow-sm btn-info" href="#">MÁS INFORMACIÓN</a>
                    </div>
                </div>
            </div> -->
    </div>
    <div class="container mt-5 mb-5">
        <div class="row justify-content-around">
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="#Modalinfodenunciasiniestro" data-toggle="modal">
                        <img src="/assets/media/icono-denuncia-siniestros.png">
                        <p>Denuncia de Siniestros</p>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="https://5.dec.cl/" target="_blank">
                        <img src="/assets/media/icono-seguros-contratados.png">
                        <p>Mis seguros contratados</p>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="#Modalinfodiversificacioncompania" data-toggle="modal">
                        <img src="/assets/media/icono-diversificacion.png">
                        <p>Diversificación por compañias</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="row justify-content-around">
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="/assets/media/autorregulacion-abcvisa.png" download="acuerdo-autorregulacion.png" id="acuerdo-autorregulacion_pdf">
                        <img src="/assets/media/icono-acuerdos.png">
                        <p>Acuerdos de autoregulación</p>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="/assets/media/circular-2131-svs.pdf" download="CIRCULARES CMF">
                        <img src="/assets/media/icono-circulares-svs.png">
                        <p>Circulares CMF</p>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxInfoSeguro">
                    <a href="#">
                        <img src="/assets/media/icono-info-adicional.png">
                        <p>Información adicional</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm py-3">
                <a href="#">
                    <img src="/assets/media/bannericonseguros.png" style="width: 100%;">
                </a>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- fin contenidos -->
<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto" class="formulario formValida">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control px-3 pb-1 soloLetras requerido" maxlength="100" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS">
                                    </div>
                                    <input type="hidden" value="LandingContato" name="formulario">
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="rut" name="rut" placeholder="12.345.678-9">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" id="telefono" maxlength="20" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control  px-3 pb-1 requerido" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar  <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposeguro" name="tiposeguro">
                                            <option disabled selected>Elige tu seguro</option>
                                            <option value="01">Seguro Vida Avance</option>
                                            <option value="02">Seguro Catastrófico familiar</option>
                                            <option value="03">Seguro Salud Dental</option>
                                            <option value="04">Seguro Escolaridad</option>
                                            <option value="05">Seguro Vida</option>
                                            <option value="06">Seguro Cesantía Cuentas Básicas</option>
                                            <option value="07">Seguro Contra robo con bonificación</option>
                                            <option value="08">Seguro Hogar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposolicitud">Tipo de solicitud <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposolicitud" name="tiposolicitud">
                                            <option disabled selected>Elige tu tipo de solicitud</option>
                                            <option value="consulta">Consulta</option>
                                            <option value="reclamo">Reclamo</option>
                                            <option value="masinfo">Más información comercial</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" maxlength="5000" name="mensaje" rows="5" placeholder="Comentarios"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 msjval" style="text-align: justify;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: justify;">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="btn btn-primary btn-block btnRojo" id="botonformulariocontacto" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal de respuesta -->
<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-info-mensaje.png">
                    <h2 class="mensajeExito">Hemos recibido tu solicitud.<br> Nos contactaremos contigo en menos de 48 horas hábiles.
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal de respuesta error -->
<div id="ModalRespuestaSolicituderror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    Error
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal denuncia de Siniestros -->
<div id="Modalinfodenunciasiniestro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                    <h2>
                        <span class="zb-requisitos-modal" style="font-size: 0.8em; color: #5d5d61;">PLAZOS DE</span>
                        <br>
                        <span class="zb-requisitos-modal" style="font-size: 0.8em; color: #B40D15;">DENUNCIAS</span>
                    </h2>
                    <div class="pasos">
                        <img style="float: right; width: 190px; padding-top: 14px;" src="/assets/media/logo-seguros.png" alt="Corredora de Seguros" />
                    </div>
                    <div class="clearfix my-4 "></div>
                    <div class="container ContenidoSeguro">
                        <h5 style="text-align: center;">(Para clientes con seguro vigente)</h5>
                        <div class="row datosTabla">
                            <table class="table table-responsive mt-3">
                                <thead>
                                    <tr>
                                        <th style="padding: 0.75rem;">Tipo de seguro</th>
                                        <th style="padding: 0.75rem;">Compañía de seguros</th>
                                        <th style="padding: 0.75rem;">Plazo máximo desde ocurrido el siniestro</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Desgravamen</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">365 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Cesantía</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">120 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro hogar (cobertura incendio)</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">90 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro Antifraude / Mochila / Cartera</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">90 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">seguro de vida</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF / BCI SEGUROS</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">Hasta 4 Años</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro de salud</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">OHIO NATIONAL</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">Hasta 4 Años</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro oncológico</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">OHIO NATIONAL</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">Hasta 4 Años</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro de accidentes personales</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">OHIO NATIONAL</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">Hasta 4 Años</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Robo con violencia</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CARDIF</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">15 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Pérdida total por agua</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">CONSORCIO</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">15 días corridos</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px;">Seguro Antifraude / Anti-robo</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">BCI</td>
                                        <td style="padding: 0.75rem; font-size: 14px;">90 Días</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col_full center" style="display: inline-block; background-color: #d5012c;">
                <div class="col_one_third center" style="display: inline-block; margin-bottom: 6px;">
                    <div style="display: inline-block; margin-top: 8px;">
                        <img style="display: inline-block; margin-left: 30px; float: left; margin-top: 10px; margin-right: 10px;" src="/assets/media/mini-telefono.png" alt="Corredora de Seguros" />
                        <h4 style="display: inline-block; margin: 0 auto; color: #fff; letter-spacing: 0; font-size: 1.02em;">DENUNCIA<br>TU SINIESTRO</h4>
                    </div>
                </div>
                <div class="col_two_third col_last center" style="display: inline-block; float: right;">
                    <div style="margin-top: 15px;">
                        <div style="display: inline-block; padding-right: 12px;">
                            <h5 style="display: inline-block; margin: 0 auto;">
                                <a href="tel:+6008302222" style="text-decoration: none; color: #fff;">600 830 2222</a>
                            </h5>
                        </div>
                        <div style="display: inline-block; padding-left: 12px; padding-right: 30px;">
                            <h5 style="display: inline-block;  margin: 0 auto;">
                                <a href="tel:+56228982701" style="text-decoration: none; color: #fff;">2 28982701</a>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal DIVERSIFICACION_POR_COMPANIA -->
<div id="Modalinfodiversificacioncompania" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                    <h2>
                        <span class="zb-requisitos-modal" style="font-size: 0.8em; color: #5d5d61;">INFORMACIÓN SOBRE</span>
                        <br>
                        <span class="zb-requisitos-modal" style="font-size: 0.8em; color: #B40D15;">DIVERSIFICACIÓN DE LA PRODUCCIÓN</span>
                    </h2>
                    <div class="pasos">
                        <img style="float: right; width: 190px; padding-top: 14px;" src="/assets/media/logo-seguros.png" alt="Corredora de Seguros" />
                    </div>
                    <div class="clearfix my-4 "></div>
                    <div class="container ContenidoSeguro">
                        <div class="row datosTabla">
                            <table class="table table-responsive mt-3">
                                <thead>
                                    <tr>
                                        <th style="padding: 0.75rem; text-align: center;">Nombre o razón social<br>del corredor de seguros</th>
                                        <th style="padding: 0.75rem; text-align: center;">Rut</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; text-align: center;">ABCDIN Corredores de<br>Seguros Limitada</td>
                                        <td style="padding: 0.75rem; font-size: 14px; text-align: center;">77.561.270-3</td>
                                    </tr>
                                </tbody>
                            </table>
                            <h6 style="text-align: center; font-size: 10px;">
                                <strong> En mi calidad de Corredor de Seguros y en cumplimiento a la ley y a las instrucciones impartidas por la Superintendencia de Valores Y seguros, informo que durante el año 2017 intermedié contratos de seguros con las compañías
                                que se indican a continuación (Art.57).
                                </strong>
                            </h6>
                        </div>
                        <h4 style="margin-top: 30px; margin-bottom: 30px; text-align: center; color: #B40D15;">PRIMA INTERMEDIADA</h4>
                        <div class="row datosTabla">
                            <table class="table table-responsive mt-3">
                                <thead>
                                    <tr>
                                        <th style="padding: 0.75rem;">Compañía de seguros</th>
                                        <th style="padding: 0.75rem;">Seguros de vida %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">BCI SEGUROS VISA S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;"> 61,50% </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">OHIO NATIONAL SEGUROS DE VIDA S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;"> 21,85% </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">BICE PARIBAS CARDIF SEGUROS VIDA S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;"> 15,70% </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">BICE VIDA COMPAÑÍA DE SEGUROS S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;"> 0,68% </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">ITAU CHILE COMPAÑÍA DE SEGUROS DE VIDA S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;"> 0,28% </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row datosTabla">
                            <table class="table table-responsive mt-3">
                                <thead>
                                    <tr>
                                        <th style="padding: 0.75rem;">Compañía de seguros</th>
                                        <th style="padding: 0.75rem;">Seguros general %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">BCI SEGUROS GENERALES S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">69,89%</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">CONSORCIO NACIONAL DE SEGUROS S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">26,96%</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">BNP PARIBAS CARDIF SEGUROS GENERALES S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">3,07%</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">HDI SEGUROS S.A.</td>
                                        <td style="padding: 0.75rem; font-size: 14px; width:50%;">0,08%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <h6 style="margin-top: 30px; margin-bottom: 30px; text-align: center; margin-top: 30px; margin-bottom: 30px;">
                INFÓRMESE EN LA SUPERINTENDENCIA SOBRE LA GARANTÍA MEDIANTE BOLETA BANCARIA O PÓLIZAS QUE, CONFORME A LA LEY, DEBEN CONSTITUIR LOS CORREDORES DE SEGUROS.
            </h6>
        </div>
    </div>
</div>