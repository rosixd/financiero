<!-- modal bases legales -->
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-hidden="true" class="close" data-dismiss="modal" type="button">x</button>
        </div>
    
        <div class="modal-body contenidoModal">
            <div class="infomodalserviCliente mb-3 moduloRequisitosTarjeta boxSeguros">
                <h2 class="mensajeExito">REVISA AQU&Iacute; LAS BASES DE PROMOCIONES</h2>
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="bata-ene2019-ok.pdf" href="/assets/media/bata-ene2019-ok.pdf"> Bases Legales Bata </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="bencinas-enero2019-ok.pdf" href="/assets/media/bencinas-enero2019-ok.pdf"> Bases legales Bencineras </a> 
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" /> 
                            <a download="cine-hoyts-enero-ok.pdf" href="/assets/media/cine-hoyts-enero-ok.pdf"> Bases legales Cine Hoyts </a> 
                        </span>
                    </div>
                </div>
        
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="framacias-ene2019-ok.pdf" href="/assets/media/framacias-ene2019-ok.pdf"> Bases legales Framacias </a>
                        </span>
                    </div>
                    
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="gimnasios-pacific.pdf" href="/assets/media/gimnasios-pacific.pdf"> Bases legales Gimnasio Pacific </a> 
                        </span>
                    </div>
                    
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" /> 
                            <a download="groupon-en2019-ok.pdf" href="/assets/media/groupon-en2019-ok.pdf"> Bases legales Groupon.cl </a> 
                        </span>
                    </div>
                </div>
        
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="jetsmart-en2019-ok.pdf" href="/assets/media/jetsmart-en2019-ok.pdf"> Bases legales Jetsmart.com </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="kfcchwkwendys-en2019-ok.pdf" href="/assets/media/kfcchwkwendys-en2019-ok.pdf"> Bases leales Restaurantes (KFC,CHWK, Wendys) </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="mercadolibrecl-en2018-ok.pdf" href="/assets/media/mercadolibrecl-en2018-ok.pdf"> Bases legales Mercadolibre.cl </a>
                        </span>
                    </div>
                </div>
                <div class="row baselegal">
                    <div class="col-lg-4"><span class="descargaCertificado" style="margin-bottom: 15px;">
                        <img src="/assets/media/icono-descarga-certificado.png" />
                        <a download="preunic-en2019-ok.pdf" href="/assets/media/preunic-en2019-ok.pdf"> Bases legales Preunic </a>
                    </span>
                </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="recargaenlineacl-ene2019-ok.pdf" href="/assets/media/recargaenlineacl-ene2019-ok.pdf"> Bases legales Recargaenlinea.cl </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="recorridocl-ene2019-ok.pdf" href="/assets/media/recorridocl-ene2019-ok.pdf"> Bases legales Recorrido.cl </a>
                        </span>
                    </div>
                </div>
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="ticketprocl-enero2019-ok.pdf" href="/assets/media/ticketprocl-enero2019-ok.pdf"> Bases legales Ticketpro.cl </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="unimarc-en2019-ok.pdf" href="/assets/media/unimarc-en2019-ok.pdf"> Bases legales Unimarc </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="unosaluddental-en2019-ok.pdf" href="/assets/media/unosaluddental-en2019-ok.pdf"> Bases legales Uno Salud Dental </a>
                        </span>
                    </div>
                </div>
                <div class="row baselegal">
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="bases-de-descuento-proxima-compra-cupon_1.pdf" href="/assets/media/bases-de-descuento-proxima-compra-cupon_1.pdf"> Bases legales Cupón de descuento Pr&oacute;xima Compra </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="bases-de-concurso-cofisa-facenote-dic-ene_1.pdf" href="/assets/media/bases-de-concurso-cofisa-facenote-dic-ene_1.pdf"> Bases leales Concurso Facenote </a>
                        </span>
                    </div>
                    <div class="col-lg-4">
                        <span class="descargaCertificado" style="margin-bottom: 15px;">
                            <img src="/assets/media/icono-descarga-certificado.png" />
                            <a download="ganadores-concurso-gitfcard-facenote_2.pdf" href="/assets/media/ganadores-concurso-gitfcard-facenote_2.pdf"> Bases legales Ganadores Gift Card Concurso Facenote </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-center">
            <button class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal" type="button">CERRAR</button>
        </div>
    </div>
</div>