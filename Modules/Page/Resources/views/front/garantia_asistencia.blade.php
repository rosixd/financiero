<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/slider-productosdeasistencia.png" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
            <img class="d-block w-100" src="/assets/media/slide-garantia-extendida.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular Asistencias">
        <div class="row justify-content-around align-content-center">
            <ul class="nav nav-tabs justify-content-around menuasistencias">
                <li class="nav-item">
                    <a href="/" class="active" data-target="#infoAsistenciaHogar" data-toggle="tab" class="hogara">
                        <img src="/assets/media/ico-asistencia-hogar.png" class="hogar"> 
                        Asistencia <br>Hogar
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" data-target="#infoAsistenciaMujer" data-toggle="tab" class="mujera">
                        <img src="/assets/media/ico-asistencia-mujer.png" class="mujer"> Asistencia <br>Mujer
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" data-target="#infoAsistenciaSalud" data-toggle="tab" class="vidaa">
                        <img src="/assets/media/ico-asistencia-vida.png" class="vida"> Asistencia <br>Salud Integral Familiar
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" data-target="#infoAsistenciaDental" data-toggle="tab" class="dentala">
                        <img src="/assets/media/ico-asistencia-dental.png" class="dental"> Asistencia <br>Dental
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" data-target="#infoAsistenciaUrgencia" data-toggle="tab" class="urgenciaa">
                        <img src="/assets/media/ico-asistencia-urgencia.png" class="urgencia"> Asistencia <br>Sala de Urgencia
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" data-target="#infoAsistenciaClinica" data-toggle="tab" class="clinicaa">
                        <img src="/assets/media/ico-asistencia-clinica.png" class="clinica"> 
                        Asistencia <br>Clínica Domiciliaria
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- fin barra superior -->
<div id="tabsJustifiedContent" class="tab-content">
    <!-- Contenidos asistencia hogar -->
    <div id="infoAsistenciaHogar" class="tab-pane p0 fade active show">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Que es la Asistencia Hogar?</h1>
                    <p class="textjustify">
                        Es un servicio que está orientado a entregarle mayor apoyo y asesoría en caso que usted tenga alguna necesidad en su vida diaria. Ha sido diseñada para acompañar a los clientes en momentos en los cuales una orientación profesional oportuna y accesible
                        es fundamental para poder tomar una buena decisión o enfrentar de manera adecuada un problema.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+562222002952"><img src="/assets/media/icono-cel.gif">+562 22 200 29 52</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">
                        <thead>
                            <tr>
                                <th>Tipo de Servicio</th>
                                <th>Servicios</th>
                                <th>Tope por evento</th>
                                <th>Eventos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="4">
                                    <img src="/assets/media/icono-serv-hogar.png">
                                    <strong>Servicios Hogar</strong>
                                </td>
                                <td>
                                    <a class="enlace">Envío y pago de plomero</a>
                                    <a class="infoModal" href="#infoModalServicioPlomeria" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td><a class="enlace">Envío y pago de electricista.</a>
                                    <a class="infoModal" href="#infoModalServicioElectricidad" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td><a class="enlace">Envío y pago de cerrajero.</a>
                                    <a class="infoModal" href="#infoModalServicioCerrajeria" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Envío y pago de vidriero.</a>
                                    <a class="infoModal" href="#infoModalServicioVidrieria" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>

                            <tr>
                                <td rowspan="2">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>Servicios en Ruta</strong>
                                </td>
                                <td>
                                    <a class="enlace">Remolque para moto o vehículo liviano.</a>
                                    <a class="infoModal" href="#infoModalRemolque" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>5 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Cambio de rueda de repuesto a causa de pinchazo o accidente.</a>
                                    <a class="infoModal" href="#infoModalRueda" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>5 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    <img src="/assets/media/icono-serv-salud.png">
                                    <strong>Servicios en Salud</strong></td>
                                <td>
                                    <a class="enlace">Examen oftalmológico.</a>
                                    <a class="infoModal" href="#ExamenOftalmologico" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>2 consultas</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Traslado en caso de accidente y/o enfermedad.</a>
                                    <a class="infoModal" href="#traslado" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Orientación médica telefónica.</a>
                                    <a class="infoModal" href="#Orientacionmedicatelefonica" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>Sin límite</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#uno" data-toggle="tab" class="active show">
                                                    <span class="round-tabs one">¿Cuál es la Vigencia?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#dos" data-toggle="tab">
                                                    <span class="round-tabs two">¿Cómo se usa?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="pdf" download target="_blank" href="/assets/media/certificado-hogar.pdf" download="certificado-hogar.pdf">
                                                    <span class="round-tabs four">Certificado PDF</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="uno">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">La vigencia de la Asistencia Hogar es por un año y el servicio comienza a contar de la fecha de compra de la Asistencia, que corresponde a la fecha de la emisión boleta de compra.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p0 fade in" id="dos">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">Sólo debes llamar al 800 200 347, o desde celulares al fono (+562) 22 200 29 52 para solicitar cualquiera de estos servicios.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="cuatro">
                                        <div class="container mt-5 mb-5">
                                            <p>&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->
    <!-- Contenidos asistencia mujer-->
    <div id="infoAsistenciaMujer" class="tab-pane p0 fade">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Que es la Asistencia Mujer?</h1>
                    <p class="textjustify">
                        La Asistencia Mujer entrega servicios especializados y orientados a la prevención y ayuda para la mujer.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+562222002952">
                            <img src="/assets/media/icono-cel.gif">+562 22 200 29 52
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">

                        <thead>
                            <tr>
                                <th>Tipo de Servicio</th>
                                <th>Servicios</th>
                                <th>Tope por evento</th>
                                <th>Eventos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="4">
                                    <img src="/assets/media/icono-serv-hogar.png">
                                    <strong>Servicios Hogar</strong>
                                </td>
                                <td>
                                    <a class="enlace">Envío y pago de plomero</a>
                                    <a class="infoModal" href="#infoModalServicioPlomeriaMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td><a class="enlace">Envío y pago de electricista.</a>
                                    <a class="infoModal" href="#infoModalServicioElectricidadMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td><a class="enlace">Envío y pago de cerrajero.</a>
                                    <a class="infoModal" href="#infoModalServicioCerrajeriaMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Envío y pago de vidriero.</a>
                                    <a class="infoModal" href="#infoModalServicioVidrieriaMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>3 UF</td>
                                <td>3 Eventos</td>
                            </tr>

                            <tr>
                                <td rowspan="4">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>Servicios de Salud</strong>
                                </td>
                                <td>
                                    <a class="enlace">Examen de Papanicolaou o examen de mamografía o presupuesto y limpieza dental.</a>
                                    <a class="infoModal" href="#ExamendePapanicolaouMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>1 evento a elección</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Traslado en caso de enfermedad o accidente.</a>
                                    <a class="infoModal" href="#trasladoMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>3 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Orientación médica telefónica general, nutricional y ginecológica.</a>
                                    <a class="infoModal" href="#OrientacionmedicatelefonicaMujer" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>Sin límite</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta psicológica por robo o asalto.</a>
                                    <a class="infoModal" href="#infoModalServicio" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>1 hora, 4 eventos.</td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    <img src="/assets/media/icono-serv-salud.png">
                                    <strong>Servicio Legal</strong>
                                </td>
                                <td>
                                    <a class="enlace">Orientación y asesoría legal telefónica en temas de familia, sociales y emprendimiento.</a>
                                    <a class="infoModal" href="#infoModalServicio" role="button" data-toggle="modal">
                                        <i class="fas fa-info-circle"></i>
                                    </a>
                                </td>
                                <td>Sin límite</td>
                                <td>Sin límite</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Bloqueo de documentos por robo, asalto o extravío.</a>
                                </td>
                                <td>Sin límite</td>
                                <td>Sin límite</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Representación judicial ante los Tribunales de Justicia.</a>
                                </td>
                                <td>Sin límite</td>
                                <td>3 eventos</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#uno" data-toggle="tab" class="active show">
                                                    <span class="round-tabs one">¿Cuál es la Vigencia?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#dos" data-toggle="tab">
                                                    <span class="round-tabs two">¿Cómo se usa?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="pdf" download target="_blank" href="/assets/media/certificado-mujer.pdf" download="certificado-mujer.pdf">
                                                    <span class="round-tabs four">Certificado PDF</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="uno">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">La vigencia de la Asistencia Mujer es por un año y el servicio comienza a contar de la fecha de compra de la Asistencia, que corresponde a la fecha de la emisión boleta de compra.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p0 fade in" id="dos">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">Sólo debes llamar al 800 200 347, o desde celulares al fono (+562) 22 200 2952 para solicitar cualquiera de estos servicios.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="cuatro">
                                        <div class="container mt-5 mb-5">
                                            <p>&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->
    <!-- Contenidos asistencia salud integral familiar-->
    <div id="infoAsistenciaSalud" class="tab-pane p0 fade">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Que es la Asistencia Salud Integral familiar?</h1>
                    <p class="textjustify">
                        Es un plan de servicio que coordina y facilita la atención con médicos especialistas, y diversos exámenes de imagenología, para el titular de tarjeta Abcdin, más tres beneficiarios.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+56223512640">
                            <img src="/assets/media/icono-cel.gif">+562 22 351 26 40
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">

                        <thead>
                            <tr>
                                <th>Prestación</th>
                                <th>Monto por evento</th>
                                <th>Eventos Anual</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Médica General</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Médica Traumatología y Ortopedia</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Médica Pediátrica o Medicina General Infantil</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Ginecología y Obtstetricia</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Médica Urología</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Consulta Médica Cardiología o Medicina Interna</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>2 Eventos</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Exámenes de Imagenología: Radiografías</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>3 Eventos de la radiografía seleccionada</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Maternidad y Embarazo</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>1 Evento</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Ecotomografía Transvaginal o Transrectal</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>1 Evento</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Ecotomografía Pélvica (femenina y/o masculina)</a>
                                </td>
                                <td>ILIMITADO</td>
                                <td>1 Evento</td>
                            </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#abc" data-toggle="tab" class="active show">
                                                    <span class="round-tabs one">¿Cuál es la Vigencia?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#def" data-toggle="tab">
                                                    <span class="round-tabs two">¿Cómo se usa?</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="abc">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">La vigencia del Servicio Asistencia Salud Integral Familiar, es anual, y se inicia a contar de la fecha de contratación.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p0 fade in" id="def">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">Para solicitar el servicio de Asistencia Salud Integral Familiar, el cliente debe llamar al siguiente teléfono preferente 223512640. Se debe señalar rut, nombre y apellidos del titular, y/o número
                                                            de boleta, además del servicio que se requiere.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->
    <!-- Contenidos asistencia dental-->
    <div id="infoAsistenciaDental" class="tab-pane p0 fade">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Que es la Asistencia Dental?</h1>
                    <p class="textjustify">
                        Es una asistencia que otorga al beneficiario las posibilidades de recuperación y prevención de la salud bucal. Dependiendo de plan contratado incluye Titular, titular + 3, Familiar y Urgencia Plan Titular, 026 UF
                        <br>
                        <ul style="font-size: 16px !important; font-weight: 300;" >
                            <li>
                                <strong>Urgencias: </strong>Entrega atención prioritaria en caso de dolor intensivo, inflamación o sangrado.
                            </li>
                            <li>
                                <strong>Prevención: </strong>Medidas que permiten reducir la probabilidad de aparición de una afección o enfermedad, o bien interrumpir o aminorar su aparición.
                            </li>
                            <li>
                                <strong>Endodoncia: </strong>(Incluye tratamiento terminado de dientes vitales y no vitales, con o sin lesión periapical en los casos de lesión periapical no están incluidas las sesiones de medicación de conductos).
                            </li>
                            <li>
                                <strong>Operatoria: </strong>Se trata de las restauraciones en caso de caries o fractura del diente - tope de 4 obturaciones por año de vigencia.
                            </li>
                            <li>
                                <strong>Cirugía Oral: </strong>Es la extracción de dientes erupcionados.
                            </li>
                            <li>
                                <strong>Odontopediatría: </strong>Es la especialidad encargada de tratar a los niños.
                            </li>
                        </ul>
                        <br>
                        <strong>Descuento del 60% sobre arancel UCO en honorarios odontológicos en el resto de los tratamientos.</strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+562227501094">
                            <img src="/assets/media/icono-cel.gif">+562 22 750 10 94
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">

                        <thead>
                            <tr>
                                <th style="width: 30%;">Dental</th>
                                <th style="width: 30%;">Tipo de Servicio</th>
                                <th style="width: 15%;">Costo</th>
                                <th style="width: 15%;">Tope por evento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="13">
                                    <img src="/assets/media/icono-serv-hogar.png">
                                    <strong>URGENCIAS:
                                        <br> Entrega atención prioritaria en caso de dolor 
                                        <br>intenso, inflamación o sangrado.</strong>
                                </td>
                                <td>
                                    <a class="enlace">Diagnóstico de urgencia dental y derivación a especialista</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Radiografía pieza afectada (periapical)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Alivio de oclusión (diente sintomático)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Drenaje de absceso intraoral</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Trepanación de urgencia (pulpitis irreversible)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Exodoncia simple de urgencia (excluye terceros molares)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Exodoncia a colgajo de urgencia (excluye terceros molares)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Complicaciones post-exodoncia: hemorragia y alveolitis</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Ferulización en caso de trauma dientes anteriores</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Retiro de cuerpo extraño</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Tratamiento sintomático de aftas menores en cavidad oral</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Tratamiento de pericoronaritis aguda</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Tratamiento de gingivitis úlcero necrótica aguda</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>

                            <tr>
                                <td rowspan="4">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>PREVENCION:<br>
                                        Medidas que permiten reducir la probabilidad de aparición 
                                        <br> de una afección o enfermedad,
                                        o bien <br> interrumpir o aminorar su aparición.</strong>
                                </td>
                                <td>
                                    <a class="enlace">Rayos X (Periapicales para diagnóstico)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Examen clínico y diagnóstico</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Profilaxis</a>
                                </td>
                                <td>$0</td>
                                <td>1 al año</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Remoción de cálculos supragingivales (una sesión anual)</a>
                                </td>
                                <td>$9.000</td>
                                <td>1 al año</td>
                            </tr>
                            <tr>
                                <td rowspan="1">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>ENDODONCIA <br>
                                        (incluye tratamiento terminado de dientes vitales y <br>no vitales,
                                        con o sin lesión periapical en los casos de <br>lesión periapical no están 
                                        <br>incluidas las sesiones de medicación de conductos). 
                                    </strong>
                                </td>
                                <td>
                                    <a class="enlace">Endodoncia en dientes anteriores</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td rowspan="5">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>OPERATORIA: <br>
                                        Se trata de las restauraciones en caso de caries o <br> fractura 
                                        del diente - tope de 4 obturaciones por año de vigencia 20.
                                    </strong>
                                </td>
                                <td>
                                    <a class="enlace">Obturación resina simple, pieza anterior o posterior</a>
                                </td>
                                <td>$9.000</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Obturación resina compuesta, pieza anterior o posterior</a>
                                </td>
                                <td>$9.000</td>
                                <td>Sin tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Obturación resina compleja, pieza anterior o posterior</a>
                                </td>
                                <td>$9.000</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Tratamiento de sensibilidad cervical sin cavidad (con ionomeros solo en caso de sensibilidad)</a>
                                </td>
                                <td>$9.000</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Resina cervical, pieza anterior o posterior (lesiones por caries, erosiones o abrasiones)</a>
                                </td>
                                <td>$9.000</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td rowspan="2">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>CIRUGIA ORAL:<br>
                                            Es la extracción de dientes erupcionados.</strong>
                                </td>
                                <td>
                                    <a class="enlace">Exodoncia simple (excluye terceros molares)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Exodoncia a colgajo (excluye terceros molares)</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td rowspan="8">
                                    <img src="/assets/media/icono-serv-ruta.png">
                                    <strong>ODONTO PEDIATRIA<br>
                                            Es la especialidad en cargada de tratar a los niños.
                                    </strong>
                                </td>
                                <td>
                                    <a class="enlace">Aplicación de flúor gel en cubetas( < de 14 años - 1 al año)</a>
                                </td>
                                <td>$0</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Aplicación sellante en pieza definitiva, fotocurado (hasta 14 años-8 por año, 1por pieza dental)</a>
                                </td>
                                <td>$4</td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Exodoncia diente temporal </a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Resina simple en dientes temporales</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Resina compuesta en dientes temporales</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Pulpotomía</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Endodoncia en diente temporal anterior</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Endodoncia en diente temporal posterior</a>
                                </td>
                                <td>$0</td>
                                <td>Sin Tope</td>
                            </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#a" data-toggle="tab" class="active show">
                                                    <span class="round-tabs one">¿Cuál es la Vigencia?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#b" data-toggle="tab">
                                                    <span class="round-tabs two">¿Cómo se usa?</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#c" data-toggle="tab">
                                                    <span class="round-tabs three">Preguntas Frecuentes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="a">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">La vigencia es anual y con renovación automática, pero con pago mensual, por lo tanto, el cliente decide el tiempo de permanencia en la asistencia. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p0 fade in" id="b">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <p class="textjustify">Mediante el área de Servicio al cliente se realiza la asesoría sobre el plan contratado y los atributos del mismo, coordinación y agendamiento de la primera hora de atención en la que el cliente
                                                            se realiza la evaluación y diagnóstico y recepción del presupuesto. A continuación en la clínica coordina directamente la siguiente hora de atención para partir con su plan de tratamiento.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p0 fade in" id="c">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <ul>
                                                            <li><strong> ¿Cuándo Puedo usar la asistencia? </strong>
                                                                <ul>
                                                                    <li>Para la atención de urgencias 10 días desde la contratación</li>
                                                                    <li>Para la atención de todas las otras prestaciones, 45 días </li>
                                                                </ul>
                                                            </li>
                                                            <li><strong> ¿Qué necesito para hacer uso de la asistencia dental?</strong> Tener al día el pago de las cuotas y llamar al fono 227501094</li>
                                                            <li><strong> ¿Puedo incorporar a más beneficiarios?</strong> Puedes hacerlo llamando al mismo número 227501094 y te orientarán respecto de los planes y la mejor alternativa</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->

    <!-- Contenidos asistencia urgencia-->
    <div id="infoAsistenciaUrgencia" class="tab-pane p0 fade">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Que es la Asistencia Sala de Urgencia?</h1>
                    <p class="textjustify">
                        Es la asistencia que contempla en caso de urgencia, ya sea por accidente o enfermedad para el Afiliado y beneficiarios, el traslado, el derecho de urgencia, la atención de urgencia, la atención médico de urgencia y valoración clínica, en centros médicos,
                        hospitales o clínicas en convenio.
                        <br> Adicionalmente contempla un programa de beneficios en Farmacias Ahumada.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+562222002952">
                            <img src="/assets/media/icono-cel.gif">+562 22 200 29 52
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">

                        <thead>
                            <tr>
                                <th>Urgencia</th>
                                <th>Tipo de Servicio</th>
                                <th>Tope por evento</th>
                                <th>Eventos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="3">
                                    <img src="/assets/media/icono-serv-salud.png">
                                    <strong>Derecho de URGENCIA</strong>
                                </td>
                                <td>
                                    <a class="enlace">Atención de URGENCIA</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Atención de Médico DE URGENCIA</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Valoración clínica por Médico Traumatólogo</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td rowspan="4">
                                    <img src="/assets/media/icono-serv-salud.png">
                                    <strong>Analgésicos (dolor)</strong>
                                </td>
                                <td>
                                    <a class="enlace">Anti-inflamatorios (inflamación)</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Antipiréticos (fiebre)</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Material de uso médico: yeso, venda, etc.</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Equipo y material de sutura</a>
                                </td>
                                <td>Sin tope</td>
                                <td>Ilimitado</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#eme" data-toggle="tab">
                                                    <span class="round-tabs three">Preguntas Frecuentes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="eme">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <ul>
                                                            <li>
                                                                <strong>¿Cuándo puedo usar  la asistencia sala de urgencia?</strong> Cuando el titular o alguno de sus beneficiarios lo necesiten, no hay período de carencia.
                                                            </li>
                                                            <li>
                                                                <strong>¿Qué necesito para usar la asistencia? </strong> Tener al día el pago mensual.
                                                            </li>
                                                            <li>
                                                                <strong>¿Cuánto tiempo debo permanecer  en la Asistencia Sala de Urgencia?</strong> La asistencia es voluntaria y por lo tanto, es el cliente, quien decide el tiempo de permanencia.
                                                            </li>
                                                            <li>
                                                                <strong>¿Cuál es el teléfono  de atención?</strong> 800200347 0 222002952
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->

    <!-- Contenidos asistencia clinica-->
    <div id="infoAsistenciaClinica" class="tab-pane p0 fade">
        <section class="container asistencia py-4">
            <div class="row">
                <div class="col-md-8">
                    <h1>¿Qué es la Asistencia Clínica Domiciliaria ?</h1>
                    <p class="textjustify">
                        Es la asistencia que otorga a sus afiliados y/o su grupo familiar que sufran de un accidente o una enfermedad previa solicitud vía telefónica, la coordinación del envío de un médico general al lugar donde se encuentren. Esta asistencia a su vez brindará
                        orientación Salud telefónica, cualquier día de la semana las 24 (veinticuatro) horas del día, los trescientos sesenta y cinco (365) días del año.
                        <br> Adicionalmente contempla la toma de exámenes Perfil Bioquímico, Perfil Lipídico, Orina Completa.
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="boxActivarAsistencia">
                        <h1>¿Necesitas activar tu asistencia?</h1>
                        <a href="tel:+562223512640">
                            <img src="/assets/media/icono-cel.gif">+562 22 351 26 40
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="container py-4">
            <div class="row">
                <div class="col">
                    <table class="table table-reflow tablaServicios">

                        <thead>
                            <tr>
                                <th>TIPO DE SERVICIO</th>
                                <th>CUADRO DE COBERTURAS</th>
                                <th>MONTO MÁXIMO POR EVENTO ($)</th>
                                <th>MÁXIMO DE EVENTOS AL AÑO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="3">
                                    <img src="/assets/media/icono-serv-hogar.png">
                                    <strong>Clínica Domiciliaria (Titular +  Grupo familiar)</strong>
                                </td>
                                <td>
                                    <a class="enlace">Visita de Médico a Domicilio</a>
                                </td>
                                <td>Sin Límite</td>
                                <td>4 eventos anuales acumulables al grupo familiar</td>
                            </tr>
                            <tr>
                                <td><a class="enlace">Orientación Salud Telefónica</a>
                                </td>
                                <td>Sin Límite</td>
                                <td>Sin Límite</td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="enlace">Toma de exámenes: Perfil Bioquímico, Perfil Lipídico, Orina Completa.</a>
                                </td>
                                <td>Sin Límite</td>
                                <td>2 eventos anuales acumulables al grupo familiar.</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <section class="container-fluid p0">
            <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
                <!-- tab 1 -->
                <section class="bannersTabs">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="board">
                                <div class="board-inner">
                                    <div class="container">
                                        <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                            <li>
                                                <a href="#zeta" data-toggle="tab" class="active show">
                                                    <span class="round-tabs one">Preguntas frecuentes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="tab-content Contenido">
                                    <div class="tab-pane p0 fade in active" id="zeta">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="container py-5">
                                                    <div>
                                                        <ul>
                                                            <li> <strong> ¿Cuándo puedo usar  la asistencia Clínica Domicialiaria? </strong> Cuando el titular o alguno de sus beneficiarios lo necesiten, no hay período de carencia.</li>
                                                            <li> <strong> ¿Qué necesito para usar la asistencia?</strong> Tener al día el pago mensual.</li>
                                                            <li> <strong> ¿Cuánto tiempo debo permanecer  en la Asistencia Clínica Domiciliaria?</strong> La asistencia es voluntaria y por lo tanto, es el cliente, quien decide el tiempo de permanencia.</li>
                                                            <li> <strong> ¿Cuál es la cobertura? </strong> Este servicio tiene una cobertura en todo Chile con excepción de las islas, excluyendo Isla de Chiloé.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- fin tab 1 -->
            </div>
        </section>
    </div>
    <!-- fin contenidos -->

</div>

<!-- MOdal Info plomeria -->
<div id="infoModalServicioPlomeria" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Plomería</p>
                    <ul>
                        <li class="textjustify">En caso que se presente alguna rotura o fuga de agua o avería que imposibilite el suministro, se enviará a la brevedad posible un técnico especializado, que realizará la asistencia necesaria para reestablecer el servicio, siempre
                            y cuando el estado de las redes lo permitan.</li>
                        <li class="textjustify">Cambio de llaves de paso: en caso que se requiera el cambio de una llave de paso debido a una avería de ésta o que no tenga reparación, MOK gestionará el envío de un técnico especializado para llevar a cabo el cambio de la pieza.
                            La llave será provista por el Servicio de Asistencia y tendrá características básicas o estándar y que cumpla para reestablecer el servicio.</li>
                        <li class="textjustify">Cambio de Fitting del estaque WC: a consecuencia de un desgaste o mal funcionamiento del fitting del estanque del WC de la vivienda afiliada, y a solicitud del afiliado, MOK gestionará el envío de un técnico especializado para
                            realizar el cambio de dicho artefacto y normalizar el funcionamiento del WC. El fitting será cubierto por MOK según la cobertura.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info electricidad -->
<div id="infoModalServicioElectricidad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Electricidad</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de una avería súbita e imprevista en las instalaciones eléctricas propias en el interior de la vivienda afiliado, que corresponda al domicilio permanente del afiliado, se produzca una falta de energía eléctrica
                            en forma total o parcial (corto circuito), se enviará un técnico especializado que realizará la asistencia necesaria para reestablecer el suministro de energía eléctrica, siempre y cuando el estado de las redes lo permita.</li>
                        <li class="textjustify">Este servicio se brindará en las redes internas del domicilio y en ningún caso a redes que pertenezcan a áreas comunes o instalaciones fuera de norma SEC o que no pertenezcan al plano original de la vivienda.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info Vidrieria -->
<div id="infoModalServicioVidrieria" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Vidriería</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de un hecho imprevisto se produzca la rotura de alguno de los vidrios de puertas o de ventanas exteriores y se genere una necesidad inmediata de reposición de vidrios, que formen parte del perímetro horizontal
                            de la vivienda afiliada, poniendo en riesgo la seguridad de la misma, sus ocupantes, o de terceros, se enviará un técnico para solucionar esa eventualidad, siempre y cuando las condiciones climáticas, de traslado o del día
                            y hora lo permitan.</li>
                        <li class="textjustify">El servicio se asistencia proveerá de un vidrio normal, transparente e incoloro, de un espesor de 3 a 4 mm., y que cumpla la función de proteger la vivienda.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info Cerrajeria -->
<div id="infoModalServicioCerrajeria" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Cerrajería</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de cualquier hecho accidental, como pérdida, extravío o robo de las llaves, inutilización de la cerradura por intento de hurto u otra causa maliciosa que impida la apertura de la puerta principal de acceso
                            peatonal a la vivienda afiliada o bien que ponga en riesgo la seguridad de la misma, y a solicitud del afiliado, se enviará con la mayor brevedad posible un técnico especializado que realizará la asistencia necesaria para reestablecer
                            el acceso al inmueble y el correcto cierre de la puerta de la vivienda afiliada.</li>
                        <li class="textjustify">Cuando el cilindro de la puerta principal peatonal se considere inutilizable, éste será repuesto sin costo para el cliente con un juego de dos llaves.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info salud -->
<div id="ExamenOftalmologico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Examen Oftalmológico</p>
                    <ul>
                        <li class="textjustify">
                            MOK coordinará a solicitud del cliente, la realización de una consulta oftalmológica con un profesional de la especialidad en forma gratuita en la red de proveedores y Centros Oftalmológicos que se encuentren en convenio con MOK.
                        </li>
                        <li class="textjustify">
                            La consulta incluirá la revisión (chequeo básico de los principales componentes oculares) y prescripción profesional que es habitual en una primera consulta médica de especialidad y no incluirá exámenes complementarios, lentes o el pago de medicamentos
                            prescritos por cualquier receta del oftalmólogo.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info salud -->
<div id="traslado" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Traslado en caso de enfermedad o accidente</p>
                    <ul>
                        <li class="textjustify">
                            En el caso de producirse una enfermedad, lesión o accidente en el hogar que le impida trasladarse por sus propios medios, y previa solicitud vía telefónica del servicio, MOK organizará el traslado terrestre en el medio que considere más idóneo el facultativo
                            que lo atienda, hasta el Centro Hospitalario adecuado.
                        </li>
                        <li class="textjustify">
                            El equipo médico de MOK mantendrá los contactos necesarios con el Centro Hospitalario o facultativo que atienda al beneficiario para supervisar que el transporte sea adecuado.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info salud -->
<div id="Orientacionmedicatelefonica" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Orientación Médica Telefónica</p>
                    <ul>
                        <li class="textjustify">
                            Previa solicitud del afiliado, MOK brindará orientación médica telefónica al grupo familiar, cualquier día de la semana, las 24 horas del día, los 365 días del año, para resolver sus consultas sobre alguna dolencia o enfermedad.
                        </li>
                        <li class="textjustify">
                            Queda entendido que el servicio se prestará como una orientación y el personal médico profesional en ningún momento diagnosticará o recetará a un paciente vía telefónica.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- MOdal Info plomeria -->
<div id="infoModalServicioPlomeriaMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Plomería</p>
                    <ul>
                        <li class="textjustify">En caso que se presente alguna rotura o fuga de agua o avería que imposibilite el suministro, se enviará a la brevedad posible un técnico especializado, que realizará la asistencia necesaria para reestablecer el servicio, siempre
                            y cuando el estado de las redes lo permitan.</li>
                        <li class="textjustify">Cambio de llaves de paso: en caso que se requiera el cambio de una llave de paso debido a una avería de ésta o que no tenga reparación, MOK gestionará el envío de un técnico especializado para llevar a cabo el cambio de la pieza.
                            La llave será provista por el Servicio de Asistencia y tendrá características básicas o estándar y que cumpla para reestablecer el servicio.</li>
                        <li class="textjustify">Cambio de Fitting del estaque WC: a consecuencia de un desgaste o mal funcionamiento del fitting del estanque del WC de la vivienda afiliada, y a solicitud del afiliado, MOK gestionará el envío de un técnico especializado para
                            realizar el cambio de dicho artefacto y normalizar el funcionamiento del WC. El fitting será cubierto por MOK según la cobertura.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info electricidad -->
<div id="infoModalServicioElectricidadMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Electricidad</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de una avería súbita e imprevista en las instalaciones eléctricas propias en el interior de la vivienda afiliado, que corresponda al domicilio permanente del afiliado, se produzca una falta de energía eléctrica
                            en forma total o parcial (corto circuito), se enviará un técnico especializado que realizará la asistencia necesaria para reestablecer el suministro de energía eléctrica, siempre y cuando el estado de las redes lo permita.</li>
                        <li class="textjustify">Este servicio se brindará en las redes internas del domicilio y en ningún caso a redes que pertenezcan a áreas comunes o instalaciones fuera de norma SEC o que no pertenezcan al plano original de la vivienda.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info Vidrieria -->
<div id="infoModalServicioVidrieriaMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Vidriería</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de un hecho imprevisto se produzca la rotura de alguno de los vidrios de puertas o de ventanas exteriores y se genere una necesidad inmediata de reposición de vidrios, que formen parte del perímetro horizontal
                            de la vivienda afiliada, poniendo en riesgo la seguridad de la misma, sus ocupantes, o de terceros, se enviará un técnico para solucionar esa eventualidad, siempre y cuando las condiciones climáticas, de traslado o del día
                            y hora lo permitan.</li>
                        <li class="textjustify">El servicio se asistencia proveerá de un vidrio normal, transparente e incoloro, de un espesor de 3 a 4 mm., y que cumpla la función de proteger la vivienda.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info Cerrajeria -->
<div id="infoModalServicioCerrajeriaMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Cerrajería</p>
                    <ul>
                        <li class="textjustify">Cuando a consecuencia de cualquier hecho accidental, como pérdida, extravío o robo de las llaves, inutilización de la cerradura por intento de hurto u otra causa maliciosa que impida la apertura de la puerta principal de acceso
                            peatonal a la vivienda afiliada o bien que ponga en riesgo la seguridad de la misma, y a solicitud del afiliado, se enviará con la mayor brevedad posible un técnico especializado que realizará la asistencia necesaria para reestablecer
                            el acceso al inmueble y el correcto cierre de la puerta de la vivienda afiliada.</li>
                        <li class="textjustify">Cuando el cilindro de la puerta principal peatonal se considere inutilizable, éste será repuesto sin costo para el cliente con un juego de dos llaves.</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal Info salud Mujer-->
<div id="ExamendePapanicolaouMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Examen de Papanicolaou o examen de mamografía o presupuesto y limpieza dental.</p>
                    <ul>
                        <li class="textjustify">
                            MOK coordinará a solicitud del cliente, la realización de uno (1) de los exámenes indicados en la red de prestadores y centros odontológicos en convenio con MOK.
                        </li>
                        <li class="textjustify">
                            El pago de los gastos se aplicará después de la cobertura de ISAPRE o Fonasa. Si el afiliado no cuenta con Sistema Previsional de Salud, MOK cubrirá de igual manera el costo por el examen solicitado.Cuando el cilindro de la puerta principal peatonal se
                            considere inutilizable, éste será repuesto sin costo para el cliente con un juego de dos llaves.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info salud -->
<div id="trasladoMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Traslado en caso de enfermedad o accidente</p>
                    <ul>
                        <li class="textjustify">
                            En el caso de producirse una enfermedad, lesión o accidente en el hogar que le impida trasladarse por sus propios medios, y previa solicitud vía telefónica del servicio, MOK organizará el traslado terrestre en el medio que considere más idóneo el facultativo
                            que lo atienda, hasta el Centro Hospitalario adecuado. El equipo médico de MOK mantendrá los contactos necesarios con el Centro Hospitalario o facultativo que atienda al beneficiario para supervisar que el transporte sea adecuado.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info salud -->
<div id="OrientacionmedicatelefonicaMujer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Orientación médica telefónica general, nutricional y ginecológica</p>
                    <ul>
                        <li class="textjustify">
                            MOK brindará a la afiliada, previa solicitud, vía telefónica, los servicios de orientación y referencia en materia general, nutricional y ginecológica.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info hogar remolque-->
<div id="infoModalRemolque" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Remolque para moto o vehículo liviano</p>
                    <ul>
                        <li class="textjustify">
                            En caso de existir alguna dificultad que no permita la circulación autónoma del vehículo particular o motocicleta del afiliado, MOK gestionará y cubrirá el costo de los servicios de remolque en grúa hasta el taller mecánico más cercano o domicilio del afiliado.
                        </li>
                        <li class="textjustify">
                            Adicionalmente, el servicio que contempla el rescate vial a través de grúa o remolque, el cliente podrá solicitar envío de bencina, sólo en casos de que el afiliado quedase en panne de combustible y lo corrobore el técnico enviado al lugar de los hechos.
                        </li>
                        <li class="textjustify">
                            Así mismo, y en el caso de necesitar el paso de corriente por falta de batería, MOK gestionará el envío de un técnico para reestablecer este suministro, quien realizará un puente de corriente a la batería del vehículo del cliente.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Info hogar rueda-->
<div id="infoModalRueda" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <p>Cambio de rueda de repuesto a causa de pinchazo o accidente</p>
                    <ul>
                        <li class="textjustify">
                            Cambio de rueda de repuesto en aquellos casos de pinchazo o accidente del afiliado. Es el afiliado quien debe proveer la rueda de repuesto para el cambio de ésta.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>