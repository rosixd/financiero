<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img alt="First slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div>

        <!-- <div class="carousel-item">
            <img alt="Second slide" class="d-block w-100" src="/assets/media/slider-seguros-100.jpg" />
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide boxSeguros">
    <div class="container-fluid navTitular SeguroIndividual">
        <div class="row justify-content-around align-content-center">
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box">
                            <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-vida">Vida</a>
                            </h1>
                        </div>
                    </div>
                    <div class="d-block d-sm-none text-center">
                        <img src="/assets/media/icono-vida2.png" style="width: 55px; height: 57px;">
                        <h1>
                            <a href="/seguros-vida">Vida</a>
                        </h1>
                    </div>
                </div>
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box">
                            <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-salud">Salud</a>
                            </h1>
                        </div>
                    </div>
                    <div class="d-block d-sm-none text-center">
                        <img src="/assets/media/icono-salud2.png" style="width: 55px; height: 57px;">
                        <h1>
                            <a href="/seguros-salud">Salud</a>
                        </h1>
                    </div>
                </div>
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box">
                            <img src="/assets/media/icono-financiera_1.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a class="active" href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                            </h1>
                        </div>
                    </div>
                    <div class="d-block d-sm-none text-center">
                        <img src="/assets/media/icono-financiera_1.png" style="width: 55px; height: 57px;">
                        <h1>
                            <a class="active" href="/seguros-proteccion-financiera">Protección<br>Financiera</a>
                        </h1>
                    </div>
                </div>
                <div class="col-sm col-lg-2 col-2">
                    <div class="d-none d-sm-block">
                        <div class="box">
                            <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                            <h1>
                                <a href="/seguros-hogar">Hogar</a>
                            </h1>
                        </div>
                    </div>
                    <div class="d-block d-sm-none text-center">
                        <img src="/assets/media/icono-hogar2.png" style="width: 55px; height: 57px;">
                        <h1>
                            <a href="/seguros-hogar">Hogar</a>
                        </h1>
                    </div>
                </div>
                <!--<li>
                    <a href="/seguros-automotriz">
                    <img src="/assets/media/icono-automotriz2.png" class="automotriz"> Automotriz</a>
                </li>--> 		
        </div>
    </div>
</section>
<!-- fin barra superior -->

<section class="BarraSubmenu">
    <div class="container">
        <ul id="tabsJustified" class="nav nav-tabs justify-content-around">
            <li class="nav-item">
                <a href="/" data-target="#segurosCesantiaCB" data-toggle="tab" class="nav-link small text-uppercase active">
                    <img class="cesantia" src="/assets/media/icono-cesantia-cuenta-basica.png"> Seguro Cesantía Cuentas Básicas
                </a>
            </li>
            <li class="nav-item">
                <a href="/" data-target="#segurosContraRobo" data-toggle="tab" class="nav-link small text-uppercase">
                    <img class="robo" src="/assets/media/icono-robo-bonificacion.png"> Seguro Contra Robo con Bonificación
                </a>
            </li>
        </ul>
    </div>
</section>


<section class="container-fluid p0">
    <div id="tabsJustifiedContent" class="tab-content ContenidoSeguro">
        <!-- tab 1 -->
        <div id="segurosCesantiaCB" class="tab-pane p0 fade active show">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro protege ante cesantía involuntaria e Incapacidad Temporal.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$4.830</p>
                            <p class="uf">UF 0,178</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSEcondicioneslegales" data-toggle="modal" style="box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>

                                <table class="table table-responsive mt-3">
                                    <thead>
                                        <tr>
                                            <th>Coberturas</th>
                                            <th>Póliza</th>
                                            <th>Capital Asegurado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cesantía</td>
                                            <td>POL 1 2013 0122 </td>
                                            <td>hasta 4 cuotas con tope por cuota de UF 2 ($54.322)</td>
                                        </tr>
                                        <tr>
                                            <td>Incapacidad Temporal</td>
                                            <td>POL 1 2013 0122 </td>
                                            <td>hasta 4 cuotas con tope por cuota de UF 2 ($54.322)</td>
                                        </tr>
                                        <tr>
                                            <td>Prima Mensual</td>
                                            <td></td>
                                            <td>UF 0,178 ( $4.830)</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">
                                    <a class="btn btn-primary btn-lg btnRojo" href="#ModalFormularioContactoSeguroInfoCesantia" role="button" data-toggle="modal">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#uno" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dos" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tres" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatro" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="uno">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Asegurado recibirá una indemnización de hasta 4 cuotas con tope de UF 2 en caso de Cesantía (involuntaria) o Incapacidad Temporal.</p>
                                                    <p>Correspondientes a las cuentas básicas del asegurado (agua, luz, gas y telefonía).</p>
                                                    <ul>
                                                        <li>De 31 a 60 días : Primera cuota.</li>
                                                        <li>De 61 a 90 días : Segunda cuota.</li>
                                                        <li>De 91 a 120 días : Tercera cuota.</li>
                                                        <li>De 121 a 150 días : Cuarta cuota.</li>
                                                    </ul>
                                                    <p>Condiciones: Antigüedad Laboral: 6 meses desde la fecha de contratación del seguro.</p>
                                                    <p>Carencia: 30 días desde el inicio de la vigencia del seguro.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <p>Este seguro cuenta con completa asistencia de orientación telefónica de acuerdo con el siguiente detalle:</p>
                                                    <ul class="p0">
                                                        <li>Orientación general telefónica para trámites administrativos en caso de cesantía.</li>
                                                        <li>Asistencia legal telefónica para trámites administrativos en caso de cesantía.</li>
                                                        <li>Asistencia Legal telefónica en caso de despido del asegurado.</li>
                                                        <li>Asistencia telefónica para reinserción laboral.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por BCI Seguros Generales S.A (www.bciseguros.cl), mediante póliza colectiva N° WP8635222-2 contratada por Créditos, Organización y finanzas S:A: (COFISA). La información presentada es solo un resumen de las principales características
                                                        del Seguro Cesantía Cuentas Básicas y no reemplaza a la póliza.
                                                    </p>
                                                    <p class="textjustify">
                                                        Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 120130122.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,178 IVA incluido, precio referencia en pesos $4.830, calculado con valor de UF $27.161,48, UF fecha 01/07/2018. Oferta de Seguro válida a Diciembre
                                                        de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane p0 fade in" id="dos">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Incapacidad a causa de embarazo o cualquier enfermedad producida on motivo del embarazo o durante el trascurso del permiso pre y post natal.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="tres">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Las edades de cobertura son las siguientes:</p>
                                                    <p class="textjustify">Edad Minima de ingreso del asegurado: 18 años.</p>
                                                    <p class="textjustify">Edad Máxima de ingreso del asegurado: Hasta 70 años y 364 días.</p>
                                                    <p class="textjustify">Edad Máxima de permanencia del asegurado: Hasta los 75 años y 364 días.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="cuatro">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Una vez ocurrido el siniestro, el asegurado o los beneficiarios deberán realizar la declaración del siniestro en cualquiera de las tiendas ABCDIN, en un plazo máximo de 180 días contados desde ocurrido.
                                                        Deberá presentar la siguiente documentación, junto con el formulario de notificación de siniestros.</p>
                                                    <ul>
                                                        <strong>DESEMPLEO:</strong>
                                                        <li>Formulario de denuncia de siniestro firmado por el asegurado.</li>
                                                        <li>Fotocopia cédula de identidad por ambos lados.</li>
                                                        <li>Fotocopia legalizada del Finiquito.</li>
                                                        <li>Certificado de AFP con últimas 12 cotizaciones con RUT empleador.</li>
                                                        <li>Copia de las boletas de los servicios básicos (agua, luz, gas, teléfono).</li>
                                                    </ul>
                                                    <br>
                                                    <ul>
                                                        <strong>INCAPACIDAD TEMPORAL (TRABAJADORES INDEPENDIENTES)</strong>
                                                        <li>Formulario de denuncia de siniestro firmado por el asegurado.</li>
                                                        <li>Fotocopia cédula de identidad por ambos lados.</li>
                                                        <li>Licencia médica legible que indique diagnóstico y cantidad de días de reposo.</li>
                                                        <li>Exámenes e historial médico que respalde diagnóstico.</li>
                                                        <li>Certificado AFP de las últimas 12 cotizaciones con RUT pagador.</li>
                                                        <li>Copia de las boletas de los servicios básicos (agua, luz, gas, teléfono).</li>
                                                    </ul>
                                                    <br>
                                                    <p>La compañía se reserva el derecho a solicitar mayores antecedentes si lo estima necesario.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 1 -->
        <!-- tab 2 -->
        <div id="segurosContraRobo" class="tab-pane p0 fade">
            <div class="container-fluid">
                <div class="row descripcion">
                    <div class="col-sm-12 col-md-10 py-5">
                        Seguro que entrega indemnización en dinero en caso de que el asegurado sufra hurto o robo de sus objetos personales. Además, te bonifica con un 30% de lo pagado si no lo usas al 3er año.
                    </div>

                    <div class="col-sm-12 col-md-2 bgrojo py-3">
                        <div class="datosPrima">
                            <span>Prima mensual desde</span>
                            <p class="monto">$6.300</p>
                            <p class="uf">UF 0,232</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid py-5">
                <div class="row">
                    <div class="container">
                        <div class="row datosTabla">
                            <div class="col-md-12">
                                <h1>Planes y tarifas</h1>
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#ModalSEcondicioneslegales" data-toggle="modal" style="box-shadow: none;">VER CONDICIONES LEGALES</a> -->
                                <!-- <a class="btn btn-primary btn-sm float-right btntarifa" href="#" data-toggle="modal" style="text-decoration: none; box-shadow: none;">VER PÓLIZA</a> -->

                                <div class="clearfix"></div>

                                <table class="table table-responsive mt-3">
                                    <thead>
                                        <tr>
                                            <th>Cobertura</th>
                                            <th>Póliza</th>
                                            <th>Capital Asegurado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Robo con violencia en las personas </td>
                                            <td>POL 1 2013 0377 (Artículo 3, Letra A2)</td>
                                            <td>Límite máximo por articulo UF 2<br> 2 eventos al año (tope de UF 10 por evento)</td>
                                        </tr>
                                        <tr>
                                            <td>Prima Mensual </td>
                                            <td></td>
                                            <td>UF 0,232 ( $6.300)</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <span class="letraChica">*Los precios son aproximados según el valor UF (1 de Julio de 2018).</span>

                                <div class="clearfix"></div>

                                <div class="py-5 text-center">
                                    <a class="btn btn-primary btn-lg btnRojo" href="#ModalFormularioContactoSeguroInfoCesantia" role="button" data-toggle="modal">QUIERO SER CONTACTADO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="bannersTabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="board">
                            <div class="board-inner">
                                <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                                    <li>
                                        <a href="#unoa" data-toggle="tab" class="active show">
                                            <span class="round-tabs one">¿Qué cubre?</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#dosa" data-toggle="tab">
                                            <span class="round-tabs two">¿Qué no cubre?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tresa" data-toggle="tab">
                                            <span class="round-tabs three">¿Cuáles son los requisitos?</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#cuatroa" data-toggle="tab">
                                            <span class="round-tabs three">¿Qué hacer en casos de siniestro?</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="tab-content Contenido">
                                <div class="tab-pane p0 fade in active" id="unoa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p>Seguro que entrega indemnización en caso que el asegurado sufra robo o hurto de sus objetos personales.</p>
                                                    <p>Además cuenta con el beneficio de Bonificación", que consiste en el reembolso de las primas efectivamente pagadas por haber permanecido vigente (y de manera ininterrumpida), durante todo el periodo de
                                                        vigencia de treinta y seis (36) meses, contados desde la aceptación de su solicitud de incorporación. Siempre y cuando no haya denunciado ningún siniestro, ni haber solicitado el pago de indemnizaciones
                                                        durante los meses de vigencia del seguro.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Beneficios Adicionales</h1>
                                                    <p>Este seguro cuenta con completa asistencia de orientación telefónica de acuerdo con el siguiente detalle:</p>
                                                    <ul class="p0">
                                                        <li>Orientación general telefónica para trámites administrativos en caso de cesantía.</li>
                                                        <li>Asistencia legal telefónica para trámites administrativos en caso de cesantía.</li>
                                                        <li>Asistencia Legal telefónica en caso de despido del asegurado.</li>
                                                        <li>Asistencia telefónica para reinserción laboral.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <h1>Textos Legales</h1>
                                                    <p class="textjustify">
                                                        El riesgo es cubierto por BCI Seguros Generales S.A (www.bciseguros.cl), mediante póliza colectiva N° WP8619442 contratada por Créditos, Organización y Finanzas (COFISA) S.A.
                                                    </p>
                                                    <p class="textjustify">
                                                        La información presentada es solo un resumen de las principales características del Seguro Contra Robo con Bonificación y no reemplaza a la póliza. Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de
                                                        la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 120130377.
                                                    </p>
                                                    <p class="textjustify">
                                                        El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                                    </p>
                                                    <p class="textjustify">
                                                        (<span class="rojo">*</span>) Prima bruta mensual UF 0,232 IVA incluido, precio referencia en pesos $6.300, calculado con valor de UF $27.161, 48 UF fecha 01/07/2018. Oferta de Seguro válida a Diciembre
                                                        de 2019.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane p0 fade in" id="dosa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">Seguro que entrega indemnización en caso que el asegurado sufra robo o hurto de sus objetos personales.</p>
                                                    <p class="textjustify">Además, cuenta con el beneficio de "Bonificación", que consiste en el reembolso de las primas efectivamente pagadas por haber permanecido vigente (y de manera ininterrumpida), durante todo el periodo
                                                        de vigencia de treinta y seis (36) meses, contados desde la aceptación de su solicitud de incorporación. <br>Siempre y cuando no haya denunciado ningún siniestro, ni haber solicitado el pago de indemnizaciones
                                                        durante los meses de vigencia del seguro.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="tresa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <ul>
                                                        <p class="textjustify">Cobertura Robo con Violencia en las Personas: </p>
                                                        <li>Pérdidas que no sean constitutivas de delito de robo con violencia en las personas, tales como hurtos, extravíos, robo con fuerza en las cosas, apropiación indebida, estafas y otros engaños.</li>
                                                        <li>Pérdida o daño, cuando existe una situación anormal a causa de guerra civil o entre países, o estado de guerra, antes o después de su declaración, o sublevación, huelga, motín, alboroto popular,
                                                            conmoción civil, insurrección, revolución o rebelión, ni cuando ocurran temblores o terremotos o erupciones volcánicas, huracanes, o cualquier otro fenómeno meteorológico.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane p0 fade in" id="cuatroa">
                                    <div class="container-fluid gris">
                                        <div class="row">
                                            <div class="container py-5">
                                                <div>
                                                    <p class="textjustify">ROBO CON VIOLENCIA EN LAS PERSONAS:</p>
                                                    <ul>
                                                        <li>Formulario de denuncia de siniestro firmado por el asegurado.</li>
                                                        <li>Fotocopia cédula de identidad por ambos lados (en caso de pérdida adjuntar copia de bloqueo).</li>
                                                        <li>Copia de ratificiación de denuncia en Fiscalía o copia del parte policial con relato de los hechos.</li>
                                                        <li>Declaración simple, indicando detalle del hecho y valor correspondiente de lo sustraído (Relato).</li>
                                                    </ul>
                                                    <br>
                                                    <p class="textjustify">PROTECCIÓN PATRIMONIAL:</p>
                                                    <ul>
                                                        <li>Formulario de denuncia de siniestro firmado por el asegurado.</li>
                                                        <li>Fotocopia cédula de identidad por ambos lados (en caso de pérdida adjuntar copia de bloqueo).</li>
                                                        <li>Certificado emitido por Institución financiera con código de bloqueo de documentos.</li>
                                                        <li>Copia de ratificiación de denuncia en Fiscalía o copia del parte policial.</li>
                                                        <li>Declaración simple, indicando detalle del hecho y valor correspondiente de lo reclamado.</li>
                                                    </ul>
                                                    <br>
                                                    <p>La compañía se reserva el derecho a solicitar mayores antecedentes si lo estima necesario.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- fin tab 2 -->
    </div>
</section>

<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoSeguros">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-sm py-3">
                    <a href="#">
                        <img src="/assets/media/bannericonseguros.png" style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->

<!-- Modales -->

<!-- Modal seguro vida condiciones legales-->
<div id="ModalSPFcesantiacondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-vida.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 270 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del seguro de vida y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en la CMF bajo códigos POL 220130289, CAD 320130293 y CAD 320130292.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,2025 IVA incluido, precio referencia en pesos $5.500, calculado con valor de UF $27161,48, UF fecha 01/07/2018. Oferta de Seguro válida a Diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal seguro ESCOLARES condiciones legales-->
<div id="ModalSEcondicioneslegales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">

                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-vida.png">
                    <h2>Condiciones Legales</h2>

                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <p class="textjustify">
                                    El riesgo es cubierto por Ohio National Seguros de Vida S.A, mediante póliza colectiva N° 273 y 278 contratada por Créditos, organización y Finanzas S.A (COFISA).
                                </p>
                                <p class="textjustify">
                                    La información presentada es solo un resumen de las principales características del Seguro Escolaridad y no reemplaza a la póliza.
                                </p>
                                <p class="textjustify">
                                    Las coberturas, condiciones y exclusiones se rigen según lo dispuesto en las condiciones particulares de la póliza colectiva y en las Condiciones Generales depositadas en Comisión para el Mercado Financiero (CMF) bajo códigos POL 220160370, CAD 320130642
                                    y CAD 320130292.
                                </p>
                                <p class="textjustify">
                                    El Seguro es intermediado por ABCDIN Corredores de Seguros Ltda., quien asume las obligaciones propias de los seguros que intermedia.
                                </p>
                                <br>
                                <p class="textjustify">
                                    (<span class="rojo">*</span>) Prima bruta mensual UF 0,368 IVA incluido, precio referencia en pesos $9.990, calculado con valor de UF $27.161,48, UF fecha 01/1/2018. Oferta de Seguro válida al 01 de diciembre de 2019.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal formulario de contacto -->
<div id="ModalFormularioContactoSeguroInfoCesantia" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal px-3">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-formulario-contacto.png">
                    <h2>Formulario de Contacto</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1">
                                <form id="formulariocontacto" class="formulario formValida">
                                    <div class="form-group">
                                        <label class="contactoCredito" for="nombrecompleto">Nombre Completo  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control px-3 pb-1 soloLetras requerido" maxlength="100" id="nombrecompleto" name="nombrecompleto" placeholder="JOSÉ FERNANDEZ VALDÉS">
                                    </div>
                                    <input type="hidden" value="LandingContato" name="formulario">
                                    <div class= "clearfix"></div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="rut">Rut  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="rut" name="rut" placeholder="12.345.678-9">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefono">Teléfono  <span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" id="telefono" maxlength="20" name="telefono" placeholder="2 2345 6789">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="email">Email  <span class="rojo requeridos">*</span></label>
                                        <input type="text" class="form-control  px-3 pb-1 requerido" id="email" name="email" placeholder="josefernandez@mail.cl">
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposeguro">Seguro que quieres contratar  <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposeguro" name="tiposeguro">
                                            <option disabled selected>Elige tu seguro</option>
                                            <option value="01">Seguro Vida Avance</option>
                                            <option value="02">Seguro Catastrófico familiar</option>
                                            <option value="03">Seguro Salud Dental</option>
                                            <option value="04">Seguro Escolaridad</option>
                                            <option value="05">Seguro Vida</option>
                                            <option value="06">Seguro Cesantía Cuentas Básicas</option>
                                            <option value="07">Seguro Contra robo con bonificación</option>
                                            <option value="08">Seguro Hogar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="contactoCredito" for="tiposolicitud">Tipo de solicitud <span class="rojo requeridos">*</span></label>
                                        <select class="form-control custom-select px-3 pb-1 requerido" id="tiposolicitud" name="tiposolicitud">
                                            <option disabled selected>Elige tu tipo de solicitud</option>
                                            <option value="consulta">Consulta</option>
                                            <option value="reclamo">Reclamo</option>
                                            <option value="masinfo">Más información comercial</option>
                                        </select>
                                    </div>
                                    <div class= "clearfix"></div>
                                    <div class="form-group mt-3">
                                        <textarea class="form-control px-3" id="mensaje" maxlength="5000" name="mensaje" rows="5" placeholder="Comentarios"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 msjval" style="text-align: justify;"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: justify;">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="btn btn-primary btn-block btnRojo" id="botonformulariocontacto" type="submit">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal de respuesta -->
<div id="ModalRespuestaSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="/assets/media/icono-info-mensaje.png">
                    <h2 class="mensajeExito">Hemos recibido tu solicitud.<br> Nos contactaremos contigo en menos de 48 horas hábiles.
                    </h2>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Modal de respuesta error -->
<div id="ModalRespuestaSolicituderror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    Error
                </div>
            </div>
        </div>
    </div>
</div>