<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <form class="boxInfoMovimientos">
            <div class="row contenidoFormulario1 boxActualizaDatos">
                <div class="col-md-4">
                    <h3 class="tituDatos">Datos personales</h3>

                    <div class="form-group">
                        <label for="nombrecompleto">NOMBRE COMPLETO</label>
                        <input type="text" class="form-control" id="nombrecompleto" placeholder="JOSÉ LUIS">
                    </div>
                    <div class="form-group">
                        <label for="apellidopaterno">APELLIDO PATERNO</label>
                        <input type="text" class="form-control" id="apellidopaterno" placeholder="JOSÉ LUIS">
                    </div>


                    <div class="form-group">
                        <label for="apellidomaterno">APELLIDO MATERNO</label>
                        <input type="text" class="form-control" id="apellidomaterno" placeholder="PAZ*********@MAS********.CL">
                    </div>


                    <div class="form-group">
                    <label for="rut">RUT</label>
                    <input type="text" class="form-control" id="rut" placeholder="123456******">
                    </div>


                    <div class="form-group">
                    <label for="fechadenacimiento">FECHA DE NACIMEINTO</label>
                    <input type="text" class="form-control" id="fechadenacimiento" placeholder="123456******">
                    </div>

                </div>
                <div class="col-md-4">
                    <div>
                        <h3 class="tituDatos float-left">Prueba</h3>
                        <h3 class="tituDatos float-right inactivo">Direcci&oacute;n Comercial</h3>
                    </div>
                

                    <div class="clearfix"></div>

                    <div class="form-group">
                        <label for="FormControlInput1">NACIONALIDAD</label>
                        <input type="text" class="form-control" id="FormControlInput6" placeholder="4 1/2 ORIENTE A 2498">
                    </div>


                    <div class="form-group">
                    <label for="FormControlInput1111">N&uacute;MERO</label>
                    <input type="text" class="form-control" id="FormControlInput7" placeholder="2345">
                    </div>


                    <div class="form-group">
                    <label for="FormControlInput11111">N DE DEPTO O CASA</label>
                    <input type="text" class="form-control" id="FormControlInput8" placeholder="1234">
                    </div>


                    <div class="form-group">
                    <label for="FormControlInput111111">REGI&oacute;N</label>
                    <input type="text" class="form-control" id="FormControlInput9" placeholder="REGI&oacute;N DEL MAULE">
                    </div>

                    <div>
                    <div class="form-group w1 float-left">
                        <label for="FormControlInput1111111">CIUDAD</label>
                        <input type="text" class="form-control" id="FormControlInput111111110" placeholder="TALCA">
                    </div>

                        <div class="form-group w1 float-right">
                        <label for="FormControlInput111111111">COMUNA</label>
                        <input type="text" class="form-control" id="FormControlInput111111111111" placeholder="TALCA">
                    </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-12"><button class="btn btn-primary btn-block btnRojo mt-4" type="submit">ACTUALIZAR</button></div>
            </div>
        </form>
    </div>
</section>
<script>
    var appListScript = appListScript || [];
    appListScript.push('/public/modules/index.js');
</scrip>