<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li class="active" data-slide-to="0" data-target="#carouselIndicators">&nbsp;</li>
        <li data-slide-to="1" data-target="#carouselIndicators">&nbsp;</li>
    </ol> -->

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active"><img alt="First slide" class="d-block w-100" src="/assets/media/slider-asesoria-100.jpg" /></div>

        <!-- <div class="carousel-item"><img alt="Second slide" class="d-block w-100" src="/assets/media/slider-asesoria-100.jpg" /></div> -->
    </div>
    <!-- <a class="carousel-control-prev" data-slide="prev" href="#carouselIndicators" role="button">
        <span class="sr-only">Previous</span> 
    </a> 
    <a class="carousel-control-next" data-slide="next" href="#carouselIndicators" role="button"> 
        <span class="sr-only">Next</span> 
    </a> -->
</div>
<!-- fin slider central -->

<!-- contenidos -->

<section class="cajasVideo p0">
    <div class="container-fluid p0 m0">
        <div class="row no-gutters">
            <div class="col-sm-12 col-md-4">
                <a class="videoBox align-items-center" href="https://www.youtube.com/embed/fu-fNkiYS4E" target="_blank">
                    <img class="img-fluid w-100" src="/assets/media/banner1-cupo.jpg" />
                </a>
            </div>

            <div class="col-sm-12 col-md-4">
                <a class="videoBox align-items-center" href="https://www.youtube.com/embed/eRPPzTXUkMA" target="_blank">
                    <img class="img-fluid w-100" src="/assets/media/banner2-clave.jpg" />
                </a>
            </div>

            <div class="col-sm-12 col-md-4">
                <a class="videoBox align-items-center" href="https://www.youtube.com/embed/KplBHhrmGOE" target="_blank">
                    <img class="img-fluid w-100" src="/assets/media/banner3-tarjeta.jpg" />
                </a>
            </div>
        </div>
    </div>
</section>
<!-- selector de beneficios -->
<section class="bannersTabs preguntasFrec">
    <div class="container-fluid">
        <div class="row">
            <div class="board">
                <div class="board-inner">
                    <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                        <li>
                            <h1><span>Preguntas</span> Frecuentes</h1>
                        </li>
                        <li>
                            <a href="#tarjeta" data-toggle="tab" class="active show">
                                <img class="tarjeta" src="/assets/media/icono-asesoria-tarjeta.png">
                                <span class="round-tabs one">Tarjeta</span>
                            </a>
                        </li>
                        <li>
                            <a href="#pagos" data-toggle="tab">
                                <img class="asesoria" src="/assets/media/icono-asesoria-pagos.png">
                                <span class="round-tabs two">Pagos</span>
                            </a>
                        </li>
                        <li>
                            <a href="#seguros" data-toggle="tab">
                                <img class="seguros" src="/assets/media/icono-asesoria-seguros.png">
                                <span class="round-tabs three">Seguros</span>
                            </a>
                        </li>
                        <li>
                            <a href="#glosario" data-toggle="tab">
                                <img class="glosario" src="/assets/media/icono-asesoria-glosario.png">
                                <span class="round-tabs four">Glosario</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tarjeta">
                        <div class="container mt-5 mb-5">
                            <div id="accordion" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            ¿Cómo puedo obtener mi Tarjeta abcvisa?
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Cómo puedo obtener mi Tarjeta abcvisa?</h5>
                                            <p class="textjustify">Si aún no tienes tu Tarjeta abcvisa y quieres ser parte de una amplia red de comercios y beneficios, puedes solicitar la tarjeta en
                                                <a href="/tarjeta-abcvisa#ModalFormularioQuieroMiTarjeta" target="_blank" style="text-decoration: none; color: #252525;"><strong> www.abcvisa.cl</strong></a> en nuestras tiendas Abcdin y Dijon a lo largo de Chile.
                                                Infórmate de los requisitos para obtener la Tarjeta
                                                <a class="requisitostarjeta" href="#" style="text-decoration: none; color: #252525;"><strong>Aquí</strong></a>.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Solicita tu clave
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="card-body collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Qué claves necesito para utilizar mi Tarjeta abcvisa?</h5>
                                            <p class="textjustify">Para poder utilizar tu tarjeta abcvisa, debes tener dos claves: <strong>Clave Pin</strong> para comprar de manera presencial, y otra llamada <strong>Clave Internet</strong> que te servirá para compras online.
                                                Ambas las puedes solicitar en Servicios Financieros de nuestras tiendas con nuestros ejecutivos. Recuerda nunca compartir tus contraseñas.
                                            </p>
                                        </div>
                                        <div class="card-header collapsed" data-toggle="collapse" href="#collapsea">
                                            <a class="card-title"><i class="fas fa-1x fa-circle-o mr-3"></i>
                                                Clave PIN abcvisa
                                            </a>
                                        </div>
                                        <div id="collapsea" class="card-body collapse" data-parent="#collapseTwo">
                                            <div class="card-body">
                                                <h5>¿Qué es la Clave PIN?</h5>
                                                <p>Clave de 4 dígitos que te será solicitada cuando realices cualquier compra presencial en Tiendas abcdin, Dijon y los más de 100.000 comercios adheridos que la requieran.
                                                </p>
                                                <h5>¿Cómo funciona?</h5>
                                                <ul>
                                                    <li>Ingresa tu tarjeta y confirma el monto en la terminal POS.</li>
                                                    <li>Ingresa tu clave de 4 dígitos.</li>
                                                    <li>Retira tu tarjeta y listo.</li>
                                                    <li>Recuerda que por seguridad tu tarjeta debe ser ingresada mediante chip y no deslizada por lector de banda magnética.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-header collapsed" data-toggle="collapse" href="#collapseb">
                                            <a class="card-title"><i class="fas fa-1x fa-circle-o mr-3"></i>
                                                Clave Internet abcvisa
                                            </a>
                                        </div>
                                        <div id="collapseb" class="card-body collapse" data-parent="#collapseTwo">
                                            <div class="card-body">
                                                <h5>¿Qué es la Clave Internet?</h5>
                                                <p class="textjustify">Clave de 4 dígitos, que te será solicitada al realizar compras en Internet en sitios nacionales. Además te sirve para ingresar a tu cuenta en <a href="https://www.abcserviciosfinancieros.cl">www.abcvisa.cl</a>                                                    y en nuestra App abcvisa.
                                                </p>
                                                <br>
                                                <h5>¿Cómo Funciona?</h5>
                                                <p class="textjustify">Cada vez que realices compras en Internet solicitaremos tu RUT y <strong>Clave Internet abcvisa.</strong> Recuerda además que cuando compres enviaremos una clave dinámica vía SMS a tu celular para completar
                                                    la transacción.
                                                </p>
                                                <br>
                                                <h5>¿Qué es la Clave Dinámica?</h5>
                                                <p class="textjustify">La Clave Dinámica es un código que será solicitado cada vez que compres por internet que deberás utilizar para finalizar la transacción. Llegará mediante un mensaje de texto (SMS) a tu celular previamente
                                                    registrado para este fin.
                                                </p>
                                                <ul>
                                                    <li>Es un servicio gratuito.</li>
                                                    <li>Por cada compra te llegará un SMS.</li>
                                                    <li>Expira en 3 minutos.</li>
                                                    <li>Mantén siempre actualizado tu número de celular en nuestras tiendas abcdin.</li>
                                                </ul>
                                                <br>
                                                <ul>
                                                    <li></li>
                                                </ul>
                                                <h5>He olvidado mis claves o no las tengo. ¿Qué hago?</h5>
                                                <p class="textjustify">En caso que hayas olvidado alguna de tus claves o no las tengas, puedes solicitarlas en cualquiera de nuestras tiendas abcdin a lo largo de todo el país con nuestros ejecutivos.
                                                </p>
                                                <br>
                                                <h5>Consejos</h5>
                                                <ul>
                                                    <li>No entregues tu clave a nadie.</li>
                                                    <li>Renueva tu clave periódicamente.</li>
                                                    <li>Memorízala, no las escribas en alguna parte.</li>
                                                    <li>En caso de olvido, acércate a nuestras tiendas abcdin.</li>
                                                    <li>Nunca solicitaremos tu clave por teléfono o correo electrónico.</li>
                                                    <li>Recuerda que tus claves son intransferibles.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Avances en efectivo
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Cómo obtengo mi Avance en Efectivo?</h5>
                                            <p class="textjustify">Si quieres pedir un avance en efectivo con tu Tarjeta abcvisa, puedes informarte cuál es tu cupo disponible en <a href="https://www.abcserviciosfinancieros.cl">www.abcvisa.cl</a>, en nuestra app abcvisa o llamando
                                                a al 600 830 22 22. Para solicitarlo acércate a cualquier tienda abcdin o Dijon a lo largo de Chile.
                                            </p>
                                            <br>
                                            <h5>¿Qué monto, facilidades y costos asociados tengo al pedir un Avance en Efectivo?</h5>
                                            <p class="textjustify">Si deseas un Avance en Efectivo puedes girar desde $10.000 hasta $3.000.000 y pagarlo desde 3 hasta 36 cuotas, todo dependiendo del cupo y comportamiento crediticio que tengas. Puedes además postergar hasta
                                                3 meses el pago de la primera cuota. No olvides que los Avances y SuperAvances tienen un cobro único de comisión en la primera cuota que es de UF 0,1354 si el contrato fue firmado hasta el 31 de Mayo de
                                                2018 o de UF 0,1678 si fue firmado desde el 1 de Junio de 2018, y que están efectos al impuesto de timbres y estampillas. <strong>Ambos cobros</strong> son cargados una vez en tu estado de cuenta siguiente
                                                a la fecha en que realizas el avance, independiente si postergas el pago de la cuota.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Aumento de cupo
                                        </a>
                                    </div>
                                    <div id="collapseFour" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Quieres un aumento de cupo?</h5>
                                            <p class="textjustify">Puedes acercarte a cualquier tienda abcdin y solicitar aumento de cupo de tu tarjeta abcvisa, el aumento dependerá de tu renta y comportamiento crediticio entre otras cosas. También puedes llamar a nuestra línea
                                                de Servicio al Cliente 600 830 22 22 para pedir mayor información.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Obtén una tarjeta adicional
                                        </a>
                                    </div>
                                    <div id="collapseFive" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Cómo puedo obtener una tarjeta adicional?</h5>
                                            <p class="textjustify">
                                                Acércate a cualquier tienda abcdin con el nombre, RUT y fecha de nacimiento de la persona a quién quieras otorgarle una tarjeta adicional. Luego, tu adicional deberá acercarse a cualquier tienda abcdin para obtener sus claves y activar su nueva Tarjeta
                                                abcvisa.
                                                <br> Ten presente que las compras realizadas con las tarjetas adicionales serán cargadas en la cuenta del titular y es él quien tiene la obligación de realizar tanto los pagos del crédito como cualquier
                                                otro costo que el adicional genere. Recuerda que puedes tener un máximo de tres tarjetas adicionales sin costo.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Bloqueo de tarjeta
                                        </a>
                                    </div>
                                    <div id="collapseSix" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Cómo puedo bloquear mi tarjeta?</h5>
                                            <p class="textjustify">
                                                En caso de pérdida o robo de la tarjeta, debes dar aviso lo más pronto posible. Puedes llamar al 800 911 140, o acercarte a cualquier tienda abcdin.
                                                <br> Una vez realizado el bloqueo, simplemente solicita su reposición en cualquier tienda abcdin donde te harán entrega de un nuevo plástico.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Otras preguntas
                                        </a>
                                    </div>
                                    <div id="collapseSeven" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <h5>¿Puedo utilizar mi Tarjeta abcvisa para compras en el extranjero?</h5>
                                            <p class="textjustify">
                                                La Tarjeta abcvisa está disponible para compras en Chile con moneda nacional a través del sistema Transbank o Internet a través de stios nacionales. Recuerda que muchos servicios web y aplicaciones móviles utilizan transacciones en dólares, por lo que
                                                no podrás realizar tu pago.
                                            </p>
                                            <br>
                                            <h5>No funciona mi Tarjeta abcvisa en algunos comercios asociados</h5>
                                            <p class="textjustify">
                                                La Tarjeta abcvisa no puede ser utilizada en comercios con reserva de cupo o en moneda internacional. Esto es muy común por ejemplo cuando quieras adquirir distintos servicios como hoteles o rent a car.
                                                <br> No olvides que siempre debes insertar tu tarjeta a través de sistema chip y no deslizarla mediante banda magnética, porque tu compra será rechazada. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pagos">
                        <div class="container mt-5 mb-5">
                            <div id="accordion2" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOneB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            ¿Dónde puedo pagar mi cuenta?
                                        </a>
                                    </div>
                                    <div id="collapseOneB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p>Desde la comodidad de tu hogar en <a href="https://www.abcserviciosfinancieros.cl/">www.abcvisa.cl</a> vía WebPay y presencialmente en cualquier tienda abcdin o Dijon, Sencillito, Cajavecina o Unired y también en tu app abcvisa a través de Khipu.
                                            </p>
                                            <p>***Las transferencias directas a la cuenta de abcvisa no son válidas ni consideradas como método de pago.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwoB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        ¿Cómo puedo conocer el estado de cuenta de mi tarjeta ABCVISA?
                                        </a>
                                    </div>
                                    <div id="collapseTwoB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p>
                                                Para conocer el Estado de Cuenta de tu Tarjeta abcvisa existen tres opciones: Entra a <a href="https://www.abcserviciosfinancieros.cl/">www.abcvisa.cl</a> y haz click en Mi cuenta, luego deberás agregar
                                                tu rut y tu Clave Internet. También puedes revisar tu estado de cuenta en nuestra App abcvisa, o puedes acercarte a cualquier tienda abcdin.
                                                <br> Más consultas en nuestra línea de Servicio al Cliente 600 830 22 22.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThreeB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        ¿Que medios de pago puedo utilizar para pagar mi cuenta?
                                        </a>
                                    </div>
                                    <div id="collapseThreeB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <ul>
                                                <li>En tiendas abcdin y Dijon puedes pagar en efectivo, con tarjetas de crédito y débito bancaria.</li>
                                                <li>A través de nuestro sitio web <a href="https://www.abcserviciosfinancieros.cl/">www.abcvisa.cl</a> puedes pagar con tarjetas de crédito y débito bancarias.</li>
                                                <li>En Sencillito, Cajavecina o Unired sólo puedes pagar en efectivo.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseForB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        ¿Puedo cambiar el día de facturación de mi pago?
                                        </a>
                                    </div>
                                    <div id="collapseForB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p>
                                                No es posible realizar cambio del día de Facturación, La fecha se define al momento de la apertura y no es modificable.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFiveB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        ¿Cuál es el monto mínimo que puedo pagar de mi estado de cuenta?
                                        </a>
                                    </div>
                                    <div id="collapseFiveB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p>
                                                Recuerda que la Tarjeta abcvisa es en CUOTAS FIJAS, es decir, al vencimiento de tu cuenta debes pagar la cuota del mes, de lo contrario caerás en mora. Recuerda pactar siempre tus cuotas como más te acomode.
                                                <br> Sin embargo puedes consultar por tu opción pago mínimo en cualquier tienda o llamando al <a href="tel:+6008302222">600 83022 22</a>, que te permite pagar una parte de la facturación del mes, y el saldo,
                                                los puedes pactar hasta en 12 cuotas.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseSixB">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            ¿Cómo puedo renegociar mi deuda?
                                        </a>
                                    </div>
                                    <div id="collapseSixB" class="card-body collapse" data-parent="#accordion2">
                                        <div class="card-body">
                                            <p>
                                                Puedes acercarte a cualquiera de nuestras tiendas para acordar el mejor plan de pago de acuerdo sus actuales condiciones. O si lo prefieres, nos puedes escribir al Whatsapp
                                                <a href="tel:+56942784912" style="text-decoration: none; color: #252525;">+56 9 4278 49 12</a> o llamarnos al
                                                <a href="tel:+6008302222" style="text-decoration: none; color: #252525;">600 830 22 22</a>, dónde lo asesoraremos sobre las opciones disponibles para regularizar tu cuenta.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="seguros">
                        <div class="container mt-5 mb-5">
                            <div id="accordion3" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOneC">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            Seguro de Vida
                                        </a>
                                    </div>
                                    <div id="collapseOneC" class="card-body collapse" data-parent="#accordion3">
                                        <div class="card-body">
                                            <ul>
                                                <li><strong>Vida Individual: </strong>El Seguro Vida Individual abcdin le entrega una amplia protección en caso de fallecimiento, e Invalidez Total y Permanente 2/3.</li>
                                                <li><strong>Renta Diaria: </strong>El Seguro de Renta Diaria por Hospitalización entrega al asegurado y a su familia una amplia protección en caso de Fallecimiento. Adicionalmente entrega protección en caso
                                                    de enfermedades y accidentes, entregando una renta diaria por hospitalización.</li>
                                                <li><strong>Renta Mensual: </strong> El Seguro Renta Mensual Individual abcdin le entrega una amplia protección en caso de muerte e invalidez accidental del titular del seguro.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwoC">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            Seguro de Salud
                                        </a>
                                    </div>
                                    <div id="collapseTwoC" class="card-body collapse" data-parent="#accordion3">
                                        <div class="card-body">
                                            <ul>
                                                <li><strong>Salud Personal: </strong>Amplia protección en caso de Fallecimiento y Enfermedades Graves.</li>
                                                <li><strong>Salud Familiar: </strong>Amplia protección en caso de Fallecimiento, Invalidez y Enfermedades Graves: Este Seguro también permite asegurar al cónyuge e hijos del asegurado titular en caso de enfermedades
                                                    graves e invalidez permanente 2/3.</li>
                                                <li><strong>Seguro Oncológico: </strong>Indemnización en caso de diagnóstico de Cáncer.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapseThreeC">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                            Protección Financiera                                        
                                        </a>
                                    </div>
                                    <div id="collapseThreeC" class="card-body collapse" data-parent="#accordion3">
                                        <div class="card-body">
                                            <ul>
                                                <li><strong>Antifraude: </strong>El Seguro de Antifraude abcdin, le entrega la más amplia protección a su tarjeta abcdin y a sus documentos en caso de extravío, robo o fraude durante la vigencia del Seguro.</li>
                                                <li><strong>Antirrobo: </strong>Seguro que entrega una indemnización en dinero en caso que el titular asegurado sufra el robo o asalto de sus objetos personales.</li>
                                                <li><strong>Desgravamen: </strong> En caso de Fallecimiento o Invalidez Total y Permanente 2/3 por accidente o enfermedad, el seguro cubre el saldo insoluto de la deuda.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapseForC">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Seguro protección hogar
                                        </a>
                                    </div>
                                    <div id="collapseForC" class="card-body collapse" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p>
                                                Es un seguro que sirve para propietarios y arrendatarios, ya que cubre la construcción y los contenidos hasta un monto único de capital asegurado.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapseFiveC">
                                        <a class="card-title mayuscula"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Seguro de desempleo e incapacidad temporal
                                        </a>
                                    </div>
                                    <div id="collapseFiveC" class="card-body collapse" data-parent="#accordion3">
                                        <div class="card-body">
                                            <p>
                                                Es un seguro que ayuda a pagar las cuotas de la tarjeta abcdin cuando surge un imprevisto de Cesantía o Incapacidad Temporal. Este seguro esta asociado al Seguro de Desgravamen.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="glosario">
                        <div class="container mt-5 mb-5">
                            <div id="accordion4" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOneD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                            Interés Mora
                                        </a>
                                    </div>
                                    <div id="collapseOneD" class="card-body collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p>
                                                El interés mora/interés penal es el monto que se carga diariamente a tu cuenta cuando hay un saldo pendiente de pago que se haya vencido (es decir, que no haya sido cancelado dentro del plazo establecido).
                                                <br> Este monto se calcula aplicando la tasa máxima convencional diaria vigente al saldo moroso que mantenga la cuenta.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseTwoD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Gastos de cobranza
                                        </a>
                                    </div>
                                    <div id="collapseTwoD" class="card-body collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p>
                                                Es el cargo correspondiente a los honorarios de cobranza extrajudicial que se aplican sobre el monto en mora una vez transcurridos 20 días del atraso. El monto se calcula aplicando la siguiente escala progresiva:
                                            </p>
                                            <br>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>% Honorarios de Cobranza</th>
                                                        <th>Monto Adecuado</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9%</td>
                                                        <td>Menos de 10 UF</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6%</td>
                                                        <td>10 UF a 50 UF</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3%</td>
                                                        <td>Sobre 50 UF</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseThreeD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Carga anual equivalente (CAE)
                                        </a>
                                    </div>
                                    <div id="collapseThreeD" class="collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="textjustify">
                                                La Carga Anual Equivalente o CAE, es un indicador que permite comparar distintas alternativas de financiamiento entre distintas empresas. Considera todos los gastos que se deben pagar en un periodo de un año. Mientras más bajo es el CAE, más barato el
                                                crédito de un determinado monto.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseForD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Cargo de administración fija mensual
                                        </a>
                                    </div>
                                    <div id="collapseForD" class="collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="textjustify">
                                                Es el cargo que tendrás que pagar al tener la tarjeta abcvisa en forma mensual. Sólo se cobra si tienes deuda en el momento de la facturación de tu cuenta. Es decir, si tienes la tarjeta y no la usas, no tienes que pagar nada. </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseFiveD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Tasa de Interés
                                        </a>
                                    </div>
                                    <div id="collapseFiveD" class="collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="textjustify">
                                                Es un porcentaje de un préstamo o crédito que se traduce en un monto de dinero, mediante el cual se paga por el uso de ese dinero.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseSixD">
                                        <a class="card-title"><i class="fas fa-1x fa-plus mr-3"></i>
                                        Tasa máxima convencional (TMC)
                                        </a>
                                    </div>
                                    <div id="collapseSixD" class="collapse" data-parent="#accordion4">
                                        <div class="card-body">
                                            <p class="textjustify">
                                                La tasa máxima convencional es la tasa de interés máxima que fija la superintendencia de bancos y que cada vez que una institución financiera te presta dinero, la tasa de interés cobrada puede exceder este tope.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->