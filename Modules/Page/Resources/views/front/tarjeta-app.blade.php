<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/assets/media/bannerpagina-app.jpg" alt="First slide">
        </div>
        <!-- <div class="carousel-item">
        <img class="d-block w-100" src="/assets/media/slide-tarjeta-app.jpg" alt="Second slide">
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->

<!-- barra superior -->
<section class="barraSlide">
    <div class="container">
        <div class="row flex-parent">
            <div class="col-sm-6">
                <div class="box">
                    <img src="/assets/media/icono-app_2.png" width="49" height="82">
                </div>

                <div class="box2">
                    <h1>Conoce nuestra APP!</h1>
                    <h2>Todo en la palma de tu mano</h2>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="box">
                    <a class="btn-app" href="https://itunes.apple.com/cl/app/tarjeta-abcvisa/id1081522360" target="_blank">
                        <img src="/assets/media/icono-btn-mac.jpg" width="32" height="44">
                    </a>
                    <a class="btn-app" href="https://play.google.com/store/apps/details?id=cl.abcdin.tarjetaabc&hl=es_CL" target="_blank">
                        <img src="/assets/media/icono-btn-android.jpg" width="32" height="44">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->

<!-- barra beneficios -->
<section class="barraBeneficios">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><strong>CON TU APP ABCVISA PODRÁS:</strong></h1>
            </div>
        </div>
    </div>
</section>
<!-- fin barra beneficios -->
<!-- contenidos -->
<!-- selector de beneficios -->
<section class="bannersTabs contenidoApp selectorTarjetaApp">
    <div class="container-fluid">
        <div class="row">
            <div class="board">
                <div class="board-inner">
                    <ul class="nav nav-tabs justify-content-around align-content-center" id="myTab">
                        <li>
                            <a href="#bloquear" data-toggle="tab" class="active show">
                                <img class="bloquear" src="/assets/media/bloqueo-off.png">
                                <span class="round-tabs two">Bloqueo de tarjeta</span>
                            </a>
                        </li>
                        <li>
                            <a href="#huella" data-toggle="tab">
                                <img class="huella" src="/assets/media/huella-off.png">
                                <span class="round-tabs one">Ingreso por Huella</span>
                            </a>
                        </li>

                        <li>
                            <a href="#cupos" data-toggle="tab">
                                <img class="cupo" src="/assets/media/icono-app-cupo.png">
                                <span class="round-tabs three">Cupos</span>
                            </a>
                        </li>

                        <li>
                            <a href="#estado" data-toggle="tab">
                                <img class="estado" src="/assets/media/icono-app-estado.png">
                                <span class="round-tabs four">Estado de cuenta</span>
                            </a>
                        </li>

                        <li>
                            <a href="#movimientos" data-toggle="tab">
                                <img class="movimientos" src="/assets/media/icono-app-movimientos.png">
                                <span class="round-tabs five">Movimientos</span>
                            </a>
                        </li>

                        <li>
                            <a href="#notificaciones" data-toggle="tab">
                                <img class="notificaciones" src="/assets/media/icono-app-notificaciones.png">
                                <span class="round-tabs five">Notificaciones de Compra</span>
                            </a>
                        </li>

                        <li>
                            <a href="#ofertas" data-toggle="tab">
                                <img class="ofertas" src="/assets/media/icono-app-ofertas.png">
                                <span class="round-tabs five">Ofertas</span>
                            </a>
                        </li>

                        <li>
                            <a href="#pagar" data-toggle="tab">
                                <img class="pagar" src="/assets/media/icono-app-pagar.png">
                                <span class="round-tabs five">Paga tu Cuenta</span>
                            </a>
                        </li>

                        <li>
                            <a href="#tiendas" data-toggle="tab">
                                <img class="tiendas" src="/assets/media/icono-app-tiendas.png">
                                <span class="round-tabs five">Tiendas</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="bloquear">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-bloqueo.png">
                                        </div>
                                        <div class="px-0 h-50">
                                            <h1>¿TIENES UNA EMERGENCIA?</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Bloqueo de tarjeta</div>
                                            <p>Asegúrate ante cualquier eventualidad y bloquea tu tarjeta abcvisa y sus adicionales de manera instantánea desde tu aplicación.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="huella">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-huella.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <h1>¡NO PIERDAS TIEMPO!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Ingreso por Huella</div>
                                            <p>Ahora puedes ingresar a tu app abcvisa de manera rápida y segura utilizando tu huella digital.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="cupos">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-cupo2.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!-- <img src="/assets/media/cupo.png"></img> -->
                                            <h1>¡PARA COMPRARTE LO QUE TE HAGA FELIZ!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Consultar Cupos Disponibles</div>
                                            <p>Revisa en línea tu cupo disponible para compras en nuestras tiendas, otros comercios y avances en efectivo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="estado">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-eecc.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!-- <img src="/assets/media/eecc.png"></img> -->
                                            <h1>¡NO PIERDAS LA CUENTA!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Consulta tu Estado de Cuenta</div>
                                            <p>Puedes revisar y descargar en formato <b>PDF</b> tus últimas 6 estados de cuenta.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="movimientos">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-nofacturados2.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!--<img src="/assets/media/nofacturados.png"></img>-->
                                            <h1>¡YA NO OLVIDARÁS PAGAR NADA!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Movimientos no Facturados</div>
                                            <p>Revisa las compras que has realizado y que aún no han sido facturadas.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="notificaciones">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-notificaciones.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!--<img src="/assets/media/notificaciones.png"></img>-->
                                            <h1>¡ABCVISA TE AVISA!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Notificaciones de Compra</div>
                                            <p>Recibe en línea información sobre tus compras realizadas en nuestras tiendas abcdin, Dijon y otros comercios.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="ofertas">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-ofertas.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!-- <img src="/assets/media/ofertas.png"></img> -->
                                            <h1>¡DISFRUTA DEL PANORAMA PERFECTO!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Ofertas Exclusivas</div>
                                            <p>Informate sobre todas las ofertas vigentes, para que puedas disfrutar de nuestros descuentos y promociones.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="pagar">
                        <div class="row justify-content-center">
                            <div class="col-lg-9 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-pago.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!-- <img src="/assets/media/pago.png"></img> -->
                                            <h1>¡AL DÍA CON TU ABCVISA!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Pago de Cuentas abcvisa en tu APP</div>
                                            <p>Ahora con tu app abcvisa puedes pagar tu monto facturado, tu deuda total, o el monto que desees. Así de fácil.</p>
                                            <p>***Las transferencias directas a la cuenta de abcvisa no son válidas ni consideradas como método de pago.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tiendas">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 px-0">
                                <div class="container-fluid">
                                    <div class="row info-app">
                                        <div class="px-0 h-50">
                                            <img src="/assets/media/celu-tiendas.png">
                                        </div>

                                        <div class="px-0 h-50">
                                            <!-- <img src="/assets/media/tiendas.png"></img> -->
                                            <h1>¡ESTAMOS MÁS CERCA DE TÍ!</h1>
                                            <div class="btn_solicitalo btn btn-primary btn-lg btnRojo btnapp">Tiendas abcdin y Dijon</div>
                                            <p>Encuentra tu tienda abcdin y dijon más cercana, a lo largo de todo Chile.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->