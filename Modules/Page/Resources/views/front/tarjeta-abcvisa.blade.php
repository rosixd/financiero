<!-- slider central -->
<div class="carousel slide" data-ride="carousel" id="carouselIndicators">
    <!-- <ol class="carousel-indicators">
        <li class="active" data-slide-to="0" data-target="#carouselIndicators">&nbsp;</li>
        <li data-slide-to="1" data-target="#carouselIndicators">&nbsp;</li>
    </ol> -->
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img alt="First slide" class="d-block w-100" src="/assets/media/slider-tarjeta-100.jpg" />
        </div>
        <!-- <div class="carousel-item">
            <img alt="Second slide" class="d-block w-100" src="/assets/media/slider-tarjeta-100.jpg" />
        </div> -->
    </div>
    <!-- <a class="carousel-control-prev" data-slide="prev" href="#carouselIndicators" role="button">
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" data-slide="next" href="#carouselIndicators" role="button">
        <span class="sr-only">Next</span>
    </a> -->
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide">
    <div class="container">
        <div class="row flex-parent">
            <div class="col-sm-6">
                <div class="box">
                    <a data-toggle="modal" id="solicitarTarjetaMenu" href="#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">
                        <img height="55" src="/assets/media/icono-tarjeta-visa.png" style="text-decoration: none;" width="85" />
                    </a>
                </div>
                <div class="box2 tarjetaabc">
                    <h1>
                        <a data-toggle="modal" href="#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">Quiero mi tarjeta!
                        </a>
                    </h1>
                    <h2>
                        <a data-toggle="modal" href="#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">Cientos de beneficios
                        </a>
                    </h2>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box">
                    <a href="/tarjeta-app" style="text-decoration: none; position: relative; left: 30%;">
                        <img height="82" src="/assets/media/icono-app_2.png" width="49" />
                    </a>
                </div>
                <div class="box2 tarjetaabc">
                    <h1>
                        <a href="/tarjeta-app" style="text-decoration: none;">Conoce nuestra APP!
                        </a>
                    </h1>

                    <h2>
                        <a href="/tarjeta-app" style="text-decoration: none;">Todo en la palma de tu mano
                        </a>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- fin barra superior -->
<!-- barra beneficios -->
<section class="barraTitulos">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>
                    <span>Descubre</span> que puedes hacer<br />con tu tarjeta
                    <img src="/assets/media/tit-abcvisa.png" />
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- fin barra beneficios -->
<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoTarjeta">
        <div class="row">
            <div class="col-lg-4">
                <a class="cajaBeneficio" href="/beneficios" style="text-decoration: none;">
                    <img src="/assets/media/icono-beneficios.png" />
                    <h2>Beneficios Exclusivos</h2>
                    <p>Puedes llegar a ahorrar hasta $35.000 mensual!<br /><strong>Descubre c&oacute;mo</strong></p>
                </a>
            </div>
            <div class="col-lg-4">
                <a class="cajaBeneficio" href="https://www.abcdin.cl/" target="_blank" style="text-decoration: none;">
                    <img src="/assets/media/icono-compra.png" />
                    <h2>Compra online</h2>
                    <p>Con tu segunda Clave compra seguro en sitios nacionales, excluye Spotify, Netflix, Uber.</p>
                </a>
            </div>
            <div class="col-lg-4">
                <a class="cajaBeneficio" style="text-decoration: none;">
                    <img src="/assets/media/icono-comercios.png" />
                    <h2>Comercios asociados</h2>
                    <p style="text-decoration: none;">Usa tu abcvisa en toda la red transbank del pa&iacute;s.</p>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <a class="cajaBeneficio" href="/tarjeta-app" style="text-decoration: none;">
                    <img src="/assets/media/icono-app_1.png" />
                    <h2>Abcvisa APP</h2>
                    <p>Desc&aacute;rgala, paga tu cuenta, revisa tu cupo disponible, revisa tu estado de cuenta, recibe notificaciones y mucho m&aacute;s!</p>
                </a>
            </div>
            <div class="col-lg-4">
                <a class="cajaBeneficio" href="/paga-tu-cuenta" target="_blank" style="text-decoration: none;">
                    <img src="/assets/media/icono-pago.png" />
                    <h2>Opciones de pago</h2>
                    <p>Necesitas otras alternativas para pagar la deuda de este mes?</p>
                </a>
            </div>
        </div>
    </div>
</section>
<div class="spacer1"></div>
<section class="Banners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p0">
                <a class="bannerAnchoCompleto bgRosado" href="#" style="text-decoration: none;">
                    <img class="img-fluid" src="/assets/media/banner-koke-1600x357.jpg" />
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 p0">
                <a class="bannerAnchoCompleto bgGris" href="/beneficios" style="text-decoration: none;">
                    <img class="img-fluid" src="/assets/media/banner-beneficios-1600x357.jpg" />
                </a>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->