<!-- slider central -->
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselIndicators" data-slide-to="1"></li>
        <li data-target="#carouselIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <a href="/tarjeta-abcvisa#ModalFormularioQuieroMiTarjeta">
                <img class="d-block w-100" src="/assets/media/slider-home-100.jpg"  alt="First slide">
            </a>
        </div>
        <div class="carousel-item">
            <a href="/beneficios">
                <img class="d-block w-100" src="/assets/media/slider-home2-100.jpg" alt="Second slide">
            </a>
        </div>
        <div class="carousel-item">
            <a href="/beneficios">
                <img class="d-block w-100" src="/assets/media/slider-home3-100.jpg" alt="Second slide">
            </a>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- fin slider central -->
<!-- barra superior -->
<section class="barraSlide">
    <div class="container">
        <div class="row flex-parent">
            <div class="col-sm-6">
                <a href="/tarjeta-abcvisa#ModalFormularioQuieroMiTarjeta" style="text-decoration: none;">
                    <div class="box">
                        <img src="/assets/media/icono-tarjeta-visa.png" width="85" height="55">
                    </div>  
                    <div class="box2">
                        <h1>Quiero mi tarjeta!</h1>
                        <h2>Cientos de beneficios</h2>
                    </div>
                </a>
            </div>
            <div class="col-sm-6">
                <a href="/tarjeta-app" style="text-decoration: none;">
                    <div class="box">
                        <img src="/assets/media/icono-app_2.png" width="49" height="82">
                    </div>
                    
                    <div class="box2">
                        <h1>Conoce nuestra APP!</h1>
                        <h2>Todo en la palma de tu mano</h2>
                    </div>
                </a>
            </div>

        </div>
    </div>
</section>
<!-- fin barra superior -->
<!-- barra beneficios -->
<section class="barraBeneficios">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="/beneficios" style="text-decoration: none; color: #ffffff;">
                    <h1>CONOCE TODOS LOS BENEFICIOS</h1>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- fin barra beneficios -->
<!-- contenidos -->
<section class="contenidos" id="contenidos">
    <div class="container-fluid contenidoHome">
        <div class="row filaBanner">
            <div class="col-lg-4">
                <a href="#ModalUnoSalud" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/uno-salud-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#ModalUnimarc" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/unimarc-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#ModalTicketpro" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/ticketpro-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
        </div>

        <div class="row filaBanner">
            <div class="col-lg-4">
                <a href="#ModalSalcobrand" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/salcobrand-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#ModalBencineras" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/bencineras-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#ModalHoyts" class="bannerHome" data-toggle="modal">
                    <img src="/assets/media/hoyts-minibox.jpg" class="img-thumbnail img-fluid">
                </a>
            </div>
        </div>
    </div>
</section>
<!-- fin contenidos -->
<div id="ModalHoyts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/hoyts-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalTicketpro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/ticketpro-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalUnoSalud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/uno-salud-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalUnimarc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/unimarc-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalSalcobrand" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/salcobrand-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ModalBencineras" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="/assets/media/bencineras-lightbox.jpg" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>