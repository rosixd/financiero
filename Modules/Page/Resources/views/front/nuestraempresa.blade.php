<link rel="stylesheet" href="/themes/abcdin/css/style.css" />
<link rel="stylesheet" href="/themes/abcdin/css/responsive.css" />
<script src="/themes/abcdin/js/function.js' ) }}"></script>
<section style="background-color: #999999; ">
    <div class="container clearfix" style="">
        <div id="zb_tree" style="background: none;">
            <div class="col_full center">
                <ul id="myTab2" class="nav nav-pills boot-tabs" style="display: inline-block !important;">
                    <li class="active toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/nuestra-empresa"  style="border-radius: 0px; width: auto!important;" class="button2">
                            <div class="">Nuestra Empresa</div>
                        </a>
                    </li>
                    <li class=" toptabsli toptabsliwidth" id="ropaid1" style="display: inline-block !important;">
                        <a href="/accionista"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Accionistas</div>
                        </a>
                    </li>
                    <li class=" toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/directores"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Directorio</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/infofinanciera"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Información Financiera</div>
                        </a>
                    </li>
                    <li class="toptabsli toptabsliwidth" style="display: inline-block !important;">
                        <a href="/organigrama"  style="border-radius: 0px; width: auto!important; " class="button2">
                            <div class="">Organigrama</div>
                        </a>
                    </li>
                    <!-- <li class="toptabsli"><a href="#ofertadi" data-toggle="tab" style="padding: 0px;border-radius: 8px; width: auto!important;" class="button2"><div class="iconos_izq3"><img alt="Seguro de Vida" src="img/Menu/blanco_saludPage_1.png"></div><div class="texto_mid3">SEGURO <span style="color:#de004a;">AUTOMOTRIZ</span></div></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<div id="myTabContent2" class="tab-content">
    <!-- #################### -->
    <!-- CONTENIDO DE NUESTRA EMPRESA -->
    <div class="tab-pane fade in active" id="nuestra1" style="padding-top: 0px !important;">
        <div class="" style="padding-top: 30px; background-color: #e4e4e4; padding-bottom: 40px;" >
            <div class="col_full center" style="margin-bottom: 0px !important;">
                <ul id="myTab3" class="nav nav-pills boot-tabs tab-box1" style="display: inline-block !important; text-align: center; margin-bottom: 0px!important;">
                    <!-- <li class="active"><a href="#segurovidaindividual" data-toggle="tab" style="border: none!important; width: auto!important; background: none!important;"><div class="comidabot"></div></a></li> -->
                    <li class="toptabsli" style="margin-right: 0px; margin-left: 0px; display: inline-block !important;">
                        <a id="historia1" href="#historia" data-toggle="tab" class="button3" style="width: auto!important; border-radius: 20px;">
                            <div class="iconos_izq3 ic1"></div>
                            <div class="texto_mid3">Historia</div>
                        </a>
                    </li>
                    <li class="toptabsli" style="display: inline-block !important;">
                        <a id="tiendas1" href="#tiendas" data-toggle="tab" class="button3" style="width: auto!important; border-radius: 20px;">
                            <div class="iconos_izq3 ic2"></div>
                            <div class="texto_mid3">Tiendas</div>
                        </a>
                    </li>
                    <li class="toptabsli" style="display: inline-block !important;">
                        <a id="vision1" href="#vision" data-toggle="tab" class="button3" style="width: auto!important; border-radius: 20px;">
                            <div class="iconos_izq3 ic3"></div>
                            <div class="texto_mid3">Visión y Valores</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="myTabContent3" class="tab-content">
            <!-- historia -->
            <div class="tab-pane fade in active" id="historia">
                <section style="background-color: #ffffff; padding-top: 20px; padding-bottom: 20px;">
                    <div class="container clearfix">
                        <div style="width: 70%;margin: 0 auto; display:block;">
                            <div class="col_half center" style="border-right: 1px solid #ccc;">
                                <img src="/assets/media/logo-abcdin.png" alt="abcdin" style="">
                            </div>
                            <div class="col_half center col_last">
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">En
                                <b>ABCDIN</b> somos una empresa nacional especialistas en tecnología, electro, deco hogar y ofrecemos conveniencia y accesibilidad en todo Chile.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
                    <div class="container clearfix">
                        <div class="col_full left" >
                            <div style="margin: 0 auto; display:block; ">
                                <h3 style="color: #333333;">
                                    <img src="/assets/media/icon-historia-activo.png" alt="abcdin" style="">Historia
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="container clearfix" style="background-color: #fff; padding: 20px 40px;  border-radius: 20px;" >
                        <div class="col_full left" >
                            <div class="" style="background-color: #fff;">
                                <div class="col_half center" style="background-color: #fff;">
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Abcdin, como especialista, sabe lo que las personas y sus 
                                    familias necesitan para mejorar sus vidas cotidianas y así 
                                    progresar; es por eso que en abcdin identifica lo que el Cliente 
                                    necesita y lo pone a su alcance, haciendo que su “felicidad le 
                                    cueste menos”.
                                </p>
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Abcdin dedica sus esfuerzos en ganar la confianza de cada uno 
                                    de sus clientes, brindándoles productos de calidad en un lugar 
                                    cómodo, con una atención ágil y con un personal cálido y 
                                    profesional, que siempre está a disposición de sus necesidades 
                                    en cada una de sus tiendas.
                                </p>
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Abcdin con un total de 156 tiendas a lo largo de todo Chile, 
                                    representa la cadena de venta de artículos para el hogar de 
                                    mayor cobertura en el país, llegando a 85,640 mts2 de 
                                    superficie.
                                </p>
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Distinguiéndose tiendas tradicionales con productos de 
                                    electrónica, línea blanca y muebles así como también mobile
                                </p>
                                </div>
                                <div class="col_half center col_last" style="background-color: #fff;">
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">con formatos de salas más pequeños y con mix acotado a 
                                    portables (telefonía y computo).
                                </p>
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Nuestra historia se remonta al año 1914 con Casa Gómez en 
                                    Antofagasta, una tienda de sombreros e hilados que se 
                                    convirtió en distribuidora de productos para el hogar, hasta que 
                                    en los años 70´se transformó en Distribuidora de Industrias 
                                    Nacionales S. A. Con posterioridad en el año 2005 se adquirió 
                                    ABC, para que en 2008 se fusionaran las marcas y se 
                                    convirtiera en abcdin.
                                </p>
                                <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Por el lado de vestuario, éste rubro se incorporó a la empresa en 
                                    el 2013 tras la adquisición de Dijon. A nivel de sociedad, 
                                    estamos controlados por el grupo Yaconi-Santa Cruz y, la 
                                    administración está encargada a un equipo con amplia 
                                    trayectoria en el retail nacional.
                                </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section style="background-color: #e4e4e4; padding-top: 20px; padding-bottom: 20px;">
                    <div class="container clearfix" style="padding: 20px 40px;  border-radius: 20px;">
                        <div class="col_full left" >
                            <div style="width: 100%;margin: 0 auto; display:block; ">
                                <h3 style="color: #333333;"><img src="/assets/media/icon-hitos.png" alt="abcdin" style="">Hitos</h3>
                            </div>
                        </div>
                    </div>
                    <div class="container clearfix" style=" width: 70%; padding: 20px 40px;  border-radius: 20px; padding-left: 45px; " >
                        <!-- Hito1 -->
                        <div class="col_full center" >
                            <div class="col_half center" style="background-color: #fff; margin-right: 0%; ">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos1.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>1942</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;"><b>Antonio Gómez</b> fundó la <b>Casa Gómez</b> en Antofagasta, 
                                        una comercializadora de sombreros e hilados.
                                    </p>
                                </div>
                                </div>
                            </div>
                            <div class="col_half col_last desktop_view" >
                                <br>
                            </div>
                        </div>
                        <!-- Hito2 -->
                        <div class="col_full center" >
                            <div class="col_half center desktop_view" style="margin-right: 0%;">
                                <br>				
                            </div>
                            <div class="col_half col_last" style="background-color: #fff;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos2.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>1978</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Cambió su nombre y comenzó a operar como <b>Distribuidora de Industrias Nacionales</b> (DIN Ltda). 
                                        Contaba con 30 sucursales en el país.
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Hito3 -->
                        <div class="col_full center" >
                            <div class="col_half center" style="background-color: #fff; margin-right: 0%;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos3.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>1995</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;"><b>Yaconi - Santa Cruz</b> adquirió la propiedad y 
                                        administración de <b>DIN</b>. Con <b>54</b> sucursales se consolidó 
                                        como la mayor cadena de electrónica, línea blanca y 
                                        muebles del país.
                                    </p>
                                </div>
                                </div>
                            </div>
                            <div class="col_half col_last desktop_view" >
                                <br>
                            </div>
                        </div>
                        <!-- Hito4 -->
                        <div class="col_full center" >
                            <div class="col_half center desktop_view" style="margin-right: 0%;">
                                <br>				
                            </div>
                            <div class="col_half col_last" style="background-color: #fff;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos4.jpg" alt="abcdin" style="">
                                </div>
                                <div  class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2005</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;"><b>DIN</b> adquirió la cadena <b>ABC</b>. Ambas empresas cuentan 
                                        con 176 tiendas en el país y mantienen separadas las 
                                        marcas, cada una con imágenes definidas.
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Hito5 -->
                        <div class="col_full center" >
                            <div class="col_half center" style="background-color: #fff; margin-right: 0%;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos5.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2008</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Se unieron las marcas y formaron <b>abcdin</b>. Con <b>49 
                                        tiendas</b> en el país entregando soluciones en las líneas de 
                                        Electro Decohogar.
                                    </p>
                                </div>
                                </div>
                            </div>
                            <div class="col_half col_last desktop_view" >
                                <br>
                            </div>
                        </div>
                        <!-- Hito6 -->
                        <div class="col_full center" >
                            <div class="col_half center desktop_view" style="margin-right: 0%;">
                                <br>				
                            </div>
                            <div class="col_half col_last" style="background-color: #fff;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos6.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2011</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Continúa el plan de remodelación y apertura de tiendas (<b>90 tiendas</b> con <b>63.994 m2</b> de superficie de venta). Se 
                                        coloca el primer bono corporativo por <b>UF 2 millones</b> a un plazo de 10 años.
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Hito7 -->
                        <div class="col_full center" >
                            <div class="col_half center" style="background-color: #fff; margin-right: 0%;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos7.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2013</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">De acuerdo con el Plan Estratégico se adquiere la 
                                        cadena <b>Dijon</b>. Ambas tiendas mantendrán su 
                                        posicionamiento en el rubro: <b>abcdin</b> como especialista en 
                                        Electro y Decohogar, y Dijon en vestuario.
                                    </p>
                                </div>
                                </div>
                            </div>
                            <div class="col_half col_last desktop_view" >
                                <br>
                            </div>
                        </div>
                        <!-- Hito8 -->
                        <div class="col_full center" >
                            <div class="col_half center desktop_view" style="margin-right: 0%;">
                                <br>				
                            </div>
                            <div class="col_half col_last" style="background-color: #fff;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos8.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2015</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Se implementa el <b>Proyecto SAM</b>, un nuevo sistema de 
                                        administración de mercadería. <b>Dijon</b> re-inagura <b>5 nuevas 
                                        tiendas</b> bajo un nuevo formato y lanza una nueva 
                                        propuesta comercial y de marketing. Refuerza su 
                                        presencia en la web para mujeres.
                                    </p>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Hito9 -->
                        <div class="col_full center" >
                            <div class="col_half center" style="background-color: #fff; margin-right: 0%;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos9.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2016</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Lanzamiento <b>tarjeta abcvisa</b>.</p>
                                </div>
                                </div>
                            </div>
                            <div class="col_half col_last desktop_view" >
                                <br>
                            </div>
                        </div>
                        <!-- Hito10 -->
                        <div class="col_full center" >
                            <div class="col_half center desktop_view" style="margin-right: 0%;">
                                <br>				
                            </div>
                            <div class="col_half col_last" style="background-color: #fff;">
                                <div style="display: table">
                                <div style="width: 32%; display: table-cell; vertical-align: middle;">
                                    <img src="/assets/media/img-hitos10.jpg" alt="abcdin" style="">
                                </div>
                                <div class="historiadivs" style="width: 66%; display: table-cell;">
                                    <h3 style="color: #b2121d; text-align: justify; margin-bottom: 5px;"><b>2017</b></h3>
                                    <p style="color: #7f7f7f; text-align: justify; margin-bottom: 5px;">Inauguración del nuevo <b>Centro de Distribución</b>.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- FIN historia -->
            <!-- Tiendas -->
            <div class="tab-pane fade in" id="tiendas" style="background-color:#e4e4e4;">
                <!-- ### -->
                <div class="container clearfix" style=" padding: 30px; padding-top: 0px; background-color: #e4e4e4;">
                    <section style="background-color: #e4e4e4; padding-bottom: 20px;">
                        <div class="container clearfix">
                            <div class="col_full left" >
                                <div style="width: 70%;margin: 0 auto; display:block; ">
                                </div>
                            </div>
                        </div>
                        <div class="container clearfix" style="background-color: #fff; width: 90%; border-radius: 20px; padding: 20px 10px;" >
                            <div class="col_full left" >
                                <div class="" style="background-color: #fff;">
                                    <div class="col_half center desktop_view" style="background-color: #fff;">
                                        <img id="my_image" alt="abcvisa" src="/assets/media/img-mapanorte.jpg">
                                    </div>
                                    <div class="col_half center col_last" style="background-color: #fff;">
                                        <img src="/assets/media/logo-abcdin.png" alt="abcdin" style="max-width: 170px; padding-top: 20px; padding-bottom: 20px;">
                                        <div class="" style="padding-top: 0px; background-color: #ffffff; padding-bottom: 40px;" >
                                            <div class="col_full center" style="margin-bottom: 0px !important;">
                                                <ul id="myTab5" class="nav nav-pills boot-tabs tab-box1" style="text-align: center; margin-bottom: 0px!important;">
                                                    <!-- <li class="active"><a href="#segurovidaindividual" data-toggle="tab" style="border: none!important; width: auto!important; background: none!important;"><div class="comidabot"></div></a></li> -->
                                                    <li class="active toptabsli" style="margin-right: 0px; margin-left: 0px;">
                                                        <a id="norte1" href="#norte" data-toggle="tab" class="button6" style="width: auto!important; border-radius: 8px;">
                                                            <div >ZONA <b>NORTE</b></div>
                                                        </a>
                                                    </li>
                                                    <li class="toptabsli">
                                                        <a id="centro1" href="#centro" data-toggle="tab" class="button6" style="width: auto!important; border-radius: 8px;">
                                                            <div >ZONA <b>CENTRO</b></div>
                                                        </a>
                                                    </li>
                                                    <li class="toptabsli">
                                                        <a id="sur1" href="#sur" data-toggle="tab" class="button6" style="width: auto!important; border-radius: 8px;">
                                                            <div >ZONA <b>SUR</b></div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="myTabContent5" class="tab-content tiendasmobile" style="">
                                            <!-- #### -->
                                            <!-- norte -->
                                            <div class="tab-pane fade in active" id="norte">
                                                <p style="text-align: left;">
                                                ARICA / 21 De Mayo 455<br>
                                                ALTO HOSPICIO / Av. Ramón Pérez Opazo 3165<br>
                                                IQUIQUE / Tarapacá 601 al 605<br>
                                                ANTOFAGASTA / Manuel Antonio Matta 2551<br>
                                                CALAMA / Eleuterio Ramírez 1942<br>
                                                TOCOPILLA / 21 De Mayo 1681-1683<br>
                                                COPIAPO / Atacama 578<br>
                                                VALLENAR / Prat 1175<br>
                                                COQUIMBO / Cerrada<br>
                                                OVALLE / Vicuña Mackenna 41<br>
                                                LA SERENA / Cordovez 625<br>
                                                MOVIL CALAMA / Av. Balmaceda 3242 Local B-264 PC, Calama<br>
                                                SAN ANTONIO / Centenario 123<br>
                                                VINA DEL MAR / Valparaíso 444<br>
                                                VILLA ALEMANA / Cerrada<br>
                                                SAN FELIPE / Arturo Prat 221<br>
                                                QUILPUE MALL PLAZA DEL SOL / Diego Portales 822 Local 108 al 111<br>
                                                ILLAPEL / Constitución 648<br>
                                                PORTAL EL BELLOTO / Ramón Freire 2414 Local 1091<br>
                                                MOVIL VALPARAISO ROSS / Victoria 2921 al 2929 local 300<br>
                                                MOVIL VIÑA DEL MAR / Avenida Benidorm 961, Local 122. Mall Espacio Urbano<br>
                                                LA LIGUA / Ortiz de Rozas 390<br>
                                                VALPARAISO / Avda. Pedro Montt 2010<br>
                                                QUILLOTA / Merced 25<br>
                                                LA CALERA / Carrera 703<br>
                                                LOS ANDES / Maipú 204 - 214
                                                </p>
                                            </div>
                                            <!-- FIN norte -->
                                            <!-- #### -->
                                            <!-- centro -->
                                            <div class="tab-pane fade in" id="centro">
                                                <p style="text-align: left;">
                                                ARAUCO MAIPU / Vespucio 399 Local 179<br>
                                                AHUMADA / Ahumada 65-67 Local 103<br>
                                                ESTADO / Estado 73<br>
                                                MELIPILLA / Serrano 469<br>
                                                TALAGANTE / Bernardo O'Higgins 957<br>
                                                LA CISTERNA / Gran Avenida 6383<br>
                                                PEÑAFLOR / 21 De Mayo 4573<br>
                                                COLINA / Gral. San Martin 068 L-A-2<br>
                                                BUIN / Balmaceda 64<br>
                                                TOBALABA/GRECIA / Av. Tobalaba 11201 Loc. 1130-1140<br>
                                                EL BOSQUE / Gran Avenida 10375<br>
                                                PORTAL ÑUÑOA / José Pedro Alessandri 1166 Local 2063<br>
                                                SAN BERNARDO / Eyzaguirre 530 al 536<br>
                                                QUILICURA MOBILE / Mall Arauco Quilicura Local 09<br>
                                                MOVIL QUILIN / Mar Tirreno 3349, Local 2048<br>
                                                MOVIL PLAZA ALAMEDA / Av. Libertador Bdo. O´higgins 3470, Local A207<br>
                                                MOVIL ARAUCO ESTACIÓN / San Borja 84 locales 10326 y 10332<br>
                                                MOVIL VIVO LA FLORIDA / Cerrada<br>
                                                ALAMEDA / Av. Libertador Bernardo O'Higgins 2812<br>
                                                PUENTE / Puente 593<br>
                                                MAIPU / Avenida Pajaritos 2018<br>
                                                PUENTE ALTO / Concha y Toro 400 Esq. Tocornal<br>
                                                LOS ANGELES / Colon 454<br>
                                                RANCAGUA / Independencia 763<br>
                                                SAN CARLOS / Serrano 563<br>
                                                CURANILAHUE / Arturo Prat 1357<br>
                                                ARAUCO / Esmeralda 342<br>
                                                LEBU / Alcázar 145<br>
                                                CAÑETE / Calle Saavedra 440<br>
                                                CORONEL / Carlos Prats 0901 local 168 (Mall Costa Pacífico de Coronel)<br>
                                                SAN VICENTE DE TT / Exequiel González 172 y 198<br>
                                                TALCAHUANO / Cristobal Colón 426<br>
                                                CONSTITUCION / Vial 435<br>
                                                SANTA CRUZ / Rafael Casanova 372<br>
                                                TOME / Sotomayor 1111<br>
                                                CONCEPCION EXPO / Cerrada<br>
                                                MOBILE RANCAGUA / Nueva Alberto Einstein 200 local 142<br>
                                                CURICO / Prat 452<br>
                                                SAN FERNANDO / Manuel Rodríguez 923<br>
                                                TALCA III / Uno Sur 1424<br>
                                                RENGO / Arturo Prat 195<br>
                                                SAN JAVIER / Arturo Prat 2450<br>
                                                LINARES / Independencia 570<br>
                                                CAUQUENES / Victoria 509<br>
                                                PARRAL / Aníbal Pinto 575 Local A<br>
                                                CHILLAN / El Roble 713<br>
                                                CONCEPCION / Barros Arana 817
                                                </p>
                                            </div>
                                            <!-- FIN centro -->
                                            <!-- #### -->
                                            <!-- sur -->
                                            <div class="tab-pane fade in" id="sur">
                                                <p style="text-align: left;">
                                                ANGOL / Lautaro 175<br>
                                                VILLARRICA / Camilo Henríquez 522<br>
                                                TEMUCO / Manuel Montt 764<br>
                                                ANCUD / Pudeto 231-235<br>
                                                VALDIVIA / Picarte 377-385<br>
                                                COYHAIQUE / Calle Prat 380<br>
                                                PUNTA ARENAS / Bories 870<br>
                                                CASTRO / San Martin 475<br>
                                                VICTORIA / Pisagua 1274<br>
                                                PUERTO NATALES / Manuel Bulnes 624<br>
                                                NUEVA IMPERIAL II / Arturo Prat 201<br>
                                                PANGUIPULLI / Av. Martinez de Rosas 430 y 440, Panguipulli<br>
                                                TRAIGUEN / Cerrada<br>
                                                CALBUCO / Federico Errázuriz 404<br>
                                                MOVIL PUNTA ARENAS / Av. Eduardo Frei Montalva 01110,
                                                Local DSM 121. Mall Espacio Urbano Pioneros<br>
                                                OSORNO / Eleuterio Ramírez 1102<br>
                                                PUERTO MONTT CENTRO / Talca 130 esq. Urmeneta
                                                </p>
                                            </div>
                                            <!-- FIN sur -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- FIN Tiendas -->
            <!-- #### -->		
            <!-- Vision y valores -->
            <div class="tab-pane fade in" id="vision">
                <!-- ### -->               
                <section class="center_movil" style="background-color: #ffffff; padding: 20px;">
                    <div class="container clearfix">
                        <div class="col_half borde_derecho">
                            <h3 style="color: #B2121D; margin-bottom: 0; text-transform: uppercase; text-align: center;">¿Qué nos guía como abcdin?</h3>
                        </div>
                        <div class="col_half col_last" style="padding-top:10px;">
                            <h3 style="margin-bottom: 0; color: #7F7F7F;">Visión abcdin</h3>
                            <p style="color: #7F7F7F;">Queremos ser un retail especialista excepcional.</p>
                        </div>
                    </div>            
                </section>
                <section class="center_movil" style="background-color: #e4e4e4; padding-top: 20px; height: 100%;">
                    <div class="container clearfix">
                        <div class="col_full left" >
                            <div style="width: 100%%; margin: 0 auto; display:block;">
                                <h3 style="color: #333333;">
                                    <img src="/assets/media/icon-visiovalores-activo.png" alt="abcdin">Valores abcdin
                                </h3>
                            </div>
                        </div>
                    </div>            
                </section>
                <section class="center_movil" style="background-color: #e4e4e4; padding-bottom: 20px;">
                    <div class="container clearfix" style="background-color: #fff; padding: 20px 40px;  border-radius: 20px;">
                        <div class="col_half">
                            <h3 style="color: #333; margin-bottom: 0;">Honestidad</h3>
                            <p>Somos en todo momento claros y sinceros en nuestro actuar tanto en forma interna como con los clientes (acorde con la moral y las buenas prácticas), estando abiertos a la autoevaluación y autocrítica en forma permanente.</p>                  
                            <h3 style="color: #333; margin-bottom: 0;">Creatividad</h3>
                            <p>Cultivamos y valoramos el aporte creativo e innovador de todos los trabajadores, en lo referente al crecimiento de la empresa, nuestros clientes y el mejoramiento profesional y personal de cada uno de nosotros.</p>
                            <h3 style="color: #333; margin-bottom: 0;">Perseverancia</h3>
                            <p>Mantenernos siempre firmes y constantes en la realización de nuestras actividades, superando las dificultades que se puedan presentar hasta lograr los objetivos y metas definidos.</p>
                            <h3 style="color: #333; margin-bottom: 0;">Austeridad</h3>
                            <p>Nuestro compromiso es permanente con el cuidado y el uso eficiente de los recursos, orientándonos siempre generar valor a la empresa, así como para entregar soluciones a los clientes acorde a su propia realidad.</p>
                            <h3 style="color: #333; margin-bottom: 0;">Compromiso</h3>
                            <p>Tenemos un alto grado de cooperación y compromiso con todos los clientes y personas que trabajan en nuestra organización, de manera de apoyar y fomentar el espíritu de equipo en toda la compañía.</p>
                        </div>
                        <div class="col_half col_last">
                            <div style="width: 100%%; margin: 0 auto; display:block;">
                                <img src="/assets/media/img-valores.jpg" alt="abcdin">
                            </div>
                        </div>
                    </div>            
                </section>
            </div>
            <!-- FIN Vision y valores -->
            <!-- #### -->						
        </div>     
    </div>   
    <!-- END CONTENIDO DE NUESTRA EMPRESA -->    
</div>
<!-- FIN #section contenido de tabs -->