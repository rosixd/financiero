<div id="ModalFormularioQuieroMiTarjeta2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                <h2>
                    <img class="tarjeta" src="img/head-tarjeta.png"> Quiero mi tarjeta 
                    <img src="img/head-marca.png">
                </h2>
                <div class="pasos">
                    <span>PASO</span>
                    <ul>
                        <li>1</li>
                        <li class="activo">2</li>
                    </ul>
                </div>
                <div class="clearfix my-4 "></div>
                <div class="container">
                    <div class="row">
                        <form class="formValida col" id="formulario2">
                            <div class="col contenidoFormulario1 px-0">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="nacionalidad">NACIONALIDAD&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="nacionalidad" name="nacionalidad">
                                                <option disabled>ELIGE TU NACIONALIDAD</option>
                                                <option value="Chileno" id="Chileno" >CHILENA</option>
                                                <option value="Extranjero Residente" id="Extranjero Residente" >EXTRANJERO CON RESIDENCIA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="estadocivil">ESTADO CIVIL&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="estadocivil" name="estadocivil">
                                                <option>ELIGE TU ESTADO CIVIL</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="sexo">SEXO &nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="sexo" name="sexo">
                                                <option disabled readonly>ELIGE TU SEXO</option>
                                                <option value="f">FEMENINO</option>
                                                <option value="m">MASCULINO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="boxhijos">
                                            <span class="float-left mr-2 ml-1">TIENES HIJOS&nbsp;<span class="rojo requeridos">*</span></span> 
                                            <div class="custom-control custom-radio float-left mr-3">
                                                <input type="radio" class="custom-control-input" id="hijossi" name="si" required>
                                                <label class="custom-control-label" for="hijossi">SI</label>
                                            </div>
                                            <div class="custom-control custom-radio float-left">
                                                <input type="radio" class="custom-control-input" id="hijosno" name="no" required>
                                                <label class="custom-control-label" for="hijosno">NO</label>
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group hijos">
                                            <label class="contactoCredito" for="nrohijos">¿CUANTOS?&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="nrohijos" name="nrohijos">
                                                <option disabled readonly>ELIGE CUANTOS HIJOS TIENES</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="actividad">ACTIVIDAD O PROFESION&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="actividad" name="actividad">
                                                <option disabled readonly>ELIGE TU ACTIVIDAD</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="extra" for="fecingreso">FECHA DE INGRESO AL ACTUAL TRABAJO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <div class="form-group"> <!-- Date input -->
                                                <input class="form-control" id="fecingreso" name="fecingreso" placeholder="DD/MM/AAAA" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlSelect16">REGIÓN&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="FormControlSelect16">
                                                <option>ELIGE TU REGIÓN</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlSelect17">COMUNA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="FormControlSelect17">
                                                <option>ELIGE LA COMUNA EN LA QUE VIVES</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlInput18">CALLE&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1" id="FormControlInput18" placeholder="AV. MANUEL MONTT">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlInput19">NÚMERO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1" id="FormControlInput19" placeholder="1234">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlInput20">INDICACIONES DIRECCIÓN</label>
                                            <input type="text" class="form-control px-3 pb-1" id="FormControlInput20" placeholder="CUENTANOS COMO LLEGAR">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlSelect21">TIPO DE VIVIENDA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="FormControlSelect21">
                                                <option>ELIGE</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="extra" for="FormControlSelect22">FECHA OCUPACIÓN VIVIENDA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <div class="form-group"> <!-- Date input -->
                                                <input class="form-control" id="date" name="date" placeholder="DD/MM/AAAA" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlSelect23">CONDICIÓN HABITACIONAL&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="FormControlSelect23">
                                                <option>ELIGE</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlSelect24">ARRIENDO / DIVIDENDO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1" id="FormControlSelect24">
                                                <option>ELIGE</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>            
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="boxhijos">
                                            <span class="float-left mr-2 ml-2">¿TIENES PROPIEDADES?&nbsp;<span class="rojo requeridos">*</span></span> 
                                            <div class="custom-control custom-radio float-left mr-5">
                                                <input type="radio" class="custom-control-input" id="Validation19" name="radio25" required >
                                                <label class="custom-control-label" for="Validation19">SI</label>
                                            </div>
                                            <div class="custom-control custom-radio float-left">
                                                <input type="radio" class="custom-control-input" id="Validation20" name="radio25" required >
                                                <label class="custom-control-label" for="Validation20">NO</label>
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="boxhijos">
                                            <span class="float-left mr-2 ml-2">¿TIENES AUTOMOVIL?&nbsp;<span class="rojo requeridos">*</span></span>
                                            <div class="custom-control custom-radio float-left mr-5">
                                                <input type="radio" class="custom-control-input" id="Validation21" name="radio26" required >
                                                <label class="custom-control-label" for="Validation21">SI</label>
                                            </div>
                                            <div class="custom-control custom-radio float-left">
                                                <input type="radio" class="custom-control-input" id="Validation22" name="radio26" required >
                                                <label class="custom-control-label" for="Validation22">NO</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12 col-md-4 offset-8">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="FormControlInput27">PATENTE&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1" id="FormControlInput27" placeholder="123456">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary btn-block btnRojo" type="submit">ENVIAR</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <div>
                <a href="#">
                    <img src="img/banners/banner-quiero-tarjeta.jpg" class="img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>
        