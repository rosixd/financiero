@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('page::pages.create page') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('page::pages.create page') }}</li>
</ol>
@stop
@section('content')

{!! Form::open(['route' => ['admin.page.page.store'], 'method' => 'post']) !!}
@php
	if( !isset( $lang ) ){
		$lang = App::getLocale();
	}
@endphp
<div class="row">

    <div class="col-md-12"> 

        <div class="box box-primary">

            <div class="box-body">

                <div class="form-group">
                    <label for="title">{{trans('page::pages.title')}}</label>
                    <input type="text" class="form-control" id="title" name="title">
                    <span class="text-danger">
                        {{ ( $errors->has('title') || $errors->has( $lang . '.title') ) ? trans('page::messages.title is required') : '' }}
                    </span>
                </div>

                <div class="form-group">
                    <label for="slug">{{trans('page::pages.slug')}}</label>
                    <input type="text" class="form-control" id="slug" name="slug">
                    <span class="text-danger">
                        {{ $errors->has('slug') ? trans('page::messages.slug is required') : '' }}
                    </span>
                </div>

                {{--  @include('core::selectImg',['name'=>'og_image', 
                    'label' => trans('page::pages.image'),
                    'required' => trans('page::pages.validation.image')
                    ])  --}}
					
		
                <div class="form-group">
                    <label for="body">
                        {{ trans('page::pages.body') }}
                    </label>
                            <textarea class="ckeditor" name="body" id="body"></textarea>
                    <span class="text-danger">
                        {{ ( $errors->has('body') || $errors->has( $lang . '.body') ) ? trans('page::messages.body is required') : '' }}
                    </span>
                </div>

                <div class="form-group">
                    <label>{{ trans('page::pages.meta_data') }}</label>
                    <select class="" multiple="multiple" name="ABC04_id[]" id="ABC04_id">
                        <option value="">--Seleccione--</option>
                        @foreach ($metadatos as $metadato)
                            <option value="{{$metadato->ABC04_id}}">{{$metadato->ABC04_contenido}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="is_home" name="is_home" value="1">
                    <label class="form-check-label" for="status">{{trans('page::pages.is homepage')}}</label>
                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-md btn-primary">{{trans('page::pages.button.save')}}</button>
                <a href="{{route('admin.page.page.index')}}" class="btn btn-md btn-danger pull-right">{{trans('page::pages.button.cancel')}}</a>
            </div>

        </div>

    </div>

</div>

{!! Form::close() !!}

@stop

@section('footer')    
@stop

@push('js-stack')
    <script>

        $( document ).ready(function() {

            $('select[multiple="multiple"]').selectize({
                plugins: ['remove_button'], 
                delimiter: ',', 
                persist: false, 
            });

            $('#title').on('keyup', function(){
                var dato =  AppGeneral.GenerateSlug( $('#title').val() );
                 $('#slug').val(dato);
            });
			 $('#slug').on('blur', function(){
                var dato =  AppGeneral.GenerateSlug( $('#slug').val() );
                 $('#slug').val(dato);
            });

        });


    </script>
@endpush
