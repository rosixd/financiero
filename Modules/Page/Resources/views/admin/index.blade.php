@extends('layouts.master')

@section('content-header')
<h1>
	{{ trans('page::pages.pages') }}
</h1>
<ol class="breadcrumb">
	<li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
	<li class="active">{{ trans('page::pages.pages') }}</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">	

		<div class="row">
			<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
				@if ($currentUser->hasAccess('page.pages.create'))
				<a href="{{ route('admin.page.page.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
					<i class="fa fa-pencil"></i> {{ trans('page::pages.create') }}
				</a>
				@endif
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 2, "desc" ]]'>
						<thead>
							<tr>
								<th>{{ trans('page::pages.table.resource titulo') }}</th>
								<th>{{ trans('page::pages.table.resource url') }}</th>
								<th>{{ trans('page::pages.table.resource creado el') }}</th>
								<th data-sortable="false">{{ trans('page::pages.table.resource accion') }}</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<!-- /.box-body -->
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
@include('core::modalLog')
@stop

@section('footer')
@stop

@push('js-stack')
@endpush

@section('scripts')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('page.pages.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('page.pages.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('page.pages.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('page.pages.index') ? 'true' : 'false' }}
    };
</script>
@stop