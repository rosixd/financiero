<?php

namespace Modules\Menu\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model implements Auditable
{
    use  SoftDeletes,Translatable, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'title',
        'status',
        'primary',
        'slug',
    ];
    public $translatedAttributes = ['title', 'status'];
    protected $table = 'menu__menus';
    protected $appends = ['key','articulo'];

    public function getKeyAttribute()
    {
        return "Men&uacute;";
    }

    public function getArticuloAttribute()
    {
        return "el";
    }

    public function menuitems()
    {
        return $this->hasMany('Modules\Menu\Entities\Menuitem')->orderBy('position', 'asc');
    }

    public static function scopeBuscarMenu( $query, $id ){

        $datos = [];
        $menus = $query
            ->with([
                'menuitems' => function( $query ){
			        $query->where('is_root', 0);}, 
                'menuitems.menuitemstrans'=> function ($query) {
                    $query->where('title', '!=', 'root')->where('locale', '=', 'es');}, 
                'menuitems.pagetranslation'
            ])
            ->find( $id );

        if(!$menus){
            return $datos;
        }
	
        foreach ($menus->menuitems as $key => $value) {

            $url = "";
            $target = false;
            $translation = false;
            
            $itemMenu = [
                'title'=> "", 
                "target" => "",
                "class" => $value->class,
                'icon' => $value->icon
            ];

            if( $value->hasTranslation() ){
                $translation = $value->getTranslation();
                $itemMenu['title'] = $translation->title ;
            }
	    
            if( $translation && $value->hasTranslation() ){
                $uri = $translation->uri;
                $itemMenu["uri"] = $uri;
            }

            if( $translation && $value->pagetranslation ){
                $url = env('APP_URL')  . "/" . $value->pagetranslation->slug;                
            }

            if( preg_match("/external/i", $value->link_type ) && $translation ){
                $url = $translation->url;
            }

            if( trim($value->target) != '' && $translation ){
                $itemMenu["target"] = ' target="' . $value->target . '"';
            }

            if( $value->hasTranslation() ){
                $itemMenu["url"] = $url;
            }

            $datos[] = $itemMenu;            
        }

	    return $datos; 
    }

    public static function scopeBuscarNombreMenu( $query, $id ){

/*         $nombre = $query->select('name')->find($id);
        if($nombre){
            return $nombre->toArray();
        }
        return $nombre;  */
       
        /* $nombre = $query->select('title')->with(['menuitems.menuitemstrans'=> function ($query) {
            $query->where('title', '!=', 'root');
        }])
        ->find($id)
        ->getTranslation(); */ 
        $nombre = $query->find($id)->getTranslation();
        //dd( $nombre->toArray() );
        if( $nombre ){
            return $nombre->toArray();
        }
	    return $nombre;
        
    }
}
