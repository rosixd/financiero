<?php

namespace Modules\Menu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMenuRequest extends FormRequest
{
    public function rules()
    {
        $menu = $this->route()->parameter('menu');

        return [
            'title' => 'required',
            /* 'name' => 'required', */
            'primary' => "unique:menu__menus,primary,{$menu->id}",
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
	        'title.required' => 'Sr. Usuario, debe ingresar el Título.',
            /* 'name.required' => 'Sr. Usuario, debe ingresar el nombre', */
            'primary.unique' => trans('menu::validation.only one primary menu'),
        ];
    }
}
