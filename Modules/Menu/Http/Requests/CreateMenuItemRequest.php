<?php

namespace Modules\Menu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMenuItemRequest extends FormRequest
{
    public function rules()
    {
        return [
            'es.title'  =>  'required',

        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required' => 'Sr. Usuario, debe ingresar el título.',
			//'title.max' => 'Sr. Usuario, el título no debe contener más de 200 carácteres.',
        ];
    }
}
