<?php

namespace Modules\Menu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMenuItemRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title'  =>  ['required', 'max:200'],

        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required' => 'Sr. Usuario, debe ingresar el título.',
			 	 'title.max' => 'Sr. Usuario, el título no debe contener más de 200 carácteres.',
        ];
    }
}
