<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group([ 'prefix' => '/menuitem'
	// , 'middleware' => 'api.token'
], function (Router $router) {
    $router->post('/update', [
        'as' => 'api.menuitem.update',
        'uses' => 'MenuItemController@update',
    ]);
    $router->post('/eliminar', [
        'as' => 'api.menuitem.delete',
        'uses' => 'MenuItemController@delete',
    ]);
});
