<?php

namespace Modules\Menu\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Http\Requests\CreateMenuRequest;
use Modules\Menu\Http\Requests\UpdateMenuRequest;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Menu\Repositories\MenuRepository;
use Modules\Menu\Services\MenuRenderer;
use DB;
use Yajra\Datatables\Datatables;

class MenuController extends AdminBaseController
{
    /**
     * @var MenuRepository
     */
    private $menu;
    /**
     * @var MenuItemRepository
     */
    private $menuItem;
    /**
     * @var MenuRenderer
     */
    private $menuRenderer;

    public function __construct(
        MenuRepository $menu,
        MenuItemRepository $menuItem,
        MenuRenderer $menuRenderer
    ) {
        parent::__construct();
        $this->menu = $menu;
        $this->menuItem = $menuItem;
        $this->menuRenderer = $menuRenderer;
    }

    public function index()
    {
        $menus = $this->menu->all();

        return view('menu::admin.menus.index', compact('menus'));
    }

    public function create()
    {
        return view('menu::admin.menus.create');
    }

    public function store(CreateMenuRequest $request)
    {
    
        $request->request->add(["slug" => str_slug($request->name)]);
        $this->menu->create($request->all());

        return redirect()->route('admin.menu.menu.index')
            ->withSuccess(trans('menu::messages.menu created'));
    }

    public function edit(Menu $menu)
    {
        $menuItems = $this->menuItem->allRootsForMenu($menu->id);

        $menuStructure = $this->menuRenderer->renderForMenu($menu->id, $menuItems->nest());

        return view('menu::admin.menus.edit', compact('menu', 'menuStructure'));
    }

    public function update(Menu $menu, UpdateMenuRequest $request)
    {
        $menu->fill($request->all());
        
       /* if(!$menu->isDirty()){
            return redirect()->route('admin.menu.menu.edit',[$menu->id])
            ->with("warning", trans('menu::messages.updated sin') );
        }*/
        //dd( $request->all() );
    	$request->request->add(["slug" => str_slug($request->name)]);
        $this->menu->update($menu, $request->all());

        return redirect()->route('admin.menu.menu.index')
            ->withSuccess(trans('menu::messages.menu updated'));
    }

    public function destroy(Menu $menu)
    {   
        $bloquear = [
            '3',
            '4',
            '5',
            '6',
            '7'
        ];
		
		 $deleted_at = $menu->deleted_at;
		 
		 $control = false;
		if(is_null($deleted_at)){
			 foreach ($bloquear as $key => $value) {
         
			  if($menu->id == $value){
            
	            $control = true;
			
	            $json = array(
	                'asignado' => $control,
	                'msn' => trans('menu::messages.resource nodesactivate')
	            );
	          }
	        }
		}
       

        if(!$control){

            $deleted_at = $menu->deleted_at;

            $success  = is_null($deleted_at)? $menu->delete():$menu->restore();
            
            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('menu::messages.resource desactivate'):trans('menu::messages.resource activate'),
                'status' => is_null($deleted_at)? true:false)
            ;
        }
        
        return response()->json($json); 
    }

        /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

    */
    public function datatable()
    {	 
        
        $menus = Menu::withTrashed()
            ->select('menu__menus.id', 'menu__menus.created_at', 'menu__menus.deleted_at', 'menu__menu_translations.title')
            ->leftJoin('menu__menu_translations', 'menu__menu_translations.menu_id', '=', 'menu__menus.id');

        return Datatables::of($menus)
            ->addColumn('ruta_edit', function ($menus) {
                return route("admin.menu.menu.edit",[$menus->id]);
            })        
            ->addColumn('ruta_destroy', function($menus){
                return route("admin.menu.menu.destroy",[$menus->id]);
            })
            ->addColumn('inactivo', function ($menus) {
                return is_null($menus->deleted_at)? false:true;
            })
            ->make(true);
    }
}
