<?php

use Illuminate\Routing\Router;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Entities\Menuitem;

/** @var Router $router */
$router->bind('menu', function ($id) {

    $repo = app(\Modules\Menu\Repositories\MenuRepository::class)->find($id);
    return (is_null($repo))? Menu::withTrashed()->find($id):$repo;
});
$router->bind('menuitem', function ($id) {
    $repo = app(\Modules\Menu\Repositories\MenuItemRepository::class)->find($id);
    return (is_null($repo))? Menuitem::withTrashed()->find($id):$repo;
});

$router->group(['prefix' => '/menu'], function (Router $router) {
    $router->get('menus', [
        'as' => 'admin.menu.menu.index',
        'uses' => 'MenuController@index',
        'middleware' => 'can:menu.menus.index',
        'script' => 'AppMenu',
        'view' => 'index',
        'modulo' => 'menu',
        'submodulo' => 'menu',
        'datatable' => 'admin.menu.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('menus/create', [
        'as' => 'admin.menu.menu.create',
         'middleware' => 'can:menu.menus.create',
        'uses' => 'MenuController@create',
    ]);
    $router->post('menus', [
        'as' => 'admin.menu.menu.store',
         'middleware' => 'can:menu.menus.create',
        'uses' => 'MenuController@store',
    ]);
    $router->get('menus/{menu}/edit', [
        'as' => 'admin.menu.menu.edit',
         'middleware' => 'can:menu.menus.edit',
        'uses' => 'MenuController@edit',
    ]);
    $router->put('menus/{menu}', [
        'as' => 'admin.menu.menu.update',
         'middleware' => 'can:menu.menus.edit',
        'uses' => 'MenuController@update',
    ]);
    $router->get('menus/destroy/{menu}', [
        'as' => 'admin.menu.menu.destroy',
         'middleware' => 'can:menu.menus.destroy',
        'uses' => 'MenuController@destroy',
    ]);

    $router->get('menus/{menu}/menuitem', [
        'as' => 'dashboard.menuitem.index',
         'middleware' => 'can:menu.menuitems.index',
        'uses' => 'MenuItemController@index',
        'script' => 'AppMenuItem',
        'view' => 'index',
        'modulo' => 'menuitem',
        'submodulo' => 'menuitem',
        'datatable' => 'admin.menuitem.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('menus/{menu}/menuitem/create', [
        'as' => 'dashboard.menuitem.create',
         'middleware' => 'can:menu.menuitems.create',
        'uses' => 'MenuItemController@create',
    ]);
    $router->post('menus/{menu}/menuitem', [
        'as' => 'dashboard.menuitem.store',
         'middleware' => 'can:menu.menuitems.create',
        'uses' => 'MenuItemController@store',
    ]);
    $router->get('menus/{menu}/menuitem/{menuitem}/edit', [
        'as' => 'dashboard.menuitem.edit',
         'middleware' => 'can:menu.menuitems.edit',
        'uses' => 'MenuItemController@edit',
    ]);
    $router->put('menus/{menu}/menuitem/{menuitem}', [
        'as' => 'dashboard.menuitem.update',
         'middleware' => 'can:menu.menuitems.edit',
        'uses' => 'MenuItemController@update',
    ]);
    $router->get('menus/{menu}/destroy/menuitem/{menuitem}', [
        'as' => 'dashboard.menuitem.destroy',
         'middleware' => 'can:menu.menuitems.destroy',
        'uses' => 'MenuItemController@destroy',
    ]);
    
    $router->get('menu/datatable', [
        'uses' => 'MenuController@datatable',
        'as' => 'admin.menu.datatable',
        'middleware' => 'can:menu.menus.index',
    ]);
});
