


@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('menu::menu.titles.menu') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('menu::menu.breadcrumb.menu') }}</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                @if ($currentUser->hasAccess('menu.menus.create'))
                <a href="{{ URL::route('admin.menu.menu.create') }}" class="btn btn-primary btn-md">
                    <i class="fa fa-pencil"></i> {{ trans('menu::menu.button.create menu') }}
                </a>
                @endif
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive" style="width: 100%;">
                    <table class="data-table table table-bordered table-hover"  id="dataTable"  data-order='[[ 1, "desc" ]]'>
                        <thead>
                            <tr>
                            {{--   <th>{{ trans('menu::menu.table.name') }}</th>  --}}
                                <th style="width:50%;">{{ trans('menu::menu.table.title') }}</th>
                                <th style="width:25%;">{{ trans('menu::menu.table.create-at') }}</th>
                                <th data-sortable="false" style="width:25%;">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
    </div>
</div>
@include('core::modalLog')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('menu::menu.titles.create menu') }}</dd>
    </dl>
@stop

@push('js-stack')
<?php $locale = App::getLocale(); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.menu.menu.create') ?>" }
            ]
        });
    });
</script>
@endpush

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('menu.menus.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('menu.menus.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('menu.menus.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('menu.menus.index') ? 'true' : 'false' }}
    };
</script>
@endprepend