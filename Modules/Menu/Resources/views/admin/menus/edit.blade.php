@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('menu::menu.titles.edit menu') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li><a href="{{ URL::route('admin.menu.menu.index') }}">{{ trans('menu::menu.breadcrumb.menu') }}</a></li>
    <li>{{ trans('menu::menu.breadcrumb.edit menu') }}</li>
</ol>
@stop

@push('css-stack')
    <link href="{!! Module::asset('menu:css/nestable.css') !!}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
{!! Form::open(['route' => ['admin.menu.menu.update', $menu->id], 'method' => 'put', 'id' => 'ValidacionEditar']) !!}
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                @if ($currentUser->hasAccess('menu.menuitems.create'))
                <a href="{{ URL::route('dashboard.menuitem.create', [$menu->id]) }}" class="btn btn-primary btn-md">
                    <i class="fa fa-pencil"></i> {{ trans('menu::menu.button.create menu item') }}
                </a>
                @endif
            </div>
        </div>
        <div class="box box-primary" style="overflow: hidden;">
            <div class="box-body">
                {!! $menuStructure !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
{{--             <div class="box-header">
                <h3 class="box-title">{{ trans('core::core.title.translatable fields') }}</h3>
            </div> --}}
            {{-- <div class="box-body"> --}}
                <div class="nav-tabs-custom">

                    <div class="tab-content">
                        <?php $i = 0; ?>
                        <?php foreach (LaravelLocalization::getSupportedLocales() as $locale => $language): ?>
                            <?php $i++; ?>
                            <div class="tab-pane {{ App::getLocale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                                @include('menu::admin.menus.partials.edit-trans-fields', ['lang' => $locale])
                            </div>
                        <?php endforeach; ?>
                        <div class="box-footer">
                            @if ($currentUser->hasAccess('menu.menus.edit'))
                            <button type="submit" class="btn btn-primary btn-md">{{ trans('core::core.button.update') }}</button>
                            @endif
                            <a class="btn btn-danger pull-right btn-md" href="{{ URL::route('admin.menu.menu.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                        </div>
                    </div>
                </div>
            {{-- </div> --}}
        </div>
{{--          <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">{{ trans('core::core.title.non translatable fields') }}</h3>
            </div>
            <div class="box-body">
                @include('menu::admin.menus.partials.edit-fields')
            </div>
        </div>  --}}

    </div>
</div>
{!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('menu::menu.titles.create menu item') }}</dd>
        <dt><code>b</code></dt>
        <dd>{{ trans('menu::menu.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'c', route: "<?= route('dashboard.menuitem.create', [$menu->id]) ?>" },
            { key: 'b', route: "<?= route('admin.menu.menu.index') ?>" }
        ]
    });

    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('input[type="checkbox"]').on('ifChecked', function(){
      $(this).parent().find('input[type=hidden]').remove();
    });

    $('input[type="checkbox"]').on('ifUnchecked', function(){
      var name = $(this).attr('name'),
          input = '<input type="hidden" name="' + name + '" value="0" />';
      $(this).parent().append(input);
    });
});
</script>
<script src="{!! Module::asset('menu:js/jquery.nestable.js') !!}"></script>
<script>
$( document ).ready(function() {
    $('.dd').nestable();
    $('.dd').on('change', function() {
        var data = $('.dd').nestable('serialize');
        $.ajax({
            type: 'POST',
            url: '{{ route('api.menuitem.update') }}',
            data: {'menu': JSON.stringify(data), '_token': '<?php echo csrf_token(); ?>'},
            dataType: 'json',
            success: function(data) {

            },
            error:function (xhr, ajaxOptions, thrownError){
            }
        });
    });
});
</script>
<script>
    $( document ).ready(function() {
        $('.jsDeleteMenuItem').on('click', function(e) {
            var self = $(this),
                menuItemId = self.data('item-id');
            $.ajax({
                type: 'POST',
                url: '{{ route('api.menuitem.delete') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    menuitem: menuItemId
                },
                success: function(data) {
                    if (! data.errors) {
                        var elem = self.closest('li');
                        elem.fadeOut()
                        AppGeneral.notificaciones('success', "Sr. Usuario, los datos se han eliminado correctamente");

                        setTimeout(function(){
                            elem.remove()
                        }, 300);
                    }
                }
            });
        });

        var inicio = $("form").serialize();

        $('#ValidacionEditar').submit(function(){
            if(inicio === $("form").serialize()){ 
               AppGeneral.notificaciones('warning', "{{ trans('media::messages.updated sin') }}");
               return false;
            } 
        });
    });
</script>
@endpush
