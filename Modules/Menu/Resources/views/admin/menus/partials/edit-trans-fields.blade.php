<div class="box-body">
    <div class="form-group">
        {!! Form::label("title", trans('menu::menu.form.title')) !!}
        <?php $old = $menu->hasTranslation($lang) ? $menu->translate($lang)->title : '' ?>
        {!! Form::text("title", old("{$lang}[title]", $old), ['class' => 'form-control', 'placeholder' => trans('menu::menu.form.title')]) !!}
        <span class="text-danger">
            {{ $errors->has('title') ? "Sr. Usuario, debe ingresar un título." : '' }}
        </span>
    </div>
</div>