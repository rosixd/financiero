<div class="box-body">    
<div class='form-group'>
    {!! Form::label("title", trans('menu::menu.form.title')) !!}
    {!! Form::text("title", old("$lang.title"), ['maxlength'=>'200','class' => 'form-control', 'placeholder' => trans('menu::menu.form.title')]) !!}
    
	<span class="text-danger">
        {{ $errors->has('title') ? "Sr. Usuario, debe ingresar un título." : '' }}
    </span>
    </div>
</div>
{{--  <div class="checkbox">
    <label for="{{$lang}}[status]">
        <input id="{{$lang}}[status]"
                name="{{$lang}}[status]"
                type="checkbox"
                class="flat-blue"
                {{ (is_null(old("$lang.status"))) ?: 'checked' }}
                value="1" />
        {{ trans('menu::menu.form.status') }}
    </label>
</div>  --}}
