
<div class='form-group'>
    {!! Form::label("{$lang}[title]", trans('menu::menu.form.title')) !!}
    {!! Form::text("{$lang}[title]", old("{$lang}[title]"), ['maxlength' => '200', 'class' => 'form-control', 'placeholder' => trans('menu::menu.form.title'), 'autofocus']) !!}
    <span class="text-danger">
        {{ $errors->has('es.title') ? "Sr. Usuario, debe ingresar el título." : '' }}
    </span>

</div>
<div class="form-group link-type-depended link-internal">
    {!! Form::label("{$lang}[uri]", trans('menu::menu.form.uri')) !!}
    <div class='input-group{{ $errors->has("{$lang}[uri]") ? ' has-error' : '' }}'>
        <span class="input-group-addon">/{{ $lang }}/</span>
        {!! Form::text("{$lang}[uri]", old("{$lang}[uri]"), ['class' => 'form-control', 'placeholder' => trans('menu::menu.form.uri')]) !!}
        {!! $errors->first("{$lang}[uri]", '<span class="help-block">:message</span>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has("{$lang}[url]") ? ' has-error' : '' }} link-type-depended link-external">
    {!! Form::label("{$lang}[url]", trans('menu::menu.form.url')) !!}
    {!! Form::text("{$lang}[url]", old("{$lang}[url]"), ['class' => 'form-control', 'placeholder' => trans('menu::menu.form.url')]) !!}
    {!! $errors->first("{$lang}[url]", '<span class="help-block">:message</span>') !!}
</div>

