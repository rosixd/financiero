<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Region extends Model implements Auditable
{
	use SoftDeletes, \OwenIt\Auditing\Auditable;

	protected $table = 'ABC11_Regiones';
	protected $primaryKey = 'ABC11_id';
	protected $fillable = [
		"ABC11_nombre",
		"ABC11_numero_romano",
		"ABC11_codigo",
		"deleted_at",
	];

	protected $appends = ['name','id','description','key', 'articulo'];


/* 	public function comuna()
	{
		return $this->hasMany('Modules\Administracion\Entities\Comuna', 'ABC11_id','ABC11_id')->withTrashed();
	} */

	public function getNameAttribute()
	{
		return $this->ABC11_nombre;
	}

	public function getIdAttribute()
	{
		return $this->ABC11_id;
	}

	public function getDescriptionAttribute()
	{
		return $this->ABC11_numero_romano;
	}

	public function getKeyAttribute()
    {
        return "Regi&oacute;n";
    }

    public function getArticuloAttribute()
    {
        return "la";
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}