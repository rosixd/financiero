<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\Model;

class CiudadTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'administracion__ciudad_translations';
}
