<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class CodigoArea extends Model implements Auditable
{
	use SoftDeletes, \OwenIt\Auditing\Auditable;

	protected $table = 'ABC29_Codigo_Area';
	protected $primaryKey = 'ABC29_id';
	protected $fillable = [
		"ABC29_codigo",
		"ABC11_id",
		"deleted_at",
	];

}