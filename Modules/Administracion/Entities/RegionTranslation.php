<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\Model;

class RegionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'administracion__region_translations';
}
