<?php

namespace Modules\Administracion\Entities;


use Illuminate\Database\Eloquent\Model;


class ActivdadEconomica extends Model 
{
	

	protected $table = 'ABC21_Actividad_economica';
    protected $primaryKey = 'ABC21_id';
    
    public $timestamps = false;

	protected $fillable = [
        
        
        'ABC21_nombre',
        'ABC21_codigo'
        
        
	];

}