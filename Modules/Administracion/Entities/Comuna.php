<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Comuna extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC13_Comunas';
	protected $primaryKey = 'ABC13_id';
    protected $fillable = [
		"ABC13_nombre",
		"ABC13_codigo",
		"ABC11_id",
		"deleted_at",
    ];

    public function region()
    {
      return $this->belongsTo('Modules\Administracion\Entities\Region','ABC11_id','ABC11_id')->withTrashed();
    }

    public function ciudad()
    {
        return $this->hasMany('Modules\Administracion\Entities\Ciudad', 'ABC13_id','ABC13_id');
    }

    protected $appends = ['name','id','description','key','articulo'];

    public function getNameAttribute()
    {
        return $this->ABC13_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC13_id;
    }

    public function getDescriptionAttribute()
    {
        return $this->region->ABC11_nombre;
    }

    public function getKeyAttribute()
    {
        return "Comuna";
    }

    public function getArticuloAttribute()
    {
        return "la";
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}