<?php

namespace Modules\Administracion\Entities;


use Illuminate\Database\Eloquent\Model;


class CondicionHabitacional extends Model 
{
	

	protected $table = 'ABC23_Condicion_habitacional';
    protected $primaryKey = 'ABC23_id';
    
    public $timestamps = false;

	protected $fillable = [
        
        
        'ABC23_nombre',
        'ABC23_codigo'
        
        
	];

}