<?php

namespace Modules\Administracion\Entities;


use Illuminate\Database\Eloquent\Model;


class Acreditacion extends Model 
{
	

	protected $table = 'ABC22_Acreditacion';
    protected $primaryKey = 'ABC22_id';
    
    public $timestamps = false;

	protected $fillable = [
        
        
        'ABC22_nombre',
        'ABC22_codigo'
        
        
	];

}