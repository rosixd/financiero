<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Ciudad extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC12_Ciudades';
	protected $primaryKey = 'ABC12_id';
    protected $fillable = [
		"ABC12_nombre",
		"ABC13_id",
		"deleted_at",
    ];

    public function comuna()
    {
      return $this->belongsTo('Modules\Administracion\Entities\Comuna','ABC13_id','ABC13_id')->withTrashed();
    }

    protected $appends = ['name','id','description','key', 'articulo'];

    public function getNameAttribute()
    {
        return $this->ABC12_nombre;
    }

    public function getIdAttribute()
    {
        return $this->ABC12_id;
    }

    public function getDescriptionAttribute()
    {
        return $this->comuna->ABC13_nombre;
    }

    public function getKeyAttribute()
    {
        return "Ciudad";
    }

    public function getArticuloAttribute()
    {
        return "la";
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
