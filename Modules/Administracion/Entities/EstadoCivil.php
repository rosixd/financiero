<?php

namespace Modules\Administracion\Entities;


use Illuminate\Database\Eloquent\Model;


class EstadoCivil extends Model 
{
	

	protected $table = 'ABC24_Estado_civil';
    protected $primaryKey = 'ABC24_id';
    
    public $timestamps = false;

	protected $fillable = [
        
        'ABC24_nombre',
        'ABC24_codigo'
        
	];

}