<?php

namespace Modules\Administracion\Entities;

use Illuminate\Database\Eloquent\Model;

class ComunaTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'administracion__comuna_translations';
}
