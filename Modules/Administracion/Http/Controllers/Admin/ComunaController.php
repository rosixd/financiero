<?php

namespace Modules\Administracion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Administracion\Entities\Comuna;
use Modules\Administracion\Entities\Region;
use Modules\Administracion\Http\Requests\CreateComunaRequest;
use Modules\Administracion\Http\Requests\UpdateComunaRequest;
use Modules\Administracion\Repositories\ComunaRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class ComunaController extends AdminBaseController
{
    /**
     * @var ComunaRepository
     */
    private $comuna;

    public function __construct(ComunaRepository $comuna)
    {
        parent::__construct();

        $this->comuna = $comuna;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$comunas = $this->comuna->all();

        return view('administracion::admin.comunas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $region = Region::all();
        return view('administracion::admin.comunas.create', compact('region') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateComunaRequest $request
     * @return Response
     */
    public function store(CreateComunaRequest $request)
    {
        $this->comuna->create($request->all());

        return redirect()->route('admin.administracion.comuna.index')
        ->withSuccess(trans('administracion::comunas.messages.resource created', ['name' => trans('administracion::comunas.title.comunas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Comuna $comuna
     * @return Response
     */
    public function edit(Comuna $comuna)
    {
        $region = Region::all();
        return view('administracion::admin.comunas.edit', compact('comuna', 'region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Comuna $comuna
     * @param  UpdateComunaRequest $request
     * @return Response
     */
    public function update(Comuna $comuna, UpdateComunaRequest $request)
    {
        $this->comuna->update($comuna, $request->all());

        return redirect()->route('admin.administracion.comuna.index')
        ->withSuccess(trans('administracion::comunas.messages.resource updated', ['name' => trans('administracion::comunas.title.comunas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Comuna $comuna
     * @return Response
     */
    public function destroy(Comuna $comuna)
    {
        $deleted_at = $comuna->deleted_at;

        if(is_null($deleted_at)){

            //valido que que no tenga ciudades asocidas
            $asignado = Comuna::with('ciudad')->find($comuna->ABC13_id)->ciudad;
            $asignado = !is_null($asignado->first())? true:false;
           
            $json = array(
                'asignado' => $asignado,
                'msn' => trans('administracion::comunas.messages.resource validciudad')
            );

        }else{

            //valido que la región asociada de la comuna esté activa
            $asignado = Comuna::with('region')->withTrashed()->find($comuna->ABC13_id)->region;
            $asignado = !is_null($asignado->deleted_at)? true:false;

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('administracion::comunas.messages.resource regioninactiva')
            );
        }

        if(!$asignado){

            $success = is_null($deleted_at)? $comuna->delete():$comuna->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('administracion::comunas.messages.resource desactivate'):trans('administracion::comunas.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }
        
        return response()->json($json);
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
        $comuna = Comuna::with('region')->withTrashed()->get();

        return Datatables::of($comuna)
        ->addColumn('ruta_edit', function ($comuna) {
            return route("admin.administracion.comuna.edit",[$comuna->ABC13_id]);
        })        
        ->addColumn('ruta_destroy', function($comuna){
            return route("admin.administracion.comuna.destroy",[$comuna->ABC13_id]);
        })
        ->addColumn('inactivo', function ($comuna) {
            return is_null($comuna->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
