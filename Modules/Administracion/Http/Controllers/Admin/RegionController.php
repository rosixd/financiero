<?php

namespace Modules\Administracion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Administracion\Entities\Region;
use Modules\Administracion\Http\Requests\CreateRegionRequest;
use Modules\Administracion\Http\Requests\UpdateRegionRequest;
use Modules\Administracion\Repositories\RegionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class RegionController extends AdminBaseController
{
    /**
     * @var RegionRepository
     */
    private $region;

    public function __construct(RegionRepository $region)
    {
        parent::__construct();

        $this->region = $region;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$regions = $this->region->all();

        return view('administracion::admin.regions.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('administracion::admin.regions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRegionRequest $request
     * @return Response
     */
    public function store(CreateRegionRequest $request)
    {
        $this->region->create($request->all());

        return redirect()->route('admin.administracion.region.index')
        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('administracion::regions.title.regions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Region $region
     * @return Response
     */
    public function edit(Region $region)
    {
        return view('administracion::admin.regions.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Region $region
     * @param  UpdateRegionRequest $request
     * @return Response
     */
    public function update(Region $region, UpdateRegionRequest $request)
    {

        $this->region->update($region, $request->all());

        return redirect()->route('admin.administracion.region.index')
        ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('administracion::regions.title.regions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Region $region
     * @return Response
     */
    public function destroy(Region $region)
    {  
        $deleted_at = $region->deleted_at;

        $asignado = (is_null($deleted_at) && 
                    Region::with('comuna')->withTrashed()->find($region->ABC11_id)->comuna->first())? true:false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('administracion::regions.messages.resource asignado')
            );

        }else{

            $success  = is_null($deleted_at)? $region->delete():$region->restore();

            $json =array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('administracion::regions.messages.resource desactivate'): trans('administracion::regions.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }
        
        return response()->json($json);
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
        $region = Region::withTrashed()->get();

        return Datatables::of($region)
        ->addColumn('ruta_edit', function ($region) {
            return route("admin.administracion.region.edit",[$region->ABC11_id]);
        })
        ->addColumn('ruta_destroy', function($region){
            return route("admin.administracion.region.destroy",[$region->ABC11_id]);
        })
        ->addColumn('inactivo', function ($region) {
            return is_null($region->deleted_at)? false:true;
        })
        ->make(true);
    }    
}
