<?php

namespace Modules\Administracion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Administracion\Entities\Ciudad;
use Modules\Administracion\Entities\Comuna;
use Modules\Administracion\Http\Requests\CreateCiudadRequest;
use Modules\Administracion\Http\Requests\UpdateCiudadRequest;
use Modules\Administracion\Repositories\CiudadRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class CiudadController extends AdminBaseController
{
    /**
     * @var CiudadRepository
     */
    private $ciudad;

    public function __construct(CiudadRepository $ciudad)
    {
        parent::__construct();

        $this->ciudad = $ciudad;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$ciudads = $this->ciudad->all();

        return view('administracion::admin.ciudads.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $comuna = Comuna::all();
        return view('administracion::admin.ciudads.create', compact('comuna') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCiudadRequest $request
     * @return Response
     */
    public function store(CreateCiudadRequest $request)
    {
        $this->ciudad->create($request->all());

        return redirect()->route('admin.administracion.ciudad.index')
        ->withSuccess(trans('administracion::ciudads.messages.resource created', ['name' => trans('administracion::ciudads.title.ciudads')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ciudad $ciudad
     * @return Response
     */
    public function edit(Ciudad $ciudad)
    {
        $comuna = Comuna::all();
        return view('administracion::admin.ciudads.edit', compact('ciudad', 'comuna'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Ciudad $ciudad
     * @param  UpdateCiudadRequest $request
     * @return Response
     */
    public function update(Ciudad $ciudad, UpdateCiudadRequest $request)
    {
        $this->ciudad->update($ciudad, $request->all());

        return redirect()->route('admin.administracion.ciudad.index')
        ->withSuccess(trans('administracion::ciudads.messages.resource updated', ['name' => trans('administracion::ciudads.title.ciudads')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ciudad $ciudad
     * @return Response
     */
    public function destroy(Ciudad $ciudad)
    {
        $deleted_at = $ciudad->deleted_at;        

        $asignado = Ciudad::with('comuna')->withTrashed()->find($ciudad->ABC12_id);
        $asignado = !is_null($asignado->comuna->deleted_at)? true:false;

        if($asignado){

            $json =  array(
                'asignado' => $asignado,
                'msn' => trans('administracion::ciudads.messages.resource comunainactiva')
            );

        }else{

            $success    = is_null($deleted_at)? $ciudad->delete():$ciudad->restore();
            
            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('administracion::ciudads.messages.resource desactivate'):trans('administracion::ciudads.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }

        return response()->json($json);
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Ruben Morales
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {
        $ciudad = Ciudad::with('comuna')->withTrashed()->get();

        return Datatables::of($ciudad)
        ->addColumn('ruta_edit', function ($ciudad) {
            return route("admin.administracion.ciudad.edit",[$ciudad->ABC12_id]);
        })        
        ->addColumn('ruta_destroy', function($ciudad){
            return route("admin.administracion.ciudad.destroy",[$ciudad->ABC12_id]);
        })
        ->addColumn('inactivo', function ($ciudad) {
            return is_null($ciudad->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
