<?php

namespace Modules\Administracion\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateComunaRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC13_nombre' => 'required',
            'ABC11_id' => 'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
