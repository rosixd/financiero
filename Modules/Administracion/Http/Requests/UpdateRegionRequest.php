<?php

namespace Modules\Administracion\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRegionRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC11_nombre' => 'required',
            'ABC11_numero_romano' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
