<?php

namespace Modules\Administracion\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCiudadRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC12_nombre' => 'required',
            'ABC13_id' => 'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
