<?php

use Illuminate\Routing\Router;
use Modules\Administracion\Entities\Region;
use Modules\Administracion\Entities\Ciudad;
use Modules\Administracion\Entities\Comuna;
/** @var Router $router */

$router->group(['prefix' =>'/administracion'], function (Router $router) {

    $router->bind('region', function ($id) {
        $repo = app('Modules\Administracion\Repositories\RegionRepository')->find($id); 
        return is_null($repo)? Region::withTrashed()->find($id):$repo; 
    });

    $router->get('regions', [
        'as' => 'admin.administracion.region.index',
        'uses' => 'RegionController@index',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'region',
        'datatable' => 'admin.administracion.region.datatable',
        'log' => 'admin.user.user.logdatatable',
        'middleware' => 'can:administracion.regions.index',
    ]);
    $router->get('regions/create', [
        'as' => 'admin.administracion.region.create',
        'uses' => 'RegionController@create',
        'script' => 'AppAdministracion',
        'view' => 'create',
        'modulo' => 'administracion',        
        'submodulo' => 'region',
        'datatable' => 'admin.administracion.region.datatable',
        'middleware' => 'can:administracion.regions.create',
    ]);
    $router->post('regions', [
        'as' => 'admin.administracion.region.store',
        'middleware' => 'can:administracion.regions.create',
        'uses' => 'RegionController@store',
    ]);
    $router->get('regions/{region}/edit', [
        'as' => 'admin.administracion.region.edit',
        'uses' => 'RegionController@edit',
        'script' => 'AppAdministracion',
        'view' => 'edit',
        'modulo' => 'administracion',
        'submodulo' => 'region',
        'middleware' => 'can:administracion.regions.edit',
        'datatable' => 'admin.administracion.region.datatable'
    ]);
    $router->put('regions/{region}', [
        'as' => 'admin.administracion.region.update',
        'middleware' => 'can:administracion.regions.edit',
        'uses' => 'RegionController@update',
    ]);
    $router->get('regions/destroy/{region}', [
        'as' => 'admin.administracion.region.destroy',
        'middleware' => 'can:administracion.regions.destroy',
        'uses' => 'RegionController@destroy',
    ]);

    $router->bind('ciudad', function ($id) {
        $repo = app('Modules\Administracion\Repositories\CiudadRepository')->find($id);
        return is_null($repo)? Ciudad::withTrashed()->find($id):$repo;
    });
    $router->get('ciudads', [
        'as' => 'admin.administracion.ciudad.index',
        'uses' => 'CiudadController@index',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'ciudad',
        'datatable' => 'admin.administracion.ciudad.datatable',
        'middleware' => 'can:administracion.ciudad.index',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('ciudads/create', [
        'as' => 'admin.administracion.ciudad.create',
        'uses' => 'CiudadController@create',
        'script' => 'AppAdministracion',
        'view' => 'create',
        'modulo' => 'administracion',
        'submodulo' => 'ciudad',
        'middleware' => 'can:administracion.ciudad.create',
        'datatable' => 'admin.administracion.ciudad.datatable'
    ]);
    $router->post('ciudads', [
        'as' => 'admin.administracion.ciudad.store',
        'middleware' => 'can:administracion.ciudad.create',
        'uses' => 'CiudadController@store',
    ]);
    $router->get('ciudads/{ciudad}/edit', [
        'as' => 'admin.administracion.ciudad.edit',
        'uses' => 'CiudadController@edit',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'ciudad',
        'middleware' => 'can:administracion.ciudad.edit',
        'datatable' => 'admin.administracion.ciudad.datatable'
    ]);
    $router->put('ciudads/{ciudad}', [
        'as' => 'admin.administracion.ciudad.update',
        'middleware' => 'can:administracion.ciudad.edit',
        'uses' => 'CiudadController@update',
    ]);
    $router->get('ciudads/destroy/{ciudad}', [
        'as' => 'admin.administracion.ciudad.destroy',
        'middleware' => 'can:administracion.ciudad.destroy',
        'uses' => 'CiudadController@destroy',
    ]);

    $router->bind('comuna', function ($id) {
        $repo = app('Modules\Administracion\Repositories\ComunaRepository')->find($id);
        return is_null($repo)? Comuna::withTrashed()->find($id):$repo;
    });
    $router->get('comunas', [
        'as' => 'admin.administracion.comuna.index',
        'uses' => 'ComunaController@index',
        'script' => 'AppAdministracion',
        'view' => 'index',
        'modulo' => 'administracion',
        'submodulo' => 'comuna',
        'middleware' => 'can:administracion.ciudad.index',
        'datatable' => 'admin.administracion.comuna.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('comunas/create', [
        'as' => 'admin.administracion.comuna.create',
        'uses' => 'ComunaController@create',
        'script' => 'AppAdministracion',
        'view' => 'create',
        'modulo' => 'administracion',
        'submodulo' => 'comuna',
        'middleware' => 'can:administracion.ciudad.create',
        'datatable' => 'admin.administracion.comuna.datatable'
    ]);
    $router->post('comunas', [
        'as' => 'admin.administracion.comuna.store',
        'middleware' => 'can:administracion.ciudad.create',
        'uses' => 'ComunaController@store',
    ]);
    $router->get('comunas/{comuna}/edit', [
        'as' => 'admin.administracion.comuna.edit',
        'uses' => 'ComunaController@edit',
        'middleware' => 'can:administracion.ciudad.edit',
        'script' => 'AppAdministracion',
        'view' => 'edit',
        'modulo' => 'administracion',
        'submodulo' => 'ciudad',
        'datatable' => 'admin.administracion.comuna.datatable'
    ]);
    $router->put('comunas/{comuna}', [
        'as' => 'admin.administracion.comuna.update',
        'middleware' => 'can:administracion.ciudad.edit',
        'uses' => 'ComunaController@update',
    ]);
    $router->get('comunas/destroy/{comuna}', [
        'as' => 'admin.administracion.comuna.destroy',
        'middleware' => 'can:administracion.ciudad.destroy',
        'uses' => 'ComunaController@destroy',
    ]);
// append
    $router->get('regions/datatable', [
        'as' => "admin.administracion.region.datatable",
        'middleware' => 'can:administracion.region.index',
        "uses" => "RegionController@datatable",
    ]);
    $router->get('ciudads/datatable', [
        'as' => "admin.administracion.ciudad.datatable",
        'middleware' => 'can:administracion.ciudad.index',
        "uses" => "CiudadController@datatable",
    ]);
    $router->get('comunas/datatable', [
        'as' => "admin.administracion.comuna.datatable",
        'middleware' => 'can:administracion.comuna.index',
        "uses" => "ComunaController@datatable",
    ]);
});
