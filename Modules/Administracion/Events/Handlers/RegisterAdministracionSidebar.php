<?php

namespace Modules\Administracion\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterAdministracionSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        if( 
            $this->auth->hasAccess('user.users.index') || 
            $this->auth->hasAccess('user.roles.index') 
        ){
            $menu->group(trans('core::sidebar.content'), function (Group $group) {
                $group->item(trans('administracion::administracions.title.administracions'), function (Item $item) {
                    $item->icon('fa fa-gear');
                    $item->weight(10);
                    // $item->item(trans('administracion::regions.title.regions'), function (Item $item) {
                    //    $item->icon('fa fa-circle-o');
                    //     $item->weight(0);
                    //     $item->append('admin.administracion.region.create');
                    //     $item->route('admin.administracion.region.index');
                    // });
                    // $item->item(trans('administracion::comunas.title.comunas'), function (Item $item) {
                    //     $item->icon('fa fa-copy');
                    //     $item->weight(0);
                    //     $item->append('admin.administracion.comuna.create');
                    //     $item->route('admin.administracion.comuna.index');
                    // });
                    // $item->item(trans('administracion::ciudads.title.ciudads'), function (Item $item) {
                    //     $item->icon('fa fa-copy');
                    //     $item->weight(0);
                    //     $item->append('admin.administracion.ciudad.create');
                    //     $item->route('admin.administracion.ciudad.index');
                    // });                
    
                    // author: Ruben Morales.
                    // Se traslada menus de usuarios a modulo de administración.
    
                    $item->item(trans('user::users.permisos.name'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-users');
                        $item->route('admin.user.user.index');
                        $item->authorize(
                            $this->auth->hasAccess('user.users.index')
                        );
                    });
    
                    $item->item(trans('user::roles.permisos.name'), function (Item $item) {
                        $item->weight(1);
                        $item->icon('fa fa-flag-o');
                        $item->route('admin.user.role.index');
                        $item->authorize(
                            $this->auth->hasAccess('user.roles.index')
                        );
                    });
                });
            });
        }        

        return $menu;
    }
}
