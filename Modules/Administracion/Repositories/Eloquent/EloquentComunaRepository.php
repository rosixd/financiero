<?php

namespace Modules\Administracion\Repositories\Eloquent;

use Modules\Administracion\Repositories\ComunaRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentComunaRepository extends EloquentBaseRepository implements ComunaRepository
{
}
