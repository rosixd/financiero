<?php

namespace Modules\Administracion\Repositories\Eloquent;

use Modules\Administracion\Repositories\RegionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRegionRepository extends EloquentBaseRepository implements RegionRepository
{
}
