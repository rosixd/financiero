<?php

namespace Modules\Administracion\Repositories\Eloquent;

use Modules\Administracion\Repositories\CiudadRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCiudadRepository extends EloquentBaseRepository implements CiudadRepository
{
}
