<?php

namespace Modules\Administracion\Repositories\Cache;

use Modules\Administracion\Repositories\RegionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRegionDecorator extends BaseCacheDecorator implements RegionRepository
{
    public function __construct(RegionRepository $region)
    {
        parent::__construct();
        $this->entityName = 'administracion.regions';
        $this->repository = $region;
    }
}
