<?php

namespace Modules\Administracion\Repositories\Cache;

use Modules\Administracion\Repositories\ComunaRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheComunaDecorator extends BaseCacheDecorator implements ComunaRepository
{
    public function __construct(ComunaRepository $comuna)
    {
        parent::__construct();
        $this->entityName = 'administracion.comunas';
        $this->repository = $comuna;
    }
}
