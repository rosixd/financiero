<?php

namespace Modules\Administracion\Repositories\Cache;

use Modules\Administracion\Repositories\CiudadRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCiudadDecorator extends BaseCacheDecorator implements CiudadRepository
{
    public function __construct(CiudadRepository $ciudad)
    {
        parent::__construct();
        $this->entityName = 'administracion.ciudads';
        $this->repository = $ciudad;
    }
}
