<?php

namespace Modules\Administracion\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Administracion\Events\Handlers\RegisterAdministracionSidebar;

class AdministracionServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterAdministracionSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('regions', array_dot(trans('administracion::regions')));
            $event->load('ciudads', array_dot(trans('administracion::ciudads')));
            $event->load('comunas', array_dot(trans('administracion::comunas')));
            // append translations



        });
    }

    public function boot()
    {
        $this->publishConfig('administracion', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Administracion\Repositories\RegionRepository',
            function () {
                $repository = new \Modules\Administracion\Repositories\Eloquent\EloquentRegionRepository(new \Modules\Administracion\Entities\Region());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Administracion\Repositories\Cache\CacheRegionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Administracion\Repositories\CiudadRepository',
            function () {
                $repository = new \Modules\Administracion\Repositories\Eloquent\EloquentCiudadRepository(new \Modules\Administracion\Entities\Ciudad());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Administracion\Repositories\Cache\CacheCiudadDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Administracion\Repositories\ComunaRepository',
            function () {
                $repository = new \Modules\Administracion\Repositories\Eloquent\EloquentComunaRepository(new \Modules\Administracion\Entities\Comuna());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Administracion\Repositories\Cache\CacheComunaDecorator($repository);
            }
        );
// add bindings



    }
}
