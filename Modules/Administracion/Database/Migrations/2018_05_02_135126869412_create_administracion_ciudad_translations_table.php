<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministracionCiudadTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administracion__ciudad_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('ciudad_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['ciudad_id', 'locale']);
            $table->foreign('ciudad_id')->references('id')->on('administracion__ciudads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administracion__ciudad_translations', function (Blueprint $table) {
            $table->dropForeign(['ciudad_id']);
        });
        Schema::dropIfExists('administracion__ciudad_translations');
    }
}
