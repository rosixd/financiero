<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministracionComunaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administracion__comuna_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('comuna_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['comuna_id', 'locale']);
            $table->foreign('comuna_id')->references('id')->on('administracion__comunas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administracion__comuna_translations', function (Blueprint $table) {
            $table->dropForeign(['comuna_id']);
        });
        Schema::dropIfExists('administracion__comuna_translations');
    }
}
