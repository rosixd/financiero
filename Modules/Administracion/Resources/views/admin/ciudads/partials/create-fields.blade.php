<div class="box-body">
	<div class="form-group">
		<label>{{ trans('administracion::ciudads.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC12_nombre" id="ABC12_nombre">
		<span class="text-danger">
			{{ $errors->has('ABC12_nombre') ? trans('administracion::ciudads.validation.name') : '' }}
		</span>
	</div>
    <div class="form-group">
    	<label>{{ trans('administracion::ciudads.form.comuna') }}</label>
        <select class="form-control" name="ABC13_id">
            @foreach ($comuna as $index => $comu)
            <option value="{{$comu->ABC13_id}}">{{$comu->ABC13_nombre}}</option>
            @endforeach
        </select>
        <span class="text-danger">
            {{ $errors->has('ABC13_id') ? trans('administracion::ciudads.validation.comuna') : '' }}
        </span>
    </div>
</div>