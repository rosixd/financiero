@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('administracion::regions.title.create region') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.administracion.region.index') }}">{{ trans('administracion::regions.title.regions') }}</a></li>
        <li class="active">{{ trans('administracion::regions.title.create region') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.administracion.region.store'], 'method' => 'post', 'class' => 'formValida']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('administracion::admin.regions.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-md">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-md" href="{{ route('admin.administracion.region.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop