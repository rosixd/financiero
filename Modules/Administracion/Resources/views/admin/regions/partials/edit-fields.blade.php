<div class="box-body">
	<div class="form-group">
		<label>{{ trans('administracion::regions.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC11_nombre" id="ABC11_nombre" value="{{ $region->ABC11_nombre }}">
		<span class="text-danger">
			{{ $errors->has('ABC11_nombre') ? trans('administracion::regions.validation.name') : '' }}
		</span>
	</div>
	<div class="form-group">
		<label>{{ trans('administracion::regions.form.prefijo') }}</label>
		<input type="text" class="form-control soloLetras" maxlength="4" name="ABC11_numero_romano" id="ABC11_numero_romano" value="{{ $region->ABC11_numero_romano }}">
		<span class="text-danger">
			{{ $errors->has('ABC11_numero_romano') ? trans('administracion::regions.validation.prefijo') : '' }}
		</span>
	</div>
	<div class="form-group">
		<label>{{ trans('administracion::regions.form.orden') }}</label>
		<input type="text" class="form-control soloNumeros" maxlength="4" name="ABC11_orden" id="ABC11_orden" value="{{ $region->ABC11_orden }}">
		<span class="text-danger">
			{{ $errors->has('ABC11_orden') ? trans('administracion::regions.validation.orden') : '' }}
		</span>
	</div>
</div>