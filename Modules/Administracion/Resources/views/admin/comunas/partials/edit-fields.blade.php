<div class="box-body">
	<div class="form-group">
		<label>{{ trans('administracion::comunas.form.name') }}</label>
		<input type="text" class="form-control soloLetras" name="ABC13_nombre" id="ABC13_nombre" value="{{ $comuna->ABC13_nombre }}">
		<span class="text-danger">
			{{ $errors->has('ABC13_nombre') ? trans('administracion::comunas.validation.name') : '' }}
		</span>
	</div>
    <div class="form-group">
    	<label>{{ trans('administracion::comunas.form.region') }}</label>
        <select class="form-control" name="ABC11_id">
            @foreach ($region as $index => $reg)
            <option value="{{$reg->ABC11_id}}" {{ ($comuna->ABC11_id == $reg->ABC11_id) ? 'selected' : '' }}>{{$reg->ABC11_nombre}}</option>
            @endforeach
        </select>
        <span class="text-danger">
            {{ $errors->has('ABC11_id') ? trans('administracion::comunas.validation.region') : '' }}
        </span>
    </div>
</div>