<?php

return [
    'list resource' => 'Consultar Regi&oacute;n',
    'create resource' => 'Crear Regi&oacute;n',
    'edit resource' => 'Editar Regi&oacute;n',
    'destroy resource' => 'Eliminar Regi&oacute;n',
    'title' => [
        'regions' => 'Regi&oacute;n',
        'create region' => 'Crear regi&oacute;n',
        'edit region' => 'Editar regi&oacute;n',
    ],
    'button' => [
        'create region' => 'Crear regi&oacute;n',
    ],
    'table' => [
        'name' =>'Nombre',
        'prefijo' =>'Prefijo',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'prefijo' =>'Prefijo',
        'orden' =>'Orden',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource deleted' => ':name eliminada &eacute;xitosamente.',
        'resource desactivate' => 'Sr. Usuario, la regi&oacute;n  se ha desactivado correctamente.',
        'resource activate' => 'Sr. Usuario, la regi&oacute;n  se ha activado correctamente.',
        'resource asignado' => 'Sr. Usuario, la regi&oacute;n tiene comunas asignadas.',
    ],
    
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'prefijo' => 'Sr. Usuario, debe ingresar el prefijo.',
    ],
];
