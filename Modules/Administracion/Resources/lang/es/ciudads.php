<?php

return [
    'list resource' => 'Consultar Ciudades',
    'create resource' => 'Crear Ciudades',
    'edit resource' => 'Editar Ciudades',
    'destroy resource' => 'Eliminar Ciudades',
    'title' => [
        'ciudads' => 'Ciudades',
        'create ciudad' => 'Crear ciudad',
        'edit ciudad' => 'Editar ciudad',
    ],
    'button' => [
        'create ciudad' => 'Crear ciudad',
    ],
    'table' => [
        'name' =>'Nombre',
        'comuna' =>'Comuna',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'comuna' =>'Comuna'
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la ciudad se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,ciudad no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource deleted' => 'Sr. Usuario, ciudad eliminada &eacute;xitosamente.',
        'resource desactivate' => 'Sr. Usuario, el  ciudad se ha desactivado correctamente.',
        'resource activate' => 'Sr. Usuario, el ciudad se ha activado correctamente.',
        'resource comunainactiva' => 'Sr. Usuario, el la comuna asociada est&aacute; inactiva.'
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'comuna' => 'Sr. Usuario, debe seleccionar la comuna.',
    ],
];
