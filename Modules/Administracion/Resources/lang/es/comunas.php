<?php

return [
    'list resource' => 'Consultar Comunas',
    'create resource' => 'Crear Comunas',
    'edit resource' => 'Editar Comunas',
    'destroy resource' => 'Eliminar Comunas',
    'title' => [
        'comunas' => 'Comunas',
        'create comuna' => 'Crear comuna',
        'edit comuna' => 'Editar comuna',
    ],
    'button' => [
        'create comuna' => 'Crear comuna',
    ],
    'table' => [
        'name' =>'Nombre',
        'region' =>'Regi&oacute;n',
        'created at' =>'Fecha de Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'region' =>'Regi&oacute;n',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, la :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,la comuna no fue no encontrada.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource deleted' => ':name eliminada &eacute;xitosamente.',
        'resource desactivate' => 'Sr. Usuario, la comuna se ha desactivado correctamente.',
        'resource activate' => 'Sr. Usuario, la comuna se ha activado correctamente.',
        'resource validciudad' => 'Sr. Usuario, el  la comuna tienes ciudad activas asociadas.',
        'resource regioninactiva' => 'Sr. Usuario, el  la regi&oacute;n asoiada est&aacute; inactiva.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'region' => 'Sr. Usuario, debe seleccionar la regi&oacute;n.',
    ],
];
