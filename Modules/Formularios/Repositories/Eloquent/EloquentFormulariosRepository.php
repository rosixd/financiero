<?php

namespace Modules\Formularios\Repositories\Eloquent;

use Modules\Formularios\Repositories\FormulariosRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFormulariosRepository extends EloquentBaseRepository implements FormulariosRepository
{
}
