<?php

namespace Modules\Formularios\Repositories\Eloquent;

use Modules\Formularios\Repositories\RespuestasRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRespuestasRepository extends EloquentBaseRepository implements RespuestasRepository
{
}
