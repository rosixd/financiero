<?php

namespace Modules\Formularios\Repositories\Cache;

use Modules\Formularios\Repositories\RespuestasRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRespuestasDecorator extends BaseCacheDecorator implements RespuestasRepository
{
    public function __construct(RespuestasRepository $respuestas)
    {
        parent::__construct();
        $this->entityName = 'formularios.respuestas';
        $this->repository = $respuestas;
    }
}
