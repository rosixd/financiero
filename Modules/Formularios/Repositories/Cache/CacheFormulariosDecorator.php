<?php

namespace Modules\Formularios\Repositories\Cache;

use Modules\Formularios\Repositories\FormulariosRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFormulariosDecorator extends BaseCacheDecorator implements FormulariosRepository
{
    public function __construct(FormulariosRepository $formularios)
    {
        parent::__construct();
        $this->entityName = 'formularios.formularios';
        $this->repository = $formularios;
    }
}
