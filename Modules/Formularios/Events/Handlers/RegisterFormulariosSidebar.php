<?php

namespace Modules\Formularios\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterFormulariosSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
      
        if( 
            $this->auth->hasAccess('formularios.formularios.index') or $this->auth->hasAccess('formularios.SolicitudTarjeta.index2')
        ){
		 $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('formularios::reportes.title.reportes'), function (Item $item) {
                $item->icon('fa fa-list-alt');
                $item->weight(10);
                if($this->auth->hasAccess('formularios.formularios.index') ){

                    $item->item(trans('formularios::FormularioContacto.title.contacto'), function (Item $item) {
                        $item->icon('fa fa-circle-o');
                        $item->weight(0);
                        //$item->append('admin.formularios.formularios.create');
                        $item->route('admin.formularios.reporte.contacto');
                    }); 
                }

                if($this->auth->hasAccess('formularios.SolicitudTarjeta.index2')){

                    $item->item(trans('formularios::FormularioSolicitarTarjeta.title.contacto'), function (Item $item) {
                        $item->icon('fa fa-circle-o');
                        $item->weight(0);
                        //$item->append('admin.formularios.formularios.create');
                        $item->route('admin.formularios.reporte.SolicitarTarjeta');
                    }); 
                }
              
            });
        }); 
 	}
      

        return $menu;
    }
}
