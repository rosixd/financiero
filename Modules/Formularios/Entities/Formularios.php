<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Formularios extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC07_Formularios';
    protected $primaryKey = 'ABC07_id';

    protected $fillable = [
    	"ABC07_nombre",
        "ABC07_campos",
        "ABC07_slug",
        "ABC07_exportar", 
		"created_at",
    ];
    
    protected $appends = ['key', 'articulo'];
    
    public function getKeyAttribute()
    {
        return "Formulario";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }
}
