<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class FormularioContacto extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC30_FormularioContacto';
    protected $primaryKey = 'ABC30_id';

    protected $fillable = [
        'ABC30_id',
        'ABC30_nombre_formulario',
        'ABC30_nombrecompleto',
        'ABC30_rut',
        'ABC30_telefono',
		'ABC30_nombre_formulario_slug',
        'ABC30_email',
        'ABC30_tiposeguro',
        'ABC30_tiposolicitud',
        'ABC30_mensaje',
        'ABC30_fecha_creacion'
    ];
    
   protected $appends = ['tiposeguro_str'];
  
  /*  
    public function getKeyAttribute()
    {
        return "Formulario";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }
 */
    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }

    public function getTiposeguroStrAttribute($value){
        $tipoSeguro = isset($this->attributes['ABC30_tiposeguro']) ? 
                        $this->attributes['ABC30_tiposeguro'] : 
                        (isset($this->attributes['Codigo de seguro']) ? $this->attributes['Codigo de seguro'] : '');

        switch ($tipoSeguro) {
            case '01':
                return "Seguro Vida Avance";
            case '02':
                return "Seguro Catastrófico familiar";
            case '03':
                return "Seguro Salud Dental";
            case '04':
                return "Seguro Escolaridad";
            case '05':
                return "Seguro Vida";
            case '06':
                return "Seguro Cesantía Cuentas Básicas";
            case '07':
                return "Seguro Contra robo con bonificación";
            case '08':
                return "Seguro Hogar";
            default:
                return "";
        }
    }

    /**
     * Agrega filtro a query para devolver registros no vacios
     * 
     * @param QueryBuilder
     * @return Querybuilder
     */
    public function scopeSinVacios( $query ){
        return $query
            ->whereRaw(
                ' TRIM(ABC30_nombre_formulario) <> ""  AND ' .
                ' TRIM(ABC30_nombrecompleto) <> "" AND ' .
                ' TRIM(ABC30_rut) <> "" AND ' .
                ' TRIM(ABC30_telefono) <> "" AND ' .
                ' TRIM(ABC30_nombre_formulario_slug) <> "" AND ' .
                ' TRIM(ABC30_email) <> "" AND ' .
                ' TRIM(ABC30_tiposeguro) <> "" AND ' .
                ' TRIM(ABC30_tiposolicitud) <> "" '
            );
    }
}
