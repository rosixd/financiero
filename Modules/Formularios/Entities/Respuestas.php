<?php

namespace Modules\Formularios\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
    use Translatable;

    protected $table = 'formularios__respuestas';
    public $translatedAttributes = [];
    protected $fillable = [];
}
