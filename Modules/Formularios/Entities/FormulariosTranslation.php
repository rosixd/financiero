<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\Model;

class FormulariosTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'formularios__formularios_translations';
}
