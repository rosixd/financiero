<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\Model;

class RespuestasTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'formularios__respuestas_translations';
}
