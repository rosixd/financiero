<?php

namespace Modules\Formularios\Entities;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class SolicitudTarjeta extends Model
{
    protected $table = 'ABC31_SolicitudTarjeta';

    protected $primaryKey = 'id';
    //protected $primaryKey = 'ABC31_id';

    /* protected $fillable = [
        'ABC31_Rut',
        'ABC31_RetornoFuncion',
        'ABC31_Comentario',
        'ABC31_TOKEN',
        'ABC31_Equifax',
        'ABC31_Previred',
        'ABC31_Serie',
        'ABC31_PrimerNombre',
        'ABC31_SegundoNombre',
        'ABC31_ApellidoPaterno',
        'ABC31_ApellidoMaterno',
        'ABC31_FechaNacimiento',
        'ABC31_Correo',
        'ABC31_TelefonoMovil',
        'ABC31_TelefonoFijo',
        'ABC31_Sexo',
        'ABC31_Hijos',
        'ABC31_Nacionalidad',
        'ABC31_Region',
        'ABC31_Comuna',
        'ABC31_Calle',
        'ABC31_Numero',
        'ABC31_DetalleDireccion',
        'ABC31_TipoVivienda',
        'ABC31_FechaOcupacionVivienda',
        'ABC31_CondicionHabitacional',
        'ABC31_ArriendoDividendo',
        'ABC31_AceptacionTerminos',
        'ABC31_EstadoCivil',
        'ABC31_Actividad',
        'ABC31_FechaIngrosoTrabajo',
        'ABC31_Patente',
        'ABC31_TienePropiedad',
        'ABC31_codRetorno' ,
        'ABC31_CodRechazo',
        'ABC31_descrRechazo',
        'ABC31_RiskStrategyDecision'
    ]; */
    protected $fillable = [
        'crea_registro',
        'rut',
        'dv',
        'escliente',
        'escliente_desc',
        'serie',
        'primer_nombre',
        'segundo_nombre',
        'apepat',
        'apemat',
        'cod_sexo',
        'desc_sexo',
        'fecnac',
        'desc_nacionalidad',
        'cod_region',
        'nom_region',
        'cod_comuna',
        'nom_comuna',
        'calle',
        'numcalle',
        'detdir',
        'cod_tipovi',
        'desc_tipovi',
        'fecvivienda',
        'cod_vivienda',
        'desc_vivienda',
        'pagomensual',
        'email',
        'fono_movil',
        'fono_fijo',
        'acepta_termycond',
        'cod_estado',
        'desc_estado',
        'cod_profe',
        'profesion',
        'q_hijos',
        'fec_ingreso',
        'patente',
        'cod_propiedad',
        'desc_propiedad',
        'responde_preguntas',
        'respuestas_correctas',
        'horario_previred',
        'evaluacion_codretorno',
        'evaluacion_codrechazo',
        'evaluacion_desrechazo',
        'act_registro',
        'ABC31_TOKEN',
        'codigo_previred',
    ];

    public function getPagomensualAttribute($value)
    {
        return number_format($value, 0, ',', '.');
    }
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
