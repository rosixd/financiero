<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;
class Controltxthistorial extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC27_Control_txt_historial';
    protected $primaryKey = 'ABC27_id';
    protected $fillable = [
    	"ABC26_id",
        "ABC27_descripcion",
    ];
    
    protected $appends = ['key', 'articulo'];
    
    public function getKeyAttribute()
    {
        return "Formulario";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
