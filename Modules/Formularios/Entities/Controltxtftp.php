<?php

namespace Modules\Formularios\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Controltxtftp extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC26_Control_txt_ftp';
    protected $primaryKey = 'ABC26_id';
    protected $fillable = [
    	"ABC26_nombre_archivo",
        "ABC26_formulario",
        "ABC26_estado", 
        "ABC26_ruta_archivo",
        "ABC26_iteraciones" 
       
    ];
    protected $appends = ['name','id','key','articulo'];

    
    public function getKeyAttribute()
    {
        return "";
    }
    public function getArticuloAttribute()
    {
        return "";
    }

    public function getNameAttribute()
    {
        return "";
    }

    public function getIdAttribute()
    {
        return $this->ABC26_id;
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}
