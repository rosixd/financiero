@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('formularios::formularios.title.create formularios') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.formularios.formularios.index') }}">{{ trans('formularios::formularios.title.formularios') }}</a></li>
        <li class="active">{{ trans('formularios::formularios.title.create formularios') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.formularios.formularios.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class= "col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            @include('partials.form-tab-headers')
                            <div class="tab-content">
                                <?php $i = 0; ?>
                                @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                                    <?php $i++; ?>
                                    <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}" style="overflow: hidden;">
                                        @include('formularios::admin.formularios.partials.fields', ['lang' => $locale])
                                    </div>
                                @endforeach
                            </div>
                        </div> {{-- end nav-tabs-custom --}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            @include('partials.form-tab-headers')
                            <div class="tab-content">
                                <?php $i = 0; ?>
                                @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                                    <?php $i++; ?>
                                    <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                                        @include('formularios::admin.formularios.partials.propiedades', ['lang' => $locale])
                                    </div>
                                @endforeach
                            </div>
                        </div> {{-- end nav-tabs-custom --}}
                    </div>
                </div>
            </div> 
            <div class= "col-md-9">           
                <div class="nav-tabs-custom">
                    @include('partials.form-tab-headers')
                    <div class="tab-content">
                        <?php $i = 0; ?>
                        @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                            <?php $i++; ?>
                            <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                                @include('formularios::admin.formularios.partials.create-fields', ['lang' => $locale])
                            </div>
                        @endforeach

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                            <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.formularios.formularios.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                        </div>
                    </div>
                </div> {{-- end nav-tabs-custom --}}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop
@push('js-stack')
    <script src="{{ Module::asset('formulario:js/camposDragDrop.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            urlEliminar = '{{url("modules/formulario/img/campos/eliminar.PNG")}}';
        });
    </script>
@endpush