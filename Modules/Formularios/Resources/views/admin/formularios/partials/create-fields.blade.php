<div class="box-body">
    <div class="col-md-12">
        <div class="form-group">
            <label>{{ trans('formularios::formularios.form.name') }}</label>
            <input type="text" class="form-control requerido" name="ABC07_nombre" id="ABC07_nombre">
            <span class="text-danger" >
                {{ $errors->has('ABC07_nombre') ? trans('formularios::formularios.validation.name') : '' }}
            </span>
        </div>
    </div>
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a id="visor_diseno" data-toggle="tab" href="#contenedor_formulario">{{ trans('formularios::formularios.form.design') }}</a>
            </li>
            <li>
                <a id="visor_codigo" data-toggle="tab" href="#contenedor_formulario_codigo">{{ trans('formularios::formularios.form.code') }}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="contenedor_formulario" class="tab-pane fade in active" ondrop="drop(event)" ondragover="allowDrop(event)" >
            </div>
	            {{-- <span class="text-danger" >
                    {{ $errors->has('ABC07_campos') ? trans('formularios::formularios.validation.design') : '' }}
                </span> --}}
            <div id="contenedor_formulario_codigo" class="tab-pane fade" >
                <textarea id="ABC07_campos" cols="185" rows="17" name="ABC07_campos" onchange="cambioCodigo()" class="requerido"></textarea>
            </div>
            <span class="text-danger" >
                    {{ $errors->has('ABC07_campos') ? trans('formularios::formularios.validation.design') : '' }}
            </span>
        </div>
    </div>
    <div class="col-md-12">
        <div class="emails_active">
            <div class="form-group">
                <br><br>
                <label>{{ trans('formularios::formularios.form.send email') }}</label>
                <input type="checkbox" class="form-control" name="ABC07_activo_email" id="ABC07_activo_email">
            </div>
        </div>
        <div class="emails">
            <div class="form-group">
                <label>{{ trans('formularios::formularios.form.subject') }}</label>
                <input type="text" class="form-control" name="ABC07_subject" id="ABC07_subject">
                <span class="text-danger" >
                    {{ $errors->has('ABC07_subject') ? trans('formularios::formularios.validation.subject') : '' }}
                </span>
            </div>

            <div class="form-group">
                <label>{{ trans('formularios::formularios.form.to') }}</label>
                <input type="text" class="form-control" name="ABC07_to" id="ABC07_to">
                <span class="text-danger" >
                    {{ $errors->has('ABC07_subject') ? trans('formularios::formularios.validation.to') : '' }}
                </span>
            </div>

            <div class="form-group">
                <label>{{ trans('formularios::formularios.form.cc') }}</label>
                <input type="text" class="form-control" name="ABC07_cc" id="ABC07_cc">
            </div>

            <div class="form-group">
                <label>{{ trans('formularios::formularios.form.cco') }}</label>
                <input type="text" class="form-control" name="ABC07_cco" id="ABC07_cco">
            </div>
        </div>
        <div class="form-group">
            <label>{{ trans('formularios::formularios.form.exportar') }}</label>
            <input type="checkbox" class="form-control" name="ABC07_exportar" id="ABC07_exportar">
        </div>
    </div>
</div>
