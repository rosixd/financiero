<h4>Campos Disponibles</h4>
<div>
    <div class="col-md-3">
        <div class=" camposdisponibles" id="titulo_h1" draggable="true" ondragstart="drag(event)">
            <a rel="titulo">h1</a>
        </div>
    </div>

    <div class="col-md-3">
        <div class="camposdisponibles" id="titulo_h2" draggable="true" ondragstart="drag(event)">
            <a rel="titulo">h2</a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="camposdisponibles" id="titulo_h3" draggable="true" ondragstart="drag(event)">
            <a rel="titulo">h3</a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="camposdisponibles" id="titulo_h4" draggable="true" ondragstart="drag(event)">
            <a rel="titulo">h4</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="input_check" draggable="true" ondragstart="drag(event)">
            <a rel="input check">
                <img src="{{url('modules/formulario/img/campos/input check.png')}}" alt="input check">
                <span>input check</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="input_text" draggable="true" ondragstart="drag(event)">
            <a rel="input text">
                <img src="{{url('modules/formulario/img/campos/input text.png')}}">
                <span>input text</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="select" draggable="true" ondragstart="drag(event)">
            <a rel="select">
                <img src="{{url('modules/formulario/img/campos/select.png')}}" alt="select">
                <span>select</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="input_file" draggable="true" ondragstart="drag(event)">
            <a rel="input file">
                <img src="{{url('modules/formulario/img/campos/input file.png')}}" alt="input file">
                <span>input file</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="input_radio" draggable="true" ondragstart="drag(event)">
            <a rel="input radio">
                <img src="{{url('modules/formulario/img/campos/input radio.png')}}" alt="input radio">
                <span>input radio</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="text_area" draggable="true" ondragstart="drag(event)">
            <a rel="text area">
                <img src="{{url('modules/formulario/img/campos/text area.png')}}" alt="text area">
                <span>text area</span>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="camposdisponibles" id="calendario" draggable="true" ondragstart="drag(event)">
            <a rel="calendario">Calendario
            </a>
        </div>
    </div>
</div>
