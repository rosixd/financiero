@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('formularios::creditodeconsumo.title.creditoconsumo') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('formularios::creditodeconsumo.title.creditoconsumo') }}</li>
    </ol>
@stop

@section('content')
   <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    {{--  <a href="{{ route('admin.formularios.creditodeconsumo.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('formularios::creditodeconsumo.button.create creditodeconsumo') }}
                    </a>  --}}
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th>{{ trans('formularios::creditodeconsumo.table.nombre archivo') }}</th>
                                <th>{{ trans('formularios::creditodeconsumo.table.estado') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            <tfoot>
                            <tr>
                                {{-- <th>{{ trans('formularios::creditodeconsumo.table.nombre archivo') }}</th>
                                <th>{{ trans('formularios::creditodeconsumo.table.estado') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th> --}}
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')  
@stop

@section('footer')
    <div class="modal fade" id="myModal" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog  modal-lg ">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <h5 class="modal-title" id="exampleModalLabel">{{trans('core::core.modal.title log')}}</h5> --}}
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-condensed" id="tableModalLog">
                        <thead>
                            <tr>
                                <th>{{trans('formularios::creditodeconsumo.table.descripcion')}}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            {{-- <th>{{trans('formularios::creditodeconsumo.table.descripcion')}}</th>
                            <th>{{ trans('core::core.table.created at') }}</th> --}}
                        </tr>
                        </tfoot>          
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-xs" data-dismiss="modal" aria-label="Close">
                    {{trans('core::core.button.cancel')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('formularios::creditodeconsumo.title.create creditodeconsumo') }}</dd>
    </dl>
@stop


