@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('formularios::FormularioSolicitarTarjeta.title.contacto') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('formularios::FormularioSolicitarTarjeta.title.contacto') }}</li>
    </ol>
@stop

@section('content')
   <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                            {{--  <a href="{{ route('admin.formularios.FormularioContacto.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                                <i class="fa fa-pencil"></i> {{ trans('formularios::FormularioContacto.button.create FormularioContacto') }}
                            </a>  --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-inline">
			
			    
                            <div class="input-group date" id="datepicker">
			                    <span class="input-group-addon">Fecha</span>
                                <input type="text" class="input-sm form-control" name="start" id="start" />
                            </div>

                            <div class="input-group" >
			    
                                <span class="input-group-addon">RUT</span>
                                <input type="text" class="input-sm form-control" name="rut" id="rut"/>
                            </div>
			    
                            <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Buscar</button>
			    	<span style="display:none; color:red" id="msjrut">RUT INVÁLIDO</span>
                            <button type="button" id="ExportExcel" class="btn btn-sm btn-success" style="float:right;">Exportar Excel</button>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
								<th>{{ trans('formularios::FormularioSolicitarTarjeta.table.rut') }}</th>
								 <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.nombrecompleto') }}</th>
                                <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.apellidocompleto') }}</th>
                                 <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.email') }}</th>       
                                <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.telefono') }}</th>
                                <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.telefonocasa') }}</th>
                         
                                <th>{{ trans('core::core.table.created at') }}</th>
                                {{-- <th data-sortable="false">{{ trans('core::core.table.actions') }}</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            <tfoot>
                            <tr>
                                {{-- <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.nombre archivo') }}</th>
                                <th>{{ trans('formularios::FormularioSolicitarTarjeta.table.estado') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th> --}}
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')  
@stop

@push('js-stack')
    <script>
        var ruta_excel = "{{ route('admin.formularios.reporte.SolicitarTarjeta.excel') }}" 
    </script>
   
@endpush
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('formularios::FormularioSolicitarTarjeta.title.create FormularioSolicitarTarjeta') }}</dd>
    </dl>
@stop


