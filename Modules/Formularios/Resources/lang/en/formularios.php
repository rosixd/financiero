<?php

return [
    'list resource' => 'Consultar Reportes',
    'create resource' => 'Crear Reportes',
    'edit resource' => 'Editar Reportes',
    'destroy resource' => 'Eliminar Reportes',
    'title' => [
        'formularios' => 'Reportes',
        'create formularios' => 'Crear Reportes',
        'edit formularios' => 'Editar Reportes',
    ],
    'button' => [
        'create formularios' => 'Crear Reportes',
    ],
    'table' => [
    	'name' => 'Nombre',
    ],
    'form' => [
        'name' => 'Nombre',
        'design' => 'Dise&ntilde;o',
        'code' => 'C&oacute;digo',
        'send email'=> 'Env&iacute;o por correo',
        'subject'=> 'Asunto',
        'to'=> 'Para',
        'cc'=> 'CC',
        'cco'=> 'CCO',
        'exportar'=> 'Exportar',
    ],
    'messages' => [
        'resource created' => ':name creada &eacute;xitosamente.',
        'resource not found' => ':name no encontrada.',
        'resource updated' => ':name actualizada &eacute;xitosamente.',
	'resource desactivate' => 'Sr. Usuario la campa&ntilde;a se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, la campa&ntilde;a se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, la campa&ntilde;a asignada est&aacute; desactivada.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'design' => 'Sr. Usuario, debe ingresar el dise&ntilde;o o el c&oacute;digo.',
        'code' => 'Sr. Usuario, debe ingresar el C&oacute;digo.',
    ],
];
