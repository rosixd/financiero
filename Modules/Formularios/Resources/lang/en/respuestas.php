<?php

return [
    'list resource' => 'List respuestas',
    'create resource' => 'Create respuestas',
    'edit resource' => 'Edit respuestas',
    'destroy resource' => 'Destroy respuestas',
    'title' => [
        'respuestas' => 'Respuestas',
        'create respuestas' => 'Create a respuestas',
        'edit respuestas' => 'Edit a respuestas',
    ],
    'button' => [
        'create respuestas' => 'Create a respuestas',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
