<?php

return [
    'list resource' => 'Cr&eacute;dito de consumo',
    'create resource' => 'Crear formularios',
    'edit resource' => 'Editar formularios',
    'destroy resource' => 'Eliminar formularios',
    'title' => [
        'creditoconsumo' => 'Cr&eacute;dito de consumo',
    ],
    'button' => [
        //'create formularios' => 'Crear formulario',
    ],
    'table' => [
        'nombre archivo' => 'Nombre archivo',
        'estado' => 'Estatus',
        'descripcion' => 'Descripci&oacute;n',
    ],
    'success' => [
        'ftp' => 'Sr. Usuario, la carga manual se ha realizado con &eacute;xito',
    ],
    'error' => [
        'ftp' => 'Sr. Usuario, se ha generado un error en la carga manual',
    ],
];
