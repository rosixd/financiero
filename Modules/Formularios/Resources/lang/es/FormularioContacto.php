<?php

return [
    'list resource' => 'Consultar contacto',
    'create resource' => 'Crear formularios',
    'edit resource' => 'Editar formularios',
    'destroy resource' => 'Eliminar formularios',
    'title' => [
        'contacto' => 'Reporte de Contacto',
    ],
    'button' => [
        //'create formularios' => 'Crear formulario',
    ],
    'table' => [
        'nombre_formulario'      => 'Formulario',
        'nombrecompleto'         => 'Nombre Completo',
        'rut'                    => 'Rut',
        'telefono'               => 'Teléfono',
        'email'                  => 'Email',
        'tiposeguro'             => 'Tipo de seguro',
        'mensaje'                => 'Mensaje',
        'tiposolicitud'                => 'Tipo solicitud',
    ],
];
