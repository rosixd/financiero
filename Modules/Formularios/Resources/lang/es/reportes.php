<?php

return [
    'list resource' => 'Consultar formularios',
    'create resource' => 'Crear formularios',
    'edit resource' => 'Editar formularios',
    'destroy resource' => 'Eliminar formularios',
    'title' => [
        'reportes' => 'Reportes',
    ],
    'button' => [
        'create formularios' => 'Crear formulario',
    ],
    'table' => [
        'name' => 'Nombre',
    ],
    'form' => [
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'design' => 'Sr. Usuario, debe ingresar el dise&ntilde;o o el c&oacute;digo.',
        'code' => 'Sr. Usuario, debe ingresar el C&oacute;digo.',
    ],

    'permisos' => [
        'name' => 'prueba',
        'user users' => 'Usuarios',
        'user roles' => 'Roles',
    ],
];
