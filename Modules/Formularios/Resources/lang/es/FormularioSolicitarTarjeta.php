<?php

return [
    'list resource' => 'Consultar Solicitud Tarjeta',
    'create resource' => 'Crear formularios',
    'edit resource' => 'Editar formularios',
    'destroy resource' => 'Eliminar formularios',
    'title' => [
        'contacto' => 'Reporte de Solicitud Tarjeta',
    ],
    'button' => [
        //'create formularios' => 'Crear formulario',
    ],
    'table' => [
        'apellidocompleto'       => 'Apellidos',
        'nombrecompleto'         => 'Nombres',
        'rut'                    => 'Rut',
        'telefono'               => 'Teléfono movil',
        'email'                  => 'Email',
        'telefonocasa'           => 'Teléfono fijo',

    ],
    'permisos' => [
        'formularios solicitudtarjeta' => 'prueba',
        'user users' => 'Usuarios',
        'user roles' => 'Roles',
    ],
];
