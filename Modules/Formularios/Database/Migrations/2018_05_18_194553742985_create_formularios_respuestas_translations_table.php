<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulariosRespuestasTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios__respuestas_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('respuestas_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['respuestas_id', 'locale']);
            $table->foreign('respuestas_id')->references('id')->on('formularios__respuestas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formularios__respuestas_translations', function (Blueprint $table) {
            $table->dropForeign(['respuestas_id']);
        });
        Schema::dropIfExists('formularios__respuestas_translations');
    }
}
