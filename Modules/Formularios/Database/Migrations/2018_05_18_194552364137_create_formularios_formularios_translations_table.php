<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulariosFormulariosTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios__formularios_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('formularios_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['formularios_id', 'locale']);
            $table->foreign('formularios_id')->references('id')->on('formularios__formularios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formularios__formularios_translations', function (Blueprint $table) {
            $table->dropForeign(['formularios_id']);
        });
        Schema::dropIfExists('formularios__formularios_translations');
    }
}
