<?php

use Illuminate\Routing\Router;
use Modules\Formularios\Entities\Formularios;

/** @var Router $router */

$router->group(['prefix' =>'/formularios'], function (Router $router) {
   /*  $router->bind('formularios', function ($id) {
        $repo = app('Modules\Formularios\Repositories\FormulariosRepository')->find($id);
        return (is_null($repo))? Formularios::withTrashed()->find($id):$repo;
    });
    $router->get('formularios', [
        'as' => 'admin.formularios.formularios.index',
        'uses' => 'FormulariosController@index',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'formulario',
        'datatable' => 'admin.formularios.datatable',
        'log' => 'admin.user.user.logdatatable'
    ]);
    $router->get('formularios/create', [
        'as' => 'admin.formularios.formularios.create',
        'uses' => 'FormulariosController@create',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'formulario',
        'datatable' => 'admin.formularios.datatable'
    ]);
    $router->post('formularios', [
        'as' => 'admin.formularios.formularios.store',
        'uses' => 'FormulariosController@store',
    ]);
    $router->get('formularios/{formularios}/edit', [
        'as' => 'admin.formularios.formularios.edit',
        'uses' => 'FormulariosController@edit',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'formulario',
        'datatable' => 'admin.formularios.datatable'
    ]);
    $router->put('formularios/{formularios}', [
        'as' => 'admin.formularios.formularios.update',
        'uses' => 'FormulariosController@update',
    ]);
    $router->get('formularios/destroy/{formularios}', [
        'as' => 'admin.formularios.formularios.destroy',
        'uses' => 'FormulariosController@destroy',
    ]);
    $router->bind('respuestas', function ($id) {
        $repo = app('Modules\Formularios\Repositories\RespuestasRepository')->find($id);
        return (is_null($repo))? Formularios::withTrashed()->find($id):$repo;
    });
    $router->get('respuestas', [
        'as' => 'admin.formularios.respuestas.index',
        'uses' => 'RespuestasController@index',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'respuesta',
        'datatable' => 'admin.respuestas.datatable'
    ]);
    $router->get('respuestas/create', [
        'as' => 'admin.formularios.respuestas.create',
        'uses' => 'RespuestasController@create',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'respuesta',
        'datatable' => 'admin.respuestas.datatable'
    ]);
    $router->post('respuestas', [
        'as' => 'admin.formularios.respuestas.store',
        'uses' => 'RespuestasController@store',
    ]);
    $router->get('respuestas/{respuestas}/edit', [
        'as' => 'admin.formularios.respuestas.edit',
        'uses' => 'RespuestasController@edit',
        'script' => 'AppFormulario',
        'view' => 'index',
        'modulo' => 'formulario',
        'submodulo' => 'respuesta',
        'datatable' => 'admin.respuestas.datatable'
    ]);
    $router->put('respuestas/{respuestas}', [
        'as' => 'admin.formularios.respuestas.update',
        'uses' => 'RespuestasController@update',
    ]);
    $router->get('respuestas/destroy/{respuestas}', [
        'as' => 'admin.formularios.respuestas.destroy',
        'uses' => 'RespuestasController@destroy',
    ]);
// append
    $router->get('formularios/datatable', [
        'as' => "admin.formularios.datatable",
        "uses" => "FormulariosController@datatable",
    ]);
    $router->get('respuestas/datatable', [
        'as' => "admin.respuestas.datatable",
        "uses" => "RespuestasController@datatable",
    ]); */

    $router->get('reporte/SolicitarTarjeta', [
        'as' => 'admin.formularios.reporte.SolicitarTarjeta',
        'uses' => 'FormularioSolicitarTarjetaController@index',
        'script' => 'AppFormulario',
        'view' => 'FormularioSolicitarTarjeta',
        'modulo' => 'formulario',
        'submodulo' => 'SolicitarTarjeta',
        'datatable' => 'admin.formularios.reporte.SolicitarTarjeta.datatable',
        'middleware' => 'can:formularios.SolicitudTarjeta.index2',
        'log' => 'admin.formularios.reporte.creditoconsumo.historial'
    ]);
    
    $router->get('reporte/SolicitarTarjeta/datatable', [
        'as' => "admin.formularios.reporte.SolicitarTarjeta.datatable",
        'middleware' => 'can:formularios.SolicitudTarjeta.index2',
        "uses" => "FormularioSolicitarTarjetaController@datatable",
    ]);
    $router->get('reporte/SolicitarTarjeta/excel', [
        'as' => "admin.formularios.reporte.SolicitarTarjeta.excel",
        'middleware' => 'can:formularios.SolicitudTarjeta.index2',
        "uses" => "FormularioSolicitarTarjetaController@excel",
    ]);



    $router->get('reporte/contacto', [
        'as' => 'admin.formularios.reporte.contacto',
        'uses' => 'FormularioContactoController@index',
        'script' => 'AppFormulario',
        'view' => 'FormularioContacto',
        'modulo' => 'formulario',
        'submodulo' => 'FormularioContacto',
        'datatable' => 'admin.formularios.reporte.contacto.datatable',
        'middleware' => 'can:formularios.formularios.index',
        'log' => 'admin.formularios.reporte.creditoconsumo.historial'
    ]);
    
    $router->get('reporte/contacto/datatable', [
        'as' => "admin.formularios.reporte.contacto.datatable",
        'middleware' => 'can:formularios.formularios.index',
        "uses" => "FormularioContactoController@datatable",
    ]);
    $router->get('reporte/contacto/excel', [
        'as' => "admin.formularios.reporte.contacto.excel",
        'middleware' => 'can:formularios.formularios.index',
        "uses" => "FormularioContactoController@excel",
    ]);


    $router->get('reporte/creditoconsumo', [
        'as' => 'admin.formularios.reporte.creditoconsumo',
        'uses' => 'CreditosConsumosController@index',
        'script' => 'AppFormulario',
        'view' => 'creditoconsumo',
        'modulo' => 'formulario',
        'submodulo' => 'creditoconsumo',
        'datatable' => 'admin.formularios.reporte.creditoconsumo.datatable',
        'middleware' => 'can:formularios.formularios.index',
        'log' => 'admin.formularios.reporte.creditoconsumo.historial'
    ]);
    $router->get('reporte/creditoconsumo/datatable', [
        'as' => "admin.formularios.reporte.creditoconsumo.datatable",
        'middleware' => 'can:formularios.formularios.index',
        "uses" => "CreditosConsumosController@datatable",
    ]);
	 $router->get('reporte/creditoconsumo/historial', [
        'as' => "admin.formularios.reporte.creditoconsumo.historial",
        'middleware' => 'can:formularios.formularios.index',
        "uses" => "CreditosConsumosController@historial",
    ]);
	 $router->get('reporte/creditoconsumo/reenviar/{id}', [
        'as' => "admin.formularios.reporte.creditoconsumo.reenviar",
        'middleware' => 'can:formularios.formularios.index',
        "uses" => "CreditosConsumosController@reenviar",
    ]);
});
