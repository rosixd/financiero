<?php

namespace Modules\Formularios\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateFormulariosRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'ABC07_nombre' => 'required',
            'ABC07_campos' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
