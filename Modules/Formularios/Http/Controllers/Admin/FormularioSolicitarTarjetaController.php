<?php

namespace Modules\Formularios\Http\Controllers\Admin;

use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Formularios\Entities\Formularios;
use Modules\Formularios\Entities\SolicitudTarjeta;
use Modules\Formularios\Repositories\FormulariosRepository;
use Yajra\Datatables\Datatables;
use Config;
use Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use DB;
class FormularioSolicitarTarjetaController extends AdminBaseController
{
    /**
     * @var FormulariosRepository
     */
    private $formularios;

    protected $form = 'prueba';

    public function __construct(FormulariosRepository $formularios)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$formularios = $this->formularios->all();
        return view('formularios::admin.reporte.FormularioSolicitarTarjeta');
    }

    public function excel(Request $request)
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(0);
        $inicio = $request->fecha;
	
        $a = explode("/", $inicio);
	if($inicio != "" ){
	
	  	$inicio = trim($a[0]) . " 00:00:00";
        	$fin = trim($a[1]) . " 23:59:59";

        	$formato = 'd-m-Y H:i:s';

        	$inicio = Carbon::createFromFormat($formato, $inicio);
        	$fin = Carbon::createFromFormat($formato, $fin);
	
	}
      
        
        $columnas = [
            'crea_registro',
            'rut',
            'dv',
            'escliente',
            'escliente_desc',
            'serie',
            'primer_nombre',
            'segundo_nombre',
            'apepat',
            'apemat',
            'cod_sexo',
            'desc_sexo',
            'fecnac',
            'desc_nacionalidad',
            'cod_region',
            'nom_region',
            'cod_comuna',
            'nom_comuna',
            'calle',
            'numcalle',
            'detdir',
            'cod_tipovi',
            'desc_tipovi',
            'fecvivienda',
            'cod_vivienda',
            'desc_vivienda',
            'pagomensual',
            'email',
            'fono_movil',
            'fono_fijo',
            'acepta_termycond',
            'cod_estado',
            'desc_estado',
            'cod_profe',
            'profesion',
            'q_hijos',
            'fec_ingreso',
            'patente',
            'cod_propiedad',
            'desc_propiedad',
            'responde_preguntas',
            'respuestas_correctas',
            'horario_previred',
            'evaluacion_codretorno',
            'evaluacion_codrechazo',
            'evaluacion_desrechazo',
            'act_registro',
            'codigo_previred',
        ];

        $_contacto = SolicitudTarjeta::select($columnas)/* ->whereNotNull('evaluacion_codretorno') */;
            $rut = $request->rut;
            if( $rut != ""){
                $rut = str_replace('-', '', str_replace('.', '', $rut));
                $dv = substr($rut, -1); 

                $rut = substr($rut, 0, -1);
                $_contacto->where(DB::raw("REPLACE( REPLACE( rut ,'.','') ,'-','')  "), $rut);
                $_contacto->where(DB::raw("REPLACE( REPLACE( dv ,'.','') ,'-','')  "), $dv);
            }
         
            if($inicio != "" ){
                $_contacto->whereBetween('ABC31_SolicitudTarjeta.created_at', [
                    $inicio,
                    $fin
                ]);
            }
	        
        
		$contactos = $_contacto->get();
      
        /* Excel::create('ReporteSolicitudTarjeta', function($excel) use ( $contactos) {

            $excel->sheet('ReporteSolicitudTarjeta', function($sheet) use($contactos) {
        
                $sheet->fromArray($contactos);
        
            });
        
        })->export('xlsx'); */

        /* Exportar Excel con libreria Spreadsheet */
        $spreadsheet = new Spreadsheet();
        
        $filename = 'ReporteSolicitudTarjeta.xlsx';

        $sheet = $spreadsheet->getActiveSheet();

        /*
        // imprimir las columnas
        $abcdario = [];
        for ($i = 'A', $c = 0; $c < count($columnas); $i++, $c++) {
            $abcdario[] = $i;
        }
	
        foreach ($columnas as $key => $columna) {
            $letra = $abcdario[$key];
            $sheet->setCellValue($letra . '1', $columna);
        }

        // Imprimir la data
        foreach ($contactos as $numeroFila => $contacto) {
            $numeroFila = $numeroFila + 2;
            foreach ($columnas as $key => $columna) {
                $letra = $abcdario[$key];
                $sheet->setCellValue($letra . $numeroFila, $contacto->{$columna});
            }
        }
        */

        $contactos = $contactos->toArray();
        array_unshift($contactos, $columnas);

        $sheet->fromArray($contactos, null, 'A1');

        $writer = new Xlsx($spreadsheet);
        //$writer->save($filename);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'"');
        header('Cache-Control: max-age=0');
        
        $writer->save("php://output");
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response
     */
    public function datatable(Request $request)
    {

      /*   $contacto =  SolicitudTarjeta::get();
        return Datatables::of($contacto)
        ->make(true);
        */

        $solicitud =  SolicitudTarjeta::select(
            'id',
            'rut',
            'dv',
            'primer_nombre',
            'segundo_nombre',
            'apepat',
            'apemat',
            'email',
            'fono_movil',
            'fono_fijo',
            'crea_registro'
        )/* ->whereNotNull('evaluacion_codretorno') */;
        

        $rut = $request->rut;
        if( $rut != ""){
            $rut = str_replace('-', '', str_replace('.', '', $rut));
            $dv = substr($rut, -1); 

            $rut = substr($rut, 0, -1);
            $solicitud->where(DB::raw("REPLACE( REPLACE( rut ,'.','') ,'-','')  "), $rut);
            $solicitud->where(DB::raw("REPLACE( REPLACE( dv ,'.','') ,'-','')  "), $dv);
        }

        $inicio = $request->inicio;
        if (!preg_match('/^\d{2}-\d{2}-\d{4} \/ \d{2}-\d{2}-\d{4}$/', $inicio)) {
            return Datatables::of($solicitud)
                ->addColumn('ABC31_Rut', function ($tiposolicitudmetadato) {
                    return $tiposolicitudmetadato->rut . $tiposolicitudmetadato->dv;
                })
                ->addColumn('nombrecompleto', function ($tiposolicitudmetadato) {
                    return $tiposolicitudmetadato->primer_nombre . ' ' . $tiposolicitudmetadato->segundo_nombre;
                })
                ->addColumn('apellidocompleto', function ($tiposolicitudmetadato) {
                    return $tiposolicitudmetadato->apepat . ' ' . $tiposolicitudmetadato->apemat;
                })
                ->make(true);
        }

        $a = explode("/", $inicio);

        $inicio = trim($a[0]) . " 00:00:00";
        $fin = trim($a[1]) . " 23:59:59";

        $formato = 'd-m-Y H:i:s';

        $inicio = Carbon::createFromFormat($formato, $inicio);
        $fin = Carbon::createFromFormat($formato, $fin);
        //dd( $inicio );
        $solicitud->whereBetween('crea_registro', [
            $inicio,
            $fin,
        ]);

        return Datatables::of($solicitud)
            ->addColumn('ABC31_Rut', function ($tiposolicitudmetadato) {
                return $tiposolicitudmetadato->rut . $tiposolicitudmetadato->dv;
            })
            ->addColumn('nombrecompleto', function ($tiposolicitudmetadato) {
                return $tiposolicitudmetadato->primer_nombre . ' ' . $tiposolicitudmetadato->segundo_nombre;
            })
            ->addColumn('apellidocompleto', function ($tiposolicitudmetadato) {
                return $tiposolicitudmetadato->apepat . ' ' . $tiposolicitudmetadato->apemat;
            })
            ->make(true);
    }
}
