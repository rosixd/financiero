<?php

namespace Modules\Formularios\Http\Controllers\Admin;

use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Formularios\Entities\FormularioContacto;
use Modules\Formularios\Entities\Formularios;
use Modules\Formularios\Repositories\FormulariosRepository;
use Yajra\Datatables\Datatables;
use DB;
class FormularioContactoController extends AdminBaseController
{
    /**
     * @var FormulariosRepository
     */
    private $formularios;

    protected $form = 'prueba';

    public function __construct(FormulariosRepository $formularios)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$formularios = $this->formularios->all();
        return view('formularios::admin.reporte.FormularioContacto');
    }

    public function excel(Request $request)
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(0);
        $log =  FormularioContacto::select('*');
        $inicio = $request->fecha;

        $a = explode("/", $inicio);
    	if($inicio != "" ){
	        $inicio = trim($a[0]) . " 00:00:00";
	        $fin = trim($a[1]) . " 23:59:59";

	        $formato = 'd-m-Y H:i:s';
		$inicio = Carbon::createFromFormat($formato, $inicio);
        	$fin = Carbon::createFromFormat($formato, $fin);
	}
	
      
        $_contacto = FormularioContacto::select(
            //'ABC30_fecha_creacion',
	    'created_at',
            'ABC30_nombre_formulario',
            'ABC30_nombrecompleto',
            'ABC30_rut',
            'ABC30_telefono',
            'ABC30_email',
            'ABC30_tiposeguro',
            'ABC30_tiposolicitud',
            'ABC30_mensaje'
        )
        ->sinVacios();



        $rut = $request->rut;
        if( $rut != ""){
            $rut = str_replace('-', '', str_replace('.', '', $rut));
            $_contacto->where(DB::raw("REPLACE( REPLACE( ABC30_rut ,'.','') ,'-','')  "), $rut);
        }
        

        //if (trim($a[0]) != trim($a[1])) {
	if ($inicio != "" ) {
            $_contacto->whereBetween('created_at', [
                $inicio,
                $fin,
            ]);
        }
        $contactos = $_contacto->get()->map(function ($contacto) {
            $contacto = $contacto->toArray();
            $contacto['Tipo_de_seguro'] = $contacto['tiposeguro_str'];
            unset($contacto['tiposeguro_str']);

            return $contacto;
        });

        $_contactos = [];
        foreach ($contactos as $key => $contacto) {
            $_contactos[] =[
                //'Fecha de Creación'     =>$contacto['ABC30_fecha_creacion'],		
		        'Fecha de Creación'     =>$contacto['created_at'],
                'Nombre del formulario' =>$contacto['ABC30_nombre_formulario'],
                'Nombre Completo'       =>$contacto['ABC30_nombrecompleto'],
                'Rut'                   =>$contacto['ABC30_rut'],
                'Telefono'              =>$contacto['ABC30_telefono'],
                'Email'                 =>$contacto['ABC30_email'],
                'Codigo de seguro'      =>$contacto['ABC30_tiposeguro'],
                'Tipo de seguro'        =>$contacto['Tipo_de_seguro'],
                'Tipo de solicitud'     =>$contacto['ABC30_tiposolicitud'],
                'Comentario'            =>$contacto['ABC30_mensaje'],
            ];
        }

	
        if(empty($_contactos)){
            $_contactos[] =[
                //'Fecha de Creación'     =>$contacto['ABC30_fecha_creacion'],		
		'Fecha de Creación'     =>'',
                'Nombre del formulario' =>'',
                'Nombre Completo'       =>'',
                'Rut'                   =>'',
                'Telefono'              =>'',
                'Email'                 =>'',
                'Codigo de seguro'      =>'',
                'Tipo de seguro'        =>'',
                'Tipo de solicitud'     =>'',
                'Comentario'            =>'',
            ];
        }

        $contactos = $_contactos;
        Excel::create('ReporteFormularioContacto', function ($excel) use ($contactos) {
            $excel->sheet('ReporteFormularioContacto', function ($sheet) use ($contactos) {
                $sheet->fromArray($contactos);
            });
        })->export('xlsx');
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     * MA: se agrega filtro de rut 
     *
     * @author Rosana Hernandez 
     * @author Miguelangel Gutierrez Modificacion 11/02/2019 
     * @param  Request $request
     * @return Response
     */
    public function datatable(Request $request)
    {
        
       
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        /*   $contacto =  FormularioContacto::get();
          return Datatables::of($contacto)
          ->make(true);
        */
        
        $rut = $request->rut;
        
        $log =  FormularioContacto::select('*')
            ->sinVacios();

        $inicio = $request->inicio;
        
        //filtro rut
        $rut = $request->rut;
        if( $rut != ""){
            $rut = str_replace('-', '', str_replace('.', '', $rut));

            $log->where(DB::raw("REPLACE( REPLACE( ABC30_rut ,'.','') ,'-','')  "), $rut);
        }
        
        
        if (!preg_match('/^\d{2}-\d{2}-\d{4} \/ \d{2}-\d{2}-\d{4}$/', $inicio)) {
            return Datatables::of($log)
                ->make(true);
        }

        $a = explode("/", $inicio);

        $inicio = trim($a[0]) . " 00:00:00";
        $fin = trim($a[1]) . " 23:59:59";

        $formato = 'd-m-Y H:i:s';

        $inicio = Carbon::createFromFormat($formato, $inicio);
        $fin = Carbon::createFromFormat($formato, $fin);
        //dd( $inicio );
        $log->whereBetween('created_at', [
            $inicio,
            $fin,
        ]);

        return Datatables::of($log)
            ->make(true);
    }
}
