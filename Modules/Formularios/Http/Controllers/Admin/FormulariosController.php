<?php

namespace Modules\Formularios\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Formularios\Entities\Formularios;
use Modules\Formularios\Http\Requests\CreateFormulariosRequest;
use Modules\Formularios\Http\Requests\UpdateFormulariosRequest;
use Modules\Formularios\Repositories\FormulariosRepository;
use Yajra\Datatables\Datatables;

class FormulariosController extends AdminBaseController
{
    /**
     * @var FormulariosRepository
     */
    private $formularios;

    public function __construct(FormulariosRepository $formularios)
    {
        parent::__construct();

        $this->formularios = $formularios;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$formularios = $this->formularios->all();

        return view('formularios::admin.formularios.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('formularios::admin.formularios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFormulariosRequest $request
     * @return Response
     */
    public function store(CreateFormulariosRequest $request)
    {
        $data = $request->all();
        if (!array_key_exists('ABC07_activo_email', $data)) {
            $data["ABC07_activo_email"] = 0;
        }

        if (preg_match('/1|on/mi', $data["ABC07_activo_email"])) {
            $data["ABC07_activo_email"] = 1;
        }
        if (!array_key_exists('ABC07_exportar', $data)) {
            $data["ABC07_exportar"] = 0;
        }

        if (preg_match('/1|on/mi', $data["ABC07_exportar"])) {
            $data["ABC07_exportar"] = 1;
        }
        $this->formularios->create($data);

        return redirect()->route('admin.formularios.formularios.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('formularios::formularios.title.formularios')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Formularios $formularios
     * @return Response
     */
    public function edit(Formularios $formularios)
    {
        return view('formularios::admin.formularios.edit', compact('formularios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Formularios $formularios
     * @param  UpdateFormulariosRequest $request
     * @return Response
     */
    public function update(Formularios $formularios, UpdateFormulariosRequest $request)
    {
        $data = $request->all();
        if (!array_key_exists('ABC07_activo_email', $data)) {
            $data["ABC07_activo_email"] = 0;
        }

        if (preg_match('/1|on/mi', $data["ABC07_activo_email"])) {
            $data["ABC07_activo_email"] = 1;
        }
        if (!array_key_exists('ABC07_exportar', $data)) {
            $data["ABC07_exportar"] = 0;
        }

        if (preg_match('/1|on/mi', $data["ABC07_exportar"])) {
            $data["ABC07_exportar"] = 1;
        }
        $this->formularios->update($formularios, $data);

        return redirect()->route('admin.formularios.formularios.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('formularios::formularios.title.formularios')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Formularios $formularios
     * @return Response
     */
    public function destroy(Formularios $formularios)
    {
        $deleted_at = $formularios->deleted_at;
        $json = null;

        $asignado = false;

        if ($asignado) {
            $json = array(
                'asignado' => $asignado,
                'msn' => trans('formularios::formularios.messages.resource assigned'),
            );
        } else {
            $success = is_null($deleted_at)? $formularios->delete():$formularios->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('formularios::formularios.messages.resource desactivate'):trans('formularios::formularios.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false,
            );
        }

        return response()->json($json);
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response
     */
    public function datatable(Request $request)
    {
        $formularios =  Formularios::withTrashed()->get();

        return Datatables::of($formularios)
        ->addColumn('ruta_edit', function ($formularios) {
            return route("admin.formularios.formularios.edit", [$formularios->ABC07_id]);
        })
        ->addColumn('ruta_destroy', function ($formularios) {
            return route("admin.formularios.formularios.destroy", [$formularios->ABC07_id]);
        })
        ->addColumn('inactivo', function ($formularios) {
            return is_null($formularios->deleted_at)? false:true;
        })
        ->make(true);
    }
}
