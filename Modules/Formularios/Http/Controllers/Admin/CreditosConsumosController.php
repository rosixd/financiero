<?php

namespace Modules\Formularios\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Formularios\Entities\Formularios;
use Modules\Formularios\Http\Requests\CreateFormulariosRequest;
use Modules\Formularios\Http\Requests\UpdateFormulariosRequest;
use Modules\Formularios\Repositories\FormulariosRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Formularios\Entities\Controltxtftp;
use Modules\Formularios\Entities\Controltxthistorial;
use DB;
use Yajra\Datatables\Datatables;
use Config;
use Storage;

class CreditosConsumosController extends AdminBaseController
{
    /**
     * @var FormulariosRepository
     */
    private $formularios;

    protected $form = 'prueba';

    public function __construct(FormulariosRepository $formularios)
    {
         parent::__construct();
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$formularios = $this->formularios->all();
        return view('formularios::admin.reporte.creditoconsumo');
    }
    public function historial(Request $request){
        $historial =  Controltxthistorial::where('ABC26_id', $request->id)->withTrashed()->get();

        return Datatables::of($historial)
        ->make(true); 
    }
    public function reenviar(Request $request, $id){
       
        try {
            $formulario = Controltxtftp::find($id);
   
            //conexion ftp
            $disco = Storage::disk('ftp'); 
             
            //configuraciond de formulario
            $config = Config::get('ConfigFormularios.'.$formulario->ABC26_formulario);

            //ruta directorio creado local
            $ruta_archivo  = Config::get($config['ruta_local']);
            
            //nombre archivo local
            $archivo_nombre_local = $config['nombre_archivo'];
           // $ruta_archivo_local = 
            $ruta_archivo_local = $ruta_archivo .'/'.$formulario->ABC26_ruta_archivo;
          
            //seleccion de archivo local
            $archivo_local  = file_get_contents($ruta_archivo_local);
            //Nombre carpeta
            //$directorio_ftp = $config['directorio'];
            // $ruta_archivo_ftp = date('Y').'/'.date('m').'/'.$config['directorio'].'/'.$archivo_nombre_local;
            $ruta_archivo_ftp = $formulario->ABC26_ruta_archivo;
           
            //subir archivo al ftp
            $resul =  $disco->put($ruta_archivo_ftp, $archivo_local);

            $msj = 'ARCHIVO CARGADO CON ÉXITO';
            if($resul){
                //$iteracion = $value->ABC26_iteraciones + 1;
                $estado = 'cargado';

            }else{
                $estado = 'error-ftp';
                $msj = 'ERROR AL CARGAR EL ARCHIVO AL SERVIDOR';
            }

        } catch (\ErrorException $e) {
            $this->informacion($e->getMessage(), 'error-ftp', $id);
             return redirect()->route('admin.formularios.reporte.creditoconsumo')
            ->withError(trans('formularios::creditodeconsumo.success.ftp'));

        } catch (\RuntimeException $e) {
            $this->informacion($e->getMessage(), 'error-ftp', $id);
             return redirect()->route('admin.formularios.reporte.creditoconsumo')
            ->withError(trans('formularios::creditodeconsumo.success.ftp'));


        } catch (Exception $e) {

            $this->informacion($e->getMessage(), 'error-ftp', $id);

		    return redirect()->route('admin.formularios.reporte.creditoconsumo')
            ->withError(trans('formularios::creditodeconsumo.error.ftp'));
        }
        
        $this->informacion($msj, $estado, $id);
        return redirect()->route('admin.formularios.reporte.creditoconsumo')
            ->withSuccess(trans('formularios::creditodeconsumo.success.ftp'));
    }

    protected function informacion($msj, $estado, $id){
        Controltxtftp::where('ABC26_id', $id)
        ->update([
            "ABC26_estado"          => $estado, 
            //"ABC26_iteraciones"     => $iteracion, 
        ]);
            
        Controltxthistorial::create([
            "ABC26_id"          => $id,
            "ABC27_descripcion" => $msj,
        ]);
    }
    /**
     * Este metodo es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

     */
    public function datatable( Request $request )
    {

        $creditoconsumo =  Controltxtftp::where('ABC26_formulario', $this->form)->withTrashed()->get();

        return Datatables::of($creditoconsumo)
        ->addColumn('ruta_reenviar', function ($creditoconsumo) {
            return route("admin.formularios.reporte.creditoconsumo.reenviar",[$creditoconsumo->ABC26_id]);
        })        
        ->addColumn('inactivo', function ($creditoconsumo) {
            return is_null($creditoconsumo->deleted_at)? false:true;
        })
        ->make(true); 
 
    }
}
