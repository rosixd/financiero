<?php

namespace Modules\Formularios\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Formularios\Entities\Respuestas;
use Modules\Formularios\Http\Requests\CreateRespuestasRequest;
use Modules\Formularios\Http\Requests\UpdateRespuestasRequest;
use Modules\Formularios\Repositories\RespuestasRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class RespuestasController extends AdminBaseController
{
    /**
     * @var RespuestasRepository
     */
    private $respuestas;

    public function __construct(RespuestasRepository $respuestas)
    {
        parent::__construct();

        $this->respuestas = $respuestas;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$respuestas = $this->respuestas->all();

        return view('formularios::admin.respuestas.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('formularios::admin.respuestas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRespuestasRequest $request
     * @return Response
     */
    public function store(CreateRespuestasRequest $request)
    {
        $this->respuestas->create($request->all());

        return redirect()->route('admin.formularios.respuestas.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('formularios::respuestas.title.respuestas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Respuestas $respuestas
     * @return Response
     */
    public function edit(Respuestas $respuestas)
    {
        return view('formularios::admin.respuestas.edit', compact('respuestas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Respuestas $respuestas
     * @param  UpdateRespuestasRequest $request
     * @return Response
     */
    public function update(Respuestas $respuestas, UpdateRespuestasRequest $request)
    {
        $this->respuestas->update($respuestas, $request->all());

        return redirect()->route('admin.formularios.respuestas.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('formularios::respuestas.title.respuestas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Respuestas $respuestas
     * @return Response
     */
    public function destroy(Respuestas $respuestas)
    {
        $this->respuestas->destroy($respuestas);

        return redirect()->route('admin.formularios.respuestas.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('formularios::respuestas.title.respuestas')]));
    }
}
