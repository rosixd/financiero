<?php

namespace Modules\Formularios\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Formularios\Events\Handlers\RegisterFormulariosSidebar;

class FormulariosServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterFormulariosSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('formularios', array_dot(trans('formularios::formularios')));
            $event->load('respuestas', array_dot(trans('formularios::respuestas')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('formularios', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Formularios\Repositories\FormulariosRepository',
            function () {
                $repository = new \Modules\Formularios\Repositories\Eloquent\EloquentFormulariosRepository(new \Modules\Formularios\Entities\Formularios());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Formularios\Repositories\Cache\CacheFormulariosDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Formularios\Repositories\RespuestasRepository',
            function () {
                $repository = new \Modules\Formularios\Repositories\Eloquent\EloquentRespuestasRepository(new \Modules\Formularios\Entities\Respuestas());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Formularios\Repositories\Cache\CacheRespuestasDecorator($repository);
            }
        );
// add bindings


    }
}
