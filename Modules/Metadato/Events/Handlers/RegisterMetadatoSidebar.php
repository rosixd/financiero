<?php

namespace Modules\Metadato\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterMetadatoSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        if( 
            $this->auth->hasAccess('metadato.metadatos.index') ||
            $this->auth->hasAccess('metadato.tipometadatos.index')
        ){
            $menu->group(trans('core::sidebar.content'), function (Group $group) {
                $group->item(trans('metadato::metadatos.title.metadatos'), function (Item $item) {
                    $item->icon('fa fa-tags');
                    $item->weight(10);
                    
                    //DMT: Item Tipo metadatos
                    $item->item(trans('metadato::tipometadatos.permisos.name'), function (Item $item) {
                        $item->icon('fa fa-circle-o');
                        $item->weight(0);
                        $item->append('admin.metadato.tipometadato.create');
                        $item->route('admin.metadato.tipometadato.index');
                        $item->authorize(
                            $this->auth->hasAccess('metadato.tipometadatos.index')
                        );
                    });

                    //DMT: Item metadatos    
                    $item->item(trans('metadato::metadatos.permisos.name'), function (Item $item) {
                        $item->icon('fa fa-circle-o');
                        $item->weight(0);
                        $item->append('admin.metadato.metadato.create');
                        $item->route('admin.metadato.metadato.index');
                        $item->authorize(
                            $this->auth->hasAccess('metadato.metadatos.index')
                        );
                    });
                });
            });
        }        

        return $menu;
    }
}
