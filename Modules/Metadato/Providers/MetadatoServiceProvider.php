<?php

namespace Modules\Metadato\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Metadato\Events\Handlers\RegisterMetadatoSidebar;

class MetadatoServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterMetadatoSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('metadatos', array_dot(trans('metadato::metadatos')));
            $event->load('tipometadatos', array_dot(trans('metadato::tipometadatos')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('metadato', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Metadato\Repositories\MetadatoRepository',
            function () {
                $repository = new \Modules\Metadato\Repositories\Eloquent\EloquentMetadatoRepository(new \Modules\Metadato\Entities\Metadato());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Metadato\Repositories\Cache\CacheMetadatoDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Metadato\Repositories\TipometadatoRepository',
            function () {
                $repository = new \Modules\Metadato\Repositories\Eloquent\EloquentTipometadatoRepository(new \Modules\Metadato\Entities\Tipometadato());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Metadato\Repositories\Cache\CacheTipometadatoDecorator($repository);
            }
        );
// add bindings


    }
}
