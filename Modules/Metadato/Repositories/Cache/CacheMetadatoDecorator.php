<?php

namespace Modules\Metadato\Repositories\Cache;

use Modules\Metadato\Repositories\MetadatoRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMetadatoDecorator extends BaseCacheDecorator implements MetadatoRepository
{
    public function __construct(MetadatoRepository $metadato)
    {
        parent::__construct();
        $this->entityName = 'metadato.metadatos';
        $this->repository = $metadato;
    }
}
