<?php

namespace Modules\Metadato\Repositories\Cache;

use Modules\Metadato\Repositories\TipometadatoRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTipometadatoDecorator extends BaseCacheDecorator implements TipometadatoRepository
{
    public function __construct(TipometadatoRepository $tipometadato)
    {
        parent::__construct();
        $this->entityName = 'metadato.tipometadatos';
        $this->repository = $tipometadato;
    }
}
