<?php

namespace Modules\Metadato\Repositories\Eloquent;

use Modules\Metadato\Repositories\TipometadatoRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTipometadatoRepository extends EloquentBaseRepository implements TipometadatoRepository
{
}
