<?php

namespace Modules\Metadato\Repositories\Eloquent;

use Modules\Metadato\Repositories\MetadatoRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMetadatoRepository extends EloquentBaseRepository implements MetadatoRepository
{
}
