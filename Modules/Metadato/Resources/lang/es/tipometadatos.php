<?php

return [
    'list resource' => 'Consultar Tipo de metadato',
    'create resource' => 'Crear Tipo de metadato',
    'edit resource' => 'Editar Tipo de metadato',
    'destroy resource' => 'Desactivar Tipo de metadato',
    'title' => [
        'tipometadatos' => 'Tipo de metadato',
        'create tipometadato' => 'Crear Tipo de metadato',
        'edit tipometadato' => 'Editar Tipo de metadato',
    ],
    'button' => [
        'create tipometadato' => 'Crear',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Desactivar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [
        'name' =>'Nombre',
        'atributte' =>'Atributo',
        'created at' =>'Fecha Creaci&oacute;n',
    ],
    'form' => [
        'name' =>'Nombre',
        'atributte' =>'Atributo',
        'type' =>'Tipo Metadato',
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, el tipo de metadatos se ha creado correctamente',
        'resource not found' => 'Sr. Usuario,:name no encontrado.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
        'resource desactivate' => 'Sr. Usuario, el tipo de metadato se ha desactivado correctamente.',
        'resource activate' => 'Sr. Usuario, el  tipo de metadato se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, el tipo metadato no se puede desactivar ya que contiene datos asociados.',
	'updated sin' => 'Sr. Usuario, no ha realizado cambios para tipo de metadato.',
    ],
    'validation' => [
        'name' => 'Sr. Usuario, debe ingresar el nombre.',
        'atributte' => 'Sr. Usuario, debe ingresar el atributo.',
        'type' => 'Sr. Usuario, debe ingresar el tipo de metadato.',
    ],
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
        'name' => 'Tipos de metadatos',
        ]
];
