<?php

return [
    'list resource' => 'Consultar Metadatos',
    'create resource' => 'Crear Metadatos',
    'edit resource' => 'Editar Metadatos',
    'destroy resource' => 'Desactivar Metadatos',
    'title' => [
        'metadatos' => 'Metadatos',
        'create metadato' => 'Crear Metadatos',
        'edit metadato' => 'Editar Metadatos',
    ],
    'button' => [
        'create metadato' => 'Crear',
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Editar',
        'delete' => 'Desactivar',
        'reset' => 'Reiniciar',
        'update and back' => 'Editar y regresar',
    ],
    'table' => [
        'content' =>'Contenido',
        'type' =>'Tipo Metadato',
        'created at' =>'Fecha Creaci&oacute;n',
	    'check_banner' => 'Metadato Global'
    ],
    'form' => [
        'content' =>'Contenido',
        'type' =>'Tipo Metadato',
        'check_banner' => 'Metadato Global'
    ],
    'messages' => [
        'resource created' => 'Sr. Usuario, el :name se ha creado exitosamente.',
        'resource not found' => 'Sr. Usuario,:name no encontrado.',
        'resource updated' => 'Sr. Usuario, los datos se han actualizado correctamente.',
	    'resource desactivate' => 'Sr. Usuario el metadato se ha desactivado &eacute;xitosamente.',
        'resource activate' => 'Sr. Usuario, el  metadato se ha activado correctamente.',
        'resource assigned' => 'Sr. Usuario, metadato no se puede desactivar ya que contiene datos asociados.',
	'updated sin' => 'Sr. Usuario, no ha realizado cambios para metadatos.',
    ],
    'validation' => [
        'content' => 'Sr. Usuario, debe ingresar el contenido.',
        'type' => 'Sr. Usuario, debe ingresar el tipo de metadato.',
    ],
    
    // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
    'permisos' => [
        // DMT: se utiliza en la seccion de permisos de los roles y nombre en menu
        'name' => 'Metadatos',
        'metadato metadatos' => 'Metadatos',
        'metadato tipometadatos' => 'Tipos de metadatos',
        ]
    ];
    