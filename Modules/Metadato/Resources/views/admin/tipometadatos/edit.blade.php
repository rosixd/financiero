@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('metadato::tipometadatos.title.edit tipometadato') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.metadato.tipometadato.index') }}">{{ trans('metadato::tipometadatos.title.tipometadatos') }}</a></li>
        <li class="active">{{ trans('metadato::tipometadatos.title.edit tipometadato') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.metadato.tipometadato.update', $tipometadato->ABC05_id], 'method' => 'put', 'id'=>'ValidacionEditar']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('metadato::admin.tipometadatos.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-md">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-md" href="{{ route('admin.metadato.tipometadato.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop
@push('js-stack')
<script>

$( document ).ready(function() {
   
    var inicio = $("form").serialize();

    $('#ValidacionEditar').submit(function(){
        if(inicio === $("form").serialize()){ 
           AppGeneral.notificaciones('warning', "{{ trans('metadato::tipometadatos.messages.updated sin') }}");
           return false;
        } 
    });

    $('#ABC05_nombre').on('blur', function(){
        var dato = AppGeneral.GenerateSlug( $('#ABC05_nombre').val() );
        $('#ABC05_nombre_slug').val(dato);

    });
    
    $('#ABC05_attributos').on('blur', function(){
       var dato =  AppGeneral.GenerateSlug( $('#ABC05_attributos').val() );
        $('#ABC05_attributos_slug').val(dato);
    });

});
</script>
@endpush