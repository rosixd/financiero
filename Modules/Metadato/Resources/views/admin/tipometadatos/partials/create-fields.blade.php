<div class="box-body">
	<div class="form-group">
	{{-- <div class="form-group{{ $errors->has('ABC05_nombre') ? ' has-error' : '' }}"> --}}
		<label>{{ trans('metadato::tipometadatos.form.name') }}</label>
		<input type="text" class="form-control soloLetras " name="ABC05_nombre" id="ABC05_nombre" value="{{ old('ABC05_nombre') }}">
		<span class="text-danger">
			{{ $errors->has('ABC05_nombre') ? trans('metadato::tipometadatos.validation.name') : '' }}
		</span>
	</div>

	<input type="hidden" name="ABC05_nombre_slug" id='ABC05_nombre_slug' value="{{ old('ABC05_attributos') }}">
	<input type="hidden" name="ABC05_attributos_slug" id='ABC05_attributos_slug' value="{{ old('ABC05_attributos') }}">
	
	<div class="form-group">
	{{-- <div class="form-group{{ $errors->has('ABC05_attributos') ? ' has-error' : '' }}"> --}}
		<label>{{ trans('metadato::tipometadatos.form.atributte') }}</label>
		<input type="text" class="form-control " name="ABC05_attributos" id="ABC05_attributos" value="{{ old('ABC05_attributos') }}">
		<span class="text-danger">
			{{ $errors->has('ABC05_attributos') ? trans('metadato::tipometadatos.validation.atributte') : '' }}
		</span>
	</div>

	<span class="text-danger">
		{!! $errors->first('unicos', ':message') !!}
	</span>
</div>

