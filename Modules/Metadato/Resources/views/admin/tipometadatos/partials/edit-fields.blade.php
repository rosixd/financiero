<div class="box-body">
	<div class="form-group">
		<label>{{ trans('metadato::tipometadatos.form.name') }}</label>
		<input type="text" class="form-control " name="ABC05_nombre" id="ABC05_nombre"  value="{{ old('ABC05_nombre') == '' ? $tipometadato->ABC05_nombre : old('ABC05_nombre') }}">
		<span class="text-danger">
			{{ $errors->has('ABC05_nombre') ? trans('metadato::tipometadatos.validation.name') : '' }}
		</span>
	</div>
	<input type="hidden" name="ABC05_nombre_slug" id='ABC05_nombre_slug' value="{{ old('ABC05_nombre_slug') == '' ? $tipometadato->ABC05_nombre_slug : old('ABC05_nombre_slug')  }}">
	<input type="hidden" name="ABC05_attributos_slug" id='ABC05_attributos_slug' value="{{  old('ABC05_attributos_slug') == '' ? $tipometadato->ABC05_attributos_slug : old('ABC05_attributos_slug') }}">
	<div class="form-group">
		<label>{{ trans('metadato::tipometadatos.form.atributte') }}</label>
		<input type="text" class="form-control " name="ABC05_attributos" id="ABC05_attributos"  value="{{  old('ABC05_attributos') == '' ? $tipometadato->ABC05_attributos : old('ABC05_attributos') }}">
		<span class="text-danger">
			{{ $errors->has('ABC05_attributos') ? trans('metadato::tipometadatos.validation.atributte') : '' }}
		</span>
	</div>

	<span class="text-danger">
		{!! $errors->first('unicos', ':message') !!}
	</span>
</div>