@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('metadato::tipometadatos.title.tipometadatos') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('metadato::tipometadatos.title.tipometadatos') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    @if ($currentUser->hasAccess('metadato.tipometadatos.create'))
                    <a href="{{ route('admin.metadato.tipometadato.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('metadato::tipometadatos.button.create tipometadato') }}
                    </a>
                    @endif
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 2, "desc" ]]'>
                            <thead>
                                <tr>
                                    <th>{{ trans('metadato::tipometadatos.table.name') }}</th>
                                    <th>{{ trans('metadato::tipometadatos.table.atributte') }}</th>
                                    <th>{{ trans('core::core.table.created at') }}</th>
                                    <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        <!--<tfoot>
                                <tr>
                                    <th>{{ trans('metadato::tipometadatos.table.name') }}</th>
                                    <th>{{ trans('metadato::tipometadatos.table.atributte') }}</th>
                                    <th>{{ trans('core::core.table.created at') }}</th>
                                    <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                                </tr>
                            </tfoot>-->
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::modalLog')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('metadato::tipometadatos.title.create tipometadato') }}</dd>
    </dl>
@stop

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('metadato.tipometadatos.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('metadato.tipometadatos.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('metadato.tipometadatos.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('metadato.tipometadatos.index') ? 'true' : 'false' }}
    };
</script>
@endprepend