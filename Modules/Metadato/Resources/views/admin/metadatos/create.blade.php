@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('metadato::metadatos.title.create metadato') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.metadato.metadato.index') }}">{{ trans('metadato::metadatos.title.metadatos') }}</a></li>
        <li class="active">{{ trans('metadato::metadatos.title.create metadato') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.metadato.metadato.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('metadato::admin.metadatos.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        @if ($currentUser->hasAccess('metadato.metadatos.create'))
                        <button type="submit" class="btn btn-primary btn-md">{{ trans('core::core.button.create') }}</button>
                        @endif
                        <a class="btn btn-danger pull-right btn-md" href="{{ route('admin.metadato.metadato.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

