@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('metadato::metadatos.title.metadatos') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('metadato::metadatos.title.metadatos') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    @if ($currentUser->hasAccess('metadato.metadatos.create'))
                    <a href="{{ route('admin.metadato.metadato.create') }}" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('metadato::metadatos.button.create metadato') }}
                    </a>
                    @endif
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 3, "desc" ]]'>
                            <thead>
                            <tr>
                                <th>{{ trans('metadato::metadatos.table.content') }}</th>
                                <th>{{ trans('metadato::metadatos.table.type') }}</th>
				                <th>{{ trans('metadato::metadatos.table.check_banner') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
				                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::modalLog')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
    <dt><code>c</code></dt>
    <dd>{{ trans('metadato::metadatos.title.create metadato') }}</dd>
</dl>
@stop

@prepend('notificaciones')
<script>
    AppGeneral.permisos = {
        "crear"     : {{ $currentUser->hasAccess('metadato.metadatos.create') ? 'true' : 'false' }},
        "editar"    : {{ $currentUser->hasAccess('metadato.metadatos.edit') ? 'true' : 'false' }},
        "desactivar": {{ $currentUser->hasAccess('metadato.metadatos.destroy') ? 'true' : 'false' }},
        "log"       : {{ $currentUser->hasAccess('metadato.metadatos.index') ? 'true' : 'false' }}
    };
</script>
@endprepend