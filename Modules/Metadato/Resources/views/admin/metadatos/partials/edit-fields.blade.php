<div class="box-body">
	<div class="form-group">
		<label>{{ trans('metadato::metadatos.form.content') }}</label>
		<input type="text" class="form-control" name="ABC04_contenido" id="ABC04_contenido"  value="{{ $metadato->ABC04_contenido }}">
		<span class="text-danger">
			{{ $errors->has('ABC04_contenido') ? trans('metadato::metadatos.validation.content') : '' }}
		</span>
	</div>
	<label>{{ trans('metadato::metadatos.form.type') }}</label>
    <select name="ABC05_id" class="select">
        <label>{{ trans('metadato::metadatos.form.type') }}</label>
        <option value="">Seleccione</option>)
        @foreach($tipometadato as $tipometadatos)
            <option {{ ($tipometadatos->ABC05_id == $metadato->ABC05_id)? "selected":"" }}  value="{{$tipometadatos->ABC05_id}}">{{$tipometadatos->ABC05_nombre}}</option>
        @endforeach
    </select>
    <span class="text-danger">
        {{ $errors->has('ABC05_id') ? trans('metadato::metadatos.validation.type') : '' }}
    </span>
    <br>
	<div class="form-check">
		<input type="checkbox" {{($metadato->ABC04_check_banner == 1) ? 'checked':''}} class="form-check-input" id="ABC04_check_banner" name="ABC04_check_banner" value="1">
		<label class="form-check-label" for="exampleCheck1">{{ trans('metadato::metadatos.form.check_banner') }}
		</label>
	</div>
</div>