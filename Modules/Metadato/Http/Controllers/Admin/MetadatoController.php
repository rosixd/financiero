<?php

namespace Modules\Metadato\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Metadato\Entities\Metadato;
use Modules\Metadato\Entities\Tipometadato;
use Modules\Metadato\Http\Requests\CreateMetadatoRequest;
use Modules\Metadato\Http\Requests\UpdateMetadatoRequest;
use Modules\Metadato\Repositories\MetadatoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class MetadatoController extends AdminBaseController
{
    /**
     * @var MetadatoRepository
     */
    private $metadato;

    public function __construct(MetadatoRepository $metadato)
    {
        parent::__construct();

        $this->metadato = $metadato;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('metadato::admin.metadatos.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $tipometadato = Tipometadato::all();
        return view('metadato::admin.metadatos.create', compact('tipometadato'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMetadatoRequest $request
     * @return Response
     */
    public function store(CreateMetadatoRequest $request)
    {

        $this->metadato->create($request->all());

        return redirect()->route('admin.metadato.metadato.index')
            ->withSuccess(trans('metadato::metadatos.messages.resource created', ['name' => trans('metadato::metadatos.title.metadatos')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Metadato $metadato
     * @return Response
     */
    public function edit(Metadato $metadato)
    {
    	$tipometadato = Tipometadato::all();
        return view('metadato::admin.metadatos.edit', compact('metadato', 'tipometadato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Metadato $metadato
     * @param  UpdateMetadatoRequest $request
     * @return Response
     */
    public function update(Metadato $metadato, UpdateMetadatoRequest $request)
    {
        $data = $request->all();
        
        $data["ABC04_check_banner"] = 0;

        if( $request->has("ABC04_check_banner") == true ){
            $data["ABC04_check_banner"] = '1';
        }
        
        $metadato->fill($data);
       /* if(!$metadato->isDirty()){
            return redirect()->route('admin.metadato.metadato.edit',[$metadato->ABC04_id] )
            ->with("warning", trans('metadato::metadatos.messages.updated sin') );
        }*/

        $this->metadato->update($metadato, $request->all());

        return redirect()->route('admin.metadato.metadato.index')
            ->withSuccess(trans('metadato::metadatos.messages.resource updated', ['name' => trans('metadato::metadatos.title.metadatos')])); 
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Metadato $metadato
     * @return Response
     */
    public function destroy(Metadato $metadato)
    {
        $deleted_at = $metadato->deleted_at;


        $asignado = (!is_null($deleted_at) && 
     
            is_null(
                $metadato->with('tipometadato')
                ->withTrashed()
                ->find($metadato->ABC04_id)->tipometadato
            ))? true:false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('metadato::metadatos.messages.resource assigned')
            );

        }else{

            $success  = is_null($deleted_at)? $metadato->delete():$metadato->restore();
            $json = array(
                    'success' => $success? true:false,
                    'msn' => is_null($deleted_at)? trans('metadato::metadatos.messages.resource desactivate'):trans('metadato::metadatos.messages.resource activate'),
                    'status' => is_null($deleted_at)? true:false
                );
        } 

        return response()->json($json); 
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

    */
    public function datatable( Request $request )
    {

        $metadato = Metadato::with('tipometadato')->withTrashed();
        return Datatables::of($metadato)
        ->addColumn('ruta_edit', function ($metadato) {
            return route("admin.metadato.metadato.edit",[$metadato->ABC04_id]);
        })        
        ->addColumn('ruta_destroy', function($metadato){
            return route("admin.metadato.metadato.destroy",[$metadato->ABC04_id]);
        })
        ->addColumn('inactivo', function ($metadato) {
            return is_null($metadato->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
