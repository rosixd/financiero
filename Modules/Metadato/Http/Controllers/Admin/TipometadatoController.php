<?php

namespace Modules\Metadato\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Metadato\Entities\Tipometadato;
use Modules\Metadato\Http\Requests\CreateTipometadatoRequest;
use Modules\Metadato\Http\Requests\UpdateTipometadatoRequest;
use Modules\Metadato\Repositories\TipometadatoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use DB;
use Yajra\Datatables\Datatables;

class TipometadatoController extends AdminBaseController
{
    /**
     * @var TipometadatoRepository
     */
    private $tipometadato;

    public function __construct(TipometadatoRepository $tipometadato)
    {
        parent::__construct();

        $this->tipometadato = $tipometadato;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$tipometadatos = $this->tipometadato->all();
		
		//\Modules\Metadato\Entities\Tipometadato::on("mysqlFake")->find(1);
		
        return view('metadato::admin.tipometadatos.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('metadato::admin.tipometadatos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateTipometadatoRequest $request
     * @return Response
     */
    public function store(CreateTipometadatoRequest $request)
    {

        $this->tipometadato->create($request->all());

        return redirect()->route('admin.metadato.tipometadato.index')
            ->withSuccess(trans('metadato::tipometadatos.messages.resource created', ['name' => trans('metadato::tipometadatos.title.tipometadatos')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tipometadato $tipometadato
     * @return Response
     */
    public function edit(Tipometadato $tipometadato)
    {
        return view('metadato::admin.tipometadatos.edit', compact('tipometadato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Tipometadato $tipometadato
     * @param  UpdateTipometadatoRequest $request
     * @return Response
     */
    public function update(Tipometadato $tipometadato, CreateTipometadatoRequest $request)
    {
        //$tipometadato->fill($request->all());
		
        //dd( !$tipometadato->isDirty() );
       /*  if(!$tipometadato->isDirty()){
            return redirect()->route('admin.metadato.tipometadato.edit',[$tipometadato->ABC05_id])
            ->with("warning", trans('metadato::tipometadatos.messages.updated sin') );
        } */

        $datos = $request->all();

        $this->tipometadato->update($tipometadato, $datos );

        return redirect()->route('admin.metadato.tipometadato.index')
            ->withSuccess(trans('metadato::tipometadatos.messages.resource updated', ['name' => trans('metadato::tipometadatos.title.tipometadatos')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tipometadato $tipometadato
     * @return Response
     */
    public function destroy(Tipometadato $tipometadato)
    {

        $deleted_at = $tipometadato->deleted_at;
        $json = null;
        $asignado = (is_null($deleted_at) && 
           Tipometadato::with('metadato')->find($tipometadato->ABC05_id)->metadato)? true:false;

        if($asignado){

            $json = array(
                'asignado' => $asignado,
                'msn' => trans('metadato::tipometadatos.messages.resource assigned')
            );

        }else{

            $success = is_null($deleted_at)? $tipometadato->delete():$tipometadato->restore();

            $json = array(
                'success' => $success? true:false,
                'msn' => is_null($deleted_at)? trans('metadato::tipometadatos.messages.resource desactivate'):trans('metadato::tipometadatos.messages.resource activate'),
                'status' => is_null($deleted_at)? true:false
            );
        }      

        return response()->json($json);
    }

    /**
     * Este método es para utilizar el datatables del plugin yajra
     *
     * @author Rosana Hernandez
     * @param  Request $request
     * @return Response

    */
    public function datatable( Request $request )
    {

        $tipometadato = Tipometadato::withTrashed();

        return Datatables::of($tipometadato)
        ->addColumn('ruta_edit', function ($tipometadato) {
            return route("admin.metadato.tipometadato.edit",[$tipometadato->ABC05_id]);
        })        
        ->addColumn('ruta_destroy', function($tipometadato){
            return route("admin.metadato.tipometadato.destroy",[$tipometadato->ABC05_id]);
        })
        ->addColumn('inactivo', function ($tipometadato) {
            return is_null($tipometadato->deleted_at)? false:true;
        })
        ->make(true);   
    }
}
