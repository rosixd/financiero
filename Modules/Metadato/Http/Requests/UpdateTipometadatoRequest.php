<?php

namespace Modules\Metadato\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateTipometadatoRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
		'ABC05_nombre' => ['required', 'min:1'],
            'ABC05_attributos' => ['required', 'min:1'], 
		];
    }

    public function translationRules()
    {
        return [
            'ABC05_nombre' => ['required', 'min:1'],
            'ABC05_attributos' => ['required', 'min:1'], 
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'ABC05_nombre.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.required' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre.min' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.min' => 'Sr. Usuario, debe ingresar los atributos.',
        ];
    }

    public function translationMessages()
    {
        return [
            'ABC05_nombre.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.required' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre.min' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.min' => 'Sr. Usuario, debe ingresar los atributos.',
        ];
    }
}
