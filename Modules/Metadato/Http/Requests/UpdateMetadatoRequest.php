<?php

namespace Modules\Metadato\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateMetadatoRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
	'ABC04_contenido' => 'required',
        'ABC05_id' => 'required',
	];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [
		'ABC04_contenido.required' => 'Sr. Usuario, debe ingresar el campo contenido.',
		'ABC05_id.required' => 'Sr. Usuario, debe ingresar el campo tipo de metadato.',
	];
    }
}
