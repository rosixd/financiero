<?php

namespace Modules\Metadato\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;
use Illuminate\Foundation\Http\FormRequest;
use  \Illuminate\Validation\Validator;
use Modules\Metadato\Entities\Tipometadato;
class CreateTipometadatoRequest extends FormRequest
{
    public function rules()
    {
        return [
            'ABC05_nombre'          => ['required', 'min:1'],
            'ABC05_attributos'      => ['required', 'min:1'], 
            'ABC05_nombre_slug'     => [],
            'ABC05_attributos_slug' => [],
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'ABC05_nombre.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.required' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre.min' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.min' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre_slug.unique' => 'Sr. Usuario, ya existe un tipo de metadatos con el nombre y el atributo ingresado. ',
            'ABC05_attributos_slug.unique' => 'Sr. Usuario, ya existe un tipo de metadatos con el nombre y el atributo ingresado. ',
        ];
    }

    public function translationMessages()
    {
        return [
            'ABC05_nombre.required' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.required' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre.min' => 'Sr. Usuario, debe ingresar el nombre.',
            'ABC05_attributos.min' => 'Sr. Usuario, debe ingresar los atributos.',
            'ABC05_nombre_slug.unique' => 'Sr. Usuario, ya existe un tipo de metadatos con el nombre y el atributo ingresado. ',
            'ABC05_attributos_slug.uniqueABC05_attributos_slug.uniqueABC05_attributos_slug.unique.unique' => 'Sr. Usuario, ya existe un tipo de metadatos con el nombre y el atributo ingresado. ',
        ];
    }
    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            
            
            $resultado  = Tipometadato::where('ABC05_nombre_slug',$this->ABC05_nombre_slug)
            ->where('ABC05_attributos_slug',$this->ABC05_attributos_slug )->get();
            
			
           if (count($resultado) > 0) {
                $validator->errors()->add('unicos', 'Sr. Usuario, ya existe un tipo de metadatos con el nombre y el atributo ingresado.');
            } 
        });
    }
}
