<?php

use Illuminate\Routing\Router;
use Modules\Metadato\Entities\Metadato;
use Modules\Metadato\Entities\Tipometadato;
/** @var Router $router */

$router->group(['prefix' =>'/metadato'], function (Router $router) {
    $router->bind('metadato', function ($id) {
        $repo = app('Modules\Metadato\Repositories\MetadatoRepository')->find($id);
        return (is_null($repo))? Metadato::withTrashed()->find($id):$repo;
    });
    $router->get('metadatos', [
        'as' => 'admin.metadato.metadato.index',
        'uses' => 'MetadatoController@index',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'metadato',
        'middleware' => 'can:metadato.metadatos.index',
        'datatable' => 'admin.metadato.datatable',
        'log' => 'admin.user.user.logdatatable',
	    'editable' => 'api.translation.translations.update',
        'clearcache' => 'api.translation.translations.clearCache'
    ]);
    $router->get('metadatos/create', [
        
        'as' => 'admin.metadato.metadato.create',
        'middleware' => 'can:metadato.metadatos.create',
        'uses' => 'MetadatoController@create',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'metadato',
        'datatable' => 'admin.metadato.datatable'
    ]);
    $router->post('metadatos', [
        
        'as' => 'admin.metadato.metadato.store',
        'middleware' => 'can:metadato.metadatos.create',
        'uses' => 'MetadatoController@store',
    ]);
    $router->get('metadatos/{metadato}/edit', [
        
        'as' => 'admin.metadato.metadato.edit',
        'middleware' => 'can:metadato.metadatos.edit',
        'uses' => 'MetadatoController@edit',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'metadato',
        'datatable' => 'admin.metadato.datatable'
    ]);
    $router->put('metadatos/{metadato}', [
        
        'as' => 'admin.metadato.metadato.update',
        'middleware' => 'can:metadato.metadatos.edit',
        'uses' => 'MetadatoController@update',
    ]);
    $router->get('metadatos/destroy/{metadato}', [
        
        'as' => 'admin.metadato.metadato.destroy',
        'middleware' => 'can:metadato.metadatos.destroy',
        'uses' => 'MetadatoController@destroy',
    ]);
    $router->bind('tipometadato', function ($id) {
        $repo = app('Modules\Metadato\Repositories\TipometadatoRepository')->find($id);
        return (is_null($repo))? Tipometadato::withTrashed()->find($id):$repo;
    });
    $router->get('tipometadatos', [
        'as' => 'admin.metadato.tipometadato.index',
        'middleware' => 'can:metadato.tipometadatos.index',
        'uses' => 'TipometadatoController@index',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'tipometadato',
        'datatable' => 'admin.tipometadato.datatable',
        'log' => 'admin.user.user.logdatatable',
	    'editable' => 'api.translation.translations.update',
        'clearcache' => 'api.translation.translations.clearCache'
    ]);
    $router->get('tipometadatos/create', [
        'as' => 'admin.metadato.tipometadato.create',
         'middleware' => 'can:metadato.tipometadatos.create',
        'uses' => 'TipometadatoController@create',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'tipometadato',
        'datatable' => 'admin.tipometadato.datatable'
    ]);
    $router->post('tipometadatos', [
        'as' => 'admin.metadato.tipometadato.store',
         'middleware' => 'can:metadato.tipometadatos.create',
        'uses' => 'TipometadatoController@store',
    ]);
    $router->get('tipometadatos/{tipometadato}/edit', [
        'as' => 'admin.metadato.tipometadato.edit',
         'middleware' => 'can:metadato.tipometadatos.edit',
        'uses' => 'TipometadatoController@edit',
        'script' => 'AppMetadato',
        'view' => 'index',
        'modulo' => 'metadato',
        'submodulo' => 'tipometadato',
        'datatable' => 'admin.tipometadato.datatable'
    ]);
    $router->put('tipometadatos/{tipometadato}', [
        'as' => 'admin.metadato.tipometadato.update',
         'middleware' => 'can:metadato.tipometadatos.edit',
        'uses' => 'TipometadatoController@update',
    ]);
    $router->get('tipometadatos/destroy/{tipometadato}', [
        'as' => 'admin.metadato.tipometadato.destroy',
         'middleware' => 'can:metadato.tipometadatos.destroy',
        'uses' => 'TipometadatoController@destroy',
    ]);
// append
    $router->get('metadatos/datatable', [
        'as' => "admin.metadato.datatable",
         'middleware' => 'can:metadato.metadatos.index',
        "uses" => "MetadatoController@datatable",
    ]);

    $router->get('tipometadatos/datatable', [
        'as' => "admin.tipometadato.datatable",
         'middleware' => 'can:metadato.tipometadatos.index',
        "uses" => "TipometadatoController@datatable",
    ]);

});
