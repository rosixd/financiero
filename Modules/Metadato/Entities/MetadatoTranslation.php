<?php

namespace Modules\Metadato\Entities;

use Illuminate\Database\Eloquent\Model;

class MetadatoTranslation extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'RDS01_id';
    protected $fillable = [
        
    ];
    protected $table = 'metadato__metadato_translations';
}
