<?php

namespace Modules\Metadato\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Tipometadato extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC05_Tipo_Metadato';
    protected $primaryKey = 'ABC05_id';
    protected $fillable = [
		"ABC05_id",
		"ABC05_nombre",
        "ABC05_attributos", 
        "ABC05_attributos_slug",
        "ABC05_nombre_slug",

    ];
    
    protected $appends = ['key', 'articulo'];

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    public function getKeyAttribute()
    {
        return "Tipo de Metadato";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }
    public function metadato()
    {
      return $this->belongsTo('Modules\Metadato\Entities\Metadato', 'ABC05_id', 'ABC05_id');
    }
}
