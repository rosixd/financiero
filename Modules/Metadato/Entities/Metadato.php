<?php

namespace Modules\Metadato\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;
class Metadato extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $table = 'ABC04_Metadatos';
    protected $primaryKey = 'ABC04_id';
    protected $fillable = [
      "ABC04_id",
      "ABC04_contenido",
      "ABC05_id", 
      "ABC04_check_banner"       
    ];
    
    protected $appends = ['key', 'articulo'];
    
    public function getKeyAttribute()
    {
        return "Metadato";
    }
    public function getArticuloAttribute()
    {
        return "el";
    }
    
    public function tipometadato()
    {
      return $this->hasOne('Modules\Metadato\Entities\Tipometadato','ABC05_id','ABC05_id')->withTrashed();
    }
    
    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
    * Scope para devolver la query para metadatos globales
    *
    * @autor Dickson Morales
    * @param QueryBuilder
    * @return QueryBuilder
    */
    public function scopeGlobal( $query ){
      return $query
        ->where("ABC04_check_banner", ">", 0);
    }
}
