<?php

namespace Modules\Metadato\Entities;

use Illuminate\Database\Eloquent\Model;

class TipometadatoTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'metadato__tipometadato_translations';
}
