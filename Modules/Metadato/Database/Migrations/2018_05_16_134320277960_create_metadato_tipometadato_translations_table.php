<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadatoTipometadatoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadato__tipometadato_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('tipometadato_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['tipometadato_id', 'locale']);
            $table->foreign('tipometadato_id')->references('id')->on('metadato__tipometadatos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadato__tipometadato_translations', function (Blueprint $table) {
            $table->dropForeign(['tipometadato_id']);
        });
        Schema::dropIfExists('metadato__tipometadato_translations');
    }
}
