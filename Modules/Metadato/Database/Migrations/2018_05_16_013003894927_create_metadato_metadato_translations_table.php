<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadatoMetadatoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadato__metadato_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('metadato_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['metadato_id', 'locale']);
            $table->foreign('metadato_id')->references('id')->on('metadato__metadatos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadato__metadato_translations', function (Blueprint $table) {
            $table->dropForeign(['metadato_id']);
        });
        Schema::dropIfExists('metadato__metadato_translations');
    }
}
