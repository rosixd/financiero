<?php

return [
    'metadato.metadatos' => [
        'index' => 'metadato::metadatos.list resource',
        'create' => 'metadato::metadatos.create resource',
        'edit' => 'metadato::metadatos.edit resource',
        'destroy' => 'metadato::metadatos.destroy resource',
    ],
    'metadato.tipometadatos' => [
        'index' => 'metadato::tipometadatos.list resource',
        'create' => 'metadato::tipometadatos.create resource',
        'edit' => 'metadato::tipometadatos.edit resource',
        'destroy' => 'metadato::tipometadatos.destroy resource',
    ],
// append


];
