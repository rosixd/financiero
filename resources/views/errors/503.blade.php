<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ Theme::url('font-awesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ Theme::url('font-awesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ Theme::url('css/estilos.css') }}">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.css') }}"/>
    <link rel="stylesheet" href="{{ asset('themes/adminlte/css/vendor/alertify/alertify.core.css') }}"/>
    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="{{ Theme::url('css/bootstrap/css/bootstrap.min.css') }}">

    <title>abc servicios financieros</title>
</head>
<body id="inicio">

    <!-- header -->

    <header class="header" id="header">
        <div class="header-top container-fluid headerMantencion"></div>

        <nav class="navbar navbar-expand-xl justify-content-between">
            <a class="navbar-brand pl-4" href="#">
                <img class="marca-top mantencion" src="http://abcdin.desarrollo.netred.cl/assets/media/logotipo-abc-nav-top.png">
            </a>
            <div class="float-right px-4">
            <a href="#" target="_blank" class="pr-2"><img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-face.gif"></a>
            <a href="#" target="_blank"><img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-twit.gif"></a>
            </div>
        </nav>
    </header>
    <!-- fin navegacion -->

    <div class="container-fluid">
        <div class="row">
            <div class="col boxInfoMantencion d-flex">
            <div class="info">
                <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-sitio-mantencion.png" width="82" height="143">
                <h1>Sitio Web<br> 
                <strong>en Mantención</strong>
                </h1>
                <p>Disculpe las molestias, estamos trabajando <br>para brindarle un mejor servicio</p>
            </div>
            </div>
        </div>
    </div>

    <section class="asymmetric"></section>

    <section class="container pb-5">
        <div class="row boxInfoContacto">
            <div class="col-md-4">
              <p>ponemos a su disposición los siguientes <br>métodos de contacto:</p>
            </div>
            <div class="col-md-4">
              <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-serv-cliente.png">
              <h2>ATENCIÓN AL CLIENTE</h2>
              <h3><a href="tel:+5626003008000">600 300 8000</a></h3>
      
            </div>
            <div class="col-md-4">
              <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-serv-contacto.png">
              <h2>CORREO DE CONTACTO</h2>
              <h3><a href="mailto:info@abcsf.cl">info@abcsf.cl</a></h3>
      
            </div>
        </div>
    </section>
    <footer>
    </footer>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
