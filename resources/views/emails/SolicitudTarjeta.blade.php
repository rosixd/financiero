<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>abcvisa - La Felicidad tiene tu Nombre</title>
      
      <style type="text/css">
         /* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #0a8cce !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}
         
         
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #0a8cce; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #0a8cce !important; 
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}
         td[class=mobile-hide]{display:none!important;}
         td[class="padding-bottom25"]{padding-bottom:25px!important;}
        
         }
      </style>
   </head>
   <body>
      <!-- Start of header -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
         <tbody>
            <tr>
               <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                     <tbody>
                        <tr>
                           <td width="100%">
                              <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                 <tbody>
                                    <!-- Spacing -->
                                    <tr>
                                       <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                    </tr>
                                    <!-- Spacing -->
                                    <tr>
                                       <td>
                                          <!-- logo -->
                                          <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                             <tbody>
                                                <tr>
                                                   <td width="600" height="330" align="center">
                                                      <div class="imgpop">
                                                         <img src="http://i.embluejet.com/ImagenesMoxie/4657/images/captacion/Act-WEB-25-02.jpg" alt="¡Bienvenido a abcvisa! Comienza marzo con los mejores beneficios." border="0" width="600" height="330" style="display:block; border:none; outline:none; text-decoration:none;">
                                                      </div>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <!-- end of logo -->
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of Header -->
      <!-- Start of Contenido -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
         <tbody>
            <tr>
               <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                     <tbody>	
                  <tr>
                           <td width="100%">
                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                 <tbody> 
                                 <tr>
                                       <!-- start of image -->
                                       <td align="center" st-image="banner-image">
                                          <div class="imgpop">
                                             <img width="600" border="0" height="1030" alt="¡Aprovecha este descuento en tu primera compra! 10% dcto. en abcdin , dijon  y abcdin.cl. Tope de descuento: $10.000." border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="http://i.embluejet.com/ImagenesMoxie/4657/images/captacion/actweb2-25-02.jpg" class="banner">
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>                     
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of Contenido -->
      <!-- Start of Contenido -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
         <tbody>
            <tr>
               <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                     <tbody>	
                  <tr>
                           <td width="100%">
                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                 <tbody> 
                                 <tr>
                                       <!-- start of image -->
                                       <td align="center" st-image="banner-image">
                                          <div class="imgpop">
                                             <a href="https://www.abcserviciosfinancieros.cl/app-tarjeta-abcvisa"><img width="600" border="0" height="200" alt="Conoce la APP abcvisa" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="http://i.embluejet.com/ImagenesMoxie/4657/images/retencion/Caluga_APP_15_02.jpg" class="banner"></a>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>                     
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of Contenido -->
      <!-- Start of seperator -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
         <tbody>
            <tr>
               <td>
                  <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                     <tbody>
                        <tr>
                           <td align="center" height="5" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>
                        <!--<tr>
                           <td width="550" align="center" height="0" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>-->
                        <tr>
                           <td align="center" height="15" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of seperator -->
      <!-- Start Full Text -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
         <tbody>
            <tr>
               <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                     <tbody>
                        <tr>
                           <td width="100%">
                              <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                 <tbody>
                                    <tr>
                                       <td>
                                          <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                             <tbody>
                                                <!-- content -->
                                                <tr>
                                                   <td style="font-family: Helvetica, arial, sans-serif; font-size: 12px; color: #889098; text-align:justify; line-height: 20px;" st-content="3col-content1">
                                                      <span style="font-weight:bold; color:#d6012b;">Condiciones Claras:</span> Descuento válido solo para la primera compra realizada con tarjeta abcvisa. Aplica sobre monto financiado con tarjeta abcvisa y será válido desde el 01 al 31 de marzo 2019. Descuento será aplicable en siguiente o subsiguiente estado de cuenta. Tope descuento $10.000.
                                                   </td>
                                                </tr>
                                                <!-- End of content -->
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                    <!-- Spacing -->
                                    <tr>
                                       <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                    </tr>
                                    <!-- Spacing -->
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- end of full text -->
      <!-- Start of seperator -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
         <tbody>
            <tr>
               <td>
                  <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                     <tbody>
                        <tr>
                           <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>
                        <tr>
                           <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>
                        <tr>
                           <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of seperator -->  
      <!-- Start of Postfooter -->
      <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter" >
         <tbody>
            <tr>
               <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                     <tbody>
                        <tr>
                           <td width="100%">
                              <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                 <tbody>
                                    <tr>
                                       <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #666666" st-content="postfooter">
                                          Este mensaje fue enviado por abcdin (abcvisa@abcdin.cl). Encuéntranos en Nueva de Lyon 72, Providencia, Región Metropolitana de Chile.
                                       </td>
                                    </tr>
                                    <!-- Spacing -->
                                    <tr>
                                       <td width="100%" height="20"></td>
                                    </tr>
                                    <!-- Spacing -->
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End of postfooter -->   
   </body>
</html>