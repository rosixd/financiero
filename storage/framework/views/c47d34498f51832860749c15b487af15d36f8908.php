<!-- header -->

<header class="header" id="header">
    <div class="header-top container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="info-servicio-top float-left negrita">
                    <img src="<?php echo e(Theme::url('img/icono-servicios-top.png')); ?>"  width="29" height="27">
                    <span>
                        <a href="tel:6008302222" style="text-decoration: none;">
                            <p>servicio al cliente</p>
                            <p class="info-fono">600 830 22 22</p>
                        </a>
                    </span>
                </div>
                <ul class="link-tiendas-top float-right">
                    <li><a href="https://www.dijon.cl" target="_blank">Dijon.cl</a></li>
                    <li><a href="https://www.abcdin.cl" target="_blank">abcdin.cl</a></li>
                </ul>
            </div>

            <div class="col-md-12 col-lg-4">
                <div class="row login-top">

                    
                    <?php if(isset($session['Logueado']) && $session['Logueado'] === 'true'): ?>
                        <?php echo $__env->make('partials.logueado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php else: ?>
                        <?php echo $__env->make('partials.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
     </div>

    <nav class="navbar navbar-expand-xl">
        <a class="navbar-brand pl-4" href="<?php echo e(route("homepage")); ?>">
            <img class="marca-top" src="<?php echo e(Theme::url('img/logotipo-abc-nav-top.png')); ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMobile" aria-controls="navbarsMobile" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-align-justify"></i></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsMobile">
            <ul class="navbar-nav mr-auto">
                <?php echo Helper::menu( 3 ); ?>	    	 
            </ul>
        </div>
    </nav>
</header>
<!-- fin navegacion -->
