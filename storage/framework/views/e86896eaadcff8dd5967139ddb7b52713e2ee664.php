<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="panel-title">
                            Test de servicios
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                            <ul class="nav nav-pills nav-stacked" id="reqTestService">
                                <?php if( isset($requerimientos) && count($requerimientos) > 0 ): ?>
                                    <?php $__currentLoopData = $requerimientos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indice => $requerimiento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="#" data-id="<?php echo e($indice); ?>">
                                                <?php echo e($requerimiento); ?>

                                            </a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="resultTestService" style="overflow:  auto;">
                       		
					    </div>   
                    </div>   
                </div>
            </div>        
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(Module::asset('dashboard:js/AppTestService.js')); ?>" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            AppTestService.urlApiReq = '<?php echo e(route("api.testSerices.getReq")); ?>';
            AppTestService.urlloaderimg = "<?php echo e(asset('themes/adminlte/img/loading.gif')); ?>";
            AppTestService.urltrueimg = "<?php echo e(asset('themes/adminlte/img/true.png')); ?>";
            AppTestService.urlfalseimg = "<?php echo e(asset('themes/adminlte/img/false.png')); ?>";
            return ;
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>