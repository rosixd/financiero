<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('page::pages.create page')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li class="active"><?php echo e(trans('page::pages.create page')); ?></li>
</ol>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<?php echo Form::open(['route' => ['admin.page.page.store'], 'method' => 'post']); ?>

<?php 
	if( !isset( $lang ) ){
		$lang = App::getLocale();
	}
 ?>
<div class="row">

    <div class="col-md-12"> 

        <div class="box box-primary">

            <div class="box-body">

                <div class="form-group">
                    <label for="title"><?php echo e(trans('page::pages.title')); ?></label>
                    <input type="text" class="form-control" id="title" name="title">
                    <span class="text-danger">
                        <?php echo e(( $errors->has('title') || $errors->has( $lang . '.title') ) ? trans('page::messages.title is required') : ''); ?>

                    </span>
                </div>

                <div class="form-group">
                    <label for="slug"><?php echo e(trans('page::pages.slug')); ?></label>
                    <input type="text" class="form-control" id="slug" name="slug">
                    <span class="text-danger">
                        <?php echo e($errors->has('slug') ? trans('page::messages.slug is required') : ''); ?>

                    </span>
                </div>

                
					
		
                <div class="form-group">
                    <label for="body">
                        <?php echo e(trans('page::pages.body')); ?>

                    </label>
                            <textarea class="ckeditor" name="body" id="body"></textarea>
                    <span class="text-danger">
                        <?php echo e(( $errors->has('body') || $errors->has( $lang . '.body') ) ? trans('page::messages.body is required') : ''); ?>

                    </span>
                </div>

                <div class="form-group">
                    <label><?php echo e(trans('page::pages.meta_data')); ?></label>
                    <select class="" multiple="multiple" name="ABC04_id[]" id="ABC04_id">
                        <option value="">--Seleccione--</option>
                        <?php $__currentLoopData = $metadatos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $metadato): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($metadato->ABC04_id); ?>"><?php echo e($metadato->ABC04_contenido); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="is_home" name="is_home" value="1">
                    <label class="form-check-label" for="status"><?php echo e(trans('page::pages.is homepage')); ?></label>
                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-md btn-primary"><?php echo e(trans('page::pages.button.save')); ?></button>
                <a href="<?php echo e(route('admin.page.page.index')); ?>" class="btn btn-md btn-danger pull-right"><?php echo e(trans('page::pages.button.cancel')); ?></a>
            </div>

        </div>

    </div>

</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
    <script>

        $( document ).ready(function() {

            $('select[multiple="multiple"]').selectize({
                plugins: ['remove_button'], 
                delimiter: ',', 
                persist: false, 
            });

            $('#title').on('keyup', function(){
                var dato =  AppGeneral.GenerateSlug( $('#title').val() );
                 $('#slug').val(dato);
            });
			 $('#slug').on('blur', function(){
                var dato =  AppGeneral.GenerateSlug( $('#slug').val() );
                 $('#slug').val(dato);
            });

        });


    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>