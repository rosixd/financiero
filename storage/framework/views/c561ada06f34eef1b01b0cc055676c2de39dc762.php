<div class="modal fade" id="modalLog" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg " style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo e(trans('core::core.modal.title log')); ?></h5>
      </div>
      <div class="modal-body">
	  	<div class="table-responsive no-padding">
	       <table class="table table-striped table-bordered table-condensed table-responsive" id="tableModalLog" style="min-width: 1300px; width: 100%;"> 
	        <thead>
	          <tr>
	            <th style="width: 20%;"><?php echo e(trans('core::core.table.resource nombre usuario')); ?></th>
	            <th style="width: 9%;"><?php echo e(trans('core::core.table.resource accion')); ?></th>
	            <th style="width: 11%;"><?php echo e(trans('core::core.table.resource fecha')); ?></th>
	            <th style="width: 16%;"><?php echo e(trans('core::core.table.resource comentario')); ?></th>
	            <th style="width: 14%;"><?php echo e(trans('core::core.table.resource navegador')); ?></th>
	            <th style="width: 22%;"><?php echo e(trans('core::core.table.resource version')); ?></th>
	            <th style="width: 6%;"><?php echo e(trans('core::core.table.resource ip')); ?></th>
	          </tr>
	        </thead>
	        <tbody>
	        </tbody>        
	      </table>
	  </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-xs" data-dismiss="modal" aria-label="Close">
        <?php echo e(trans('core::core.button.cancel')); ?>

      </button>
    </div>
  </div>
</div>
</div>