<?php $altAttribute = isset($file->translate($lang)->alt_attribute) ? $file->translate($lang)->alt_attribute : '' ?>
<div class='form-group<?php echo e($errors->has("{$lang}[alt_attribute]") ? ' has-error' : ''); ?>'>
    <?php echo Form::label("{$lang}[alt_attribute]", trans('media::media.form.alt_attribute')); ?>

    <?php echo Form::text("{$lang}[alt_attribute]", old("{$lang}[alt_attribute]", $altAttribute), ['class' => 'form-control', 'placeholder' => trans('media::media.form.alt_attribute')]); ?>

    <?php echo $errors->first("{$lang}[alt_attribute]", '<span class="help-block">:message</span>'); ?>

</div>
<?php $description = isset($file->translate($lang)->description) ? $file->translate($lang)->description : '' ?>
<div class='form-group<?php echo e($errors->has("{$lang}[description]") ? ' has-error' : ''); ?>'>
    <?php echo Form::label("{$lang}[description]", trans('media::media.form.description')); ?>

    <?php echo Form::textarea("{$lang}[description]", old("{$lang}[description]", $description), ['class' => 'form-control', 'placeholder' => trans('media::media.form.description')]); ?>

    <?php echo $errors->first("{$lang}[description]", '<span class="help-block">:message</span>'); ?>

</div>

