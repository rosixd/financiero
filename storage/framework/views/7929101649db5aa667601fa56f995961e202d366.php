<?php $__env->startSection('content-header'); ?>
    <h1>
        <?php echo e(trans('translation::translations.title.translations')); ?>

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
        <li class="active"><?php echo e(trans('translation::translations.title.translations')); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="" class="btn btn-md btn-primary jsClearTranslationCache"><i class="fa fa-refresh"></i> <?php echo e(trans('translation::translations.Clear translation cache')); ?></a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover tableTranslation" id="dataTable" >
                        <thead>
                        <tr>
                            <th>Llave</th>
                            <th>Mensaje</th>
			                
                            <th data-sortable="false"><?php echo e(trans('core::core.table.actions')); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                       <!-- <tfoot>
                        <tr>
                            <th>Llave</th>
                            <th>Mensaje</th>
			                
                            <th data-sortable="false"><?php echo e(trans('core::core.table.actions')); ?></th>
                        </tr>
                        </tfoot> -->
                    </table>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
<?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
    <?php if ($errors->has('file')): ?>

    <?php endif; ?>
    <?php $locale = locale(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startPrepend('notificaciones'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('translation.translations.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('translation.translations.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('translation.translations.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('translation.translations.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopPrepend(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>