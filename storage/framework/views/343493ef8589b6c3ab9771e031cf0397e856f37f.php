<?php $__env->startSection('content'); ?>
    <?php $__env->startComponent('page::components.slider', [
        'img' => [
            'slide-micuenta.jpg',
            'slide-micuenta.jpg'
        ]
    ]); ?>
    <?php echo $__env->renderComponent(); ?>
    <?php $__env->startComponent('page::components.barra_saludo', [
        'datospersonales' => $datospersonales
    ]); ?>
    <?php echo $__env->renderComponent(); ?>

    <?php $__env->startComponent('page::components.barra_info_titular', [
        'nombrecompleto' => array_key_exists('PrimerNombre', $datospersonales) && array_key_exists('PrimerApellido', $datospersonales) ? $datospersonales['PrimerNombre'] . ' ' . $datospersonales['PrimerApellido'] : '' ,
        'ultimostarjeta' => isset( $cupostarjetas['ultimostarjeta'] ) ? $cupostarjetas['ultimostarjeta'] : [] ,
	    'tarjetaseleccionada' => $tarjetaseleccionada,
        'cupostarjetas' => $cupostarjetas,
        'disponibleotroscomercios' => $disponibleotroscomercios,
        'extracupotienda' => $extracupotienda,
        'extracupoavance' => $extracupoavance,
        'tarjetaBloqueo' => $tarjetaBloqueo

    ]); ?>
    <?php echo $__env->renderComponent(); ?>
    <?php $__env->startComponent('page::components.barra_navegacion_titular', ['controller'=>$controller, 'tarjetaseleccionada'=>$tarjetaseleccionada]); ?>
    <?php echo $__env->renderComponent(); ?>

    <?php echo $__env->yieldContent('contentPrivate'); ?>

    <?php $__env->startComponent('page::components.barra_banners_doble', [
        'banner' => [
            (object)[
                'img' => 'banner-uso-responsable.jpg',
                'url' => '#'
            ],
            (object)[
                'img' => 'banner-beneficios2.jpg',
                'url' => '#'
            ]
        ]
    ]); ?>
    <?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>