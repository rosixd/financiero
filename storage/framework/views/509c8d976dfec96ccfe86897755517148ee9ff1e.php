<?php $__env->startSection('content-header'); ?>

<ol class="breadcrumb">
    <li><a href="<?php echo e(URL::route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li class=""><a href="<?php echo e(URL::route('admin.user.user.index')); ?>"><?php echo e(trans('user::users.breadcrumb.users')); ?></a></li>
    <li class="active"><?php echo e(trans('user::users.breadcrumb.edit-user')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo Form::open(['route' => ['admin.user.user.update', $user->id], 'method' => 'put', 'id' => 'ValidacionEditar']); ?>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1-1" data-toggle="tab"><?php echo e(trans('user::users.tabs.data')); ?></a></li>
                <li class="">
                    <a href="#tab_2-2" data-toggle="tab">
                        <?php echo e(trans('user::users.tabs.roles')); ?>

                    </a>
                </li>
                
                <li class=""><a href="#password_tab" data-toggle="tab"><?php echo e(trans('user::users.tabs.new password')); ?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('first_name', trans('user::users.form.first-name')); ?>

                                    <?php echo Form::text('first_name', old('first_name', $user->first_name), ['class' => 'form-control', 'placeholder' => trans('user::users.form.first-name')]); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('first_name', ':message'); ?>                
                                    </span>
								
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('last_name', trans('user::users.form.last-name')); ?>

                                    <?php echo Form::text('last_name', old('last_name', $user->last_name), ['class' => 'form-control', 'placeholder' => trans('user::users.form.last-name')]); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('last_name', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('email', trans('user::users.form.email')); ?>

                                    <?php echo Form::text('email', old('email', $user->email), ['class' => 'form-control', 'placeholder' => trans('user::users.form.email')]); ?>

                                    <?php if( $errors->has('email') ): ?>
                                        <span class="text-danger">
                                            <?php echo $errors->first('email', ':message'); ?>

                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <input type="hidden" value="<?php echo e($user->id === $currentUser->id ? '1' : '0'); ?>" name="activated"/>
                                    <?php $oldValue = (bool) $user->isActivated() ? 'checked' : ''; ?>
                                    <label for="activated">
                                        <input id="activated"
                                               name="activated"
                                               type="checkbox"
                                               class="flat-blue"
                                               <?php echo e($user->id === $currentUser->id ? 'disabled' : ''); ?>

                                               <?php echo e(old('activated', $oldValue)); ?>

                                               value="1" />
                                        <?php echo e(trans('user::users.form.is activated')); ?>

                                        <?php if( $errors->has('activated') ): ?>
                                            <span class="text-danger">
                                                <?php echo $errors->first('activated', ':message'); ?>

                                            </span>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?php echo e(trans('user::users.form.roles')); ?></label>
                                <select multiple="" class="form-control" name="roles[]" id="roles">
                                    <?php foreach ($roles as $role): ?>
                                        <option value="<?php echo e($role->id); ?>" <?php echo $user->hasRoleId($role->id) ? 'selected' : '' ?>><?php echo e($role->name); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php if( $errors->has('roles') ): ?>
                                    <span class="text-danger">
                                        <?php echo $errors->first('roles', ':message'); ?>

                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_3-3">
                    <?php echo $__env->make('user::admin.partials.permissions', ['model' => $user], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="tab-pane" id="password_tab">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4><?php echo e(trans('user::users.new password setup')); ?></h4>
                                <div class="form-group">
                                    <?php echo Form::label('password', trans('user::users.form.new password')); ?>

                                    <?php echo Form::input('password', 'password', '', ['class' => 'form-control alfanumerico']); ?>

                                    <?php if( $errors->has('password') ): ?>
                                        <span class="text-danger">
                                            <?php echo $errors->first('password', ':message'); ?>

                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('password_confirmation', trans('user::users.form.new password confirmation')); ?>

                                    <?php echo Form::input('password', 'password_confirmation', '', ['class' => 'form-control alfanumerico']); ?>

                                    <?php if( $errors->has('password_confirmation') ): ?>
                                        <span class="text-danger">
                                            <?php echo $errors->first('password_confirmation', ':message'); ?>

                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4><?php echo e(trans('user::users.tabs.or send reset password mail')); ?></h4>
                                <a href="<?php echo e(route("admin.user.user.sendResetPassword", $user->id)); ?>" class="btn btn-md bg-maroon">
                                    <?php echo e(trans('user::users.send reset password email')); ?>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    
                    <button type="submit" class="btn btn-primary btn-md"><?php echo e(trans('core::core.button.update')); ?></button>
                    
                    <a class="btn btn-danger pull-right btn-md" href="<?php echo e(route('admin.user.user.index')); ?>"><i class="fa fa-times"></i> <?php echo e(trans('core::core.button.cancel')); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd><?php echo e(trans('user::users.navigation.back to index')); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
<script>
$( document ).ready(function() {



  /*  $original = { 
        'first_name' : AppGeneral.GenerateSlug('<?php echo e($user->first_name); ?>'),
        'last_name'  : AppGeneral.GenerateSlug('<?php echo e($user->last_name); ?>'),
        'email'      : AppGeneral.GenerateSlug('<?php echo e($user->email); ?>'),
        'roles'      : $('#roles').val()[0],
    };
   
   
    $IDCaposFormulario = {
        'first_name' : '#first_name' ,
        'last_name'  : '#last_name'  ,
        'email'      : '#email'      ,
        'roles'      : '#roles'      ,
    };
*/
    var inicio = $("form").serialize();

    $('#ValidacionEditar').submit(function(){
        
        if($('#password').val() == ''){
            if(inicio === $("form").serialize()){
                AppGeneral.notificaciones('warning', "<?php echo e(trans('user::users.messages.validate edit')); ?>");
                return false;
            }
        }
    });    
    
   /* function _dirty($original, $datos) {  
		var dirty = false;
		
        $.each($original, function(id,valor){
			var dato = $($datos[id]).val();
			if(!$.isNumeric(dato)){
				dato = AppGeneral.GenerateSlug($($datos[id]).val());
				
			} else{

				dato = $($datos[id]).val()[0];
            }
            
            console.log( valor , dato);
            if(valor != dato){
				
                dirty = true;
            }
        });
    
       return dirty
       
    }
    */
    $('[data-toggle="tooltip"]').tooltip();
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.role.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>