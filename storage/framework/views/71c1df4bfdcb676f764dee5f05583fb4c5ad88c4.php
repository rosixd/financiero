<section class="Banners">
    <div class="container-fluid">
        <div class="row">
            <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banners): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-6 p0">
                    
                    <a href="<?php echo e($banners->url); ?>" class="bannerAnchoCompleto">
                        <img src="<?php echo e(Theme::url('img/banners/'.$banners->img)); ?>" class="img-fluid">
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>