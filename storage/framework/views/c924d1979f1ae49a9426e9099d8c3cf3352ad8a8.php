<!-- Modal formulario quiero mi tarjeta paso 1-->
<div id="ModalFormularioQuieroMiTarjeta" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                    <h2>
                        <img class="tarjeta" src="http://abcdin.desarrollo.netred.cl/assets/media/head-tarjeta.png"> Quiero mi tarjeta 
                        <img src="http://abcdin.desarrollo.netred.cl/assets/media/head-marca.png">
                    </h2>
                    <div class="pasos">
                        <span>PASO</span>
                        <ul>
                            <li class="activo">1</li>
                            <li>2</li>
                        </ul>
                    </div>
                    <div class="clearfix my-4 "></div>
                    <div class="container">
                        <div class="row">
                            <form class="formValida col" id="formulario1">
                                <div class="contenidoFormulario1 px-0">
                                    <div class="row docs pb-4">
                                        <div class="col-sm-12 col-md-6">
                                            <a href="#ModalCargo" data-toggle="modal">
                                                <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-tarjeta-cargo.png"> CARGO MENSUAL
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <a href="#ModalRequisitos" data-toggle="modal">
                                                <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-tarjeta-requisitos.png"> REQUISITOS
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="ruttarjeta">RUT&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumerosrut" id="ruttarjeta" name="ruttarjeta" placeholder="12.345.678-9">
                                            </div>
                                            <div class="alertaSerie msjrut"></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="numerodeserie">NÚMERO DE SERIE</label>
                                                <input type="text" class="form-control px-3 pb-1 alfanumerico" maxlength="10" id="numerodeserie" name="numerodeserie" placeholder="AO12345678">
                                            </div>
                                            <div class="alertaSerie msjnumserie"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="primernombre">PRIMER NOMBRE&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloLetrasinespacios" maxlength="25" id="primernombre" name="primernombre" placeholder="JOSÉ" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="segundonombre">SEGUNDO NOMBRE&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloLetrasinespacios" maxlength="25" id="segundonombre" name="segundonombre" placeholder="LUIS" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="apellidopaterno">APELLIDO PATERNO&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloLetrasinespacios" maxlength="50" id="apellidopaterno" name="apellidopaterno" placeholder="FERNANDEZ" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="apellidomaterno">APELLIDO MATERNO&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloLetrasinespacios" maxlength="50" id="apellidomaterno" name="apellidomaterno" placeholder="VALDÉZ" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="email">EMAIL&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido" id="email" name="email" maxlength="40" placeholder="josefernandez@email.cl" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="extra" for="fechadenacimiento">FECHA DE NACIMIENTO&nbsp;<span class="rojo requeridos">*</span></label>
                                                <div class="form-group date" id="datepicker"> <!-- Date input -->
                                                    <input class="form-control requerido soloFecha" id="fechadenacimiento" name="fechadenacimiento" placeholder="DD/MM/AAAA" type="text" readonly />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefonocelular">TELÉFONO CELULAR&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" maxlength="8" id="telefonocelular" name="telefonocelular" placeholder="9 9876 5432" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label class="contactoCredito" for="telefonofijo">TELÉFONO FIJO&nbsp;<span class="rojo requeridos">*</span></label>
                                                <input type="text" class="form-control px-3 pb-1 requerido soloNumeros" maxlength="9" id="telefonofijo" name="telefonofijo" placeholder="2 2345 6789" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="terminos">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input requerido" id="terminos" name="terminos">
                                            <label class="custom-control-label txt" for="terminos">He leído y acepto los términos y condiciones&nbsp;<span class="rojo requeridos">*</span></label>
                                        </div>
                                        <a href="#ModalTerminosCondiciones" data-toggle="modal">LEER ACÁ</a>
                                    </div>
                                    <input type="hidden" class="form-control px-3 pb-1" id="lugar1" name="lugar" value="1">
                                    <input type="hidden" class="form-control px-3 pb-1" id="id1" name="id">
                                    <input type="hidden" class="form-control px-3 pb-1" id="token1" name="token" >
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alertaSerie msjval"></div>
                                    </div>
                                    <div class="col-md-12">
                                        Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span><br>
                                    </div>
                                </div> 
                                <div class="clearfix"></div>
                                <br>
                                <button class="btn btn-primary btn-block btnRojo" type="submit" role="button" id="btnformulario1">SIGUIENTE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="#">
                  <img src="http://abcdin.desarrollo.netred.cl/assets/media/banner-quiero-tarjeta.jpg" class="img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Modal formulario quiero mi tarjeta paso 2-->
<div id="ModalFormularioQuieroMiTarjeta2" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                <h2>
                    <img class="tarjeta" src="http://abcdin.desarrollo.netred.cl/assets/media/head-tarjeta.png"> Quiero mi tarjeta 
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/head-marca.png">
                </h2>
                <div class="pasos">
                    <span>PASO</span>
                    <ul>
                        <li>1</li>
                        <li class="activo">2</li>
                    </ul>
                </div>
                <div class="clearfix my-4 "></div>
                <div class="container">
                    <div class="row">
                        <form class="formValida col" id="formulario2">
                            <div class="col contenidoFormulario1 px-0">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="nacionalidad">NACIONALIDAD&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="nacionalidad" name="nacionalidad">
                                                <option readonly="readonly" selected disabled>ELIGE TU NACIONALIDAD</option>
                                                <option value="Chileno" id="Chileno" >CHILENA</option>
                                                <option value="Extranjero Residente" id="Extranjero Residente" >EXTRANJERO CON RESIDENCIA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="estadocivil">ESTADO CIVIL&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="estadocivil" name="estadocivil">
                                                <option readonly="readonly" selected disabled>ELIGE TU ESTADO CIVIL</option>
                                                <?php $__currentLoopData = $estadosciviles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $estadocivil): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($estadocivil->ABC24_codigo); ?>"><?php echo e(strtoupper($estadocivil->ABC24_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="sexo">SEXO &nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="sexo" name="sexo">
                                                <option disabled readonly selected disabled>ELIGE TU SEXO</option>
                                                <option value="f">FEMENINO</option>
                                                <option value="m">MASCULINO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="boxhijos">
                                            <span class="float-left mr-2 ml-1">TIENES HIJOS&nbsp;<span class="rojo requeridos">*</span></span> 
                                            <div class="custom-control custom-radio float-left mr-3">
                                                <input type="radio" class="custom-control-input" id="hijossi" name="tienehijos" value="si">
                                                <label class="custom-control-label" for="hijossi">SI</label>
                                            </div>
                                            <div class="custom-control custom-radio float-left">
                                                <input type="radio" class="custom-control-input" id="hijosno" name="tienehijos" value="no">
                                                <label class="custom-control-label" for="hijosno">NO</label>
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group hijos">
                                            <label class="contactoCredito" for="nrohijos">¿CUANTOS?&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="nrohijos" name="nrohijos">
                                                <option disabled readonly selected disabled>ELIGE CUANTOS HIJOS TIENES</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="actividad">ACTIVIDAD O PROFESION&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="actividad" name="actividad">
                                                <option disabled readonly selected disabled>ELIGE TU ACTIVIDAD</option>
                                                <?php $__currentLoopData = $actividadeseconomicas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $actividadeconomica): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($actividadeconomica->ABC21_codigo); ?>"><?php echo e(strtoupper($actividadeconomica->ABC21_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="extra" for="fecingreso">FECHA DE INGRESO AL ACTUAL TRABAJO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <div class="form-group date" id="datepicker"> <!-- Date input -->
                                                <input class="form-control requerido" id="fecha" name="fecingreso" placeholder="DD/MM/AAAA" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="region">REGIÓN&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="region" name="region">
                                                <option selected disabled>ELIGE TU REGIÓN</option>
                                                <?php $__currentLoopData = $regiones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($region->ABC11_codigo); ?>"><?php echo e(strtoupper($region->ABC11_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="comuna">COMUNA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="comuna" name="comuna">
                                                <option selected disabled>ELIGE LA COMUNA EN LA QUE VIVES</option>
                                                <?php $__currentLoopData = $comunas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comuna): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($comuna->ABC13_codigo); ?>"><?php echo e(strtoupper($comuna->ABC13_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="calle">CALLE&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1 alfanumericoespacios requerido" maxlength="50" id="calle" name="calle" placeholder="AV. MANUEL MONTT">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="numero">NÚMERO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1 soloNumeros requerido" maxlength="12" id="numero" name="numero" placeholder="1234">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="detdireccionp">INDICACIONES DIRECCIÓN</label>
                                            <input type="text" class="form-control px-3 pb-1" id="detdireccionp" name="detdireccionp" placeholder="CUENTANOS COMO LLEGAR">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="tipoviv">TIPO DE VIVIENDA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="tipoviv" name="tipoviv">
                                                <option selected disabled>ELIGE TIPO DE VIVIENDA</option>
                                                <option value="C">CASA</option>
                                                <option value="D">DEPARTAMENTO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="extra" for="fecocupviv">FECHA OCUPACIÓN VIVIENDA&nbsp;<span class="rojo requeridos">*</span></label>
                                            <div class="form-group date" id="datepicker"> <!-- Date input -->
                                                <input class="form-control requerido" id="fecha2" name="fecocupviv" placeholder="DD/MM/AAAA" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="condhabit">CONDICIÓN HABITACIONAL&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="condhabit" name="condhabit">
                                                <option selected disabled>ELIGE CONDICIÓN HABITACIONAL</option>
                                                <?php $__currentLoopData = $condicioneshabitacionales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $condicionhabitacional): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($condicionhabitacional->ABC23_codigo); ?>"><?php echo e(strtoupper($condicionhabitacional->ABC23_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="arriendodivmensual">ARRIENDO / DIVIDENDO&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input class="form-control soloNumeros requerido" maxlength="12" id="arriendodivmensual" name="arriendodivmensual" placeholder="$200.000" type="text" />
                                        </div>
                                    </div>
                                </div>            
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="propiedades">¿TINES PROPIEDADES?&nbsp;<span class="rojo requeridos">*</span></label>
                                            <select class="form-control custom-select px-3 pb-1 requerido" id="propiedades" name="propiedades">
                                                <option selected disabled>ELIGE PROPIEDADES</option>
                                                <?php $__currentLoopData = $acreditaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $acreditacion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($acreditacion->ABC22_codigo); ?>"><?php echo e(strtoupper($acreditacion->ABC22_nombre)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>   
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="boxhijos">
                                            <span class="float-left mr-2 ml-2">¿TIENES AUTOMOVIL?&nbsp;<span class="rojo requeridos">*</span></span>
                                            <div class="custom-control custom-radio float-left mr-5">
                                                <input type="radio" class="custom-control-input" id="siautomovil" name="automovil" value="si" >
                                                <label class="custom-control-label" for="siautomovil">SI</label>
                                            </div>
                                            <div class="custom-control custom-radio float-left">
                                                <input type="radio" class="custom-control-input" id="noautomovil" name="automovil" value="no" >
                                                <label class="custom-control-label" for="noautomovil">NO</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 offset-8">
                                        <div class="form-group">
                                            <label class="contactoCredito" for="nropatente">PATENTE&nbsp;<span class="rojo requeridos">*</span></label>
                                            <input type="text" class="form-control px-3 pb-1 alfanumerico requerido" maxlength="6" id="nropatente" name="nropatente" placeholder="123456">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control px-3 pb-1" id="lugar2" name="lugar" value="2">
                                <input type="hidden" class="form-control px-3 pb-1" id="id2" name="id">
                                <input type="hidden" class="form-control px-3 pb-1" id="token2" name="token" >
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alertaSerie msjval"></div>
                                </div>
                                <div class="col-md-12">
                                    Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span><br>
                                </div>
                            </div>
                            <br>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary btn-block btnRojo" type="submit" id="btnformulario2">ENVIAR</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <div>
                <a href="#">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/banner-quiero-tarjeta.jpg" class="img-fluid" style="width: 100%;">
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Modal formulario equifax -->
<div id="ModalFormularioEquifax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal p-0">
                <div class="infomodalquieroTarjeta">
                    <div class="clearfix my-4 "></div>
                    <div class="container">
                        <div class="row">
                            <div class="col contenidoFormulario1 px-0">
                                <form id="formularioequifax">
                                    <div class="row">
                                        <div class="col-md-12 preguntasequifax">
                
                                            <div class="cajaAutorizacion">
                                                <p>Autorizo a mi AFP a entregar por intermedio de PreviRed, mis 12 últimos periodos de cotizaciones 
                                                provisionales a la institución  con el fin de ser considerados como antecedentes a esta solicitud 
                                                comercial, dando así cumplimiento al articulo 4º de la ley Nº 19.628 sobre protección de la vida 
                                                privada y a lo dispuesto en la ley Nº 19.799 sobre documentos electrónicos, firma electrónica y 
                                                servicios de certificación de dicha firma.</p>
                    
                                                <button type="button" class="btn btn-outline-primary" id="autorizo">AUTORIZO</button>
                                                <button type="button" class="btn btn-primary" id="noautorizo">NO AUTORIZO</button>
                                                <input type="hidden" id="autorizacion" name="autorizacion" value="0" >
                    
                                                <p class="aprobacion">Al autorizar podremos analizar mejor tus datos aumentando las posibilidades de aprobación de tu tarjeta</p>   
                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alertaSerie msjval"></div>
                                        </div>
                                        <div class="col-md-12">
                                            Debes ingresar todos los datos marcados como obligatorios. <span class="rojo">(*)</span><br>
                                        </div>
                                    </div>
                                    <br>
				                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="btn btn-primary btn-block btnRojo" type="submit" id="btnenviarequifax">ACEPTAR</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary btn-block btnRojo" id="btnrechazarequifax">RECHAZAR</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cargo -->
<div id="ModalCargo" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-cargo-mensual.png">
                    <h2 class="mensajeExito">CARGO MENSUAL</h2>
                    <p class="text-justify">
                        El cargo de administración mensual de Tarjeta abcvisa es un monto fijo equivalente a UF 0,2714 con un máximo anual de UF 3,2568. Este cargo tiene un plan de descuento establecido con fecha 14 de diciembre de 2017, que indica que el cargo fijo mensual para Tarjeta abcvisa será de UF 0,19. Hasta el 31 de Diciembre de 2018. Este cobro es independiente y no incluido en la tasa de interés.
                    </p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Terminos y condiciones -->
<div id="ModalTerminosCondiciones" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-cargo-mensual.png">
                    <h2 class="mensajeExito">TÉRMINOS Y CONDICIONES</h2>
                    <p class="text-justify">
                        El cargo de administración mensual de Tarjeta abcvisa es un monto fijo equivalente a UF 0,2714 con un máximo anual de UF 3,2568. Este cargo tiene un plan de descuento establecido con fecha 14 de diciembre de 2017, que indica que el cargo fijo mensual para Tarjeta abcvisa será de UF 0,19. Hasta el 31 de Diciembre de 2018. Este cobro es independiente y no incluido en la tasa de interés.
                    </p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- modal requisitos de tarjeta -->
<div id="ModalRequisitos" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente mb-3 moduloRequisitosTarjeta">
                    <img src="http://abcdin.desarrollo.netred.cl/assets/media/icono-requisitos-tarjeta.png">
                    <h2 class="mensajeExito">REQUISITOS TARJETA ABCVISA</h2>
                    <ul>
                        <li>Persona natural</li>
                        <li>Nacionalidad Chilena o Extranjero con residencia definitiva (se puede acceder sin residencia definitiva pero debe comprobar 1 año de antigüedad laboral).</li>
                        <li>Cédula de identidad vigente y sin bloqueos.</li>
                        <li>Sin morosidades y/o protestos informados vigentes.</li>
                        <li>Domicilio Acreditado y Verificado.</li>
                        <li>Teléfono particular Acreditado de red fija o celular.</li>
                        <li>Cumplir con los niveles de aprobación previstos en los análisis de Riesgo utilizados por la empresa.</li>
                        <li>Encontrarse vigente en la promoción u oferta establecida por la empresa que permita la contratación de la tarjeta de crédito.</li>
                        <li>No poseer incumplimientos de deuda previos con ABCDIN ni DIJON.</li>
                        <li>Edad mínima: Dependiente 18 años, Independiente y Jubilado 24 años.</li>
                        <li>Edad máxima: 75 años y 364 días.</li>
                        <li>Acreditar un ingreso suficiente y estable en el tiempo.</li>
                        <li>Hablar y entender el idioma español.</li>
                        <li>Acreditar toda la información precedente de manera oportuna y exacta mediante la entrega de la documentación permitida para cada caso.</li>
                    </ul>
            
                    <p class="text-left"><strong>ABCDIN se reserva el derecho de realizar modificaciones a su Política de Crédito en el momento que estime necesario. </strong> </p>
            
                    <h3>REQUISITOS ESPECIFICOS</h3>
            
                    <h5>Clientes con Acreditación de Ingresos</h5>
                    <ul>
                        <li>Dependientes: Edad mayor o igual a 18 años y hasta 75 con 364 días.</li>
                        <li>Renta mínima acreditada de 100.000</li>
                        <li>Antigüedad mínima de 6 meses.</li>
                    </ul>
            
                    <h5>Independientes:</h5>
            
                    <ul>
                        <li>Edad mayor o igual a 24 años y 75 con 364 días.</li>
                        <li>Ingresos mínimos acreditados de 100.000</li>
                        <li>Antigüedad en el giro mínima de 1 año.</li>
                    </ul>
            
                    <h5>Jubilado - Pensionado:</h5>
            
                    <ul>
                        <li>Edad mayor o igual a 24 años y 75 con 364 días</li>
                        <li>Pensión mínima acreditada de 80.000</li>
                    </ul>
            
                    <p class="text-left">Nota: Dependiendo del Flujo de Solicitudes nuestro proceso de evaluación y verificación puede tomar hasta 45 días.</p>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary btnRojo btn-lg w-50" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<div id="ModalExitoTarjeta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="<?php echo e(Theme::url('img/imagen-tarjeta-aprobada.jpg')); ?>" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>  
<div id="ModalErrorTarjeta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#"><img src="<?php echo e(Theme::url('img/imagen-tarjeta-no-aprobada.jpg')); ?>" class="img-fluid"></a>
                </div>
            </div>
        </div>
    </div>
</div>