<!-- contenidos -->
<section class="contenidosLogeado EstadoCuentaPrivado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="<?php echo e(Theme::url('img/icono-mis-ofertas-activo.png')); ?>"> Mis ofertas
                </h1>
            </div>
        </div>

        <div class="boxInfoMovimientos">
            <div class="row boxActualizaDatos">
                <div class="col-md-12">
                    <h2>Tenemos las siguientes ofertas especiales para ti</h2>
                    <div class="progress">
                        <div class="progress-bar misOfertas" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="row infoMisOfertas">
                <div class="bannerTop">
                    <a href="#">
                        <img src="<?php echo e(Theme::url('img/banners/banner-beneficios.jpg')); ?>" class="img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner1-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner2-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner3-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>
            </div>

            <div class="row infoMisOfertas">
                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner1-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner2-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>

                <div class="col-sm-12 col-md-4 boxBanner">
                    <a href="#ModalBeneficios" role="button" data-toggle="modal">
                        <img src="<?php echo e(Theme::url('img/banners/banner3-525x480.jpg')); ?>" class="img-fluid">
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>


<div id="ModalBeneficios" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <div class="infomodalserviCliente">
                    <a href="#">
                        <img src="<?php echo e(Theme::url('img/banners/banner-beneficio-ejemplo.jpg')); ?>" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>