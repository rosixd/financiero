<!DOCTYPE html>
<html>
<head>
    <base>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta id="token" name="token" value="<?php echo e(csrf_token()); ?>" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo e(url('themes/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/sweetalert/dist/sweetalert.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/fileinput/css/fileinput.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/grapesjs/dist/css/grapes.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/daterangepicker/daterangepicker-bs3.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/font-awesome/css/font-awesome.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/iCheck/skins/flat/blue.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/admin-lte/dist/css/AdminLTE.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/admin-lte/dist/css/skins/_all-skins.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/animate.css/animate.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/pace/pace.min.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('modules/core/vendor/selectize/dist/css/selectize.default.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo e(url('themes/adminlte/css/asgard.css')); ?>" media="all" rel="stylesheet" type="text/css">
    <script src="<?php echo e(url('themes/adminlte/vendor/jquery/jquery.min.js')); ?>">
    </script>
    <script>
    var Asgard = {
        backendUrl: "<?php echo e(url('backend')); ?>",
        mediaGridCkEditor : "<?php echo e(url('backend/media/media-grid/ckIndex')); ?>",
        mediaGridSelectUrl: "<?php echo e(url('backend/media/media-grid/select')); ?>",
        dropzonePostUrl: "<?php echo e(url('api/file-dropzone')); ?>",
        mediaSortUrl: "<?php echo e(url('api/media/sort')); ?>",
        mediaLinkUrl: "<?php echo e(url('api/media/link')); ?>",
        mediaUnlinkUrl: "<?php echo e(url('api/media/unlink')); ?>"
    };
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
</head>
<body class="skin-blue sidebar-mini" style="padding-bottom: 0 !important;">
    <div class="wrapper">
        <header class="main-header">
            <a class="logo" href="<?php echo e(url('backend')); ?>">
                <span class="logo-mini"></span> 
                <span class="logo-lg"></span>
            </a> 
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                 <a class="navbar-btn sidebar-toggle" data-toggle="push-menu" href="#" role="button" style="margin: 0;">
                    <span class="sr-only">Toggle navigation</span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        
                    </ul>
                </div>
            </nav>
        </header><!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <ul class="sidebar-menu">
                    
                </ul>
            </section><!-- /.sidebar -->
        </aside>
        <aside class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>500</h1>
            </section><!-- Main content -->
            <section class="content">
                <div class="error-page">
                    <h2 class="headline text-red" style="line-height: 0.6; margin-top: 0;">500</h2>
                    <div class="error-content">
                        <h3>
                            <i class="fa fa-warning text-red"></i>
                            Oops! Algo salió mal.
                        </h3>
                        <p>Un administrador fue notificado.</p>
                    </div><!-- /.error-content -->
                </div>
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                
            </div>
        </footer>
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <script src="<?php echo e(url('themes/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/sweetalert/dist/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/fileinput/js/fileinput.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/daterangepicker/moment.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/daterangepicker/daterangepicker.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/js/vendor/mousetrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/iCheck/icheck.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/datatables.net/js/jquery.dataTables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/js/vendor/jquery.slug.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('modules/core/js/keypressAction.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/admin-lte/dist/js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/admin-lte/plugins/pace/pace.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('modules/core/vendor/selectize/dist/js/standalone/selectize.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/js/main.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/vendor/grapesjs/dist/grapes.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('themes/adminlte/js/vendor/ckeditor/ckeditor.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('modules/general/js/AppGeneral.js')); ?>" type="text/javascript"></script>
    <script>
        AppGeneral.submodulo = "";
        AppGeneral.vista = "";
        AppGeneral.urlData = "";
        
        AppGeneral.notificaciones("error", "<?php echo e(isset($message) ? $message : ''); ?>");
    </script>
</body>
</html>