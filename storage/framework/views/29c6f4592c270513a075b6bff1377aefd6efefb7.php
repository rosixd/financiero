<div class="col-md-12 col-lg-8">
    <div class="row bgGriss">
        <div class="acceso logeado">
            <span>
                <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                    <a href="<?php echo e(route('Public.private.ic.movimientos')); ?>" class="cerrar">
                <?php else: ?>
                    <a href="<?php echo e(route('Public.private.pagarcuenta')); ?>" class="cerrar">
                <?php endif; ?>
                        <i class="fas fa-sign-out-alt"></i> 
                        Tu Cuenta
                    </a>
            </span>
            <span>
                <a href="<?php echo e(route('Public.private.logout')); ?>" class="cerrar">
                    <i class="far fa-times-circle"></i> 
                    Cerrar Sesi&oacute;n
                </a>
            </span>
        </div>
    </div>
</div> 
