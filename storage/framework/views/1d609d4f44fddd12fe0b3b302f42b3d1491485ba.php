<?php $__env->startSection('content-header'); ?>
    <h1>
        <?php echo e(trans('metadato::metadatos.title.metadatos')); ?>

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
        <li class="active"><?php echo e(trans('metadato::metadatos.title.metadatos')); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <?php if($currentUser->hasAccess('metadato.metadatos.create')): ?>
                    <a href="<?php echo e(route('admin.metadato.metadato.create')); ?>" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> <?php echo e(trans('metadato::metadatos.button.create metadato')); ?>

                    </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 3, "desc" ]]'>
                            <thead>
                            <tr>
                                <th><?php echo e(trans('metadato::metadatos.table.content')); ?></th>
                                <th><?php echo e(trans('metadato::metadatos.table.type')); ?></th>
				                <th><?php echo e(trans('metadato::metadatos.table.check_banner')); ?></th>
                                <th><?php echo e(trans('core::core.table.created at')); ?></th>
				                <th data-sortable="false"><?php echo e(trans('core::core.table.actions')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
<dl class="dl-horizontal">
    <dt><code>c</code></dt>
    <dd><?php echo e(trans('metadato::metadatos.title.create metadato')); ?></dd>
</dl>
<?php $__env->stopSection(); ?>

<?php $__env->startPrepend('notificaciones'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('metadato.metadatos.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('metadato.metadatos.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('metadato.metadatos.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('metadato.metadatos.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopPrepend(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>