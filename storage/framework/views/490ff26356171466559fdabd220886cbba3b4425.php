<?php if(Session::has('success')): ?>
    <?php $__env->startPush('notificaciones'); ?>
        <script>
            AppGeneral.notificaciones("success",  "<?php echo e(Session::get('success')); ?>" );
        </script>   
    <?php $__env->stopPush(); ?>


<?php endif; ?>

<?php if(Session::has('error')): ?>
    <?php $__env->startPush('notificaciones'); ?>
        <script>
            AppGeneral.notificaciones("error",  "<?php echo e(Session::get('error')); ?>");
        </script>   
    <?php $__env->stopPush(); ?>


<?php endif; ?>

<?php if(Session::has('warning')): ?>
    <?php $__env->startPush('notificaciones'); ?>
        <script>
            AppGeneral.notificaciones("warning",  "<?php echo e(Session::get('warning')); ?>");
        </script>   
    <?php $__env->stopPush(); ?>

<?php endif; ?>
