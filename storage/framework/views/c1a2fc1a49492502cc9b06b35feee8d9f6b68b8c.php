<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="push-menu" role="button" style="margin: 0;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <?php if (is_module_enabled('Notification')): ?>
            <?php echo $__env->make('notification::partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
            <li>
                <a href="" class="publicUrl" style="display: none">
                    <i class="fa fa-eye"></i> <?php echo e(trans('page::pages.view-page')); ?>

                </a>
            </li>
            <li><a href="<?php echo e(url('/')); ?>"><i class="fa fa-eye"></i> <?php echo e(trans('core::core.general.view website')); ?></a></li>
            
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span>
                        <?php if ($user->present()->fullname() != ' '): ?>
                            <?php echo e($user->present()->fullName()); ?>

                        <?php else: ?>
                            <em><?php echo e(trans('core::core.general.complete your profile')); ?>.</em>
                        <?php endif; ?>
                        <i class="caret"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                        <img src="<?php echo e($user->present()->gravatar()); ?>" class="img-circle" alt="User Image" />
                        <p>
                            <?php if ($user->present()->fullname() != ' '): ?>
                                <?php echo e($user->present()->fullname()); ?>

                            <?php else: ?>
                                <em><?php echo e(trans('core::core.general.complete your profile')); ?>.</em>
                            <?php endif; ?>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="<?php echo e(route('admin.account.profile.edit')); ?>" class="btn btn-default btn-md">
                                <?php echo e(trans('core::core.general.profile')); ?>

                            </a>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo e(route('logout')); ?>" class="btn btn-danger btn-md">
                                <?php echo e(trans('core::core.general.sign out')); ?>

                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
