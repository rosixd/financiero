<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('user::roles.title.roles')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(URL::route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li class="active"><?php echo e(trans('user::roles.breadcrumb.roles')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
				<?php if($currentUser->hasAccess('user.roles.create')): ?>
                <a href="<?php echo e(URL::route('admin.user.role.create')); ?>" class="btn btn-primary btn-md" style="padding: 4px 10px;">
                    <i class="fa fa-pencil"></i> <?php echo e(trans('user::roles.button.new-role')); ?>

                </a>
				<?php endif; ?>
            </div>
        </div>
        <div class="box box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 1, "desc" ]]'>
                    <thead>
                        <tr>
                            
                            <th><?php echo e(trans('user::roles.table.name')); ?></th>
                            <th><?php echo e(trans('user::users.table.created-at')); ?></th>
                            <th data-sortable="false"><?php echo e(trans('user::users.table.actions')); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <!--<tfoot>
                        <tr>
                            
                            <th><?php echo e(trans('user::roles.table.name')); ?></th>
                            <th><?php echo e(trans('user::users.table.created-at')); ?></th>
                            <th><?php echo e(trans('user::users.table.actions')); ?></th>
                        </tr>
                    </tfoot> -->
                </table>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
<?php echo $__env->make('core::partials.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startPrepend('notificaciones'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('user.roles.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('user.roles.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('user.roles.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('user.roles.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopPrepend(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>