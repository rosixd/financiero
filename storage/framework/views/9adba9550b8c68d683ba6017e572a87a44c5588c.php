<!-- barra navegacion titular -->
<section>
    
    <div class="container-fluid navTitular">
        <div class="row justify-content-center align-content-center">
            <ul>
                <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                    <li>
                        <a href="<?php echo e(route('Public.private.ic.movimientos')); ?>" class="<?php echo e($controller->activarmenu == 'movimientos' ? 'activo movimientos' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-movimientos.png')); ?>"> Movimientos
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('Public.private.estadosdecuenta')); ?>" class="<?php echo e($controller->activarmenu == 'estado' ? 'activo estado' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-estado.png')); ?>" > Estados de cuenta
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('Public.private.ic.vencimientos')); ?>" class="<?php echo e($controller->activarmenu == 'vencimientos' ? 'activo vencimientos' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-proximos.png')); ?>"> Pr&oacute;ximos vencimientos
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo e(route('Public.private.misofertas')); ?>" class="<?php echo e($controller->activarmenu == 'ofertas' ? 'activo ofertas' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-misofertas.png')); ?>"> Mis ofertas
                        </a>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="<?php echo e(route('Public.private.pagarcuenta')); ?>" class="<?php echo e($controller->activarmenu == 'avance' ? 'activo avance' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-avance.png')); ?>" > Paga tu cuenta
                        </a>
                    </li> 
                    <li>
                        <a href="<?php echo e(route('Public.private.estadosdecuenta')); ?>" class="<?php echo e($controller->activarmenu == 'estado' ? 'activo estado' : ''); ?>">
                            <img src="<?php echo e(Theme::url('img/icono-estado.png')); ?>" > Estados de cuenta
                        </a>
                    </li>                
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>
<!-- fin barra navegacion titular -->