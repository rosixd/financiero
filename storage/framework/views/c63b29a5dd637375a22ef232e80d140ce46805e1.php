<?php $__env->startSection('title'); ?>
    <?php echo e(trans('user::auth.resetpassword')); ?> | ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="login-logo">
        <a href="<?php echo e(url('/')); ?>"><?php echo e(setting('core::site-name')); ?></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo e(trans('user::auth.resetpassword')); ?></p>
        <?php echo $__env->make('partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo Form::open(); ?>

        <div class="alert alert-dismissible alert-danger" style="display: <?php echo e($errors->first('password') == 's' ? 'block' : 'none'); ?>">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
            Sr. Usuario, las contrase&ntilde;as no coinciden, por favor ingr&eacute;selas nuevamente.
		</div>
       

        <div class="form-group has-feedback">
            <input type="password" class="form-control alfanumerico" autofocus
                   name="password" placeholder="<?php echo e(trans('user::auth.password')); ?>">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <span class="text-danger">
                
                <?php echo e($errors->first('password') != 's' ? $errors->first('password', ':message') : ''); ?>

            </span>
        </div>
        <div class="form-group has-feedback ">
            <input type="password" name="password_confirmation" class="form-control alfanumerico" placeholder="<?php echo e(trans('user::auth.passwordconfirmation')); ?>">
           <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
           <span class="text-danger">
                <?php echo $errors->first('password_confirmation', ':message'); ?>

               
            </span>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-md pull-right">
                    Actualizar
                </button>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.account', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>