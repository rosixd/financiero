<?php $__env->startSection('content-header'); ?>
    <h1>
        <?php echo e(trans('log::logs.title.logs')); ?>

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
        <li class="active"><?php echo e(trans('log::logs.title.logs')); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">

            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4 form-inline">
                            <div class="input-group date" id="datepicker">
                                <input type="text" class="input-sm form-control" name="start" id="start" />
                        
                            </div>
                            <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Buscar</button>
                        </div>
                    </div>
			        <br>
                    <div class="table-responsive">

                        <table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 6, "desc" ]]'>
                            <thead>
                            <tr>
                                <th><?php echo e(trans('log::logs.table.event')); ?></th>
                                <th><?php echo e(trans('log::logs.table.new_values')); ?></th>
                                <th><?php echo e(trans('log::logs.table.url')); ?></th>
                                <th><?php echo e(trans('log::logs.table.ip')); ?></th>
                                <th><?php echo e(trans('log::logs.table.info_browser')); ?></th>
                                <th><?php echo e(trans('log::logs.table.coments')); ?></th>
                                <th><?php echo e(trans('log::logs.table.created_at')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <?php echo $__env->make('core::partials.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd><?php echo e(trans('log::logs.title.create log')); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>