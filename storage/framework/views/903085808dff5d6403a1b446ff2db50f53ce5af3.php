<?php $__env->startSection('title'); ?>
    <?php echo e(trans('user::auth.reset password')); ?> | ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="login-logo">
        <a href="<?php echo e(url('/')); ?>"><?php echo e(setting('core::site-name')); ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo e(trans('user::auth.to reset password complete this form')); ?></p>
        <?php echo $__env->make('partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo Form::open(['route' => 'reset.post']); ?>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" autofocus
                       name="email" placeholder="<?php echo e(trans('user::auth.email')); ?>" value="<?php echo e(old('email')); ?>">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		        <span class="text-danger">
                    <?php echo $errors->first('email', ':message'); ?>

                    
                </span>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-md pull-right">
                        <?php echo e(trans('user::auth.resetpassword')); ?>

                    </button>
                </div>
            </div>
        <?php echo Form::close(); ?>

	<br>
        <a href="<?php echo e(route('login')); ?>" class="text-center"><?php echo e(trans('user::auth.Iremembered my password')); ?></a>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.account', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>