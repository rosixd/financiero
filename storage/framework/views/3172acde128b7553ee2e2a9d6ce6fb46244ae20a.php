<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
                <h3>Detalle de Extracupo</h3>
                <?php if( !empty( $extracupotienda ) ): ?>
                    <h4 class="text-center linea">Cupo para Compras en Tiendas abcdin y Dijon</h4>
                    <?php if($cupostarjetas): ?>
                        <p class="text-center">$ <?php echo e($cupostarjetas['DisponibleTiendas']); ?></p>
                    <?php endif; ?>
                    <h4 class="text-left">Y puedes aumentarlo si:</h4>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>COMPRAS ENTRE</th>
                                <th>MONTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $extracupotienda; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $extracupotiendas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($extracupotiendas['cuotaMin']); ?> a <?php echo e($extracupotiendas['cuotaMax']); ?> cuotas</td>
                                    <td>$ <?php echo e($extracupotiendas['tope']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php if( !empty($extracupoavance) ): ?>
		
                    <h4 class="text-center linea">Cupo para Avances en efectivo</h4>
                    <?php if($cupostarjetas): ?>
                        <p class="text-center">$ <?php echo e($cupostarjetas['DisponibleAvanceEnEfectivo']); ?></p>
                    <?php endif; ?>
                    <h4 class="text-left">Y puedes aumentarlo si:</h4>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>COMPRAS ENTRE</th>
                                <th>MONTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $extracupoavance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $extracupoavances): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($extracupoavances['cuotaMin']); ?> a <?php echo e($extracupoavances['cuotaMax']); ?> cuotas</td>
                                    <td>$ <?php echo e($extracupoavances['tope']); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                <?php endif; ?> 
		<?php if( !empty($disponibleotroscomercios) ): ?> 
                    <h4 class="text-center linea">Cupo para otros comercios</h4>
                    <p class="text-center">$ <?php echo e($disponibleotroscomercios[0]); ?></p>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>