<!-- footer -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-4">
                        <div>
                            <img class="logoAbc-footer" src="<?php echo e(Theme::url('img/logotipo-abc-footer.png')); ?>" width="179" heigth="53">
                            <ul>
                                <?php echo Helper::menu(4); ?>

                            </ul>
                        </div>  
                    </div>
                    <div class="col-lg-4">
                        <div>
                            <?php if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('5')) ): ?>
                                <h3><?php echo e(strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('5')['title'])); ?></h3>
                            <?php endif; ?>
                            <ul>
                                <?php echo Helper::menu(5); ?>

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div>
                            <?php if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('6')) ): ?>
                                <h3><?php echo e(strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('6')['title'])); ?></h3>
                            <?php endif; ?>
                            <ul>
                                <?php echo Helper::menu(6); ?>

                            </ul>
                            <?php if( array_key_exists('title', \Modules\Menu\Entities\Menu::BuscarNombreMenu('7')) ): ?>
                                <h3><?php echo e(strtoupper(\Modules\Menu\Entities\Menu::BuscarNombreMenu('7')['title'])); ?></h3>
                            <?php endif; ?>
                            <ul>
                                <?php $__currentLoopData = \Modules\Menu\Entities\Menu::BuscarMenu('7'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <i class="fas fa-trophy"></i>
                                        <a href="<?php echo e($menu['url']); ?>" <?php echo isset($menu['target']) ? $menu["target"] : ''; ?>><?php echo e(strtoupper($menu['title'])); ?></a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="float-left">
                    <h2>SERVICIO AL CLIENTE</h2>
                    <h1>600 830 22 22</h1>
                    <ul class="redes-footer">
                        <li>
                            <a href="#">
                                <img src="<?php echo e(Theme::url('img/icono-facebook.png')); ?>">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="<?php echo e(Theme::url('img/icono-correo.png')); ?>">
                            </a>
                        </li>
                    </ul> 
                </div>
                <img src="<?php echo e(Theme::url('img/icono-tarjeta-footer.png')); ?>" width="105" height="97" class="tarjeta-footer">
                <div class="clearfix"></div>
                <div class="info_perdida">En caso de p&eacute;rdida, robo, hurto, adulteraci&oacute;n o falsificaci&oacute;n debes bloquear tu tarjeta 
                  llamando al <strong>600 911 140</strong>, de lunes a domingo las 24 hrs. del día.
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col legales">
                <strong>Abcdin y Din - Copyright © 2018 - Todos los derechos reservados.</strong>
                <p>
                    Instituci&oacute;n Regulada por Superintendencia de Bancos e Instituciones Financieras Chile.<br>
                    Inf&oacute;rmese sobre las entidades autorizadas para emitir Tarjetas de Pago en el país, quienes se encuentran inscritas en 
                    los Registros de Emisores de Tarjetas que lleva la SBIF, en 
                    <a href="http://www.sbif.cl" target="_blank">www.sbif.cl</a>
                </p>
            </div>
        </div>
    </div>
</footer>
  <!-- fin footer -->
