<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                    <h1>
                        <img src="<?php echo e(Theme::url('img/icono-movimientos-activo.png')); ?>"> Movimientos
                    </h1>
                <?php else: ?>
                    <h1>
                        <img src="<?php echo e(Theme::url('img/icono-pago-cuenta.png')); ?>"> Paga tu cuenta
                    </h1>
                <?php endif; ?>
            </div>
        </div>

        <div class="row boxInfoMovimientos">
            <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                <div class="col-md-6">
            <?php else: ?>
                <div class="col-md-6 offset-md-3">
            <?php endif; ?>
                <h2>Facturados</h2>
                <div class="tablaDeuda">
                    <table class="table table-reflow">
                        <tbody>
			
                             <?php if($deudapagar): ?>
                                <tr>
                                    <td class="infoleft border-top-0">Monto Facturado</td>
                                    <td class="inforight border-top-0">
                                        <span class="first">$<?php echo e($deudapagar['MontoFacturado']); ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="infoleft">Fecha Vencimiento</td>
                                    <td class="inforight">
                                        <span><?php echo e($deudapagar['FechaVencimiento']); ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="infoleft">Deuda Total</td>
                                    <td class="inforight">
                                        <span>$<?php echo e($deudapagar['DeudaTotal']); ?></span>
                                    </td>
                                </tr>
                                <?php if( $deudapagar['DeudaTotal'] != '0' || $deudapagar['MontoFacturado'] != '0' ): ?>
                                    <tr>
                                        <td colspan="2" class="border-top-0 px-0 pb-0 pt-0">
                                            <button type="button" class="btn btn-primary" onclick="window.location.href='/private/paga-tu-cuenta/'">PAGAR</button>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php else: ?>
                                <td class="infoleft border-top-0"><?php echo e(trans('page::global.servicios no disponible')); ?></td>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                </div>  
            </div>
            <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                <div class="col-md-6">
                    <div class="container boxTablaMovimientos">
                        <div class="row">
                            <h2>No facturado</h2>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>

                            <table class="points_table" id="movimientosnofacturados">
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
    <?php $__env->startPush('scripts'); ?>
        <script>
            AppMovimientos.urlmovimientosNoFacturados = "<?php echo e(route('Public.private.cuposnofacturados')); ?>";
        </script>
    <?php $__env->stopPush(); ?>
<?php endif; ?>