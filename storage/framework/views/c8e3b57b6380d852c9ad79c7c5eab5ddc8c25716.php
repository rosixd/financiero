
<!-- barra saludo -->
<section class="barraSaludo">
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <?php if(!empty($datospersonales) && array_key_exists('PrimerNombre', $datospersonales) && array_key_exists('PrimerApellido', $datospersonales) ): ?>
                <h1>¡Buenos d&iacute;as, <?php echo e($datospersonales['PrimerNombre'] . ' ' . $datospersonales['PrimerApellido']); ?> !</h1>
            <?php endif; ?>
        </div>

    </div>
    </div>
</section>
<!-- fin barra saludo -->