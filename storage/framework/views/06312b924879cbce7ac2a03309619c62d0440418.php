<!DOCTYPE html>
<html>
<head>
    <base>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta id="token" name="token" value="<?php echo e(csrf_token()); ?>" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(Theme::url('css/estilos_abcdin.css')); ?>">
</head>
<body>
    <div id="">
		<div class="divcontent">
			<h2 style="margin-top: 40px; text-align: center;"><?php echo e(trans('core::core.error 404 nuevo')); ?></h2>
			<h1><?php echo e(trans('core::core.error 404 title')); ?></h1>
			<h2 class="h2perdido" style="text-align: center;"><?php echo trans('core::core.error 404 description'); ?></h2>
			<div style="text-align: center;">
                <a class="atarjeta" href="/">
                    <img  alt="Tarjeta abcvisa" style="margin-left: 10px; margin-right: 10px;" src="/assets/media/tarjeta-404.png" >
                </a>
			</div>
			<p class="pparrafo"><?php echo trans('core::core.error 404 description1'); ?></p>
			<ul class="ullista" style="list-style-type:none;">
				<li><a href="/tarjeta-abcvisa">Tarjeta abcvisa</a></li>
				<li><a href="/seguros">Seguros</a></li>
				<li><a href="/beneficios-abcvisa">Beneficios</a></li>
				<li><a href="/garantia">Garantía Extendida</a></li>
				<li><a href="/paga-tu-cuenta">Paga tu cuenta</a></li>
				<li><a href="/asesoria-financiera">Asesoría financiera</a></li>
			</ul>
		</div>
    </div>
</body>
</html>