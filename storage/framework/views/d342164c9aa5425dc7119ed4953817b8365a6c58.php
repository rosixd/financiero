<?php $__env->startSection('title'); ?>
    <?php echo e($page->title); ?> | ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('meta'); ?>
    <meta name="title" content="<?php echo e($page->meta_title); ?>" />
    <meta name="description" content="<?php echo e($page->meta_description); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('metadatos'); ?>
	<?php 

		$metasGlobales = \Helper::metadatosGlobales();

		$metadatos = $page->Metadato()
			->getResults(); 
		
		$tagsMetadatos = '' . $metasGlobales["tags"];

		if( $metadatos->count() > 0 ){
			foreach( $metadatos as $indice => $metadato ){
				
				if( !in_array( $metadato->ABC04_id, $metasGlobales["ids"] ) ){
					$tipoMetadato = $metadato
						->tipometadato()
						->getResults();

					if( $tipoMetadato->count() > 0 ){
						$tagsMetadatos .= '<meta name="' . $tipoMetadato->ABC05_nombre . '" ' . 
							$tipoMetadato->ABC05_attributos . '="' . $metadato->ABC04_contenido . '" >';
					}
				}
			}
		}
	 ?>
	
	<?php echo $tagsMetadatos; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
        <?php echo $page->body; ?>

	<?php $__env->startSection('components.slider'); ?>
       		@ 
    	<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>