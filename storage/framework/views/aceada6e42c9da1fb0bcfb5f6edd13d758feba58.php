<div class='form-group'>
    <?php echo Form::label($settingName . "[$lang]", trans($moduleInfo['description'])); ?>

    <?php if (isset($dbSettings[$settingName])): ?>
        <?php $value = $dbSettings[$settingName]->hasTranslation($lang) ? $dbSettings[$settingName]->translate($lang)->value : ''; ?>
        <?php echo Form::textarea($settingName . "[$lang]", old($settingName . "[$lang]", $value), ['class' => 'form-control', 'placeholder' => trans($moduleInfo['description']), 'maxlength' => '500']); ?>

        <span class="text-danger">
		    <?php echo $errors->first('core::site-description.es'); ?>

	    </span>
        <?php else: ?>
        <?php echo Form::textarea($settingName . "[$lang]", old($settingName . "[$lang]"), ['class' => 'form-control', 'placeholder' => trans($moduleInfo['description'])]); ?>

    <?php endif; ?>
</div>
