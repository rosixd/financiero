<?php $__env->startSection('content-header'); ?>
    <h1>
        <?php echo e(trans('formularios::FormularioContacto.title.contacto')); ?>

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
        <li class="active"><?php echo e(trans('formularios::FormularioContacto.title.contacto')); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
   <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-inline">
                    <div class="input-group date" id="datepicker">
                        <input type="text" class="input-sm form-control" name="start" id="start" />
                
                    </div>
                    <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Buscar</button>
                    <button type="button" id="ExportExcel" class="btn btn-sm btn-success" style="float:right;">Exportar Excel</button>
                </div>
            </div>
            <br>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.nombre_formulario')); ?></th>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.nombrecompleto')); ?></th>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.rut')); ?></th>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.telefono')); ?></th>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.email')); ?></th>
                                <th><?php echo e(trans('formularios::FormularioContacto.table.tiposeguro')); ?></th>
                               
                                <th><?php echo e(trans('core::core.table.created at')); ?></th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            <tfoot>
                            <tr>
                                
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <?php echo $__env->make('core::partials.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
    <script>
        var ruta_excel = "<?php echo e(route('admin.formularios.reporte.contacto.excel')); ?>" 
    </script>
   
<?php $__env->stopPush(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd><?php echo e(trans('formularios::FormularioContacto.title.create FormularioContacto')); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>