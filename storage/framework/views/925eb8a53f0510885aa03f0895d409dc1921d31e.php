<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('menu::menu.titles.menu')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(URL::route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li class="active"><?php echo e(trans('menu::menu.breadcrumb.menu')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                <?php if($currentUser->hasAccess('menu.menus.create')): ?>
                <a href="<?php echo e(URL::route('admin.menu.menu.create')); ?>" class="btn btn-primary btn-md">
                    <i class="fa fa-pencil"></i> <?php echo e(trans('menu::menu.button.create menu')); ?>

                </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive" style="width: 100%;">
                    <table class="data-table table table-bordered table-hover"  id="dataTable"  data-order='[[ 1, "desc" ]]'>
                        <thead>
                            <tr>
                            
                                <th style="width:50%;"><?php echo e(trans('menu::menu.table.title')); ?></th>
                                <th style="width:25%;"><?php echo e(trans('menu::menu.table.create-at')); ?></th>
                                <th data-sortable="false" style="width:25%;"><?php echo e(trans('core::core.table.actions')); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
    </div>
</div>
<?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd><?php echo e(trans('menu::menu.titles.create menu')); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
<?php $locale = App::getLocale(); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.menu.menu.create') ?>" }
            ]
        });
    });
</script>
<?php $__env->stopPush(); ?>

<?php $__env->startPrepend('notificaciones'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('menu.menus.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('menu.menus.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('menu.menus.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('menu.menus.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopPrepend(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>