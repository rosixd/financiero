
<div class='form-group'>
    <?php echo Form::label("{$lang}[title]", trans('menu::menu.form.title')); ?>

    <?php echo Form::text("{$lang}[title]", old("{$lang}[title]"), ['maxlength' => '200', 'class' => 'form-control', 'placeholder' => trans('menu::menu.form.title'), 'autofocus']); ?>

    <span class="text-danger">
        <?php echo e($errors->has('es.title') ? "Sr. Usuario, debe ingresar el título." : ''); ?>

    </span>

</div>
<div class="form-group link-type-depended link-internal">
    <?php echo Form::label("{$lang}[uri]", trans('menu::menu.form.uri')); ?>

    <div class='input-group<?php echo e($errors->has("{$lang}[uri]") ? ' has-error' : ''); ?>'>
        <span class="input-group-addon">/<?php echo e($lang); ?>/</span>
        <?php echo Form::text("{$lang}[uri]", old("{$lang}[uri]"), ['class' => 'form-control', 'placeholder' => trans('menu::menu.form.uri')]); ?>

        <?php echo $errors->first("{$lang}[uri]", '<span class="help-block">:message</span>'); ?>

    </div>
</div>
<div class="form-group<?php echo e($errors->has("{$lang}[url]") ? ' has-error' : ''); ?> link-type-depended link-external">
    <?php echo Form::label("{$lang}[url]", trans('menu::menu.form.url')); ?>

    <?php echo Form::text("{$lang}[url]", old("{$lang}[url]"), ['class' => 'form-control', 'placeholder' => trans('menu::menu.form.url')]); ?>

    <?php echo $errors->first("{$lang}[url]", '<span class="help-block">:message</span>'); ?>

</div>

