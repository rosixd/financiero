<?php $__env->startSection('content-header'); ?>
<h1>
	<?php echo e(trans('page::pages.pages')); ?>

</h1>
<ol class="breadcrumb">
	<li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
	<li class="active"><?php echo e(trans('page::pages.pages')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-xs-12">	

		<div class="row">
			<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
				<?php if($currentUser->hasAccess('page.pages.create')): ?>
				<a href="<?php echo e(route('admin.page.page.create')); ?>" class="btn btn-primary btn-md" style="padding: 4px 10px;">
					<i class="fa fa-pencil"></i> <?php echo e(trans('page::pages.create')); ?>

				</a>
				<?php endif; ?>
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="data-table table table-bordered table-hover" id="dataTable" data-order='[[ 2, "desc" ]]'>
						<thead>
							<tr>
								<th><?php echo e(trans('page::pages.table.resource titulo')); ?></th>
								<th><?php echo e(trans('page::pages.table.resource url')); ?></th>
								<th><?php echo e(trans('page::pages.table.resource creado el')); ?></th>
								<th data-sortable="false"><?php echo e(trans('page::pages.table.resource accion')); ?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<!-- /.box-body -->
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
<?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('page.pages.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('page.pages.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('page.pages.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('page.pages.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>