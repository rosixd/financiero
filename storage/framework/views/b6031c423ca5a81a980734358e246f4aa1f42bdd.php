<div class="box-body">
	<div class="form-group">
		<label><?php echo e(trans('metadato::metadatos.form.content')); ?></label>
		<input type="text" class="form-control" name="ABC04_contenido" id="ABC04_contenido"  value="<?php echo e($metadato->ABC04_contenido); ?>">
		<span class="text-danger">
			<?php echo e($errors->has('ABC04_contenido') ? trans('metadato::metadatos.validation.content') : ''); ?>

		</span>
	</div>
	<label><?php echo e(trans('metadato::metadatos.form.type')); ?></label>
    <select name="ABC05_id" class="select">
        <label><?php echo e(trans('metadato::metadatos.form.type')); ?></label>
        <option value="">Seleccione</option>)
        <?php $__currentLoopData = $tipometadato; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipometadatos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option <?php echo e(($tipometadatos->ABC05_id == $metadato->ABC05_id)? "selected":""); ?>  value="<?php echo e($tipometadatos->ABC05_id); ?>"><?php echo e($tipometadatos->ABC05_nombre); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <span class="text-danger">
        <?php echo e($errors->has('ABC05_id') ? trans('metadato::metadatos.validation.type') : ''); ?>

    </span>
    <br>
	<div class="form-check">
		<input type="checkbox" <?php echo e(($metadato->ABC04_check_banner == 1) ? 'checked':''); ?> class="form-check-input" id="ABC04_check_banner" name="ABC04_check_banner" value="1">
		<label class="form-check-label" for="exampleCheck1"><?php echo e(trans('metadato::metadatos.form.check_banner')); ?>

		</label>
	</div>
</div>