<!DOCTYPE html>
<html>
    <head lang="es">
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php 
            $polifyllCssBootstrap = '';
            $polifyllJsBootstrap = '';            
            $polifyll = '';

            $metaIE = Agent::is("ie") ? '<meta http-equiv="X-UA-Compatible" content="IE=edge,11,10,9,8,7">' : '';

            /* Si se detecta internet explorer version menor a 10 se cargan polifyll */
            if( Agent::is("ie") && (Agent::version( Agent::browser() ) < 10) ){
                $polifyllCssBootstrap = '<link href="' . Theme::url( 'IE/css/bootstrap-ie9.css' ) . '" rel="stylesheet">';
                $polifyllJsBootstrap = '<script src="' . Theme::url( 'IE/js/bootstrap-ie9.js' ) . '"></script>';
                $polifyll .= '<script src="' . Theme::url( 'IE/js/html5shiv.js' ) . '"></script>' .
                    '<script src="' . Theme::url( 'IE/js/html5shiv-printshiv.js' ) . '"></script>';
            }

            if( Agent::is("ie") && (Agent::version( Agent::browser() ) > 10) ){
                $polifyllCssBootstrap .= '<link href="' . Theme::url( 'IE/css/hackIE.css' ) . '" rel="stylesheet">';

                $polifyllJsBootstrap .= '<script src="' . Theme::url( 'IE/js/hackIE.js' ) . '"></script>';
            }

         ?>
	    <?php echo $metaIE; ?>

        <title>
            <?php $__env->startSection('title'); ?><?php echo SettingDirective::show(['core::site-name']); ?><?php echo $__env->yieldSection(); ?>
        </title>
        <link rel="shortcut icon" href="<?php echo e(Theme::url('favicon.ico')); ?>">

        <!--estilos CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(Theme::url('font-awesome/css/fontawesome-all.min.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(Theme::url('css/estilos.css')); ?>">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo e(asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(asset('themes/adminlte/css/vendor/alertify/alertify.core.css')); ?>"/>
        
        <!-- bootstrap CSS -->
        <?php echo $polifyllCssBootstrap; ?>

        <?php echo $polifyll; ?>

        <link rel="stylesheet" href="<?php echo e(Theme::url( 'css/bootstrap/css/bootstrap.css' )); ?>">
        
        <!-- datepicker -->
        <link rel="stylesheet" href="<?php echo e(asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker_v3/daterangepicker.css')); ?>">

        <?php echo $__env->yieldContent('metadatos'); ?>
	    <?php if (Setting::has('core::analytics-script')): ?>
            <?php echo Setting::get('core::analytics-script'); ?>

        <?php endif; ?>
    </head>
    <body id="inicio">
        <?php echo $__env->make('partials.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <!-- contenidos -->
        <section>
            <?php echo $__env->yieldContent('content'); ?>
        </section>    

        <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldPushContent('html'); ?>

        <!-- jQuery library -->
        <script src="<?php echo e(Theme::url('js/jquery/jquery.min.js')); ?>"></script>

        <!-- Popper JS -->
        <script src="<?php echo e(Theme::url('js/popper/popper.min.js')); ?>"></script>

        <!-- Latest compiled JavaScript -->
        <script src="<?php echo e(Theme::url( 'css/bootstrap/js/bootstrap.min.js' )); ?>"></script>
        <?php echo $polifyllJsBootstrap; ?>

        <script src="<?php echo e(asset('themes/adminlte/js/vendor/alertify/alertify.js')); ?>"></script>
        <script src="<?php echo e(asset('themes/adminlte/vendor/sweetalert/dist/sweetalert.min.js')); ?>"></script>
        <script src="<?php echo e(asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker/moment.min.js')); ?>"></script>
        <script src="<?php echo e(asset('themes/adminlte/vendor/admin-lte/plugins/daterangepicker_v3/daterangepicker.js')); ?>"></script>
        <script src="<?php echo e(asset('themes/adminlte/vendor/jQuery-Mask/jquery.mask.min.js')); ?>"></script>
        <?php echo Theme::script('js/appGeneral.js'); ?>

        <?php echo Theme::script('js/appValidacion.js'); ?>

        
        <?php 
            $RequestRouteAction = request()->route()->getAction();
         ?>

        <?php if(array_key_exists('script',$RequestRouteAction)): ?>
            <?php echo Theme::script('js/front/private/'.$RequestRouteAction['modulo'].'/'.$RequestRouteAction['script'].'.js'); ?>

        <?php endif; ?>
	
        
        <script>
            var urlformulario = "<?php echo e(route('Public.formulario.guardar')); ?>";
        </script>

        
        <!-- Rut JS -->
        <script src="<?php echo e(url('js/jquery.rut.js')); ?>"></script>
        <script>
            servicionodisponible = "<?php echo e(trans('page::global.servicios no disponible')); ?>";
            transnumserietarjeta = "<?php echo e(trans('user::SolicitudDeTarjeta.no ingresado serie')); ?>";
            validaruttarjeta = "<?php echo e(route('Public.solicitudtajerta.validacliente')); ?>";
            guardarformulariotarjeta = "<?php echo e(route('Public.solicitudtajerta.guardar')); ?>";
            AppValidacion.baseurl = "<?php echo e(env('APP_URL')); ?>";
            AppValidacion.consultatarjeta = "<?php echo e(route('Public.private.ConsultaTarjetas')); ?>";
            AppValidacion.login = "<?php echo e(route('Public.private.login')); ?>";
            var appListScript = appListScript || [];
            for (var i = 0; i < appListScript.length; i++) {
                $("body").append('<script src="' + appListScript[i] + '"><\/script>');
            }

            <?php if( isset($session['TimeSession']) ): ?>
                var urlrenovar = "<?php echo e(route('Public.private.renovar')); ?>";
		
                tiemposession( <?php echo e($session['TimeSession']); ?> , <?php echo e($session['tiempo']); ?>, <?php echo e($session['tiempodealerta']); ?>);
            <?php endif; ?>

            $(document).ready(function(){
                if(( typeof AppEstadoCuenta) == 'object'){
                    AppEstadoCuenta.urlDocPdf = "<?php echo e(route('api.resource.estado_cuenta')); ?>";
                }
                
                urlApiResourcePdf = "<?php echo e(route('api.resource.getPdfImg')); ?>";
                return ;
            });
        </script>
        <?php echo $__env->yieldPushContent('scripts'); ?>
        
    </body>
</html>
