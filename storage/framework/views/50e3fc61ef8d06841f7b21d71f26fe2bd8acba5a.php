<!-- barra Info Titular -->
<section class="barraTitular">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 bggris1">
                <div>
                    
                    <?php if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABCDIN" ): ?>
                        <?php if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") ): ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                                <?php if($tarjetaBloqueo): ?>
                                    <i class="fas fa-lock"></i>
                                <?php else: ?>
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                <?php endif; ?>
                            </div>
                        <?php elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T'])): ?>
                            <div class="float-left">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                            </div>
                        <?php else: ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    
                    <?php if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABC" ): ?>
                        <?php if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") ): ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabc-habilitada.png')); ?>">
                                <?php if($tarjetaBloqueo): ?>
                                    <i class="fas fa-lock"></i>
                                <?php else: ?>
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                <?php endif; ?>
                            </div>
                        <?php elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T'])): ?>
                            <div class="float-left">
                                <img src="<?php echo e(Theme::url('img/tarjetaabc-habilitada.png')); ?>">
                            </div>
                        <?php else: ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabc-habilitada.png')); ?>">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    
                    <?php if( $tarjetaseleccionada['ValorProcesoFlujo'] === "ABCVISA" ): ?>
                        <?php if( ($tarjetaseleccionada['EstadoDeLaCuenta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaCuenta'] === "B") || ($tarjetaseleccionada['EstadoDeLaTarjeta'] === "Bloqueada" || $tarjetaseleccionada['EstadoDeLaTarjeta'] === "B") ): ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                                <?php if($tarjetaBloqueo): ?>
                                    <i class="fas fa-lock"></i>
                                <?php else: ?>
                                    <div class="clearfix"></div> 
                                    <span class="mensaje_bloqueo">
                                        Estimado cliente, su tarjeta se encuentra bloqueada. 
                                        Para mayor información puede llamar a Atención Clientes al 600 830 2222
                                    </span>
                                <?php endif; ?>
                            </div>
                        <?php elseif( $tarjetaseleccionada['EstadoDeLaTarjeta'] === "Activa" || in_array($tarjetaseleccionada['EstadoDeLaTarjeta'], ['A', 'N', 'E', 'X', 'Z', 'G', 'P', 'R', 'T'])): ?>
                            <div class="float-left">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                            </div>
                        <?php else: ?>
                            <div class="float-left tarjetaBloqueada">
                                <img src="<?php echo e(Theme::url('img/tarjetaabcdinvisa-habilitada.png')); ?>">
                                <i class="fas fa-lock">
                                    <div class="clearfix"></div> 
                                </i>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
	
                    <div class="boxtitular">
                        <h4>TITULAR</h4>
                        <h3><?php echo e($nombrecompleto); ?></h3>
                        
			
                    <?php if( !empty($ultimostarjeta) ): ?>
                        <h4>Nº DE CUENTA</h4>
                        <p>*** **** **** <?php echo e($ultimostarjeta); ?></p>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <ul>
                    <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                        <li><i class="fas fa-sync-alt"></i><a href="<?php echo e(route('Public.private.actualizadatos')); ?>">Actualizar Datos</a></li>
                    <?php endif; ?>
                    <li><i class="fas fa-file-alt"></i><a href="#">Consultar Contrato</a></li>
                </ul>        
            </div>

            <div class="col-md-6 bggris2">
                <div class="boxCupo">
                    <div class="float-left text-left">
                        <h2>Cupo Utilizado</h2>

                        <h1>$ <?php echo e(array_key_exists('CuposUtilizados', $cupostarjetas) && $cupostarjetas['CuposUtilizados'] ? $cupostarjetas['CuposUtilizados'] : '0'); ?></h1>
                    </div>

                    <div class="float-right text-right">
                        <h2>Cupo Disponible</h2>
                        <h1>$ <?php echo e(array_key_exists('CupoTotal', $cupostarjetas) && $cupostarjetas['CupoTotal'] ? $cupostarjetas['CupoTotal'] : '0'); ?></h1>
                    </div>

                    <div class="clearfix"></div>


                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?php echo e(array_key_exists('porcentaje', $cupostarjetas) && $cupostarjetas['porcentaje'] ? $cupostarjetas['porcentaje'] : '0'); ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>


                    <div class="clearfix"></div>

                    <div class="float-left text-left box diagnalBar1">
                        <p>Cupo para compras en<br>
                        Tiendas abcdin y dijon</p>
                        <h3>$ <?php echo e(array_key_exists('DisponibleTiendas', $cupostarjetas) && $cupostarjetas['DisponibleTiendas'] ? $cupostarjetas['DisponibleTiendas'] : '0'); ?></h3>
                    </div>


                    <div class="float-left text-left box diagnalBar2">
                        <p>Cupo para avances <br>en efectivo</p>
                        <h3>$ <?php echo e(array_key_exists('DisponibleAvanceEnEfectivo', $cupostarjetas) && $cupostarjetas['DisponibleAvanceEnEfectivo'] ? $cupostarjetas['DisponibleAvanceEnEfectivo'] : '0'); ?></h3>
                    </div>

                    <div class="float-left text-left box">
                        <p>Cupo para <br> otros comercios</p>
                        <h3>$ <?php echo e(array_key_exists('0', $disponibleotroscomercios) && $disponibleotroscomercios[0] ? $disponibleotroscomercios['0'] : '0'); ?></h3>
                    </div>

                    <div class="clearfix"></div>
                   
                        <a href="#myModal" class="btn btn-primary btn-sm btn-extracupo" role="button" data-toggle="modal">VER EXTRACUPO</a>
                    

                </div>


            </div>
            
            <div class="col-md-2 bggris3">
                <?php if( $tarjetaseleccionada['OrigenDeLosDatos'] == 'IC'): ?>
                    <div class="boxBloqueo">
                        <i class="fas fa-lock"></i>
                        <h2>¿PERDISTE<br>TU TARJETA?</h2>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input vchecked" <?php echo e($tarjetaBloqueo ? 'checked="checked"' : ''); ?>>
                            <span class="custom-control-indicator bdchecked"></span>
                        </label>
                        <p>BLOQUEAR</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- fin barra info Titular -->
<?php $__env->startPush('html'); ?>
    <?php echo $__env->make('page::private.extracupo.index', ['cupostarjetas' => $cupostarjetas, 'disponibleotroscomercios' => $disponibleotroscomercios, 'extracupotienda' => $extracupotienda, 'extracupoavance' => $extracupoavance ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
    <script>
        urlabloqueodesbloqueo = "<?php echo e(route('Public.private.ic.bloqueodesbloqueo')); ?>";
    </script>
<?php $__env->stopPush(); ?>