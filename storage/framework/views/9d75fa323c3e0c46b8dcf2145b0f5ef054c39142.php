<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('media::media.title.media')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li><i class="fa fa-camera"></i> <?php echo e(trans('media::media.breadcrumb.media')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('css-stack'); ?>
<link href="<?php echo Module::asset('media:css/dropzone.css'); ?>" rel="stylesheet" type="text/css" />
<style>
.dropzone {
    border: 1px dashed #CCC;
    min-height: 227px;
    margin-bottom: 20px;
}
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <form method="POST" class="dropzone">
            <?php echo Form::token(); ?>

        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive" style="width: 100%;">
                    <table class="data-table table table-bordered table-hover jsFileList" id="dataTable"  data-order='[[ 2, "desc" ]]'>
                        <thead>
                            <tr>
                                <th><?php echo e(trans('core::core.table.thumbnail')); ?></th>
                                <th><?php echo e(trans('media::media.table.filename')); ?></th>
                                <th><?php echo e(trans('core::core.table.created at')); ?></th>
                                <th data-sortable="false"><?php echo e(trans('core::core.table.actions')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <!--   <?php if ($files): ?>
                                <?php foreach ($files as $file): ?>
                                    <tr>
                                        <td>
                                            <?php if ($file->isImage()): ?>
                                                <img src="<?php echo e(Imagy::getThumbnail($file->path, 'smallThumb')); ?>" alt=""/>
                                            <?php else: ?>
                                                <i class="fa <?php echo e(FileHelper::getFaIcon($file->media_type)); ?>" style="font-size: 20px;"></i>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo e(route('admin.media.media.edit', [$file->id])); ?>">
                                                <?php echo e($file->filename); ?>

                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo e(route('admin.media.media.edit', [$file->id])); ?>">
                                                <?php echo e($file->created_at); ?>

                                            </a>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="<?php echo e(route('admin.media.media.edit', [$file->id])); ?>" class="btn btn-default btn-md"><i class="fa fa-pencil"></i></a>
                                                <button class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="<?php echo e(route('admin.media.media.destroy', [$file->id])); ?>"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?> -->
                        </tbody>
                    <!-- <tfoot>
                            <tr>
                                <th><?php echo e(trans('core::core.table.thumbnail')); ?></th>
                                <th><?php echo e(trans('media::media.table.filename')); ?></th>
                                <th><?php echo e(trans('core::core.table.created at')); ?></th>
                                <th><?php echo e(trans('core::core.table.actions')); ?></th>
                            </tr>
                        </tfoot>-->
                    </table>
                </div>
            <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('core::modalLog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
<script src="<?php echo Module::asset('media:js/dropzone.js'); ?>"></script>
<?php $config = config('asgard.media.config'); ?>
<script>
    var maxFilesize = '<?php echo $config['max-file-size'] ?>',
            acceptedFiles = '<?php echo $config['allowed-types'] ?>';
</script>
<script src="<?php echo Module::asset('media:js/init-dropzone.js'); ?>"></script>

<?php $locale = App::getLocale(); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startPrepend('notificaciones'); ?>
<script>
    AppGeneral.permisos = {
        "crear"     : <?php echo e($currentUser->hasAccess('media.medias.create') ? 'true' : 'false'); ?>,
        "editar"    : <?php echo e($currentUser->hasAccess('media.medias.edit') ? 'true' : 'false'); ?>,
        "desactivar": <?php echo e($currentUser->hasAccess('media.medias.destroy') ? 'true' : 'false'); ?>,
        "log"       : <?php echo e($currentUser->hasAccess('media.medias.index') ? 'true' : 'false'); ?>

    };
</script>
<?php $__env->stopPrepend(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>