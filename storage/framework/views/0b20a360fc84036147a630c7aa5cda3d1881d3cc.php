<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('menu::menu.titles.edit menu item')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(URL::route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li><a href="<?php echo e(URL::route('admin.menu.menu.index')); ?>"><?php echo e(trans('menu::menu.breadcrumb.menu')); ?></a></li>
    <li><?php echo e(trans('menu::menu.breadcrumb.edit menu item')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo Form::open(['route' => ['dashboard.menuitem.update', $menu->id, $menuItem->id], 'method' => 'put']); ?>

<div class="row">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <?php $i = 0; ?>
                    <?php foreach (LaravelLocalization::getSupportedLocales() as $locale => $language): ?>
                        <?php $i++; ?>
                        <div class="tab-pane <?php echo e(App::getLocale() == $locale ? 'active' : ''); ?>" id="tab_<?php echo e($i); ?>">
                            <?php echo $__env->make('menu::admin.menuitems.partials.edit-trans-fields', ['lang' => $locale], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    <?php endforeach; ?>
                    <?php echo $__env->make('menu::admin.menuitems.partials.edit-fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-md"><?php echo e(trans('core::core.button.update')); ?></button>
                        <a class="btn btn-danger pull-right btn-md" href="<?php echo e(URL::route('admin.menu.menu.edit', [$menu->id])); ?>"><i class="fa fa-times"></i> <?php echo e(trans('core::core.button.cancel')); ?></a>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo e(trans('menu::menu-items.link-type.link type')); ?></h3>
            </div>
            <div class="box-body">
                <div class="radio">
                    <input type="radio" id="link-page" name="link_type"
                           value="page"<?php echo e($menuItem->link_type === 'page' ? ' checked' : ''); ?>>
                    <label for="link-page"><?php echo e(trans('menu::menu-items.link-type.page')); ?></label>
                </div>
                <div class="radio">
                    <input type="radio" id="link-internal" name="link_type"
                           value="internal"<?php echo e($menuItem->link_type === 'internal' ? ' checked' : ''); ?>>
                    <label for="link-internal"><?php echo e(trans('menu::menu-items.link-type.internal')); ?></label>
                </div>
                <div class="radio">
                    <input type="radio" id="link-external" name="link_type"
                           value="external"<?php echo e($menuItem->link_type === 'external' ? ' checked' : ''); ?>>
                    <label for="link-external"><?php echo e(trans('menu::menu-items.link-type.external')); ?></label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd><?php echo e(trans('core::core.back to index', ['name' => 'menu'])); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js-stack'); ?>
<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.menu.menu.edit', [$menu->id]) ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
    $('input[type="checkbox"]').on('ifChecked', function(){
        $(this).parent().find('input[type=hidden]').remove();
    });

    $('input[type="checkbox"]').on('ifUnchecked', function(){
        var name = $(this).attr('name'),
            input = '<input type="hidden" name="' + name + '" value="0" />';
        $(this).parent().append(input);
    });

    $('.link-type-depended').hide();
    $('.link-<?php echo e($menuItem->link_type); ?>').fadeIn();
    $('[name="link_type"]').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_flat-blue'
    }).on('ifChecked',function(){
        $('.link-type-depended').hide();
        $('.link-'+$(this).val()).fadeIn();
    });
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>