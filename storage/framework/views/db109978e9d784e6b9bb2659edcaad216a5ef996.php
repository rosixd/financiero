<?php $__env->startSection('content-header'); ?>
<h1>
    <?php echo e(trans('user::users.title.new-user')); ?>

</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo e(URL::route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('core::core.breadcrumb.home')); ?></a></li>
    <li class=""><a href="<?php echo e(URL::route('admin.user.user.index')); ?>"><?php echo e(trans('user::users.breadcrumb.users')); ?></a></li>
    <li class="active"><?php echo e(trans('user::users.breadcrumb.new')); ?></li>
</ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
<?php $__env->stopSection(); ?>
<?php $__env->startSection('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd><?php echo e(trans('user::users.navigation.back to index')); ?></dd>
    </dl>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo Form::open(['route' => 'admin.user.user.store', 'method' => 'post']); ?>

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1-1" data-toggle="tab"><?php echo e(trans('user::users.tabs.data')); ?></a></li>
                 <li class=""><a href="#tab_2-2" data-toggle="tab"><?php echo e(trans('user::users.tabs.roles')); ?></a></li>
                
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('first_name', trans('user::users.form.first-name')); ?>

                                    <?php echo Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.first-name')]); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('first_name', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('last_name', trans('user::users.form.last-name')); ?>

                                    <?php echo Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.last-name')]); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('last_name', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php echo Form::label('email', trans('user::users.form.email')); ?>

                                    <?php echo Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.email')]); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('email', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo Form::label('password', trans('user::users.form.password')); ?>

                                    <?php echo Form::password('password', ['class' => 'form-control alfanumerico']); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('password', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo Form::label('password_confirmation', trans('user::users.form.password-confirmation')); ?>

                                    <?php echo Form::password('password_confirmation', ['class' => 'form-control alfanumerico']); ?>

                                    <span class="text-danger">
                                        <?php echo $errors->first('password_confirmation', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2-2">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo e(trans('user::users.tabs.roles')); ?></label>
                                    <select multiple="" class="form-control" name="roles[]">
                                        <?php foreach ($roles as $role): ?>
                                            <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="text-danger">
                                        <?php echo $errors->first('roles', ':message'); ?>

                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_3-3">
                    <?php echo $__env->make('user::admin.partials.permissions-create', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="box-footer">
                    <?php if($currentUser->hasAccess('user.users.create')): ?>
                    <button type="submit" class="btn btn-primary btn-md"><?php echo e(trans('user::button.create')); ?></button>
                    <?php endif; ?>
                    <a class="btn btn-danger pull-right btn-md" href="<?php echo e(URL::route('admin.user.user.index')); ?>"><i class="fa fa-times"></i> <?php echo e(trans('user::button.cancel')); ?></a>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>