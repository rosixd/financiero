<!-- contenidos -->
<section class="contenidosLogeado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="<?php echo e(Theme::url('img/icono-actualizadatos-activo.png')); ?>"> Actualiza tus datos
                </h1>
            </div>
        </div>
        <?php if( $datos['error'] === false ): ?>
        
            <form class="boxInfoMovimientos" id="formactualizadatos">
                <div class="row contenidoFormulario1 boxActualizaDatos">
                    <div class="col-md-5">
                        <h3 class="tituDatos">Datos personales</h3>
                        <div class="form-group">
                            <label for="telefonocelular">TELÉFONO CELULAR <!-- <span class="rojo requeridos">*</span> --></label>
                            <input type="text" class="form-control soloNumeros" id="telefonocelular" name="telefonocelular" placeholder="123456******" value="<?php echo e(( isset( $datoscontacto['fono'] ) ? trim($datoscontacto['fono']) : '' )); ?>"> 
                            <input type="hidden" class="form-control" id="telefonocelularoriginal" name="telefonocelularoriginal" value="X XXXX <?php echo e(substr($DatosLegal['numFono'], -4)); ?>"> 
                            <input type="hidden" class="form-control" id="smsactivo" name="smsactivo" value="<?php echo e($DatosLegal['smsActivo']); ?>"> 
                        </div>

                        <div class="clearfix"></div>
                        <br>
                        <div>
                            <h3 class="tituDatos float-left">Dirección Particular</h3>
                        </div>
                    
                        <div class="clearfix"></div>
                        
                        <div class="form-group">
                            <label for="calle">CALLE <!-- <span class="rojo requeridos">*</span> --></label>
                            <input type="text" class="form-control " id="calle" name="calle" placeholder="4 1/2 ORIENTE A 2498" value="<?php echo e(( isset( $direccionfacturacion['Calle'] ) ? trim($direccionfacturacion['Calle']) : '' )); ?>" >
                        </div>

                        <div class="form-group">
                            <label for="numero">NÚMERO <!-- <span class="rojo requeridos">*</span> --></label>
                            <input type="text" class="form-control alfanumericoespacios " id="numero" name="numero" placeholder="2345" value="<?php echo e(( isset( $direccionfacturacion['NumDireccion'] ) ? trim($direccionfacturacion['NumDireccion']) : '' )); ?>"  >
                        </div>

                        <div class="form-group">
                            <label for="depto">N DE DEPTO O CASA <!-- <span class="rojo requeridos">*</span> --></label>
                            <input type="text" class="form-control alfanumericoespacios " id="depto" name="depto" placeholder="1234" value="<?php echo e(( isset( $direccionfacturacion['DetalleDireccion'] ) ? trim($direccionfacturacion['DetalleDireccion']) : '' )); ?>" >
                        </div>

                        <div class="form-group">
                            <label for="cod_region">REGIÓN <!-- <span class="rojo requeridos">*</span> --></label>
                            <select class="form-control custom-select" id="cod_region" name="cod_region">
                                    <option <?php echo e(( $direccionfacturacion && trim($direccionfacturacion['CodigoRegion']) == '' ) ? "selected" : ""); ?> value="0" disabled>Seleccione</option>
                                <?php $__currentLoopData = $region; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $regiones): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option <?php echo e(( $direccionfacturacion && trim($direccionfacturacion['CodigoRegion']) == $regiones['ABC11_codigo'] ) ? "selected" : ""); ?> value="<?php echo e($regiones['ABC11_codigo']); ?>"><?php echo e($regiones['ABC11_nombre']); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    
                        <div>
                            <div class="form-group">
                                <label for="cod_comuna">COMUNA <!-- <span class="rojo requeridos">*</span> --></label>
                                <select class="form-control custom-select " id="cod_comuna" name="cod_comuna">
                                    <?php $__currentLoopData = $comuna; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comunas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e(( $direccionfacturacion && trim($direccionfacturacion['CodigoComuna']) == $comunas['ABC13_codigo'] ) ? "selected" : ""); ?> value="<?php echo e($comunas['ABC13_codigo']); ?>"><?php echo e($comunas['ABC13_nombre']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" id="codigo" name="codigo" value="">
                    </div>
                    <div class="col-md-5">
                        <div class="selectCorreo mb-3">
                            <h3>Envío de Estado de Cuenta por Email</h3>
                            <div class="float-left mr-3">
                                <img src="<?php echo e(Theme::url('img/icono-estadocuentamail.png')); ?>">
                            </div>   
                            <div class="float-left">
                                <p>¿Cómo desea recibir las cartolas<br> de su cuenta abcvisa?</p>
                                <div class="custom-control custom-radio">
                                    <input type="checkbox" class="custom-control-input" id="cuentaemail" name="radiostacked"  value="email" <?php echo e($getmaileecc['datos']['PINDEECCEMAIL'] == 'S' ? 'checked="checked"' : ''); ?>>
                                    <label class="custom-control-label " for="cuentaemail" disabled>Deseo recibirlas en mi cuenta de email</label>
                                </div>
                            </div>
                    
                        </div>
                        
                        <div class="checkemail d-none">
                            <div class="form-group">
                                <label for="emails">EMAIL <!-- <span class="rojo requeridos">*</span> --></label>
                                <input type="text" class="form-control " id="emails" name="emails" placeholder="PAZ*********@MAS********.CL" value="<?php echo e(( isset( $getmaileecc['datos']['PEMAIL'] ) ? trim($getmaileecc['datos']['PEMAIL']) : '' )); ?>">
                                <input type="hidden" class="form-control" id="emaillegal" name="emaillegal" value="<?php echo e($DatosLegal['email']); ?>">
                            </div>

                            <div class="form-group">
                                <label for="confirmar">ESCRIBE NUEVAMENTE EL EMAIL <!-- <span class="rojo requeridos">*</span> --></label>
                                <input type="text" class="form-control " id="confirmar" name="confirmar" placeholder="PAZ*********@MAS********.CL" value="<?php echo e(( isset( $getmaileecc['datos']['PEMAIL'] ) ? trim($getmaileecc['datos']['PEMAIL']) : '' )); ?>">
                            </div>
                        </div> 
                        <?php if( isset( $getmaileecc ) && $getmaileecc['datos']['PINDEECCEMAIL'] == 'S' ): ?>
                            <div class="row inscrito">
                                <div class="col-md-12 msjgetmail rojo"><strong><br>Ya estás inscrito para el envío de tu estado de cuenta al email que tenemos registrado.</strong></div>
                            </div>
                        <?php endif; ?>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-12 msjval"></div>
                    <div class="col-md-12 msjsmsactivo"></div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-block btnRojo mt-4" type="submit" disabled>ACTUALIZAR</button>
                    </div>
                </div>
            </form>
        <?php else: ?>
            <form class="boxInfoMovimientos">
                <div class="row contenidoFormulario2 boxActualizaDatos">
                    <div class="col-md-12">
                        <?php echo e(trans('page::global.servicios no disponible')); ?>

                    </div>
                </div>
            </form>
        <?php endif; ?>
    </div>
</section>
<?php $__env->startPush('scripts'); ?>
    <script>
        urlactualizadatos = "<?php echo e(route('Public.private.ic.actualizadatos.guardar')); ?>";
        urlvalidar = "<?php echo e(route('Public.private.ic.actualizadatos.validar')); ?>";
    </script>
<?php $__env->stopPush(); ?>


<div id="ModalExito" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
            
                <div class="infomodalserviCliente mb-3">
                    <img src="<?php echo e(Theme::url('img/icono-exito-mensaje.png')); ?>">
                    <h2 class="mensajeExito">Datos Actualizados Correctamente</h2>
                </div>
            </div>
        
        </div>
    </div>
</div>

<div id="ModalError" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
            
                <div class="infomodalserviCliente mb-3">
                    <img src="<?php echo e(Theme::url('img/icono-alerta.png')); ?>">
                    <h2 class="mensajeExito">No se han podido actualizar sus datos</h2>
                </div>
            </div>
        
        </div>
    </div>
</div>
<div id="ModalServiciomantenimiento" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal">
            
                <div class="infomodalserviCliente mb-3">
                    <img src="<?php echo e(Theme::url('img/icono-alerta.png')); ?>">
                    <h2 class="mensajeExito"><?php echo e(trans('page::global.servicios no disponible')); ?></h2>
                </div>
            </div>
        
        </div>
    </div>
</div>
<div id="ModalAutorizarTransferencia" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body contenidoModal pagoExitoso">
                <img src="<?php echo e(Theme::url('img/icono-autorizar-transferencia.png')); ?>" class="mb-3">
                <h3 class="mb-2">Autorizar Transferencia</h3>
                <p>Hemos enviado la Clave Dinámica<br>
                a tu correo electrónico <span class="rojo email"></span><br>
                y como SMS al <span class="rojo telefono">+569 ****1234</span></p>
                <form class="boxAutorizaTransferencia mt-5">
                    <p>INGRESAR CLAVE DINAMICA</p>
                    <div class="form-group">
                        <label for="clavedinamica"></label>
                        <input type="password" class="form-control" id="clavedinamica" name="clavedinamica" value="12345">
                    </div>
                    <a href="#" id="volverenviar">VOLVER A ENVIAR CLAVE</a>
                    <button class="btn btn-primary btn-block btnRojo mt-3 mb-4" type="submit">ACEPTAR</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('themes/adminlte/vendor/jQuery-Mask/jquery.mask.min.js')); ?>"></script>
    <script>
        var urlhome = "<?php echo e(route('Public.private.ic.movimientos')); ?>";
        $(function() {
            $('#telefonocelular').mask('000000000');
        });
    </script>
<?php $__env->stopPush(); ?>