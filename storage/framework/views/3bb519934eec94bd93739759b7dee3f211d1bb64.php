<!-- contenidos -->
<section class="contenidosLogeado EstadoCuentaPrivado" id="contenidosLogeado">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>
                    <img src="<?php echo e(Theme::url('img/icono-estado-cuenta-activo.jpg')); ?>"> Estado de cuenta
                </h1>
            </div>
        </div>

        <form class="boxInfoMovimientos">
            <div class="row contenidoFormulario1 boxActualizaDatos">
                <div class="col-md-6 estado_cuenta">
                    <h2>Último estado de cuenta</h2>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                    <div class="form-group">   
		                     
                        <select class="form-control custom-select" id="FormControlSelect2">
				            <?php if( empty( $data ) ): ?>
                                <option value="0">Sin Per&iacute;odos</option>
                            <?php endif; ?>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $periodo_vencimiento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($periodo_vencimiento['nombre_archivo']); ?>">Per&iacute;odo <?php echo e($periodo_vencimiento['vencimiento']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
                <input type="hidden" id="valorT" value="<?php echo e($valorT); ?>">
                <input type="hidden" id="valorR" value="<?php echo e($valorR); ?>">
                <input type="hidden" id="valorV" value="<?php echo e($valorV); ?>">
                <input type="hidden" id="valorhaydetalle" value="<?php echo e($valorhaydetalle); ?>">
                <input type="hidden" id="url" value="<?php echo e($url['url']); ?>">

                <div class="col-md-6">
                    <div class="selectCorreo bgBlanco mb-3 pl-0">
                        <a class="btnAccion" href="" download="" data-toggle="tooltip"
                        data-placement="top" title="Descargar" id="botondescargar">
                            <img src="<?php echo e(Theme::url('img/icono-descargar.png')); ?>" class="descargar">
                            Descargar
                        </a>
                        <a class="btnAccion" href="#imgPdf" data-toggle="tooltip"
                        data-placement="top" title="Imprimir" id="btnImprimirPdf">
                            <img src="<?php echo e(Theme::url('img/icono-impresion.png')); ?>" class="imprimir">
                            Imprimir
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php echo $__env->make('page::private.estado_cuenta.detalle_cuenta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startPush('scripts'); ?>   
    <script>
        AppEstadoCuenta.urlestadocuenta = "<?php echo e(route('Public.private.estadocuenta')); ?>";
    </script>
<?php $__env->stopPush(); ?>