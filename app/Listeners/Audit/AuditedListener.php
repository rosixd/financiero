<?php

namespace App\Listeners\Audit;

use Modules\Media\Entities\FileTranslation;
use OwenIt\Auditing\Events\Audited;
use OwenIt\Auditing\Models\Audit;
use Auth;
use Illuminate\Http\Request;
use Modules\User\Entities\UserToken;

class AuditedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct( Auth $auth, Request $request )
    {
        $this->auth = $auth;
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Audited  $event
     * @return void
     */
    public function handle(Audited $event)
    {	
        /*
	    if($event->model->key == "FileTranslation"){

            $trans = FileTranslation::find($event->model->id);
            $trans->file_trans_audit = $event->audit->id;
            $trans->save();                
        }
	
    	if($event->model->key =="Archivo Multimedia"){
		
        }*/
	
	    $evento = array_key_exists('last_login', $event->audit->new_values)? 'login':$event->audit->event;
	
    	$audit = Audit::find($event->audit->id);
    	$audit->mac_address = "00:00:00:00";
    	$audit->key_model = html_entity_decode($event->model->key);
	if( isset( $event->model->llave_audit ) && trim( $event->model->llave_audit ) != '' ){
		$audit->key_model = $event->model->llave_audit;
	}
    	$audit->coments = $this->coment($evento ,html_entity_decode($event->model->key));
	if( isset( $event->model->llave_audit ) && trim( $event->model->llave_audit ) != '' ){
		$audit->coments = $this->coment($evento , '');
	}
    	$audit->event = $this->events($evento);
        //$audit->ip_address = request()->server->get('HTTP_X_FORWARDED_FOR');
        
        $id = $audit->save();

       /* 
       * Se detecta si la peticion es por ajax
       * de ser asi, se debe recapturar el token de session
       * ya que viene vacio
       */
        if( $this->request->ajax() ){
            $user_token = false;

            // se verificapara para el modulo de multimedia
            if( 
                preg_match("/Archivo\sMultimedia/mi", $event->model->key ) 
            ){
                $token = preg_replace("/^bearer\s/i", "", $this->request->header('Authorization') );
                $user_token = UserToken::where("access_token", "like", trim( $token ) )
                    ->select("user_id")
                    ->first();                
            }
	    if( 
                preg_match("/translation_translation/mi", $event->model->llave_audit ) 
            ){
                $token = preg_replace("/^bearer\s/i", "", $this->request->header('Authorization') );
                $user_token = UserToken::where("access_token", "like", trim( $token ) )
                    ->select("user_id")
                    ->first();                       
	    }
	    if( 
                preg_match("/translation/mi", $event->model->llave_audit ) 
            ){
                $token = preg_replace("/^bearer\s/i", "", $this->request->header('Authorization') );
                $user_token = UserToken::where("access_token", "like", trim( $token ) )
                    ->select("user_id")
                    ->first();                       
	    }
            
            if( $user_token ){
                $audit->user_type = 'Modules\User\Entities\Sentinel\User';
                $audit->user_id = $user_token->user_id;
 
                $audit->save();   
            }
        }
        /**/
        
    }
    
    private function coment($event, $key){
    	return $this->events($event)." ".$key;
    }
    
    private function events($event){
    	switch($event){
    	  case 'restored': return 'Activaci&oacute;n';  break;
    	  case 'deleted' : return 'Desactivaci&oacute;n';  break;
    	  case 'created' : return 'Creaci&oacute;n';  break;
    	  case 'updated' : return 'Actualizaci&oacute;n';  break;
    	  case 'login'   : return 'Login'; break;
    	}
    }
}
