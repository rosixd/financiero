<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use Modules\Media\Entities\File;

class HomologaMedias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'homologadormedias';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para homologar los archivos subidos y que no se encuentren en la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        //directorio donde se guardan los archivos que se suben desde el modulo de multimedia
        $directorioMedias = public_path( config('asgard.media.config.files-path') );
        $pathMedias = config('asgard.media.config.files-path');

        $archivos = scandir( $directorioMedias );
        $fechaHora = date('Y-m-d H:i:s');

        if( $archivos && count( $archivos ) > 0 ){
            foreach( $archivos as $indice => $archivo ){
                if( trim( $archivo ) != '.' && trim( $archivo ) != '..' ){
                    $file = File::where('filename', '=', $archivo  )
                        ->first();

                    if( !$file  ){                       
    
                        File::create([
                            "is_folder" => 0,
                            'filename' => $archivo,
                            'path' => $pathMedias . $archivo,
                            'extension' => pathinfo( $directorioMedias . "/" . $archivo )["extension"],
                            'mimetype' => finfo_file( finfo_open(FILEINFO_MIME_TYPE), $directorioMedias . "/" . $archivo ),
                            'filesize' => 0,
                            'folder_id' => 0,
                            'created_at' => $fechaHora,
                            'updated_at' => $fechaHora
                        ]);
                    }
                }
            }
        }                
    }
}
