<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CargaFtp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cargaftp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generacion de txt y carga de archivos txt generados a ftp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $controller = app('Modules\User\Http\Controllers\PublicPrivate\FormulariosController');
       $controller->index();
        //echo 'hola mundo';
    }
}
