<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Trazabilidad_Servicio extends Model
{

    protected $table = 'ABC32_Trazabilidad_Servicio';
	protected $primaryKey = 'ABC32_id';
    protected $fillable = [
		"ABC32_nombre",
		"ABC32_tiempo"
    ];

    static public $before;

    static public function init() {
        self::$before = microtime(true);
    }
    
    static public function end($nombre) {

        $timeTotal = (microtime(true) - self::$before) * 1000;
        
        self::$before = 0;

        $nombre = $nombreOriginal = $nombre;
        if (starts_with($nombre, 'http')){
            $nombre = strtolower($nombre);


            $pos = strpos($nombre, '?');
            if( $pos !== false){

                $nombre = substr($nombre, 0, $pos);
            }
            $nombre = explode('/', $nombre);
            $nombre = end($nombre);
        } else {
            $pos = strpos($nombre, '(');
            $nombre = substr($nombre, 0, $pos);
            $nombre = str_replace(chr(2), '', $nombre);
            $nombre = strtolower($nombre);
        }

        $nombre = trim($nombre);
        $nombre = ltrim($nombre, '00');

        $servicios = [
            "tinfctc.tc_interfazpos3.coninfocuenta3" => "coninfocuenta3",
            "tcdistc.tc_obt_disponible" => "tcobtdisponible_ic",
            "tcdistc.tc_obt_disponible" => "tcobtdisponible_tc",
            "texettc.tc_interfazpos3.obtenerduplica" => "obtenerduplica",
            "teecc.tc_interfazpos3.eecc_tar" => "eecc_tar",
            "bdclitc.tc_interfazpos3.datospersona" => "datospersona",
            "aexectc.tc_interfazpos3.soldatavancecs" => "soldatavancecs",
            "entretfestadobloqclienteservice" => "taestadobloqclienteservice",
            "entretfdatosclienteservice" => "newoperation",
            "entretfconsultaticketeccservice" => "getticketeecc",
            "entretfactualizaestatusclienteservice" => "entretfactualizaestatusclienteservice",
            "csiv1001" => "csi",
            "entebncoperacionesmaileeccservice" => "getmaileecc",
            "tc2000_evaluacionexpress_client_ep" => "tc2000_evaluacionexpress",
            'entretfextracuposecureservice' => "sitioretfininterfazcalculoextracupo",
            "texectc.tc_interfazpos3.totobligdia5" => "totobligdia5_ic",
            //"texectc.tc_interfazpos3.totobligdia5" => "totobligdia5_tc",
            'entretfcreaticketeeccsecureservice' => "sitioretfininterfazcreaticketeecc",
            "gexectc.tc_interfazpos3.consultaflujoeecc" => "consultaflujoeecc",
            "clivapa.pa_interfaztransact.esclientevalido_tc_ic" => "esclientevalidoweb",
            "entebncoperacionesdatoslegalsecureservice" => 'getdatoslegal',
            "smsws" => 'smsws',
            "actualizaclienteicport" => 'actualizaclienteic',
            "entebncoperacionesmaileeccservice" => 'mergemaileecc',
            "entretfbloqueodesbloqicservice" => 'bloqueodes',
            "service" => 'polaris',
            "entregistronavegacionservice" => 'trazabilidad',
            "iframe_privado" => 'pagatucuenta',
            "service" => 'processmessage',
            "entreftmantienecuentasecureservice" => 'entreftmantienecuentaservice',
            "estadocuenta.aspx" => 'estadocuenta',
            "eidrenta" => 'idverifier'
        ];

        if ($nombre === 'csiservicemanager') {
            $servicio = $nombre;
        } else {
            $servicio = $servicios[$nombre];
            
            $cmdPutMetric = "aws cloudwatch put-metric-data --region us-east-1 --metric-name Latency --namespace ServiciosFinancieros --unit Milliseconds --value " . $timeTotal . " --dimensions Servicio=" . $servicio;
            //$output = shell_exec($cmdPutMetric);
        }

        self::create([
            "ABC32_nombre" => $servicio,
            "ABC32_tiempo" => $timeTotal
        ]);
    }
}