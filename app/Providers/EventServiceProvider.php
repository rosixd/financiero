<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        
	   \OwenIt\Auditing\Events\Audited::class => [
            \App\Listeners\Audit\AuditedListener::class 
        ], 
        \OwenIt\Auditing\Events\Auditing::class => [
            \App\Listeners\Audit\AuditingListener::class
        ]
    ];
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
