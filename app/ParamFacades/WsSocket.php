<?php
namespace App\ParamFacades;

use Config;
use App\ParamFacades\SoapClient;
use Carbon\Carbon;
use SimpleXMLElement;
use Illuminate\Support\Collection;
use Jenssegers\Agent\Agent;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Session;
use App\Trazabilidad_Servicio;

class WsSocket
{
    /**
     * DMT: Metodo de conexion general 
     * 
     * @param String Trama que se envia al socket
     * @return Array
     */
    protected static function conexionSocket($conexion1 = '', $contador = false, $expresionRegular = null)
    {
        error_reporting(E_ALL);
        self::log("inicio de:". $conexion1['trama']);
        try {
            //$conexion1 = Config::get("configSocket.MW-CONINFOCUENTA3");
        
            $trama = $conexion1['trama'];
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			
            if( $contador ) {
                $contador = ( $contador > 0 ) ? $contador : 2;

                //socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => $contador, 'usec' => 0]);
            }
            
            if ($socket === false) {
                return [
                    "error" => true,
                    "msg" => "Error al tratar de crear la conexion: " . socket_strerror(socket_last_error()),
                    'original-data' => "Error al tratar de crear la conexion: " . socket_strerror(socket_last_error()),
                ];
            } 
            Trazabilidad_Servicio::init();

            $result = socket_connect($socket, $conexion1["host"], $conexion1["puerto"]);
            if ($result === false) {
                return [
                    "error" => true,
                    "msg" => "Error al tratar de conectar: ( $result ) " . socket_strerror(socket_last_error($socket)),
                    'original-data' => "Error al tratar de conectar: ( $result ) " . socket_strerror(socket_last_error($socket)),
                ];
            } 
            $out = '';

            /*Enviamos el comando o peticion al Fiscal printer por via TCP*/
            $trama = chr(2) . $trama . chr(10) . chr(3);
            
            socket_write($socket, $trama, strlen($trama));
            $ite = 0;
            
            $cont = 0;
            $respuesta = '';
            $array_rep= [];
            
            while ($out = socket_read($socket, 8192)) {
                $respuesta .= $out;
                $array_rep[$ite++] = $out;

                if ($expresionRegular && preg_match($expresionRegular, $respuesta, $match)) {
                    //echo $ite . "--" . $out . "--\n";
                    break;
                }

                if (strpos($out, chr(3)) !== false) {
                    break;
                }
            }
            $errorSocket = @socket_last_error($socket);
            $errormsg = @socket_strerror($errorSocket);

            socket_close($socket);
            Trazabilidad_Servicio::end($trama);

		    $error = false;
		    $msg = '';
        	$estatus_cliente = '00';
        
            if($respuesta == ''){ 
                $error = true;
                $msg = 'error servicio';
		        $estatus_cliente = 99;
            }
       
        } catch (\Exception $ex) {
            self::log([
                'trama' => $trama,
                "error" => true,
                "ip"     => $conexion1["host"],
                'puerto' => $conexion1["puerto"],
                "msg" => $ex->getMessage(),
                'original-data' => $ex->getMessage(),
                "Conexion" => $conexion1,
                "fin de " => $trama
            ]);
            
            return [
                'error' => true,
                'msg'  => $ex->getMessage(),
                'original-data' => $ex->getMessage(),
                'Exception'=>true,
                'Conexion' => $conexion1,
                'estatus_cliente' => '99'
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
        
        self::log([
            'trama'  => $trama,
            "ip"     => $conexion1["host"],
            'puerto' => $conexion1["puerto"],
            "error"  => $error,
            "msg"    => $msg,
            //'original-data' => mb_detect_encoding($respuesta) != 'UTF-8' ? $respuesta : utf8_encode($respuesta),
            //"original-data" =>  utf8_encode($respuesta),
            "datos"  => $array_rep,
            "fin de "=> $trama

        ]);
        
        return [
            "error" => $error,
            "msg" => $msg,
            
            'original-data' => mb_detect_encoding($respuesta) != 'UTF-8' ? $respuesta : utf8_encode($respuesta),
            //"original-data" =>  utf8_encode($respuesta),
            "datos" => $array_rep,
	    "estatus_cliente" => $estatus_cliente
        ];
    }
    
    /**
     * MA: Metodo de conexion para web services
     * @param Array  $param Parametros que se envia al  web services
     * @param Array  $metodo metodo del web service 
     * @param Array  $config conexion del web service 
     * @return Array
     */
    protected static function conexionWebService($config = '', $param = '', $metodo= '', $prueba = false, $login = false, $option = false)
    {
        //error_reporting(E_ALL);

        try {
            $url = $config['url'];
            $datos = [];
            
            $login = ( 
                (
                    array_key_exists("Auth", $config ) && 
                    $config["Auth"] &&
                    array_key_exists("login", $config ) && 
                    array_key_exists("password", $config )
                ) 
                ? true 
                : $login 
            );
            Trazabilidad_Servicio::init();
            if($login){
                $options = $option;
		
                if($config['login'] != ""){
                    $options['login'] = $config['login'];
                    $options['password'] = $config['password'];
                }
                $options["trace"] = 1;
                $options["exceptions"] = true;

                $options["soap_version"] = SOAP_1_1;
                $options["cache_wsdl"] = WSDL_CACHE_NONE;
                
                if (starts_with($url, 'https')) {
                    $options["stream_context"] = stream_context_create([
                        'ssl' => [
                            // set some SSL/TLS specific options
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true
                        ]
                    ]);
		
                    $cliente = new SoapClient($url, $options);
                }else{
                    $cliente = new SoapClient($url, $options);
                }

            }elseif( $option ){
                $option["trace"] = true;
                $option["exceptions"] = true;
                $option["connection_timeout"] = 5;
                $cliente = new SoapClient($url, $option);
            }else{
                $cliente = new SoapClient($url, [ "trace" => true , "exceptions" => true, "connection_timeout" => 5 ]);
            }
			
            $datos = [];
	 
            if($prueba){
                dd($cliente->__getFunctions());
            }

            $respuesta = $cliente->__soapCall($metodo, array($param));
            Trazabilidad_Servicio::end($url);
            $datos = json_decode(json_encode($respuesta), true);
            
        }catch( FatalErrorException $fault ){
            self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => []
            ]);

            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'Conexion' => $config,
                'datos' => []
            ];
        }catch (\SoapFault $fault) {
			self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => [],
            ]);
            
            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'Conexion' => $config,
                'datos' => []
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }catch (\Exception $fault) {
            self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => $datos,
                'Conexion' => $config,
            ]);
            
            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'datos' => []
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
       
        self::log([
            'url' => $url,
            'error' => false,
            'msg'   => '',
            'estatus_cliente' => 01,
            'response' => $datos
        ]);
        return [
            'error' => false,
            'msg'   => '',
            'estatus_cliente' => 01,
            'datos' => $datos
        ]; 
    }


    /**
     * MA: metodo de conexion para equifax
     * 
     */


    protected static function conexionWebServiceEquifax($config = '', $param = '', $metodo= '', $prueba = false)
    {
        //error_reporting(E_ALL);
        try {
            //dd($config);
            $url = $config['url'];
            $options=[];
            //$options = $option;
            $datos  = '';
            //$options['login'] = $config['login'];
            //$options['password'] = $config['password']; 
            $options["trace"] = 1;
            $options["exceptions"] = true;
            $options["stream_context"] = stream_context_create([
                'ssl' => [
                    // set some SSL/TLS specific options
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ]
            ]);
            Trazabilidad_Servicio::init();
            $ns = "http://eid.equifax.com/soap/schema/chile";

            $cliente = new SoapClient($url, $options);
            
            $xml = '
            <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <wsse:UsernameToken>
                    <wsse:Username>' .$config['login'] . '</wsse:Username>
                    <wsse:Password>' . $config['password'] . '</wsse:Password>
                    <wsse:Nonce></wsse:Nonce>
                    </wsse:UsernameToken>
                    </wsse:Security>
                    ';
                
            $headers = new \SoapHeader($ns, 'Security',  new \SoapVar($xml, XSD_ANYXML) );
                   
            $cliente->__setSoapHeaders($headers);
           
            if($prueba){
                dd($cliente->__getFunctions());
            }
            if($metodo === 'startTransaction'){

                $respuesta = $cliente->__soapCall($metodo, array($param));
            }else{

                $respuesta = $cliente->__soapCall($metodo, $param);
            }
            
            Trazabilidad_Servicio::end($url);
            $datos = json_decode(json_encode($respuesta), true);
            
        }catch( FatalErrorException $fault ){
            self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => []
            ]);

            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'Conexion' => $config,
                'datos' => []
            ];
        }catch (\SoapFault $fault) {
			self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => [],
            ]);
       
            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'Conexion' => $config,
                'datos' => []
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }catch (\Exception $fault) {
            self::log([
                'url' => $url,
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'response' => $datos,
                'Conexion' => $config,
            ]);
               
            return [
                'error' => true,
                'msg'   => $fault->getMessage(),
                'estatus_cliente' => 99,
                'Exception'=>true,
                'datos' => []
            ];
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
       
       /*
        self::log([
            'url' => $url,
            'error' => false,
            'msg'   => '',
            'estatus_cliente' => 01,
            'response' => $datos
        ]);
	*/
	
        return [
            'error' => false,
            'msg'   => '',
            'estatus_cliente' => 01,
            'datos' => $datos
        ]; 
    }

    protected static function conexionCurlXml($url, $xml)
    {
        //iniciamos curl
        $ch = curl_init();
        
        //seteamos la url
        curl_setopt($ch, CURLOPT_URL, $url);
       
        //seteamos los datos post asociados a la variable xmlData
        $xml = trim(preg_replace("/\s+/", " ", $xml));
		$xml = "<?xml version='1.0' encoding='ISO-8859-1'?>\n" . 
				str_replace("> <", "><", $xml);
    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
     
            
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        //tiempo de espera, 0 infinito
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
         
        //ejecutamos la petición y asignamos el resultado a $data
        Trazabilidad_Servicio::init();
        $data = curl_exec($ch);
        
        //cerramos la petición curl
        curl_close($ch);
        Trazabilidad_Servicio::end($url);
        self::log([
            'url'  => $url,
            'request'  => $xml,
            'response' => $data
        ]);
		
        return $data;
    }

    public static function milliways($conexion)
    {
        $host   = $conexion['host'];
        $puerto = $conexion['puerto'];
        $trama  = $conexion['trama'];

        $pathJava = resource_path('java/milliways.jar');
        $comando = 'java -jar ' . $pathJava . " " . $host . " " . $puerto . " \"" . $trama . "\"";
        
        $respuesta = shell_exec($comando);
        //$respuesta = str_replace("\n", '', $respuesta);
        //$respuesta = trim($respuesta);

        return $respuesta;
    }

    /**
     * DMT: Metodo para teatear la conexion de miliways
     * 
     * @return Array
     */
    public static function testConexion()
    {
        error_reporting(E_ALL);
        $conexion1 = Config::get("configSocket.conexion1");
       
        return self::conexionSocket( $conexion1["trama"] );
    }
       
    
    /**
     * MA: Metodo de conexion para el miliways ConInfoCuenta3
     * 
     * @param String rut del cliente
     * @return Array
     */
    public static function ConInfoCuenta3($rut)
    {
        error_reporting(E_ALL);
        try {

            $conexion = Config::get("configSocket.MW-CONINFOCUENTA3");

            $conexion["trama"] = str_replace('{RUT}',$rut,$conexion["trama"]);

          	$respuesta =[];
            $respuesta = self::conexionSocket( $conexion );

            if($respuesta['error']){
                $respuesta['estatus_cliente'] = 99;

                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => '8', 
                    "deTransaccion"        => 'Consulta de informacion de tarjetas', 
                    "cdError"              => 99,
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta["original-data"], 
                ];
                self::Trazabilidad($datos);
                return  $respuesta;
            }
    
            $_respuesta = self::ConInfoCuenta3FormatData($respuesta["original-data"]);
    
            if($_respuesta['error']){
                switch ($_respuesta['data']) {
                    case '99':

                        $_respuesta['msg'] = trans('page::global.servicios no disponible');
                        $_respuesta['error'] = true;
                        $_respuesta['estatus_cliente'] = '99';
                        
                        $datos = [
                            "rut"                  => $rut, 
                            "cdTransaccion"        => '8', 
                            "deTransaccion"        => 'Consulta de informacion de tarjetas', 
                            "cdError"              => 99,
                            "cd_accion"            => 0,
                            "de_accion"            => 'Login', 
                            "entradaTransaccion"   => $conexion["trama"], 
                            "salidaTransaccion"    => $respuesta["original-data"], 
                        ];
                        self::Trazabilidad($datos);
                    
                        return $_respuesta;
                        
                        break;
                    case '1':
                        $_respuesta['msg'] = 'Se sugiere invitar al cliente a solicitar una tarjeta abc visa';
                        $_respuesta['estatus_cliente'] = '01';

                        return $_respuesta;
                        break;
                }
            }
            $respuesta['datos'] = $_respuesta['data'];

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        return $respuesta;
    }

    /**
     * MA: Metodo de conexion para el miliways OBTENERDUPLICA
     * 
     * @param String rut del cliente
     * @return Array
     */

    public static function OBTENERDUPLICA($CodigoEmpresa ,$Cuenta, $rut = 19)
    {
        error_reporting(E_ALL);
        
        try {
            $conexion = Config::get("configSocket.MW-OBTENERDUPLICA");
            //'trama' => "TEXETTC.TC_INTERFAZPOS3.OBTENERDUPLICA('{empresa}','{cuenta}','{comercio}','')"

            $conexion["trama"] = str_replace('{empresa}', str_pad($CodigoEmpresa, 4, "0", STR_PAD_LEFT) ,$conexion["trama"]);
            $conexion["trama"] = str_replace('{cuenta}',  $Cuenta, $conexion["trama"]);
            $conexion["trama"] = str_replace('{comercio}','648', $conexion["trama"]);

            $respuesta = self::conexionSocket( $conexion, true );
           
            $respuesta["datos"] = self::FormatData($respuesta["datos"]);
            
            if($respuesta['error']){
                return  $respuesta;
            }
            $respuesta['codigo'] = substr($respuesta['datos'][0], -2);
            switch (substr($respuesta['datos'][0], -2)) {
                case '99':
                    $respuesta['error'] = true;
                    $respuesta['msg']   = 'Problemas de comunicación (Milliways)';
                    
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 6, 
                        "deTransaccion"        => 'Consulta Cupo Otros comercios', 
                        "cdError"              => 99,
                        "cd_accion"            => 1,
                        "de_accion"            => 'Cupos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta["datos"], 
                    ];
                    self::Trazabilidad($datos);

                    break;
                case '01':
                    $respuesta['error'] = true;
                    $respuesta['msg']   = 'Error, Avance no puede realizarse';
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 6, 
                        "deTransaccion"        => 'Consulta Cupo Otros comercios', 
                        "cdError"              => '',
                        "cd_accion"            => 1,
                        "de_accion"            => 'Cupos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta["datos"], 
                    ];
                    self::Trazabilidad($datos);
                    break;
            }
        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            
            return $respuesta;
        }
        
        return $respuesta;
    }
    /**
     * MA: Metodo de conexion para el miliways totobligdia5
     * 
     * @param String rut del cliente
     * @return Array
     */
    public static function totobligdia5($CodigoEmpresa ,$tarjeta, $tipoCuneta, $rut = "19")
    {
        error_reporting(E_ALL);
        try {
           
    
            if($tipoCuneta == 'IC'){
                $conexion = Config::get("configSocket.MW-totobligdia5-IC");
            }else if ($tipoCuneta == 'TC'){
                $conexion = Config::get("configSocket.MW-totobligdia5-TC");
            }
    
            $conexion["trama"] = str_replace('{codEmpresa}', $CodigoEmpresa ,$conexion["trama"]);
            $conexion["trama"] = str_replace('{NroTarjeta}',  $tarjeta, $conexion["trama"]);
    
            $respuesta = self::conexionSocket( $conexion, true);
           
            if($respuesta['error']){
                
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 9, 
                    "deTransaccion"        => 'TC.Tc_InterfazPos3.totobligdia5 ', 
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'Cupos', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta["datos"], 
                ];
                self::Trazabilidad($datos);
                return  $respuesta;
            }
           
            $respuesta["datos"] = self::FormatData($respuesta["datos"]);
          
            $cod_respuesta = $respuesta["datos"][1];
            if($tipoCuneta == 'IC'){
                $cod_respuesta = substr($respuesta["datos"][0], -2);
            }
    
           
            switch ($cod_respuesta) {
                case '99':
                    $respuesta['error'] = true;
                    $respuesta['msg']   = 'Datos tarjeta con problemas';
                   
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 2, 
                        "deTransaccion"        => 'TC.Tc_InterfazPos3.totobligdia5 ', 
                        "cdError"              => '',
                        "cd_accion"            => 1,
                        "de_accion"            => 'Cupos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    =>  $respuesta["datos"], 
                    ];
                    self::Trazabilidad($datos);

                    break;
                case '01':
                    $respuesta['error'] = true;
                    $respuesta['msg']   = 'Error, Avance no puede realizarse';
                   
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 2, 
                        "deTransaccion"        => 'TC.Tc_InterfazPos3.totobligdia5 ', 
                        "cdError"              => '',
                        "cd_accion"            => 1,
                        "de_accion"            => 'Cupos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    =>  $respuesta["datos"], 
                    ];
                    self::Trazabilidad($datos);

                    break;
                }
           
            if($cod_respuesta == 00){
    
                $formato = Config::get("FormatoDataUser.MW-totobligdia5");
                $datos = [];
                $num = 0;
                if($tipoCuneta == 'IC'){
                    $respuesta["datos"][0] =  $cod_respuesta;
                    foreach ($respuesta["datos"] as $key => $value) {
                    
                        if (array_key_exists($key,$formato)){
                            $index = $formato[$key];
                        }
                        else{
                            $index = "SinNombre". ++$num;
                        }
        
                        $datos[$index] = $value;
                    }
                }
                if($tipoCuneta == 'TC'){
                    unset($respuesta["datos"][0]);
                    array_pop($respuesta["datos"]);
                    
                    foreach ($respuesta["datos"] as $key => $value) {
                    
                        if (array_key_exists($key,$formato)){
                            $index = $formato[$key-1];
                        }
                        else{
                            $index = "SinNombre". ++$num;
                        }
        
                        $datos[$index] = $value;
                    }
                }
                
                $respuesta["datos"] = $datos;

                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 0, 
                    "deTransaccion"        => 'Éxito', 
                    "cdError"              => '',
                    "cd_accion"            => 9,
                    "de_accion"            => 'Deuda por Pagar', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta["datos"], 
                ];
                self::Trazabilidad($datos);
            }
        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;

        } catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }

    /**
     * MA: Metodo de conexion para el miliways TcObtDisponible informacion depende ConInfoCuenta3
     * "00TCDISTC.Tc_Obt_Disponible('{COD_EMPRESA}', '{CUENTA_CLIENTE}', '3', '500', '{FECHA}', '{CLASIFICACION}')"
     * @param String CodEmpresa codigo de la empresa
     * @param String CuentaCliente cuenta del cliente
     * @param String clasificacion cuenta del cliente
     * @return Array
     */
    
    public static function TcObtDisponible($CodEmpresa, $CuentaCliente, $clasificacion, $tipo_tarjeta, $rut="19")
    {
        error_reporting(E_ALL);

        try {
            if ($tipo_tarjeta == 'IC') {
                $conexion = Config::get("configSocket.MW-TcObtDisponible-IC");
            } elseif ($tipo_tarjeta == 'TC') {
                $conexion = Config::get("configSocket.MW-TcObtDisponible-TC");
            }
    
            $conexion["trama"] = str_replace('{COD_EMPRESA}', $CodEmpresa, $conexion["trama"]);
            $conexion["trama"] = str_replace('{CUENTA_CLIENTE}', $CuentaCliente, $conexion["trama"]);
            $conexion["trama"] = str_replace('{CLASIFICACION}', $clasificacion, $conexion["trama"]);
            $fecha = Carbon::now()->format('d/m/Y');
            //$fecha = Carbon::now();;
            $conexion["trama"] = str_replace('{FECHA}', $fecha, $conexion["trama"]);
    
            
            $respuesta = self::conexionSocket($conexion, true);
            $_respuesta = self::FormatData($respuesta["datos"]);
            
            $_respuesta[3] = $_respuesta[2];
            $_respuesta[2] = $_respuesta[1];
            $_respuesta[1] = str_replace (chr(2), '', $_respuesta[0]);
            $respuesta["datos"] = $_respuesta;

            if ($respuesta['datos']['1'] == 99) {
                $datos = [
                    "rut"                  => $rut,
                    "cdTransaccion"        => '6',
                    "deTransaccion"        => 'Consulta Cupo Otros comercios',
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'Cupos',
                    "entradaTransaccion"   => $conexion["trama"],
                    "salidaTransaccion"    => $respuesta["datos"],
                ];
                self::Trazabilidad($datos);
            } elseif ($respuesta['datos']['1'] == 1) {
                $datos = [
                    "rut"                  => $rut,
                    "cdTransaccion"        => '6',
                    "deTransaccion"        => 'Consulta Cupo Otros comercios',
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'Cupos',
                    "entradaTransaccion"   => $conexion["trama"],
                    "salidaTransaccion"    => $respuesta["datos"],
                ];
                self::Trazabilidad($datos);
            }
        } catch (\Exception $e) {
            $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  [];
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  [];
            return $respuesta;
        }
        return $respuesta;
    }
   
    /**
     * MA: Metodo de para formato de respuesta de ConInfoCuenta3
     * 
     * @param Array datos del clientes
     * @return Array
     */
    public static function FormatData($datos)
    {
        $valores = '';
        foreach ($datos as $key => $dato) {
            $valores .= $dato; 
        }
        
        $valores = preg_replace('/\\r/', "",  $valores);
        $valores = explode('|', preg_replace('/\\n/', "|",  $valores));
        //$valores = $this->userdatos($valores);
        return $valores;
    }
    /**
     * MA: Metodo de para formato de respuesta de ConInfoCuenta3FormatData
     * 
     * @param Array datos del clientes
     * @return Array
     */

    public static function ConInfoCuenta3FormatData($datos)
    {

        $datos_usuarios = [];
        $formato_user = Config::get("FormatoDataUser.MW-CONINFOCUENTA3");
        $formato_user_tarjeta = Config::get("FormatoDataUser.MW-CONINFOCUENTA3-TARJETA");
        
        $datos = preg_replace('/\\r/', "", $datos);
        $valores = explode("\n", $datos);
        
        $estatus = false;
        $error = false;
			 			 
		 if(strlen($valores[1]) > 2){
			 $valores[1] = substr($valores[1], 0, 2);
		 }
		 
        switch ($valores[1]) {
            case '00':
                
                $estatus = 00;
			
                break;
            case '01':
               
                $datos_usuarios = 01;
                $estatus = 01;
                $error = true;
				
                break;
            case '99':
               
                $estatus = 99;
                $datos_usuarios = 99;
                $error = true;
                break;
        }
		
        if($estatus == 00){
            $_tarjeta = [];
            $datos_usuarios = [];
            foreach ($valores as $key => $value) {
                if($key >= 9){
                
                    $datos = [];
                    $_value =  preg_replace('/\\r/', "", $value);
                    $_tarjeta = explode('|', $_value);
                    foreach ($_tarjeta as $_key => $_value) {
                        $datos[$formato_user_tarjeta[$_key]] = $_value;
                    }

                    if (array_key_exists("NumeroTarjeta",$datos)){
                       $indece = substr($datos['NumeroTarjeta'],-4);
					   $indece = str_pad($indece, 4, "0", STR_PAD_LEFT);
                        $datos_usuarios['tarjetas'][$indece] = $datos;
                    }
                    else{
                       continue;
                    }

                }else{
                    $datos_usuarios[$formato_user[ $key ]] = $value;
                }

            }
           
        }
        if(is_array($datos_usuarios)){
            if(array_key_exists("tarjetas", $datos_usuarios)){
                $rut_titular= $datos_usuarios['RutCliente'];
                foreach ($datos_usuarios['tarjetas'] as $key => $value) {
                    if($rut_titular != $value['RutTitular'] ){
                        unset($datos_usuarios['tarjetas'][$key]);
                    }
                }
            }
        } 
        
      
        return $respuesta = [
            'error' => $error,
            //'msj'   => $mensaje,
            'data'  => $datos_usuarios
        ];
    }
    
    /**
     * MA: Metodo de conexion para el web service  TaEstadoBloqClienteService
     * 
     * @param String rut del cliente
     * @return Array
     */
    public static function TaEstadoBloqClienteService($rut)
    {
        error_reporting(E_ALL);

        try {
            $conexion = Config::get("configSocket.WS-TaEstadoBloqClienteService");
            
            $parametros = array(
                'PCOD_PERSONA' => $rut,
                'PCOD_CANAL' => '1'
            ); 

            $metodo = "TaEstadoBloqClienteService";

            $respuesta = self::conexionWebService($conexion, $parametros, $metodo);
            
            if($respuesta['error']){
                $info = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        =>  2, 
                    "deTransaccion"        => 'Consulta estado bloqueo clave internet', 
                    "cdError"              => '', 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                ];
                  self::trazabilidad($info);
            }
            if($respuesta['datos']['PINDICADOR_BLOQ_BLANDO'] == 1 ){

                $datos_user['msg'] = trans('user::Autenticacion.bloqueo blando');
                $datos_user['estatus_cliente'] = '03';
                $info = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        =>  2, 
                    "deTransaccion"        => 'Consulta estado bloqueo clave internet', 
                    "cdError"              => '', 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                  self::trazabilidad($info);
            }else if($respuesta['datos']['PINDICADOR_BLOQ_DURO'] == 1){

                $datos_user['msg'] = trans('user::Autenticacion.bloqueo duro');
                $datos_user['estatus_cliente'] = '06';
                $info = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        =>  2, 
                    "deTransaccion"        => 'Consulta estado bloqueo clave internet', 
                    "cdError"              => '',
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                  self::trazabilidad($info);
            }

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;

    }

    /**
     * MA: Metodo de conexion para el web service  SitioretfinInterfazCalculoextracupo
     * 
     * @param XML con los factores Factores
     * @return Array
     */
    public static function SitioretfinInterfazCalculoextracupo($RutCliente, $SaldoDisponible, $CupoTotaldeLaCuenta, $FactoresSobrecupo, $tipo_tarjeta)
    {


        //$respuesta = WsSocket::SitioretfinInterfazCalculoextracupo($RutCliente, $SaldoDisponible, $CupoTotaldeLaCuenta, $FactoresSobrecupo, $OrigenDeLosDatos);

        error_reporting(E_ALL);
        try {
            $conexion = Config::get("configSocket.WS-SitioretfinInterfazCalculoextracupo");
            //error se invirtieron valores
            /* $parametros = array(
               'RutCliente'    => $RutCliente,
               'CupoTotal'     => $SaldoDisponible,
               'DispTienda'    => $CupoTotaldeLaCuenta,
               'FactExtracupo' => $FactoresSobrecupo,
               'TipoTarjeta'   => $tipo_tarjeta,
               'Canal'         => 'POS'
           );  */
            $parametros = array(
               'RutCliente'    => $RutCliente,
               'CupoTotal'     => $CupoTotaldeLaCuenta,
               'DispTienda'    => $SaldoDisponible,
               'FactExtracupo' => $FactoresSobrecupo,
               'TipoTarjeta'   => $tipo_tarjeta,
               'Canal'         => 'POS'
           ); 
           
           
           //$metodo = "SitioretfinInterfazCalculoextracupo";
           $metodo = "CalculateExtracupo";
   
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo);

            if($respuesta['error']){
                $datos = [
                    "rut"                  =>  $RutCliente, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Consulta de extracupo Tienda', 
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'Cupos', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                ];
                self::Trazabilidad($datos);
            }
            
   
           if($respuesta['datos']['Retorno'] != 00 ){
                $respuesta['error'] = true;
                $respuesta['msg'] = $respuesta['datos']['Retorno'];
                $datos = [
                    "rut"                  =>  $RutCliente, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Consulta de extracupo Tienda', 
                    "cdError"              => '',
                    "cd_accion"            => 1,
                    "de_accion"            => 'Cupos', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos']['Retorno'], 
                ];

                self::Trazabilidad($datos);
                return $respuesta;
           }
           
           $formato = [
               'CuotaDesde' => 'cuotaMin',
               'CuotaHasta' => 'cuotaMax',
               'Valor'      => 'tope'
           ];
   
           $extracupos = $respuesta['datos']['ExtraCupos']['ExtraCupo'];
         
           $datos= [];
           if($extracupos != ""){
                foreach ($extracupos as $key => $value) {
                    foreach ($value as $_key => $_value) {
                        $_dato = $_value;
                        if($_key == 'Valor'){
                            $_dato  = number_format($_value, 0, ',', '.');
                        }
        
                        $datos[$key][$formato[$_key]] = $_dato;
                    }
                }
            }
            $respuesta['datos'] = $datos;
         

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        return $respuesta;
    }

    /**
     * MA: Metodo de conexion para el web service  EntRetfDatosclienteService
     * 
     * @param String rut del cliente
     * @return Array
    */
    public static function EntRetfDatosclienteService($rut)
    {
        error_reporting(E_ALL);
        try {
            $conexion = Config::get("configSocket.WS-NewOperation");
        
            $parametros = array(
                'PC_COD_PERSONA' => $rut,
            );
            
            $metodo = "NewOperation";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo);
            
            if($respuesta['datos']['PN_COD_ERROR'] != 0){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Obtencion de offset del cliente IC', 
                    "cdError"              => '',
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
    
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
            }
          

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        return $respuesta;
        
    }

    /**
     * MA: Metodo de conexion para el web service  EntRetfConsultaticketeccService
     * 
     * @param String rut del cliente
     * @return Array
     */
    public static function EntRetfConsultaticketeccService($rut)
    {
        error_reporting(E_ALL);
        try {
            $conexion = Config::get("configSocket.WS-getTicketeecc");

            $parametros = array(
                'Rut' => $rut,
            );
            
            $metodo = "getTicketeecc";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo);
            
            $cdError = '';
            if($respuesta['error']){
                $cdError =  99;
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 1, 
                    "deTransaccion"        => 'Obtencion de offset del cliente TC', 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "cdError"              => '', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);

                return $respuesta;
            }
            if($respuesta['datos']['CodError'] != 0 ){
                $cdError =  99;
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Obtencion de offset del cliente TC', 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "cdError"              => $respuesta['datos']['CodError'], 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
            }

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
     
        return $respuesta;
    }

    /**
     * MA: Metodo de conexion para el web service  CSI Servidor de criptografico 
     * 
     * @param String tarjeta del cliente
     * @return Array
     */
    public static function CSILogin($offset, $tarjeta, $rut, $password, $TipoTarjeta)
    {
        error_reporting(E_ALL);
        $data = [
            //'pan'             => substr($tarjeta, 0, -1), // tarjeta sin digito verificador
            'pan'             => '',
            'ctype'           => 'CC', 
            'ctext'           => $password . $offset,
            'interface_id'    => 'XMLCSI',
            'customer_id'     => Config::get("configSocket.CSI.usuario"),
            'client_ip'       => Config::get("configSocket.CSI.ip"),
        ];

        // VALPINCDI el parámetro pan debe ser el rut 
        // VALPIN el parámetro pan debe ser la tarjeta
        if ($TipoTarjeta == 'TC') {
            $servicio = 'VALPIN';
            $data['pan'] = $tarjeta;
        } else {
            $servicio = 'VALPINCDI';
            $data['pan'] = substr($rut, 0, -1);
        }

        /*self::log([
            'data' => json_encode($data),
        ]);
	    */

        $dataXml = self::csiEncryptArrayToXml($data);

        $xml =  "<request_auth>
            <service_id>" . $servicio . "</service_id>
            <subtype_service>00" . Config::get("configSocket.CSI.llave") . "</subtype_service>
            " . $dataXml . "</request_auth>";

        $respuesta = self::conexionCurlXml(Config::get("configSocket.CSI.url"), $xml);
        
        $_c = "<?xml version='1.0' encoding='ISO-8859-1'?><request_response><request_result_code>1000</request_result_code></request_response> <?xml version='1.0' encoding='ISO-8859-1'?><request_response><request_result_code>2000</request_result_code></request_response> ";

        if ($_c == $respuesta) {

            $info = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 5, 
                "deTransaccion"        => 'Verificacion de contraseña', 
                "cdError"              => '',
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => 'CSI: VALPINCDI (IC) o VALPIN (TC)', 
                "salidaTransaccion"    => "Contraseña incorrecta", 
            ];
            
            self::trazabilidad($info);
            return false;
        }

        $respuesta = self::CSIProcesar($respuesta);
       /*  if(!$respuesta){
            $info = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 5, 
                "deTransaccion"        => 'Verificacion de contraseña', 
                "cdError"              => '',
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => 'CSI: VALPINCDI (IC) o VALPIN (TC)', 
                "salidaTransaccion"    => $respuesta, 
            ];
            
            self::trazabilidad($info);
        } */
        return $respuesta;
    }
    
    public static function CSIProcesar($xml)
    {
	
        
		$xml = new SimpleXMLElement($xml);
		
		$xml = (array) $xml;
       
        $location = @$xml['location'];
        $key      = @$xml['key'];

        $url = $location . '?key=' . $key;
        
        $xmlEjecutar = '<authorization>
        <accesskey>' . $key . '</accesskey>
        <ip>' . Config::get("configSocket.CSI.ip") . '</ip>
        <action>1</action>
        </authorization>';

        $respuesta = self::conexionCurlXml($url, $xmlEjecutar);

        $xml = new SimpleXMLElement($respuesta);
        $xml = (array) $xml;
        $codigores = @$xml['codigores'];

        $CheckCSI = @$xml['CheckCSI'];
        $cadenaCompuesta = @$xml['accesskey'] . 
            $xml['fi'] . 
            $xml['service'] . 
            $xml['tiposervicio'] . 
            $xml['numtrx'] . 
            $xml['ctext'] . 
            $xml['InternalCode'];

        $validacion = false;
        if (substr($xml['InternalCode'], -2) == $xml['codigores']) {
            $validacion = self::csiValidadChecksun($CheckCSI, $cadenaCompuesta);
        }

        return $validacion && $codigores === '00';
    }

    public static function csiEncryptArrayToXml($datos)
    {
        $xml = '';
        $_datos = [];

        foreach ($datos as $key => $value) {
            $_datos[] = $key . ":" . $value;
        }

        $datos = implode(" ", $_datos);
        $datos = self::csiEncrypt($datos);
        $datos = explode(" ", $datos);
        
        foreach ($datos as $dato) {
            $dato = explode(":", $dato);
            $xml .= '<' . $dato[0] . '>' . $dato[1] . '</' . $dato[0] . '>';
        }
        self::log("fin Encrypt");

        $datos = implode(" ", $_datos);
        $datos = self::csiEncrypt($datos);
        $datos = explode(" ", $datos);
        
        foreach ($datos as $dato) {
            $dato = explode(":", $dato);
            $xml .= '<' . $dato[0] . '>' . $dato[1] . '</' . $dato[0] . '>';
        }
        self::log("fin Encrypt");

        return $xml;
    }

    public static function csiExec($metodo, $dato)
    {
        $pathJava = 'java/';
        $pathJavaCSI = resource_path($pathJava . 'csi.jar');
        $comando = 'java -jar ' . $pathJavaCSI . " " . $metodo . " " . $dato;

        $respuesta = shell_exec($comando);
        $respuesta = str_replace("\n", '', $respuesta);
        $respuesta = trim($respuesta);

        return $respuesta;
    }

    public static function aesExec($metodo, $dato)
    {
        //$metodo [Encriptar ó Desencriptar]
        $pathJava = 'java/';
        $pathJavaCSI = resource_path($pathJava . 'AESCoder.jar');
        $comando = 'java -jar ' . $pathJavaCSI . " " . $metodo . " " . $dato;

        $respuesta = shell_exec($comando);
        $respuesta = str_replace("\n", '', $respuesta);
        $respuesta = trim($respuesta);

        return $respuesta;
    }

    public static function csiEncrypt($dato)
    {
        $llave = Config::get("configSocket.CSI.llave");
        $mancha = Config::get("configSocket.CSI.mancha");

        $respuesta = self::csiExec("Encriptar", $llave . " " . $mancha . " " . $dato);
        return $respuesta;
    }

    public static function csiValidadChecksun($CheckCSI, $cadenaCompuesta)
    {
        $respuesta = self::csiExec("ChecksunCSI", $CheckCSI . " " . $cadenaCompuesta);
        return $respuesta === 'true' && md5($cadenaCompuesta) === $CheckCSI;
    }

    /**
     * MA: Metodo de conexion para el el miliways  MW-EECC_TAR 
     * 
     * @param String NumeroTarjeta numero de tarjeta
     * @return Array
     */
    public static function EECC_TAR($NumeroTarjeta, $proceso, $rut = '19', $traza = true)
    {
        error_reporting(E_ALL);
        try {
            
            if ($proceso === "movimientosnofacturados") {
                $seccion = "MOVIMIENTOS POSTERIORES AL ULTIMO ESTADO DE CUENTA";
                $seccion_fin = "==<FIN Cartola>==";
                $expresionRegular = '/==<FIN Cartola>==/';
            }

            if ($proceso === "cupoutilizadotarjeta") {
                $seccion = "CARTOLA EN LINEA";
                $seccion_fin = "";
                $expresionRegular = '/\|Cupo Utilizado         -: ([\d,\s]+)\|/';
            }

            $conexion = Config::get("configSocket.MW-EECC_TAR");
            $conexion["trama"] = str_replace('{numerotarjera}', $NumeroTarjeta, $conexion["trama"]);
            $respuesta = self::conexionSocket($conexion, false, $expresionRegular);
           
        
            //$respuesta["datos"] = self::FormatData($respuesta["datos"]);
            $respuesta['data'] = false;
            $valores = '';
            foreach ($respuesta['datos'] as $key => $dato) {
                $valores .= $dato; 
            }
        
            $valores = explode("\n", $valores);

            $tabla = '';
            $inicio = false;
            $bandera = 0;

            for ($i = 0, $c = count($valores); $i < $c; $i++) {
                $linea = $valores[$i];
                
                if ($bandera === 0 && preg_match('/\['.$seccion.'\]/', $linea)) {
                    $bandera = str_slug($seccion);
                    
                    continue;
                }

                if ($bandera === 0) {
                    continue;
                }

                if ($linea === $seccion_fin) {
                    break;
                }
                
                $tabla .= $linea . "\n";
            }

            if($tabla === ''){
                $respuesta["datos"] = self::FormatData($respuesta["datos"]);
               
                if (preg_match("/[tarjeta(\d|-|\s)+no(\d|-|\s)+encontrada]/i", $respuesta["datos"][0])) {
                    $respuesta['error'] = true;
                    $respuesta["msg"] = "tarjeta no encontrada";
                    $respuesta["datos"] = [
                        'estado_respuesta' => $respuesta["datos"][1],
                        "datos" => "tarjeta no encontrada"
                    ];
                    $datos = [
                        "rut"                  => Session::get('TarjetaSeleccionada')['RutTitular'], 
                        "cdTransaccion"        => 0, 
                        "deTransaccion"        => 'Cartola de movimientos no facturados ', 
                        "cdError"              => '', 
                        "cd_accion"            => 5,
                        "de_accion"            => 'Movimientos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta["datos"], 
                    ];
                    self::Trazabilidad($datos);
                }else{
                    $datos = [
                        "rut"                  => Session::get('TarjetaSeleccionada')['RutTitular'], 
                        "cdTransaccion"        => 0, 
                        "deTransaccion"        => 'Cartola de movimientos no facturados',
                        "cdError"              => '',
                        "cd_accion"            => 5,
                        "de_accion"            => 'Movimientos',
                        "entradaTransaccion"   => $conexion["trama"],
                        "salidaTransaccion"    => $respuesta["datos"],
                    ];
                    self::Trazabilidad($datos);
                }

            }else{
                $respuesta['data'] = true;
                $respuesta["datos"] = $tabla;

                if($traza){
                    $datos = [
                        "rut"                  => Session::get('TarjetaSeleccionada')['RutTitular'], 
                        "cdTransaccion"        => 0, 
                        "deTransaccion"        => 'Exito',
                        "cdError"              => '',
                        "cd_accion"            => 5,
                        "de_accion"            => 'Movimientos',
                        "entradaTransaccion"   => $conexion["trama"],
                        "salidaTransaccion"    => $respuesta["datos"],
                    ];
                    self::Trazabilidad($datos);	
                }
                
            }

        } catch (Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;

            return $respuesta;
        }catch (FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;

            return $respuesta;
        }
        return $respuesta;
    }
    
    /**
     * MA: Metodo de conexion para el el miliways MW-datospersona
     * 
     * @param String rut cliente
     * @return Array datos personales cliente
     */
    public static function datospersona($rut)
    {
        error_reporting(E_ALL);
        try {

            $conexion = Config::get("configSocket.MW-datospersona");
            $conexion["trama"] = str_replace('{RUT}',$rut,$conexion["trama"]);
            $respuesta = self::conexionSocket( $conexion );
            $respuesta["datos"] = self::FormatData($respuesta["datos"]);
            
            if($respuesta['error']){
                $info = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => '1', 
                    "deTransaccion"        => 'Recupera Datos Cliente', 
                    "cdError"              => "", 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta["datos"], 
                ];
		         self::trazabilidad($info);

            }
	
            if($respuesta["datos"][1] == 99 || $respuesta["datos"][1] == 01){
                $info = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => '1', 
                    "deTransaccion"        => 'Recupera Datos Cliente', 
                    "cdError"              => "", 
                    "cd_accion"            => 0,
                    "de_accion"            => 'Login', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta["datos"], 
                ];
		        self::trazabilidad($info);

            }
          
        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            return $respuesta;
        }
        
        return $respuesta;	
    }
    /**
     * MA: Metodo de conexion para el el miliways MW-ConsultaFlujoEECC
     * 
     * @param String rut cliente
     * @return Array datos personales cliente
     */
    public static function ConsultaFlujoEECC($EmpresaId,$NumCuenta, $PrimerVcto, $Plazo = 0, $ValorCuota = 0 )
    {
        error_reporting(E_ALL);
       

        try {
            
            $conexion = Config::get("configSocket.MW-ConsultaFlujoEECC");
            $conexion["trama"] = str_replace('{EmpresaId}',$EmpresaId,$conexion["trama"]);
            $conexion["trama"] = str_replace('{NumCuenta}',$NumCuenta,$conexion["trama"]);
            
            $conexion["trama"] = str_replace('{PrimerVcto}',$PrimerVcto,$conexion["trama"]);
            $conexion["trama"] = str_replace('{Plazo}',$Plazo,$conexion["trama"]);
            $conexion["trama"] = str_replace('{ValorCuota}',$ValorCuota,$conexion["trama"]);
            $respuesta = self::conexionSocket( $conexion, true );
			
		
                $datos=[];
            $rut = Session::get('TarjetaSeleccionada')['RutTitular'];
            $valores = explode("\n", $respuesta['original-data']);
            $valores[0] = substr($valores[0],-2);
            switch ($valores[0]) {
                case '99':
                    $respuesta['msg'] = trans('page::global.servicios no disponible');
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 2, 
                        "deTransaccion"        => 'Obtención de próximos vencimientos', 
                        "cdError"              => 99, 
                        "cd_accion"            => 6,
                        "de_accion"            => 'Próximos Vencimientos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta["datos"], 

                    ];
                    self::Trazabilidad($datos);
                    return response()->json($respuesta);
                    break;
                case '1':
                    $respuesta['msg'] = $valores[1];
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 2, 
                        "deTransaccion"        => 'Obtención de próximos vencimientos', 
                        "cdError"              => '', 
                        "cd_accion"            => 6,
                        "de_accion"            => 'Próximos Vencimientos', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta["datos"], 

                    ];
                    self::Trazabilidad($datos);
                    return response()->json($respuesta);
                    break;
                }

            $datos = [
                "rut"                  => $rut,
                "cdTransaccion"        => 0,
                "deTransaccion"        => 'Exito',
                "cdError"              => '',
                "cd_accion"            => 6,
                "de_accion"            => 'Próximos Vencimientos',
                "entradaTransaccion"   => $conexion["trama"],
                "salidaTransaccion"    => $respuesta["datos"],
            ];
            self::Trazabilidad($datos);

            $formato_general = Config::get("FormatoDataUser.MW-ConsultaFlujoEECC");
            $formato_general_DatosFlujo = Config::get("FormatoDataUser.MW-ConsultaFlujoEECC-DatosFlujo");
            $datos = [];
            $datosFlujo = [];

            $meses = [
                1 => 'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre',
            ];
        
            
            foreach ($valores as $key => $value) {
                if($key >= 5){

                    $resultado = explode("|", $value);
                    $_datos=  [];
                    foreach ($resultado as $_key => $_value) {
                        if($_value == "\x03"){
                            continue;
                        }
                    
                        $_datos[$formato_general_DatosFlujo[$_key]] = $_value;
                    

                    }
                    if(count($_datos) == 4){

                        $_fecha =  explode("/", trim($_datos['FechaVctoFlujo']));
                        $_datos['FechaVctoFlujo'] = $_fecha[0].' de '. $meses[intval($_fecha[1])].' '. $_fecha[2];
                        $_datos['EstadoActualFlujo'] = number_format($_datos['EstadoActualFlujo'], 0,',','.');
                        $_datos['EstadoNuevoFlujo'] = number_format($_datos['EstadoNuevoFlujo'], 0,',','.');
                        $datosFlujo[] = $_datos;
                    }
                    continue;
                }
                $datos[$formato_general[$key]] = $value;
            }

            $datos['DatosFlujo'] = $datosFlujo;
            
            $respuesta['datos'] = $datos;
            unset($respuesta['original-data']);
        
        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;	
    }

    /**
     * MA: metodo de conxion para el web service WS-getMaileecc
     * @author Miguelangel Gutierrez 
     * @param string $rut
     * @return array $respuesta
     */
    public static function getMaileecc($rut, $metodo, $parametros)
    {
        try {
            $conexion = Config::get("configSocket.WS-getMaileecc");
        
           /*  $parametros = array(
                'PCODPERSONA' => $rut
            );  */
            
           // $metodo = "getMaileecc";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 0, 
                    "deTransaccion"        => 'getMaileecc', 
                    "cdError"              => '', 
                    "cd_accion"            => 11,
                    "de_accion"            => '', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta["datos"], 
                ];
                self::Trazabilidad($datos);
            }
        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }

        
        return $respuesta;
    }

    /**
     * MA: metodo de conexion para el web service EntRetfActualizaestatusclienteService
     * @author Miguelangel Gutierrez 
     * @param string $rut
     * @return array $respuesta
     */
    public static function EntRetfActualizaestatusclienteService($rut, $paccion){
        error_reporting(E_ALL);
        
        $conexion = Config::get("configSocket.WS-EntRetfActualizaestatusclienteService");
        
        $parametros = array(
            'PCOD_PERSONA'  => $rut,
            'PCOD_CANAL'    => 1,
            'PACCION'       => $paccion
        ); 
        
        $metodo = "TaActualizaEstatusClienteService";
        $respuesta = self::conexionWebService($conexion, $parametros, $metodo);

        if($respuesta['error']){
            $datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 3, 
                "deTransaccion"        => 'Incrementar Bloqueo', 
                "cdError"              => '',
                "cd_accion"            => 0,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => $parametros, 
                "salidaTransaccion"    => $respuesta['datos'], 
            ];
            self::Trazabilidad($datos);
        }
       
        return $respuesta;

    }


    /**
     * MA: Metodo de conexion para el web service  SitioretfinInterfazCreaticketeecc
     * 
     * @param String rut del cliente
     * @return Array
     */
    public static function SitioretfinInterfazCreaticketeecc($rut, $userChannel)
    {
       
        try {
            $conexion = Config::get("configSocket.WS-SitioretfinInterfazCreaticketeecc");

            $parametros = array(
                'userId' => $rut,
                'userChannel' => $userChannel,
            );

            $metodo = "createTicketByRut";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo);

            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Ticket para consulta de Estado Cuenta', 
                    "cdError"              => '',
                    "cd_accion"            =>  4,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }else{
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 0, 
                    "deTransaccion"        => 'Exito', 
                    "cdError"              => '',
                    "cd_accion"            =>  4,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
            

        } catch (\Exception $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }

    /**
     *  MA: Metodo de conexion para el web MW  EsClienteValidoWeb
     *
     * @param [type] $rut
     * @return void
     */
    public static function EsClienteValidoWeb($rut){
        try {
            $conexion = Config::get("configSocket.MW-EsClienteValidoWeb");
            $conexion["trama"] = str_replace('{PCODPERSONA}',$rut, $conexion["trama"]);
            $respuesta = self::conexionSocket( $conexion, true );
            $valores = [];

            if($respuesta['error']){
                $respuesta['error'] = false;
                $valores[0] = "99";
                $valores[1] = 99;

                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 1, 
                    "deTransaccion"        => 'Validación Cliente abcdin', 
                    "cdError"              => '',
                    "cd_accion"            =>  10,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta['msg'], 
                   
                ];
                self::Trazabilidad($datos);

            }
            if(!empty($respuesta['datos'])){
                $valores = explode("\n", $respuesta['original-data']);
                if($valores[1] == '02' || $valores[1] == '04' ){
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 1, 
                        "deTransaccion"        => 'Validación Cliente abcdin', 
                        "cdError"              => '',
                        "cd_accion"            =>  10,
                        "de_accion"            => 'Estado de Cuenta', 
                        "entradaTransaccion"   => $conexion["trama"], 
                        "salidaTransaccion"    => $respuesta['msg'], 
                    ];
                    self::Trazabilidad($datos);
                }
            }else{
                $valores[1] = 99;
                $valores[0] = "99";
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 1, 
                    "deTransaccion"        => 'Validación Cliente abcdin', 
                    "cdError"              => '',
                    "cd_accion"            =>  10,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $conexion["trama"], 
                    "salidaTransaccion"    => $respuesta['msg'], 
                ];
                self::Trazabilidad($datos);
            }
            
            $respuesta['datos'] = $valores;

        } catch (\Exception $e) {
            $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;

            return $respuesta;
        }catch (\FatalErrorException $e) {
		    $respuesta['error'] = true;
            $respuesta['msg']   =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;

            return $respuesta;
        }

        return $respuesta;
    }

    /**
     * MA: Metodo de conexion para el web service  
     *
     * @return void
     */
    public static function TC2000EvaluacionExpress($datos){
        error_reporting(E_ALL);
        try {
            $conexion = Config::get("configSocket.WS-TC2000_EvaluacionExpress");
            $rut = $datos['rut'].$datos['dv'] ;

            $fechaNacimiento = Carbon::parse($datos['fecnac']);
            $edad = $fechaNacimiento->age;

            $datos_correo = $porciones = explode("@",trim($datos['email']));

            $DatosBasicos = new \SoapVar('
                <ns1:DatosBasicos>
                    <ns1:nomPrincipal>'.$datos['primer_nombre'] .'</ns1:nomPrincipal> 
                    <ns1:nomSecundario>'.$datos['segundo_nombre'] .'</ns1:nomSecundario> 
                    <ns1:apeMaterno>'.$datos['apemat'] .'</ns1:apeMaterno> 
                    <ns1:apePaterno>'.$datos['apepat'] .'</ns1:apePaterno> 
                    <ns1:rut>'.$datos['rut'].$datos['dv'] .'</ns1:rut> 
                    <ns1:fecNac>'.$fechaNacimiento->format('dmY') .'</ns1:fecNac> 
                    <ns1:edad>'.$edad.'</ns1:edad>
                    <ns1:nacionalidad>'.$datos['desc_nacionalidad'] .'</ns1:nacionalidad>
                    <ns1:sexo>'.strtoupper($datos['cod_sexo']) .'</ns1:sexo>
                    <ns1:calle>'.$datos['calle'] .'</ns1:calle>
                    <ns1:numero>'.$datos['numcalle'] .'</ns1:numero>
                    <ns1:comuna>'.$datos['cod_comuna'] .'</ns1:comuna>
                    <ns1:region>'.$datos['cod_region'] .'</ns1:region>
                    <ns1:detDireccionP>'.$datos['detdir'] .'</ns1:detDireccionP> 
                    <ns1:fonoFijoArea></ns1:fonoFijoArea>
                    <ns1:fonoFijoNum>'.$datos['fono_fijo'].'</ns1:fonoFijoNum>
                    <ns1:fonoMovilArea></ns1:fonoMovilArea>
                    <ns1:fonoMovilNum>'.$datos['fono_movil'].'</ns1:fonoMovilNum>
                    <ns1:emailUsuario>'.$datos_correo[0].'</ns1:emailUsuario>
                    <ns1:emailServidor>'.$datos_correo[1].'</ns1:emailServidor>
                </ns1:DatosBasicos>
            ', XSD_ANYXML, null);

            $DatosPersonalesAcademicos = new \SoapVar('
                <ns1:DatosPersonalesAcademicos>
                    <ns1:nivelEstudios>7</ns1:nivelEstudios>
                    <ns1:estadoCivil>'.strtoupper($datos['cod_estado']).'</ns1:estadoCivil>
                    <ns1:actividad>'.$datos['cod_profe'].'</ns1:actividad>
                    <ns1:sepBienes></ns1:sepBienes>
                    <ns1:nroHijos>'.$datos['q_hijos'].'</ns1:nroHijos>
                    <ns1:hijo1Sexo></ns1:hijo1Sexo>
                    <ns1:hijo1Edad></ns1:hijo1Edad>
                    <ns1:hijo2Sexo></ns1:hijo2Sexo>
                    <ns1:hijo2Edad></ns1:hijo2Edad>
                    <ns1:hijo3Sexo></ns1:hijo3Sexo>
                    <ns1:hijo3Edad></ns1:hijo3Edad>
                    <ns1:carrera></ns1:carrera>
                    <ns1:casaEstudios></ns1:casaEstudios>
                    <ns1:semestre></ns1:semestre>
                    <ns1:profActiv></ns1:profActiv>
                    <ns1:condHabit>2</ns1:condHabit>
                    <ns1:tipoViv>'.$datos['cod_tipovi'].'</ns1:tipoViv>
                    <ns1:fecOcupViv>'.Carbon::parse($datos['fecvivienda'])->format('dmY').'</ns1:fecOcupViv>
                    <ns1:arriendoDivMensual>'.str_replace('.','',$datos['pagomensual']).'</ns1:arriendoDivMensual>
                </ns1:DatosPersonalesAcademicos>
            ', XSD_ANYXML, null);

            $DatosComerciales = new \SoapVar('
                <ns1:DatosComerciales>
                    <ns1:CC1Codigo></ns1:CC1Codigo>
                    <ns1:CC1Cupo></ns1:CC1Cupo>
                    <ns1:CC2Codigo></ns1:CC2Codigo>
                    <ns1:CC2Cupo></ns1:CC2Cupo>
                    <ns1:CC3Codigo></ns1:CC3Codigo>
                    <ns1:CC3Cupo></ns1:CC3Cupo>
                    <ns1:BE1Codigo></ns1:BE1Codigo>
                    <ns1:BE1MarcaTarjeta></ns1:BE1MarcaTarjeta>
                    <ns1:BE1Cupo></ns1:BE1Cupo>
                    <ns1:BE2Codigo></ns1:BE2Codigo>
                    <ns1:BE2MarcaTarjeta></ns1:BE2MarcaTarjeta>
                    <ns1:BE2Cupo></ns1:BE2Cupo>
                    <ns1:BE3Codigo></ns1:BE3Codigo>
                    <ns1:BE3MarcaTarjeta></ns1:BE3MarcaTarjeta>
                    <ns1:BE3Cupo></ns1:BE3Cupo>
                    <ns1:cuentaCorrienteBE></ns1:cuentaCorrienteBE>
                    <ns1:cupoLinea></ns1:cupoLinea>
                    <ns1:cuentaVistaBE></ns1:cuentaVistaBE>
                    <ns1:automovil></ns1:automovil>
                    <ns1:nroPatente></ns1:nroPatente>
                    <ns1:propiedad></ns1:propiedad>
                </ns1:DatosComerciales>
            ', XSD_ANYXML, null);

            $DatosLaborales = new \SoapVar('
                <ns1:DatosLaborales>
                    <ns1:nomEmpresa></ns1:nomEmpresa>
                    <ns1:fecIngreso></ns1:fecIngreso>
                    <ns1:rubroEmpresa></ns1:rubroEmpresa>
                    <ns1:diaPago></ns1:diaPago>
                    <ns1:calle></ns1:calle>
                    <ns1:numero></ns1:numero>
                    <ns1:region></ns1:region>
                    <ns1:comuna></ns1:comuna>
                    <ns1:detDireccionL></ns1:detDireccionL> 
                    <ns1:cargoOcupado></ns1:cargoOcupado>
                    <ns1:fonoFijo1Area></ns1:fonoFijo1Area>
                    <ns1:fonoFijo1Num></ns1:fonoFijo1Num>
                    <ns1:fonoFijo1Anexo1></ns1:fonoFijo1Anexo1>
                    <ns1:fonoFijo1Tipo></ns1:fonoFijo1Tipo>
                    <ns1:fonoFijo2Area></ns1:fonoFijo2Area>
                    <ns1:fonoFijo2Num></ns1:fonoFijo2Num>
                    <ns1:fonoFijo2Anexo1></ns1:fonoFijo2Anexo1>
                    <ns1:fonoFijo2Tipo></ns1:fonoFijo2Tipo>
                    <ns1:ingActTipo></ns1:ingActTipo>
                    <ns1:ingActRentaLiqMens></ns1:ingActRentaLiqMens>
                    <ns1:otrosIngTipo></ns1:otrosIngTipo>
                    <ns1:otrosIngRentaLiqMens></ns1:otrosIngRentaLiqMens>
                    <ns1:empAntFecIngreso></ns1:empAntFecIngreso>
                    <ns1:empAntFecSalida></ns1:empAntFecSalida>
                </ns1:DatosLaborales>
            ', XSD_ANYXML, null);

            $DatosReferencia = new \SoapVar('
                <ns1:DatosReferencia>
                    <ns1:ref1NomApe></ns1:ref1NomApe>
                    <ns1:ref1RelTit></ns1:ref1RelTit>
                    <ns1:fonoFijo1Area></ns1:fonoFijo1Area>
                    <ns1:fonoFijo1Num></ns1:fonoFijo1Num>
                    <ns1:fonoFijo1Anexo></ns1:fonoFijo1Anexo>
                    <ns1:fonoFijo1Ubic></ns1:fonoFijo1Ubic>
                    <ns1:fonoMovil1Area></ns1:fonoMovil1Area>
                    <ns1:fonoMovil1Num></ns1:fonoMovil1Num>
                    <ns1:ref2NomApe></ns1:ref2NomApe>
                    <ns1:ref2RelTit></ns1:ref2RelTit>
                    <ns1:fonoFijo2Area></ns1:fonoFijo2Area>
                    <ns1:fonoFijo2Num></ns1:fonoFijo2Num>
                    <ns1:fonoFijo2Anexo></ns1:fonoFijo2Anexo>
                    <ns1:fonoFijo2Ubic></ns1:fonoFijo2Ubic>
                    <ns1:fonoMovil2Area></ns1:fonoMovil2Area>
                    <ns1:fonoMovil2Num></ns1:fonoMovil2Num>
                    <ns1:diaVencimiento></ns1:diaVencimiento>
                    <ns1:domicilioEnvio></ns1:domicilioEnvio>
                    <ns1:dispContacto></ns1:dispContacto>
                    <ns1:horario></ns1:horario>
                </ns1:DatosReferencia>
            ', XSD_ANYXML, null);

            $parametros = array(
                "codEmp"                    => 1,
                "codCanal"                  => "ECO",
                "codPer"                    => $rut,
                "fecHoy"                    => Carbon::now()->format('dmY'),
                "codAutPrev"                => $datos['transactionKey'],
                "modoOp"                    => 2,
                "DatosBasicos"              => $DatosBasicos,
                "DatosPersonalesAcademicos" => $DatosPersonalesAcademicos,
                "DatosComerciales"          => $DatosComerciales,
                "DatosLaborales"            => $DatosLaborales,
                "DatosReferencia"           => $DatosReferencia
            );

            $metodo = "process";

            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 6, 
                    "deTransaccion"        => 'Evaluación express', 
                    "cdError"              => '',
                    "cd_accion"            =>  10,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
         

        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
     
        return $respuesta;
    }

    public static function TC2000EvaluacionExpressPrueba($datos = ''){
        error_reporting(E_ALL);
        try {
            $conexion = Config::get("configSocket.WS-TC2000_EvaluacionExpress");
            $rut = "124943043";

            
		$datos = '';
       
            $DatosBasicos = new \SoapVar('
                <ns1:DatosBasicos>
                    <ns1:nomPrincipal>Maria</ns1:nomPrincipal> 
                    <ns1:nomSecundario>Tereaa</ns1:nomSecundario> 
                    <ns1:apeMaterno>Navarro</ns1:apeMaterno> 
                    <ns1:apePaterno>Rojas</ns1:apePaterno> 
                    <ns1:rut>'.$rut.'</ns1:rut> 
                    <ns1:fecNac>19011983</ns1:fecNac> 
                    <ns1:edad></ns1:edad>
                    <ns1:nacionalidad>Chileno</ns1:nacionalidad>
                    <ns1:sexo>F</ns1:sexo>
                    <ns1:calle>Coyancura</ns1:calle>
                    <ns1:numero>2270</ns1:numero>
                    <ns1:comuna>6670</ns1:comuna>
                    <ns1:region>13</ns1:region>
                    <ns1:detDireccionP>Piso 8</ns1:detDireccionP> 
                    <ns1:fonoFijoArea>09</ns1:fonoFijoArea>
                    <ns1:fonoFijoNum>224294453</ns1:fonoFijoNum>
                    <ns1:fonoMovilArea>09</ns1:fonoMovilArea>
                    <ns1:fonoMovilNum>73111122</ns1:fonoMovilNum>
                    <ns1:emailUsuario>mariamargarita.navarro</ns1:emailUsuario>
                    <ns1:emailServidor>gmail.com</ns1:emailServidor>
                </ns1:DatosBasicos>
            ', XSD_ANYXML, null);

            $DatosPersonalesAcademicos = new \SoapVar('
                <ns1:DatosPersonalesAcademicos>
                    <ns1:nivelEstudios>7</ns1:nivelEstudios>
                    <ns1:estadoCivil>5</ns1:estadoCivil>
                    <ns1:actividad>16</ns1:actividad>
                    <ns1:sepBienes></ns1:sepBienes>
                    <ns1:nroHijos>0</ns1:nroHijos>
                    <ns1:hijo1Sexo></ns1:hijo1Sexo>
                    <ns1:hijo1Edad></ns1:hijo1Edad>
                    <ns1:hijo2Sexo></ns1:hijo2Sexo>
                    <ns1:hijo2Edad></ns1:hijo2Edad>
                    <ns1:hijo3Sexo></ns1:hijo3Sexo>
                    <ns1:hijo3Edad></ns1:hijo3Edad>
                    <ns1:carrera></ns1:carrera>
                    <ns1:casaEstudios></ns1:casaEstudios>
                    <ns1:semestre></ns1:semestre>
                    <ns1:profActiv>Técnico en enfermeri</ns1:profActiv>
                    <ns1:condHabit>2</ns1:condHabit>
                    <ns1:tipoViv>C</ns1:tipoViv>
                    <ns1:fecOcupViv>11091973</ns1:fecOcupViv>
                    <ns1:arriendoDivMensual></ns1:arriendoDivMensual>
                </ns1:DatosPersonalesAcademicos>
            ', XSD_ANYXML, null);

            $DatosComerciales = new \SoapVar('
                <ns1:DatosComerciales>
                    <ns1:CC1Codigo></ns1:CC1Codigo>
                    <ns1:CC1Cupo></ns1:CC1Cupo>
                    <ns1:CC2Codigo></ns1:CC2Codigo>
                    <ns1:CC2Cupo></ns1:CC2Cupo>
                    <ns1:CC3Codigo></ns1:CC3Codigo>
                    <ns1:CC3Cupo></ns1:CC3Cupo>
                    <ns1:BE1Codigo></ns1:BE1Codigo>
                    <ns1:BE1MarcaTarjeta></ns1:BE1MarcaTarjeta>
                    <ns1:BE1Cupo></ns1:BE1Cupo>
                    <ns1:BE2Codigo></ns1:BE2Codigo>
                    <ns1:BE2MarcaTarjeta></ns1:BE2MarcaTarjeta>
                    <ns1:BE2Cupo></ns1:BE2Cupo>
                    <ns1:BE3Codigo></ns1:BE3Codigo>
                    <ns1:BE3MarcaTarjeta></ns1:BE3MarcaTarjeta>
                    <ns1:BE3Cupo></ns1:BE3Cupo>
                    <ns1:cuentaCorrienteBE></ns1:cuentaCorrienteBE>
                    <ns1:cupoLinea></ns1:cupoLinea>
                    <ns1:cuentaVistaBE></ns1:cuentaVistaBE>
                    <ns1:automovil></ns1:automovil>
                    <ns1:nroPatente></ns1:nroPatente>
                    <ns1:propiedad></ns1:propiedad>
                </ns1:DatosComerciales>
            ', XSD_ANYXML, null);

            $DatosLaborales = new \SoapVar('
                <ns1:DatosLaborales>
                    <ns1:nomEmpresa>abcdin</ns1:nomEmpresa>
                    <ns1:fecIngreso>10091998</ns1:fecIngreso>
                    <ns1:rubroEmpresa>PR</ns1:rubroEmpresa>
                    <ns1:diaPago>5</ns1:diaPago>
                    <ns1:calle>Martin de Zamora</ns1:calle>
                    <ns1:numero>2887</ns1:numero>
                    <ns1:region>13</ns1:region>
                    <ns1:comuna>6670</ns1:comuna>
                    <ns1:detDireccionL>ex colegio</ns1:detDireccionL> 
                    <ns1:cargoOcupado>99</ns1:cargoOcupado>
                    <ns1:fonoFijo1Area>02</ns1:fonoFijo1Area>
                    <ns1:fonoFijo1Num>3367788</ns1:fonoFijo1Num>
                    <ns1:fonoFijo1Anexo1></ns1:fonoFijo1Anexo1>
                    <ns1:fonoFijo1Tipo></ns1:fonoFijo1Tipo>
                    <ns1:fonoFijo2Area></ns1:fonoFijo2Area>
                    <ns1:fonoFijo2Num></ns1:fonoFijo2Num>
                    <ns1:fonoFijo2Anexo1></ns1:fonoFijo2Anexo1>
                    <ns1:fonoFijo2Tipo></ns1:fonoFijo2Tipo>
                    <ns1:ingActTipo>S</ns1:ingActTipo>
                    <ns1:ingActRentaLiqMens></ns1:ingActRentaLiqMens>
                    <ns1:otrosIngTipo></ns1:otrosIngTipo>
                    <ns1:otrosIngRentaLiqMens></ns1:otrosIngRentaLiqMens>
                    <ns1:empAntFecIngreso></ns1:empAntFecIngreso>
                    <ns1:empAntFecSalida></ns1:empAntFecSalida>
                </ns1:DatosLaborales>
            ', XSD_ANYXML, null);

            $DatosReferencia = new \SoapVar('
                <ns1:DatosReferencia>
                    <ns1:ref1NomApe></ns1:ref1NomApe>
                    <ns1:ref1RelTit></ns1:ref1RelTit>
                    <ns1:fonoFijo1Area></ns1:fonoFijo1Area>
                    <ns1:fonoFijo1Num></ns1:fonoFijo1Num>
                    <ns1:fonoFijo1Anexo></ns1:fonoFijo1Anexo>
                    <ns1:fonoFijo1Ubic></ns1:fonoFijo1Ubic>
                    <ns1:fonoMovil1Area></ns1:fonoMovil1Area>
                    <ns1:fonoMovil1Num></ns1:fonoMovil1Num>
                    <ns1:ref2NomApe></ns1:ref2NomApe>
                    <ns1:ref2RelTit></ns1:ref2RelTit>
                    <ns1:fonoFijo2Area></ns1:fonoFijo2Area>
                    <ns1:fonoFijo2Num></ns1:fonoFijo2Num>
                    <ns1:fonoFijo2Anexo></ns1:fonoFijo2Anexo>
                    <ns1:fonoFijo2Ubic></ns1:fonoFijo2Ubic>
                    <ns1:fonoMovil2Area></ns1:fonoMovil2Area>
                    <ns1:fonoMovil2Num></ns1:fonoMovil2Num>
                    <ns1:diaVencimiento></ns1:diaVencimiento>
                    <ns1:domicilioEnvio></ns1:domicilioEnvio>
                    <ns1:dispContacto></ns1:dispContacto>
                    <ns1:horario></ns1:horario>
                </ns1:DatosReferencia>
            ', XSD_ANYXML, null);

            $parametros = array(
                "codEmp"                    => 1,
                "codCanal"                  => "ECO",
                "codPer"                    => $rut,
                "fecHoy"                    => Carbon::now()->format('dmY'),
                "codAutPrev"                => "",
                "modoOp"                    => 2,
                "DatosBasicos"              => $DatosBasicos,
                "DatosPersonalesAcademicos" => $DatosPersonalesAcademicos,
                "DatosComerciales"          => $DatosComerciales,
                "DatosLaborales"            => $DatosLaborales,
                "DatosReferencia"           => $DatosReferencia
            );

            $metodo = "process";

            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
            self::log([
                'url' => $conexion["url"],
                'msg'   => '',
                'response' => $respuesta
            ]);
            

        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
       /*  $datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 6, 
                "deTransaccion"        => 'Evaluación express', 
                "cdError"              => '', 
                "entradaTransaccion"   => 'TC2000_EvaluacionExpress_WEB', 
                "salidaTransaccion"    => '', 
            ];
            self::Trazabilidad($datos); */
        return $respuesta;
    } 

    public static function getDatosLegal($rut){
        try {
            
            $conexion = Config::get("configSocket.wS-getDatosLegal");
    
            $parametros = [
                'codPersona' => $rut
            ]; 
            
            $metodo = "getDatosLegal";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false, false, $conexion["opciones"]);

            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 4, 
                    "deTransaccion"        => 'Recupera datos legales', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Recupera datos legales', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
           /*  if($respuesta['datos']['msjError']){

            }
            $datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 4, 
                "deTransaccion"        => 'Recupera datos legales', 
                "cdError"              => '',
                "cd_accion"            =>  11,
                "de_accion"            => 'Recupera datos legales', 
                "entradaTransaccion"   => $parametros, 
                "salidaTransaccion"    => $respuesta['datos'], 
               
            ];
            self::Trazabilidad($datos);
            */
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
			$respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }

      
        return $respuesta;
    }

    public static function SmsWs($rut, $email = '', $DatosContacto = ''){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.wS-SmsWs");
            $userPhone = '';   
            
            if(array_key_exists('tipFono',$DatosContacto)){
                $userPhone =  $DatosContacto['numFono'];
            }
            
	    //MA: comentado ya que el pro se tiene que enviar con numero verificador y el des sin numero verificador
            //<ns1:userId>'. substr($rut, 0, -1) .'</ns1:userId> en desarrollo
            $soapvar = new \SoapVar('
            <ns1:SmsTransactionData>
                <ns1:userId>'. $rut .'</ns1:userId>
                <ns1:operationId>ActualizarEmail</ns1:operationId>
              
                <ns1:userEmail>'.$email.'</ns1:userEmail>
               
                <ns1:userPhone>'. $userPhone.'</ns1:userPhone>
                <ns1:smsOperationData>
                    <ns1:smsParameter>
                        <ns1:key>telefono</ns1:key>
                        <ns1:value>'.$userPhone.'</ns1:value>
                    </ns1:smsParameter>
                </ns1:smsOperationData>
            </ns1:SmsTransactionData>
            ', XSD_ANYXML, 'null');

            $parametros = array("SmsTransactionData" => $soapvar);

            $metodo = "SmsGenerateChallenge"; 
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false, true);
          
            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 5, 
                    "deTransaccion"        => 'envio de clave dinamica ', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Envio clave dinámica', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                   
                ];
                self::Trazabilidad($datos);
                return $respuesta;
            }
    
            if($respuesta['datos']['OperationResponse']['statusCode'] != 0){
                $respuesta['error'] = true;
                $respuesta['msg'] = $respuesta['datos']['OperationResponse']['statusDescription'];
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 5, 
                    "deTransaccion"        => 'envio de clave dinamica ', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Envio clave dinámica', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
       
    }

    public static function ValidaSmsWS($rut, $email = '', $DatosContacto = '', $clave){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.wS-SmsWs");
            $userPhone = '';
            if(array_key_exists('tipFono',$DatosContacto)){


                /* if($DatosContacto['tipFono'] != 'C'){
                } */
                $userPhone =  $DatosContacto['numFono'];
            }

           $soapvar = new \SoapVar('
            <ns1:SmsTransactionData>
                  <ns1:userId>'. $rut .'</ns1:userId>
                <ns1:operationId>ActualizarEmail</ns1:operationId>
              
                <ns1:userEmail>'.$email.'</ns1:userEmail>
               
                <ns1:userPhone>'. $userPhone.'</ns1:userPhone>
                <ns1:smsOperationData>
                    <ns1:smsParameter>
                        <ns1:key>telefono</ns1:key>
                        <ns1:value>'.$userPhone.'</ns1:value>
                    </ns1:smsParameter>
                </ns1:smsOperationData>
            </ns1:SmsTransactionData>
            
            ', XSD_ANYXML, 'null');

           $soapvar2 = new \SoapVar('
                <ns1:confirmationCode>'.$clave.'</ns1:confirmationCode>
            ', XSD_ANYXML, 'null');

            $parametros = array("SmsTransactionData" => $soapvar, "confirmationCode" => $clave);
            
            $metodo = "SmsValidateTransaction";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false, true);

            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 6, 
                    "deTransaccion"        => 'Validación Clave Dinámica', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'valida clave dinámica', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                   
                ];
                self::Trazabilidad($datos);
                return $repuesta;
            }
    
            if($respuesta['datos']['OperationResponse']['statusCode'] != 0){
                $respuesta['error'] = true;
                $respuesta['msg'] = $respuesta['datos']['OperationResponse']['statusDescription'];
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 6, 
                    "deTransaccion"        => 'Validación Clave Dinámica', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'valida clave dinámica', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
            }
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
         return $respuesta;
       
    }

    public static function ActualizaClienteIC($datos = ''){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.wS-ActualizaClienteIC");
            
            /* $parametros = array(
                'updateClienteICRequest' => $datos
            );  */

            $updateClienteICRequest = new \SoapVar('
                <updateClienteICRequest>
                    <RUT_CLIENTE>'.$datos['RUT_CLIENTE'].'</RUT_CLIENTE>
                    <EST_CIVIL></EST_CIVIL>
                    <SEXO></SEXO>
                    <FEC_NACIMIENTO></FEC_NACIMIENTO>
                    <PRIMER_APELLIDO>'.$datos['PRIMER_APELLIDO'].'</PRIMER_APELLIDO>
                    <SEGUNDO_APELLIDO>'.$datos['SEGUNDO_APELLIDO'].'</SEGUNDO_APELLIDO>
                    <PRIMER_NOMBRE>'.$datos['PRIMER_NOMBRE'].'</PRIMER_NOMBRE>
                    <SEGUNDO_NOMBRE>'.$datos['SEGUNDO_NOMBRE'].'</SEGUNDO_NOMBRE>
                    <PROFESION></PROFESION>
                    <CONYUGUE></CONYUGUE>
                    <NACIONALIDAD></NACIONALIDAD>
                    <EMAIL_USUARIO>'.$datos['EMAIL_USUARIO'].'</EMAIL_USUARIO>
                    <EMAIL_SERVIDOR>'.$datos['EMAIL_SERVIDOR'].'</EMAIL_SERVIDOR>
                    <NIVEL_ESTUDIOS></NIVEL_ESTUDIOS>
                    <TIPO_VIVIENDA></TIPO_VIVIENDA>
                    <NUM_HIJOS></NUM_HIJOS>
                    <NUM_DEPENDIENTES></NUM_DEPENDIENTES>
                    <ES_RESIDENTE></ES_RESIDENTE>
                    <TIEMPO_VIVIEN_ACT></TIEMPO_VIVIEN_ACT>
                    <TOTAL_INGRESOS></TOTAL_INGRESOS>
                    <COD_PAIS></COD_PAIS>
                    <SCORING></SCORING>
                    <ACTIVIDAD></ACTIVIDAD>
                    <TIPO_SOC_CONYUGAL></TIPO_SOC_CONYUGAL>
                    <NOM_CONYUGUE></NOM_CONYUGUE>
                    <FEC_MATRIMONIO></FEC_MATRIMONIO>
                    <IND_SOL_PATRIMONIO></IND_SOL_PATRIMONIO>
                    <IND_EECC_EMAIL>S</IND_EECC_EMAIL>
                    <COD_DIRECCION>1</COD_DIRECCION>
                    <TIP_DIRECCION>1</TIP_DIRECCION>
                    <COD_POSTAL></COD_POSTAL>
                    <DETALLE>'.$datos['DETALLE'].'</DETALLE>
                    <COD_PAIS_CNT></COD_PAIS_CNT>
                    <COD_REGION>'.$datos['COD_REGION'].'</COD_REGION>
                    <COD_COMUNA>'.$datos['COD_COMUNA'].'</COD_COMUNA>
                    <CALLE>'.$datos['CALLE'].'</CALLE>
                    <NUMERO>'.$datos['NUMERO'].'</NUMERO>
                    <ES_DIR_EECC>S</ES_DIR_EECC>
                    <IND_ACCION_BLOQ_DIR></IND_ACCION_BLOQ_DIR>
                    <MOTIVO_RECHZ></MOTIVO_RECHZ>
                    <IND_ESTADO>-</IND_ESTADO>
                    <COD_FUENTE></COD_FUENTE>
                    <COD_AREA>'.$datos['COD_AREA'].'</COD_AREA>
                    <NUM_TELEFONO>'.$datos['NUM_TELEFONO'].'</NUM_TELEFONO>
                    <TIP_TELEFONO>C</TIP_TELEFONO>
                    <TEL_UBICACION></TEL_UBICACION>
                    <EXTENSION></EXTENSION>
                    <ES_DEFAULT></ES_DEFAULT>
                    <INCLUIDO_POR></INCLUIDO_POR>
                    <FEC_INCLUSION></FEC_INCLUSION>
                    <MODIFICADO_POR></MODIFICADO_POR>
                    <FEC_MODIFICACION></FEC_MODIFICACION>
                    <TEL_ACTIVO></TEL_ACTIVO>
                    <COD_FUENTE_FONO></COD_FUENTE_FONO>
                </updateClienteICRequest>
        ', XSD_ANYXML, null);

            $parametros = array("updateClienteICRequest" => $updateClienteICRequest);
            $metodo = "updateClienteIC";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
            if($respuesta['error']){ 
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 7, 
                    "deTransaccion"        => 'Actualiza Datos Clientes IC', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
         
           
            if($respuesta['datos']['updateClienteICResponse']['ESTADO_PROCESO'] != 0){
                $respuesta['error'] = true;
                $respuesta['msg']   = 'sus datos no se actualizaron';
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 7, 
                    "deTransaccion"        => 'Actualiza Datos Clientes IC', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                   
                ];
                self::Trazabilidad($datos);
            }
			
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        return $respuesta;
    }

    public static function mergeMaileecc($PCODPERSONA, $PEMAIL, $PINDEECCEMAIL, $PESLEGAL = '', $PINCLUIDOPOR ){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.wS-mergeMaileecc");
		
            $parametros = array(
                'PCODPERSONA'   => $PCODPERSONA,
                'PEMAIL'        => $PEMAIL,
                'PINDEECCEMAIL' => $PINDEECCEMAIL,
                'PESLEGAL'      => $PESLEGAL,
                'PINCLUIDOPOR'  => $PINCLUIDOPOR,
            );

            $metodo = "mergeMaileecc";

            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);

            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 8, 
                    "deTransaccion"        => 'Actualiza mail legal', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
                return $respuesta;
            }

            if($respuesta['datos']['PCODERROR'] == 1){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 8, 
                    "deTransaccion"        => 'Actualiza mail legal', 
                    "cdError"              => '',
                    "cd_accion"            =>  11,
                    "de_accion"            => 'Estado de Cuenta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
                return $respuesta;
            }
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        

      
        return $respuesta;
        
        
    }
    public static function wsBloqueodes($datos){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.ws-Bloqueodes");

            $parametros = $datos;
            $metodo = "NewOperation";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);

            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 3, 
                    "deTransaccion"        => 'Error bloqueando tarjeta', 
                    "cdError"              => '',
                    "cd_accion"            =>  12,
                    "de_accion"            => '', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                   
                ];
                self::Trazabilidad($datos);
                return $respuesta;
            }

            if($respuesta['datos']['PCODRETORNO'] != 0){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 3, 
                    "deTransaccion"        => 'Error bloqueando tarjeta', 
                    "cdError"              => '',
                    "cd_accion"            =>  12,
                    "de_accion"            => '', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['msg'], 
                   
                ];
                self::Trazabilidad($datos);
            }else{

                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 0, 
                    "deTransaccion"        => 'Exito', 
                    "cdError"              => '',
                    "cd_accion"            =>  12,
                    "de_accion"            => '', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                    
                ];
                self::Trazabilidad($datos);
            }

        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }
    public static function wspolaris(){
        error_reporting(E_ALL);
        try {
            
            $conexion = Config::get("configSocket.ws-Polaris");
		
            $parametros = array(
                /* 'PCODPERSONA'   => $PCODPERSONA,
                'PEMAIL'        => $PEMAIL,
                'PINDEECCEMAIL' => $PINDEECCEMAIL,
                'PESLEGAL'      => $PESLEGAL,
                'PINCLUIDOPOR'  => $PINCLUIDOPOR, */
            ); 
            
            $metodo = "processMessage";
    
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);

            

        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }
    public static function Trazabilidad($datos = [] ){
        error_reporting(E_ALL);
        try {
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
         
          
            $conexion = Config::get("configSocket.Trazabilidad");
            
            $agent = new Agent();
            $browser = $agent->browser();
            //sessionID
            $session = Session::all();
            $datos = [
                "rut"                  => array_key_exists('rut', $datos) ? $datos['rut'] : '', 
                "idSesion"             => array_key_exists('sessionID', $session) ? $session['sessionID'] : '',
                "IP"                   => self::getServerAddress(), 
                "infoBrowser"          => $browser ." V-".  $agent->version($browser), 
                "infoSO"               => self::getPlatform($user_agent), 
                "numTelefono"          => '', 
                "cdCanal"              => $conexion['cdCanal'],
                "cdAccion"            => array_key_exists('cd_accion', $datos) ? $datos['cd_accion'] : '', 
                //"de_accion"            => array_key_exists('de_accion', $datos) ? $datos['de_accion'] : '',
                "cdTienda"             => $conexion['CdTienda'], 
                "cdTransaccion"        => array_key_exists('cdTransaccion', $datos) ? $datos['cdTransaccion'] : '', 
                "deTransaccion"        => array_key_exists('deTransaccion', $datos) ? $datos['deTransaccion'] : '', 
                "cdAplicacion"         => $conexion['cdAplicacion'],
                "cdError"              => array_key_exists('cdError', $datos) ? $datos['cdError'] : '', 
                "entradaTransaccion"   => array_key_exists('entradaTransaccion', $datos) ? $datos['entradaTransaccion'] : '', 
                "salidaTransaccion"    => array_key_exists('salidaTransaccion', $datos) ? $datos['salidaTransaccion'] : '', 
            ];
        
        if($datos['cdTransaccion'] == 0){
            $datos["entradaTransaccion"] = "";
            $datos["salidaTransaccion"]  = "";
        }
	    if (is_array($datos["salidaTransaccion"])) {
	    	$datos["salidaTransaccion"] = json_encode($datos["salidaTransaccion"]);
	    } else {
	    	str_replace(["\/", "\n"], ["/", "\n"], $datos["salidaTransaccion"]);
	    }
	     if (is_array($datos["entradaTransaccion"])) {
	    	$datos["entradaTransaccion"] = json_encode($datos["entradaTransaccion"]);
	    } else {
	    	str_replace(["\/", "\n"], ["/", "\n"], $datos["entradaTransaccion"]);
	    }
	    
	    $datos["salidaTransaccion"] = str_replace('\\n', "\n", $datos["salidaTransaccion"]);
       
        
        
            $respuesta ="";
            $respuesta = self::conexionCurlJSON($conexion['url'], $datos);
            //\Storage::put('trazabilidad.txt', json_encode($datos));

            \Storage::disk('public')->append('trazabilidad.txt', "[" . date('Y-m-d H:i:s') . "] " . json_encode($datos, JSON_PRETTY_PRINT));
        
        } catch (\Exception $e) {
           
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }

    protected static function conexionCurlJSON($url,$datos = "")
    {
            //iniciamos curl
            $ch = curl_init();
            
            //seteamos la url
            curl_setopt($ch, CURLOPT_URL, $url);
        
            //seteamos los datos post asociados a la variable xmlData
       
	        if(is_array($datos)){
	            $json = json_encode($datos);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	        }
                
            //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            
            //tiempo de espera, 0 infinito
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            
            //ejecutamos la petición y asignamos el resultado a $data
    
            $data = curl_exec($ch);
            
            //cerramos la petición curl
            curl_close($ch);

            self::log([
                'url'  => $url,
                'request'  => $datos,
                'response' => $data
            ]);
            
            return $data;
    }


    protected static function getServerAddress() {
        if(isset($_SERVER["SERVER_ADDR"]))
            return $_SERVER["SERVER_ADDR"];
        else {
            if(stristr(PHP_OS, 'WIN')) {
                exec('ipconfig /all', $catch);
                foreach($catch as $line) {
                    if(eregi('IP Address', $line)) {
                        if(count($lineCount = split(':', $line)) == 1) {
                            list($t, $ip) = split(':', $line);
                            $ip = trim($ip);
                        } else {
                            $parts = explode('IP Address', $line);
                            $parts = explode('Subnet Mask', $parts[1]);
                            $parts = explode(': ', $parts[0]);
                            $ip = trim($parts[1]);
                        }
                        if(ip2long($ip > 0)) {
                        echo 'IP is '.$ip."\n";
                        return $ip;
                        } else
                        ; // TODO: Handle this failure condition.
                    }
                }
            } else {
                $hostname = shell_exec('hostname -I');
                return trim($hostname);
            }
        }
    }

    protected static function getPlatform($user_agent) {
		if(strpos($user_agent, 'Windows NT 10.0') !== FALSE)
			return "Windows 10";
		elseif(strpos($user_agent, 'Windows NT 6.3') !== FALSE)
			return "Windows 8.1";
		elseif(strpos($user_agent, 'Windows NT 6.2') !== FALSE)
			return "Windows 8";
		elseif(strpos($user_agent, 'Windows NT 6.1') !== FALSE)
			return "Windows 7";
		elseif(strpos($user_agent, 'Windows NT 6.0') !== FALSE)
			return "Windows Vista";
		elseif(strpos($user_agent, 'Windows NT 5.1') !== FALSE)
			return "Windows XP";
		elseif(strpos($user_agent, 'Windows NT 5.2') !== FALSE)
			return 'Windows 2003';
		elseif(strpos($user_agent, 'Windows NT 5.0') !== FALSE)
			return 'Windows 2000';
		elseif(strpos($user_agent, 'Windows ME') !== FALSE)
			return 'Windows ME';
		elseif(strpos($user_agent, 'Win98') !== FALSE)
			return 'Windows 98';
		elseif(strpos($user_agent, 'Win95') !== FALSE)
			return 'Windows 95';
		elseif(strpos($user_agent, 'WinNT4.0') !== FALSE)
			return 'Windows NT 4.0';
		elseif(strpos($user_agent, 'Windows Phone') !== FALSE)
			return 'Windows Phone';
		elseif(strpos($user_agent, 'Windows') !== FALSE)
			return 'Windows';
		elseif(strpos($user_agent, 'iPhone') !== FALSE)
			return 'iPhone';
		elseif(strpos($user_agent, 'iPad') !== FALSE)
			return 'iPad';
		elseif(strpos($user_agent, 'Debian') !== FALSE)
			return 'Debian';
		elseif(strpos($user_agent, 'Ubuntu') !== FALSE)
			return 'Ubuntu';
		elseif(strpos($user_agent, 'Slackware') !== FALSE)
			return 'Slackware';
		elseif(strpos($user_agent, 'Linux Mint') !== FALSE)
			return 'Linux Mint';
		elseif(strpos($user_agent, 'Gentoo') !== FALSE)
			return 'Gentoo';
		elseif(strpos($user_agent, 'Elementary OS') !== FALSE)
			return 'ELementary OS';
		elseif(strpos($user_agent, 'Fedora') !== FALSE)
			return 'Fedora';
		elseif(strpos($user_agent, 'Kubuntu') !== FALSE)
			return 'Kubuntu';
		elseif(strpos($user_agent, 'Linux') !== FALSE)
			return 'Linux';
		elseif(strpos($user_agent, 'FreeBSD') !== FALSE)
			return 'FreeBSD';
		elseif(strpos($user_agent, 'OpenBSD') !== FALSE)
			return 'OpenBSD';
		elseif(strpos($user_agent, 'NetBSD') !== FALSE)
			return 'NetBSD';
		elseif(strpos($user_agent, 'SunOS') !== FALSE)
			return 'Solaris';
		elseif(strpos($user_agent, 'BlackBerry') !== FALSE)
			return 'BlackBerry';
		elseif(strpos($user_agent, 'Android') !== FALSE)
			return 'Android';
		elseif(strpos($user_agent, 'Mobile') !== FALSE)
			return 'Firefox OS';
		elseif(strpos($user_agent, 'Mac OS X+') || strpos($user_agent, 'CFNetwork+') !== FALSE)
			return 'Mac OS X';
		elseif(strpos($user_agent, 'Macintosh') !== FALSE)
			return 'Mac OS Classic';
		elseif(strpos($user_agent, 'OS/2') !== FALSE)
			return 'OS/2';
		elseif(strpos($user_agent, 'BeOS') !== FALSE)
			return 'BeOS';
		elseif(strpos($user_agent, 'Nintendo') !== FALSE)
			return 'Nintendo';
		else
			return 'Unknown Platform';
    }


    /**
     * Setter de metodo conexionWebService($config = '', $param = '', $metodo= '', $prueba = false, $login = false)
     * 
     * @autor Dickson Morales
     * @param String $config
     * @param String $param
     * @param String $metodo
     * @param Boolean $prueba
     * @param Boolean $login
     */
    public static function setConexionWebService($config = '', $param = '', $metodo= '', $prueba = false, $login = false, $option = false ){
        return self::conexionWebService($config, $param, $metodo, $prueba, $login, $option );
    }
    public static function setConexionSocket($conexion1 = '', $contador = false){
		
        return self::conexionSocket($conexion1, $contador);
    }
    
    public static function log($arr)
    {
        if (is_array($arr)) {
            $arr = json_encode($arr, JSON_PRETTY_PRINT);
        }

        \Log::debug($arr);
        return;
    }

    public static function IDverifier($rut, $numSerie ) {
        try {
            
            $conexion = Config::get("configSocket.ws-IDverifier-pro");
  

            $parametros = array(
                'Identity' => [
                    'RUT'   => $rut,
                    'IDCardSerialNumber' => $numSerie
                ],
                'ProcessingOptions'=> [
                    'Language'=> 'ES',
                    'EnvironmentOverride' => $conexion["env"]
                ]
            ); 
            
            $metodo = "startTransaction";    

            $respuesta = self::conexionWebServiceEquifax($conexion, $parametros, $metodo, false);
	    
	    if($respuesta['error']){
            $datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 4, 
                "deTransaccion"        => 'Equifax: Servicio eIDverifier', 
                "cdError"              => '',
                "cd_accion"            => 10,
                "de_accion"            => 'Equifax: Servicio eIDverifier', 
                "entradaTransaccion"   => $parametros, 
                "salidaTransaccion"    => $respuesta['datos'], 
            ];

           	 self::Trazabilidad($datos);
	    }
           
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }
    
    public static function IDverifierEnvioPreguntas($respuestas, $rut = 19){
        try {
            
            $conexion = Config::get("configSocket.ws-IDverifier");
            /*
                'RUT'                => '98542353',
                'IDCardSerialNumber' => 'A007027811'
            */

            $xml = '';
            

            $preguntas = Session::get('IDverifier');
            
            foreach ($respuestas as $key => $respuesta) {
                $xml .= '<Answer questionId="'.$key.'" answerId="'.$respuesta.'"/>' ;
            }
            
            $val = '<IQAnswerRequest  xmlns="http://eid.equifax.com/soap/schema/chile" transactionKey="'.$preguntas['datos']['transactionKey'].'" interactiveQueryId="'.$preguntas['datos']['InteractiveQuery']['interactiveQueryId'].'">';
            $val .=  $xml;
            $val .= '</IQAnswerRequest>';
            
            
            $IQAnswerRequest = new \SoapVar($val, XSD_ANYXML, null);
            
            $parametros = array("IQAnswerRequest" => $IQAnswerRequest); 
             
            
            $metodo = "interactiveQueryResponse";
        
            
            $respuesta = self::conexionWebServiceEquifax($conexion, $parametros, $metodo, false);

            $datos = [
                "rut"                  => $rut, 
                "cdTransaccion"        => 5, 
                "deTransaccion"        => 'Equifax: Servicio de respuestas identidad cliente', 
                "cdError"              => '',
                "cd_accion"            =>10,
                "de_accion"            => 'Login', 
                "entradaTransaccion"   => $parametros, 
                "salidaTransaccion"    => $respuesta['datos'], 
            ];

            self::Trazabilidad($datos);
          
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;

    }

    public static function processMessage($rut,$NROCUENTA,$NROTARJETA, $traza = true ){
        try {
            
            $conexion = Config::get("configSocket.processMessage");

            $soapvar = new \SoapVar('<input>
            <![CDATA[
                <CONSCuentaBlq_REQ_COMP_IN>
                    <ACCION>C</ACCION>
                    <RUTCLIENTE>'.$rut.'</RUTCLIENTE>
                    <NROCUENTA>'.$NROCUENTA.'</NROCUENTA>
                    <NROTARJETA>'.$NROTARJETA.'</NROTARJETA>
                    <CODPRODUCTO>24101</CODPRODUCTO>
                    <BLOQUEOS>66</BLOQUEOS>
                </CONSCuentaBlq_REQ_COMP_IN>
            ]]>
            </input>', XSD_ANYXML, null);

            $parametros = array("input" => $soapvar);

            $metodo = "processMessage";
            $respuesta = self::conexionWebService($conexion, $parametros, $metodo, false);
            if($respuesta['error']){
                $datos = [
                    "rut"                  => $rut, 
                    "cdTransaccion"        => 2, 
                    "deTransaccion"        => 'Consulta bloqueo Tarjeta', 
                    "cdError"              => '',
                    "cd_accion"            => 14,
                    "de_accion"            => 'Consulta bloqueo Tarjeta', 
                    "entradaTransaccion"   => $parametros, 
                    "salidaTransaccion"    => $respuesta['datos'], 
                ];
                self::Trazabilidad($datos);
            }else{
                if($traza){
                    $datos = [
                        "rut"                  => $rut, 
                        "cdTransaccion"        => 0, 
                        "deTransaccion"        => 'Exito', 
                        "cdError"              => '',
                        "cd_accion"            => 14,
                        "de_accion"            => 'Exito',
                        "entradaTransaccion"   => $parametros,
                        "salidaTransaccion"    => $respuesta['datos'],
                    ];
		    self::Trazabilidad($datos);
                }
              
                
            }
        } catch (\Exception $e) {
            
            $respuesta['error'] = true;
            $respuesta['msg']  =  $e->getMessage();
            $respuesta['datos'] =  [];
            $respuesta['Exception'] =  true;
            return $respuesta;
        }
        
        return $respuesta;
    }

  
}
