<?php
namespace App\ParamFacades;

use Modules\Metadato\Entities\Metadato;
use Modules\Menu\Entities\Menu;

class Helper
{
    
    /**
    * Devuelve los metadatos globales, por ids de registros
    *   y por tags html
    *
    *   @autor Dickson Morales
    *   @return Array
    */
   public static function metadatosGlobales(){

        $Metadato = Metadato::with('tipometadato')
            ->global()
            ->get();

        $tags = '';

        $ids = [];

        if( $Metadato ){
            foreach ($Metadato as $indie => $meta ) {
                $ids[] = $meta->ABC04_id;

                $tags .= '<meta name="' . $meta->tipometadato->ABC05_nombre . '" ' . 
                        $meta->tipometadato->ABC05_attributos . '="' . $meta->ABC04_contenido . '" >';
            }
        }

        return [
            "ids" => $ids,
            "tags" => $tags
        ];
   }

    /**
    * Genera html de los menus tanto del footer como
    *  menu principal
    *
    *  @autor Dickson Morales
    *  @param Integer
    *  @return String
    */
    public static function menu( $id = false ){
        $html = '';

        if( $id ){

            $Menu = Menu::BuscarMenu( $id );

            if( count( $Menu ) > 0 ){
                foreach( $Menu as $menu ){
                  
                    
                    if( $menu['uri'] == 'mi-cuenta' ){
                        $html .= '<li style="font-size: 11px;">
                        '.( array_key_exists("icon", $menu ) && trim( $menu["icon"] ) != '' ? '<i class="' .$menu["icon"] . '"></i>' : '' ) . strtoupper($menu['title']) . '
                        </li>';
                    }else{
                        
                        $html .= '<li>
                            <a href="' . $menu['url'] .'" ' . ( isset($menu['target']) ? $menu["target"] : '' ) . ' ' . ( trim( $menu["class"] ) != '' ? ' class="' . $menu["class"] . '"' : '' ) . '>' . 
                                ( array_key_exists("icon", $menu ) && trim( $menu["icon"] ) != '' ? '<i class="' .$menu["icon"] . '"></i>' : '' ) 
                                . strtoupper($menu['title']) . '
                            </a>
                        </li>';
                    }
                    
                }   
            }
        }

        return $html;
    }

    /**
     * Metodo para validar si un rut es valido
     * 
     * @author Dickson Morales
     * @param String Rut
     * @return Boolean 
     */
    public static function validaRut( $rut = '' ){

        $validacion = false;

        if( trim( $rut ) != '' ){
            $rut = preg_replace( "/(\-|\.|\s)+/", "", $rut );
            $dv = strtoupper( substr ( $rut, - 1 ) );

            $rut = number_format (
                ( int ) preg_replace ( 
                    '/[^0-9]/', 
                    '', 
                    substr ( $rut, 0, - 1 ) 
                ), 0, "", "." ) . '-' . 
                $dv;
            
            // agrega cero inicial
            if (strpos ( $rut, '.' ) == 1) {
                $rut = '0' . $rut;
            }

            // valida rut
            $rut2 = $rut;

            $rut = preg_replace ( '/[^k0-9]/i', '', $rut );
            $numero = substr ( $rut, 0, strlen ( $rut ) - 1 );
            
            if( is_numeric( $numero ) ){
                $i = 2;
                $suma = 0;
                foreach ( array_reverse ( str_split ( $numero ) ) as $v ){
                    if($i==8)
                        $i = 2;
                    $suma += $v * $i;++ $i;
                }
                
                $dvr = 11 - ($suma % 11);
                if ($dvr == 11) {
                    $dvr = 0;
                }
    
                if ($dvr == 10) {
                    $dvr = 'K';
                }

                if($dvr == strtoupper($dv)){
                    $validacion = true;
                }
            }            
        }
        
        return $validacion;
    }
}