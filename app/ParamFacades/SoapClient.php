<?php

namespace App\ParamFacades;

use SoapClient as SoapClientPHP;
use Log;
use WsSocket;

class SoapClient extends SoapClientPHP
{
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {	
      
	
	if($location === "http://wildfly/SafeSignerWs-v2/SmsWs"){
            $location = "https://dev.safesigner.com/SafeSignerWs-v2/SmsWs?wsdl";
        }
	    if ($location === 'http://10.240.0.3/abcdin/SmsWs') {
           //dd($request, $location);
	        $location = "https://10.240.0.3/abcdin/SmsWs";
        }
        
        if ($location === 'http://abcdin.amazon.trunk.netred.cl/uru/soap-latam/ut/70/chile') {
	        $location = "https://abcdin.amazon.trunk.netred.cl/uru/soap-latam/ut/70/chile";
        }
        
        if ($location === 'http://soa-osb-vip.adretail.cl/osb/SitioretfinInterfazCreaticketeecc/Ent/EntRetfCreaticketeeccSecureService?WSDL') {
	        dd($request, $location, $action, $version, $one_way);
        }
        
        $location = preg_replace("/clpr\d*lnx\d*osb\d*:\d*/", preg_replace("/http(s)*\:\/\//i", "", env( "APP_URL" )), $location);      
	
        $respuesta = parent::__doRequest($request, $location, $action, $version, $one_way);
        $this->log([
            "REQUEST"    => $request,
            "RESPONSE"   => $respuesta,
            "LOCATION"   => $location,
            "ACTION"     => $action,
            "VERSION"    => $version,
            "ONE WAY"    => $one_way
        ]);
        return $respuesta;
    }

    public function log($arr)
    {
        if (is_array($arr)) {
            $arr = json_encode($arr);
        }

        Log::debug($arr);
        return;
    }
}
