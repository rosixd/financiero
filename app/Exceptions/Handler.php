<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\User\Contracts\Authentication;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Contracts\Container\Container;
use OwenIt\Auditing\Models\Audit;
use OwenIt\Auditing\Resolvers\IpAddressResolver;
use OwenIt\Auditing\Resolvers\UserResolver;
use OwenIt\Auditing\Resolvers\UserAgentResolver;
use OwenIt\Auditing\Resolvers\UrlResolver;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {	
        $errorBdGlobal = false;
        /*echo "PDOException: " . ($e instanceof \PDOException) 
         .  "<br>QueryException:" . ( $e instanceof \QueryException );
*/
        /*
        if( ($e instanceof \PDOException) || ( $e instanceof \QueryException ) ){
            $errorBdGlobal = true;
            echo "<hr>DMT: handler -> report";
            dd( $e );
            dd( $e->getMessage() );
            dd( $e->getTrace() );
            
        }
        */
        
        if( !($e instanceof \PDOException) && !( $e instanceof \QueryException ) ){
            $ip_address = IpAddressResolver::resolve();
            
            $user = UserResolver::resolve();
            $user_agent = UserAgentResolver::resolve();
            $url = UrlResolver::resolve();
        
            $name = get_class($e);
            //dd( json_encode($e->getTraceAsString(), JSON_PRETTY_PRINT) );
            Audit::create([
                'old_values'      => ( $user ? $user : null),
                'event'          => 'error',
                'new_values'     => 'codigo: '.$e->getCode().'; message: '.$e->getMessage().'; nombre del fchero: '.$e->getFile().'; linea: '.$e->getLine().'; trace: '.$e->getTraceAsString(),
                'url'            => $url,
                'ip_address'     => $ip_address,
                'mac_address'    => "00:00:00:00",
                'user_agent'     => $user_agent,
                'coments'        => $name,
                //'created_at'     => '',
                //'updated_at'     => '',
            ]);
        }
/* 
        if( $errorBdGlobal ){
            return response()
                ->view('errors.globalBd');
        } */
          
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ValidationException) {
            return parent::render($request, $e);
        }

        if ($e instanceof TokenMismatchException) {
            return redirect()
                ->back()
                 ->withInput($request->except('password'))
                 ->withErrors(trans('core::core.error token mismatch'));
        }

        if (config('app.debug') === false) {
            return $this->handleExceptions($e);
        }

        /*
        if( ($e instanceof \PDOException) || ( $e instanceof \QueryException ) ){
            $errorBdGlobal = true;
            echo "<hr>DMT: handler -> render con backslash";
            dd( 
                $e->getMessage(), 
                $e->getCode(), 
                $e instanceof \PDOException ? 'es PDOException' : 'es QueryException', 
                $e 
            );
        }
        */

        return parent::render($request, $e);
    }

    private function getMessageDB($e)
    {
        $traces = $e->getTrace();
        $ruta = trim( strtolower(url()->getRequest()->getPathInfo()) );
        $method = trim( strtolower(request()->method()) );

        $objetivos = [
            'MetadatoController'         => ['el metadato', 'los metadatos'],
            'TipometadatoController'     => ['el tipo de metadato', 'los tipos de metadatos'],
            'UserController'             => ['el usuario', 'los usuarios'],
            'MenuController'             => ['el menú', 'los menús'],
            'MenuItemController'         => ['el ítem', 'los ítems'],
            'MediaController'            => ['el archivo', 'los archivo'],
            'PageController'             => ['la página', 'las páginas'],
            'RolesController'            => ['el perfil', 'los perfiles'],
            'TranslationController'      => ['el mensaje', 'los mensajes'],
            'SettingController'          => ['el mensaje', 'los mensajes'],

            'LogController'              => ['el log', 'los logs'],
            'CreditosConsumosController' => ['el credito de consumo', 'los creditos de consumos'],
            'FormulariosController'      => ['el formulario', 'los formularios'],
            'RespuestasController'       => ['el respuesta', 'las Respuestas'],
        ];

        $objetivosUrl = [
            'metadatos'      => $objetivos['MetadatoController'],
            'tipometadatos'  => $objetivos['TipometadatoController'],
            'users'          => $objetivos['UserController'],
            'menuitem'       => $objetivos['MenuItemController'],
            'menus'          => $objetivos['MenuController'],
            'menu'          => $objetivos['MenuController'],
            'media'          => $objetivos['MediaController'],
            'pages'          => $objetivos['PageController'],
            'roles'          => $objetivos['RolesController'],
            'translations'   => $objetivos['TranslationController'],
            'translation'    => $objetivos['TranslationController'],
            'settings'       => $objetivos['SettingController'],
            'setting'        => $objetivos['SettingController'],
		    'file-dropzone'  => $objetivos['MediaController'],
            'logs'           => $objetivos['LogController'],
            'creditoconsumo' => $objetivos['CreditosConsumosController'],
            'formulario'     => $objetivos['FormulariosController'],
            'respuesta'      => $objetivos['RespuestasController'],
        ];

        $acciones = [
            'index'     => 'listar',
            'create'    => 'ingresar',
            'store'     => 'ingresar',
            'edit'      => 'modificar',
            'update'    => 'modificar',
            'destroy'   => 'desactivar',
            'activar'   => 'activar',
            'datatable' => 'listar',
            'upload'    => 'subir'
        ];        
        
        $msj = '';
        
        foreach ($traces as $trace) {
            $file     = isset($trace['file'])     ? $trace['file']     : '';
            $class    = isset($trace['class'])    ? $trace['class']    : '';
            $function = isset($trace['function']) ? $trace['function'] : '';
            
            if (preg_match('/^Modules\\\\(\w+)\\\\Http\\\\Controllers\\\\(.+)/', $class, $coincidencias)) {
                $modulo = $coincidencias[1];
                $controller = explode('\\', $coincidencias[2]);
                $controller = end($controller);
                
                if ($controller == 'AuthController') {
                    $msj = "Sr. Usuario, en estos momentos no se encuentra disponible la plataforma, por favor vuelva a intentarlo";
                    if ($function == 'postReset') {
                        $msj = "Sr. Usuario, no se ha podido enviar el correo, por favor inténtelo nuevamente";
                    }
                    break;
                }

                if (!isset($acciones[$function]) || !isset($objetivos[$controller])) {
                    return '';
                }

                $objetivo = $objetivos[$controller][0];
                if ($function == 'index' || $function == 'datatable') {
                    $objetivo = $objetivos[$controller][1];
                }

                if ($controller == 'MediaController' && ($function == 'create' || $function == 'store')) {
                    $msj = 'Sr. Usuario, no se ha podido subir el archivo, por favor inténtelo nuevamente.';
                    break;
                }
                
                $msj = "Sr. Usuario, no se ha podido " . $acciones[$function] . " " . $objetivo . ", por favor inténtelo nuevamente.";
                break;
            }
        }

        if ($msj === '') {
            foreach ($objetivosUrl as $key => $value) {
                if (strpos($ruta, $key) === false) {
                    continue;
                }

                $function = 'index';

                if (strpos($ruta, 'menuitem') !== false &&  $method === 'post' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido eliminar el ítem, por favor inténtelo nuevamente.';
                    break;
                }

                if (strpos($ruta, 'tipometadatos') !== false &&  $method === 'get' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido desactivar el tipo de metadato, por favor inténtelo nuevamente.';
                    break;
                }
                //dd( $method );
                if (strpos($ruta, 'tipometadatos') !== false &&  $method === 'post' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido ingresar el tipo de metadato, por favor inténtelo nuevamente.';
                    break;
                }
                if (strpos($ruta, 'tipometadatos') !== false &&  $method === 'put' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido modificar el tipo de metadatos, por favor inténtelo nuevamente.';
                    break;
                }

                if (strpos($ruta, 'settings') !== false &&  $method === 'post' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido modificar la configuración, por favor inténtelo nuevamente.';
                    break;
                }

                if (strpos($ruta, 'translation') !== false &&  $method === 'post' && $function == 'index' ) {
                    $msj = 'Sr. Usuario, no se ha podido modificar el mensaje, por favor inténtelo nuevamente.';
                    break;
                }
                
                if (strpos($ruta, 'destroy') !== false || $method === 'delete') {
                    $function = 'activar';
                    if(request()->has('status') && request()->status === 'true'){
                        $function = 'destroy';
                    }
                }elseif ($method === 'post') {
                    $function = 'create';
                }elseif (strpos($ruta, 'datatable') !== false) {
                    $function = 'datatable';
                }elseif (strpos($ruta, 'edit') !== false || $method === 'put' || $method === 'patch') {
                    $function = 'edit';
                }
                $objetivo = $value[0];
                if ($function == 'index' || $function == 'datatable') {
                    $objetivo = $value[1];
                }

                if (strpos($ruta, 'auth') !== false) {
                    $msj = "Sr. Usuario, en estos momentos no se encuentra disponible la plataforma, por favor vuelva a intentarlo";
                    if (strpos($ruta, 'reset') !== false && $method === 'post') {
                        $msj = "Sr. Usuario, no se ha podido enviar el correo, por favor inténtelo nuevamente";
                    }
                    break;
                }

                if(
                    ( strpos($ruta, 'media') !== false && ($function == 'create' || $function == 'store')) ||
                    (
                        request()->ajax() &&
                        preg_match( "/^post?/i", trim($method) ) && 
                        preg_match( "/^\/api\/file\-dropzone?/", trim( $ruta ) ) 
                    )
                ){
                    $msj = 'Sr. Usuario, no se ha podido subir el archivo, por favor inténtelo nuevamente.';
                    break;
                }

                $msj = "Sr. Usuario, no se ha podido " . $acciones[$function] . " " . $objetivo . ", por favor inténtelo nuevamente.";
                
                break;
            }
        }

        if ($msj === '') {
            $msj = 'Sr. Usuario, se ha producido un error interno en el servidor, por favor intente mas tarde.';

            /**
             * Mensaje personalizado para la subida de archivos mediante
             * drop file de multimedia
             * 
             * æutor Dickson Morales
             */
            if( 
                request()->ajax() &&
                preg_match( "/^post?/i", trim($method) ) && 
                preg_match( "/^\/api\/file\-dropzone?/", trim( $ruta ) ) 
            ){
                $msj = 'Sr. Usuario, no se ha podido subir el archivo, por favor inténtelo nuevamente.';
            }
            // Fin de la funcionalidad
        }

        return $msj;
    }

    private function handleExceptions($e)
    {
        $error = $e->getMessage();
        $error = 'Sr. Usuario, se ha producido un error interno en el servidor, por favor intente mas tarde.';

        if( ($e instanceof \PDOException || $e instanceof \QueryException) && (
            $e->getCode() == 1045 || // credenciales no validas
            $e->getCode() == 2005 || // no existe el servicio
            $e->getCode() == 1044 || // no existe la base de datos
            $e->getCode() == 2003    // error de conexion (puerto equivocado)
        )){
            $error = $this->getMessageDB($e);
        }
        
        if ($e instanceof ModelNotFoundException) {
            if(request()->ajax()){
                return \Response::json(array(
                    'message'   =>  $error
                ), 404);
            }
		
            return response()
                ->view('errors.404', [
                    'message' => $error
                ], 404);
        }

        if ($e instanceof NotFoundHttpException) {
            if(request()->ajax()){
                return \Response::json(array(
                    'message'   =>  $error
                ), 404);
            }
            return response()
                ->view('errors.404', [
                    'message' => $error
                ], 404);
        }
        if (request()->ajax()) {
            return \Response::json(array(
                    'message'   =>  $error
                ), 500);
        }

        return response()
            ->view('errors.500', [
                'message' => $error
            ], 500);
    }
}
