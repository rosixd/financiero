<?php 
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class WsSocket extends Facade{
   
	/**
	 * DMT: Obtienendo el nombre del facade de conexion con miliways
	 * @return String
	*/
	protected static function getFacadeAccessor(){
		return 'WsSocket';
	}
}