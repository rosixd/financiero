<?php 
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Helper extends Facade{
   
	/**
	 * DMT: Helper general
	 * @return String
	*/
	protected static function getFacadeAccessor(){
		return 'Helper';
	}
}