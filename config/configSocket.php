<?php
/**
 *  MA: Tramas y URL de los WS y MW del sistema 
 */
return [
	"MW-CONINFOCUENTA3" => [
		'host' => "192.168.75.205", 
		'puerto' => "21000",
		'trama' => "00TINFCTC.Tc_InterfazPos3.ConInfoCuenta3('','500','{RUT}','POS','19','')"
	],
	"MW-TcObtDisponible-IC" => [
		'host' => "192.168.75.145", 
		'puerto' => "9999",
		'trama' => "00TCDISTC.Tc_Obt_Disponible('{COD_EMPRESA}', '{CUENTA_CLIENTE}', '1', '500', '{FECHA}', '{CLASIFICACION}')"		
	],
	"MW-TcObtDisponible-TC" => [
		'host' => "192.168.51.234", 	
		'puerto' => "11000",
		'trama' => "00TCDISTC.Tc_Obt_Disponible('{COD_EMPRESA}', '{CUENTA_CLIENTE}', '3', '500', '{FECHA}', '{CLASIFICACION}')"
	],
	"MW-OBTENERDUPLICA" => [
		'host' => "192.168.75.128", 	
		'puerto' => "9999",
		'trama' => "TEXETTC.TC_INTERFAZPOS3.OBTENERDUPLICA('{empresa}','{cuenta}','{comercio}','')"
	],

	"MW-EECC_TAR" => [
		'host' => "192.168.75.128", 
		'puerto' => "9999",
		'trama' => "00TEECC.Tc_InterfazPos3.EECC_TAR('{numerotarjera}','1')"
	],
	"MW-datospersona" => [
		'host' => "192.168.51.234", 
		'puerto' => "11000",
		'trama' => "00BDCLITC.Tc_interfazpos3.datospersona('{RUT}')"
		
	],
	"WS-TaEstadoBloqClienteService" => [
		'url' => "http://192.168.50.96/osb/AutvisawebpayInterfazAutenticacionweb/Ent/EntRetfEstadobloqclienteService?wsdl"
	],
	"WS-NewOperation" => [
		'url' => "http://192.168.50.96:80/osb/AppmovilInterfazDatosCliente/Ent/EntRetfDatosclienteService?wsdl"
	],
	"WS-getTicketeecc" => [
		'url' => "http://192.168.50.96/osb/SegttaInterfazConsultaticketeecc/Ent/EntRetfConsultaticketeccService?wsdl"
	],
	"WS-EntRetfActualizaestatusclienteService" => [
		'url' => "http://192.168.50.96/osb/AutvisawebpayInterfazAutenticacionweb/Ent/EntRetfActualizaestatusclienteService?wsdl"
	],
	 "CSI" => [
		'url' => "http://192.168.50.225/CSI_TrustedConnectWeb/CSIv1001",
		'usuario' => 'USRSITFINAWS',
		'llave'	  => 'CSKSITFINAWS',
		'mancha'  => '5204949238473E919D3FCE582F8D654F5C268837B2AB5A4E',
		'ip' 	  => $_SERVER['SERVER_ADDR']
	], 
	"WS-getMaileecc" => [
		'url' => "http://192.168.50.96/osb/EbncOperacionesmaileecc/Ent/EntEbncOperacionesmaileeccService?wsdl"		
	],
	"WS-TC2000_EvaluacionExpress" => [
		'url' => "http://192.168.50.100/soa-infra/services/RetailFinanciero/TC2000_EvaluacionExpress/tc2000_evaluacionexpress_client_ep?WSDL"
	],
	"WS-SitioretfinInterfazCalculoextracupo" => [
		'url' => "http://soa-osb-vip.adretail.cl/osb/SitioretfinInterfazCalculoextracupo/Ent/EntRetfExtracupoSecureService?WSDL",
		"Auth" => true,
		'login' => "sitretfin",
		'password' => "S3rg31K0r0l3v"
	],
	"MW-totobligdia5-IC" => [
		'host' => "192.168.75.145", 
		'puerto' => "9999",
		'trama' => "00TEXECtc.Tc_InterfazPos3.TotObligDia5('{codEmpresa}','061','{NroTarjeta}')"
	],
	"MW-totobligdia5-TC" => [
		'host' => "192.168.51.234", 
		'puerto' => "11000",
		'trama' => "00TEXECtc.Tc_InterfazPos3.TotObligDia5('{codEmpresa}','061','{NroTarjeta}')"
	],
	"WS-SitioretfinInterfazCreaticketeecc" => [
        'url' => "http://soa-osb-vip.adretail.cl/osb/SitioretfinInterfazCreaticketeecc/Ent/EntRetfCreaticketeeccSecureService?WSDL",
        //'url' => "http://osbpmm.adretail.cl:443/osb/SitioretfinInterfazCreaticketeecc/Ent/EntRetfCreaticketeeccSecureService?wsdl",
        "Auth" => true,
        'login' => "sitretfin",
        'password' => "S3rg31K0r0l3v"
	],
	"MW-ConsultaFlujoEECC" => [
		'host' => "192.168.75.128", 
		'puerto' => "9999",
		'trama' => "00GEXECTC.TC_INTERFAZPOS3.ConsultaFlujoEECC('{EmpresaId}', '{NumCuenta}',  '{PrimerVcto}', '{Plazo}', '{ValorCuota}')"
	],
    "MW-EsClienteValidoWeb" => [
		'host' => "192.168.51.179", 
		'puerto' => "21000",
		'trama' => "00CLIVAPA.PA_INTERFAZTRANSACT.EsClienteValidoWeb_TC_IC('{PCODPERSONA}', '')"
	],
	"MW-EsClienteValidoWeb" => [
		'host' => "192.168.51.234",
		'puerto' => "11000",
		'trama' => "00CLIVAPA.PA_INTERFAZTRANSACT.EsClienteValido_TC_IC('{PCODPERSONA}')"
	],
	'wS-getDatosLegal' => [
		'url' => "http://192.168.50.96/osb/EbncOperacionesdatoslegal/Ent/EntEbncOperacionesdatoslegalSecureService?wsdl",
		"opciones" => [
			'encoding' => 'UTF-8' ,
            'login' => 'dtslegal',
            'password' => 'dt5l3g4l'
		]
	],
	'wS-SmsWs' => [
		'url' => "https://10.240.0.3/abcdin/SmsWs?wsdl",
		'login' => "",
		'password' => ""
	],
	'wS-ActualizaClienteIC' => [
		'url' => "http://192.168.50.126:80/abcdin/ACTUALIZADATOSCLIENTEICWS/services/ActualizaClienteICPort?WSDL",
	],
	'wS-mergeMaileecc' => [
		'url' => "http://192.168.50.96/osb/EbncOperacionesmaileecc/Ent/EntEbncOperacionesmaileeccService?wsdl"
	],
	'ws-Bloqueodes' => [
		'url' => "http://192.168.50.96/osb/EntRetfBloqueodesbloqicService?wsdl",
	],
	'ws-Polaris' => [
		'url' => "http://192.168.51.160:8031/MH/SOAPWS/Service?wsdl",
	],

	'ws-IDverifier-pro' => [
		//'url' => env('APP_URL')."/uru/soap-latam/ut/70/chile?wsdl",		
		'url' => env('APP_URL')."/osb-efx/Equifax/EidRenta?Wsdl",
		'encoding' => 'UTF-8' ,
		'login' => '/din1',
		'password' => 'M3ZQnSFMjLWgdul/oUq5RuqxZmUHIYMD',
		'env' => 'PROD' // Produccion => PROD
		
	],
	'ws-IDverifier' => [
		//'url' => env('APP_URL')."/uru/soap-latam/ut/70/chile?WSDL",
		'url' => env('APP_URL') . '/osb-efx/Equifax/EidRenta?Wsdl',
		'encoding' => 'UTF-8',
		'login' => '/din1',
		'password' => 'M3ZQnSFMjLWgdul/oUq5RuqxZmUHIYMD',
		'env' => 'PROD' // Produccion => PROD
	],
	
	'Trazabilidad' => [
		'url' => "http://192.168.50.96/osb/AppRegistroNavegacionTracking/Ent/EntRegistroNavegacionService",
		'cdCanal' => '2',
		'CdTienda' => '500',
		"cdAplicacion" => '2', 
	],
	'pagatucuenta' => [
		'url' => "https://soluciones.devetel.cl/ipago_abcdin/iframe_privado"
	],
	'processMessage' => [
		'url' => "http://192.168.51.160:8031/MH/SOAPWS/Service?wsdl"
	],
	'EstadoCuenta' => [
		'url' => "https://abcdin.jp2.cl/JRDNSLN01PR01/EstadoCuenta.aspx?"
	],
];
