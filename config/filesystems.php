<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */
    'default' => env('FILESYSTEM_DRIVER', 'local'),
    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */
    'cloud' => env('FILESYSTEM_CLOUD', 's3'),
    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => base_path(),
            'permissions' => [
                'file' => [
                    'public' => 0777,
                    'private' => 0700,
                ],
                'dir' => [
                    'public' => 0777,
                    'private' => 0700,
                ],
            ],
            'url' => env('APP_URL'),
            'visibility' => 'public',
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'framework' => [
            'driver' => 'local',
            'root' => storage_path('framework'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'content' => [
            'driver' => 'local',
            'root' => resource_path('views'),
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
        'ftp' => [
       
            'driver'   => env('FTP_DRIVER'),
            'host'     => env('FTP_HOST'),
            'username' => env('FTP_USERNAME'),
            'password' => env('FTP_PASSWORD'),
            'port'     => env('FTP_PORT'),

            // Optional FTP Settings...
            // 'port'     => 21,
            // 'root'     => '',
            // 'passive'  => true,
            // 'ssl'      => true,
            // 'timeout'  => 30,
        ],

        //Donde se guardaran los pdf de garantias
        'garantias' => [
            'driver' => 'local',
            'root' => storage_path('app/public/pdfs/garantias'),
            'url' => env('APP_URL') . '/storage/pdfs/garantias',
            'visibility' => 'public',
        ],

        // Donde se guardaran los pdf de estados de cuenta
        'eecc' => [
            'driver' => 'local',
            'root' => storage_path('app/public/pdfs/eecc'),
            'url' => env('APP_URL') . '/storage/pdfs/eecc',
            'visibility' => 'public',
        ]
    ],

];
