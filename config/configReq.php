<?php
/**
 * Devuelve la lista de los requerimientos
 * 
 * @autor Dickson Morales
 * @return Array
 */ 
return [
    [
        'name' => 'REQ01',
        'flujo' => [
            [
                'controlador' => 'Modules\User\Http\Controllers\PublicPrivate\LoginPrivateUserController',
                'metodo' => 'login',
                'request' => [
                    'password' => '5103',
                    'rut' => '108152494',
                    'tarjeta' => '5102'
                ]
            ]
        ]
    ],
    [
        'name' => 'REQ02'
    ],
    [
        'name' => 'REQ03'
    ],
    [
        'name' => 'REQ04'
    ],
    [
        'name' => 'REQ05'
    ],
    [
        'name' => 'REQ06'
    ],
    [
        'name' => 'REQ07'
    ],
    [
        'name' => 'REQ08'
    ],
    [
        'name' => 'REQ09'
    ],
    [
        'name' => 'REQ10'
    ],
    [
        'name' => 'REQ11'
    ],
    [
        'name' => 'REQ12'
    ],
    [
        'name' => 'REQ13'
    ],
    [
        'name' => 'REQ00 (Trazabilidad)'
    ],
];