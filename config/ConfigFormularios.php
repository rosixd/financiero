<?php
/**
 *  MA: Tramas y URL de los WS y MW del sistema.
 */

return [
	"CreditoConsumo" => [
        'nombre'         => 'CreditoConsumo',
        'nombre_archivo' => 'CreditoConsumo.txt',
        'directorio'     => 'CreditoConsumo',
        'ruta_local'     => 'filesystems.disks.public.root'
	],
	"prueba" => [
        'nombre'         => 'prueba',
        'nombre_archivo' => 'prueba.txt',
        'directorio'     => 'prueba',
        'ruta_local'     => 'filesystems.disks.public.root'
	],
];