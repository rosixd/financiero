<?php
    /**
   	 * MA: Confing de Session de PublicPrivate User
   	 * 
   	 *
   	 * @return Array Config
   	*/

return [

     /**
   	 * MA:Nombre de la de Session 
   	 * 
   	*/
   'Session_name' => 'User_public',


    /**
   	 * MA:tiempo de expiracion de la session, se expresa en  Segundos 
   	 * 
   	*/
   'Time_Session'=> 180,

   /**
	* MA: tiempo de alerta de session
	*/
	'Alert_Time' => 150,
];
